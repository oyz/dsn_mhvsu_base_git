##== TP ==##
#set_property MARK_DEBUG true [get_nets {TP_in[*]}]


##== MEM_SIO ==##
##  #set_property MARK_DEBUG true [get_nets {*MEM_SIO_in*}]
##  #
##  #set_property MARK_DEBUG true [get_nets {control_eeprom__11AA160T_inst/r_clk_en_200K               }]

# based on 10MHz
set_property MARK_DEBUG true [get_nets {control_eeprom__11AA160T_inst/r_state[*]                  }]
set_property MARK_DEBUG true [get_nets {control_eeprom__11AA160T_inst/r_idx_subframe[*]           }]
set_property MARK_DEBUG true [get_nets {control_eeprom__11AA160T_inst/r_cnt_bytes_DAT[*]          }]
set_property MARK_DEBUG true [get_nets {control_eeprom__11AA160T_inst/i_frame_data_CMD[*]         }]

##  #
##  set_property MARK_DEBUG true [get_nets {control_eeprom__11AA160T_inst/i_SCIO_DI                   }]
##  set_property MARK_DEBUG true [get_nets {control_eeprom__11AA160T_inst/r_SCIO_OE                   }]
set_property MARK_DEBUG true [get_nets {control_eeprom__11AA160T_inst/r_SCIO_DO                   }]
set_property MARK_DEBUG true [get_nets {control_eeprom__11AA160T_inst/r_buf_SCIO_DO[*]            }]

##  #
##  #set_property MARK_DEBUG true [get_nets {control_eeprom__11AA160T_inst/r_flag_load__frame_data_STA }]
##  #set_property MARK_DEBUG true [get_nets {control_eeprom__11AA160T_inst/r_frame_data_STA[*]         }]
##  #set_property MARK_DEBUG true [get_nets {control_eeprom__11AA160T_inst/o_frame_data_STA[*]         }]
##  set_property MARK_DEBUG true [get_nets {control_eeprom__11AA160T_inst/r_frame_data_DI[*]          }]
##  #

# based on 104MHz
set_property MARK_DEBUG true [get_nets {control_eeprom__11AA160T_inst/EEPROM_fifo_wr_inst/rst     }]
set_property MARK_DEBUG true [get_nets {control_eeprom__11AA160T_inst/EEPROM_fifo_wr_inst/wr_en   }]
set_property MARK_DEBUG true [get_nets {control_eeprom__11AA160T_inst/EEPROM_fifo_wr_inst/din[*]  }]

# based on 10MHz
set_property MARK_DEBUG true [get_nets {control_eeprom__11AA160T_inst/EEPROM_fifo_wr_inst/rd_en   }]
set_property MARK_DEBUG true [get_nets {control_eeprom__11AA160T_inst/EEPROM_fifo_wr_inst/dout[*] }]

##== SPIO ==##
##  set_property MARK_DEBUG true [get_nets {master_spi_mcp23s17_inst/o_S1_SPI_CSB[*]}]
##  set_property MARK_DEBUG true [get_nets {master_spi_mcp23s17_inst/o_S2_SPI_CSB[*]}]
##  set_property MARK_DEBUG true [get_nets {master_spi_mcp23s17_inst/o_S3_SPI_CSB[*]}]
##  set_property MARK_DEBUG true [get_nets {master_spi_mcp23s17_inst/o_S4_SPI_CSB[*]}]
##  set_property MARK_DEBUG true [get_nets {master_spi_mcp23s17_inst/o_S5_SPI_CSB[*]}]
##  set_property MARK_DEBUG true [get_nets {master_spi_mcp23s17_inst/o_S6_SPI_CSB[*]}]
##  set_property MARK_DEBUG true [get_nets {master_spi_mcp23s17_inst/o_S7_SPI_CSB[*]}]
##  set_property MARK_DEBUG true [get_nets {master_spi_mcp23s17_inst/o_S8_SPI_CSB[*]}]
##  #
##  set_property MARK_DEBUG true [get_nets master_spi_mcp23s17_inst/o_SPIOx_SCLK]
##  set_property MARK_DEBUG true [get_nets master_spi_mcp23s17_inst/o_SPIOx_MOSI]
##  set_property MARK_DEBUG true [get_nets master_spi_mcp23s17_inst/i_SPIOx_MISO]
##  
##  ##  #
##  ##  set_property MARK_DEBUG true [get_nets {master_spi_mcp23s17_inst/state[*]}]
##  ##  set_property MARK_DEBUG true [get_nets {master_spi_mcp23s17_inst/r_fbit_index[*]}]
##  ##  set_property MARK_DEBUG true [get_nets {master_spi_mcp23s17_inst/r_shift_send_w32[*]}]
##  ##  set_property MARK_DEBUG true [get_nets {master_spi_mcp23s17_inst/r_shift_recv_w16[*]}]
##  ##  set_property MARK_DEBUG true [get_nets master_spi_mcp23s17_inst/r_R_W_bar]
##  ##  set_property MARK_DEBUG true [get_nets {master_spi_mcp23s17_inst/r_rd_DA[*]}]
##  ##  set_property MARK_DEBUG true [get_nets {master_spi_mcp23s17_inst/r_rd_DB[*]}]
##  #
##  ##  set_property MARK_DEBUG true [get_nets master_spi_mcp23s17_inst/r_trig_SPI_frame]
##  ##  set_property MARK_DEBUG true [get_nets master_spi_mcp23s17_inst/r_done_SPI_frame]
##  
##  set_property MARK_DEBUG true [get_nets master_spi_mcp23s17_inst/reset_n              ]
##  set_property MARK_DEBUG true [get_nets master_spi_mcp23s17_inst/i_trig_SPI_frame     ]
##  set_property MARK_DEBUG true [get_nets master_spi_mcp23s17_inst/o_done_SPI_frame     ]
##  set_property MARK_DEBUG true [get_nets master_spi_mcp23s17_inst/o_done_SPI_frame_TO  ]
##  set_property MARK_DEBUG true [get_nets master_spi_mcp23s17_inst/o_busy_SPI_frame     ]
##  set_property MARK_DEBUG true [get_nets master_spi_mcp23s17_inst/i_socket_en[*]       ]
##  set_property MARK_DEBUG true [get_nets master_spi_mcp23s17_inst/i_CS_en[*]           ]
##  set_property MARK_DEBUG true [get_nets master_spi_mcp23s17_inst/i_forced_pin_mode_en ]
##  set_property MARK_DEBUG true [get_nets master_spi_mcp23s17_inst/i_forced_sig_mosi    ]
##  set_property MARK_DEBUG true [get_nets master_spi_mcp23s17_inst/i_forced_sig_sclk    ]
##  set_property MARK_DEBUG true [get_nets master_spi_mcp23s17_inst/i_forced_sig_csel    ]
##  set_property MARK_DEBUG true [get_nets master_spi_mcp23s17_inst/i_pin_adrs_A[*]      ]
##  set_property MARK_DEBUG true [get_nets master_spi_mcp23s17_inst/i_R_W_bar            ]
##  set_property MARK_DEBUG true [get_nets master_spi_mcp23s17_inst/i_reg_adrs_A[*]      ]
##  set_property MARK_DEBUG true [get_nets master_spi_mcp23s17_inst/i_wr_DA[*]           ]
##  set_property MARK_DEBUG true [get_nets master_spi_mcp23s17_inst/i_wr_DB[*]           ]
##  set_property MARK_DEBUG true [get_nets master_spi_mcp23s17_inst/o_rd_DA[*]           ]
##  set_property MARK_DEBUG true [get_nets master_spi_mcp23s17_inst/o_rd_DB[*]           ]



##== DAC ==##
set_property MARK_DEBUG true [get_nets master_spi_dac_AD5754_inst/i_trig_SPI_frame]
set_property MARK_DEBUG true [get_nets master_spi_dac_AD5754_inst/i_trig_DAC_init]
set_property MARK_DEBUG true [get_nets master_spi_dac_AD5754_inst/i_trig_DAC_update]
##  #
set_property MARK_DEBUG true [get_nets master_spi_dac_AD5754_inst/o_done_SPI_frame]
##  set_property MARK_DEBUG true [get_nets master_spi_dac_AD5754_inst/o_done_SPI_frame_TO]
set_property MARK_DEBUG true [get_nets master_spi_dac_AD5754_inst/o_done_DAC_init]
##  set_property MARK_DEBUG true [get_nets master_spi_dac_AD5754_inst/o_done_DAC_init_TO]
set_property MARK_DEBUG true [get_nets master_spi_dac_AD5754_inst/o_done_DAC_update]
##  set_property MARK_DEBUG true [get_nets master_spi_dac_AD5754_inst/o_done_DAC_update_TO]
## 
set_property MARK_DEBUG true [get_nets {master_spi_dac_AD5754_inst/o_SCLK[*]}]
set_property MARK_DEBUG true [get_nets {master_spi_dac_AD5754_inst/o_SYNC_N[*]}]
set_property MARK_DEBUG true [get_nets {master_spi_dac_AD5754_inst/o_DIN[*]}]
set_property MARK_DEBUG true [get_nets {master_spi_dac_AD5754_inst/i_SDO[*]}]
#
set_property MARK_DEBUG true [get_nets {master_spi_dac_AD5754_inst/master_spi_dac_AD5754_core_0_inst/r_sdo_pdata[*]      }]
set_property MARK_DEBUG true [get_nets {master_spi_dac_AD5754_inst/master_spi_dac_AD5754_core_0_inst/r_test_sdo_pdata[*] }]
set_property MARK_DEBUG true [get_nets {master_spi_dac_AD5754_inst/master_spi_dac_AD5754_core_1_inst/r_sdo_pdata[*]      }]
set_property MARK_DEBUG true [get_nets {master_spi_dac_AD5754_inst/master_spi_dac_AD5754_core_1_inst/r_test_sdo_pdata[*] }]
set_property MARK_DEBUG true [get_nets {master_spi_dac_AD5754_inst/master_spi_dac_AD5754_core_2_inst/r_sdo_pdata[*]      }]
set_property MARK_DEBUG true [get_nets {master_spi_dac_AD5754_inst/master_spi_dac_AD5754_core_2_inst/r_test_sdo_pdata[*] }]
set_property MARK_DEBUG true [get_nets {master_spi_dac_AD5754_inst/master_spi_dac_AD5754_core_3_inst/r_sdo_pdata[*]      }]
set_property MARK_DEBUG true [get_nets {master_spi_dac_AD5754_inst/master_spi_dac_AD5754_core_3_inst/r_test_sdo_pdata[*] }]

##== slave SPI ==##
##  set_property MARK_DEBUG true [get_nets r_M0_SPI_CS_B_BUF]
##  set_property MARK_DEBUG true [get_nets r_M0_SPI_CLK]
##  set_property MARK_DEBUG true [get_nets r_M0_SPI_MOSI]
##  set_property MARK_DEBUG true [get_nets r_M0_SPI_MISO]
##  set_property MARK_DEBUG true [get_nets r_M0_SPI_MISO_EN]
##  set_property MARK_DEBUG true [get_nets r_M1_SPI_CS_B_BUF]
##  set_property MARK_DEBUG true [get_nets r_M1_SPI_CLK]
##  set_property MARK_DEBUG true [get_nets r_M1_SPI_MOSI]
##  set_property MARK_DEBUG true [get_nets r_M1_SPI_MISO]
##  set_property MARK_DEBUG true [get_nets r_M1_SPI_MISO_EN]
#
set_property MARK_DEBUG true [get_nets slave_spi_mth_brd__M0_inst/i_SPI_CS_B]
set_property MARK_DEBUG true [get_nets slave_spi_mth_brd__M0_inst/i_SPI_CLK]
set_property MARK_DEBUG true [get_nets slave_spi_mth_brd__M0_inst/i_SPI_MOSI]
set_property MARK_DEBUG true [get_nets slave_spi_mth_brd__M0_inst/o_SPI_MISO]
set_property MARK_DEBUG true [get_nets slave_spi_mth_brd__M0_inst/o_SPI_MISO_EN]
set_property MARK_DEBUG true [get_nets slave_spi_mth_brd__M1_inst/i_SPI_CS_B]
set_property MARK_DEBUG true [get_nets slave_spi_mth_brd__M1_inst/i_SPI_CLK]
set_property MARK_DEBUG true [get_nets slave_spi_mth_brd__M1_inst/i_SPI_MOSI]
set_property MARK_DEBUG true [get_nets slave_spi_mth_brd__M1_inst/o_SPI_MISO]
set_property MARK_DEBUG true [get_nets slave_spi_mth_brd__M1_inst/o_SPI_MISO_EN]
##  ##  #
##  set_property MARK_DEBUG true [get_nets {slave_spi_mth_brd__M0_inst/r_frame_index[*]}]
##  set_property MARK_DEBUG true [get_nets {slave_spi_mth_brd__M0_inst/r_frame_ctrl[*]}]
##  set_property MARK_DEBUG true [get_nets slave_spi_mth_brd__M0_inst/w_frame_ctrl_read]
##  set_property MARK_DEBUG true [get_nets {slave_spi_mth_brd__M0_inst/r_frame_adrs[*]}]
##  set_property MARK_DEBUG true [get_nets slave_spi_mth_brd__M0_inst/r_frame_adrs_valid]
##  set_property MARK_DEBUG true [get_nets {slave_spi_mth_brd__M0_inst/r_frame_mosi[*]}]
##  set_property MARK_DEBUG true [get_nets slave_spi_mth_brd__M0_inst/r_frame_mosi_trig]
##  set_property MARK_DEBUG true [get_nets {slave_spi_mth_brd__M0_inst/r_frame_miso[*]}]
##  set_property MARK_DEBUG true [get_nets slave_spi_mth_brd__M0_inst/r_frame_miso_trig]
##  #
##  set_property MARK_DEBUG true [get_nets {slave_spi_mth_brd__M0_inst/r_port_wi_sadrs_h000[*]}]
##  set_property MARK_DEBUG true [get_nets {slave_spi_mth_brd__M0_inst/r_port_wi_sadrs_h004[*]}]
##  set_property MARK_DEBUG true [get_nets {slave_spi_mth_brd__M0_inst/r_port_wi_sadrs_h008[*]}]
##  set_property MARK_DEBUG true [get_nets {slave_spi_mth_brd__M0_inst/r_port_wi_sadrs_h00C[*]}]
##  set_property MARK_DEBUG true [get_nets {slave_spi_mth_brd__M0_inst/r_port_wi_sadrs_h010[*]}]
##  set_property MARK_DEBUG true [get_nets {slave_spi_mth_brd__M0_inst/r_port_wi_sadrs_h014[*]}]
##  set_property MARK_DEBUG true [get_nets {slave_spi_mth_brd__M0_inst/r_port_wi_sadrs_h018[*]}]
##  set_property MARK_DEBUG true [get_nets {slave_spi_mth_brd__M0_inst/r_port_wi_sadrs_h060[*]}]
##  set_property MARK_DEBUG true [get_nets {slave_spi_mth_brd__M0_inst/r_port_wi_sadrs_h064[*]}]
##  set_property MARK_DEBUG true [get_nets {slave_spi_mth_brd__M0_inst/r_port_wi_sadrs_h068[*]}]
##  set_property MARK_DEBUG true [get_nets {slave_spi_mth_brd__M0_inst/r_port_wi_sadrs_h06C[*]}]
##  set_property MARK_DEBUG true [get_nets {slave_spi_mth_brd__M0_inst/r_port_wi_sadrs_h070[*]}]
##  set_property MARK_DEBUG true [get_nets {slave_spi_mth_brd__M0_inst/r_port_wi_sadrs_h074[*]}]
##  set_property MARK_DEBUG true [get_nets {slave_spi_mth_brd__M0_inst/r_port_wi_sadrs_h078[*]}]
##  set_property MARK_DEBUG true [get_nets {slave_spi_mth_brd__M0_inst/r_port_wi_sadrs_h07C[*]}]
##  set_property MARK_DEBUG true [get_nets {slave_spi_mth_brd__M0_inst/r_port_wi_sadrs_h01C[*]}]
##  set_property MARK_DEBUG true [get_nets {slave_spi_mth_brd__M0_inst/r_port_wi_sadrs_h040[*]}]
##  #
##  set_property MARK_DEBUG true [get_nets {slave_spi_mth_brd__M0_inst/r_port_ti_sadrs_h104[*]}]
##  set_property MARK_DEBUG true [get_nets {slave_spi_mth_brd__M0_inst/r_port_ti_sadrs_h114[*]}]
##  set_property MARK_DEBUG true [get_nets {slave_spi_mth_brd__M0_inst/r_port_ti_sadrs_h118[*]}]
##  set_property MARK_DEBUG true [get_nets {slave_spi_mth_brd__M0_inst/r_port_ti_sadrs_h11C[*]}]
##  #
##  set_property MARK_DEBUG true [get_nets {slave_spi_mth_brd__M0_inst/r_port_to_sadrs_h194[*]}]
##  set_property MARK_DEBUG true [get_nets {slave_spi_mth_brd__M0_inst/r_port_to_sadrs_h198[*]}]
##  set_property MARK_DEBUG true [get_nets {slave_spi_mth_brd__M0_inst/r_port_to_sadrs_h19C[*]}]

# based on 104MHz 
set_property MARK_DEBUG true [get_nets {slave_spi_mth_brd__M0_inst/r_port_pi_sadrs_h24C[*]}]
set_property MARK_DEBUG true [get_nets {slave_spi_mth_brd__M0_inst/r_wr__sadrs_h24C       }]

##  #
##  ## set_property MARK_DEBUG true [get_nets {slave_spi_mth_brd__M1_inst/r_frame_index[*]}]
##  ## set_property MARK_DEBUG true [get_nets {slave_spi_mth_brd__M1_inst/r_frame_ctrl[*]}]
##  ## set_property MARK_DEBUG true [get_nets slave_spi_mth_brd__M1_inst/w_frame_ctrl_read]
##  ## set_property MARK_DEBUG true [get_nets {slave_spi_mth_brd__M1_inst/r_frame_adrs[*]}]
##  ## set_property MARK_DEBUG true [get_nets slave_spi_mth_brd__M1_inst/r_frame_adrs_valid]
##  ## set_property MARK_DEBUG true [get_nets {slave_spi_mth_brd__M1_inst/r_frame_mosi[*]}]
##  ## set_property MARK_DEBUG true [get_nets slave_spi_mth_brd__M1_inst/r_frame_mosi_trig]
##  ## set_property MARK_DEBUG true [get_nets {slave_spi_mth_brd__M1_inst/r_frame_miso[*]}]
##  ## set_property MARK_DEBUG true [get_nets slave_spi_mth_brd__M1_inst/r_frame_miso_trig]
##  ## set_property MARK_DEBUG true [get_nets {slave_spi_mth_brd__M1_inst/r_port_wi_sadrs_h000[*]}]
##  ## set_property MARK_DEBUG true [get_nets {slave_spi_mth_brd__M1_inst/r_port_wi_sadrs_h008[*]}]

##== TRIG IO ==##
##  set_property MARK_DEBUG true [get_nets r_M_TRIG*]
##  set_property MARK_DEBUG true [get_nets r_M_PRE_TRIG*]
##  set_property MARK_DEBUG true [get_nets r_M_BUSY_B_OUT*]

##== EXT_TRIG IO ==##
##  set_property MARK_DEBUG true [get_nets r_EXT_TRIG*]
##  set_property MARK_DEBUG true [get_nets r_EXT_BUSY_B_OUT*]
#
##  set_property MARK_DEBUG true [get_nets {control_ext_trig_inst/i_PIN_trig_disable          }]
##  set_property MARK_DEBUG true [get_nets {control_ext_trig_inst/i_M_TRIG_PIN                }]
##  set_property MARK_DEBUG true [get_nets {control_ext_trig_inst/i_M_PRE_TRIG_PIN            }]
##  set_property MARK_DEBUG true [get_nets {control_ext_trig_inst/i_AUX_TRIG_PIN              }]
##  set_property MARK_DEBUG true [get_nets {control_ext_trig_inst/i_M_TRIG_SW                 }]
##  set_property MARK_DEBUG true [get_nets {control_ext_trig_inst/i_M_PRE_TRIG_SW             }]
##  set_property MARK_DEBUG true [get_nets {control_ext_trig_inst/i_AUX_TRIG_SW               }]
##  set_property MARK_DEBUG true [get_nets {control_ext_trig_inst/i_conf_M_TRIG[*]            }]
##  set_property MARK_DEBUG true [get_nets {control_ext_trig_inst/i_conf_M_PRE_TRIG[*]        }]
##  set_property MARK_DEBUG true [get_nets {control_ext_trig_inst/i_conf_AUX[*]               }]
##  set_property MARK_DEBUG true [get_nets {control_ext_trig_inst/i_count_delay_trig_spio[*]  }]
##  set_property MARK_DEBUG true [get_nets {control_ext_trig_inst/i_count_delay_trig_dac[*]   }]
##  set_property MARK_DEBUG true [get_nets {control_ext_trig_inst/i_count_delay_trig_adc[*]   }]
##  set_property MARK_DEBUG true [get_nets {control_ext_trig_inst/o_spio_busy_cowork          }]
##  set_property MARK_DEBUG true [get_nets {control_ext_trig_inst/o_dac_busy_cowork           }]
##  set_property MARK_DEBUG true [get_nets {control_ext_trig_inst/o_adc_busy_cowork           }]
##  set_property MARK_DEBUG true [get_nets {control_ext_trig_inst/o_spio_trig_cowork          }]
##  set_property MARK_DEBUG true [get_nets {control_ext_trig_inst/o_dac_trig_cowork           }]
##  set_property MARK_DEBUG true [get_nets {control_ext_trig_inst/o_adc_trig_cowork           }]
##  #
##  set_property MARK_DEBUG true [get_nets {control_ext_trig_inst/control_delay_trig__spio_inst/r_count_delay[*] }]
##  set_property MARK_DEBUG true [get_nets {control_ext_trig_inst/control_delay_trig__dac_inst/r_count_delay[*] }]
##  set_property MARK_DEBUG true [get_nets {control_ext_trig_inst/control_delay_trig__adc_inst/r_count_delay[*] }]
##  #
##  set_property MARK_DEBUG true [get_nets {control_ext_trig_inst/control_delay_trig__spio_inst/i_trig }]
##  set_property MARK_DEBUG true [get_nets {control_ext_trig_inst/control_delay_trig__dac_inst/i_trig }]
##  set_property MARK_DEBUG true [get_nets {control_ext_trig_inst/control_delay_trig__adc_inst/i_trig }]
##  #
##  set_property MARK_DEBUG true [get_nets {control_ext_trig_inst/control_delay_trig__spio_inst/r_trig[*] }]
##  set_property MARK_DEBUG true [get_nets {control_ext_trig_inst/control_delay_trig__dac_inst/r_trig[*] }]
##  set_property MARK_DEBUG true [get_nets {control_ext_trig_inst/control_delay_trig__adc_inst/r_trig[*] }]


##== time stamp ==##
set_property MARK_DEBUG true [get_nets {sub_timestamp_inst/r_global_time_idx[*]}]


##== ADC ==##
##  set_property MARK_DEBUG true [get_nets r_ADCx_CNV*]
##  set_property MARK_DEBUG true [get_nets r_ADCx_SCK*]
##  #
##  set_property MARK_DEBUG true [get_nets r_ADC0_DCO_*]
##  set_property MARK_DEBUG true [get_nets r_ADC0_SDOA*]
##  set_property MARK_DEBUG true [get_nets r_ADC0_SDOB*]
##  set_property MARK_DEBUG true [get_nets r_ADC0_SDOC*]
##  set_property MARK_DEBUG true [get_nets r_ADC0_SDOD*]
##  set_property MARK_DEBUG true [get_nets r_ADC1_DCO_*]
##  set_property MARK_DEBUG true [get_nets r_ADC1_SDOA*]
##  set_property MARK_DEBUG true [get_nets r_ADC1_SDOB*]
##  set_property MARK_DEBUG true [get_nets r_ADC1_SDOC*]
##  set_property MARK_DEBUG true [get_nets r_ADC1_SDOD*]
##  set_property MARK_DEBUG true [get_nets r_ADC2_DCO_*]
##  set_property MARK_DEBUG true [get_nets r_ADC2_SDOA*]
##  set_property MARK_DEBUG true [get_nets r_ADC2_SDOB*]
##  set_property MARK_DEBUG true [get_nets r_ADC2_SDOC*]
##  set_property MARK_DEBUG true [get_nets r_ADC2_SDOD*]
##  set_property MARK_DEBUG true [get_nets r_ADC3_DCO_*]
##  set_property MARK_DEBUG true [get_nets r_ADC3_SDOA*]
##  set_property MARK_DEBUG true [get_nets r_ADC3_SDOB*]
##  set_property MARK_DEBUG true [get_nets r_ADC3_SDOC*]
##  set_property MARK_DEBUG true [get_nets r_ADC3_SDOD*]
##  #
##  #set_property MARK_DEBUG true [get_nets {control_adc_ddr_LTC2325_inst/control_adc_ddr_LTC2325_sdo_recv_core__inst0/r_cnt_SDO_bits[*]}]
##  #set_property MARK_DEBUG true [get_nets control_adc_ddr_LTC2325_inst/control_adc_ddr_LTC2325_sdo_recv_core__inst0/r_done_cnt_SDO_bits]
##  #
##  set_property MARK_DEBUG true [get_nets control_adc_ddr_LTC2325_inst/control_adc_ddr_LTC2325_sdo_recv_core__inst0/w_trig_load_SDO_pclk]
##  set_property MARK_DEBUG true [get_nets control_adc_ddr_LTC2325_inst/control_adc_ddr_LTC2325_sdo_recv_core__inst0/w_trig_load_SDO_pclk__pre]
##  set_property MARK_DEBUG true [get_nets {control_adc_ddr_LTC2325_inst/control_adc_ddr_LTC2325_sdo_recv_core__inst0/r_p_data_A_pclk[*]}]
##  set_property MARK_DEBUG true [get_nets {control_adc_ddr_LTC2325_inst/control_adc_ddr_LTC2325_sdo_recv_core__inst0/r_p_data_B_pclk[*]}]
##  set_property MARK_DEBUG true [get_nets {control_adc_ddr_LTC2325_inst/control_adc_ddr_LTC2325_sdo_recv_core__inst0/r_p_data_C_pclk[*]}]
##  set_property MARK_DEBUG true [get_nets {control_adc_ddr_LTC2325_inst/control_adc_ddr_LTC2325_sdo_recv_core__inst0/r_p_data_D_pclk[*]}]
##  #
##  set_property MARK_DEBUG true [get_nets control_adc_ddr_LTC2325_inst/control_adc_ddr_LTC2325_sdo_recv_core__inst1/w_trig_load_SDO_pclk]
##  set_property MARK_DEBUG true [get_nets control_adc_ddr_LTC2325_inst/control_adc_ddr_LTC2325_sdo_recv_core__inst1/w_trig_load_SDO_pclk__pre]
##  set_property MARK_DEBUG true [get_nets {control_adc_ddr_LTC2325_inst/control_adc_ddr_LTC2325_sdo_recv_core__inst1/r_p_data_A_pclk[*]}]
##  set_property MARK_DEBUG true [get_nets {control_adc_ddr_LTC2325_inst/control_adc_ddr_LTC2325_sdo_recv_core__inst1/r_p_data_B_pclk[*]}]
##  set_property MARK_DEBUG true [get_nets {control_adc_ddr_LTC2325_inst/control_adc_ddr_LTC2325_sdo_recv_core__inst1/r_p_data_C_pclk[*]}]
##  set_property MARK_DEBUG true [get_nets {control_adc_ddr_LTC2325_inst/control_adc_ddr_LTC2325_sdo_recv_core__inst1/r_p_data_D_pclk[*]}]
##  #
##  set_property MARK_DEBUG true [get_nets control_adc_ddr_LTC2325_inst/control_adc_ddr_LTC2325_sdo_recv_core__inst2/w_trig_load_SDO_pclk]
##  set_property MARK_DEBUG true [get_nets control_adc_ddr_LTC2325_inst/control_adc_ddr_LTC2325_sdo_recv_core__inst2/w_trig_load_SDO_pclk__pre]
##  set_property MARK_DEBUG true [get_nets {control_adc_ddr_LTC2325_inst/control_adc_ddr_LTC2325_sdo_recv_core__inst2/r_p_data_A_pclk[*]}]
##  set_property MARK_DEBUG true [get_nets {control_adc_ddr_LTC2325_inst/control_adc_ddr_LTC2325_sdo_recv_core__inst2/r_p_data_B_pclk[*]}]
##  set_property MARK_DEBUG true [get_nets {control_adc_ddr_LTC2325_inst/control_adc_ddr_LTC2325_sdo_recv_core__inst2/r_p_data_C_pclk[*]}]
##  set_property MARK_DEBUG true [get_nets {control_adc_ddr_LTC2325_inst/control_adc_ddr_LTC2325_sdo_recv_core__inst2/r_p_data_D_pclk[*]}]
##  #
##  set_property MARK_DEBUG true [get_nets control_adc_ddr_LTC2325_inst/control_adc_ddr_LTC2325_sdo_recv_core__inst3/w_trig_load_SDO_pclk]
##  set_property MARK_DEBUG true [get_nets control_adc_ddr_LTC2325_inst/control_adc_ddr_LTC2325_sdo_recv_core__inst3/w_trig_load_SDO_pclk__pre]
##  set_property MARK_DEBUG true [get_nets {control_adc_ddr_LTC2325_inst/control_adc_ddr_LTC2325_sdo_recv_core__inst3/r_p_data_A_pclk[*]}]
##  set_property MARK_DEBUG true [get_nets {control_adc_ddr_LTC2325_inst/control_adc_ddr_LTC2325_sdo_recv_core__inst3/r_p_data_B_pclk[*]}]
##  set_property MARK_DEBUG true [get_nets {control_adc_ddr_LTC2325_inst/control_adc_ddr_LTC2325_sdo_recv_core__inst3/r_p_data_C_pclk[*]}]
##  set_property MARK_DEBUG true [get_nets {control_adc_ddr_LTC2325_inst/control_adc_ddr_LTC2325_sdo_recv_core__inst3/r_p_data_D_pclk[*]}]
##  #
##  set_property MARK_DEBUG true [get_nets control_adc_ddr_LTC2325_inst/acc_16b_to_24b__adc0_a_inst/i_trig_clear]
##  set_property MARK_DEBUG true [get_nets control_adc_ddr_LTC2325_inst/acc_16b_to_24b__adc0_a_inst/i_trig_load]
##  set_property MARK_DEBUG true [get_nets {control_adc_ddr_LTC2325_inst/acc_16b_to_24b__adc0_a_inst/r_data_acc[*]}]
##  #
##  set_property MARK_DEBUG true [get_nets control_adc_ddr_LTC2325_inst/acc_16b_to_24b__adc0_b_inst/i_trig_clear]
##  set_property MARK_DEBUG true [get_nets control_adc_ddr_LTC2325_inst/acc_16b_to_24b__adc0_b_inst/i_trig_load]
##  set_property MARK_DEBUG true [get_nets {control_adc_ddr_LTC2325_inst/acc_16b_to_24b__adc0_b_inst/r_data_acc[*]}]
##  #
##  set_property MARK_DEBUG true [get_nets control_adc_ddr_LTC2325_inst/acc_16b_to_24b__adc0_c_inst/i_trig_clear]
##  set_property MARK_DEBUG true [get_nets control_adc_ddr_LTC2325_inst/acc_16b_to_24b__adc0_c_inst/i_trig_load]
##  set_property MARK_DEBUG true [get_nets {control_adc_ddr_LTC2325_inst/acc_16b_to_24b__adc0_c_inst/r_data_acc[*]}]
##  #
##  set_property MARK_DEBUG true [get_nets control_adc_ddr_LTC2325_inst/acc_16b_to_24b__adc0_d_inst/i_trig_clear]
##  set_property MARK_DEBUG true [get_nets control_adc_ddr_LTC2325_inst/acc_16b_to_24b__adc0_d_inst/i_trig_load]
##  set_property MARK_DEBUG true [get_nets {control_adc_ddr_LTC2325_inst/acc_16b_to_24b__adc0_d_inst/r_data_acc[*]}]
##  #
##  set_property MARK_DEBUG true [get_nets control_adc_ddr_LTC2325_inst/find_min_max_signed_16b__adc0_a_inst/i_trig_clear]
##  set_property MARK_DEBUG true [get_nets control_adc_ddr_LTC2325_inst/find_min_max_signed_16b__adc0_a_inst/i_trig_load]
##  set_property MARK_DEBUG true [get_nets {control_adc_ddr_LTC2325_inst/find_min_max_signed_16b__adc0_a_inst/r_data_min[*]}]
##  set_property MARK_DEBUG true [get_nets {control_adc_ddr_LTC2325_inst/find_min_max_signed_16b__adc0_a_inst/r_data_max[*]}]
##  #
##  set_property MARK_DEBUG true [get_nets control_adc_ddr_LTC2325_inst/find_min_max_signed_16b__adc0_b_inst/i_trig_clear]
##  set_property MARK_DEBUG true [get_nets control_adc_ddr_LTC2325_inst/find_min_max_signed_16b__adc0_b_inst/i_trig_load]
##  set_property MARK_DEBUG true [get_nets {control_adc_ddr_LTC2325_inst/find_min_max_signed_16b__adc0_b_inst/r_data_min[*]}]
##  set_property MARK_DEBUG true [get_nets {control_adc_ddr_LTC2325_inst/find_min_max_signed_16b__adc0_b_inst/r_data_max[*]}]
##  #
##  set_property MARK_DEBUG true [get_nets control_adc_ddr_LTC2325_inst/find_min_max_signed_16b__adc0_c_inst/i_trig_clear]
##  set_property MARK_DEBUG true [get_nets control_adc_ddr_LTC2325_inst/find_min_max_signed_16b__adc0_c_inst/i_trig_load]
##  set_property MARK_DEBUG true [get_nets {control_adc_ddr_LTC2325_inst/find_min_max_signed_16b__adc0_c_inst/r_data_min[*]}]
##  set_property MARK_DEBUG true [get_nets {control_adc_ddr_LTC2325_inst/find_min_max_signed_16b__adc0_c_inst/r_data_max[*]}]
##  #
##  set_property MARK_DEBUG true [get_nets control_adc_ddr_LTC2325_inst/find_min_max_signed_16b__adc0_d_inst/i_trig_clear]
##  set_property MARK_DEBUG true [get_nets control_adc_ddr_LTC2325_inst/find_min_max_signed_16b__adc0_d_inst/i_trig_load]
##  set_property MARK_DEBUG true [get_nets {control_adc_ddr_LTC2325_inst/find_min_max_signed_16b__adc0_d_inst/r_data_min[*]}]
##  set_property MARK_DEBUG true [get_nets {control_adc_ddr_LTC2325_inst/find_min_max_signed_16b__adc0_d_inst/r_data_max[*]}]
##  #
##  set_property MARK_DEBUG true [get_nets {control_adc_ddr_LTC2325_inst/r_trig_conv_single[*]}]
##  set_property MARK_DEBUG true [get_nets {control_adc_ddr_LTC2325_inst/r_trig_conv_run[*]}]
##  set_property MARK_DEBUG true [get_nets {control_adc_ddr_LTC2325_inst/r_cnt_period_set[*]}]
##  set_property MARK_DEBUG true [get_nets {control_adc_ddr_LTC2325_inst/r_cnt_CNV_set[*]}]
##  set_property MARK_DEBUG true [get_nets {control_adc_ddr_LTC2325_inst/r_cnt_period[*]}]
##  set_property MARK_DEBUG true [get_nets {control_adc_ddr_LTC2325_inst/r_idx_frame[*]}]
##  set_property MARK_DEBUG true [get_nets {control_adc_ddr_LTC2325_inst/r_cnt_CNV[*]}]
##  set_property MARK_DEBUG true [get_nets control_adc_ddr_LTC2325_inst/r_done_to]
##  set_property MARK_DEBUG true [get_nets {control_adc_ddr_LTC2325_inst/r_busy[*]}]
##  set_property MARK_DEBUG true [get_nets control_adc_ddr_LTC2325_inst/o_busy]
#
##  set_property MARK_DEBUG true [get_nets control_adc_ddr_LTC2325_inst/i_trig_conv_single]
##  set_property MARK_DEBUG true [get_nets control_adc_ddr_LTC2325_inst/i_trig_conv_run   ]
##  set_property MARK_DEBUG true [get_nets control_adc_ddr_LTC2325_inst/i_count_period_div4[*]]
##  set_property MARK_DEBUG true [get_nets control_adc_ddr_LTC2325_inst/i_count_conv_div4[*]  ]
##  set_property MARK_DEBUG true [get_nets control_adc_ddr_LTC2325_inst/o_busy_pclk   ]
##  set_property MARK_DEBUG true [get_nets control_adc_ddr_LTC2325_inst/o_done_to_pclk]

##------------------------------------------------------------------------##




##== TEMP SENSOR ==##
#
#set_property MARK_DEBUG true [get_nets r_temp_sig*]
#set_property MARK_DEBUG true [get_nets r_toggle_temp_sig*]



##------------------------------------------------------------------------##


##== soft CPU (soft_cpu_mcs_inst) ==##

## ##set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/soft_cpu_mcs_inst/Reset            ]
## set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/soft_cpu_mcs_inst/IO_addr_strobe   ]
## set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/soft_cpu_mcs_inst/IO_address[*]    ]
## ##set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/soft_cpu_mcs_inst/IO_byte_enable   ]
## set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/soft_cpu_mcs_inst/IO_read_data[*]  ]
## set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/soft_cpu_mcs_inst/IO_read_strobe   ]
## set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/soft_cpu_mcs_inst/IO_write_data[*] ]
## set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/soft_cpu_mcs_inst/IO_write_strobe  ]
## set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/soft_cpu_mcs_inst/IO_ready         ]


##== IO bridge (mcs_io_bridge_inst2) ==##

## set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/mcs_io_bridge_inst2/o_lan_wi_00[*] ]
## set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/mcs_io_bridge_inst2/o_lan_wi_01[*] ]
## set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/mcs_io_bridge_inst2/i_lan_wo_20[*] ]
## set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/mcs_io_bridge_inst2/o_lan_ti_40[*] ]
## set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/mcs_io_bridge_inst2/i_lan_to_60[*] ]
## set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/mcs_io_bridge_inst2/o_lan_wr_80    ]
## set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/mcs_io_bridge_inst2/o_lan_pi_80[*] ]
## set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/mcs_io_bridge_inst2/o_lan_rd_A0    ]
## set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/mcs_io_bridge_inst2/i_lan_po_A0[*] ]
#
#
#set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/mcs_io_bridge_inst2/o_port_wi_00[*] ]
#set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/mcs_io_bridge_inst2/o_port_wi_01[*] ]
#set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/mcs_io_bridge_inst2/o_port_wi_02[*] ]
#set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/mcs_io_bridge_inst2/o_port_wi_03[*] ]
#
## set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/mcs_io_bridge_inst2/o_port_wi_12[*] ]
## set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/mcs_io_bridge_inst2/o_port_wi_13[*] ]
#
#set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/mcs_io_bridge_inst2/i_port_wo_21[*] ]
#set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/mcs_io_bridge_inst2/i_port_wo_23[*] ]
#
#set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/mcs_io_bridge_inst2/o_port_ti_45[*] ]
## set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/mcs_io_bridge_inst2/o_port_ti_53[*] ]
#
#set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/mcs_io_bridge_inst2/i_port_to_65[*] ]
## set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/mcs_io_bridge_inst2/i_port_to_73[*] ]
#
##  set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/mcs_io_bridge_inst2/o_port_wr_93    ]
##  set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/mcs_io_bridge_inst2/o_port_pi_93[*] ]
##  #
##  set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/mcs_io_bridge_inst2/o_port_rd_B3    ]
##  set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/mcs_io_bridge_inst2/i_port_po_B3[*] ]

##  
##  ##== LAN SPI (master_spi_wz850_inst) ==##
##  
##  set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/master_spi_wz850_inst/i_trig_LAN_reset]
##  set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/master_spi_wz850_inst/o_done_LAN_reset]
##  set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/master_spi_wz850_inst/i_trig_SPI_frame]
##  #set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/master_spi_wz850_inst/o_done_SPI_frame]
##  #set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/master_spi_wz850_inst/o_done_SPI_frame_TO]
##  #
##  set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/master_spi_wz850_inst/o_LAN_RSTn]
##  #set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/master_spi_wz850_inst/i_LAN_INTn]
##  set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/master_spi_wz850_inst/o_LAN_SCSn]
##  set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/master_spi_wz850_inst/o_LAN_SCLK]
##  set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/master_spi_wz850_inst/o_LAN_MOSI]
##  set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/master_spi_wz850_inst/i_LAN_MISO]
##  #
##  set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/master_spi_wz850_inst/i_frame_adrs[*]         ]
##  set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/master_spi_wz850_inst/i_frame_ctrl_blck_sel[*]]
##  set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/master_spi_wz850_inst/i_frame_ctrl_rdwr_sel   ]
##  set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/master_spi_wz850_inst/i_frame_ctrl_opmd_sel[*]]
##  set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/master_spi_wz850_inst/i_frame_num_byte_data[*]]
##  #
##  set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/master_spi_wz850_inst/i_frame_data_wr[*]      ]
##  set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/master_spi_wz850_inst/o_frame_done_wr         ]
##  set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/master_spi_wz850_inst/o_frame_data_rd[*]      ]
##  set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/master_spi_wz850_inst/o_frame_done_rd         ]
##  
##  
##  ##== LAN-FIFO (LAN_fifo_wr_inst )==##  
##  
##  set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/LAN_fifo_wr_inst/rst       ]
##  set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/LAN_fifo_wr_inst/wr_en     ]
##  set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/LAN_fifo_wr_inst/din[*]    ]
##  set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/LAN_fifo_wr_inst/wr_ack    ]
##  set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/LAN_fifo_wr_inst/overflow  ]
##  set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/LAN_fifo_wr_inst/full      ]
##  set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/LAN_fifo_wr_inst/rd_en     ]
##  set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/LAN_fifo_wr_inst/dout[*]   ]
##  set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/LAN_fifo_wr_inst/valid     ]
##  set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/LAN_fifo_wr_inst/underflow ]
##  set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/LAN_fifo_wr_inst/empty     ]
##  
##  ##== LAN-FIFO (LAN_fifo_rd_inst) ==##  
##  
##  set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/LAN_fifo_rd_inst/rst       ]
##  set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/LAN_fifo_rd_inst/wr_en     ]
##  set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/LAN_fifo_rd_inst/din[*]    ]
##  set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/LAN_fifo_rd_inst/wr_ack    ]
##  set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/LAN_fifo_rd_inst/overflow  ]
##  set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/LAN_fifo_rd_inst/full      ]
##  set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/LAN_fifo_rd_inst/rd_en     ]
##  set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/LAN_fifo_rd_inst/dout[*]   ]
##  set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/LAN_fifo_rd_inst/valid     ]
##  set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/LAN_fifo_rd_inst/underflow ]
##  set_property MARK_DEBUG true [get_nets lan_endpoint_wrapper_inst/LAN_fifo_rd_inst/empty     ]
##  

##------------------------------------------------------------------------##

####
##  set_property C_CLK_INPUT_FREQ_HZ 300000000 [get_debug_cores dbg_hub]
##  set_property C_ENABLE_CLK_DIVIDER false [get_debug_cores dbg_hub]
##  set_property C_USER_SCAN_CHAIN 1 [get_debug_cores dbg_hub]
##  connect_debug_port dbg_hub/clk [get_nets sys_clk]


