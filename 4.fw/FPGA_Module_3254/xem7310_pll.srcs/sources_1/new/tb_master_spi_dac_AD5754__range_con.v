`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Module Name: tb_master_spi_dac_AD5754__range_con
// 
// http://www.asic-world.com/verilog/art_testbench_writing1.html
// http://www.asic-world.com/verilog/art_testbench_writing2.html
// https://www.xilinx.com/support/documentation/sw_manuals/xilinx2016_1/ug937-vivado-design-suite-simulation-tutorial.pdf
//
//////////////////////////////////////////////////////////////////////////////////


module tb_master_spi_dac_AD5754__range_con;
reg clk; // assume 10MHz or 100ns
reg reset_n;
reg en;
//
reg clk_bus; //$$ 9.92ns for USB3.0	
//
reg init;
reg update;
reg test;
reg [31:0] test_sdi_pdata;
wire [31:0] test_sdo_pdata;
//
reg [31:0] r_sdo;
wire sdo = r_sdo[31];
//
wire w_test_done;

// DUT
wire sclk;
//$$master_spi_dac_AD5754 master_spi_dac_AD5754_inst(
master_spi_dac_AD5754__range_con  master_spi_dac_AD5754_inst(
	.clk(clk), // assume 10MHz or 100ns
	.reset_n(reset_n),
	.en(en),
	//
	//.conf(16'h0000), 
	//.conf(16'h000B), //$$ force not to use clear and load
	//.conf(16'h00_0B), //$$ all, RNG_SEL_S5_0000
	//.conf(16'h55_0B), //$$ all, RNG_SEL_S10_0001
	//.conf(16'hAA_0B), //$$ all, RNG_SEL_D5_0003
	.conf(16'hFF_0B), //$$ all, RNG_SEL_D10_0004
	//
	.init(init),
	.update(update),
	.test(test),
	//
	.test_sdi_pdata(test_sdi_pdata),
	.test_sdo_pdata(test_sdo_pdata),
	.readback_pdata(),
	// DAC input
	.DAC_VALUE_IN1(16'h00C8),
	.DAC_VALUE_IN2(16'h00AC),
	.DAC_VALUE_IN3(16'h5555),
	.DAC_VALUE_IN4(16'hAAAA),
	// DAC readback
	.DAC_READBACK1(),
	.DAC_READBACK2(),
	.DAC_READBACK3(),
	.DAC_READBACK4(),
	// DAC control
	.SCLK(sclk),
	.SYNC_N(),
	.DIN(),
	.SDO(sdo),
	//
	.init_done  (),
	.update_done(),
	.test_done  (w_test_done),    
	.error(),
	.debug_out()
);

initial begin
#0		clk = 1'b0;
		reset_n = 1'b0;
		en = 1'b0;
		clk_bus = 1'b0;
		init = 1'b0;
		update = 1'b0;
		test = 1'b0;
		test_sdi_pdata = 32'b1010_0101_1001_0110_1100_1010_0011_0101;
#200	reset_n = 1'b1;
#200	en = 1'b1;
#200	test = 1'b1;
#15_000	init = 1'b1;
		test = 1'b0;
#30_000	update = 1'b1;
		test = 1'b1;
@(posedge w_test_done)
#500	$finish; 		
end

always
#50 	clk = ~clk; // toggle every 50ns --> clock 100ns 

always
#4.96 	clk_bus = ~clk_bus; // toggle every 4.96ns --> clock 9.92ns for USB3.0	 

always @(posedge sclk, negedge reset_n)
    if (!reset_n) begin
        r_sdo <= 32'b1010_0101_1001_0110_1100_1010_0011_0101;
        end 
    else begin
        r_sdo <= {r_sdo[30:0], r_sdo[31]}; 
        end

//initial begin
	//$dumpfile ("waveform.vcd"); 
	//$dumpvars; 
//end 
  
//initial  begin
	//$display("\t\t time,\t clk,\t reset_n,\t en"); 
	//$monitor("%d,\t%b,\t%b,\t%b,\t%d",$time,clk,reset_n,en); 
//end 
  
//initial 
	//#200_000 $finish; // 200us = 200_000 ns
	//#1000 $finish; // 1us = 1000 ns
	//#1000_000 $finish; // 1ms = 1000_000 ns
	
endmodule
