//------------------------------------------------------------------------
// master_spi_dac_AD5754__range_con.v
//  for AD5754 Quad, 16-Bit, Serial Input, Unipolar/Bipolar Voltage Output DAC
//  	http://www.analog.com/media/en/technical-documentation/data-sheets/AD5724_5734_5754.pdf
//
//  update DAC value from port
//  readback DAC value to port
//  range control for each port
//  
//  - IO
//  	GPO2_ISO CLR_DAC_B
//  	GPO3_ISO B2C_DAC
//  	GPO4_ISO LOAD_DAC_B 
//  	BIAS_SYNCn_ISO SYNC_B
//  	BIAS_SCLK_ISO SCLK
//  	BIAS_SDI_ISO SDIN
//  	BIAS_SDO_ISO SDO
//
//  - reg 
//  	CON = {conf[15:0], 8'b0, test_mode[3:0], test,update,init,en}
//  	FLAG = {... , test_done,update_done,init_done,en}
//  	DATAIN
//  	DATAOUT
//
//  - serial data format or input shift register
//  	DB23= R/W_B
//  	DB22= 0
//  	DB[21:19]= REG[2:0]
//  	DB[18:16]= A[2:0]
//  	DB[15:0]= D[15:0]
//
//  - timing 
//  	SCLK 33ns min 1/33ns > 30MHz
//  	base clock: clk_out4_125M vs clk_out3_10M
//  		100ns base timing
//  	DAC settling time 10us ~ 100kHz
//  	...
//  	<write mode>
//  	        555544444444443333333333222222222211111111110000000000
//  	        321098765432109876543210987654321098765432109876543210
//  	SCLK    ---_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_----
//  	SYNCB   --________________________________________________----
//  	SDIN    XXDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBXXXX 
//  	          2 2 2 2 1 1 1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 0 0 0
//  	          3 2 1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0
//  	LDACB   ----------------------------------------------------_-
//  	CLRB    -_----------------------------------------------------
//  	...
//  	54 ticks
//  		100ns * 54 = 5.4us
//  	CLRB anytime for 1 tick.
//  	....
//  	<readback mode>
//  	        555544444444443333333333222222222211111111110000000000
//  	        321098765432109876543210987654321098765432109876543210
//  	SCLK    ---_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_----
//  	SYNCB   --________________________________________________----
//  	SDIN    XXDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBXXXX 
//  	          2 2 2 2 1 1 1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 0 0 0
//  	          3 2 1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0
//  	SDO     XXDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBXXXX 
//  	          2 2 2 2 1 1 1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 0 0 0
//  	          3 2 1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0
//  			  
//------------------------------------------------------------------------

`timescale 1ns / 1ps
module master_spi_dac_AD5754__range_con (  
	// 
	input wire clk, // assume 10MHz or 100ns
	input wire reset_n,
	input wire en,
	//
	input wire [15:0] conf, // configuration bits
	// control
	input wire init, // rising
	input wire update, // rising
	input wire test, // rising
	// test
	input wire [31:0] test_sdi_pdata, 
	output wire [31:0] test_sdo_pdata, 
	output wire [31:0] readback_pdata,
	output wire [31:0] readback1_pdata, 
	//$$output wire [31:0] readback2_pdata, 
	// DAC input
	input wire [15:0] DAC_VALUE_IN1,
	input wire [15:0] DAC_VALUE_IN2,
	input wire [15:0] DAC_VALUE_IN3,
	input wire [15:0] DAC_VALUE_IN4,
	// DAC readback
	output wire [15:0] DAC_READBACK1,
	output wire [15:0] DAC_READBACK2,
	output wire [15:0] DAC_READBACK3,
	output wire [15:0] DAC_READBACK4,
	// DAC control
	(* keep = "true" *) output wire SCLK,
	(* keep = "true" *) output wire SYNC_N,
	(* keep = "true" *) output wire DIN,
	(* keep = "true" *) input wire SDO,
	// GPIO
	output wire CLR_DAC_N,
	output wire LOAD_DAC_N,
	output wire B2C_DAC,
	// flag
	output wire init_done,
	output wire update_done,
	output wire test_done,
	output wire error,
	output wire [7:0] debug_out
);
//
(* keep = "true" *) reg [15:0] r_DAC_READBACK1; // 16 bit 
(* keep = "true" *) reg [15:0] r_DAC_READBACK2; // 16 bit 
(* keep = "true" *) reg [15:0] r_DAC_READBACK3; // 16 bit 
(* keep = "true" *) reg [15:0] r_DAC_READBACK4; // 16 bit 

(* keep = "true" *) reg [31:0] r_sdi_pdata; // 32 bit 
(* keep = "true" *) reg [31:0] r_sdo_pdata; // 32 bit 
(* keep = "true" *) reg [31:0] r_readback_pdata; // 32 bit  // WR_PACKET_GEN and ANY_PACKET_GEN
(* keep = "true" *) reg [31:0] r_readback1_pdata; // 32 bit // RD_PACKET_GEN
//$$(* keep = "true" *) reg [31:0] r_readback2_pdata; // 32 bit // TEST_PACKET_DONE
//

// readback registers
(* keep = "true" *) reg [31:0] r_range_pdata;
(* keep = "true" *) reg [31:0] r_power_pdata;
(* keep = "true" *) reg [31:0] r_control_pdata;


////
assign DIN = r_sdi_pdata[31];
//
assign readback_pdata = r_readback_pdata;
//
assign readback1_pdata = r_readback1_pdata; 
//$$assign readback2_pdata = r_readback2_pdata; 

//
assign DAC_READBACK1 = r_DAC_READBACK1;
assign DAC_READBACK2 = r_DAC_READBACK2;
assign DAC_READBACK3 = r_DAC_READBACK3;
assign DAC_READBACK4 = r_DAC_READBACK4;


//
// input wire [15:0] conf, // configuration bits
//   conf[15:0] = {
//   	D_range_opt[1:0], C_range_opt[1:0], B_range_opt[1:0], A_range_opt[1:0],
//   	4'b0, FORCE, B2C_DAC, LOAD_DAC_N, CLR_DAC_N}
//
reg r_CLR_DAC_N;
reg r_LOAD_DAC_N;
//
assign CLR_DAC_N 	= (conf[3])? conf[0] : r_CLR_DAC_N;
assign LOAD_DAC_N 	= (conf[3])? conf[1] : r_LOAD_DAC_N;
assign B2C_DAC 		=  conf[2];
//
wire [1:0] w_D_range_opt = conf[15:14];
wire [1:0] w_C_range_opt = conf[13:12];
wire [1:0] w_B_range_opt = conf[11:10];
wire [1:0] w_A_range_opt = conf[9 :8 ];



(* keep = "true" *) reg [7:0] r_cnt_packet;
// AD5754 Quad, 16-Bit voltage DAc
// <timing>
//  	        555544444444443333333333222222222211111111110000000000
//  	        321098765432109876543210987654321098765432109876543210
//  	SCLK    ---_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_----
//  	SYNCB   --________________________________________________----
//  	SDIN    XXDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBXXXX 
//  	          2 2 2 2 1 1 1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 0 0 0
//  	          3 2 1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0
//      term    a b                                               c
//
parameter 	LEN_PACKET	= 8'd54; // packet size based on half clock
//
reg [LEN_PACKET-1:0] r_SCLK;
reg [LEN_PACKET-1:0] r_SYNC_N;
//
assign SCLK = r_SCLK[LEN_PACKET-1];
assign SYNC_N = r_SYNC_N[LEN_PACKET-1];

// function of wave_sclk	
// wave_sclk(2,48,4)	
parameter 	RST_SCLK = 1'b1;
function [LEN_PACKET-1:0] wave_sclk;
input [7:0] a,b,c; // terms
integer ii;
begin
	for(ii=LEN_PACKET-1; ii>LEN_PACKET-1-a; ii=ii-1) begin
		wave_sclk[ii] = RST_SCLK;
	end
	for(ii=LEN_PACKET-1-a; ii>LEN_PACKET-1-a-b; ii=ii-2) begin
		wave_sclk[ii]   = 1'b1;
		wave_sclk[ii-1] = 1'b0;
	end
	for(ii=0; ii<c; ii=ii+1) begin
		wave_sclk[ii] = RST_SCLK;
	end
end
endfunction 

// function of wave_sync_n // bit assignment
// wave_sync_n(2,48,4)	
function [LEN_PACKET-1:0] wave_sync_n;
input [7:0] a,b,c; // terms
integer ii;
begin
	for(ii=LEN_PACKET-1; ii>LEN_PACKET-1-a; ii=ii-1) begin
		wave_sync_n[ii] = 1'b1;
	end
	for(ii=LEN_PACKET-1-a; ii>LEN_PACKET-1-a-b; ii=ii-1) begin
		wave_sync_n[ii]   = 1'b0;
	end
	for(ii=0; ii<c; ii=ii+1) begin
		wave_sync_n[ii] = 1'b1;
	end
end
endfunction 

// function of packet_shift_left
function [LEN_PACKET-1:0] packet_shift_left;
input [LEN_PACKET-1:0] packet_str; 
input data_s_in; // data shift in
begin
	packet_shift_left = {packet_str[LEN_PACKET-2:0], data_s_in};
end
endfunction 


////
(* keep = "true" *) reg [7:0] state; 
(* keep = "true" *) reg [7:0] state_after_packet; 
(* keep = "true" *) reg [7:0] state_parent; 
//
parameter 	CLR_PACKET_SET 		= 8'h40; // make DAC clear packet
parameter 	CLR_PACKET_DONE 	= 8'h41; 
//
parameter 	LOAD_PACKET_SET 	= 8'h50; // make DAC load packet
parameter 	LOAD_PACKET_DONE 	= 8'h51; 
//
parameter 	RNG_PACKET_SET 	= 8'ha0; // make DAC range packet, all DAC
parameter 	RNG_PACKET_READ	= 8'ha1; 
parameter 	RNG_PACKET_DONE	= 8'ha2; 
//
parameter 	RNG_A_PACKET_SET 	= 8'ha3; // make DAC range packet, DAC A
parameter 	RNG_A_PACKET_READ	= 8'ha4; 
parameter 	RNG_A_PACKET_DONE	= 8'ha5; 
//
parameter 	RNG_B_PACKET_SET 	= 8'ha6; // make DAC range packet, DAC B
parameter 	RNG_B_PACKET_READ	= 8'ha7; 
parameter 	RNG_B_PACKET_DONE	= 8'ha8; 
//
parameter 	RNG_C_PACKET_SET 	= 8'ha9; // make DAC range packet, DAC C
parameter 	RNG_C_PACKET_READ	= 8'haa; 
parameter 	RNG_C_PACKET_DONE	= 8'hab; 
//
parameter 	RNG_D_PACKET_SET 	= 8'hac; // make DAC range packet, DAC D
parameter 	RNG_D_PACKET_READ	= 8'had; 
parameter 	RNG_D_PACKET_DONE	= 8'hae; 
//
parameter 	PWR_PACKET_SET 	= 8'hb0; // make DAC power packet
parameter 	PWR_PACKET_READ	= 8'hb1; 
parameter 	PWR_PACKET_DONE	= 8'hb2; 
//
parameter 	CON_PACKET_SET 	= 8'hc0; // make DAC control packet
parameter 	CON_PACKET_READ = 8'hc1; 
parameter 	CON_PACKET_DONE = 8'hc2; 
//
parameter 	DAC_PACKET_SET 	= 8'hd0; // make DAC packet, all DAC
parameter 	DAC_PACKET_READ	= 8'hd1; 
parameter 	DAC_PACKET_DONE	= 8'hd2; 
parameter 	DAC_A_PACKET_SET 	= 8'hd3; // make DAC A packet
parameter 	DAC_A_PACKET_READ	= 8'hd4; 
parameter 	DAC_A_PACKET_DONE	= 8'hd5; 
parameter 	DAC_B_PACKET_SET 	= 8'hd6; // make DAC B packet
parameter 	DAC_B_PACKET_READ	= 8'hd7; 
parameter 	DAC_B_PACKET_DONE	= 8'hd8; 
parameter 	DAC_C_PACKET_SET 	= 8'hd9; // make DAC C packet
parameter 	DAC_C_PACKET_READ	= 8'hda; 
parameter 	DAC_C_PACKET_DONE	= 8'hdb; 
parameter 	DAC_D_PACKET_SET 	= 8'hdc; // make DAC D packet
parameter 	DAC_D_PACKET_READ	= 8'hdd; 
parameter 	DAC_D_PACKET_DONE	= 8'hde; 
//
parameter 	TEST_PACKET_SET		= 8'he0; // make test packet
parameter 	TEST_PACKET_READ	= 8'he1; // make test packet
parameter 	TEST_PACKET_DONE	= 8'he2; // make test packet
//
parameter 	READBACK_SET	= 8'hf8; // make readback packet
//
//parameter 	READBACK1_SET	= 8'he7; // make readback1 packet // remove
//parameter 	READBACK1_GEN	= 8'he8; // send readback1 packet // remove
//parameter 	READBACK2_SET	= 8'he9; // make readback2 packet // remove
//parameter 	READBACK2_GEN	= 8'hea; // send readback2 packet // remove
//
parameter 	ANY_PACKET_GEN	= 8'hf9; // send any packet
parameter 	WR_PACKET_GEN	= 8'hfa; // send any packet
parameter 	RD_PACKET_GEN	= 8'hfb; // send any packet
//
parameter 	INIT_STATE 		= 8'hfc; // reset state
//
parameter 	SEQ_START 		= 8'hfd; // start of seq
parameter 	SEQ_END 		= 8'hfe; // end of seq
parameter 	SEQ_END_WAIT	= 8'hff; // end of seq
//

//// AD5754 register info
parameter   PRE_ZERO_8B_0   = 8'b00;
//
parameter   NOP_18       = 8'b00_011_000;
parameter   CLR_1C       = 8'b00_011_100;
parameter   LOAD_1D      = 8'b00_011_101;
//
parameter   CON_19       = 8'b00_011_001;
parameter   CON_TSD_EN_0008		= 16'b0000_0000_0000_1000; // 1 for enble thermal shutdown
parameter   CON_CLAMP_EN_0004	= 16'b0000_0000_0000_0100; // 1 for 20mA clamping
parameter   CON_CLR_SEL_0002	= 16'b0000_0000_0000_0010; // zero for 0V clear
parameter   CON_SDO_DIS_0001	= 16'b0000_0000_0000_0001; // zero for enable SDO
//
parameter   RNG_SEL_08   	= 8'b00_001_000;
parameter   RNG_SEL_A_08   	= 8'b00_001_000;
parameter   RNG_SEL_B_09   	= 8'b00_001_001;
parameter   RNG_SEL_C_0A   	= 8'b00_001_010;
parameter   RNG_SEL_D_0B   	= 8'b00_001_011;
parameter   RNG_SEL_ALL_0C 	= 8'b00_001_100; // all DAC
parameter   RNG_SEL_S5_0000   	= 16'b0000_0000_0000_0000; // 5V
parameter   RNG_SEL_S10_0001   	= 16'b0000_0000_0000_0001; // 10V
parameter   RNG_SEL_S10P8_0002 	= 16'b0000_0000_0000_0010;
parameter   RNG_SEL_D5_0003   	= 16'b0000_0000_0000_0011; // +/-5V
parameter   RNG_SEL_D10_0004  	= 16'b0000_0000_0000_0100; // +/-10V
parameter   RNG_SEL_D10P8_0005	= 16'b0000_0000_0000_0101;
//
parameter   PWR_10       	= 8'b00_010_000;
parameter   PWR_PUA_0001   	= 16'b0000_0000_0000_0001;
parameter   PWR_PUB_0002   	= 16'b0000_0000_0000_0010;
parameter   PWR_PUC_0004   	= 16'b0000_0000_0000_0100;
parameter   PWR_PUD_0008   	= 16'b0000_0000_0000_1000;
parameter   PWR_PU_ALL_000F	= 16'b0000_0000_0000_1111;
parameter   PWR_TSD_0020   	= 16'b0000_0000_0010_0000; // read-only, thermal shutdown alert
parameter   PWR_OCA_0080   	= 16'b0000_0000_1000_0000; // read-only, OC A alert
parameter   PWR_OCB_0100   	= 16'b0000_0001_0000_0000; // read-only, OC B alert
parameter   PWR_OCC_0200   	= 16'b0000_0010_0000_0000; // read-only, OC C alert
parameter   PWR_OCD_0400   	= 16'b0000_0100_0000_0000; // read-only, OC D alert
//
parameter   DAC_SEL_00   		= 8'b00_000_000;
parameter   DAC_SEL_A_00   		= 8'b00_000_000;
parameter   DAC_SEL_B_01   		= 8'b00_000_001;
parameter   DAC_SEL_C_02   		= 8'b00_000_010;
parameter   DAC_SEL_D_03   		= 8'b00_000_011;
parameter   DAC_SEL_ALL_04   	= 8'b00_000_100;
//
parameter   RD_REG_80   		= 8'b10_000_000;
//
parameter   DUM_DATA_A5C3   	= 16'b1010_0101_1100_0011; // dummy data for read packet

// function for range_opt
function [15:0] range_opt;
input [1:0] sel; 
begin
	range_opt = 
		(sel==2'b00)? RNG_SEL_S5_0000 :
		(sel==2'b01)? RNG_SEL_S10_0001 :
		(sel==2'b10)? RNG_SEL_D5_0003 :
		RNG_SEL_D10_0004 ;
end
endfunction 
//

////
// INIT_STATE -> SEQ_START (wait for triggers) -> (options) -> SEQ_END -> SEQ_START ...
//
// initial seq: triggered by r_init_trig
//		send CLR packet
//		send 3 packets for range, power, and control.
//		send more readback
//		...
//		CLR_PACKET_SET // clear DAC
//		ANY_PACKET_GEN
//		//
//		RNG_PACKET_SET // set up range 
//		ANY_PACKET_GEN
//		RNG_PACKET_READ // 
//		ANY_PACKET_GEN
//		RNG_PACKET_DONE // 
//		//
//		CON_PACKET_SET // set up other control 
//		ANY_PACKET_GEN
//		CON_PACKET_READ 
//		ANY_PACKET_GEN
//		CON_PACKET_DONE
//		//
//		PWR_PACKET_SET // set up power configuration 
//		ANY_PACKET_GEN
//		PWR_PACKET_READ
//		ANY_PACKET_GEN
//		PWR_PACKET_DONE
//
// update RDAC seq: triggered by r_update_trig
//		send 4 packets to write DAC_A, DAC_B, DAC_C, and DAC_D.
//		send more readback
//		send LOAD packet
//		...
//		DAC_A_PACKET_SET 
//		ANY_PACKET_GEN
//		DAC_A_PACKET_READ
//		ANY_PACKET_GEN
//		DAC_A_PACKET_DONE
//		//
//		...
//		//
//		LOAD_PACKET_SET // load all DAC
//		ANY_PACKET_GEN
//
// test packet seq: triggered by r_test_trig // read RDAC ... control any...
//		send packets: command and NOP for readback 
//		...
//		TEST_PACKET_SET // 24 data bits
//		ANY_PACKET_GEN
//		TEST_PACKET_READ // readback with NOP
//		ANY_PACKET_GEN
//		TEST_PACKET_DONE // readback with NOP
//		ANY_PACKET_GEN
//		...
//		READBACK_SET // readback with NOP
//		ANY_PACKET_GEN
//
//
assign debug_out = state;


// init packet
reg r_init_trig;
reg r_init_done;
reg [1:0] r_init;
reg r_init_done_smp;

// update packet
reg r_update_trig;
reg r_update_done;
reg [1:0] r_update;
reg r_update_done_smp;
	
// test packet
reg r_test_trig;
reg r_test_done;
reg [1:0] r_test;
reg r_test_done_smp;
//
assign init_done = r_init_done;
assign update_done = r_update_done;
assign test_done = r_test_done;
//
always @(posedge clk, negedge reset_n)
	if (!reset_n) begin
        r_init <= 2'b0;
		r_init_done_smp <= 1'b0;
		//
        r_update <= 2'b0;
		r_update_done_smp <= 1'b0;
		//
        r_test <= 2'b0;
		r_test_done_smp <= 1'b0;
        end
    else if (en) begin
        r_init <= {r_init[0], init};
        r_init_done_smp <= r_init_done;
		//
        r_update <= {r_update[0], update};
        r_update_done_smp <= r_update_done;
		//
        r_test <= {r_test[0], test};
        r_test_done_smp <= r_test_done;
        end 
    else begin
        r_init <= 2'b0;
		r_init_done_smp <= 1'b0;
		//
        r_update <= 2'b0;
		r_update_done_smp <= 1'b0;
		//
        r_test <= 2'b0;
		r_test_done_smp <= 1'b0;
        end
//
always @(posedge clk, negedge reset_n)
	if (!reset_n) begin
        r_init_trig <= 1'b0;
        r_update_trig <= 1'b0;
        r_test_trig <= 1'b0;
        end
    else if (en) begin
		if (!r_init_trig)
			r_init_trig <= ~r_init[1]&r_init[0];
		else if (~r_init_done_smp&r_init_done)
			r_init_trig <= 1'b0;
		//
		if (!r_update_trig)
			r_update_trig <= ~r_update[1]&r_update[0];
		else if (~r_update_done_smp&r_update_done)
			r_update_trig <= 1'b0;
		//
		if (!r_test_trig)
			r_test_trig <= ~r_test[1]&r_test[0];
		else if (~r_test_done_smp&r_test_done)
			r_test_trig <= 1'b0;
        end 
    else begin
        r_init_trig <= 1'b0;
        r_update_trig <= 1'b0;
        r_test_trig <= 1'b0;
        end

// sdo buffer
(* keep = "true" *) reg [31:0] r_test_sdo_pdata;
//$$assign test_sdo_pdata = r_test_sdo_pdata;
// for debug
assign test_sdo_pdata = 
	(test_sdi_pdata[31:24]==8'h00)? r_test_sdo_pdata :
	(test_sdi_pdata[31:24]==8'hAA)? r_range_pdata    :
	(test_sdi_pdata[31:24]==8'hCC)? r_control_pdata  :
	(test_sdi_pdata[31:24]==8'hEE)? r_power_pdata    :
	r_test_sdo_pdata ;

// reset wait counter
//(* keep = "true" *) reg [15:0] r_cnt_reset;
		
		
reg r_error; // DAC update error
assign error = r_error;
//
(* keep = "true" *) reg r_init_busy;		
(* keep = "true" *) reg r_update_busy;		
(* keep = "true" *) reg r_test_busy;		
		
		
// process state 
always @(posedge clk, negedge reset_n)
	if (!reset_n) begin
		state       		<= INIT_STATE;
		state_after_packet	<= INIT_STATE;
		state_parent		<= INIT_STATE;
		//
		r_cnt_packet		<= 8'b0;
		//r_cnt_reset			<= 16'b0;
		//
		r_SCLK      		<= {(LEN_PACKET) {RST_SCLK}}; 
		r_SYNC_N    		<= {(LEN_PACKET) {1'b1}};
        r_sdi_pdata     	<= 32'b0;
		r_sdo_pdata     	<= 32'b0;
		//
		r_readback_pdata	<= 32'b0;
		r_readback1_pdata	<= 32'b0;
		//$$r_readback2_pdata	<= 32'b0;
		r_range_pdata		<= 32'b0;
		r_power_pdata		<= 32'b0;
		r_control_pdata		<= 32'b0;
		//
        r_init_done     	<= 1'b0;
		r_update_done     	<= 1'b0;
		r_test_done     	<= 1'b0;
		//
        r_init_busy     	<= 1'b0;
		r_update_busy     	<= 1'b0;
		r_test_busy     	<= 1'b0;
		//
		r_test_sdo_pdata	<= 32'b0;
		//
		r_DAC_READBACK1   <= 16'b0;
		r_DAC_READBACK2   <= 16'b0;
		r_DAC_READBACK3   <= 16'b0;
		r_DAC_READBACK4   <= 16'b0;
		//
		r_CLR_DAC_N			<=1'b1;
		r_LOAD_DAC_N		<=1'b1;
		//
		r_error				<=1'b0;
		end 
	else case (state)
		// wait for stable state ... may need more times.
		INIT_STATE : begin
			if (en) begin
				state 			<= SEQ_START; // wait for ADC not busy
				state_parent	<= INIT_STATE;
				end
			end
		// start seq
		SEQ_START : begin
			//
			r_error				<=1'b0;
			//
			if (en && r_init_trig) begin
				r_init_done		<= 1'b0;
				r_init_busy 	<= 1'b1;
                state   		<= CLR_PACKET_SET; // wait for en
				state_parent	<= SEQ_START;
				end
			//
			else if (en && r_update_trig) begin
				r_update_done	<= 1'b0;
				r_update_busy 	<= 1'b1;
                state   		<= DAC_A_PACKET_SET; // wait for en
				state_parent	<= SEQ_START;
				end
			//
			else if (en && r_test_trig) begin
				r_test_done		<= 1'b0;
				r_test_busy 	<= 1'b1;
                state   		<= TEST_PACKET_SET; // wait for en
				state_parent	<= SEQ_START;
				end
			end
		//
		// init packet
		CLR_PACKET_SET : begin //$$ Addressing this function sets the DAC registers to the clear code and updates the outputs.
			// control output
			r_CLR_DAC_N			<= 1'b0;
			//
			r_SCLK          	<= wave_sclk(2,48,4);
			r_SYNC_N        	<= wave_sync_n(2,48,4);
			r_sdi_pdata     	<= {CLR_1C,16'hACC8,8'b0}; // 24-bit format for AD5724
            r_sdo_pdata     	<= 32'b0;
			r_cnt_packet    	<= LEN_PACKET;
			state_after_packet	<= CLR_PACKET_DONE;
			state           	<= WR_PACKET_GEN;	
			state_parent		<= CLR_PACKET_SET;			
			end
		CLR_PACKET_DONE : begin
			// control output
			r_CLR_DAC_N			<= 1'b1;
			//state           	<= RNG_PACKET_SET; 
			state           	<= RNG_A_PACKET_SET; //$$
			state_parent		<= CLR_PACKET_DONE;			
			end
		//
		// make DAC range packet, all DAC -- bypass
		RNG_PACKET_SET : begin
			r_SCLK          	<= wave_sclk(2,48,4);
			r_SYNC_N        	<= wave_sync_n(2,48,4);
			r_sdi_pdata     	<= {RNG_SEL_ALL_0C,RNG_SEL_D5_0003,8'b0}; // 24-bit format for AD5724
            r_sdo_pdata     	<= 32'b0;
			r_cnt_packet    	<= LEN_PACKET;
			state_after_packet	<= RNG_PACKET_READ; //		
			state           	<= WR_PACKET_GEN;			
			state_parent		<= RNG_PACKET_SET;	
			end
		RNG_PACKET_READ : begin
			r_SCLK          	<= wave_sclk(2,48,4);
			r_SYNC_N        	<= wave_sync_n(2,48,4);
			r_sdi_pdata     	<= {RD_REG_80|RNG_SEL_ALL_0C,RNG_SEL_D5_0003,8'b0}; // 24-bit format for AD5724
            r_sdo_pdata     	<= 32'b0;
			r_cnt_packet    	<= LEN_PACKET;
			state_after_packet	<= RNG_PACKET_DONE; //			
			state           	<= RD_PACKET_GEN;			
			state_parent		<= RNG_PACKET_READ;
			end
		RNG_PACKET_DONE : begin
			// collect readback
			r_range_pdata		<= r_readback_pdata;
			state           	<= RNG_C_PACKET_SET; //	 		
			state_parent		<= RNG_PACKET_DONE;	
			end
		//
		// make DAC range packet, DAC A
		RNG_A_PACKET_SET : begin
			r_SCLK          	<= wave_sclk(2,48,4);
			r_SYNC_N        	<= wave_sync_n(2,48,4);
			r_sdi_pdata     	<= {RNG_SEL_A_08,range_opt(w_A_range_opt),8'b0}; // 24-bit format for AD5724
			r_sdo_pdata     	<= 32'b0;
			r_cnt_packet    	<= LEN_PACKET;
			state_after_packet	<= RNG_A_PACKET_READ; //		
			state           	<= WR_PACKET_GEN;			
			state_parent		<= RNG_A_PACKET_SET;	
			end
		RNG_A_PACKET_READ : begin
			r_SCLK          	<= wave_sclk(2,48,4);
			r_SYNC_N        	<= wave_sync_n(2,48,4);
			r_sdi_pdata     	<= {RD_REG_80|RNG_SEL_A_08,range_opt(w_A_range_opt),8'b0}; // 24-bit format for AD5724
			r_sdo_pdata     	<= 32'b0;
			r_cnt_packet    	<= LEN_PACKET;
			state_after_packet	<= RNG_A_PACKET_DONE; //			
			state           	<= RD_PACKET_GEN;			
			state_parent		<= RNG_A_PACKET_READ;
			end
		RNG_A_PACKET_DONE : begin
			// collect readback
			r_range_pdata		<= r_readback_pdata;
			state           	<= RNG_B_PACKET_SET; //	 		
			state_parent		<= RNG_A_PACKET_DONE;	
			end
		// make DAC range packet, DAC B
		RNG_B_PACKET_SET : begin
			r_SCLK          	<= wave_sclk(2,48,4);
			r_SYNC_N        	<= wave_sync_n(2,48,4);
			r_sdi_pdata     	<= {RNG_SEL_B_09,range_opt(w_B_range_opt),8'b0}; // 24-bit format for AD5724
			r_sdo_pdata     	<= 32'b0;
			r_cnt_packet    	<= LEN_PACKET;
			state_after_packet	<= RNG_B_PACKET_READ; //		
			state           	<= WR_PACKET_GEN;			
			state_parent		<= RNG_B_PACKET_SET;	
			end
		RNG_B_PACKET_READ : begin
			r_SCLK          	<= wave_sclk(2,48,4);
			r_SYNC_N        	<= wave_sync_n(2,48,4);
			r_sdi_pdata     	<= {RD_REG_80|RNG_SEL_B_09,range_opt(w_B_range_opt),8'b0}; // 24-bit format for AD5724
			r_sdo_pdata     	<= 32'b0;
			r_cnt_packet    	<= LEN_PACKET;
			state_after_packet	<= RNG_B_PACKET_DONE; //			
			state           	<= RD_PACKET_GEN;			
			state_parent		<= RNG_B_PACKET_READ;
			end
		RNG_B_PACKET_DONE : begin
			// collect readback
			r_range_pdata		<= r_readback_pdata;
			state           	<= RNG_C_PACKET_SET; //	 		
			state_parent		<= RNG_B_PACKET_DONE;	
			end
		// make DAC range packet, DAC C
		RNG_C_PACKET_SET : begin
			r_SCLK          	<= wave_sclk(2,48,4);
			r_SYNC_N        	<= wave_sync_n(2,48,4);
			//$$r_sdi_pdata     	<= {RNG_SEL_C_0A,RNG_SEL_S10_0001,8'b0}; // 24-bit format for AD5724
			r_sdi_pdata     	<= {RNG_SEL_C_0A,range_opt(w_C_range_opt),8'b0}; // 24-bit format for AD5724
			r_sdo_pdata     	<= 32'b0;
			r_cnt_packet    	<= LEN_PACKET;
			state_after_packet	<= RNG_C_PACKET_READ; //		
			state           	<= WR_PACKET_GEN;			
			state_parent		<= RNG_C_PACKET_SET;	
			end
		RNG_C_PACKET_READ : begin
			r_SCLK          	<= wave_sclk(2,48,4);
			r_SYNC_N        	<= wave_sync_n(2,48,4);
			//$$r_sdi_pdata     	<= {RD_REG_80|RNG_SEL_C_0A,RNG_SEL_S10_0001,8'b0}; // 24-bit format for AD5724
			r_sdi_pdata     	<= {RD_REG_80|RNG_SEL_C_0A,range_opt(w_C_range_opt),8'b0}; // 24-bit format for AD5724
			r_sdo_pdata     	<= 32'b0;
			r_cnt_packet    	<= LEN_PACKET;
			state_after_packet	<= RNG_C_PACKET_DONE; //			
			state           	<= RD_PACKET_GEN;			
			state_parent		<= RNG_C_PACKET_READ;
			end
		RNG_C_PACKET_DONE : begin
			// collect readback
			r_range_pdata		<= r_readback_pdata;
			state           	<= RNG_D_PACKET_SET; //	 		
			state_parent		<= RNG_C_PACKET_DONE;	
			end
		// make DAC range packet, DAC D
		RNG_D_PACKET_SET : begin
			r_SCLK          	<= wave_sclk(2,48,4);
			r_SYNC_N        	<= wave_sync_n(2,48,4);
			r_sdi_pdata     	<= {RNG_SEL_D_0B,range_opt(w_D_range_opt),8'b0}; // 24-bit format for AD5724
			r_sdo_pdata     	<= 32'b0;
			r_cnt_packet    	<= LEN_PACKET;
			state_after_packet	<= RNG_D_PACKET_READ; //		
			state           	<= WR_PACKET_GEN;			
			state_parent		<= RNG_D_PACKET_SET;	
			end
		RNG_D_PACKET_READ : begin
			r_SCLK          	<= wave_sclk(2,48,4);
			r_SYNC_N        	<= wave_sync_n(2,48,4);
			r_sdi_pdata     	<= {RD_REG_80|RNG_SEL_D_0B,range_opt(w_D_range_opt),8'b0}; // 24-bit format for AD5724
			r_sdo_pdata     	<= 32'b0;
			r_cnt_packet    	<= LEN_PACKET;
			state_after_packet	<= RNG_D_PACKET_DONE; //			
			state           	<= RD_PACKET_GEN;			
			state_parent		<= RNG_D_PACKET_READ;
			end
		RNG_D_PACKET_DONE : begin
			// collect readback
			r_range_pdata		<= r_readback_pdata;
			state           	<= CON_PACKET_SET; //	 		
			state_parent		<= RNG_D_PACKET_DONE;	
			end
		//
		CON_PACKET_SET : begin
			r_SCLK          	<= wave_sclk(2,48,4);
			r_SYNC_N        	<= wave_sync_n(2,48,4);
			r_sdi_pdata     	<= {CON_19,CON_TSD_EN_0008|CON_CLAMP_EN_0004,8'b0}; // 24-bit format for AD5724
			r_sdo_pdata     	<= 32'b0;
			r_cnt_packet    	<= LEN_PACKET;
			state_after_packet	<= CON_PACKET_READ;			
			state           	<= WR_PACKET_GEN;			
			state_parent		<= CON_PACKET_SET;			
			end
		CON_PACKET_READ : begin
			r_SCLK          	<= wave_sclk(2,48,4);
			r_SYNC_N        	<= wave_sync_n(2,48,4);
			r_sdi_pdata     	<= {RD_REG_80|CON_19,CON_TSD_EN_0008|CON_CLAMP_EN_0004,8'b0}; // 24-bit format for AD5724
            r_sdo_pdata     	<= 32'b0;
			r_cnt_packet    	<= LEN_PACKET;
			state_after_packet	<= CON_PACKET_DONE;			
			state           	<= RD_PACKET_GEN;			
			state_parent		<= CON_PACKET_READ;			
			end
		CON_PACKET_DONE : begin
			// collect readback
			r_control_pdata		<= r_readback_pdata;
			state           	<= PWR_PACKET_SET;			
			state_parent		<= CON_PACKET_DONE;			
			end
		//
		PWR_PACKET_SET : begin
			r_SCLK          	<= wave_sclk(2,48,4);
			r_SYNC_N        	<= wave_sync_n(2,48,4);
			r_sdi_pdata     	<= {PWR_10,PWR_PU_ALL_000F,8'b0}; // 24-bit format for AD5724
            r_sdo_pdata     	<= 32'b0;
			r_cnt_packet    	<= LEN_PACKET;
			state_after_packet	<= PWR_PACKET_READ; 			
			state           	<= WR_PACKET_GEN;			
			state_parent		<= PWR_PACKET_SET;			
			end
		PWR_PACKET_READ : begin
			r_SCLK          	<= wave_sclk(2,48,4);
			r_SYNC_N        	<= wave_sync_n(2,48,4);
			r_sdi_pdata     	<= {RD_REG_80|PWR_10,PWR_PU_ALL_000F,8'b0}; // 24-bit format for AD5724
            r_sdo_pdata     	<= 32'b0;
			r_cnt_packet    	<= LEN_PACKET;
			state_after_packet	<= PWR_PACKET_DONE; 			
			state           	<= RD_PACKET_GEN;			
			state_parent		<= PWR_PACKET_READ;			
			end
		PWR_PACKET_DONE : begin
			// collect readback
			r_power_pdata		<= r_readback_pdata;
			state           	<= SEQ_END;			
			state_parent		<= PWR_PACKET_DONE;			
			end
		//
		// update packet
		DAC_A_PACKET_SET : begin
			r_SCLK          	<= wave_sclk(2,48,4);
			r_SYNC_N        	<= wave_sync_n(2,48,4);
			r_sdi_pdata     	<= {DAC_SEL_A_00,DAC_VALUE_IN1,8'b0}; // 24-bit format for AD5724
            r_sdo_pdata     	<= 32'b0;
			r_cnt_packet    	<= LEN_PACKET;
			state_after_packet	<= DAC_A_PACKET_READ; 			
			state           	<= WR_PACKET_GEN;			
			state_parent		<= DAC_A_PACKET_SET;			
			end
		DAC_A_PACKET_READ : begin
			r_SCLK          	<= wave_sclk(2,48,4);
			r_SYNC_N        	<= wave_sync_n(2,48,4);
			r_sdi_pdata     	<= {RD_REG_80|DAC_SEL_A_00,DUM_DATA_A5C3,8'b0}; // 24-bit format for AD5724 
            r_sdo_pdata     	<= 32'b0;
			r_cnt_packet    	<= LEN_PACKET;
			state_after_packet	<= DAC_A_PACKET_DONE; 			
			state           	<= RD_PACKET_GEN;	
			state_parent		<= DAC_A_PACKET_READ;
			end
		DAC_A_PACKET_DONE : begin
			// collect readback
			r_DAC_READBACK1		<= r_readback_pdata[15:0]; //$$
			state           	<= DAC_B_PACKET_SET;			
			state_parent		<= DAC_A_PACKET_DONE;
			end
		//
		DAC_B_PACKET_SET : begin
			r_SCLK          	<= wave_sclk(2,48,4);
			r_SYNC_N        	<= wave_sync_n(2,48,4);
			r_sdi_pdata     	<= {DAC_SEL_B_01,DAC_VALUE_IN2,8'b0}; // 24-bit format for AD5724
            r_sdo_pdata     	<= 32'b0;
			r_cnt_packet    	<= LEN_PACKET;
			state_after_packet	<= DAC_B_PACKET_READ; 			
			state           	<= WR_PACKET_GEN;			
			state_parent		<= DAC_B_PACKET_SET;
			end
		DAC_B_PACKET_READ : begin
			r_SCLK          	<= wave_sclk(2,48,4);
			r_SYNC_N        	<= wave_sync_n(2,48,4);
			r_sdi_pdata     	<= {RD_REG_80|DAC_SEL_B_01,DUM_DATA_A5C3,8'b0}; // 24-bit format for AD5724
            r_sdo_pdata     	<= 32'b0;
			r_cnt_packet    	<= LEN_PACKET;
			state_after_packet	<= DAC_B_PACKET_DONE; 			
			state           	<= RD_PACKET_GEN;			
			state_parent		<= DAC_B_PACKET_READ;
			end
		DAC_B_PACKET_DONE : begin
			// collect readback
			r_DAC_READBACK2		<= r_readback_pdata[15:0]; //$$
			state           	<= DAC_C_PACKET_SET;			
			state_parent		<= DAC_B_PACKET_DONE;
			end
		//
		DAC_C_PACKET_SET : begin
			r_SCLK          	<= wave_sclk(2,48,4);
			r_SYNC_N        	<= wave_sync_n(2,48,4);
			r_sdi_pdata     	<= {DAC_SEL_C_02,DAC_VALUE_IN3,8'b0}; // 24-bit format for AD5724
            r_sdo_pdata     	<= 32'b0;
			r_cnt_packet    	<= LEN_PACKET;
			state_after_packet	<= DAC_C_PACKET_READ; 			
			state           	<= WR_PACKET_GEN;	
			state_parent		<= DAC_C_PACKET_SET;
			end
		DAC_C_PACKET_READ : begin
			r_SCLK          	<= wave_sclk(2,48,4);
			r_SYNC_N        	<= wave_sync_n(2,48,4);
			r_sdi_pdata     	<= {RD_REG_80|DAC_SEL_C_02,DUM_DATA_A5C3,8'b0}; // 24-bit format for AD5724
            r_sdo_pdata     	<= 32'b0;
			r_cnt_packet    	<= LEN_PACKET;
			state_after_packet	<= DAC_C_PACKET_DONE; 			
			state           	<= RD_PACKET_GEN;			
			state_parent		<= DAC_C_PACKET_READ;
			end
		DAC_C_PACKET_DONE : begin
			// collect readback
			r_DAC_READBACK3		<= r_readback_pdata[15:0]; //$$
			state           	<= DAC_D_PACKET_SET;			
			state_parent		<= DAC_C_PACKET_DONE;
			end
		//
		DAC_D_PACKET_SET : begin
			r_SCLK          	<= wave_sclk(2,48,4);
			r_SYNC_N        	<= wave_sync_n(2,48,4);
			r_sdi_pdata     	<= {DAC_SEL_D_03,DAC_VALUE_IN4,8'b0}; // 24-bit format for AD5724
            r_sdo_pdata     	<= 32'b0;
			r_cnt_packet    	<= LEN_PACKET;
			state_after_packet	<= DAC_D_PACKET_READ; 			
			state           	<= WR_PACKET_GEN;			
			state_parent		<= DAC_D_PACKET_SET;
			end
		DAC_D_PACKET_READ : begin
			r_SCLK          	<= wave_sclk(2,48,4);
			r_SYNC_N        	<= wave_sync_n(2,48,4);
			r_sdi_pdata     	<= {RD_REG_80|DAC_SEL_D_03,DUM_DATA_A5C3,8'b0}; // 24-bit format for AD5724
            r_sdo_pdata     	<= 32'b0;
			r_cnt_packet    	<= LEN_PACKET;
			state_after_packet	<= DAC_D_PACKET_DONE; 			
			state           	<= RD_PACKET_GEN;			
			state_parent		<= DAC_D_PACKET_READ;
			end
		DAC_D_PACKET_DONE : begin
			// collect readback
			r_DAC_READBACK4		<= r_readback_pdata[15:0]; //$$
			state           	<= LOAD_PACKET_SET;			
			state_parent		<= DAC_D_PACKET_DONE;
			end
		//
		LOAD_PACKET_SET : begin //$$ Addressing this function updates the DAC registers and, consequently, the DAC outputs.
			// control output
			r_LOAD_DAC_N		<=1'b0;
			//
			r_SCLK          	<= wave_sclk(2,48,4);
			r_SYNC_N        	<= wave_sync_n(2,48,4);
			r_sdi_pdata     	<= {LOAD_1D,16'h55AA,8'b0}; // 24-bit format for AD5724
            r_sdo_pdata     	<= 32'b0;
			r_cnt_packet    	<= LEN_PACKET;
			state_after_packet	<= LOAD_PACKET_DONE;			
			state           	<= WR_PACKET_GEN;			
			state_parent		<= LOAD_PACKET_SET;
			end
		LOAD_PACKET_DONE : begin
			// control output
			r_LOAD_DAC_N		<=1'b1;
			state           	<= SEQ_END;			
			state_parent		<= LOAD_PACKET_DONE;
			end
		//
		// test packet
		TEST_PACKET_SET : begin
			r_SCLK          	<= wave_sclk(2,48,4);
			r_SYNC_N        	<= wave_sync_n(2,48,4);
			r_sdi_pdata     	<= {test_sdi_pdata[23:0],8'b0}; // 24-bit format for AD5724
            r_sdo_pdata     	<= 32'b0;
			r_cnt_packet    	<= LEN_PACKET;
			state_after_packet	<= TEST_PACKET_READ;			
			state           	<= WR_PACKET_GEN;			
			state_parent		<= TEST_PACKET_SET;
			end
		TEST_PACKET_READ : begin
			r_SCLK          	<= wave_sclk(2,48,4);
			r_SYNC_N        	<= wave_sync_n(2,48,4);
			r_sdi_pdata     	<= {NOP_18,16'hA97F,8'b0}; // NOP + dummy data + zeros
            r_sdo_pdata     	<= 32'b0;
			r_cnt_packet    	<= LEN_PACKET;
			state_after_packet	<= TEST_PACKET_DONE;			
			state           	<= RD_PACKET_GEN;			
			state_parent		<= TEST_PACKET_READ;
			end
		TEST_PACKET_DONE : begin
			// collect readback
			//$$r_readback2_pdata	<= r_test_sdo_pdata;
			r_test_sdo_pdata <= r_readback1_pdata; // data after WR packet
			//$$r_test_sdo_pdata <= r_readback_pdata;  // data after RD packet
			//
			state           	<= SEQ_END; // without packet
			state_parent		<= TEST_PACKET_DONE;
			end
		//
		//
		// send write packet 
		WR_PACKET_GEN : if (r_cnt_packet == 0) begin // packet sent all.
			r_readback_pdata	<= r_sdo_pdata; // collect r_readback_pdata
			//$$ if (r_test_busy) begin // test packet was sent!
			//$$ 	r_test_sdo_pdata	<= r_sdo_pdata; // last packet in the test busy
			//$$ 	end
			state       <= state_after_packet; // set state to return
			end else begin
			r_SCLK      <= packet_shift_left(r_SCLK, RST_SCLK); // move towards MSB 
			r_SYNC_N    <= packet_shift_left(r_SYNC_N, 1'b1); // move towards MSB 
			//
			if (SYNC_N==0&&SCLK==0) 
                r_sdi_pdata <= {r_sdi_pdata[30:0], 1'b0}; // send r_sdi_pdata
			if (SYNC_N==0&&SCLK==1) 
                r_sdo_pdata <= {r_sdo_pdata[30:0], SDO}; // collect r_sdo_pdata
			//
			r_cnt_packet <= r_cnt_packet - 1;            
			end
		//
		// send read packet 
		RD_PACKET_GEN : if (r_cnt_packet == 0) begin // packet sent all.
			r_readback1_pdata	<= r_sdo_pdata; // collect r_readback_pdata
			//$$ if (r_test_busy) begin // test packet was sent!
			//$$ 	r_test_sdo_pdata	<= r_sdo_pdata; // last packet in the test busy
			//$$ 	end
			state       <= READBACK_SET; 
			end else begin
			r_SCLK      <= packet_shift_left(r_SCLK, RST_SCLK); // move towards MSB 
			r_SYNC_N    <= packet_shift_left(r_SYNC_N, 1'b1); // move towards MSB 
			//
			if (SYNC_N==0&&SCLK==0) 
                r_sdi_pdata <= {r_sdi_pdata[30:0], 1'b0}; // send r_sdi_pdata
			if (SYNC_N==0&&SCLK==1) 
                r_sdo_pdata <= {r_sdo_pdata[30:0], SDO}; // collect r_sdo_pdata
			//
			r_cnt_packet <= r_cnt_packet - 1;            
			end
		// readback with NOP
		READBACK_SET : begin
			r_SCLK          	<= wave_sclk(2,48,4);
			r_SYNC_N        	<= wave_sync_n(2,48,4);
			r_sdi_pdata     	<= {NOP_18,16'hA97F,8'b0}; // NOP + dummy data + zeros
			r_sdo_pdata     	<= 32'b0;
			r_cnt_packet    	<= LEN_PACKET;
			state_after_packet	<= state_after_packet;			
			state           	<= ANY_PACKET_GEN;						
			end
		// send any packet 
		ANY_PACKET_GEN : if (r_cnt_packet == 0) begin // packet sent all.
			r_readback_pdata	<= r_sdo_pdata; // collect r_readback_pdata
			//$$ if (r_test_busy) begin // test packet was sent!
			//$$ 	r_test_sdo_pdata	<= r_sdo_pdata; // last packet in the test busy
			//$$ 	end
			state       <= state_after_packet; // set state to return
			end else begin
			r_SCLK      <= packet_shift_left(r_SCLK, RST_SCLK); // move towards MSB 
			r_SYNC_N    <= packet_shift_left(r_SYNC_N, 1'b1); // move towards MSB 
			//
			if (SYNC_N==0&&SCLK==0) 
                r_sdi_pdata <= {r_sdi_pdata[30:0], 1'b0}; // send r_sdi_pdata
			if (SYNC_N==0&&SCLK==1) 
                r_sdo_pdata <= {r_sdo_pdata[30:0], SDO}; // collect r_sdo_pdata
			//
			r_cnt_packet <= r_cnt_packet - 1;            
			end
		// end of seq
		SEQ_END : begin
			// check done 
			if (r_init_busy) begin // init packet was sent!
				r_init_done		<= 1'b1;
				r_init_busy		<= 1'b0;
				end
			if (r_update_busy) begin // update packet was sent!
				r_update_done	<= 1'b1;
				r_update_busy	<= 1'b0;
				end
			if (r_test_busy) begin // test packet was sent!
				r_test_done		<= 1'b1;
				r_test_busy		<= 1'b0;
				end
			//
			state_after_packet	<= INIT_STATE;
			state 				<= SEQ_END_WAIT; // return to start of seq
			state_parent		<= SEQ_END;
			end
		SEQ_END_WAIT : begin // wait for clearing the previous trigger.
			state 				<= SEQ_START; // return to start of seq
			state_parent		<= SEQ_END_WAIT;
			end
		default  : begin
			state 			<= INIT_STATE;
			state_parent	<= INIT_STATE;			
			r_error				<=1'b1;
			end
	endcase
 
	
endmodule
