//------------------------------------------------------------------------
// control_adc_ddr_LTC2325.v
//   control for LTC2325-16 ADC DDR 
//   	http://www.analog.com/media/en/technical-documentation/data-sheets/232516fa.pdf
//
//
//   === timing (rough) ===
//   conversion_bar signal    : o_CNV_B
//   serial clock input       : o_SCK
//   serial clock output      : i_DCO
//   serial data  output[3:0] : i_SDO[3:0]
//   ----
//   index_H             0000000000111111111122222222223333333333444444444455555555556666666666777777777
//   index_L             0123456789012345678901234567890123456789012345678901234567890123456789012345678
//   r_idx_cnv_H          000000000011111111112222222222333333333344444444445555555555666666666677777770
//   r_idx_cnv_L          012345678901234567890123456789012345678901234567890123456789012345678901234560
// ----
//   r_idx_frame_H       000000000011111111112222222222333333333344444444445555555555666666666677777770
//   r_idx_frame_L       012345678901234567890123456789012345678901234567890123456789012345678901234560
//   r_trig_conv_run  ___-______________________________________________________________________________________
//   o_CNV_B          ____-------______________________________________________________________________-------__
//   r_conv_busy      ___________------------------------------------_________________________________________--
//                               <-- conversion.................. -->
//   o_SCK            _________________________________________________--__--__--__--__--__--__--__--___________ 
//   i_DCO            __________________________________________________--__--__--__--__--__--__--__--__________
//   i_SDO[3:0]       ___________xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxDDddDDddDDddDDddDDddDDddDDddDDddxx________
//   r_sdo_valid      ________________________________________________--------------------------------__________
//
//   note: 77 b_clk for most fast cycle ... b_clk*77=366.666667ns or 2.727Msps
//         for 5Msps or 200ns period, 170ns + 30ns = 200ns... 30ns // b_clk*42=200ns 
//         r_conv_busy is not secured...
//         for safe operation, 
//             b_clk* 84	= 400		ns or 2.5   Msps (max)
//             b_clk*105	= 500		ns or 2.0   Msps 
//             b_clk*210	= 1000		ns or 1.0   Msps
//             b_clk*420	= 2000		ns or 0.5   Msps
//             b_clk*2100	= 10000		ns or 0.1   Msps
//             b_clk*4200	= 20000		ns or 0.05  Msps
//             b_clk*21000	= 100000	ns or 0.01  Msps // 100us
//             b_clk*42000	= 200000	ns or 0.005 Msps
//             b_clk*210000	= 1000000	ns or 0.001 Msps (min) // log_2(210000)=17.68
//
//   note: 10ns based control looks available. 100MHz vs 104MHz ... 10ns vs 9.62ns
//


`timescale 1ns / 1ps


module signal_ext_bclk_pclk ( 
	input  wire b_clk  , // assume 192MHz
	input  wire reset_n,
	input  wire p_clk  , // assume 12MHz
	
	input  wire i_sig_b, // sig based on b_clk            // ___-_______
	output wire o_sig_b, // sig based on b_clk and p_clk  // ___-----___ 
	output wire o_sig_p  // sig based on p_clk            // _____---___
);
//{
reg r_sig_p;
reg r_sig_b;  
//
always @(posedge b_clk, negedge reset_n)
	if (!reset_n) begin
		r_sig_b       <= 1'b0;
		end 
	else begin 
		// update:
		//  r_sig_b+          r_sig_b          i_sig_b              r_sig_p
		//  0                 0                0                    0
		//  0                 0                0                    1
		//  1                 0                1                    0
		//  1                 0                1                    1
		//  1                 1                0                    0 <<<
		//  0                 1                0                    1
		//  1                 1                1                    0
		//  1                 1                1                    1
		r_sig_b       <= i_sig_b            |
		                (r_sig_b & ~r_sig_p); 
		end
//
always @(posedge p_clk, negedge reset_n)
	if (!reset_n) begin
		r_sig_p       <= 1'b0;
		end 
	else begin
		r_sig_p       <= r_sig_b;
		end
//
assign  o_sig_b = r_sig_b;
assign  o_sig_p = r_sig_p;
//}
endmodule

// TODO: control_adc_ddr_LTC2325_sdo_recv_core
module control_adc_ddr_LTC2325_sdo_recv_core (  // need to add test data out increasing pattern
	// 
	input  wire b_clk  , // assume 192MHz
	input  wire reset_n,
	input  wire p_clk  , // assume 12MHz
		
	input  wire         i_DCO ,
	input  wire [3:0]   i_SDO ,
	
	output wire [15:0]  o_p_data_A, // from i_ADC0_SDO[0]
	output wire [15:0]  o_p_data_B, // from i_ADC0_SDO[1]
	output wire [15:0]  o_p_data_C, // from i_ADC0_SDO[2]
	output wire [15:0]  o_p_data_D, // from i_ADC0_SDO[3]
	output wire         o_p_data_rd,
	//
	output wire [15:0]  o_p_data_A_pclk , // from i_ADC0_SDO[0]
	output wire [15:0]  o_p_data_B_pclk , // from i_ADC0_SDO[1]
	output wire [15:0]  o_p_data_C_pclk , // from i_ADC0_SDO[2]
	output wire [15:0]  o_p_data_D_pclk , // from i_ADC0_SDO[3]
	//
	output wire         o_p_data_rd_pclk, // fifo read control on pclk
	output wire         o_trig_conv_pclk, // conv monitor on pclk
	output wire [15:0]  o_cnt_p_data_pclk, // count p_data on pclk
	output wire [15:0]  o_cnt_trig_conv_pclk, // count trig_conv on pclk
	
	input  wire         i_test_mode_en, // enable test pattern mode
	input  wire         i_test_mode_hs, // enable test pattern mode high speed

	input  wire         i_clear_cnt_SDO_bits, // clear SDO bits // or trigger to increase test pattern
	
	input  wire         i_trig_conv // to skip the first conversion pulse. // or trigger to reset test pattern 
);
//{

//// input sampling //{
reg [1:0] r_DCO;
reg [1:0] r_SDO_A;
reg [1:0] r_SDO_B;
reg [1:0] r_SDO_C;
reg [1:0] r_SDO_D;
//
always @(posedge b_clk, negedge reset_n)
	if (!reset_n) begin
		r_DCO   <= 2'b0;
		r_SDO_A <= 2'b0;
		r_SDO_B <= 2'b0;
		r_SDO_C <= 2'b0;
		r_SDO_D <= 2'b0;
		end 
	else begin 
		r_DCO   <= {r_DCO  [0], i_DCO   };
		r_SDO_A <= {r_SDO_A[0], i_SDO[0]};
		r_SDO_B <= {r_SDO_B[0], i_SDO[1]};
		r_SDO_C <= {r_SDO_C[0], i_SDO[2]};
		r_SDO_D <= {r_SDO_D[0], i_SDO[3]};
		end
//
wire w_DCO_rise = (~r_DCO[1]) & ( r_DCO[0]);
wire w_DCO_fall = ( r_DCO[1]) & (~r_DCO[0]);
//}

//// SDO shift registers //{
reg [15:0]  sr_p_data_A;
reg [15:0]  sr_p_data_B;
reg [15:0]  sr_p_data_C;
reg [15:0]  sr_p_data_D;
//
always @(posedge b_clk, negedge reset_n)
	if (!reset_n) begin
		sr_p_data_A  <= 16'b0;
		sr_p_data_B  <= 16'b0;
		sr_p_data_C  <= 16'b0;
		sr_p_data_D  <= 16'b0;
		end 
	else begin 
		if (w_DCO_rise | w_DCO_fall) begin
			sr_p_data_A <= {sr_p_data_A[14:0], r_SDO_A[1]};
			sr_p_data_B <= {sr_p_data_B[14:0], r_SDO_B[1]};
			sr_p_data_C <= {sr_p_data_C[14:0], r_SDO_C[1]};
			sr_p_data_D <= {sr_p_data_D[14:0], r_SDO_D[1]};
			end
		end
//}

//// SDO recv bit counter //{
(* keep = "true" *) reg [4:0] r_cnt_SDO_bits     ;
(* keep = "true" *) reg       r_done_cnt_SDO_bits;
(* keep = "true" *) reg       r_check_first_data;
//
wire w_trig_conv          = i_trig_conv;
wire w_clear_cnt_SDO_bits = i_clear_cnt_SDO_bits; // assign by frame index
//
always @(posedge b_clk, negedge reset_n)
	if (!reset_n) begin
		r_cnt_SDO_bits       <= 5'b0;
		r_done_cnt_SDO_bits  <= 1'b0;
		end 
	else begin 
		if (w_DCO_rise | w_DCO_fall) begin
			r_cnt_SDO_bits       <= r_cnt_SDO_bits + 1;
			r_done_cnt_SDO_bits  <= ((r_cnt_SDO_bits == 5'd15)&r_check_first_data)? 1'b1 : 1'b0;
			end
		else if (w_clear_cnt_SDO_bits) begin 
			r_cnt_SDO_bits  <= 5'b0;
			r_done_cnt_SDO_bits  <= 1'b0;
			end
		else begin
			r_cnt_SDO_bits       <= r_cnt_SDO_bits; // stay
			r_done_cnt_SDO_bits  <= 1'b0; // clear
			end
		end
//
always @(posedge b_clk, negedge reset_n)
	if (!reset_n) begin
		r_check_first_data   <= 1'b0;
		end 
	else begin 
		if (w_DCO_rise | w_DCO_fall) begin
			r_check_first_data   <= ((r_cnt_SDO_bits == 5'd15))? 1'b1 : r_check_first_data;
			end
		else if (w_trig_conv) begin 
			r_check_first_data   <= 1'b0;
			end
		else begin
			r_check_first_data   <= r_check_first_data;
			end
		end
//}

//// SDO registers //{
(* keep = "true" *) reg [15:0]  r_p_data_A;
(* keep = "true" *) reg [15:0]  r_p_data_B;
(* keep = "true" *) reg [15:0]  r_p_data_C;
(* keep = "true" *) reg [15:0]  r_p_data_D;
//
wire w_trig_load_SDO = r_done_cnt_SDO_bits; // 1 clock delay
//
always @(posedge b_clk, negedge reset_n)
	if (!reset_n) begin
		r_p_data_A  <= 16'b0;
		r_p_data_B  <= 16'b0;
		r_p_data_C  <= 16'b0;
		r_p_data_D  <= 16'b0;
		end 
	else begin 
		if (w_trig_load_SDO) begin
			r_p_data_A  <= sr_p_data_A;
			r_p_data_B  <= sr_p_data_B;
			r_p_data_C  <= sr_p_data_C;
			r_p_data_D  <= sr_p_data_D;
			end
		end
//}

//// data output //{
assign  o_p_data_A = r_p_data_A;
assign  o_p_data_B = r_p_data_B;
assign  o_p_data_C = r_p_data_C;
assign  o_p_data_D = r_p_data_D;
//}

//// read control out //{
reg r_p_data_rd;
//
always @(posedge b_clk, negedge reset_n)
	if (!reset_n) begin
		r_p_data_rd       <= 1'b0;
		end 
	else begin 
		r_p_data_rd       <= r_done_cnt_SDO_bits; // delay
		end
//
assign  o_p_data_rd = r_p_data_rd;
//}

// use p_clk
(* keep = "true" *) wire w_trig_load_SDO_pclk;
(* keep = "true" *) wire w_trig_load_SDO_pclk__pre;
// 
signal_ext_bclk_pclk  signal_ext_bclk_pclk__inst_trig_load (
	.b_clk   (b_clk   ),
	.reset_n (reset_n ),
	.p_clk   (p_clk   ),
	.i_sig_b (w_trig_load_SDO),
	.o_sig_b (w_trig_load_SDO_pclk__pre), 
	.o_sig_p (w_trig_load_SDO_pclk) 
);
//
(* keep = "true" *) wire w_trig_conv_pclk;
(* keep = "true" *) wire w_trig_conv_pclk__pre;
// 
signal_ext_bclk_pclk  signal_ext_bclk_pclk__inst_trig_conv (
	.b_clk   (b_clk   ),
	.reset_n (reset_n ),
	.p_clk   (p_clk   ),
	.i_sig_b (w_trig_conv),
	.o_sig_b (w_trig_conv_pclk__pre), 
	.o_sig_p (w_trig_conv_pclk) 
);

//
assign  o_p_data_rd_pclk = w_trig_load_SDO_pclk;
assign  o_trig_conv_pclk = w_trig_conv_pclk;

// p_data count 
(* keep = "true" *) reg [15:0] r_cnt_p_data_pclk;
//
always @(posedge p_clk, negedge reset_n)
	if (!reset_n) begin
		r_cnt_p_data_pclk  <= 16'b0;
		end 
	else begin 
		if (w_trig_conv_pclk) begin
			r_cnt_p_data_pclk  <= 16'b0;
			end
		else if (w_trig_load_SDO_pclk) begin
			r_cnt_p_data_pclk  <=  r_cnt_p_data_pclk + 1 ;
			end
		end
//
assign o_cnt_p_data_pclk = r_cnt_p_data_pclk;

// trig_conv count with rise detect
(* keep = "true" *) reg [15:0] r_cnt_trig_conv_pclk;
reg r_smp_trig_conv_pclk;
//
always @(posedge p_clk, negedge reset_n)
	if (!reset_n) begin
		r_cnt_trig_conv_pclk  <= 16'b0;
		r_smp_trig_conv_pclk  <= 1'b0;
		end 
	else begin 
		if ((~r_smp_trig_conv_pclk) & w_trig_conv_pclk) begin // rise
			r_cnt_trig_conv_pclk  <=  r_cnt_trig_conv_pclk + 1 ;
			end
		//
		r_smp_trig_conv_pclk  <= w_trig_conv_pclk;
		end
//
assign o_cnt_trig_conv_pclk = r_cnt_trig_conv_pclk;


// p_data 
(* keep = "true" *) reg [15:0]  r_p_data_A_pclk;
(* keep = "true" *) reg [15:0]  r_p_data_B_pclk;
(* keep = "true" *) reg [15:0]  r_p_data_C_pclk;
(* keep = "true" *) reg [15:0]  r_p_data_D_pclk;
//
(* keep = "true" *) reg [15:0]  r_p_data_test__pclk; // test pattern // clear by w_trig_conv 
//
always @(posedge p_clk, negedge reset_n)
	if (!reset_n) begin
		r_p_data_test__pclk  <= 16'b0;
		end 
	else begin 
		if (w_trig_conv) begin
			r_p_data_test__pclk  <= 16'b0;
			end
		else if (w_trig_load_SDO_pclk__pre) begin
			r_p_data_test__pclk  <=  r_p_data_test__pclk + 1 ;
			end
		end
// fast test pattern option 
(* keep = "true" *) reg [15:0]  r_p_data_test__bclk; // test pattern // fast option
always @(posedge b_clk, negedge reset_n)
	if (!reset_n) begin
		r_p_data_test__bclk  <= 16'b0;
		end 
	else begin 
		if (w_trig_conv) begin
			r_p_data_test__bclk  <= 16'b0;
			end
		else begin
			r_p_data_test__bclk  <=  r_p_data_test__bclk + 1 ;
			end
		end
//
always @(posedge p_clk, negedge reset_n)
	if (!reset_n) begin
		r_p_data_A_pclk  <= 16'b0;
		r_p_data_B_pclk  <= 16'b0;
		r_p_data_C_pclk  <= 16'b0;
		r_p_data_D_pclk  <= 16'b0;
		end 
	else begin 
		if (w_trig_load_SDO_pclk__pre) begin
			r_p_data_A_pclk  <=  (i_test_mode_en)? ((i_test_mode_hs)? r_p_data_test__bclk : r_p_data_test__pclk) : sr_p_data_A ;
			r_p_data_B_pclk  <=  (i_test_mode_en)? ((i_test_mode_hs)? r_p_data_test__bclk : r_p_data_test__pclk) : sr_p_data_B ;
			r_p_data_C_pclk  <=  (i_test_mode_en)? ((i_test_mode_hs)? r_p_data_test__bclk : r_p_data_test__pclk) : sr_p_data_C ;
			r_p_data_D_pclk  <=  (i_test_mode_en)? ((i_test_mode_hs)? r_p_data_test__bclk : r_p_data_test__pclk) : sr_p_data_D ;
			end
		end
//
assign  o_p_data_A_pclk = r_p_data_A_pclk;
assign  o_p_data_B_pclk = r_p_data_B_pclk;
assign  o_p_data_C_pclk = r_p_data_C_pclk;
assign  o_p_data_D_pclk = r_p_data_D_pclk;



//}
endmodule


module acc_16b_to_32b (
	// 
	input  wire clk, // assume 12MHz
	input  wire reset_n,
	
	input  wire i_trig_clear,
	input  wire i_trig_load ,
	
	input  wire [15:0] i_data_in, // assume 2's complement
	output wire [31:0] o_data_acc
);
//{
(* keep = "true" *) reg [31:0] r_data_acc;
//
always @(posedge clk, negedge reset_n)
	if (!reset_n) begin
		r_data_acc <= 32'b0;
	end
	else begin
		if (i_trig_clear)
			r_data_acc <= 32'b0;
		else if (i_trig_load)
			r_data_acc <= r_data_acc + {{16{i_data_in[15]}},i_data_in}; // sign bits added
	end
//
assign o_data_acc = r_data_acc;
//}
endmodule


module find_min_max_signed_16b (
	// 
	input  wire clk, // assume 12MHz
	input  wire reset_n,
	
	input  wire i_trig_clear,
	input  wire i_trig_load ,
	
	input  wire [15:0] i_data_in , // assume 2's complement
	output wire [15:0] o_data_min, // min 
	output wire [15:0] o_data_max  // max
);
//{
(* keep = "true" *) reg [15:0] r_data_min;
(* keep = "true" *) reg [15:0] r_data_max;
//
always @(posedge clk, negedge reset_n)
	if (!reset_n) begin
		r_data_min <= 16'h7FFF;
		r_data_max <= 16'h8000;
	end
	else begin
		if (i_trig_clear) begin
			r_data_min <= 16'h7FFF;
			r_data_max <= 16'h8000;
			end
		else if (i_trig_load) begin
			r_data_min <= ($signed(i_data_in) < $signed(r_data_min))? i_data_in : r_data_min;
			r_data_max <= ($signed(i_data_in) > $signed(r_data_max))? i_data_in : r_data_max;
			end
	end
//
assign o_data_min = r_data_min;
assign o_data_max = r_data_max;
//}
endmodule

// TODO: control_adc_ddr_LTC2325
module control_adc_ddr_LTC2325 (
	// 
	input  wire b_clk, // assume 192MHz
	input  wire reset_n,
	input  wire p_clk, // assume 12MHz = 192MHz/16 for parallel data out 
		
	input  wire         i_test_mode_en, // enable test pattern mode
	input  wire         i_test_mode_hs,
		
	//// ADC control //{
	input  wire         i_trig_conv_single , // trigger for one ADC sample
	input  wire         i_trig_conv_run    , // trigger for ADC samples 
	input  wire [15:0]  i_count_period_div4, // sample period = i_count_period_div4*4  //based on 210MHz 
	input  wire [15:0]  i_count_conv_div4  , // adc samples   = i_count_conv_div4*4    // < 2^17 
	
	output wire         o_busy         , // busy during ADC operation 
	output wire         o_busy_pclk    , // busy during ADC operation 
	output wire         o_done_to      , // done trig out
	output wire         o_done_to_pclk , // done trig out
	//}
	
	//// ADC IO port //{
	output wire         o_CNV_B ,
	output wire         o_SCK   ,

	input  wire         i_ADC0_DCO ,
	input  wire [3:0]   i_ADC0_SDO ,
	input  wire         i_ADC1_DCO ,
	input  wire [3:0]   i_ADC1_SDO ,
	input  wire         i_ADC2_DCO ,
	input  wire [3:0]   i_ADC2_SDO ,
	input  wire         i_ADC3_DCO ,
	input  wire [3:0]   i_ADC3_SDO ,
	//}

	//// ADC output interface //{
	// based on b_clk
	output wire [15:0]  o_p_data_ADC0_A, // from i_ADC0_SDO[0]
	output wire [15:0]  o_p_data_ADC0_B, // from i_ADC0_SDO[1]
	output wire [15:0]  o_p_data_ADC0_C, // from i_ADC0_SDO[2]
	output wire [15:0]  o_p_data_ADC0_D, // from i_ADC0_SDO[3]
	output wire [15:0]  o_p_data_ADC1_A, // from i_ADC1_SDO[0]
	output wire [15:0]  o_p_data_ADC1_B, // from i_ADC1_SDO[1]
	output wire [15:0]  o_p_data_ADC1_C, // from i_ADC1_SDO[2]
	output wire [15:0]  o_p_data_ADC1_D, // from i_ADC1_SDO[3]
	output wire [15:0]  o_p_data_ADC2_A, // from i_ADC2_SDO[0]
	output wire [15:0]  o_p_data_ADC2_B, // from i_ADC2_SDO[1]
	output wire [15:0]  o_p_data_ADC2_C, // from i_ADC2_SDO[2]
	output wire [15:0]  o_p_data_ADC2_D, // from i_ADC2_SDO[3]
	output wire [15:0]  o_p_data_ADC3_A, // from i_ADC3_SDO[0]
	output wire [15:0]  o_p_data_ADC3_B, // from i_ADC3_SDO[1]
	output wire [15:0]  o_p_data_ADC3_C, // from i_ADC3_SDO[2]
	output wire [15:0]  o_p_data_ADC3_D, // from i_ADC3_SDO[3]
	
	output wire         o_p_data_ADC0_rd, //
	output wire         o_p_data_ADC1_rd, //
	output wire         o_p_data_ADC2_rd, //
	output wire         o_p_data_ADC3_rd, //
	
	// based on p_clk
	output wire [15:0]  o_p_data_ADC0_A_pclk, // from i_ADC0_SDO[0]
	output wire [15:0]  o_p_data_ADC0_B_pclk, // from i_ADC0_SDO[1]
	output wire [15:0]  o_p_data_ADC0_C_pclk, // from i_ADC0_SDO[2]
	output wire [15:0]  o_p_data_ADC0_D_pclk, // from i_ADC0_SDO[3]
	output wire [15:0]  o_p_data_ADC1_A_pclk, // from i_ADC1_SDO[0]
	output wire [15:0]  o_p_data_ADC1_B_pclk, // from i_ADC1_SDO[1]
	output wire [15:0]  o_p_data_ADC1_C_pclk, // from i_ADC1_SDO[2]
	output wire [15:0]  o_p_data_ADC1_D_pclk, // from i_ADC1_SDO[3]
	output wire [15:0]  o_p_data_ADC2_A_pclk, // from i_ADC2_SDO[0]
	output wire [15:0]  o_p_data_ADC2_B_pclk, // from i_ADC2_SDO[1]
	output wire [15:0]  o_p_data_ADC2_C_pclk, // from i_ADC2_SDO[2]
	output wire [15:0]  o_p_data_ADC2_D_pclk, // from i_ADC2_SDO[3]
	output wire [15:0]  o_p_data_ADC3_A_pclk, // from i_ADC3_SDO[0]
	output wire [15:0]  o_p_data_ADC3_B_pclk, // from i_ADC3_SDO[1]
	output wire [15:0]  o_p_data_ADC3_C_pclk, // from i_ADC3_SDO[2]
	output wire [15:0]  o_p_data_ADC3_D_pclk, // from i_ADC3_SDO[3]

	output wire         o_p_data_ADC0_rd_pclk, //
	output wire         o_p_data_ADC1_rd_pclk, //
	output wire         o_p_data_ADC2_rd_pclk, //
	output wire         o_p_data_ADC3_rd_pclk, //
	
	output wire [15:0]  o_cnt_adc_fifo_in_pclk, //$$ w_SSPI_CNT_ADC_FIFO_IN_WO // w_p_data_ADC0_rd_pclk
	output wire [15:0]  o_cnt_trig_conv_pclk,  //$$ w_SSPI_CNT_ADC_TRIG_WO
	
	//}

	//// ACC interface //{
	output wire [31:0] o_p_data_ADC0_A_ACC,
	output wire [31:0] o_p_data_ADC0_B_ACC,
	output wire [31:0] o_p_data_ADC0_C_ACC,
	output wire [31:0] o_p_data_ADC0_D_ACC,
	output wire [31:0] o_p_data_ADC1_A_ACC,
	output wire [31:0] o_p_data_ADC1_B_ACC,
	output wire [31:0] o_p_data_ADC1_C_ACC,
	output wire [31:0] o_p_data_ADC1_D_ACC,
	output wire [31:0] o_p_data_ADC2_A_ACC,
	output wire [31:0] o_p_data_ADC2_B_ACC,
	output wire [31:0] o_p_data_ADC2_C_ACC,
	output wire [31:0] o_p_data_ADC2_D_ACC,
	output wire [31:0] o_p_data_ADC3_A_ACC,
	output wire [31:0] o_p_data_ADC3_B_ACC,
	output wire [31:0] o_p_data_ADC3_C_ACC,
	output wire [31:0] o_p_data_ADC3_D_ACC,
	//}
	
	//// MIN MAX interface //{
	
	output wire [15:0] o_p_data_ADC0_A_MIN,  wire [15:0] o_p_data_ADC0_A_MAX,
	output wire [15:0] o_p_data_ADC0_B_MIN,  wire [15:0] o_p_data_ADC0_B_MAX,
	output wire [15:0] o_p_data_ADC0_C_MIN,  wire [15:0] o_p_data_ADC0_C_MAX,
	output wire [15:0] o_p_data_ADC0_D_MIN,  wire [15:0] o_p_data_ADC0_D_MAX,
	output wire [15:0] o_p_data_ADC1_A_MIN,  wire [15:0] o_p_data_ADC1_A_MAX,
	output wire [15:0] o_p_data_ADC1_B_MIN,  wire [15:0] o_p_data_ADC1_B_MAX,
	output wire [15:0] o_p_data_ADC1_C_MIN,  wire [15:0] o_p_data_ADC1_C_MAX,
	output wire [15:0] o_p_data_ADC1_D_MIN,  wire [15:0] o_p_data_ADC1_D_MAX,
	output wire [15:0] o_p_data_ADC2_A_MIN,  wire [15:0] o_p_data_ADC2_A_MAX,
	output wire [15:0] o_p_data_ADC2_B_MIN,  wire [15:0] o_p_data_ADC2_B_MAX,
	output wire [15:0] o_p_data_ADC2_C_MIN,  wire [15:0] o_p_data_ADC2_C_MAX,
	output wire [15:0] o_p_data_ADC2_D_MIN,  wire [15:0] o_p_data_ADC2_D_MAX,
	output wire [15:0] o_p_data_ADC3_A_MIN,  wire [15:0] o_p_data_ADC3_A_MAX,
	output wire [15:0] o_p_data_ADC3_B_MIN,  wire [15:0] o_p_data_ADC3_B_MAX,
	output wire [15:0] o_p_data_ADC3_C_MIN,  wire [15:0] o_p_data_ADC3_C_MAX,
	output wire [15:0] o_p_data_ADC3_D_MIN,  wire [15:0] o_p_data_ADC3_D_MAX,
	
	//}
	
	//// FIFO interface //{
	
	// common 
	input  wire f_clk, // assume 104MHz or 108MHz // fifo reading clock 
	
	// adc0
	input  wire        i_fifo_adc0_a_rd_en ,
	output wire [15:0] o_fifo_adc0_a_dout  ,
	output wire        o_fifo_adc0_a_full  ,
	output wire        o_fifo_adc0_a_empty ,
	input  wire        i_fifo_adc0_b_rd_en ,
	output wire [15:0] o_fifo_adc0_b_dout  ,
	output wire        o_fifo_adc0_b_full  ,
	output wire        o_fifo_adc0_b_empty ,
	input  wire        i_fifo_adc0_c_rd_en ,
	output wire [15:0] o_fifo_adc0_c_dout  ,
	output wire        o_fifo_adc0_c_full  ,
	output wire        o_fifo_adc0_c_empty ,
	input  wire        i_fifo_adc0_d_rd_en ,
	output wire [15:0] o_fifo_adc0_d_dout  ,
	output wire        o_fifo_adc0_d_full  ,
	output wire        o_fifo_adc0_d_empty ,
	// adc1
	input  wire        i_fifo_adc1_a_rd_en ,
	output wire [15:0] o_fifo_adc1_a_dout  ,
	output wire        o_fifo_adc1_a_full  ,
	output wire        o_fifo_adc1_a_empty ,
	input  wire        i_fifo_adc1_b_rd_en ,
	output wire [15:0] o_fifo_adc1_b_dout  ,
	output wire        o_fifo_adc1_b_full  ,
	output wire        o_fifo_adc1_b_empty ,
	input  wire        i_fifo_adc1_c_rd_en ,
	output wire [15:0] o_fifo_adc1_c_dout  ,
	output wire        o_fifo_adc1_c_full  ,
	output wire        o_fifo_adc1_c_empty ,
	input  wire        i_fifo_adc1_d_rd_en ,
	output wire [15:0] o_fifo_adc1_d_dout  ,
	output wire        o_fifo_adc1_d_full  ,
	output wire        o_fifo_adc1_d_empty ,
	// adc2
	input  wire        i_fifo_adc2_a_rd_en ,
	output wire [15:0] o_fifo_adc2_a_dout  ,
	output wire        o_fifo_adc2_a_full  ,
	output wire        o_fifo_adc2_a_empty ,
	input  wire        i_fifo_adc2_b_rd_en ,
	output wire [15:0] o_fifo_adc2_b_dout  ,
	output wire        o_fifo_adc2_b_full  ,
	output wire        o_fifo_adc2_b_empty ,
	input  wire        i_fifo_adc2_c_rd_en ,
	output wire [15:0] o_fifo_adc2_c_dout  ,
	output wire        o_fifo_adc2_c_full  ,
	output wire        o_fifo_adc2_c_empty ,
	input  wire        i_fifo_adc2_d_rd_en ,
	output wire [15:0] o_fifo_adc2_d_dout  ,
	output wire        o_fifo_adc2_d_full  ,
	output wire        o_fifo_adc2_d_empty ,
	// adc3
	input  wire        i_fifo_adc3_a_rd_en ,
	output wire [15:0] o_fifo_adc3_a_dout  ,
	output wire        o_fifo_adc3_a_full  ,
	output wire        o_fifo_adc3_a_empty ,
	input  wire        i_fifo_adc3_b_rd_en ,
	output wire [15:0] o_fifo_adc3_b_dout  ,
	output wire        o_fifo_adc3_b_full  ,
	output wire        o_fifo_adc3_b_empty ,
	input  wire        i_fifo_adc3_c_rd_en ,
	output wire [15:0] o_fifo_adc3_c_dout  ,
	output wire        o_fifo_adc3_c_full  ,
	output wire        o_fifo_adc3_c_empty ,
	input  wire        i_fifo_adc3_d_rd_en ,
	output wire [15:0] o_fifo_adc3_d_dout  ,
	output wire        o_fifo_adc3_d_full  ,
	output wire        o_fifo_adc3_d_empty ,
	
	//}
	
	output valid
);
//{

//// valid //{
(* keep = "true" *) reg r_valid;
assign valid = r_valid;
//
always @(posedge b_clk, negedge reset_n)
	if (!reset_n) begin
		r_valid <= 1'b0;
	end
	else begin
		r_valid <= 1'b1;
	end
//}

//// receive SDOs //{
wire w_clear_cnt_SDO_bits;
(* keep = "true" *) wire w_p_data_ADC0_rd;
(* keep = "true" *) wire w_p_data_ADC1_rd;
(* keep = "true" *) wire w_p_data_ADC2_rd;
(* keep = "true" *) wire w_p_data_ADC3_rd;
(* keep = "true" *) wire w_p_data_ADC0_rd_pclk;
(* keep = "true" *) wire w_p_data_ADC1_rd_pclk;
(* keep = "true" *) wire w_p_data_ADC2_rd_pclk;
(* keep = "true" *) wire w_p_data_ADC3_rd_pclk;
//
wire [15:0] w_cnt_p_data_pclk;
wire [15:0] w_cnt_trig_conv_pclk;
//
control_adc_ddr_LTC2325_sdo_recv_core  control_adc_ddr_LTC2325_sdo_recv_core__inst0(  
	.b_clk               (b_clk  ), // assume 192MHz
	.reset_n             (reset_n),
	.p_clk               (p_clk  ), // assume 12MHz
	//
	.i_DCO               (       i_ADC0_DCO),
	.i_SDO               (       i_ADC0_SDO), // [3:0] 
	.o_p_data_A          (o_p_data_ADC0_A), // [15:0]
	.o_p_data_B          (o_p_data_ADC0_B), // [15:0]
	.o_p_data_C          (o_p_data_ADC0_C), // [15:0]
	.o_p_data_D          (o_p_data_ADC0_D), // [15:0]
	.o_p_data_rd         (w_p_data_ADC0_rd),
	.o_p_data_A_pclk     (o_p_data_ADC0_A_pclk), // [15:0]
	.o_p_data_B_pclk     (o_p_data_ADC0_B_pclk), // [15:0]
	.o_p_data_C_pclk     (o_p_data_ADC0_C_pclk), // [15:0]
	.o_p_data_D_pclk     (o_p_data_ADC0_D_pclk), // [15:0]
	.o_p_data_rd_pclk    (w_p_data_ADC0_rd_pclk),
	.o_trig_conv_pclk    (),
	.o_cnt_p_data_pclk   (w_cnt_p_data_pclk),
	.o_cnt_trig_conv_pclk(w_cnt_trig_conv_pclk),
	.i_test_mode_en      (i_test_mode_en),
	.i_test_mode_hs      (i_test_mode_hs),
	.i_clear_cnt_SDO_bits(w_clear_cnt_SDO_bits),
	.i_trig_conv         (i_trig_conv_single|i_trig_conv_run)
);
//
control_adc_ddr_LTC2325_sdo_recv_core  control_adc_ddr_LTC2325_sdo_recv_core__inst1(  
	.b_clk               (b_clk  ), // assume 192MHz
	.reset_n             (reset_n),
	.p_clk               (p_clk  ), // assume 12MHz
	//
	.i_DCO               (       i_ADC1_DCO),
	.i_SDO               (       i_ADC1_SDO), // [3:0] 
	.o_p_data_A          (o_p_data_ADC1_A), // [15:0]
	.o_p_data_B          (o_p_data_ADC1_B), // [15:0]
	.o_p_data_C          (o_p_data_ADC1_C), // [15:0]
	.o_p_data_D          (o_p_data_ADC1_D), // [15:0]
	.o_p_data_rd         (w_p_data_ADC1_rd),
	.o_p_data_A_pclk     (o_p_data_ADC1_A_pclk), // [15:0]
	.o_p_data_B_pclk     (o_p_data_ADC1_B_pclk), // [15:0]
	.o_p_data_C_pclk     (o_p_data_ADC1_C_pclk), // [15:0]
	.o_p_data_D_pclk     (o_p_data_ADC1_D_pclk), // [15:0]
	.o_p_data_rd_pclk    (w_p_data_ADC1_rd_pclk),
	.o_trig_conv_pclk    (),
	.o_cnt_p_data_pclk   (),
	.o_cnt_trig_conv_pclk(),
	.i_test_mode_en      (i_test_mode_en),
	.i_test_mode_hs      (i_test_mode_hs),
	.i_clear_cnt_SDO_bits(w_clear_cnt_SDO_bits),
	.i_trig_conv         (i_trig_conv_single|i_trig_conv_run)
);
//
control_adc_ddr_LTC2325_sdo_recv_core  control_adc_ddr_LTC2325_sdo_recv_core__inst2(  
	.b_clk               (b_clk  ), // assume 192MHz
	.reset_n             (reset_n),
	.p_clk               (p_clk  ), // assume 12MHz
	//
	.i_DCO               (       i_ADC2_DCO),
	.i_SDO               (       i_ADC2_SDO), // [3:0] 
	.o_p_data_A          (o_p_data_ADC2_A), // [15:0]
	.o_p_data_B          (o_p_data_ADC2_B), // [15:0]
	.o_p_data_C          (o_p_data_ADC2_C), // [15:0]
	.o_p_data_D          (o_p_data_ADC2_D), // [15:0]
	.o_p_data_rd         (w_p_data_ADC2_rd),
	.o_p_data_A_pclk     (o_p_data_ADC2_A_pclk), // [15:0]
	.o_p_data_B_pclk     (o_p_data_ADC2_B_pclk), // [15:0]
	.o_p_data_C_pclk     (o_p_data_ADC2_C_pclk), // [15:0]
	.o_p_data_D_pclk     (o_p_data_ADC2_D_pclk), // [15:0]
	.o_p_data_rd_pclk    (w_p_data_ADC2_rd_pclk),
	.o_trig_conv_pclk    (),
	.o_cnt_p_data_pclk   (),
	.o_cnt_trig_conv_pclk(),
	.i_test_mode_en      (i_test_mode_en),
	.i_test_mode_hs      (i_test_mode_hs),
	.i_clear_cnt_SDO_bits(w_clear_cnt_SDO_bits),
	.i_trig_conv         (i_trig_conv_single|i_trig_conv_run)
);
//
control_adc_ddr_LTC2325_sdo_recv_core  control_adc_ddr_LTC2325_sdo_recv_core__inst3(  
	.b_clk               (b_clk  ), // assume 192MHz
	.reset_n             (reset_n),
	.p_clk               (p_clk  ), // assume 12MHz
	//
	.i_DCO               (       i_ADC3_DCO),
	.i_SDO               (       i_ADC3_SDO), // [3:0] 
	.o_p_data_A          (o_p_data_ADC3_A), // [15:0]
	.o_p_data_B          (o_p_data_ADC3_B), // [15:0]
	.o_p_data_C          (o_p_data_ADC3_C), // [15:0]
	.o_p_data_D          (o_p_data_ADC3_D), // [15:0]
	.o_p_data_rd         (w_p_data_ADC3_rd),
	.o_p_data_A_pclk     (o_p_data_ADC3_A_pclk), // [15:0]
	.o_p_data_B_pclk     (o_p_data_ADC3_B_pclk), // [15:0]
	.o_p_data_C_pclk     (o_p_data_ADC3_C_pclk), // [15:0]
	.o_p_data_D_pclk     (o_p_data_ADC3_D_pclk), // [15:0]
	.o_p_data_rd_pclk    (w_p_data_ADC3_rd_pclk),
	.o_trig_conv_pclk    (),
	.o_cnt_p_data_pclk   (),
	.o_cnt_trig_conv_pclk(),
	.i_test_mode_en      (i_test_mode_en),
	.i_test_mode_hs      (i_test_mode_hs),
	.i_clear_cnt_SDO_bits(w_clear_cnt_SDO_bits),
	.i_trig_conv         (i_trig_conv_single|i_trig_conv_run)
);


//
assign  o_p_data_ADC0_rd = w_p_data_ADC0_rd;
assign  o_p_data_ADC1_rd = w_p_data_ADC1_rd;
assign  o_p_data_ADC2_rd = w_p_data_ADC2_rd;
assign  o_p_data_ADC3_rd = w_p_data_ADC3_rd;
//
assign  o_p_data_ADC0_rd_pclk = w_p_data_ADC0_rd_pclk;
assign  o_p_data_ADC1_rd_pclk = w_p_data_ADC1_rd_pclk;
assign  o_p_data_ADC2_rd_pclk = w_p_data_ADC2_rd_pclk;
assign  o_p_data_ADC3_rd_pclk = w_p_data_ADC3_rd_pclk;

//$$
assign  o_cnt_adc_fifo_in_pclk = w_cnt_p_data_pclk;
assign  o_cnt_trig_conv_pclk   = w_cnt_trig_conv_pclk;

//}

//// ACC interface //{
// 24-bit accumulator 
//wire w_trig_clear = i_trig_clear_ACC | i_trig_conv_run; 
wire w_trig_clear = i_trig_conv_run; // for ACC/MIN/MAX
//
//wire w_trig_load_ADC0 = w_p_data_ADC0_rd;
//wire w_trig_load_ADC1 = w_p_data_ADC1_rd;
//wire w_trig_load_ADC2 = w_p_data_ADC2_rd;
//wire w_trig_load_ADC3 = w_p_data_ADC3_rd;
//
wire w_trig_load_ADC0_pclk = w_p_data_ADC0_rd_pclk; // for ACC/MIN/MAX
wire w_trig_load_ADC1_pclk = w_p_data_ADC1_rd_pclk; // for ACC/MIN/MAX
wire w_trig_load_ADC2_pclk = w_p_data_ADC2_rd_pclk; // for ACC/MIN/MAX
wire w_trig_load_ADC3_pclk = w_p_data_ADC3_rd_pclk; // for ACC/MIN/MAX
//
wire [31:0] w_p_data_ADC0_A_ACC;
wire [31:0] w_p_data_ADC0_B_ACC;
wire [31:0] w_p_data_ADC0_C_ACC;
wire [31:0] w_p_data_ADC0_D_ACC;
wire [31:0] w_p_data_ADC1_A_ACC;
wire [31:0] w_p_data_ADC1_B_ACC;
wire [31:0] w_p_data_ADC1_C_ACC;
wire [31:0] w_p_data_ADC1_D_ACC;
wire [31:0] w_p_data_ADC2_A_ACC;
wire [31:0] w_p_data_ADC2_B_ACC;
wire [31:0] w_p_data_ADC2_C_ACC;
wire [31:0] w_p_data_ADC2_D_ACC;
wire [31:0] w_p_data_ADC3_A_ACC;
wire [31:0] w_p_data_ADC3_B_ACC;
wire [31:0] w_p_data_ADC3_C_ACC;
wire [31:0] w_p_data_ADC3_D_ACC;
// adc0
acc_16b_to_32b  acc_16b_to_32b__adc0_a_inst(
	.clk                 (p_clk  ), // assume 12MHz
	.reset_n             (reset_n),
	.i_trig_clear        (w_trig_clear),
	.i_trig_load         (w_trig_load_ADC0_pclk  ),
	.i_data_in           (   o_p_data_ADC0_A_pclk), // [15:0]  // assume 2's complement
	.o_data_acc          (   w_p_data_ADC0_A_ACC )  // [31:0] 
);
acc_16b_to_32b  acc_16b_to_32b__adc0_b_inst(
	.clk                 (p_clk  ), // assume 12MHz
	.reset_n             (reset_n),
	.i_trig_clear        (w_trig_clear),
	.i_trig_load         (w_trig_load_ADC0_pclk  ),
	.i_data_in           (   o_p_data_ADC0_B_pclk), // [15:0]  // assume 2's complement
	.o_data_acc          (   w_p_data_ADC0_B_ACC )  // [31:0] 
);
acc_16b_to_32b  acc_16b_to_32b__adc0_c_inst(
	.clk                 (p_clk  ), // assume 12MHz
	.reset_n             (reset_n),
	.i_trig_clear        (w_trig_clear),
	.i_trig_load         (w_trig_load_ADC0_pclk  ),
	.i_data_in           (   o_p_data_ADC0_C_pclk), // [15:0]  // assume 2's complement
	.o_data_acc          (   w_p_data_ADC0_C_ACC )  // [31:0] 
);
acc_16b_to_32b  acc_16b_to_32b__adc0_d_inst(
	.clk                 (p_clk  ), // assume 12MHz
	.reset_n             (reset_n),
	.i_trig_clear        (w_trig_clear),
	.i_trig_load         (w_trig_load_ADC0_pclk  ),
	.i_data_in           (   o_p_data_ADC0_D_pclk), // [15:0]  // assume 2's complement
	.o_data_acc          (   w_p_data_ADC0_D_ACC )  // [31:0] 
);
// adc1
acc_16b_to_32b  acc_16b_to_32b__adc1_a_inst(
	.clk                 (p_clk  ), // assume 12MHz
	.reset_n             (reset_n),
	.i_trig_clear        (w_trig_clear),
	.i_trig_load         (w_trig_load_ADC1_pclk  ),
	.i_data_in           (   o_p_data_ADC1_A_pclk), // [15:0]  // assume 2's complement
	.o_data_acc          (   w_p_data_ADC1_A_ACC )  // [31:0] 
);
acc_16b_to_32b  acc_16b_to_32b__adc1_b_inst(
	.clk                 (p_clk  ), // assume 12MHz
	.reset_n             (reset_n),
	.i_trig_clear        (w_trig_clear),
	.i_trig_load         (w_trig_load_ADC1_pclk  ),
	.i_data_in           (   o_p_data_ADC1_B_pclk), // [15:0]  // assume 2's complement
	.o_data_acc          (   w_p_data_ADC1_B_ACC )  // [31:0] 
);
acc_16b_to_32b  acc_16b_to_32b__adc1_c_inst(
	.clk                 (p_clk  ), // assume 12MHz
	.reset_n             (reset_n),
	.i_trig_clear        (w_trig_clear),
	.i_trig_load         (w_trig_load_ADC1_pclk  ),
	.i_data_in           (   o_p_data_ADC1_C_pclk), // [15:0]  // assume 2's complement
	.o_data_acc          (   w_p_data_ADC1_C_ACC )  // [31:0] 
);
acc_16b_to_32b  acc_16b_to_32b__adc1_d_inst(
	.clk                 (p_clk  ), // assume 12MHz
	.reset_n             (reset_n),
	.i_trig_clear        (w_trig_clear),
	.i_trig_load         (w_trig_load_ADC1_pclk  ),
	.i_data_in           (   o_p_data_ADC1_D_pclk), // [15:0]  // assume 2's complement
	.o_data_acc          (   w_p_data_ADC1_D_ACC )  // [31:0] 
);
// adc2
acc_16b_to_32b  acc_16b_to_32b__adc2_a_inst(
	.clk                 (p_clk  ), // assume 12MHz
	.reset_n             (reset_n),
	.i_trig_clear        (w_trig_clear),
	.i_trig_load         (w_trig_load_ADC2_pclk  ),
	.i_data_in           (   o_p_data_ADC2_A_pclk), // [15:0]  // assume 2's complement
	.o_data_acc          (   w_p_data_ADC2_A_ACC )  // [31:0] 
);
acc_16b_to_32b  acc_16b_to_32b__adc2_b_inst(
	.clk                 (p_clk  ), // assume 12MHz
	.reset_n             (reset_n),
	.i_trig_clear        (w_trig_clear),
	.i_trig_load         (w_trig_load_ADC2_pclk  ),
	.i_data_in           (   o_p_data_ADC2_B_pclk), // [15:0]  // assume 2's complement
	.o_data_acc          (   w_p_data_ADC2_B_ACC )  // [31:0] 
);
acc_16b_to_32b  acc_16b_to_32b__adc2_c_inst(
	.clk                 (p_clk  ), // assume 12MHz
	.reset_n             (reset_n),
	.i_trig_clear        (w_trig_clear),
	.i_trig_load         (w_trig_load_ADC2_pclk  ),
	.i_data_in           (   o_p_data_ADC2_C_pclk), // [15:0]  // assume 2's complement
	.o_data_acc          (   w_p_data_ADC2_C_ACC )  // [31:0] 
);
acc_16b_to_32b  acc_16b_to_32b__adc2_d_inst(
	.clk                 (p_clk  ), // assume 12MHz
	.reset_n             (reset_n),
	.i_trig_clear        (w_trig_clear),
	.i_trig_load         (w_trig_load_ADC2_pclk  ),
	.i_data_in           (   o_p_data_ADC2_D_pclk), // [15:0]  // assume 2's complement
	.o_data_acc          (   w_p_data_ADC2_D_ACC )  // [31:0] 
);
// adc3
acc_16b_to_32b  acc_16b_to_32b__adc3_a_inst(
	.clk                 (p_clk  ), // assume 12MHz
	.reset_n             (reset_n),
	.i_trig_clear        (w_trig_clear),
	.i_trig_load         (w_trig_load_ADC3_pclk  ),
	.i_data_in           (   o_p_data_ADC3_A_pclk), // [15:0]  // assume 2's complement
	.o_data_acc          (   w_p_data_ADC3_A_ACC )  // [31:0] 
);
acc_16b_to_32b  acc_16b_to_32b__adc3_b_inst(
	.clk                 (p_clk  ), // assume 12MHz
	.reset_n             (reset_n),
	.i_trig_clear        (w_trig_clear),
	.i_trig_load         (w_trig_load_ADC3_pclk  ),
	.i_data_in           (   o_p_data_ADC3_B_pclk), // [15:0]  // assume 2's complement
	.o_data_acc          (   w_p_data_ADC3_B_ACC )  // [31:0] 
);
acc_16b_to_32b  acc_16b_to_32b__adc3_c_inst(
	.clk                 (p_clk  ), // assume 12MHz
	.reset_n             (reset_n),
	.i_trig_clear        (w_trig_clear),
	.i_trig_load         (w_trig_load_ADC3_pclk  ),
	.i_data_in           (   o_p_data_ADC3_C_pclk), // [15:0]  // assume 2's complement
	.o_data_acc          (   w_p_data_ADC3_C_ACC )  // [31:0] 
);
acc_16b_to_32b  acc_16b_to_32b__adc3_d_inst(
	.clk                 (p_clk  ), // assume 12MHz
	.reset_n             (reset_n),
	.i_trig_clear        (w_trig_clear),
	.i_trig_load         (w_trig_load_ADC3_pclk  ),
	.i_data_in           (   o_p_data_ADC3_D_pclk), // [15:0]  // assume 2's complement
	.o_data_acc          (   w_p_data_ADC3_D_ACC )  // [31:0] 
);
//
assign o_p_data_ADC0_A_ACC = w_p_data_ADC0_A_ACC;
assign o_p_data_ADC0_B_ACC = w_p_data_ADC0_B_ACC;
assign o_p_data_ADC0_C_ACC = w_p_data_ADC0_C_ACC;
assign o_p_data_ADC0_D_ACC = w_p_data_ADC0_D_ACC;
assign o_p_data_ADC1_A_ACC = w_p_data_ADC1_A_ACC;
assign o_p_data_ADC1_B_ACC = w_p_data_ADC1_B_ACC;
assign o_p_data_ADC1_C_ACC = w_p_data_ADC1_C_ACC;
assign o_p_data_ADC1_D_ACC = w_p_data_ADC1_D_ACC;
assign o_p_data_ADC2_A_ACC = w_p_data_ADC2_A_ACC;
assign o_p_data_ADC2_B_ACC = w_p_data_ADC2_B_ACC;
assign o_p_data_ADC2_C_ACC = w_p_data_ADC2_C_ACC;
assign o_p_data_ADC2_D_ACC = w_p_data_ADC2_D_ACC;
assign o_p_data_ADC3_A_ACC = w_p_data_ADC3_A_ACC;
assign o_p_data_ADC3_B_ACC = w_p_data_ADC3_B_ACC;
assign o_p_data_ADC3_C_ACC = w_p_data_ADC3_C_ACC;
assign o_p_data_ADC3_D_ACC = w_p_data_ADC3_D_ACC;

//}

//// MIN-MAX interface //{
// find min and max 

wire [15:0] w_p_data_ADC0_A_MIN;  wire [15:0] w_p_data_ADC0_A_MAX;
wire [15:0] w_p_data_ADC0_B_MIN;  wire [15:0] w_p_data_ADC0_B_MAX;
wire [15:0] w_p_data_ADC0_C_MIN;  wire [15:0] w_p_data_ADC0_C_MAX;
wire [15:0] w_p_data_ADC0_D_MIN;  wire [15:0] w_p_data_ADC0_D_MAX;
wire [15:0] w_p_data_ADC1_A_MIN;  wire [15:0] w_p_data_ADC1_A_MAX;
wire [15:0] w_p_data_ADC1_B_MIN;  wire [15:0] w_p_data_ADC1_B_MAX;
wire [15:0] w_p_data_ADC1_C_MIN;  wire [15:0] w_p_data_ADC1_C_MAX;
wire [15:0] w_p_data_ADC1_D_MIN;  wire [15:0] w_p_data_ADC1_D_MAX;
wire [15:0] w_p_data_ADC2_A_MIN;  wire [15:0] w_p_data_ADC2_A_MAX;
wire [15:0] w_p_data_ADC2_B_MIN;  wire [15:0] w_p_data_ADC2_B_MAX;
wire [15:0] w_p_data_ADC2_C_MIN;  wire [15:0] w_p_data_ADC2_C_MAX;
wire [15:0] w_p_data_ADC2_D_MIN;  wire [15:0] w_p_data_ADC2_D_MAX;
wire [15:0] w_p_data_ADC3_A_MIN;  wire [15:0] w_p_data_ADC3_A_MAX;
wire [15:0] w_p_data_ADC3_B_MIN;  wire [15:0] w_p_data_ADC3_B_MAX;
wire [15:0] w_p_data_ADC3_C_MIN;  wire [15:0] w_p_data_ADC3_C_MAX;
wire [15:0] w_p_data_ADC3_D_MIN;  wire [15:0] w_p_data_ADC3_D_MAX;


// adc0
find_min_max_signed_16b  find_min_max_signed_16b__adc0_a_inst(
	.clk                 (p_clk  ), // assume 12MHz
	.reset_n             (reset_n),
	.i_trig_clear        (w_trig_clear),
	.i_trig_load         (w_trig_load_ADC0_pclk  ),
	.i_data_in           (   o_p_data_ADC0_A_pclk), // [15:0]  // assume 2's complement
	.o_data_min          (   w_p_data_ADC0_A_MIN ), // [15:0] 
	.o_data_max          (   w_p_data_ADC0_A_MAX )  // [15:0] 
);
find_min_max_signed_16b  find_min_max_signed_16b__adc0_b_inst(
	.clk                 (p_clk  ), // assume 12MHz
	.reset_n             (reset_n),
	.i_trig_clear        (w_trig_clear),
	.i_trig_load         (w_trig_load_ADC0_pclk  ),
	.i_data_in           (   o_p_data_ADC0_B_pclk), // [15:0]  // assume 2's complement
	.o_data_min          (   w_p_data_ADC0_B_MIN ), // [15:0] 
	.o_data_max          (   w_p_data_ADC0_B_MAX )  // [15:0] 
);
find_min_max_signed_16b  find_min_max_signed_16b__adc0_c_inst(
	.clk                 (p_clk  ), // assume 12MHz
	.reset_n             (reset_n),
	.i_trig_clear        (w_trig_clear),
	.i_trig_load         (w_trig_load_ADC0_pclk  ),
	.i_data_in           (   o_p_data_ADC0_C_pclk), // [15:0]  // assume 2's complement
	.o_data_min          (   w_p_data_ADC0_C_MIN ), // [15:0] 
	.o_data_max          (   w_p_data_ADC0_C_MAX )  // [15:0] 
);
find_min_max_signed_16b  find_min_max_signed_16b__adc0_d_inst(
	.clk                 (p_clk  ), // assume 12MHz
	.reset_n             (reset_n),
	.i_trig_clear        (w_trig_clear),
	.i_trig_load         (w_trig_load_ADC0_pclk  ),
	.i_data_in           (   o_p_data_ADC0_D_pclk), // [15:0]  // assume 2's complement
	.o_data_min          (   w_p_data_ADC0_D_MIN ), // [15:0] 
	.o_data_max          (   w_p_data_ADC0_D_MAX )  // [15:0] 
);
// adc1
find_min_max_signed_16b  find_min_max_signed_16b__adc1_a_inst(
	.clk                 (p_clk  ), // assume 12MHz
	.reset_n             (reset_n),
	.i_trig_clear        (w_trig_clear),
	.i_trig_load         (w_trig_load_ADC1_pclk  ),
	.i_data_in           (   o_p_data_ADC1_A_pclk), // [15:0]  // assume 2's complement
	.o_data_min          (   w_p_data_ADC1_A_MIN ), // [15:0] 
	.o_data_max          (   w_p_data_ADC1_A_MAX )  // [15:0] 
);
find_min_max_signed_16b  find_min_max_signed_16b__adc1_b_inst(
	.clk                 (p_clk  ), // assume 12MHz
	.reset_n             (reset_n),
	.i_trig_clear        (w_trig_clear),
	.i_trig_load         (w_trig_load_ADC1_pclk  ),
	.i_data_in           (   o_p_data_ADC1_B_pclk), // [15:0]  // assume 2's complement
	.o_data_min          (   w_p_data_ADC1_B_MIN ), // [15:0] 
	.o_data_max          (   w_p_data_ADC1_B_MAX )  // [15:0] 
);
find_min_max_signed_16b  find_min_max_signed_16b__adc1_c_inst(
	.clk                 (p_clk  ), // assume 12MHz
	.reset_n             (reset_n),
	.i_trig_clear        (w_trig_clear),
	.i_trig_load         (w_trig_load_ADC1_pclk  ),
	.i_data_in           (   o_p_data_ADC1_C_pclk), // [15:0]  // assume 2's complement
	.o_data_min          (   w_p_data_ADC1_C_MIN ), // [15:0] 
	.o_data_max          (   w_p_data_ADC1_C_MAX )  // [15:0] 
);
find_min_max_signed_16b  find_min_max_signed_16b__adc1_d_inst(
	.clk                 (p_clk  ), // assume 12MHz
	.reset_n             (reset_n),
	.i_trig_clear        (w_trig_clear),
	.i_trig_load         (w_trig_load_ADC1_pclk  ),
	.i_data_in           (   o_p_data_ADC1_D_pclk), // [15:0]  // assume 2's complement
	.o_data_min          (   w_p_data_ADC1_D_MIN ), // [15:0] 
	.o_data_max          (   w_p_data_ADC1_D_MAX )  // [15:0] 
);
// adc2
find_min_max_signed_16b  find_min_max_signed_16b__adc2_a_inst(
	.clk                 (p_clk  ), // assume 12MHz
	.reset_n             (reset_n),
	.i_trig_clear        (w_trig_clear),
	.i_trig_load         (w_trig_load_ADC2_pclk  ),
	.i_data_in           (   o_p_data_ADC2_A_pclk), // [15:0]  // assume 2's complement
	.o_data_min          (   w_p_data_ADC2_A_MIN ), // [15:0] 
	.o_data_max          (   w_p_data_ADC2_A_MAX )  // [15:0] 
);
find_min_max_signed_16b  find_min_max_signed_16b__adc2_b_inst(
	.clk                 (p_clk  ), // assume 12MHz
	.reset_n             (reset_n),
	.i_trig_clear        (w_trig_clear),
	.i_trig_load         (w_trig_load_ADC2_pclk  ),
	.i_data_in           (   o_p_data_ADC2_B_pclk), // [15:0]  // assume 2's complement
	.o_data_min          (   w_p_data_ADC2_B_MIN ), // [15:0] 
	.o_data_max          (   w_p_data_ADC2_B_MAX )  // [15:0] 
);
find_min_max_signed_16b  find_min_max_signed_16b__adc2_c_inst(
	.clk                 (p_clk  ), // assume 12MHz
	.reset_n             (reset_n),
	.i_trig_clear        (w_trig_clear),
	.i_trig_load         (w_trig_load_ADC2_pclk  ),
	.i_data_in           (   o_p_data_ADC2_C_pclk), // [15:0]  // assume 2's complement
	.o_data_min          (   w_p_data_ADC2_C_MIN ), // [15:0] 
	.o_data_max          (   w_p_data_ADC2_C_MAX )  // [15:0] 
);
find_min_max_signed_16b  find_min_max_signed_16b__adc2_d_inst(
	.clk                 (p_clk  ), // assume 12MHz
	.reset_n             (reset_n),
	.i_trig_clear        (w_trig_clear),
	.i_trig_load         (w_trig_load_ADC2_pclk  ),
	.i_data_in           (   o_p_data_ADC2_D_pclk), // [15:0]  // assume 2's complement
	.o_data_min          (   w_p_data_ADC2_D_MIN ), // [15:0] 
	.o_data_max          (   w_p_data_ADC2_D_MAX )  // [15:0] 
);
// adc3
find_min_max_signed_16b  find_min_max_signed_16b__adc3_a_inst(
	.clk                 (p_clk  ), // assume 12MHz
	.reset_n             (reset_n),
	.i_trig_clear        (w_trig_clear),
	.i_trig_load         (w_trig_load_ADC3_pclk  ),
	.i_data_in           (   o_p_data_ADC3_A_pclk), // [15:0]  // assume 2's complement
	.o_data_min          (   w_p_data_ADC3_A_MIN ), // [15:0] 
	.o_data_max          (   w_p_data_ADC3_A_MAX )  // [15:0] 
);
find_min_max_signed_16b  find_min_max_signed_16b__adc3_b_inst(
	.clk                 (p_clk  ), // assume 12MHz
	.reset_n             (reset_n),
	.i_trig_clear        (w_trig_clear),
	.i_trig_load         (w_trig_load_ADC3_pclk  ),
	.i_data_in           (   o_p_data_ADC3_B_pclk), // [15:0]  // assume 2's complement
	.o_data_min          (   w_p_data_ADC3_B_MIN ), // [15:0] 
	.o_data_max          (   w_p_data_ADC3_B_MAX )  // [15:0] 
);
find_min_max_signed_16b  find_min_max_signed_16b__adc3_c_inst(
	.clk                 (p_clk  ), // assume 12MHz
	.reset_n             (reset_n),
	.i_trig_clear        (w_trig_clear),
	.i_trig_load         (w_trig_load_ADC3_pclk  ),
	.i_data_in           (   o_p_data_ADC3_C_pclk), // [15:0]  // assume 2's complement
	.o_data_min          (   w_p_data_ADC3_C_MIN ), // [15:0] 
	.o_data_max          (   w_p_data_ADC3_C_MAX )  // [15:0] 
);
find_min_max_signed_16b  find_min_max_signed_16b__adc3_d_inst(
	.clk                 (p_clk  ), // assume 12MHz
	.reset_n             (reset_n),
	.i_trig_clear        (w_trig_clear),
	.i_trig_load         (w_trig_load_ADC3_pclk  ),
	.i_data_in           (   o_p_data_ADC3_D_pclk), // [15:0]  // assume 2's complement
	.o_data_min          (   w_p_data_ADC3_D_MIN ), // [15:0] 
	.o_data_max          (   w_p_data_ADC3_D_MAX )  // [15:0] 
);
//

assign o_p_data_ADC0_A_MIN = w_p_data_ADC0_A_MIN;  assign o_p_data_ADC0_A_MAX = w_p_data_ADC0_A_MAX;
assign o_p_data_ADC0_B_MIN = w_p_data_ADC0_B_MIN;  assign o_p_data_ADC0_B_MAX = w_p_data_ADC0_B_MAX;
assign o_p_data_ADC0_C_MIN = w_p_data_ADC0_C_MIN;  assign o_p_data_ADC0_C_MAX = w_p_data_ADC0_C_MAX;
assign o_p_data_ADC0_D_MIN = w_p_data_ADC0_D_MIN;  assign o_p_data_ADC0_D_MAX = w_p_data_ADC0_D_MAX;
assign o_p_data_ADC1_A_MIN = w_p_data_ADC1_A_MIN;  assign o_p_data_ADC1_A_MAX = w_p_data_ADC1_A_MAX;
assign o_p_data_ADC1_B_MIN = w_p_data_ADC1_B_MIN;  assign o_p_data_ADC1_B_MAX = w_p_data_ADC1_B_MAX;
assign o_p_data_ADC1_C_MIN = w_p_data_ADC1_C_MIN;  assign o_p_data_ADC1_C_MAX = w_p_data_ADC1_C_MAX;
assign o_p_data_ADC1_D_MIN = w_p_data_ADC1_D_MIN;  assign o_p_data_ADC1_D_MAX = w_p_data_ADC1_D_MAX;
assign o_p_data_ADC2_A_MIN = w_p_data_ADC2_A_MIN;  assign o_p_data_ADC2_A_MAX = w_p_data_ADC2_A_MAX;
assign o_p_data_ADC2_B_MIN = w_p_data_ADC2_B_MIN;  assign o_p_data_ADC2_B_MAX = w_p_data_ADC2_B_MAX;
assign o_p_data_ADC2_C_MIN = w_p_data_ADC2_C_MIN;  assign o_p_data_ADC2_C_MAX = w_p_data_ADC2_C_MAX;
assign o_p_data_ADC2_D_MIN = w_p_data_ADC2_D_MIN;  assign o_p_data_ADC2_D_MAX = w_p_data_ADC2_D_MAX;
assign o_p_data_ADC3_A_MIN = w_p_data_ADC3_A_MIN;  assign o_p_data_ADC3_A_MAX = w_p_data_ADC3_A_MAX;
assign o_p_data_ADC3_B_MIN = w_p_data_ADC3_B_MIN;  assign o_p_data_ADC3_B_MAX = w_p_data_ADC3_B_MAX;
assign o_p_data_ADC3_C_MIN = w_p_data_ADC3_C_MIN;  assign o_p_data_ADC3_C_MAX = w_p_data_ADC3_C_MAX;
assign o_p_data_ADC3_D_MIN = w_p_data_ADC3_D_MIN;  assign o_p_data_ADC3_D_MAX = w_p_data_ADC3_D_MAX;

//}

//// FIFO interface //{
// SDO base clock 210MHz vs p_clk 12MHz --> slave SPI base clock 104MHz vs USB pipe-out 108MHz
//
// fifo_generator_6 : from adc (16-bit, 12MHz) to slave spi bus (104MHz) or usb interface (108MHz)
//   width "16-bit"
//   depth "32768 = 2^15" // vs "65536 = 2^16"
//   read mode: "first word fall through (FWFT)"  ... // vs standard
//   rd 108MHz
//   wr 12MHz
//   USE built-in FIFO // vs block ram
//   NO  output pipeline register (embedded and fabric)		

// common
wire        w_fifo_adcx_rst    = i_trig_conv_run ; // reset fifo at the beginning of run trigger
wire        w_fifo_adcx_wr_clk = p_clk ; // 12MHz
wire        w_fifo_adcx_rd_clk = f_clk ; // 104MHz or 108MHz
// adc0
wire        w_fifo_adc0_a_wr_en = w_p_data_ADC0_rd_pclk;
wire [15:0] w_fifo_adc0_a_din   = o_p_data_ADC0_A_pclk ;
wire        w_fifo_adc0_a_rd_en =   i_fifo_adc0_a_rd_en  ;
wire [15:0] w_fifo_adc0_a_dout  ;
wire        w_fifo_adc0_a_full  ;
wire        w_fifo_adc0_a_empty ;
assign      o_fifo_adc0_a_dout  =   w_fifo_adc0_a_dout  ;
assign      o_fifo_adc0_a_full  =   w_fifo_adc0_a_full  ;
assign      o_fifo_adc0_a_empty =   w_fifo_adc0_a_empty ;
wire        w_fifo_adc0_b_wr_en = w_p_data_ADC0_rd_pclk;
wire [15:0] w_fifo_adc0_b_din   = o_p_data_ADC0_B_pclk ;
wire        w_fifo_adc0_b_rd_en =   i_fifo_adc0_b_rd_en  ;
wire [15:0] w_fifo_adc0_b_dout  ;
wire        w_fifo_adc0_b_full  ;
wire        w_fifo_adc0_b_empty ;
assign      o_fifo_adc0_b_dout  =   w_fifo_adc0_b_dout  ;
assign      o_fifo_adc0_b_full  =   w_fifo_adc0_b_full  ;
assign      o_fifo_adc0_b_empty =   w_fifo_adc0_b_empty ;
wire        w_fifo_adc0_c_wr_en = w_p_data_ADC0_rd_pclk;
wire [15:0] w_fifo_adc0_c_din   = o_p_data_ADC0_C_pclk ;
wire        w_fifo_adc0_c_rd_en =   i_fifo_adc0_c_rd_en  ;
wire [15:0] w_fifo_adc0_c_dout  ;
wire        w_fifo_adc0_c_full  ;
wire        w_fifo_adc0_c_empty ;
assign      o_fifo_adc0_c_dout  =   w_fifo_adc0_c_dout  ;
assign      o_fifo_adc0_c_full  =   w_fifo_adc0_c_full  ;
assign      o_fifo_adc0_c_empty =   w_fifo_adc0_c_empty ;
wire        w_fifo_adc0_d_wr_en = w_p_data_ADC0_rd_pclk;
wire [15:0] w_fifo_adc0_d_din   = o_p_data_ADC0_D_pclk ;
wire        w_fifo_adc0_d_rd_en =   i_fifo_adc0_d_rd_en  ;
wire [15:0] w_fifo_adc0_d_dout  ;
wire        w_fifo_adc0_d_full  ;
wire        w_fifo_adc0_d_empty ;
assign      o_fifo_adc0_d_dout  =   w_fifo_adc0_d_dout  ;
assign      o_fifo_adc0_d_full  =   w_fifo_adc0_d_full  ;
assign      o_fifo_adc0_d_empty =   w_fifo_adc0_d_empty ;
// adc1
wire        w_fifo_adc1_a_wr_en = w_p_data_ADC1_rd_pclk;
wire [15:0] w_fifo_adc1_a_din   = o_p_data_ADC1_A_pclk ;
wire        w_fifo_adc1_a_rd_en =   i_fifo_adc1_a_rd_en  ;
wire [15:0] w_fifo_adc1_a_dout  ;
wire        w_fifo_adc1_a_full  ;
wire        w_fifo_adc1_a_empty ;
assign      o_fifo_adc1_a_dout  =   w_fifo_adc1_a_dout  ;
assign      o_fifo_adc1_a_full  =   w_fifo_adc1_a_full  ;
assign      o_fifo_adc1_a_empty =   w_fifo_adc1_a_empty ;
wire        w_fifo_adc1_b_wr_en = w_p_data_ADC1_rd_pclk;
wire [15:0] w_fifo_adc1_b_din   = o_p_data_ADC1_B_pclk ;
wire        w_fifo_adc1_b_rd_en =   i_fifo_adc1_b_rd_en  ;
wire [15:0] w_fifo_adc1_b_dout  ;
wire        w_fifo_adc1_b_full  ;
wire        w_fifo_adc1_b_empty ;
assign      o_fifo_adc1_b_dout  =   w_fifo_adc1_b_dout  ;
assign      o_fifo_adc1_b_full  =   w_fifo_adc1_b_full  ;
assign      o_fifo_adc1_b_empty =   w_fifo_adc1_b_empty ;
wire        w_fifo_adc1_c_wr_en = w_p_data_ADC1_rd_pclk;
wire [15:0] w_fifo_adc1_c_din   = o_p_data_ADC1_C_pclk ;
wire        w_fifo_adc1_c_rd_en =   i_fifo_adc1_c_rd_en  ;
wire [15:0] w_fifo_adc1_c_dout  ;
wire        w_fifo_adc1_c_full  ;
wire        w_fifo_adc1_c_empty ;
assign      o_fifo_adc1_c_dout  =   w_fifo_adc1_c_dout  ;
assign      o_fifo_adc1_c_full  =   w_fifo_adc1_c_full  ;
assign      o_fifo_adc1_c_empty =   w_fifo_adc1_c_empty ;
wire        w_fifo_adc1_d_wr_en = w_p_data_ADC1_rd_pclk;
wire [15:0] w_fifo_adc1_d_din   = o_p_data_ADC1_D_pclk ;
wire        w_fifo_adc1_d_rd_en =   i_fifo_adc1_d_rd_en  ;
wire [15:0] w_fifo_adc1_d_dout  ;
wire        w_fifo_adc1_d_full  ;
wire        w_fifo_adc1_d_empty ;
assign      o_fifo_adc1_d_dout  =   w_fifo_adc1_d_dout  ;
assign      o_fifo_adc1_d_full  =   w_fifo_adc1_d_full  ;
assign      o_fifo_adc1_d_empty =   w_fifo_adc1_d_empty ;
// adc2
wire        w_fifo_adc2_a_wr_en = w_p_data_ADC2_rd_pclk;
wire [15:0] w_fifo_adc2_a_din   = o_p_data_ADC2_A_pclk ;
wire        w_fifo_adc2_a_rd_en =   i_fifo_adc2_a_rd_en  ;
wire [15:0] w_fifo_adc2_a_dout  ;
wire        w_fifo_adc2_a_full  ;
wire        w_fifo_adc2_a_empty ;
assign      o_fifo_adc2_a_dout  =   w_fifo_adc2_a_dout  ;
assign      o_fifo_adc2_a_full  =   w_fifo_adc2_a_full  ;
assign      o_fifo_adc2_a_empty =   w_fifo_adc2_a_empty ;
wire        w_fifo_adc2_b_wr_en = w_p_data_ADC2_rd_pclk;
wire [15:0] w_fifo_adc2_b_din   = o_p_data_ADC2_B_pclk ;
wire        w_fifo_adc2_b_rd_en =   i_fifo_adc2_b_rd_en  ;
wire [15:0] w_fifo_adc2_b_dout  ;
wire        w_fifo_adc2_b_full  ;
wire        w_fifo_adc2_b_empty ;
assign      o_fifo_adc2_b_dout  =   w_fifo_adc2_b_dout  ;
assign      o_fifo_adc2_b_full  =   w_fifo_adc2_b_full  ;
assign      o_fifo_adc2_b_empty =   w_fifo_adc2_b_empty ;
wire        w_fifo_adc2_c_wr_en = w_p_data_ADC2_rd_pclk;
wire [15:0] w_fifo_adc2_c_din   = o_p_data_ADC2_C_pclk ;
wire        w_fifo_adc2_c_rd_en =   i_fifo_adc2_c_rd_en  ;
wire [15:0] w_fifo_adc2_c_dout  ;
wire        w_fifo_adc2_c_full  ;
wire        w_fifo_adc2_c_empty ;
assign      o_fifo_adc2_c_dout  =   w_fifo_adc2_c_dout  ;
assign      o_fifo_adc2_c_full  =   w_fifo_adc2_c_full  ;
assign      o_fifo_adc2_c_empty =   w_fifo_adc2_c_empty ;
wire        w_fifo_adc2_d_wr_en = w_p_data_ADC2_rd_pclk;
wire [15:0] w_fifo_adc2_d_din   = o_p_data_ADC2_D_pclk ;
wire        w_fifo_adc2_d_rd_en =   i_fifo_adc2_d_rd_en  ;
wire [15:0] w_fifo_adc2_d_dout  ;
wire        w_fifo_adc2_d_full  ;
wire        w_fifo_adc2_d_empty ;
assign      o_fifo_adc2_d_dout  =   w_fifo_adc2_d_dout  ;
assign      o_fifo_adc2_d_full  =   w_fifo_adc2_d_full  ;
assign      o_fifo_adc2_d_empty =   w_fifo_adc2_d_empty ;
// adc3
wire        w_fifo_adc3_a_wr_en = w_p_data_ADC3_rd_pclk;
wire [15:0] w_fifo_adc3_a_din   = o_p_data_ADC3_A_pclk ;
wire        w_fifo_adc3_a_rd_en =   i_fifo_adc3_a_rd_en  ;
wire [15:0] w_fifo_adc3_a_dout  ;
wire        w_fifo_adc3_a_full  ;
wire        w_fifo_adc3_a_empty ;
assign      o_fifo_adc3_a_dout  =   w_fifo_adc3_a_dout  ;
assign      o_fifo_adc3_a_full  =   w_fifo_adc3_a_full  ;
assign      o_fifo_adc3_a_empty =   w_fifo_adc3_a_empty ;
wire        w_fifo_adc3_b_wr_en = w_p_data_ADC3_rd_pclk;
wire [15:0] w_fifo_adc3_b_din   = o_p_data_ADC3_B_pclk ;
wire        w_fifo_adc3_b_rd_en =   i_fifo_adc3_b_rd_en  ;
wire [15:0] w_fifo_adc3_b_dout  ;
wire        w_fifo_adc3_b_full  ;
wire        w_fifo_adc3_b_empty ;
assign      o_fifo_adc3_b_dout  =   w_fifo_adc3_b_dout  ;
assign      o_fifo_adc3_b_full  =   w_fifo_adc3_b_full  ;
assign      o_fifo_adc3_b_empty =   w_fifo_adc3_b_empty ;
wire        w_fifo_adc3_c_wr_en = w_p_data_ADC3_rd_pclk;
wire [15:0] w_fifo_adc3_c_din   = o_p_data_ADC3_C_pclk ;
wire        w_fifo_adc3_c_rd_en =   i_fifo_adc3_c_rd_en  ;
wire [15:0] w_fifo_adc3_c_dout  ;
wire        w_fifo_adc3_c_full  ;
wire        w_fifo_adc3_c_empty ;
assign      o_fifo_adc3_c_dout  =   w_fifo_adc3_c_dout  ;
assign      o_fifo_adc3_c_full  =   w_fifo_adc3_c_full  ;
assign      o_fifo_adc3_c_empty =   w_fifo_adc3_c_empty ;
wire        w_fifo_adc3_d_wr_en = w_p_data_ADC3_rd_pclk;
wire [15:0] w_fifo_adc3_d_din   = o_p_data_ADC3_D_pclk ;
wire        w_fifo_adc3_d_rd_en =   i_fifo_adc3_d_rd_en  ;
wire [15:0] w_fifo_adc3_d_dout  ;
wire        w_fifo_adc3_d_full  ;
wire        w_fifo_adc3_d_empty ;
assign      o_fifo_adc3_d_dout  =   w_fifo_adc3_d_dout  ;
assign      o_fifo_adc3_d_full  =   w_fifo_adc3_d_full  ;
assign      o_fifo_adc3_d_empty =   w_fifo_adc3_d_empty ;

// adc0
fifo_generator_6  fifo_adc0_a_inst (
.rst         (~reset_n | w_fifo_adcx_rst),  // input wire rst
.wr_clk      (w_fifo_adcx_wr_clk ),  // input wire wr_clk  // assume 12MHz
.rd_clk      (w_fifo_adcx_rd_clk ),  // input wire rd_clk
//
.wr_en       (w_fifo_adc0_a_wr_en),  // input wire wr_en
.din         (w_fifo_adc0_a_din  ),  // input wire [15 : 0] din
.rd_en       (w_fifo_adc0_a_rd_en),  // input wire rd_en
.dout        (w_fifo_adc0_a_dout ),  // output wire [15 : 0] dout
.full        (w_fifo_adc0_a_full ),  // output wire full
.empty       (w_fifo_adc0_a_empty),  // output wire empty
.wr_ack      (                   ),  // output wire wr_ack
.valid       (                   )   // output wire valid
);
fifo_generator_6  fifo_adc0_b_inst (
.rst         (~reset_n | w_fifo_adcx_rst),  // input wire rst
.wr_clk      (w_fifo_adcx_wr_clk ),  // input wire wr_clk  // assume 12MHz
.rd_clk      (w_fifo_adcx_rd_clk ),  // input wire rd_clk
//
.wr_en       (w_fifo_adc0_b_wr_en),  // input wire wr_en
.din         (w_fifo_adc0_b_din  ),  // input wire [15 : 0] din
.rd_en       (w_fifo_adc0_b_rd_en),  // input wire rd_en
.dout        (w_fifo_adc0_b_dout ),  // output wire [15 : 0] dout
.full        (w_fifo_adc0_b_full ),  // output wire full
.empty       (w_fifo_adc0_b_empty),  // output wire empty
.wr_ack      (                   ),  // output wire wr_ack
.valid       (                   )   // output wire valid
);
fifo_generator_6  fifo_adc0_c_inst (
.rst         (~reset_n | w_fifo_adcx_rst),  // input wire rst
.wr_clk      (w_fifo_adcx_wr_clk ),  // input wire wr_clk  // assume 12MHz
.rd_clk      (w_fifo_adcx_rd_clk ),  // input wire rd_clk
//
.wr_en       (w_fifo_adc0_c_wr_en),  // input wire wr_en
.din         (w_fifo_adc0_c_din  ),  // input wire [15 : 0] din
.rd_en       (w_fifo_adc0_c_rd_en),  // input wire rd_en
.dout        (w_fifo_adc0_c_dout ),  // output wire [15 : 0] dout
.full        (w_fifo_adc0_c_full ),  // output wire full
.empty       (w_fifo_adc0_c_empty),  // output wire empty
.wr_ack      (                   ),  // output wire wr_ack
.valid       (                   )   // output wire valid
);
fifo_generator_6  fifo_adc0_d_inst (
.rst         (~reset_n | w_fifo_adcx_rst),  // input wire rst
.wr_clk      (w_fifo_adcx_wr_clk ),  // input wire wr_clk  // assume 12MHz
.rd_clk      (w_fifo_adcx_rd_clk ),  // input wire rd_clk
//
.wr_en       (w_fifo_adc0_d_wr_en),  // input wire wr_en
.din         (w_fifo_adc0_d_din  ),  // input wire [15 : 0] din
.rd_en       (w_fifo_adc0_d_rd_en),  // input wire rd_en
.dout        (w_fifo_adc0_d_dout ),  // output wire [15 : 0] dout
.full        (w_fifo_adc0_d_full ),  // output wire full
.empty       (w_fifo_adc0_d_empty),  // output wire empty
.wr_ack      (                   ),  // output wire wr_ack
.valid       (                   )   // output wire valid
);
// adc1
fifo_generator_6  fifo_adc1_a_inst (
.rst         (~reset_n | w_fifo_adcx_rst),  // input wire rst
.wr_clk      (w_fifo_adcx_wr_clk ),  // input wire wr_clk  // assume 12MHz
.rd_clk      (w_fifo_adcx_rd_clk ),  // input wire rd_clk
//
.wr_en       (w_fifo_adc1_a_wr_en),  // input wire wr_en
.din         (w_fifo_adc1_a_din  ),  // input wire [15 : 0] din
.rd_en       (w_fifo_adc1_a_rd_en),  // input wire rd_en
.dout        (w_fifo_adc1_a_dout ),  // output wire [15 : 0] dout
.full        (w_fifo_adc1_a_full ),  // output wire full
.empty       (w_fifo_adc1_a_empty),  // output wire empty
.wr_ack      (                   ),  // output wire wr_ack
.valid       (                   )   // output wire valid
);
fifo_generator_6  fifo_adc1_b_inst (
.rst         (~reset_n | w_fifo_adcx_rst),  // input wire rst
.wr_clk      (w_fifo_adcx_wr_clk ),  // input wire wr_clk  // assume 12MHz
.rd_clk      (w_fifo_adcx_rd_clk ),  // input wire rd_clk
//
.wr_en       (w_fifo_adc1_b_wr_en),  // input wire wr_en
.din         (w_fifo_adc1_b_din  ),  // input wire [15 : 0] din
.rd_en       (w_fifo_adc1_b_rd_en),  // input wire rd_en
.dout        (w_fifo_adc1_b_dout ),  // output wire [15 : 0] dout
.full        (w_fifo_adc1_b_full ),  // output wire full
.empty       (w_fifo_adc1_b_empty),  // output wire empty
.wr_ack      (                   ),  // output wire wr_ack
.valid       (                   )   // output wire valid
);
fifo_generator_6  fifo_adc1_c_inst (
.rst         (~reset_n | w_fifo_adcx_rst),  // input wire rst
.wr_clk      (w_fifo_adcx_wr_clk ),  // input wire wr_clk  // assume 12MHz
.rd_clk      (w_fifo_adcx_rd_clk ),  // input wire rd_clk
//
.wr_en       (w_fifo_adc1_c_wr_en),  // input wire wr_en
.din         (w_fifo_adc1_c_din  ),  // input wire [15 : 0] din
.rd_en       (w_fifo_adc1_c_rd_en),  // input wire rd_en
.dout        (w_fifo_adc1_c_dout ),  // output wire [15 : 0] dout
.full        (w_fifo_adc1_c_full ),  // output wire full
.empty       (w_fifo_adc1_c_empty),  // output wire empty
.wr_ack      (                   ),  // output wire wr_ack
.valid       (                   )   // output wire valid
);
fifo_generator_6  fifo_adc1_d_inst (
.rst         (~reset_n | w_fifo_adcx_rst),  // input wire rst
.wr_clk      (w_fifo_adcx_wr_clk ),  // input wire wr_clk  // assume 12MHz
.rd_clk      (w_fifo_adcx_rd_clk ),  // input wire rd_clk
//
.wr_en       (w_fifo_adc1_d_wr_en),  // input wire wr_en
.din         (w_fifo_adc1_d_din  ),  // input wire [15 : 0] din
.rd_en       (w_fifo_adc1_d_rd_en),  // input wire rd_en
.dout        (w_fifo_adc1_d_dout ),  // output wire [15 : 0] dout
.full        (w_fifo_adc1_d_full ),  // output wire full
.empty       (w_fifo_adc1_d_empty),  // output wire empty
.wr_ack      (                   ),  // output wire wr_ack
.valid       (                   )   // output wire valid
);
// adc2
fifo_generator_6  fifo_adc2_a_inst (
.rst         (~reset_n | w_fifo_adcx_rst),  // input wire rst
.wr_clk      (w_fifo_adcx_wr_clk ),  // input wire wr_clk  // assume 12MHz
.rd_clk      (w_fifo_adcx_rd_clk ),  // input wire rd_clk
//
.wr_en       (w_fifo_adc2_a_wr_en),  // input wire wr_en
.din         (w_fifo_adc2_a_din  ),  // input wire [15 : 0] din
.rd_en       (w_fifo_adc2_a_rd_en),  // input wire rd_en
.dout        (w_fifo_adc2_a_dout ),  // output wire [15 : 0] dout
.full        (w_fifo_adc2_a_full ),  // output wire full
.empty       (w_fifo_adc2_a_empty),  // output wire empty
.wr_ack      (                   ),  // output wire wr_ack
.valid       (                   )   // output wire valid
);
fifo_generator_6  fifo_adc2_b_inst (
.rst         (~reset_n | w_fifo_adcx_rst),  // input wire rst
.wr_clk      (w_fifo_adcx_wr_clk ),  // input wire wr_clk  // assume 12MHz
.rd_clk      (w_fifo_adcx_rd_clk ),  // input wire rd_clk
//
.wr_en       (w_fifo_adc2_b_wr_en),  // input wire wr_en
.din         (w_fifo_adc2_b_din  ),  // input wire [15 : 0] din
.rd_en       (w_fifo_adc2_b_rd_en),  // input wire rd_en
.dout        (w_fifo_adc2_b_dout ),  // output wire [15 : 0] dout
.full        (w_fifo_adc2_b_full ),  // output wire full
.empty       (w_fifo_adc2_b_empty),  // output wire empty
.wr_ack      (                   ),  // output wire wr_ack
.valid       (                   )   // output wire valid
);
fifo_generator_6  fifo_adc2_c_inst (
.rst         (~reset_n | w_fifo_adcx_rst),  // input wire rst
.wr_clk      (w_fifo_adcx_wr_clk ),  // input wire wr_clk  // assume 12MHz
.rd_clk      (w_fifo_adcx_rd_clk ),  // input wire rd_clk
//
.wr_en       (w_fifo_adc2_c_wr_en),  // input wire wr_en
.din         (w_fifo_adc2_c_din  ),  // input wire [15 : 0] din
.rd_en       (w_fifo_adc2_c_rd_en),  // input wire rd_en
.dout        (w_fifo_adc2_c_dout ),  // output wire [15 : 0] dout
.full        (w_fifo_adc2_c_full ),  // output wire full
.empty       (w_fifo_adc2_c_empty),  // output wire empty
.wr_ack      (                   ),  // output wire wr_ack
.valid       (                   )   // output wire valid
);
fifo_generator_6  fifo_adc2_d_inst (
.rst         (~reset_n | w_fifo_adcx_rst),  // input wire rst
.wr_clk      (w_fifo_adcx_wr_clk ),  // input wire wr_clk  // assume 12MHz
.rd_clk      (w_fifo_adcx_rd_clk ),  // input wire rd_clk
//
.wr_en       (w_fifo_adc2_d_wr_en),  // input wire wr_en
.din         (w_fifo_adc2_d_din  ),  // input wire [15 : 0] din
.rd_en       (w_fifo_adc2_d_rd_en),  // input wire rd_en
.dout        (w_fifo_adc2_d_dout ),  // output wire [15 : 0] dout
.full        (w_fifo_adc2_d_full ),  // output wire full
.empty       (w_fifo_adc2_d_empty),  // output wire empty
.wr_ack      (                   ),  // output wire wr_ack
.valid       (                   )   // output wire valid
);
// adc3
fifo_generator_6  fifo_adc3_a_inst (
.rst         (~reset_n | w_fifo_adcx_rst),  // input wire rst
.wr_clk      (w_fifo_adcx_wr_clk ),  // input wire wr_clk  // assume 12MHz
.rd_clk      (w_fifo_adcx_rd_clk ),  // input wire rd_clk
//
.wr_en       (w_fifo_adc3_a_wr_en),  // input wire wr_en
.din         (w_fifo_adc3_a_din  ),  // input wire [15 : 0] din
.rd_en       (w_fifo_adc3_a_rd_en),  // input wire rd_en
.dout        (w_fifo_adc3_a_dout ),  // output wire [15 : 0] dout
.full        (w_fifo_adc3_a_full ),  // output wire full
.empty       (w_fifo_adc3_a_empty),  // output wire empty
.wr_ack      (                   ),  // output wire wr_ack
.valid       (                   )   // output wire valid
);
fifo_generator_6  fifo_adc3_b_inst (
.rst         (~reset_n | w_fifo_adcx_rst),  // input wire rst
.wr_clk      (w_fifo_adcx_wr_clk ),  // input wire wr_clk  // assume 12MHz
.rd_clk      (w_fifo_adcx_rd_clk ),  // input wire rd_clk
//
.wr_en       (w_fifo_adc3_b_wr_en),  // input wire wr_en
.din         (w_fifo_adc3_b_din  ),  // input wire [15 : 0] din
.rd_en       (w_fifo_adc3_b_rd_en),  // input wire rd_en
.dout        (w_fifo_adc3_b_dout ),  // output wire [15 : 0] dout
.full        (w_fifo_adc3_b_full ),  // output wire full
.empty       (w_fifo_adc3_b_empty),  // output wire empty
.wr_ack      (                   ),  // output wire wr_ack
.valid       (                   )   // output wire valid
);
fifo_generator_6  fifo_adc3_c_inst (
.rst         (~reset_n | w_fifo_adcx_rst),  // input wire rst
.wr_clk      (w_fifo_adcx_wr_clk ),  // input wire wr_clk  // assume 12MHz
.rd_clk      (w_fifo_adcx_rd_clk ),  // input wire rd_clk
//
.wr_en       (w_fifo_adc3_c_wr_en),  // input wire wr_en
.din         (w_fifo_adc3_c_din  ),  // input wire [15 : 0] din
.rd_en       (w_fifo_adc3_c_rd_en),  // input wire rd_en
.dout        (w_fifo_adc3_c_dout ),  // output wire [15 : 0] dout
.full        (w_fifo_adc3_c_full ),  // output wire full
.empty       (w_fifo_adc3_c_empty),  // output wire empty
.wr_ack      (                   ),  // output wire wr_ack
.valid       (                   )   // output wire valid
);
fifo_generator_6  fifo_adc3_d_inst (
.rst         (~reset_n | w_fifo_adcx_rst),  // input wire rst
.wr_clk      (w_fifo_adcx_wr_clk ),  // input wire wr_clk  // assume 12MHz
.rd_clk      (w_fifo_adcx_rd_clk ),  // input wire rd_clk
//
.wr_en       (w_fifo_adc3_d_wr_en),  // input wire wr_en
.din         (w_fifo_adc3_d_din  ),  // input wire [15 : 0] din
.rd_en       (w_fifo_adc3_d_rd_en),  // input wire rd_en
.dout        (w_fifo_adc3_d_dout ),  // output wire [15 : 0] dout
.full        (w_fifo_adc3_d_full ),  // output wire full
.empty       (w_fifo_adc3_d_empty),  // output wire empty
.wr_ack      (                   ),  // output wire wr_ack
.valid       (                   )   // output wire valid
);

//}

//// conversion and serial closk output //{
///////////////////////////////////
// trigger sampling 
(* keep = "true" *) reg [1:0] r_trig_conv_single; // trigger for one ADC sample
(* keep = "true" *) reg [1:0] r_trig_conv_run   ; // trigger for ADC samples 
//
always @(posedge b_clk, negedge reset_n)
	if (!reset_n) begin
		r_trig_conv_single <= 2'b0;
		r_trig_conv_run    <= 2'b0;
		end 
	else begin 
		r_trig_conv_single <= {r_trig_conv_single[0], i_trig_conv_single};
		r_trig_conv_run    <= {r_trig_conv_run   [0], i_trig_conv_run   };
		end
//
wire w_trig_conv_single_rise = (~r_trig_conv_single[1]) & ( r_trig_conv_single[0]);
wire w_trig_conv_run_rise    = (~r_trig_conv_run[1]   ) & ( r_trig_conv_run[0]   );


// parameters for conv running
wire [17:0]  w_count_period = {i_count_period_div4, 2'b0}; // sample period = i_count_period_div4*4  //based on 210MHz 
wire [17:0]  w_count_conv   = {i_count_conv_div4  , 2'b0}; // adc samples   = i_count_conv_div4*4    // < 2^17 
//
parameter MIN_COUNT_PERIOD = 18'd80     ; // for 2.4Msps // due to 192MHz/2.4MHz=80
parameter MAX_COUNT_PERIOD = 18'd192_000; // for 1.0ksps // due to 192MHz/1kHz  =192000
//parameter MAX_COUNT_PERIOD = 18'd120; // test
//
// coherency with 256 samples and 60/50Hz
//   192MHz/12500 = 15.36kHz,  15.36kHz/256=60Hz
//   192MHz/15000 =  12.8kHz,   12.8kHz/256=50Hz
parameter INIT_COUNT_PERIOD = 18'd12500;  // for 15.36ksps 
parameter INIT_COUNT_CONV   = 18'd256; 
//
parameter COUNT_ACTIVE_CNV_B = 18'd7; // CNV_B active width for 7 clocks
//
parameter MAX_IDX_FRAME = 8'd76; // max for r_idx_frame


// period count set
(* keep = "true" *) reg [17:0] r_cnt_period_set; //
//
always @(posedge b_clk, negedge reset_n)
	if (!reset_n) begin
		r_cnt_period_set <= INIT_COUNT_PERIOD; // for 15.36ksps 
		end 
	else begin 
		if (w_trig_conv_single_rise | w_trig_conv_run_rise)
			r_cnt_period_set <= (w_count_period==0               )? INIT_COUNT_PERIOD  : 
								(w_count_period< MIN_COUNT_PERIOD)? MIN_COUNT_PERIOD : 
								(w_count_period> MAX_COUNT_PERIOD)? MAX_COUNT_PERIOD : 
								 w_count_period;
		else 
			r_cnt_period_set <= r_cnt_period_set; //$$
		end

// conversion count set 
(* keep = "true" *) reg [17:0] r_cnt_CNV_set;
//
always @(posedge b_clk, negedge reset_n)
	if (!reset_n) begin
		r_cnt_CNV_set <= INIT_COUNT_CONV; 
		end 
	else begin 
		if (w_trig_conv_single_rise)
			r_cnt_CNV_set <= 18'd1; // min 1 conversions // 
		else if (w_trig_conv_run_rise)
			r_cnt_CNV_set <= (w_count_conv==0)? INIT_COUNT_CONV : w_count_conv;
		else 
			r_cnt_CNV_set <= r_cnt_CNV_set;
		end


// period count 
//(* keep = "true" *) 
reg [17:0] r_cnt_period; //
//
wire [17:0] w_cnt_CNV;
//
wire w_trig_cnt_period   = w_trig_conv_single_rise | w_trig_conv_run_rise;
wire w_reload_cnt_period = ((r_cnt_period >= r_cnt_period_set)&(w_cnt_CNV <  r_cnt_CNV_set))? 1'b1 : 1'b0;
wire w_stop_cnt_period   = ((r_cnt_period >= r_cnt_period_set)&(w_cnt_CNV >= r_cnt_CNV_set))? 1'b1 : 1'b0;
//
always @(posedge b_clk, negedge reset_n)
	if (!reset_n) begin
		r_cnt_period <= 18'd0;
		end 
	else begin 
		if (w_trig_cnt_period)
			r_cnt_period <= 18'd1;
		else if (w_stop_cnt_period) // stop
			r_cnt_period <= 18'd0;
		else if (w_reload_cnt_period) // reload 
			r_cnt_period <= 18'd1;
		else if (r_cnt_period > 0)
			r_cnt_period <= r_cnt_period + 1;
		else 
			r_cnt_period <= r_cnt_period;
		end


// frame index
(* keep = "true" *) reg [7:0] r_idx_frame; 
//
wire w_trig_idx_frame = w_trig_conv_single_rise | w_trig_conv_run_rise | (r_cnt_period == r_cnt_period_set);
wire w_stop_idx_frame = 1'b0;
//
always @(posedge b_clk, negedge reset_n)
	if (!reset_n) begin
		r_idx_frame <= 8'd0;
		end 
	else begin 
		if (w_trig_idx_frame)
			r_idx_frame <= 8'd1;
		else if (w_stop_idx_frame)
			r_idx_frame <= 8'd0;
		else if (r_idx_frame >= MAX_IDX_FRAME)
			if (r_cnt_period == 0)
				r_idx_frame <= 8'd0;
			else
				r_idx_frame <= r_idx_frame;
		else if (r_idx_frame > 0)
			r_idx_frame <= r_idx_frame + 1;
		else 
			r_idx_frame <= r_idx_frame;
		end
//
assign w_clear_cnt_SDO_bits = (r_idx_frame==1)? 1'b1 :  1'b0;


// count of converions 
(* keep = "true" *) reg [17:0] r_cnt_CNV;
//
always @(posedge b_clk, negedge reset_n)
	if (!reset_n) begin
		r_cnt_CNV <= 18'd0;
		end 
	else begin 
		if (r_cnt_period == 1)
			r_cnt_CNV <= r_cnt_CNV + 1;
		//else if (r_cnt_period == 0)
		else if (r_idx_frame == 0)
			r_cnt_CNV <= 18'd0;
		else 
			r_cnt_CNV <= r_cnt_CNV;
		end
//
assign w_cnt_CNV = r_cnt_CNV;


// conversion signal 
assign o_CNV_B  =  ((r_idx_frame>0) & (r_idx_frame<=COUNT_ACTIVE_CNV_B))? 1'b1 :  1'b0;


// busy output 
(* keep = "true" *) reg [8:0] r_busy; // 6 delay // 9 delay by debugger check 
wire      w_busy  =  (r_idx_frame>0)? 1'b1 :  1'b0;
//
assign o_busy = w_busy | r_busy[8];
//
always @(posedge b_clk, negedge reset_n)
	if (!reset_n) begin
		r_busy <= 9'b0;
		end 
	else begin 
		r_busy <= {r_busy[7:0], w_busy};
		end
//
wire w_busy_pclk__pre;
wire w_busy_pclk;
//
signal_ext_bclk_pclk  signal_ext_bclk_pclk__inst_busy (
	.b_clk   (b_clk   ),
	.reset_n (reset_n ),
	.p_clk   (p_clk   ),
	.i_sig_b (o_busy),
	.o_sig_b (w_busy_pclk__pre), 
	.o_sig_p (w_busy_pclk) 
);
//
assign o_busy_pclk = w_busy_pclk | w_busy_pclk__pre;

// done output 
(* keep = "true" *) reg r_done_to;
//
assign o_done_to = r_done_to;
//
always @(posedge b_clk, negedge reset_n)
	if (!reset_n) begin
		r_done_to <= 1'b0;
		end 
	else begin 
		r_done_to <= ( r_busy[7]) & (~r_busy[6]); // fall edge
		end
//
signal_ext_bclk_pclk  signal_ext_bclk_pclk__inst_done_to (
	.b_clk   (b_clk   ),
	.reset_n (reset_n ),
	.p_clk   (p_clk   ),
	.i_sig_b (o_done_to),
	.o_sig_b (), 
	.o_sig_p (o_done_to_pclk) 
);


// o_SCK
reg [3:0] sr_SCK;
//
parameter IDX_SCK_START = 8'd42; // 43  // 44-NG with 80
parameter IDX_SCK_STOP  = 8'd74; // +32
//
always @(posedge b_clk, negedge reset_n)
	if (!reset_n) begin
		sr_SCK <= 4'b0011;
		end 
	else begin 
		if (r_idx_frame == 1)
			sr_SCK <= 4'b0011;
		else if ( (r_idx_frame >= IDX_SCK_START) & (r_idx_frame < IDX_SCK_STOP) )
			sr_SCK <= {sr_SCK[2:0], sr_SCK[3]};
		else 
			sr_SCK <= sr_SCK;
		end
//
assign o_SCK  =  sr_SCK[3];

//}

//}
endmodule
