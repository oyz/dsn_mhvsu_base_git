//------------------------------------------------------------------------
// test_model_adc_ddr_LTC2325.v
//   test model for LTC2325-16 ADC DDR 
//   	http://www.analog.com/media/en/technical-documentation/data-sheets/232516fa.pdf
//
//   === parameters (210MHz) ===
//   logic base freq                                      : 210MHz or 4.76190476 nanoseconds (b_clk)
//   ADC data configuration                               : DDR mode, LVDS
//   multi-channel                                        : 4EA 
//   conversion period (tCYC)                             : min 0.2us ~ max 1000us, or 5Msps~1ksps
//   convserion_bar high time (tCNVH)                     : min 30ns          // b_clk*7=33.33ns
//   Conversion Time (tCONV)                              : max 170ns         // b_clk*36=171.43ns
//   wakeup time from sleep more (tWAKE)                  : 50ms
//   SLK Period for DDR  (tSCK)                           : min 18.2ns        // b_clk*4=19.05ns
//   SCK High Time                                        : min 8.2ns         // b_clk*2=9.52ns
//   SCK Low Time                                         : min 8.2ns         // b_clk*2=9.52ns
//   SDO Data Remains Valid Delay from CLKOUT (tHSDO_DDR) : max 1.5ns         // b_clk*1=4.76ns
//   SCK to CLKOUT Delay (tDSCKCLKOUT)                    : min 2ns ~ max 4ns // b_clk*1=4.76ns
//   SCK Delay Time to CNV_bar rise  (tDSCKHCNVH)         : min 0ns           // b_clk*2=9.52ns
//
//   * note : board delay measure ... SCK between DCO ... 4 clocks based on 210MHz ... ~ 20ns
//   * note : one-cycle conversion latency ... first adc data is invalid (say garbage).
//
//   === revised parameters (192MHz) ===
//   logic base freq                                      : 192MHz or 5.20833333 nanoseconds (b_clk)
//   ADC data configuration                               : DDR mode, LVDS
//   multi-channel                                        : 4EA 
//   conversion period (tCYC)                             : min 0.2us ~ max 1000us, or 5Msps~1ksps
//   convserion_bar high time (tCNVH)                     : min 30ns          // b_clk*7=36.4583333ns
//   Conversion Time (tCONV)                              : max 170ns         // b_clk*36=187.5ns
//   wakeup time from sleep more (tWAKE)                  : 50ms
//   SLK Period for DDR  (tSCK)                           : min 18.2ns        // b_clk*4=20.8333333ns
//   SCK High Time                                        : min 8.2ns         // b_clk*2=10.4166667ns
//   SCK Low Time                                         : min 8.2ns         // b_clk*2=10.4166667ns
//   SDO Data Remains Valid Delay from CLKOUT (tHSDO_DDR) : max 1.5ns         // b_clk*1=5.20833333ns
//   SCK to CLKOUT Delay (tDSCKCLKOUT)                    : min 2ns ~ max 4ns // b_clk*1=5.20833333ns
//   SCK Delay Time to CNV_bar rise  (tDSCKHCNVH)         : min 0ns           // b_clk*2=10.4166667ns
//
//   * note : board delay measure ... SCK between DCO ... 4 clocks based on 210MHz ... ~ 20ns
//   * note : one-cycle conversion latency ... first adc data is invalid (say garbage).
//
//
//
//   === timing (rough) ===
//   conversion_bar signal    : i_CNV_B
//   serial clock input       : i_SCK
//   serial clock output      : o_CLKOUT
//   serial data  output[3:0] : o_SDO[3:0]
//   ----
//   index_H        0000000000111111111122222222223333333333444444444455555555556666666666777777777
//   index_L        0123456789012345678901234567890123456789012345678901234567890123456789012345678
//   r_idx_cnv_H     000000000011111111112222222222333333333344444444445555555555666666666677777770
//   r_idx_cnv_L     012345678901234567890123456789012345678901234567890123456789012345678901234560
//   i_CNV_B     ____-------______________________________________________________________________-------__
//   r_conv_busy ___________------------------------------------_________________________________________--
//                          <-- conversion.................. -->
//   i_SCK       _________________________________________________--__--__--__--__--__--__--__--___________ 
//   o_CLKOUT    __________________________________________________--__--__--__--__--__--__--__--__________
//   o_SDO[3:0]  ___________xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxDDddDDddDDddDDddDDddDDddDDddDDddxx________
//   r_sdo_valid ________________________________________________--------------------------------__________
//   o_CLKOUT_BD _____________________________________________________--__--__--__--__--__--__--__--_______
//
//   note: o_CLKOUT_BD is considered for board delay bclk*4.
//   note: 77 b_clk for most fast cycle ... b_clk*77=366.666667ns or 2.727Msps with b_clk=210MHz
//   note: 77 b_clk for most fast cycle ... b_clk*77=401.041667ns or 2.494Msps with b_clk=192MHz
//
//         for 5Msps or 200ns period, 170ns + 30ns = 200ns... 30ns // b_clk*42=200ns 
//         r_conv_busy is not secured...
//         for safe operation, 
//            [210MHz                                        ] [192MHz                                        ]
//             b_clk* 84	= 400		ns or 2.5   Msps (max)  b_clk* 80	 = 416.666667ns or 2.4    Msps (max)
//             b_clk*105	= 500		ns or 2.0   Msps        b_clk* 96	 = 500		 ns or 2.0    Msps 
//             b_clk*210	= 1000		ns or 1.0   Msps        b_clk*192	 = 1000		 ns or 1.0    Msps
//             b_clk*420	= 2000		ns or 0.5   Msps        b_clk*384	 = 2000		 ns or 0.5    Msps
//             b_clk*2100	= 10000		ns or 0.1   Msps        b_clk*1920	 = 10000	 ns or 0.1    Msps
//             b_clk*4200	= 20000		ns or 0.05  Msps        b_clk*3840	 = 20000	 ns or 0.05   Msps
//                                                              b_clk*12500  = 65104.1667ns or 0.01536Msps (coherent 60Hz 256samples)
//             b_clk*21000	= 100000	ns or 0.01  Msps        b_clk*19200	 = 100000	 ns or 0.01   Msps
//             b_clk*42000	= 200000	ns or 0.005 Msps        b_clk*38400	 = 200000	 ns or 0.005  Msps
//             b_clk*210000	= 1000000	ns or 0.001 Msps (min)  b_clk*192000 = 1000000	 ns or 0.001  Msps (min)
//             log_2(210000)=17.68                              log_2(192000)=17.55
//
//   note: 10ns based control looks available. 100MHz vs 104MHz ... 10ns vs 9.62ns
//
//   === IO ports ===
//   i_CNV_B
//   i_SCK
//   o_CLKOUT
//   o_SDO[3:0]
//
//------------------------------------------------------------------------

`timescale 1ns / 1ps
module test_model_adc_ddr_LTC2325 (  
	input  wire b_clk, // 210MHz vs 192MHz
	input  wire reset_n,
	
	input  wire i_CNV_B    ,
	input  wire i_SCK      ,
	output wire o_CLKOUT   ,
	output wire [3:0] o_SDO
);

///////////////////////////////////////

//// input sampling //{
reg [1:0] r_CNV_B;
reg [1:0] r_SCK;
//
always @(posedge b_clk, negedge reset_n)
	if (!reset_n) begin
		r_CNV_B <= 2'b0;
		r_SCK   <= 2'b0;
		end 
	else begin 
		r_CNV_B <= {r_CNV_B[0], i_CNV_B};
		r_SCK   <= {r_SCK  [0], i_SCK  };
		end
//
wire w_CNV_B_rise = (~r_CNV_B[1]) & ( r_CNV_B[0]);
wire w_CNV_B_fall = ( r_CNV_B[1]) & (~r_CNV_B[0]);
wire w_SCK_rise   = (~r_SCK  [1]) & ( r_SCK  [0]);
wire w_SCK_fall   = ( r_SCK  [1]) & (~r_SCK  [0]);
//}

//// index for conversion //{
reg [17:0] r_idx_cnv;
//
always @(posedge b_clk, negedge reset_n)
	if (!reset_n) begin
		r_idx_cnv <= 18'b0;
		end 
	else begin
		if (w_CNV_B_rise)
			r_idx_cnv <= 1; // start to increase
		else if (r_idx_cnv > 0) 
			r_idx_cnv <= r_idx_cnv + 1;
		else
			r_idx_cnv <= r_idx_cnv; // stay
		end
//}

//// r_conv_busy //{
reg r_conv_busy;
parameter IDX_CNV_DONE = 42;
wire w_CNV_DONE = (r_idx_cnv >= IDX_CNV_DONE)? 1'b1 : 1'b0;
//
always @(posedge b_clk, negedge reset_n)
	if (!reset_n) begin
		r_conv_busy <= 1'b0;
		end 
	else begin
		if (w_CNV_B_fall)
			r_conv_busy <= 1'b1; 
		else if (w_CNV_DONE) 
			r_conv_busy <= 1'b0;
		else
			r_conv_busy <= r_conv_busy; // stay
		end
//}

//// r_sdo_valid //{
reg r_sdo_valid;
//parameter IDX_SDO_DONE = 75;
wire w_SDO_DONE;
//
always @(posedge b_clk, negedge reset_n)
	if (!reset_n) begin
		r_sdo_valid <= 1'b0;
		end 
	else begin
		if (r_conv_busy & w_CNV_DONE)
			r_sdo_valid <= 1'b1; 
		//else if (r_idx_cnv >= IDX_SDO_DONE) 
		else if (w_SDO_DONE) 
			r_sdo_valid <= 1'b0;
		else
			r_sdo_valid <= r_sdo_valid; // stay
		end
//}

//// set delay 
parameter DELAY = 10;

//// o_CLKOUT //{
reg r_CLKOUT;
wire w_trig_CNV_DONE = (r_idx_cnv == IDX_CNV_DONE)? 1'b1 : 1'b0;
//assign o_CLKOUT = r_SCK;
assign #DELAY o_CLKOUT = r_CLKOUT;
//
always @(posedge b_clk, negedge reset_n)
	if (!reset_n) begin
		r_CLKOUT  <=  1'b0;
		end 
	else begin
		if (r_conv_busy & w_trig_CNV_DONE) begin 
			r_CLKOUT <=  1'b0;
			end
		else if (w_SCK_rise) begin 
			r_CLKOUT <=  1'b1;
			end
		else if (w_SCK_fall) begin
			r_CLKOUT <=  1'b0;
			end
		else begin
			r_CLKOUT <=  r_CLKOUT;
			end
		end
//}

//// data pattern //{

reg [15:0] test_pttn [0:7];
initial begin
	test_pttn[0] = 16'h0010;
	test_pttn[1] = 16'h0100;
	test_pttn[2] = 16'h0010;
	test_pttn[3] = 16'h0001;
	test_pttn[4] = 16'hFFFF;
	test_pttn[5] = 16'hFFF0;
	test_pttn[6] = 16'hFF00;
	test_pttn[7] = 16'hFFF0;
	end

//}

//// sr_SDO //{
reg [15:0] sr_SDO;
reg [2:0]  idx_sr_SDO;
reg [5:0]  r_cnt_SDO;
//
always @(posedge b_clk, negedge reset_n)
	if (!reset_n) begin
		sr_SDO     <= 16'hA3C5; // initial pattern
		idx_sr_SDO <=  3'b0;
		r_cnt_SDO  <=  6'b0;
		end 
	else begin
		if (r_conv_busy & w_trig_CNV_DONE) begin // load new data
			//sr_SDO     <= sr_SDO + 1; // increasing pattern
			sr_SDO     <= test_pttn[idx_sr_SDO]; // load test pattern
			r_cnt_SDO  <=  6'b0;
			idx_sr_SDO <= idx_sr_SDO + 1;
			end
		else if (w_SCK_rise) begin 
			sr_SDO     <= {sr_SDO[14:0], sr_SDO[15]};
			r_cnt_SDO  <=  r_cnt_SDO + 1;
			end
		else if (w_SCK_fall) begin
			sr_SDO     <= {sr_SDO[14:0], sr_SDO[15]};
			r_cnt_SDO  <=  r_cnt_SDO + 1;
			end
		else begin
			sr_SDO     <= sr_SDO; // stay
			r_cnt_SDO  <=  r_cnt_SDO;
			end
		end
//
assign w_SDO_DONE = (r_cnt_SDO>=16)? 1'b1 : 1'b0;
//}

//// o_SDO //{
assign #DELAY  o_SDO[0] = sr_SDO[15];
assign #DELAY  o_SDO[1] = sr_SDO[15];
assign #DELAY  o_SDO[2] = sr_SDO[15];
assign #DELAY  o_SDO[3] = sr_SDO[15];
//}

///////////////////////////////////////

endmodule
