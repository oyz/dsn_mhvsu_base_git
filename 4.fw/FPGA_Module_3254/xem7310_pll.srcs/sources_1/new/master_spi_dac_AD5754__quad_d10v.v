//------------------------------------------------------------------------
// master_spi_dac_AD5754__quad_d10v.v
//  for AD5754 Quad, 16-Bit, Serial Input, Unipolar/Bipolar Voltage Output DAC
//  	http://www.analog.com/media/en/technical-documentation/data-sheets/AD5724_5734_5754.pdf
//
//  wrapper for master_spi_dac_AD5754__range_con.v
//
//  update DAC value from port
//  readback DAC value to port
//  range control for each port
//  
//  - IO
//  	GPO2_ISO CLR_DAC_B
//  	GPO3_ISO B2C_DAC
//  	GPO4_ISO LOAD_DAC_B 
//  	BIAS_SYNCn_ISO SYNC_B
//  	BIAS_SCLK_ISO SCLK
//  	BIAS_SDI_ISO SDIN
//  	BIAS_SDO_ISO SDO
//
//  - reg 
//  	CON = {conf[15:0], 8'b0, test_mode[3:0], test,update,init,en}
//  	FLAG = {... , test_done,update_done,init_done,en}
//  	DATAIN
//  	DATAOUT
//
//  - serial data format or input shift register
//  	DB23= R/W_B
//  	DB22= 0
//  	DB[21:19]= REG[2:0]
//  	DB[18:16]= A[2:0]
//  	DB[15:0]= D[15:0]
//
//  - timing 
//  	SCLK 33ns min 1/33ns > 30MHz
//  	base clock: clk_out4_125M vs clk_out3_10M
//  		100ns base timing
//  	DAC settling time 10us ~ 100kHz
//  	...
//  	<write mode>
//  	        555544444444443333333333222222222211111111110000000000
//  	        321098765432109876543210987654321098765432109876543210
//  	SCLK    ---_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_----
//  	SYNCB   --________________________________________________----
//  	SDIN    XXDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBXXXX 
//  	          2 2 2 2 1 1 1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 0 0 0
//  	          3 2 1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0
//  	LDACB   ----------------------------------------------------_-
//  	CLRB    -_----------------------------------------------------
//  	...
//  	54 ticks
//  		100ns * 54 = 5.4us
//  	CLRB anytime for 1 tick.
//  	....
//  	<readback mode>
//  	        555544444444443333333333222222222211111111110000000000
//  	        321098765432109876543210987654321098765432109876543210
//  	SCLK    ---_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_----
//  	SYNCB   --________________________________________________----
//  	SDIN    XXDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBXXXX 
//  	          2 2 2 2 1 1 1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 0 0 0
//  	          3 2 1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0
//  	SDO     XXDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBXXXX 
//  	          2 2 2 2 1 1 1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 0 0 0
//  	          3 2 1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0
//  			  
//------------------------------------------------------------------------

`timescale 1ns / 1ps
module master_spi_dac_AD5754__quad_d10v (  
	input  wire clk, // assume 10MHz or 100ns
	input  wire reset_n,

	// trig control
	input  wire i_trig_SPI_frame    ,
	output wire o_done_SPI_frame    ,
	output wire o_done_SPI_frame_TO ,
		
	input  wire i_trig_DAC_init     , 
	output wire o_done_DAC_init     , //$$ later to consider 10us power up delay : 100 count based on 10MHz
	output wire o_done_DAC_init_TO  ,
	
	input  wire i_trig_DAC_update   ,
	output wire o_done_DAC_update   ,
	output wire o_done_DAC_update_TO,
	
	output wire o_busy_DAC_update   ,

	output wire [15:0] o_cnt_dac_trig, //$$ count all trig based on done

	// DAC load control
	input  wire i_load_packet_en      , // load packet enable
	input  wire i_load_pin_ext_sig_en , // load pin control by external signal 
	input  wire i_load_pin_ext_sig    , // external signal for load pin
	
	// DAC power delay disable 
	input  wire i_power_up_delay_disable, // power up delay 10us or 100 count on 10MHz

	// DAC selection
	input  wire [3:0] i_DAC_sel       , // [3:0] // i_DAC_sel[3:0] for DAC3,DAC2,DAC1,DAC0

	// test frame 24-bit
	input  wire [23:0] i_test_sdi_pdata	, // input // [23:0]
	output wire [23:0] o_test_sdo_pdata	, // out   // [23:0]

	// DAC data in/out //{
	input  wire[15:0] i_DAC0_A_val  , // [15:0] // DAC value in
	input  wire[15:0] i_DAC0_B_val  , // [15:0] // DAC value in
	input  wire[15:0] i_DAC0_C_val  , // [15:0] // DAC value in
	input  wire[15:0] i_DAC0_D_val  , // [15:0] // DAC value in
								    
	input  wire[15:0] i_DAC1_A_val  , // [15:0] // DAC value in
	input  wire[15:0] i_DAC1_B_val  , // [15:0] // DAC value in
	input  wire[15:0] i_DAC1_C_val  , // [15:0] // DAC value in
	input  wire[15:0] i_DAC1_D_val  , // [15:0] // DAC value in
								    
	input  wire[15:0] i_DAC2_A_val  , // [15:0] // DAC value in
	input  wire[15:0] i_DAC2_B_val  , // [15:0] // DAC value in
	input  wire[15:0] i_DAC2_C_val  , // [15:0] // DAC value in
	input  wire[15:0] i_DAC2_D_val  , // [15:0] // DAC value in
								    
	input  wire[15:0] i_DAC3_A_val  , // [15:0] // DAC value in
	input  wire[15:0] i_DAC3_B_val  , // [15:0] // DAC value in
	input  wire[15:0] i_DAC3_C_val  , // [15:0] // DAC value in
	input  wire[15:0] i_DAC3_D_val  , // [15:0] // DAC value in
	//
	output wire[15:0] o_DAC0_A_rbk  , // [15:0]  // DAC readback out
	output wire[15:0] o_DAC0_B_rbk  , // [15:0]  // DAC readback out
	output wire[15:0] o_DAC0_C_rbk  , // [15:0]  // DAC readback out
	output wire[15:0] o_DAC0_D_rbk  , // [15:0]  // DAC readback out
								    
	output wire[15:0] o_DAC1_A_rbk  , // [15:0]  // DAC readback out
	output wire[15:0] o_DAC1_B_rbk  , // [15:0]  // DAC readback out
	output wire[15:0] o_DAC1_C_rbk  , // [15:0]  // DAC readback out
	output wire[15:0] o_DAC1_D_rbk  , // [15:0]  // DAC readback out
								    
	output wire[15:0] o_DAC2_A_rbk  , // [15:0]  // DAC readback out
	output wire[15:0] o_DAC2_B_rbk  , // [15:0]  // DAC readback out
	output wire[15:0] o_DAC2_C_rbk  , // [15:0]  // DAC readback out
	output wire[15:0] o_DAC2_D_rbk  , // [15:0]  // DAC readback out
								    
	output wire[15:0] o_DAC3_A_rbk  , // [15:0]  // DAC readback out
	output wire[15:0] o_DAC3_B_rbk  , // [15:0]  // DAC readback out
	output wire[15:0] o_DAC3_C_rbk  , // [15:0]  // DAC readback out
	output wire[15:0] o_DAC3_D_rbk  , // [15:0]  // DAC readback out
	//}
	
	// DAC status //{
	// 
	// POWER CONTROL REGISTER --> power up, thermal shutdown alert, overcurrent alert
	// CONTROL REGISTER --> {TSD_en, Clamp_en, CLR_sel, SDO_dis}
	//
	// DACn_status[3:0] = {PU_D,PU_C,PU_B,PU_A} // power up
	// DACn_status[5] = TSD // thermal shutdown alert
	// DACn_status[10:7] = {OC_D,OC_C,OC_B,OC_A} // overcurrent alert
	// DACn_status[15:12] = {TSD_en, Clamp_en, CLR_sel, SDO_dis}
	
	// output wire [15:0] o_DAC0_status , // DAC status 
	// output wire [15:0] o_DAC1_status , // DAC status 
	// output wire [15:0] o_DAC2_status , // DAC status 
	// output wire [15:0] o_DAC3_status , // DAC status 
	
	//}

	// DAC control pins // quad
	output wire [3:0] o_SCLK	    , // [3:0]
	output wire [3:0] o_SYNC_N	    , // [3:0]
	output wire [3:0] o_DIN		    , // [3:0]
	input  wire [3:0] i_SDO		    , // [3:0]
	
	// IO
	output wire o_LOAD_DAC_N	    , // ext pin

	// flag
	output wire valid
);

// valid 
(* keep = "true" *) reg r_valid;
assign valid = r_valid;
//
always @(posedge clk, negedge reset_n)
	if (!reset_n) begin
		r_valid <= 1'b0;
	end
	else begin
		r_valid <= 1'b1;
	end


//// call sub-module

wire [31:0] w_sub_test_sdi_pdata = {8'h00, i_test_sdi_pdata};
wire [31:0] w_sub_0_test_sdo_pdata ;
wire [31:0] w_sub_1_test_sdo_pdata ;
wire [31:0] w_sub_2_test_sdo_pdata ;
wire [31:0] w_sub_3_test_sdo_pdata ;
//
assign o_test_sdo_pdata = (i_DAC_sel[0]) ? w_sub_0_test_sdo_pdata[23:0] :
                          (i_DAC_sel[1]) ? w_sub_1_test_sdo_pdata[23:0] :
                          (i_DAC_sel[2]) ? w_sub_2_test_sdo_pdata[23:0] :
                          (i_DAC_sel[3]) ? w_sub_3_test_sdo_pdata[23:0] :
                          24'b0 ;
//
wire w_sub_0_init_done   ;
wire w_sub_0_update_done ;
wire w_sub_0_test_done   ;
wire w_sub_1_init_done   ;
wire w_sub_1_update_done ;
wire w_sub_1_test_done   ;
wire w_sub_2_init_done   ;
wire w_sub_2_update_done ;
wire w_sub_2_test_done   ;
wire w_sub_3_init_done   ;
wire w_sub_3_update_done ;
wire w_sub_3_test_done   ;
//
wire w_done_SPI_frame  ;
wire w_done_DAC_init   ;
wire w_done_DAC_update ;

// power up delay 
parameter CNT_POWER_UP_DELAY     = 8'd100; // 10us on 10MHz
reg [7:0] r_cnt_power_up_delay;
reg r_delay_done_DAC_init;
//
always @(posedge clk, negedge reset_n)
	if (!reset_n) begin
		r_delay_done_DAC_init      <= 1'b0;
		r_cnt_power_up_delay <= 8'b0;
		end
	else begin
		//
		if (w_done_DAC_init==0) 
			r_cnt_power_up_delay <= 8'b0;
		else if (r_cnt_power_up_delay < CNT_POWER_UP_DELAY)
			r_cnt_power_up_delay <= r_cnt_power_up_delay + 1;
		//
		if (w_done_DAC_init==0) 
			r_delay_done_DAC_init <= 1'b0;
		else if (i_power_up_delay_disable)
			r_delay_done_DAC_init <= 1'b1;
		else if (r_cnt_power_up_delay == CNT_POWER_UP_DELAY)
			r_delay_done_DAC_init <= 1'b1;
		//
		end


//
assign o_done_SPI_frame  = w_done_SPI_frame  ;
assign o_done_DAC_init   = r_delay_done_DAC_init   ;
assign o_done_DAC_update = w_done_DAC_update ;
//
assign w_done_SPI_frame  = (w_sub_0_test_done | (~i_DAC_sel[0]) ) &
                           (w_sub_1_test_done | (~i_DAC_sel[1]) ) &
                           (w_sub_2_test_done | (~i_DAC_sel[2]) ) &
                           (w_sub_3_test_done | (~i_DAC_sel[3]) ) ;
//
assign w_done_DAC_init   = (w_sub_0_init_done | (~i_DAC_sel[0]) ) &
                           (w_sub_1_init_done | (~i_DAC_sel[1]) ) &
                           (w_sub_2_init_done | (~i_DAC_sel[2]) ) &
                           (w_sub_3_init_done | (~i_DAC_sel[3]) ) ;
//
assign w_done_DAC_update = (w_sub_0_update_done | (~i_DAC_sel[0]) ) &
                           (w_sub_1_update_done | (~i_DAC_sel[1]) ) &
                           (w_sub_2_update_done | (~i_DAC_sel[2]) ) &
                           (w_sub_3_update_done | (~i_DAC_sel[3]) ) ;


//
wire w_sub_0_sclk   ;
wire w_sub_0_sync_n ;
wire w_sub_0_sdi    ;
wire w_sub_0_sdo    = i_SDO[0];
assign o_SCLK  [0]  = w_sub_0_sclk   &   i_DAC_sel[0] ; // enabled by i_DAC_sel
assign o_SYNC_N[0]  = w_sub_0_sync_n | (~i_DAC_sel[0]); // enabled by i_DAC_sel
assign o_DIN   [0]  = w_sub_0_sdi    &   i_DAC_sel[0] ; // enabled by i_DAC_sel
//
wire w_sub_1_sclk   ;
wire w_sub_1_sync_n ;
wire w_sub_1_sdi    ;
wire w_sub_1_sdo    = i_SDO[1];
assign o_SCLK  [1]  = w_sub_1_sclk   &   i_DAC_sel[1] ; // enabled by i_DAC_sel
assign o_SYNC_N[1]  = w_sub_1_sync_n | (~i_DAC_sel[1]); // enabled by i_DAC_sel
assign o_DIN   [1]  = w_sub_1_sdi    &   i_DAC_sel[1] ; // enabled by i_DAC_sel
//
wire w_sub_2_sclk   ;
wire w_sub_2_sync_n ;
wire w_sub_2_sdi    ;
wire w_sub_2_sdo    = i_SDO[2];
assign o_SCLK  [2]  = w_sub_2_sclk   &   i_DAC_sel[2] ; // enabled by i_DAC_sel
assign o_SYNC_N[2]  = w_sub_2_sync_n | (~i_DAC_sel[2]); // enabled by i_DAC_sel
assign o_DIN   [2]  = w_sub_2_sdi    &   i_DAC_sel[2] ; // enabled by i_DAC_sel
//
wire w_sub_3_sclk   ;
wire w_sub_3_sync_n ;
wire w_sub_3_sdi    ;
wire w_sub_3_sdo    = i_SDO[3];
assign o_SCLK  [3]  = w_sub_3_sclk   &   i_DAC_sel[3] ; // enabled by i_DAC_sel
assign o_SYNC_N[3]  = w_sub_3_sync_n | (~i_DAC_sel[3]); // enabled by i_DAC_sel
assign o_DIN   [3]  = w_sub_3_sdi    &   i_DAC_sel[3] ; // enabled by i_DAC_sel
//
wire [15:0] w_sub_0_DAC_VALUE_IN_A =  i_DAC0_A_val; 
wire [15:0] w_sub_0_DAC_VALUE_IN_B =  i_DAC0_B_val; 
wire [15:0] w_sub_0_DAC_VALUE_IN_C =  i_DAC0_C_val; 
wire [15:0] w_sub_0_DAC_VALUE_IN_D =  i_DAC0_D_val; 
wire [15:0] w_sub_0_DAC_READBACK_A ; 
wire [15:0] w_sub_0_DAC_READBACK_B ; 
wire [15:0] w_sub_0_DAC_READBACK_C ; 
wire [15:0] w_sub_0_DAC_READBACK_D ; 
assign       o_DAC0_A_rbk          = w_sub_0_DAC_READBACK_A ;
assign       o_DAC0_B_rbk          = w_sub_0_DAC_READBACK_B ;
assign       o_DAC0_C_rbk          = w_sub_0_DAC_READBACK_C ;
assign       o_DAC0_D_rbk          = w_sub_0_DAC_READBACK_D ;
//
wire [15:0] w_sub_1_DAC_VALUE_IN_A =  i_DAC1_A_val; 
wire [15:0] w_sub_1_DAC_VALUE_IN_B =  i_DAC1_B_val; 
wire [15:0] w_sub_1_DAC_VALUE_IN_C =  i_DAC1_C_val; 
wire [15:0] w_sub_1_DAC_VALUE_IN_D =  i_DAC1_D_val; 
wire [15:0] w_sub_1_DAC_READBACK_A ; 
wire [15:0] w_sub_1_DAC_READBACK_B ; 
wire [15:0] w_sub_1_DAC_READBACK_C ; 
wire [15:0] w_sub_1_DAC_READBACK_D ; 
assign       o_DAC1_A_rbk          = w_sub_1_DAC_READBACK_A ;
assign       o_DAC1_B_rbk          = w_sub_1_DAC_READBACK_B ;
assign       o_DAC1_C_rbk          = w_sub_1_DAC_READBACK_C ;
assign       o_DAC1_D_rbk          = w_sub_1_DAC_READBACK_D ;
//
wire [15:0] w_sub_2_DAC_VALUE_IN_A =  i_DAC2_A_val; 
wire [15:0] w_sub_2_DAC_VALUE_IN_B =  i_DAC2_B_val; 
wire [15:0] w_sub_2_DAC_VALUE_IN_C =  i_DAC2_C_val; 
wire [15:0] w_sub_2_DAC_VALUE_IN_D =  i_DAC2_D_val; 
wire [15:0] w_sub_2_DAC_READBACK_A ; 
wire [15:0] w_sub_2_DAC_READBACK_B ; 
wire [15:0] w_sub_2_DAC_READBACK_C ; 
wire [15:0] w_sub_2_DAC_READBACK_D ; 
assign       o_DAC2_A_rbk          = w_sub_2_DAC_READBACK_A ;
assign       o_DAC2_B_rbk          = w_sub_2_DAC_READBACK_B ;
assign       o_DAC2_C_rbk          = w_sub_2_DAC_READBACK_C ;
assign       o_DAC2_D_rbk          = w_sub_2_DAC_READBACK_D ;
//
wire [15:0] w_sub_3_DAC_VALUE_IN_A =  i_DAC3_A_val; 
wire [15:0] w_sub_3_DAC_VALUE_IN_B =  i_DAC3_B_val; 
wire [15:0] w_sub_3_DAC_VALUE_IN_C =  i_DAC3_C_val; 
wire [15:0] w_sub_3_DAC_VALUE_IN_D =  i_DAC3_D_val; 
wire [15:0] w_sub_3_DAC_READBACK_A ; 
wire [15:0] w_sub_3_DAC_READBACK_B ; 
wire [15:0] w_sub_3_DAC_READBACK_C ; 
wire [15:0] w_sub_3_DAC_READBACK_D ; 
assign       o_DAC3_A_rbk          = w_sub_3_DAC_READBACK_A ;
assign       o_DAC3_B_rbk          = w_sub_3_DAC_READBACK_B ;
assign       o_DAC3_C_rbk          = w_sub_3_DAC_READBACK_C ;
assign       o_DAC3_D_rbk          = w_sub_3_DAC_READBACK_D ;
//
wire w_sub_0_load_n ;
wire w_sub_1_load_n ;
wire w_sub_2_load_n ;
wire w_sub_3_load_n ;
//
wire w_LOAD_DAC          = ( (~w_sub_0_load_n) & (i_DAC_sel[0]) ) |
                           ( (~w_sub_1_load_n) & (i_DAC_sel[1]) ) |
                           ( (~w_sub_2_load_n) & (i_DAC_sel[2]) ) |
                           ( (~w_sub_3_load_n) & (i_DAC_sel[3]) ) ;
//
assign o_LOAD_DAC_N = ~w_LOAD_DAC ;
//
master_spi_dac_AD5754__range_con  master_spi_dac_AD5754_core_0_inst(
	.clk		(clk), // assume 10MHz or 100ns
	.reset_n	(reset_n),
	.en			(r_valid & i_DAC_sel[0] ), // delayed enable 

	// configuration : all +/-10V ranges; and force not to use clear and load.
	//   conf[15:0] = {
	//   	D_range_opt[1:0], C_range_opt[1:0], B_range_opt[1:0], A_range_opt[1:0],
	//   	4'b0, FORCE, B2C_DAC, LOAD_DAC_N, CLR_DAC_N}
	//.conf		(16'hFF_0B), //$$ all, RNG_SEL_D10_0004 // fixed load_n pin
	.conf		(16'hFF_03), //$$ all, RNG_SEL_D10_0004 // load_n out enabled
	//
	.init		(i_trig_DAC_init),
	.update		(i_trig_DAC_update),
	.test		(i_trig_SPI_frame),
	//
	.test_sdi_pdata(w_sub_test_sdi_pdata   ),
	.test_sdo_pdata(w_sub_0_test_sdo_pdata ),
	.readback_pdata  (),
	.readback1_pdata (),

	// DAC input
	.DAC_VALUE_IN1    (w_sub_0_DAC_VALUE_IN_A), // A
	.DAC_VALUE_IN2    (w_sub_0_DAC_VALUE_IN_B), // B
	.DAC_VALUE_IN3    (w_sub_0_DAC_VALUE_IN_C), // C
	.DAC_VALUE_IN4    (w_sub_0_DAC_VALUE_IN_D), // D
	// DAC readback
	.DAC_READBACK1    (w_sub_0_DAC_READBACK_A),
	.DAC_READBACK2    (w_sub_0_DAC_READBACK_B),
	.DAC_READBACK3    (w_sub_0_DAC_READBACK_C),
	.DAC_READBACK4    (w_sub_0_DAC_READBACK_D),

	// DAC control
	.SCLK			(w_sub_0_sclk  ),
	.SYNC_N			(w_sub_0_sync_n),
	.DIN			(w_sub_0_sdi   ),
	.SDO			(w_sub_0_sdo   ),
	
	// IO
	.LOAD_DAC_N 	(w_sub_0_load_n),
	.CLR_DAC_N  	(),
	.B2C_DAC    	(),

	//
	.init_done  	(w_sub_0_init_done  ),
	.update_done	(w_sub_0_update_done),
	.test_done  	(w_sub_0_test_done  ),    
	.error(),
	.debug_out()
);

master_spi_dac_AD5754__range_con  master_spi_dac_AD5754_core_1_inst(
	.clk		(clk), // assume 10MHz or 100ns
	.reset_n	(reset_n),
	.en			(r_valid & i_DAC_sel[1] ), // delayed enable 
	
	// configuration : all +/-10V ranges; and force not to use clear and load.
	//.conf		(16'hFF_0B), //$$ all, RNG_SEL_D10_0004
	.conf		(16'hFF_03), //$$ all, RNG_SEL_D10_0004 // load_n out enabled
	//
	.init		(i_trig_DAC_init),
	.update		(i_trig_DAC_update),
	.test		(i_trig_SPI_frame),
	//
	.test_sdi_pdata(w_sub_test_sdi_pdata   ),
	.test_sdo_pdata(w_sub_1_test_sdo_pdata ),
	.readback_pdata  (),
	.readback1_pdata (),
	
	// DAC input
	.DAC_VALUE_IN1    (w_sub_1_DAC_VALUE_IN_A), // A
	.DAC_VALUE_IN2    (w_sub_1_DAC_VALUE_IN_B), // B
	.DAC_VALUE_IN3    (w_sub_1_DAC_VALUE_IN_C), // C
	.DAC_VALUE_IN4    (w_sub_1_DAC_VALUE_IN_D), // D
	// DAC readback
	.DAC_READBACK1    (w_sub_1_DAC_READBACK_A),
	.DAC_READBACK2    (w_sub_1_DAC_READBACK_B),
	.DAC_READBACK3    (w_sub_1_DAC_READBACK_C),
	.DAC_READBACK4    (w_sub_1_DAC_READBACK_D),

	// DAC control
	.SCLK			(w_sub_1_sclk  ),
	.SYNC_N			(w_sub_1_sync_n),
	.DIN			(w_sub_1_sdi   ),
	.SDO			(w_sub_1_sdo   ),

	// IO
	.LOAD_DAC_N 	(w_sub_1_load_n),
	.CLR_DAC_N  	(),
	.B2C_DAC    	(),

	//
	.init_done  	(w_sub_1_init_done  ),
	.update_done	(w_sub_1_update_done),
	.test_done  	(w_sub_1_test_done  ),    
	.error(),
	.debug_out()
);

master_spi_dac_AD5754__range_con  master_spi_dac_AD5754_core_2_inst(
	.clk		(clk), // assume 10MHz or 100ns
	.reset_n	(reset_n),
	.en			(r_valid & i_DAC_sel[2] ), // delayed enable 
	
	// configuration : all +/-10V ranges; and force not to use clear and load.
	//.conf		(16'hFF_0B), //$$ all, RNG_SEL_D10_0004
	.conf		(16'hFF_03), //$$ all, RNG_SEL_D10_0004 // load_n out enabled
	//
	.init		(i_trig_DAC_init),
	.update		(i_trig_DAC_update),
	.test		(i_trig_SPI_frame),
	//
	.test_sdi_pdata(w_sub_test_sdi_pdata   ),
	.test_sdo_pdata(w_sub_2_test_sdo_pdata ),
	.readback_pdata  (),
	.readback1_pdata (),
	
	// DAC input
	.DAC_VALUE_IN1    (w_sub_2_DAC_VALUE_IN_A), // A
	.DAC_VALUE_IN2    (w_sub_2_DAC_VALUE_IN_B), // B
	.DAC_VALUE_IN3    (w_sub_2_DAC_VALUE_IN_C), // C
	.DAC_VALUE_IN4    (w_sub_2_DAC_VALUE_IN_D), // D
	// DAC readback
	.DAC_READBACK1    (w_sub_2_DAC_READBACK_A),
	.DAC_READBACK2    (w_sub_2_DAC_READBACK_B),
	.DAC_READBACK3    (w_sub_2_DAC_READBACK_C),
	.DAC_READBACK4    (w_sub_2_DAC_READBACK_D),

	// DAC control
	.SCLK			(w_sub_2_sclk  ),
	.SYNC_N			(w_sub_2_sync_n),
	.DIN			(w_sub_2_sdi   ),
	.SDO			(w_sub_2_sdo   ),

	// IO
	.LOAD_DAC_N 	(w_sub_2_load_n),
	.CLR_DAC_N  	(),
	.B2C_DAC    	(),

	//
	.init_done  	(w_sub_2_init_done  ),
	.update_done	(w_sub_2_update_done),
	.test_done  	(w_sub_2_test_done  ),    
	.error(),
	.debug_out()
);

master_spi_dac_AD5754__range_con  master_spi_dac_AD5754_core_3_inst(
	.clk		(clk), // assume 10MHz or 100ns
	.reset_n	(reset_n),
	.en			(r_valid & i_DAC_sel[3] ), // delayed enable 
	
	// configuration : all +/-10V ranges; and force not to use clear and load.
	//.conf		(16'hFF_0B), //$$ all, RNG_SEL_D10_0004
	.conf		(16'hFF_03), //$$ all, RNG_SEL_D10_0004 // load_n out enabled	
	//
	.init		(i_trig_DAC_init),
	.update		(i_trig_DAC_update),
	.test		(i_trig_SPI_frame),
	//
	.test_sdi_pdata(w_sub_test_sdi_pdata   ),
	.test_sdo_pdata(w_sub_3_test_sdo_pdata ),
	.readback_pdata  (),
	.readback1_pdata (),
	
	// DAC input
	.DAC_VALUE_IN1    (w_sub_3_DAC_VALUE_IN_A), // A
	.DAC_VALUE_IN2    (w_sub_3_DAC_VALUE_IN_B), // B
	.DAC_VALUE_IN3    (w_sub_3_DAC_VALUE_IN_C), // C
	.DAC_VALUE_IN4    (w_sub_3_DAC_VALUE_IN_D), // D
	// DAC readback
	.DAC_READBACK1    (w_sub_3_DAC_READBACK_A),
	.DAC_READBACK2    (w_sub_3_DAC_READBACK_B),
	.DAC_READBACK3    (w_sub_3_DAC_READBACK_C),
	.DAC_READBACK4    (w_sub_3_DAC_READBACK_D),

	// DAC control
	.SCLK			(w_sub_3_sclk  ),
	.SYNC_N			(w_sub_3_sync_n),
	.DIN			(w_sub_3_sdi   ),
	.SDO			(w_sub_3_sdo   ),

	// IO
	.LOAD_DAC_N 	(w_sub_3_load_n),
	.CLR_DAC_N  	(),
	.B2C_DAC    	(),

	//
	.init_done  	(w_sub_3_init_done  ),
	.update_done	(w_sub_3_update_done),
	.test_done  	(w_sub_3_test_done  ),    
	.error(),
	.debug_out()
);
 
 
// rise detections:
wire w_rise_done_SPI_frame  ;
wire w_rise_done_DAC_init   ;
wire w_rise_trig_DAC_update ;
wire w_rise_done_DAC_update ;
//
reg [1:0] r_done_SPI_frame  ;
reg [1:0] r_done_DAC_init   ;
reg [1:0] r_trig_DAC_update ;
reg [1:0] r_done_DAC_update ;
//
always @(posedge clk, negedge reset_n)
	if (!reset_n) begin
		r_done_SPI_frame  <= 2'b0;
		r_done_DAC_init   <= 2'b0;
		r_trig_DAC_update <= 2'b0;
		r_done_DAC_update <= 2'b0;
		end 
	else begin
		r_done_SPI_frame  <= {r_done_SPI_frame [0] , w_done_SPI_frame };
		r_done_DAC_init   <= {r_done_DAC_init  [0] , r_delay_done_DAC_init  };
		r_trig_DAC_update <= {r_trig_DAC_update[0] , i_trig_DAC_update};
		r_done_DAC_update <= {r_done_DAC_update[0] , w_done_DAC_update};
		end
//
assign w_rise_done_SPI_frame  = r_valid & (~r_done_SPI_frame [1]) & r_done_SPI_frame [0] ;
assign w_rise_done_DAC_init   = r_valid & (~r_done_DAC_init  [1]) & r_done_DAC_init  [0] ;
assign w_rise_trig_DAC_update = r_valid & (~r_trig_DAC_update[1]) & r_trig_DAC_update[0] ;
assign w_rise_done_DAC_update = r_valid & (~r_done_DAC_update[1]) & r_done_DAC_update[0] ;
//
assign o_done_SPI_frame_TO  = w_rise_done_SPI_frame  ;
assign o_done_DAC_init_TO   = w_rise_done_DAC_init   ;
assign o_done_DAC_update_TO = w_rise_done_DAC_update ;	

// busy 
reg r_busy_DAC_update;
//
always @(posedge clk, negedge reset_n)
	if (!reset_n) begin
		r_busy_DAC_update  <= 1'b0;
		end 
	else begin
		if (w_rise_trig_DAC_update) 
			r_busy_DAC_update  <= 1'b1;
		else if (w_rise_done_DAC_update)
			r_busy_DAC_update  <= 1'b0;
		end
//
assign o_busy_DAC_update = r_busy_DAC_update;


//// count triggers : 
(* keep = "true" *) reg [15:0] r_cnt_dac_trig;
//
always @(posedge clk, negedge reset_n)
	if (!reset_n) begin
		r_cnt_dac_trig <= 16'b0;
	end
	else begin
		//
		if (w_rise_done_SPI_frame|w_rise_done_DAC_init|w_rise_done_DAC_update)
			r_cnt_dac_trig <= r_cnt_dac_trig + 1;
		else 
			r_cnt_dac_trig <= r_cnt_dac_trig;
	end

//
assign o_cnt_dac_trig = r_cnt_dac_trig;


endmodule
