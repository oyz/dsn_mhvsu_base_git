`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Module Name: tb_master_spi_dac_AD5754__quad_d10v
// 
// http://www.asic-world.com/verilog/art_testbench_writing1.html
// http://www.asic-world.com/verilog/art_testbench_writing2.html
// https://www.xilinx.com/support/documentation/sw_manuals/xilinx2016_1/ug937-vivado-design-suite-simulation-tutorial.pdf
//
//////////////////////////////////////////////////////////////////////////////////


module tb_master_spi_dac_AD5754__quad_d10v;
reg clk; // assume 10MHz or 100ns
reg reset_n;
//  reg en;

//  reg clk_bus; //$$ 9.92ns for USB3.0	

reg init;
reg update;
reg test;
reg [31:0] test_sdi_pdata;
//  wire [31:0] test_sdo_pdata;
//
reg [31:0] r_sdo;
wire sdo = r_sdo[31];
//
wire w_test_done;

// DUT
wire sclk;
//
master_spi_dac_AD5754__quad_d10v  master_spi_dac_AD5754_inst(

	
	.clk       (clk    ), // assume 10MHz or 100ns
	.reset_n   (reset_n),

	// trig control
	.i_trig_SPI_frame      (test),
	.o_done_SPI_frame      (w_test_done),
	.o_done_SPI_frame_TO   (),
	
	.o_cnt_dac_trig  (), // [15:0] //$$ count i_trig_SPI_frame
	
	.i_trig_DAC_init       (init),
	.o_done_DAC_init       (),
	.o_done_DAC_init_TO    (),
	
	.i_trig_DAC_update     (update),
	.o_done_DAC_update     (),
	.o_done_DAC_update_TO  (),
	.o_busy_DAC_update     (),
	
	//.i_power_up_delay_disable (1'b1),
	.i_power_up_delay_disable (1'b0),

	// DAC load control
	.i_load_packet_en      (1'b1), // load packet enable
	.i_load_pin_ext_sig_en (1'b0), // load pin control by external signal 
	.i_load_pin_ext_sig    (1'b0), // external signal for load pin

	// DAC selection
	.i_DAC_sel             (4'b1111), // [3:0] // i_DAC_sel[3:0] for DAC3,DAC2,DAC1,DAC0

	// test frame 24-bit
	.i_test_sdi_pdata      (test_sdi_pdata[31:8]), // [23:0]
	.o_test_sdo_pdata      (), // [23:0]

	// DAC data in/out //{
	.i_DAC0_A_val  (16'h00C8), // [15:0] // DAC value in
	.i_DAC0_B_val  (16'h00AC), // [15:0] // DAC value in
	.i_DAC0_C_val  (16'h0555), // [15:0] // DAC value in
	.i_DAC0_D_val  (16'h0AAA), // [15:0] // DAC value in
	
	.i_DAC1_A_val  (16'h10C8), // [15:0] // DAC value in
	.i_DAC1_B_val  (16'h10AC), // [15:0] // DAC value in
	.i_DAC1_C_val  (16'h1555), // [15:0] // DAC value in
	.i_DAC1_D_val  (16'h1AAA), // [15:0] // DAC value in
	
	.i_DAC2_A_val  (16'h20C8), // [15:0] // DAC value in
	.i_DAC2_B_val  (16'h20AC), // [15:0] // DAC value in
	.i_DAC2_C_val  (16'h2555), // [15:0] // DAC value in
	.i_DAC2_D_val  (16'h2AAA), // [15:0] // DAC value in
	
	.i_DAC3_A_val  (16'h30C8), // [15:0] // DAC value in
	.i_DAC3_B_val  (16'h30AC), // [15:0] // DAC value in
	.i_DAC3_C_val  (16'h3555), // [15:0] // DAC value in
	.i_DAC3_D_val  (16'h3AAA), // [15:0] // DAC value in
	
	.o_DAC0_A_rbk  (), // [15:0]  // DAC readback out
	.o_DAC0_B_rbk  (), // [15:0]  // DAC readback out
	.o_DAC0_C_rbk  (), // [15:0]  // DAC readback out
	.o_DAC0_D_rbk  (), // [15:0]  // DAC readback out
	
	.o_DAC1_A_rbk  (), // [15:0]  // DAC readback out
	.o_DAC1_B_rbk  (), // [15:0]  // DAC readback out
	.o_DAC1_C_rbk  (), // [15:0]  // DAC readback out
	.o_DAC1_D_rbk  (), // [15:0]  // DAC readback out
	
	.o_DAC2_A_rbk  (), // [15:0]  // DAC readback out
	.o_DAC2_B_rbk  (), // [15:0]  // DAC readback out
	.o_DAC2_C_rbk  (), // [15:0]  // DAC readback out
	.o_DAC2_D_rbk  (), // [15:0]  // DAC readback out
	
	.o_DAC3_A_rbk  (), // [15:0]  // DAC readback out
	.o_DAC3_B_rbk  (), // [15:0]  // DAC readback out
	.o_DAC3_C_rbk  (), // [15:0]  // DAC readback out
	.o_DAC3_D_rbk  (), // [15:0]  // DAC readback out
	//}

	// DAC control pins // quad
	.o_SCLK	       (sclk), // [3:0]
	.o_SYNC_N      (), // [3:0]
	.o_DIN	       (), // [3:0]
	.i_SDO         (sdo), // [3:0]
	
	// IO
	.o_LOAD_DAC_N  (), // ext pin

	// flag
	.valid         ()
	
);

initial begin
#0		clk = 1'b0;
		reset_n = 1'b0;
		//  en = 1'b0;
		//  clk_bus = 1'b0;
		init = 1'b0;
		update = 1'b0;
		test = 1'b0;
		test_sdi_pdata = 32'b1010_0101_1001_0110_1100_1010_0011_0101;
#200	reset_n = 1'b1;
//  #200	en = 1'b1;
#200	test = 1'b1;
#15_000	init = 1'b1;
		test = 1'b0;
#30_000	update = 1'b1;

#0		test = 1'b1;
#200	test = 1'b0;
@(posedge w_test_done)
#500	$finish; 	

#0		test = 1'b1;
#200	test = 1'b0;
@(posedge w_test_done)
#500	$finish; 	

#0		test = 1'b1;
#200	test = 1'b0;
@(posedge w_test_done)
#500	$finish; 	

#0		test = 1'b1;
#200	test = 1'b0;
@(posedge w_test_done)
#500	$finish; 	

//	
end

always
#50 	clk = ~clk; // toggle every 50ns --> clock 100ns 

//  always
//  #4.96 	clk_bus = ~clk_bus; // toggle every 4.96ns --> clock 9.92ns for USB3.0	 

always @(posedge sclk, negedge reset_n)
    if (!reset_n) begin
        r_sdo <= 32'b1010_0101_1001_0110_1100_1010_0011_0101;
        end 
    else begin
        r_sdo <= {r_sdo[30:0], r_sdo[31]}; 
        end

//initial begin
	//$dumpfile ("waveform.vcd"); 
	//$dumpvars; 
//end 
  
//initial  begin
	//$display("\t\t time,\t clk,\t reset_n,\t en"); 
	//$monitor("%d,\t%b,\t%b,\t%b,\t%d",$time,clk,reset_n,en); 
//end 
  
//initial 
	//#200_000 $finish; // 200us = 200_000 ns
	//#1000 $finish; // 1us = 1000 ns
	//#1000_000 $finish; // 1ms = 1000_000 ns
	
endmodule
