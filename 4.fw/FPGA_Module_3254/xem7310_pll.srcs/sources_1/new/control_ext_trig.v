`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Module Name: control_ext_trig
//
// delayed trigger control for MHVSU BASE board
// 

//// ref 
//----------------------------------------------------------------------------
// http://www.asic-world.com/verilog/art_testbench_writing1.html
// http://www.asic-world.com/verilog/art_testbench_writing2.html
// https://www.xilinx.com/support/documentation/sw_manuals/xilinx2016_1/ug937-vivado-design-suite-simulation-tutorial.pdf
//
//----------------------------------------------------------------------------
// This demonstration testbench may instantiate the example design for the SelectIO wizard. 
//$$    https://www.xilinx.com/support/documentation/ip_documentation/selectio_wiz/v5_1/pg070-selectio-wiz.pdf
//$$    https://www.xilinx.com/support/documentation/user_guides/ug471_7Series_SelectIO.pdf
//----------------------------------------------------------------------------


module control_delay_trig (
	input  wire clk    , // assume 10MHz 
	input  wire reset_n,
	input  wire clk_en , // assume 100kHz enable
	
	input  wire i_trig,
	
	input  wire [15:0] i_count_delay, // delay count based on 100kHz or 10us 
	
	output wire o_busy,
	output wire o_trig_delayed // trig out pulse width based on 10MHz or 100ns 
);
//{

//// sample i_trig and detect rise w_rise_trig //{
(* keep = "true" *) reg [1:0] r_trig;
wire w_rise_trig = (~r_trig[1]) & r_trig[0];
//
always @(posedge clk, negedge reset_n)
	if (!reset_n) begin
		r_trig <= 2'b0;
	end
	else begin
		r_trig <= {r_trig[0], i_trig};
	end
//}

//// count down r_count_delay //{{
(* keep = "true" *) reg [15:0] r_count_delay;
//
always @(posedge clk, negedge reset_n)
	if (!reset_n) begin
		r_count_delay <= 16'b0;
	end
	else begin
		if (w_rise_trig) begin
			r_count_delay <= i_count_delay + 1;
			end
		else if (clk_en) begin 
			if (r_count_delay>0)
				r_count_delay <= r_count_delay - 1;
			end
	end
//}

//// generate busy and trig_delayed //{
reg r_busy        ;
reg r_trig_delayed;
//
always @(posedge clk, negedge reset_n)
	if (!reset_n) begin
		r_busy         <=  1'b0;
		r_trig_delayed <=  1'b0;
	end
	else begin
		if      (clk_en & (r_count_delay==1)) 
			r_busy <= 1'b0; // clear busy 
		else if (clk_en & (r_count_delay> 1))
			r_busy <= 1'b1; // set busy
		//
		if      (clk_en & (r_count_delay==1)) 
			r_trig_delayed <= 1'b1; // set
		else 
			r_trig_delayed <= 1'b0; // clear right after 10MHz one clock
	end
//
assign o_busy         = r_busy        ;
assign o_trig_delayed = r_trig_delayed;

//}

//}
endmodule

//// control_ext_trig
module control_ext_trig (
	input  wire clk                    , // assume 10MHz
	input  wire reset_n                ,
	
	input  wire i_PIN_trig_disable     , // pin disable
	input  wire i_M_TRIG_PIN           , //
	input  wire i_M_PRE_TRIG_PIN       , //
	input  wire i_AUX_TRIG_PIN         , //
	
	input  wire i_M_TRIG_SW            , //
	input  wire i_M_PRE_TRIG_SW        , //
	input  wire i_AUX_TRIG_SW          , //
	
	input  wire [2:0] i_conf_M_TRIG             , // [2:0] // M_TRIG     connectivity to {SPIO,DAC,ADC}
	input  wire [2:0] i_conf_M_PRE_TRIG         , // [2:0] // M_PRE_TRIG connectivity to {SPIO,DAC,ADC}
	input  wire [2:0] i_conf_AUX                , // [2:0] // AUX        connectivity to {SPIO,DAC,ADC}
	
	input  wire [15:0] i_count_delay_trig_spio  , // [15:0] // based on 100kHz or 10us
	input  wire [15:0] i_count_delay_trig_dac   , // [15:0] // based on 100kHz or 10us
	input  wire [15:0] i_count_delay_trig_adc   , // [15:0] // based on 100kHz or 10us
	
	output wire o_spio_busy_cowork        , // 
	output wire o_dac_busy_cowork         , // 
	output wire o_adc_busy_cowork         , // 
	
	output wire o_spio_trig_cowork        , // 
	output wire o_dac_trig_cowork         , // 
	output wire o_adc_trig_cowork         , // 
	
	output wire valid                  
);
//{

//// valid //{
(* keep = "true" *) reg r_valid;
assign valid = r_valid;
//
always @(posedge clk, negedge reset_n)
	if (!reset_n) begin
		r_valid <= 1'b0;
	end
	else begin
		r_valid <= 1'b1;
	end
//}

//// generate 100kHz pulse or 10us // div by 100 //{
reg r_clk_en__100kHz;
(* keep = "true" *) reg [7:0] r_cnt_div__100kHz;
//
always @(posedge clk, negedge reset_n)
	if (!reset_n) begin
		r_clk_en__100kHz   <= 1'b0;
		r_cnt_div__100kHz  <= 8'b0;
	end
	else begin
		//
		if (r_cnt_div__100kHz == 8'd1) 
			r_clk_en__100kHz <= 1'b1;
		else 
			r_clk_en__100kHz <= 1'b0;
		//
		if (r_cnt_div__100kHz>=8'd100)
			r_cnt_div__100kHz  <= 8'b1;
		else 
			r_cnt_div__100kHz  <= r_cnt_div__100kHz  + 1;
		//
	end
//}

//// trig input control //{
wire w_trig_from__m_trig     = (i_M_TRIG_PIN     &(~i_PIN_trig_disable)) | i_M_TRIG_SW     ;
wire w_trig_from__m_pre_trig = (i_M_PRE_TRIG_PIN &(~i_PIN_trig_disable)) | i_M_PRE_TRIG_SW ;
wire w_trig_from__aux_trig   = (i_AUX_TRIG_PIN   &(~i_PIN_trig_disable)) | i_AUX_TRIG_SW   ;
//
wire w_trig__spio = (w_trig_from__m_trig     &     i_conf_M_TRIG[2]) | 
					(w_trig_from__m_pre_trig & i_conf_M_PRE_TRIG[2]) | 
					(w_trig_from__aux_trig   &        i_conf_AUX[2]) ;
wire w_trig__dac  = (w_trig_from__m_trig     &     i_conf_M_TRIG[1]) | 
					(w_trig_from__m_pre_trig & i_conf_M_PRE_TRIG[1]) | 
					(w_trig_from__aux_trig   &        i_conf_AUX[1]) ;
wire w_trig__adc  = (w_trig_from__m_trig     &     i_conf_M_TRIG[0]) | 
					(w_trig_from__m_pre_trig & i_conf_M_PRE_TRIG[0]) | 
					(w_trig_from__aux_trig   &        i_conf_AUX[0]) ;
//}

//// call delayed trig //{
control_delay_trig  control_delay_trig__spio_inst (
	.clk             (clk                    ), // assume 10MHz 
	.reset_n         (reset_n                ),
	.clk_en          (r_clk_en__100kHz       ), // assume 100kHz enable
	.i_trig          (w_trig__spio           ), // trigger input
	.i_count_delay   (i_count_delay_trig_spio), // [15:0] //
	.o_busy          (o_spio_busy_cowork     ),
	.o_trig_delayed	 (o_spio_trig_cowork     )  // delayed trigger output
);
//
control_delay_trig  control_delay_trig__dac_inst (
	.clk             (clk                    ), // assume 10MHz 
	.reset_n         (reset_n                ),
	.clk_en          (r_clk_en__100kHz       ), // assume 100kHz enable
	.i_trig          (w_trig__dac            ), // trigger input
	.i_count_delay   (i_count_delay_trig_dac ), // [15:0] //
	.o_busy          (o_dac_busy_cowork      ),
	.o_trig_delayed	 (o_dac_trig_cowork      )  // delayed trigger output
);
//
control_delay_trig  control_delay_trig__adc_inst (
	.clk             (clk                    ), // assume 10MHz 
	.reset_n         (reset_n                ),
	.clk_en          (r_clk_en__100kHz       ), // assume 100kHz enable
	.i_trig          (w_trig__adc            ), // trigger input
	.i_count_delay   (i_count_delay_trig_adc ), // [15:0] //
	.o_busy          (o_adc_busy_cowork      ),
	.o_trig_delayed	 (o_adc_trig_cowork      )  // delayed trigger output
);
//}

//}
endmodule

