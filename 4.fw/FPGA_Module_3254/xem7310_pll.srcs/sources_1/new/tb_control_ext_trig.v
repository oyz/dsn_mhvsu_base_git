`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Module Name: tb_control_ext_trig
//
// test control_ext_trig.v
// cowork control_ext_trig with control_adc_ddr_LTC2325

//// ref 
//----------------------------------------------------------------------------
// http://www.asic-world.com/verilog/art_testbench_writing1.html
// http://www.asic-world.com/verilog/art_testbench_writing2.html
// https://www.xilinx.com/support/documentation/sw_manuals/xilinx2016_1/ug937-vivado-design-suite-simulation-tutorial.pdf
//
//----------------------------------------------------------------------------
// This demonstration testbench may instantiate the example design for the SelectIO wizard. 
//$$    https://www.xilinx.com/support/documentation/ip_documentation/selectio_wiz/v5_1/pg070-selectio-wiz.pdf
//$$    https://www.xilinx.com/support/documentation/user_guides/ug471_7Series_SelectIO.pdf
//----------------------------------------------------------------------------


// testbench
module tb_control_ext_trig;

//// clock and reset //{
reg clk = 1'b0; // assume 10MHz or 100ns
	always
	#50 	clk = ~clk; // toggle every 50ns --> clock 100ns 

reg reset_n = 1'b0;
wire reset = ~reset_n;

wire sys_clk = clk;
	
reg clk_192M = 1'b0; // 192MHz
reg clk_96M  = 1'b0; //  96MHz
reg clk_48M  = 1'b0; //  48MHz
reg clk_24M  = 1'b0; //  24MHz
reg clk_12M  = 1'b0; //  12MHz
always begin
	#2.60416667;
	clk_192M = ~clk_192M;  // toggle every 1/(192MHz)/2=2.60416667ns
	clk_96M  = ~clk_96M ;
	clk_48M  = ~clk_48M ;
	clk_24M  = ~clk_24M ;
	clk_12M  = ~clk_12M ;
	#2.60416667;
	clk_192M = ~clk_192M;  // toggle every 1/(192MHz)/2=2.60416667ns
	#2.60416667;
	clk_192M = ~clk_192M;  // toggle every 1/(192MHz)/2=2.60416667ns
	clk_96M  = ~clk_96M ;
	#2.60416667;
	clk_192M = ~clk_192M;  // toggle every 1/(192MHz)/2=2.60416667ns
	//
	#2.60416667;
	clk_192M = ~clk_192M;  // toggle every 1/(192MHz)/2=2.60416667ns
	clk_96M  = ~clk_96M ;
	clk_48M  = ~clk_48M ;
	#2.60416667;
	clk_192M = ~clk_192M;  // toggle every 1/(192MHz)/2=2.60416667ns
	#2.60416667;
	clk_192M = ~clk_192M;  // toggle every 1/(192MHz)/2=2.60416667ns
	clk_96M  = ~clk_96M ;
	#2.60416667;
	clk_192M = ~clk_192M;  // toggle every 1/(192MHz)/2=2.60416667ns
	//
	#2.60416667;
	clk_192M = ~clk_192M;  // toggle every 1/(192MHz)/2=2.60416667ns
	clk_96M  = ~clk_96M ;
	clk_48M  = ~clk_48M ;
	clk_24M  = ~clk_24M ;
	#2.60416667;
	clk_192M = ~clk_192M;  // toggle every 1/(192MHz)/2=2.60416667ns
	#2.60416667;
	clk_192M = ~clk_192M;  // toggle every 1/(192MHz)/2=2.60416667ns
	clk_96M  = ~clk_96M ;
	#2.60416667;
	clk_192M = ~clk_192M;  // toggle every 1/(192MHz)/2=2.60416667ns
	//
	#2.60416667;
	clk_192M = ~clk_192M;  // toggle every 1/(192MHz)/2=2.60416667ns
	clk_96M  = ~clk_96M ;
	clk_48M  = ~clk_48M ;
	#2.60416667;
	clk_192M = ~clk_192M;  // toggle every 1/(192MHz)/2=2.60416667ns
	#2.60416667;
	clk_192M = ~clk_192M;  // toggle every 1/(192MHz)/2=2.60416667ns
	clk_96M  = ~clk_96M ;
	#2.60416667;
	clk_192M = ~clk_192M;  // toggle every 1/(192MHz)/2=2.60416667ns
	//
	end

reg clk_104M = 1'b0; // 104MHz
reg clk_52M  = 1'b0; //  52MHz
reg clk_26M  = 1'b0; //  26MHz
always begin
	#4.80769231;
	clk_104M = ~clk_104M;  // toggle every 1/(104MHz)/2=4.80769231ns
	clk_52M  = ~clk_52M ;  // toggle every 1/( 52MHz)/2=9.61538462ns
	clk_26M  = ~clk_26M ;  // toggle every 1/( 26MHz)/2=19.2307692ns
	#4.80769231;
	clk_104M = ~clk_104M;  // toggle every 1/(104MHz)/2=4.80769231ns
	#4.80769231;
	clk_104M = ~clk_104M;  // toggle every 1/(104MHz)/2=4.80769231ns
	clk_52M  = ~clk_52M ;  // toggle every 1/( 52MHz)/2=9.61538462ns
	#4.80769231;
	clk_104M = ~clk_104M;  // toggle every 1/(104MHz)/2=4.80769231ns
	end

//}


//// DUT: test model0 //{
//   test without controller
//
/////////////
reg r_CNV_B;
reg r_SCK  ;
//
test_model_adc_ddr_LTC2325  test_model_adc_ddr_LTC2325_inst (
	////////////////////////	
	.b_clk		(clk_192M), // 192MHz
	.reset_n	(reset_n),
	
	.i_CNV_B    (r_CNV_B),
	.i_SCK      (r_SCK  ),
	.o_CLKOUT   (),
	.o_SDO      () // [3:0] 	
	///////////////////////////
);
//}

//// DUT: test model1 //{
//   test with controller
//
/////////////
wire        w_CNV_B ;
wire        w_SCK   ;
wire        w_DCO_model   ;
wire [3:0]  w_SDO_model   ;
//
test_model_adc_ddr_LTC2325  test_model_adc_ddr_LTC2325_inst1 (
	////////////////////////	
	.b_clk		(clk_192M), // 192MHz
	.reset_n	(reset_n ),
	
	.i_CNV_B    (w_CNV_B ),
	.i_SCK      (w_SCK   ),
	.o_CLKOUT   (w_DCO_model   ),
	.o_SDO      (w_SDO_model   )  // [3:0] 
	///////////////////////////
);
//}


//// DUT: test control for quad adc //{
//
wire       #10 w_DCO   =  w_DCO_model;
wire [3:0] #10 w_SDO   =  w_SDO_model;
//
reg r_trig_conv_single;
reg r_trig_conv_run;
reg r_fifo_rd_en;

wire w_trig_conv_run__ext;

wire w_busy;

wire [15:0]  w_p_data_ADC0_A;
wire [15:0]  w_p_data_ADC0_B;
wire [15:0]  w_p_data_ADC0_C;
wire [15:0]  w_p_data_ADC0_D;
wire [15:0]  w_p_data_ADC1_A;
wire [15:0]  w_p_data_ADC1_B;
wire [15:0]  w_p_data_ADC1_C;
wire [15:0]  w_p_data_ADC1_D;
wire [15:0]  w_p_data_ADC2_A;
wire [15:0]  w_p_data_ADC2_B;
wire [15:0]  w_p_data_ADC2_C;
wire [15:0]  w_p_data_ADC2_D;
wire [15:0]  w_p_data_ADC3_A;
wire [15:0]  w_p_data_ADC3_B;
wire [15:0]  w_p_data_ADC3_C;
wire [15:0]  w_p_data_ADC3_D;

control_adc_ddr_LTC2325  control_adc_ddr_LTC2325_inst( 
	.b_clk 			(clk_192M    ), // 192MHz
	.reset_n 		(reset_n     ),
	.p_clk 			(clk_12M     ), // 12MHz for parallel data out
	//
	.i_test_mode_en (1'b0),
	.i_test_mode_hs (1'b0),
	//
	.i_trig_conv_single    (r_trig_conv_single), // trigger for one ADC sample
	.i_trig_conv_run       (r_trig_conv_run | w_trig_conv_run__ext), // trigger for ADC samples 
	//.i_count_period_div4   (16'd105), // [15:0] // sample period = i_count_period_div4*4  //based on 210MHz 
	//.i_count_period_div4   (16'd20), // [15:0] // sample period = i_count_period_div4*4  //
	.i_count_period_div4   (16'd30), // [15:0] // sample period = i_count_period_div4*4  //
	//.i_count_period_div4   (16'd48_000), // [15:0] // sample period = i_count_period_div4*4  
	.i_count_conv_div4     (16'd2  ), // [15:0] // adc samples   = i_count_conv_div4*4    // < 2^17 
	//
	.o_busy           (w_busy), // busy 
	.o_done_to        (), // done trig out
	//
	
	//// IO pin //{
	.o_CNV_B        (w_CNV_B ), 
	.o_SCK          (w_SCK   ), 
	//
	.i_ADC0_DCO          (w_DCO   ), 
	.i_ADC0_SDO          (w_SDO   ), // [3:0] 
	.i_ADC1_DCO          (w_DCO   ), 
	.i_ADC1_SDO          (w_SDO   ), // [3:0] 
	.i_ADC2_DCO          (w_DCO   ), 
	.i_ADC2_SDO          (w_SDO   ), // [3:0] 
	.i_ADC3_DCO          (w_DCO   ), 
	.i_ADC3_SDO          (w_SDO   ), // [3:0] 
	//}
	
	//// adc p data out //{
	.o_p_data_ADC0_A       (w_p_data_ADC0_A), // [15:0] // from i_ADC0_SDO[0]
	.o_p_data_ADC0_B       (w_p_data_ADC0_B), // [15:0] // from i_ADC0_SDO[1]
	.o_p_data_ADC0_C       (w_p_data_ADC0_C), // [15:0] // from i_ADC0_SDO[2]
	.o_p_data_ADC0_D       (w_p_data_ADC0_D), // [15:0] // from i_ADC0_SDO[3]
	.o_p_data_ADC1_A       (w_p_data_ADC1_A), // [15:0] // from i_ADC1_SDO[0]
	.o_p_data_ADC1_B       (w_p_data_ADC1_B), // [15:0] // from i_ADC1_SDO[1]
	.o_p_data_ADC1_C       (w_p_data_ADC1_C), // [15:0] // from i_ADC1_SDO[2]
	.o_p_data_ADC1_D       (w_p_data_ADC1_D), // [15:0] // from i_ADC1_SDO[3]
	.o_p_data_ADC2_A       (w_p_data_ADC2_A), // [15:0] // from i_ADC2_SDO[0]
	.o_p_data_ADC2_B       (w_p_data_ADC2_B), // [15:0] // from i_ADC2_SDO[1]
	.o_p_data_ADC2_C       (w_p_data_ADC2_C), // [15:0] // from i_ADC2_SDO[2]
	.o_p_data_ADC2_D       (w_p_data_ADC2_D), // [15:0] // from i_ADC2_SDO[3]
	.o_p_data_ADC3_A       (w_p_data_ADC3_A), // [15:0] // from i_ADC3_SDO[0]
	.o_p_data_ADC3_B       (w_p_data_ADC3_B), // [15:0] // from i_ADC3_SDO[1]
	.o_p_data_ADC3_C       (w_p_data_ADC3_C), // [15:0] // from i_ADC3_SDO[2]
	.o_p_data_ADC3_D       (w_p_data_ADC3_D), // [15:0] // from i_ADC3_SDO[3]
	//}	
	
	//// fifo interface //{
	.f_clk          (clk_104M), // assume 104MHz or 108MHz // fifo reading clock 
	
	.i_fifo_adc0_a_rd_en (r_fifo_rd_en) , //       
	.o_fifo_adc0_a_dout  () , // [15:0]
	.o_fifo_adc0_a_full  () , //       
	.o_fifo_adc0_a_empty () , //       
	.i_fifo_adc0_b_rd_en (r_fifo_rd_en) , //       
	.o_fifo_adc0_b_dout  () , // [15:0]
	.o_fifo_adc0_b_full  () , //       
	.o_fifo_adc0_b_empty () , //       
	.i_fifo_adc0_c_rd_en (r_fifo_rd_en) , //       
	.o_fifo_adc0_c_dout  () , // [15:0]
	.o_fifo_adc0_c_full  () , //       
	.o_fifo_adc0_c_empty () , //       
	.i_fifo_adc0_d_rd_en (r_fifo_rd_en) , //       
	.o_fifo_adc0_d_dout  () , // [15:0]
	.o_fifo_adc0_d_full  () , //       
	.o_fifo_adc0_d_empty () , //       

	.i_fifo_adc1_a_rd_en (r_fifo_rd_en) , //       
	.o_fifo_adc1_a_dout  () , // [15:0]
	.o_fifo_adc1_a_full  () , //       
	.o_fifo_adc1_a_empty () , //       
	.i_fifo_adc1_b_rd_en (r_fifo_rd_en) , //       
	.o_fifo_adc1_b_dout  () , // [15:0]
	.o_fifo_adc1_b_full  () , //       
	.o_fifo_adc1_b_empty () , //       
	.i_fifo_adc1_c_rd_en (r_fifo_rd_en) , //       
	.o_fifo_adc1_c_dout  () , // [15:0]
	.o_fifo_adc1_c_full  () , //       
	.o_fifo_adc1_c_empty () , //       
	.i_fifo_adc1_d_rd_en (r_fifo_rd_en) , //       
	.o_fifo_adc1_d_dout  () , // [15:0]
	.o_fifo_adc1_d_full  () , //       
	.o_fifo_adc1_d_empty () , //       

	.i_fifo_adc2_a_rd_en (r_fifo_rd_en) , //       
	.o_fifo_adc2_a_dout  () , // [15:0]
	.o_fifo_adc2_a_full  () , //       
	.o_fifo_adc2_a_empty () , //       
	.i_fifo_adc2_b_rd_en (r_fifo_rd_en) , //       
	.o_fifo_adc2_b_dout  () , // [15:0]
	.o_fifo_adc2_b_full  () , //       
	.o_fifo_adc2_b_empty () , //       
	.i_fifo_adc2_c_rd_en (r_fifo_rd_en) , //       
	.o_fifo_adc2_c_dout  () , // [15:0]
	.o_fifo_adc2_c_full  () , //       
	.o_fifo_adc2_c_empty () , //       
	.i_fifo_adc2_d_rd_en (r_fifo_rd_en) , //       
	.o_fifo_adc2_d_dout  () , // [15:0]
	.o_fifo_adc2_d_full  () , //       
	.o_fifo_adc2_d_empty () , //       

	.i_fifo_adc3_a_rd_en (r_fifo_rd_en) , //       
	.o_fifo_adc3_a_dout  () , // [15:0]
	.o_fifo_adc3_a_full  () , //       
	.o_fifo_adc3_a_empty () , //       
	.i_fifo_adc3_b_rd_en (r_fifo_rd_en) , //       
	.o_fifo_adc3_b_dout  () , // [15:0]
	.o_fifo_adc3_b_full  () , //       
	.o_fifo_adc3_b_empty () , //       
	.i_fifo_adc3_c_rd_en (r_fifo_rd_en) , //       
	.o_fifo_adc3_c_dout  () , // [15:0]
	.o_fifo_adc3_c_full  () , //       
	.o_fifo_adc3_c_empty () , //       
	.i_fifo_adc3_d_rd_en (r_fifo_rd_en) , //       
	.o_fifo_adc3_d_dout  () , // [15:0]
	.o_fifo_adc3_d_full  () , //       
	.o_fifo_adc3_d_empty () , //       
	//}
	
	.valid			()
	///////////////////////////
);

//}

//// DUT: test control for ext trig //{

//
reg r_trig_ext;

//
wire w_EXT_TRIG_en = 1'b1; //
wire w_trig_cowork = r_trig_ext; //
wire [15:0] w_count_delay_trig_spio = 16'd3; //
wire [15:0] w_count_delay_trig_dac  = 16'd4; //
wire [15:0] w_count_delay_trig_adc  = 16'd5; //
wire w_spio_busy_cowork;
wire w_dac_busy_cowork ;
wire w_adc_busy_cowork ;
wire w_spio_trig_cowork;
wire w_dac_trig_cowork ;
wire w_adc_trig_cowork ;

//
control_ext_trig  control_ext_trig_inst(
	.clk                    (sys_clk), // 10MHz
	.reset_n                (reset_n & w_EXT_TRIG_en),
	//
	.i_PIN_trig_disable     (1'b1), // pin disable
	.i_M_TRIG_PIN           (1'b0), //
	.i_M_PRE_TRIG_PIN       (1'b0), //
	.i_AUX_TRIG_PIN         (1'b0), //
	//
	.i_M_TRIG_SW            (w_trig_cowork), //
	.i_M_PRE_TRIG_SW        (1'b0), //
	.i_AUX_TRIG_SW          (1'b0), //
	//
	.i_conf_M_TRIG          (3'b111), // [2:0] // M_TRIG     connectivity to {ADC,DAC,SPIO}
	.i_conf_M_PRE_TRIG      (3'b111), // [2:0] // M_PRE_TRIG connectivity to {ADC,DAC,SPIO}
	.i_conf_AUX             (3'b111), // [2:0] // AUX        connectivity to {ADC,DAC,SPIO}
	//
	.i_count_delay_trig_spio   (w_count_delay_trig_spio), // [15:0] // based on 100kHz or 10us
	.i_count_delay_trig_dac    (w_count_delay_trig_dac ), // [15:0] // based on 100kHz or 10us
	.i_count_delay_trig_adc    (w_count_delay_trig_adc ), // [15:0] // based on 100kHz or 10us
	//                         
	.o_spio_busy_cowork        (w_spio_busy_cowork), // 
	.o_dac_busy_cowork         (w_dac_busy_cowork ), // 
	.o_adc_busy_cowork         (w_adc_busy_cowork ), // 
	//                         
	.o_spio_trig_cowork        (w_spio_trig_cowork), // 
	.o_dac_trig_cowork         (w_dac_trig_cowork ), // 
	.o_adc_trig_cowork         (w_adc_trig_cowork ), // 
	//
	.valid                  ()
);


//
assign w_trig_conv_run__ext = w_adc_trig_cowork;

//}


initial begin
//// initialize //{
#0	reset_n 		= 1'b0;
	////
	r_CNV_B            = 1'b0;
	r_SCK              = 1'b0;
	//
	r_trig_conv_single = 1'b0;
	r_trig_conv_run    = 1'b0;
	r_fifo_rd_en       = 1'b0;
	r_trig_ext         = 1'b0;
#200;
	reset_n = 1'b1; 
#200;
//}

//// manual signal test //{
#100;
	TRIG_ADC_CONV(); // task
////
	TRIG_ADC_CONV(); // task
///////////////////////
#100;
//	$finish;
//}

//// trig test //{
#100;
	r_trig_conv_single = 1'b1;
#100;
	r_trig_conv_single = 1'b0;
#100;
	@(negedge w_busy)
#1000;
	r_trig_conv_single = 1'b1;
#100;
	r_trig_conv_single = 1'b0;
#100;
	@(negedge w_busy)
#1000;
	r_trig_conv_run = 1'b1;
#100;
	r_trig_conv_run = 1'b0;
#100;
	@(negedge w_busy)
#1000;
	r_trig_conv_run = 1'b1;
#100;
	r_trig_conv_run = 1'b0;
#100;
	@(negedge w_busy)
///////////////////////
#100;
//	$finish;
//}

//// fifo read test //{
#100;
	READ_FIFO();
#100;
	READ_FIFO();
#100;
	READ_FIFO();
#100;
	READ_FIFO();

#100;
	READ_FIFO();
#100;
	READ_FIFO();
#100;
	READ_FIFO();
#100;
	READ_FIFO();

#100;
	READ_FIFO();
#100;
	READ_FIFO();
#100;
	READ_FIFO();
#100;
	READ_FIFO();

#100;
	READ_FIFO();
#100;
	READ_FIFO();
#100;
	READ_FIFO();
#100;
	READ_FIFO();

#100;
	READ_FIFO();
#100;
	READ_FIFO();
#100;
	READ_FIFO();
#100;
	READ_FIFO();

///////////////////////
#100;
	$finish;
	
//}

//// ext trig test //{
	r_trig_ext = 1'b1;
#100;
	r_trig_ext = 1'b0;
#100;
	@(negedge w_adc_busy_cowork)
///////////////////////
#1000;
	$finish;

//}

end

//initial begin
	//$dumpfile ("waveform.vcd"); 
	//$dumpvars; 
//end 
  
//initial  begin
	//$display("\t\t time,\t clk,\t reset_n,\t en"); 
	//$monitor("%d,\t%b,\t%b,\t%b,\t%d",$time,clk,reset_n,en); 
//end 

//initial begin
//#1000_000; // 1ms = 1000_000ns
//	$finish;
//end


// task  ADC conversion signal 
task  TRIG_ADC_CONV;
	//input  [31:0] opt;
	begin 
		@(posedge clk_192M);
		r_CNV_B         = 1'b1;
	#30;
		r_CNV_B         = 1'b0;
	#170;
	#10;
		r_SCK           = 1'b1;
	#10;
		r_SCK           = 1'b0;
	#10;
		r_SCK           = 1'b1;
	#10;
		r_SCK           = 1'b0;
	#10;
		r_SCK           = 1'b1;
	#10;
		r_SCK           = 1'b0;
	#10;
		r_SCK           = 1'b1;
	#10;
		r_SCK           = 1'b0;
	#10;
		r_SCK           = 1'b1;
	#10;
		r_SCK           = 1'b0;
	#10;
		r_SCK           = 1'b1;
	#10;
		r_SCK           = 1'b0;
	#10;
		r_SCK           = 1'b1;
	#10;
		r_SCK           = 1'b0;
	#10;
		r_SCK           = 1'b1;
	#10;
		r_SCK           = 1'b0;
	#10;
	end
endtask 

// task  fifo read 
task  READ_FIFO;
	//input none
	begin 
		@(posedge clk_104M);
		r_fifo_rd_en       = 1'b1;
		@(posedge clk_104M);
		r_fifo_rd_en       = 1'b0;
	end
endtask



endmodule
