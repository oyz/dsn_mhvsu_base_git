## test__MHVSU_BASE__lan.py

# ADC test to add 
# SPIO / DAC controls
# rev 6/9 note: txt change ... ADC_IM --> ADC_VM
# change control USB to LAN

from time import sleep
import sys


## tkinter GUI module 
# https://www.tutorialspoint.com/python3/python_gui_programming.htm
# https://www.tutorialspoint.com/python3/tk_button.htm
# https://www.tutorialspoint.com/python3/tk_checkbutton.htm
# https://www.python-course.eu/tkinter_checkboxes.php
# https://www.tutorialspoint.com/python3/tk_entry.htm
# https://www.python-course.eu/tkinter_entry_widgets.php ... entry text number...


### common ###


import platform    # For getting the operating system name
import subprocess  # For executing a shell command

def ping(host):
	"""
	Returns True if host (str) responds to a ping request.
	Remember that a host may not respond to a ping (ICMP) request even if the host name is valid.
	"""
	
	# Option for the number of packets as a function of
	param1 = '-n' if platform.system().lower()=='windows' else '-c'
	param2 = '-w' if platform.system().lower()=='windows' else '-i'
	value2 = '50' if platform.system().lower()=='windows' else '0.05'
	
	# Building the command. Ex: "ping -c 1 google.com"
	command = ['ping', param1, '1', param2, value2, host]
	
	return subprocess.call(command) == 0




###########################################################################
## common converter

def conv_dec_to_bit_2s_comp_16bit(dec, full_scale=20):
	if dec > full_scale/2-full_scale/2**16 :
		dec = full_scale/2-full_scale/2**16
	if dec < -full_scale/2-full_scale/2**16 :
		dec = -full_scale/2-full_scale/2**16
	#bit_2s_comp = int( 0x10000 * ( dec + full_scale/2)    / full_scale ) + 0x8000
	bit_2s_comp = int( 0x10000 * ( dec + full_scale/2)    / full_scale +0.5) + 0x8000
	if bit_2s_comp > 0xFFFF :
		bit_2s_comp -= 0x10000
	return bit_2s_comp

#test_codes = [ conv_dec_to_bit_2s_comp_16bit(x) for x in [-10,-5,0,5,10] ]
#print(test_codes)
	
def conv_bit_2s_comp_16bit_to_dec(bit_2s_comp, full_scale=20):
	if bit_2s_comp >= 0x8000:
		bit_2s_comp -= 0x8000
		#dec = full_scale * (bit_2s_comp) / 0x10000 -10
		dec = full_scale * (bit_2s_comp) / 0x10000 - full_scale/2
	else :
		dec = full_scale * (bit_2s_comp) / 0x10000
		if dec == full_scale/2-full_scale/2**16 :
			dec = full_scale/2
	return dec

#test_codes2 = [ conv_bit_2s_comp_16bit_to_dec(x) for x in test_codes ]
#print(test_codes2)

	


###########################################################################
## open LAN socket  ####################################################

import socket

## socket control parameters 

HOST = '192.168.168.143'  # The server's hostname or IP address // MHVSU-BASE test ip
HOST143 = '192.168.168.143'  # The server's hostname or IP address // MHVSU-BASE test ip
PORT = 5025               # The port used by the server
#TIMEOUT = 5.3 # socket timeout
TIMEOUT = 500 # socket timeout // for debug 
#TIMEOUT = 1000 # socket timeout // for debug 1000s
SO_SNDBUF = 2048
SO_RCVBUF = 32768
INTVAL = 0.1 # sec for waiting before recv()
BUF_SIZE_NORMAL = 2048
BUF_SIZE_LARGE = 16384
TIMEOUT_LARGE = TIMEOUT*10

ss = None # socket




## command strings ##############################################################
cmd_str__IDN      = b'*IDN?\n'
cmd_str__RST      = b'*RST\n'
cmd_str__EPS_EN   = b':EPS:EN'
cmd_str__EPS_WMI  = b':EPS:WMI'
cmd_str__EPS_WMO  = b':EPS:WMO'
cmd_str__EPS_TAC  = b':EPS:TAC'
cmd_str__EPS_TMO  = b':EPS:TMO' 
cmd_str__EPS_TWO  = b':EPS:TWO' ## new
cmd_str__EPS_PI   = b':EPS:PI'
cmd_str__EPS_PO   = b':EPS:PO'

##  cmd_str__EPS_MKWI = b':EPS:MKWI'
##  cmd_str__EPS_MKWO = b':EPS:MKWO'
##  cmd_str__EPS_MKTI = b':EPS:MKTI'
##  cmd_str__EPS_MKTO = b':EPS:MKTO'
##  cmd_str__EPS_WI   = b':EPS:WI'
##  cmd_str__EPS_WO   = b':EPS:WO'
##  cmd_str__EPS_TI   = b':EPS:TI'
##  cmd_str__EPS_TO   = b':EPS:TO'
#

## scpi functions ####################################################################

def scpi_open (timeout=TIMEOUT):
	try:
		ss = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		ss.settimeout(timeout)
		ss.setsockopt(socket.SOL_SOCKET, socket.SO_SNDBUF, SO_SNDBUF)
		ss.setsockopt(socket.SOL_SOCKET, socket.SO_RCVBUF, SO_RCVBUF) # 8192 16384 32768 65536
	except OSError as msg:
		ss = None
		print('error in socket: ', msg)
		raise
	return ss

def scpi_connect (ss, HOST, PORT):
	try:
		ss.connect((HOST, PORT))
	except OSError as msg:
		ss.close()
		ss = None
		print('error in connect: ', msg)
		raise


def scpi_close (ss):
	try:
		ss.close()
	except:
		if ss == None:
			print('error: ss==None')
		raise

def scpi_comm_resp_ss (ss, cmd_str, buf_size=BUF_SIZE_NORMAL, intval=INTVAL) :
	try:
		if __debug__:print('Send:', repr(cmd_str[:40]))
		ss.sendall(cmd_str)
	except:
		if __debug__:print('error in sendall')
		raise
	##
	sleep(intval)
	#
	# recv data until finding the sentinel '\n'
	try:
		data = ss.recv(buf_size) # try 1024 131072 524288
		# try   
		while (1):
			if (chr(data[-1])=='\n'): # check the sentinel '\n' 
				break
			data = data + ss.recv(buf_size)
	except:
		if __debug__:print('error in recv')
		raise
	#
	## check response 
	if (len(data)>20):
		if __debug__:print('Received:', repr(data[0:20]),  ' (first 20 bytes)')
	else:
		if __debug__:print('Received:', repr(data))
	#
	# NG response check 
	if data[0:2]==b'NG':
		if __debug__:print('Received: NG as response')
		#input('Press Enter key!')
		#sleep(3)
	#
	return data


# scpi command for numeric block response
def scpi_comm_resp_numb_ss (ss, cmd_str, buf_size=BUF_SIZE_LARGE, intval=INTVAL, timeout_large=TIMEOUT_LARGE) :
	try:
		if __debug__:print('Send:', repr(cmd_str))
		ss.sendall(cmd_str)
	except:
		if __debug__:print('error in sendall')
		raise
	##
	sleep(intval)
	#
	# cmd: ":PGEP:PO#HBC 524288\n"
	# rsp: "#4_001024_rrrrrrrrrr...rrrrrrrrrr\n"
	#
	# recv data until finding the sentinel '\n' 
	# but check the sentinel after the data byte count is met.
	#
	# read timeout
	to = ss.gettimeout()
	#print(to)
	# increase timeout
	ss.settimeout(timeout_large)
	#
	try:
		# find the numeric head : must 10 in data 
		data = ss.recv(buf_size)
		while True:
			if len(data)>=10:
				break
			data = data + ss.recv(buf_size)
		#
		#print('header: ', repr(data[0:10])) # header
		#
		# find byte count 
		byte_count = int(data[3:9])
		#print('byte_count=', repr(byte_count)) 
		#
		# collect all data by byte count
		count_to_recv = byte_count + 10 + 1# add header count #add /n
		while True:
			if len(data)>=count_to_recv:
				break
			data = data + ss.recv(buf_size)
		#
		# check the sentinel 
		while True:
			if (chr(data[-1])=='\n'): # check the sentinel '\n' 
				break
			data = data + ss.recv(buf_size)
		#
	except:
		if __debug__:print('error in recv')
		raise
	#
	if (len(data)>20):
		if __debug__:print('Received:', repr(data[0:20]),  ' (first 20 bytes)')
	else:
		if __debug__:print('Received:', repr(data))
	#
	# timeout back to prev
	ss.settimeout(to)
	#
	data = data[10:(10+byte_count)]
	if __debug__:print('data:', data[0:20].hex(),  ' (first 20 bytes)')
	#
	return [byte_count, data]
	



###########################################################################
## open OK USB device  ####################################################

##  import ok
##  
##  # OK init
##  dev = ok.okCFrontPanel()
##  
##  # OK open
##  DeviceCount = dev.GetDeviceCount()
##  print(' {}={}'.format('DeviceCount',DeviceCount))
##  dev.OpenBySerial("")
##  DevSR = dev.GetSerialNumber()
##  print(' {}={}'.format('DevSR',DevSR))


###########################################################################
## EPS_Dev replaces USB dev class  ########################################

class EPS_Dev:
	dev_count = 0
	idn = []
	ss = None # socket
	#
	f_scpi_open = scpi_open
	f_scpi_connect = scpi_connect
	f_scpi_close = scpi_close
	f_scpi_cmd = scpi_comm_resp_ss
	f_scpi_cmd_numb = scpi_comm_resp_numb_ss
	#
	def _test(self):
		return '_class__EPS_Dev_'
	#
	def GetDeviceCount(self):
		# must update from ping ip ... or else 
		self.dev_count = 1
		return self.dev_count 
	#
	def Init(self):
		# nothing
		pass
	def Open(self, hh=HOST, pp=PORT):
		##  # open scpi
		##  self.ss = LAN_CMU_Dev.f_scpi_open()		
		##  # connect scpi
		##  LAN_CMU_Dev.f_scpi_connect(self.ss,hh,pp)
		##  # board reset 
		##  ret = LAN_CMU_Dev.f_scpi_cmd(self.ss, cmd_str__RST).decode()
		##  # LAN end-point control enable
		##  ret = LAN_CMU_Dev.f_scpi_cmd(self.ss, cmd_str__CMEP_EN+b' ON\n').decode()
		##  return ret
		
		# 
		self.ss = EPS_Dev.f_scpi_open()
		try:
			print('>> try to connect : {}:{}'.format(hh,pp))
			EPS_Dev.f_scpi_connect(self.ss,hh,pp)
		except socket.timeout:
			self.ss = None
		except ConnectionRefusedError:
			self.ss = None
		except:
			raise
		return self.ss
	#
	def IsOpen(self):
		if self.ss == None:
			ret = False
		else:
			ret = True
		return ret	
	#
	def GetSerialNumber(self):
		ret = EPS_Dev.f_scpi_cmd(self.ss, cmd_str__IDN).decode() # will revise
		return ret # must come from board later 
	def ConfigureFPGA(self, opt=[]):
		# not support
		pass
		return 0
	def GetErrorString(self, opt):
		# not support
		pass
		return []
	def Close(self):
		##  ret = EPS_Dev.f_scpi_cmd(self.ss, cmd_str__CMEP_EN+b' OFF\n').decode()
		# close scpi
		EPS_Dev.f_scpi_close(self.ss)
		self.ss = None
		return ret
	#
	def GetWireOutValue(self, adrs, mask=0xFFFFFFFF):
		# :EPS:WMO#Hnn  #Hmmmmmmmm<NL>
		cmd_str = cmd_str__EPS_WMO + ('#H{:02X} #H{:08X}\n'.format(adrs,mask)).encode()
		rsp_str = EPS_Dev.f_scpi_cmd(self.ss, cmd_str)
		#
		rsp = rsp_str.decode()
		# assume hex decimal response: #HF3190306<NL>
		rsp = '0x' + rsp[2:-1] # convert "#HF3190306<NL>" --> "0xF3190306"
		rsp = int(rsp,16) # convert hex into int
		return rsp
	def UpdateWireOuts(self):
		# no global update : nothing to do.
		pass
	def SetWireInValue(self, adrs, data, mask=0xFFFFFFFF):
		# :EPS:WMI#Hnn  #Hnnnnnnnn #Hmmmmmmmm<NL>
		cmd_str = cmd_str__EPS_WMI + ('#H{:02X} #H{:08X} #H{:08X}\n'.format(adrs,data,mask)).encode()
		rsp_str = EPS_Dev.f_scpi_cmd(self.ss, cmd_str)
		#
		rsp = rsp_str.decode()
		return rsp
	def UpdateWireIns(self):
		# no global update : nothing to do.
		pass
	#
	def ActivateTriggerIn(self, adrs, loc_bit):
		## activate trig 
		# :EPS:TAC#Hnn  #Hnn<NL>
		#
		cmd_str = cmd_str__EPS_TAC + ('#H{:02X} #H{:02X}\n'.format(adrs,loc_bit)).encode()
		rsp_str = EPS_Dev.f_scpi_cmd(self.ss, cmd_str)
		rsp = rsp_str.decode()
		return rsp
	#
	def UpdateTriggerOuts(self) :
		# no global update : nothing to do.
		pass
	#
	def IsTriggered (self, adrs, mask):
		# cmd: ":EPS:TMO#H60 #H0000FFFF\n"
		# rsp: "ON\n" or "OFF\n"
		cmd_str = cmd_str__EPS_TMO + ('#H{:02X} #H{:08X}\n'.format(adrs,mask)).encode()
		rsp_str = EPS_Dev.f_scpi_cmd(self.ss, cmd_str)
		#
		rsp = rsp_str.decode()
		#
		if rsp[0:3]=='OFF':
			ret =  False
		elif rsp[0:2]=='ON':
			ret =  True
		else:
			ret =  None
		#
		return ret
	#
	def GetTriggerOutVector(self, adrs, mask=0xFFFFFFFF):
		# cmd: ":EPS:TWO#H60 #H0000FFFF\n"
		# rsp: "#H000O3245\n" 
		cmd_str = cmd_str__EPS_TWO + ('#H{:02X} #H{:08X}\n'.format(adrs,mask)).encode()
		rsp_str = EPS_Dev.f_scpi_cmd(self.ss, cmd_str)
		#
		rsp = rsp_str.decode()
		# assume hex decimal response: #HF3190306<NL>
		rsp = '0x' + rsp[2:-1] # convert "#HF3190306<NL>" --> "0xF3190306"
		rsp = int(rsp,16) # convert hex into int
		return rsp
	#
	def ReadFromPipeOut(self, adrs, data_bytearray):
		## read pipeout
		# cmd: ":EPS:PO#HAA 001024\n"
		# rsp: "#4_001024_rrrrrrrrrr...rrrrrrrrrr\n"		
		#
		byte_count = len(data_bytearray)
		#
		cmd_str = cmd_str__EPS_PO + ('#H{:02X} {:06d}\n'.format(adrs,byte_count)).encode()
		if __debug__:print(cmd_str)
		#
		[rsp_cnt, rsp_str] = EPS_Dev.f_scpi_cmd_numb(self.ss, cmd_str)
		#
		# assume numeric block : "#4_001024_rrrrrrrrrr...rrrrrrrrrr\n"
		# assume rsp_str is data part.
		# copy data 
		for ii in range(0,rsp_cnt): 
			data_bytearray[ii] = rsp_str[ii]
		#
		return rsp_cnt

	def WriteToPipeIn(self, adrs, data_bytearray):
		## write pipein
		# cmd: ":EPS:PI#H8A #4_001024_rrrrrrrrrr...rrrrrrrrrr\n"
		# rsp: "OK\n"		
		
		# check later : byte_count is multiple of 4 
		
		#
		byte_count = len(data_bytearray)
		#
		#cmd_str = cmd_str__EPS_PI + ('#H{:02X} #4_{:06d}_{}\n'.format(adrs,byte_count,data_bytearray)).encode() # NG
		cmd_str = cmd_str__EPS_PI + ('#H{:02X} #4_{:06d}_'.format(adrs,byte_count)).encode() + data_bytearray + b'\n'
		if __debug__:print(cmd_str[:30])
		
		rsp_str = EPS_Dev.f_scpi_cmd(self.ss, cmd_str)
		
		rsp = rsp_str.decode() # OK or NG
		
		#
		return rsp
		

# class init
dev = EPS_Dev()
# test
print(dev._test())


###########################################################################
# check debug mode
if __debug__:
	print('>>> In debug mode ... ')


###########################################################################
## load shell parameters for IP 

# example command in shell: 
#    python test__MHVSU_BASE__lan.py  192.168.168.143  5025
#    slot id -- default ip address 
#    0x0     -- 192.168.168.128
#    0x1     -- 192.168.168.129
#    0x2     -- 192.168.168.130
#    0x3     -- 192.168.168.131
#    0x4     -- 192.168.168.132
#    0x5     -- 192.168.168.133
#    0x6     -- 192.168.168.134
#    0x7     -- 192.168.168.135
#    0x8     -- 192.168.168.136
#    0x9     -- 192.168.168.137
#    0xA     -- 192.168.168.138
#    0xB     -- 192.168.168.139
#    0xC     -- 192.168.168.140
#    0xE     -- 192.168.168.141
#    0xD     -- 192.168.168.142
#    0xF     -- 192.168.168.143 (slot id is not detected)
_host_names_ = [
	'192.168.168.143',
	'192.168.168.139',
	'192.168.168.140',
	'192.168.168.141',
	'192.168.168.142',
	'192.168.168.128',
	'192.168.168.129',
	'192.168.168.130',
	'192.168.168.131',
	'192.168.168.132',
	'192.168.168.133',
	'192.168.168.134',
	'192.168.168.135',
	'192.168.168.136',
	'192.168.168.137',
	'192.168.168.138']

print(sys.argv[0])
argc=len(sys.argv)
print(argc)
#
if argc>1:
	_host_                    = sys.argv[1]         # ex: 192.168.168.143
else:
	#_host_                    = '192.168.168.143'
	for xx in _host_names_:
		if ping(xx):
			_host_ = xx
			print('{} is available.'.format(xx))
			break 
		else:
			print('{} is NOT available.'.format(xx))
#
if argc>2:
	_port_                    = int  (sys.argv[2])  # ex: 5025
else:
	_port_                    = 5025
#


###########################################################################
## open socket and connect scpi server ####################################

#ss = dev.Open(HOST143,PORT) # firmware test
ss = dev.Open(_host_,_port_) # firmware test

#if ss == None :
#	raise


## flag check max retry count
MAX_count = 50
##MAX_count = 500 # 20000 # 280 for 192000 samples
#MAX_count = 5000 # 1500 for 25600 samples @ 12500 count period

## note 
# (1 / ((192 MHz) / 12500)) * 2560 = 166.666667 milliseconds
# (1 / ((192 MHz) / 12500)) * 5120 = 333.333333 milliseconds


if dev.IsOpen():
	print('>>> device opened.')
else:
	print('>>> device is not open.')
	## raise
	input('')
	MAX_count = 3


ret = dev.GetSerialNumber()
print(ret)

# input('stop!!')

###########################################################################

### scpi : *IDN?
print('\n>>> {} : {}'.format('Test',cmd_str__IDN))
rsp = scpi_comm_resp_ss(ss, cmd_str__IDN)
print('hex code rcvd: ' + rsp.hex())
print('string rcvd: ' + repr(rsp))


### scpi : ":EPS:EN?"
print('\n>>> {} : {}'.format('Test',cmd_str__EPS_EN))
rsp = scpi_comm_resp_ss(ss, cmd_str__EPS_EN +b'?\n')
#print('hex code rcvd: ' + rsp.hex())
print('string rcvd: ' + repr(rsp))


#### check fpga ID through dedicated EP vs PORT EP
#    ADRS_FPGA_IMAGE_MHVSU
#    ADRS_PORT_WO_20_MHVSU : ":EPS:WMO#H23 #HFFFFFFFF\n"

### scpi : ":EPS:WMO#H20 #HFFFFFFFF\n"
print('\n>>> {} : {}'.format('Test',cmd_str__EPS_WMO))
rsp = scpi_comm_resp_ss(ss, cmd_str__EPS_WMO +b'#H20 #HFFFFFFFF\n')
#print('hex code rcvd: ' + rsp.hex())
print('string rcvd: ' + repr(rsp))

#### test LED 

print('\n>>> {} : {}'.format('Test',cmd_str__EPS_WMI))
### scpi : ":EPS:WMI#H03 #H00000000 #H0000000F\n"
### scpi : ":EPS:WMI#H03 #H0000000E #H0000000F\n"
### scpi : ":EPS:WMI#H03 #H0000000B #H0000000F\n"
### scpi : ":EPS:WMI#H03 #H00000007 #H0000000F\n"
### scpi : ":EPS:WMI#H03 #H0000000D #H0000000F\n"
### scpi : ":EPS:WMI#H03 #H0000000F #H0000000F\n"
rsp = scpi_comm_resp_ss(ss, cmd_str__EPS_WMI +b'#H03 #H00000000 #H0000000F\n')
sleep(0.1)
rsp = scpi_comm_resp_ss(ss, cmd_str__EPS_WMI +b'#H03 #H0000000E #H0000000F\n')
sleep(0.1)
rsp = scpi_comm_resp_ss(ss, cmd_str__EPS_WMI +b'#H03 #H0000000B #H0000000F\n')
sleep(0.1)
rsp = scpi_comm_resp_ss(ss, cmd_str__EPS_WMI +b'#H03 #H00000007 #H0000000F\n')
sleep(0.1)
rsp = scpi_comm_resp_ss(ss, cmd_str__EPS_WMI +b'#H03 #H0000000D #H0000000F\n')
sleep(0.1)
rsp = scpi_comm_resp_ss(ss, cmd_str__EPS_WMI +b'#H03 #H0000000F #H0000000F\n')
sleep(0.1)

#### test count2 for trigger 

#	xil_printf(">>> test count :  clear autocount2 \r\n");
#	write_mcs_ep_wi(ADRS_BASE_MHVSU, 0x01, 0x00000000, 0x00000004); // adrs_base, EP_offset_EP, data, mask
rsp = scpi_comm_resp_ss(ss, cmd_str__EPS_WMI +b'#H01 #H00000000 #H00000004\n')
sleep(0.2)
#
#	xil_printf(">>> test count2 \r\n");
#	// read  : test counts at WO21 
#	value = read_mcs_ep_wo(ADRS_BASE_MHVSU, 0x21, 0x0000FFFF);
rsp = scpi_comm_resp_ss(ss, cmd_str__EPS_WMO +b'#H21 #H0000FFFF\n')
#	xil_printf("WO21 rd: 0x%08X \r\n", value );
#	//
#	activate_mcs_ep_ti(ADRS_BASE_MHVSU,0x41,0); // reset : test count2 TI41[0]
rsp = scpi_comm_resp_ss(ss, cmd_str__EPS_TAC +b'#H41 #H00\n')
#	value = read_mcs_ep_wo(ADRS_BASE_MHVSU, 0x21, 0x0000FFFF);
rsp = scpi_comm_resp_ss(ss, cmd_str__EPS_WMO +b'#H21 #H0000FFFF\n')
#	xil_printf("WO21 rd: 0x%08X \r\n", value );
#	//
#	activate_mcs_ep_ti(ADRS_BASE_MHVSU,0x41,1); // up    : test count2 TI41[1] 
rsp = scpi_comm_resp_ss(ss, cmd_str__EPS_TAC +b'#H41 #H01\n')
#	value = read_mcs_ep_wo(ADRS_BASE_MHVSU, 0x21, 0x0000FFFF);
rsp = scpi_comm_resp_ss(ss, cmd_str__EPS_WMO +b'#H21 #H0000FFFF\n')
#	xil_printf("WO21 rd: 0x%08X \r\n", value );
#	//
#	activate_mcs_ep_ti(ADRS_BASE_MHVSU,0x41,2); // down  : test count2 TI41[2] 
rsp = scpi_comm_resp_ss(ss, cmd_str__EPS_TAC +b'#H41 #H02\n')
#	value = read_mcs_ep_wo(ADRS_BASE_MHVSU, 0x21, 0x0000FFFF);
rsp = scpi_comm_resp_ss(ss, cmd_str__EPS_WMO +b'#H21 #H0000FFFF\n')
#	xil_printf("WO21 rd: 0x%08X \r\n", value );
#
#	// test count :  set autocount2
#	xil_printf(">>> test count :  set autocount2 \r\n");
#	write_mcs_ep_wi(ADRS_BASE_MHVSU, 0x01, 0x00000004, 0x00000004); // adrs_base, EP_offset_EP, data, mask
rsp = scpi_comm_resp_ss(ss, cmd_str__EPS_WMI +b'#H01 #H00000004 #H00000004\n')


######################################################
## TODO: check MHVSU FPGA image ID 

### adc test ###
#__FPGA_IMG_IMP_INFO__ = 0xD220 
#__FPGA_IMG_IMP_DATE__ = 0x0610
#BIT_FILENAME = './img_h_D2_20_0610_c/xem7310__mhvsu_base__top.bit'
### adc-acc test ###
#__FPGA_IMG_IMP_INFO__ = 0xD220 
#__FPGA_IMG_IMP_DATE__ = 0x0618
#BIT_FILENAME = ''
### adc-acc 192MHz test ###
#__FPGA_IMG_IMP_INFO__ = 0xD220 
#__FPGA_IMG_IMP_DATE__ = 0x0619
#BIT_FILENAME = ''
### adc-p_clk test ###
#__FPGA_IMG_IMP_INFO__ = 0xD320 
#__FPGA_IMG_IMP_DATE__ = 0x0622
#BIT_FILENAME = ''
### adc-min-max test ###
#__FPGA_IMG_IMP_INFO__ = 0xD320 
#__FPGA_IMG_IMP_DATE__ = 0x0623
#BIT_FILENAME = ''
### adc-fifo test ###
#__FPGA_IMG_IMP_INFO__ = 0xD320 
#__FPGA_IMG_IMP_DATE__ = 0x0629
#BIT_FILENAME = ''
### ext-trig test ###
#__FPGA_IMG_IMP_INFO__ = 0xD320 
#__FPGA_IMG_IMP_DATE__ = 0x0709
#BIT_FILENAME = ''
### ext-trig test + M1-enabled + hs adc test pattern ###
#__FPGA_IMG_IMP_INFO__ = 0xD520 
#__FPGA_IMG_IMP_DATE__ = 0x0710
#BIT_FILENAME = ''
### acc 32bit support + sspi wo ###
#__FPGA_IMG_IMP_INFO__ = 0xD620 
#__FPGA_IMG_IMP_DATE__ = 0x0712
#BIT_FILENAME = ''
### SPIO forced mode support ###
#__FPGA_IMG_IMP_INFO__ = 0xD620 
#__FPGA_IMG_IMP_DATE__ = 0x0714
#BIT_FILENAME = ''
### LAN support ###
#__FPGA_IMG_IMP_INFO__ = 0xD820 
#__FPGA_IMG_IMP_DATE__ = 0x0828
#BIT_FILENAME = ''
### EEPROM test ###
#__FPGA_IMG_IMP_INFO__ = 0xD920 
#__FPGA_IMG_IMP_DATE__ = 0x0923
#BIT_FILENAME = ''
### new base board support MHVSU-BASE-REV2 (B0301) ###
#__FPGA_IMG_IMP_INFO__ = 0xDA20
#__FPGA_IMG_IMP_DATE__ = 0x1016
#BIT_FILENAME = ''
### new base board support MHVSU-BASE-REV2 (B0301) : slot ID update from EEPROM and LAN auto-selection
#__FPGA_IMG_IMP_INFO__ = 0xDA20
#__FPGA_IMG_IMP_DATE__ = 0x1021
#BIT_FILENAME = ''
### new base board support MHVSU-BASE-REV2 (B0301) : eeprom path control rev
#__FPGA_IMG_IMP_INFO__ = 0xDA20
#__FPGA_IMG_IMP_DATE__ = 0x1024
#BIT_FILENAME = ''
#
### new base board support MHVSU-BASE-REV2 (B0301) : sspi counters added
#__FPGA_IMG_IMP_INFO__ = 0xDA20
#__FPGA_IMG_IMP_DATE__ = 0x1028
#BIT_FILENAME = ''
#
### new base board support MHVSU-BASE-REV2 (B0301) : control master spi for sspi
#__FPGA_IMG_IMP_INFO__ = 0xDA20
#__FPGA_IMG_IMP_DATE__ = 0x1101
#BIT_FILENAME = ''
#
### new base board support MHVSU-BASE-REV2 (B0301) : DAC status register
__FPGA_IMG_IMP_INFO__ = 0xDA20
__FPGA_IMG_IMP_DATE__ = 0x1103
BIT_FILENAME = ''


dev.UpdateWireOuts()
FPGA_image_id = dev.GetWireOutValue(0x20)
print('{} = 0x{:08X}'.format('FPGA_image_id', FPGA_image_id))
# TODO: configure FPGA
if (FPGA_image_id==0): 
	error = dev.ConfigureFPGA(BIT_FILENAME)
	if error !=0:
		print('ConfigureFPGA Error {} : {}'.format(error,dev.GetErrorString(error)))
		## raise
		input('')
	# read again
	dev.UpdateWireOuts()
	FPGA_image_id = dev.GetWireOutValue(0x20)
	print('{} = 0x{:08X}'.format('FPGA_image_id', FPGA_image_id))
#
if not (FPGA_image_id>>16 == __FPGA_IMG_IMP_INFO__):
	print('>>> Please check FPGA iamge ID : ADC test.')
	## raise
	input('')	
#
if not (FPGA_image_id & 0xFFFF == __FPGA_IMG_IMP_DATE__):
	print('>>> Please check FPGA iamge ID : implmentation date.')
	## raise
	input('')


	
## input('stop!!')


###########################################################################
### GUI setup ###
## 
print('>>> tk GUI control')


#### GUI: Checkbutton and Button
from tkinter import *
from functools import partial

master   = Tk()
sub_spio = Toplevel(master)
sub_dac_ = Toplevel(master)
sub_adc_ = Toplevel(master)
sub_trig = Toplevel(master)

# titles 
master  .wm_title('MHVSU BASE board')
sub_spio.wm_title('SPIO control      ')
sub_dac_.wm_title('DAC  control      ')
sub_adc_.wm_title('ADC  control      ')
sub_trig.wm_title('EXT TRIG control  ')


### GUI variables ###

## gui variables for board status
ee_FPGA_IMAGE_ID_ = Entry(master, justify='right', width=12)
ee_TEST_FLAG_____ = Entry(master, justify='right', width=12)
ee_Slave_SPI_FLAG = Entry(master, justify='right', width=12)
ee_MON_XADC______ = Entry(master, justify='right', width=12)
#
ee_slot_id_______ = Entry(master, justify='right', width=8)
ee_board_status__ = Entry(master, justify='right', width=8)
ee_temp_fpga_____ = Entry(master, justify='right', width=8)
#
ee_DAC0_con______ = Entry(master, justify='right', width=8)
ee_DAC1_con______ = Entry(master, justify='right', width=8)
ee_DAC2_con______ = Entry(master, justify='right', width=8)
ee_DAC3_con______ = Entry(master, justify='right', width=8)
#
ee_DAC0_alert____ = Entry(master, justify='right', width=8)
ee_DAC1_alert____ = Entry(master, justify='right', width=8)
ee_DAC2_alert____ = Entry(master, justify='right', width=8)
ee_DAC3_alert____ = Entry(master, justify='right', width=8)
#
#
ee_FPGA_IMAGE_ID_.insert(0,"0x00000000")
ee_TEST_FLAG_____.insert(0,"0x00000000")
ee_Slave_SPI_FLAG.insert(0,"0x00000000")
ee_MON_XADC______.insert(0,"0x00000000")
#
ee_slot_id_______.insert(0,"0x0")
ee_board_status__.insert(0,"0x00")
ee_temp_fpga_____.insert(0,"0.0")
#
ee_DAC0_con______.insert(0,"0x0")
ee_DAC1_con______.insert(0,"0x0")
ee_DAC2_con______.insert(0,"0x0")
ee_DAC3_con______.insert(0,"0x0")
#
ee_DAC0_alert____.insert(0,"0x000")
ee_DAC1_alert____.insert(0,"0x000")
ee_DAC2_alert____.insert(0,"0x000")
ee_DAC3_alert____.insert(0,"0x000")


## gui variables for SPIO
# socket selections
# output nets

var_SKT1_EN = IntVar(value=1)  #SKT1 ENABLE
var_SKT2_EN = IntVar(value=1)  #SKT2 ENABLE
var_SKT3_EN = IntVar(value=1)  #SKT3 ENABLE
var_SKT4_EN = IntVar(value=1)  #SKT4 ENABLE
var_SKT5_EN = IntVar(value=1)  #SKT5 ENABLE
var_SKT6_EN = IntVar(value=1)  #SKT6 ENABLE
var_SKT7_EN = IntVar(value=1)  #SKT7 ENABLE
var_SKT8_EN = IntVar(value=1)  #SKT8 ENABLE

var_forced_mode_en   = IntVar() # forced mode control
var_forced_sig_mosi  = IntVar() # forced mode control
var_forced_sig_sclk  = IntVar() # forced mode control
var_forced_sig_csel  = IntVar() # forced mode control
var_forced_cs0       = IntVar() # forced mode control
var_forced_cs1       = IntVar() # forced mode control
var_forced_cs2       = IntVar() # forced mode control

var_CS0_GPB7 = IntVar()  #CS0 GPB7 CHECK_LED0
var_CS0_GPB6 = IntVar()  #CS0 GPB6 CH2_ADC_VM
var_CS0_GPB5 = IntVar()  #CS0 GPB5 CH2_GAIN_X20
var_CS0_GPB4 = IntVar()  #CS0 GPB4 CH2_GAIN_X2
var_CS0_GPB3 = IntVar()  #CS0 GPB3 NA
var_CS0_GPB2 = IntVar()  #CS0 GPB2 CH2_RANGE_20uA
var_CS0_GPB1 = IntVar()  #CS0 GPB1 CH2_RANGE_2mA
var_CS0_GPB0 = IntVar()  #CS0 GPB0 CH2_RANGE_50mA
var_CS0_GPA7 = IntVar()  #CS0 GPA7 NA
var_CS0_GPA6 = IntVar()  #CS0 GPA6 CH1_ADC_VM
var_CS0_GPA5 = IntVar()  #CS0 GPA5 CH1_GAIN_X20
var_CS0_GPA4 = IntVar()  #CS0 GPA4 CH1_GAIN_X2
var_CS0_GPA3 = IntVar()  #CS0 GPA3 NA
var_CS0_GPA2 = IntVar()  #CS0 GPA2 CH1_RANGE_20uA
var_CS0_GPA1 = IntVar()  #CS0 GPA1 CH1_RANGE_2mA
var_CS0_GPA0 = IntVar()  #CS0 GPA0 CH1_RANGE_50mA

var_CS1_GPB7 = IntVar()  #CS1 GPB7 CHECK_LED1
var_CS1_GPB6 = IntVar()  #CS1 GPB6 CH2_RELAY_DIAG
var_CS1_GPB5 = IntVar()  #CS1 GPB5 CH2_RELAY_OUTPUT
var_CS1_GPB4 = IntVar()  #CS1 GPB4 CH2_LIMIT_50mA
var_CS1_GPB3 = IntVar()  #CS1 GPB3 NA
var_CS1_GPB2 = IntVar()  #CS1 GPB2 NA
var_CS1_GPB1 = IntVar()  #CS1 GPB1 CH2_CMP_330p
var_CS1_GPB0 = IntVar()  #CS1 GPB0 CH2_CMP_100p
var_CS1_GPA7 = IntVar()  #CS1 GPA7 NA
var_CS1_GPA6 = IntVar()  #CS1 GPA6 CH1_RELAY_DIAG
var_CS1_GPA5 = IntVar()  #CS1 GPA5 CH1_RELAY_OUTPUT
var_CS1_GPA4 = IntVar()  #CS1 GPA4 CH1_LIMIT_50mA
var_CS1_GPA3 = IntVar()  #CS1 GPA3 NA
var_CS1_GPA2 = IntVar()  #CS1 GPA2 NA
var_CS1_GPA1 = IntVar()  #CS1 GPA1 CH1_CMP_330p
var_CS1_GPA0 = IntVar()  #CS1 GPA0 CH1_CMP_100p

var_CS2_GPB7 = IntVar()  #CS2 GPB7
var_CS2_GPB6 = IntVar()  #CS2 GPB6
var_CS2_GPB5 = IntVar()  #CS2 GPB5
var_CS2_GPB4 = IntVar()  #CS2 GPB4
var_CS2_GPB3 = IntVar()  #CS2 GPB3
var_CS2_GPB2 = IntVar()  #CS2 GPB2
var_CS2_GPB1 = IntVar()  #CS2 GPB1
var_CS2_GPB0 = IntVar()  #CS2 GPB0
var_CS2_GPA7 = IntVar()  #CS2 GPA7
var_CS2_GPA6 = IntVar()  #CS2 GPA6
var_CS2_GPA5 = IntVar()  #CS2 GPA5
var_CS2_GPA4 = IntVar()  #CS2 GPA4
var_CS2_GPA3 = IntVar()  #CS2 GPA3
var_CS2_GPA2 = IntVar()  #CS2 GPA2
var_CS2_GPA1 = IntVar()  #CS2 GPA1
var_CS2_GPA0 = IntVar()  #CS2 GPA0

## gui variables for DAC
# DAC value input 
# DAC readback

ee_dac_skt1_ch1_val = Entry(sub_dac_, justify='right', width=14) 
ee_dac_skt2_ch1_val = Entry(sub_dac_, justify='right', width=14) 
ee_dac_skt3_ch1_val = Entry(sub_dac_, justify='right', width=14) 
ee_dac_skt4_ch1_val = Entry(sub_dac_, justify='right', width=14) 
ee_dac_skt5_ch1_val = Entry(sub_dac_, justify='right', width=14) 
ee_dac_skt6_ch1_val = Entry(sub_dac_, justify='right', width=14) 
ee_dac_skt7_ch1_val = Entry(sub_dac_, justify='right', width=14) 
ee_dac_skt8_ch1_val = Entry(sub_dac_, justify='right', width=14) 
ee_dac_skt1_ch2_val = Entry(sub_dac_, justify='right', width=14) 
ee_dac_skt2_ch2_val = Entry(sub_dac_, justify='right', width=14) 
ee_dac_skt3_ch2_val = Entry(sub_dac_, justify='right', width=14) 
ee_dac_skt4_ch2_val = Entry(sub_dac_, justify='right', width=14) 
ee_dac_skt5_ch2_val = Entry(sub_dac_, justify='right', width=14) 
ee_dac_skt6_ch2_val = Entry(sub_dac_, justify='right', width=14) 
ee_dac_skt7_ch2_val = Entry(sub_dac_, justify='right', width=14) 
ee_dac_skt8_ch2_val = Entry(sub_dac_, justify='right', width=14) 

ee_dac_skt1_ch1_rdb = Entry(sub_dac_, justify='right', width=14)  
ee_dac_skt2_ch1_rdb = Entry(sub_dac_, justify='right', width=14)  
ee_dac_skt3_ch1_rdb = Entry(sub_dac_, justify='right', width=14)  
ee_dac_skt4_ch1_rdb = Entry(sub_dac_, justify='right', width=14)  
ee_dac_skt5_ch1_rdb = Entry(sub_dac_, justify='right', width=14)  
ee_dac_skt6_ch1_rdb = Entry(sub_dac_, justify='right', width=14)  
ee_dac_skt7_ch1_rdb = Entry(sub_dac_, justify='right', width=14)  
ee_dac_skt8_ch1_rdb = Entry(sub_dac_, justify='right', width=14)  
ee_dac_skt1_ch2_rdb = Entry(sub_dac_, justify='right', width=14) 
ee_dac_skt2_ch2_rdb = Entry(sub_dac_, justify='right', width=14)  
ee_dac_skt3_ch2_rdb = Entry(sub_dac_, justify='right', width=14)  
ee_dac_skt4_ch2_rdb = Entry(sub_dac_, justify='right', width=14)  
ee_dac_skt5_ch2_rdb = Entry(sub_dac_, justify='right', width=14)  
ee_dac_skt6_ch2_rdb = Entry(sub_dac_, justify='right', width=14)  
ee_dac_skt7_ch2_rdb = Entry(sub_dac_, justify='right', width=14)  
ee_dac_skt8_ch2_rdb = Entry(sub_dac_, justify='right', width=14)  

ee_dac_skt1_ch1_val.insert(0,"0")
ee_dac_skt2_ch1_val.insert(0,"0")
ee_dac_skt3_ch1_val.insert(0,"0")
ee_dac_skt4_ch1_val.insert(0,"0")
ee_dac_skt5_ch1_val.insert(0,"0")
ee_dac_skt6_ch1_val.insert(0,"0")
ee_dac_skt7_ch1_val.insert(0,"0")
ee_dac_skt8_ch1_val.insert(0,"0")
ee_dac_skt1_ch2_val.insert(0,"0")
ee_dac_skt2_ch2_val.insert(0,"0")
ee_dac_skt3_ch2_val.insert(0,"0")
ee_dac_skt4_ch2_val.insert(0,"0")
ee_dac_skt5_ch2_val.insert(0,"0")
ee_dac_skt6_ch2_val.insert(0,"0")
ee_dac_skt7_ch2_val.insert(0,"0")
ee_dac_skt8_ch2_val.insert(0,"0")

ee_dac_skt1_ch1_rdb.insert(0,"0")
ee_dac_skt2_ch1_rdb.insert(0,"0")
ee_dac_skt3_ch1_rdb.insert(0,"0")
ee_dac_skt4_ch1_rdb.insert(0,"0")
ee_dac_skt5_ch1_rdb.insert(0,"0")
ee_dac_skt6_ch1_rdb.insert(0,"0")
ee_dac_skt7_ch1_rdb.insert(0,"0")
ee_dac_skt8_ch1_rdb.insert(0,"0")
ee_dac_skt1_ch2_rdb.insert(0,"0")
ee_dac_skt2_ch2_rdb.insert(0,"0")
ee_dac_skt3_ch2_rdb.insert(0,"0")
ee_dac_skt4_ch2_rdb.insert(0,"0")
ee_dac_skt5_ch2_rdb.insert(0,"0")
ee_dac_skt6_ch2_rdb.insert(0,"0")
ee_dac_skt7_ch2_rdb.insert(0,"0")
ee_dac_skt8_ch2_rdb.insert(0,"0")


## gui variables for ADC
var_ADC_PWR_ON           = IntVar(value=1) # ADC power on 
var_ADC_ACC_disable      = IntVar(value=0) # disable ADC-ACC bit-shift
var_ADC_ACC_16bit_shift  = IntVar(value=0) # set ACC 16 bit shift
var_ADC_MIN_ON           = IntVar(value=0) # enable ADC MIN port enable
var_ADC_MAX_ON           = IntVar(value=0) # enable ADC MAX port enable	
#
ee_adc_con_wi = Entry(sub_adc_, justify='right', width=12) # ADC control in 
ee_adc_con_wi.insert(0,"0x00000000")
#
ee_adc_sta_wo = Entry(sub_adc_, justify='right', width=12) # ADC status out
ee_adc_sta_wo.insert(0,"0x00000000")
#
ee_adc_period_wi = Entry(sub_adc_, justify='right', width=12) # ADC period in 
ee_adc_period_wi.insert(0,"12500") #  19200 vs 12500 for 1PLC @ 192MHz # 21000 vs 13672 for 1PLC @ 210MHz #
# 1/(60Hz) = 1/(192MHz)*256*n
# n = (192MHz)/(60Hz)/256 = 12500
# (192MHz)/12500 = 15.36kHz
ee_adc_num_samples_wi = Entry(sub_adc_, justify='right', width=12) # ADC number of samples in 
ee_adc_num_samples_wi.insert(0,"256") # 168 vs 256 for 1PLC
#
# ADC codes : packed as ch2,ch1
ee_adc_s1_wo = Entry(sub_adc_, justify='right', width=12) 
ee_adc_s2_wo = Entry(sub_adc_, justify='right', width=12) 
ee_adc_s3_wo = Entry(sub_adc_, justify='right', width=12) 
ee_adc_s4_wo = Entry(sub_adc_, justify='right', width=12) 
ee_adc_s5_wo = Entry(sub_adc_, justify='right', width=12) 
ee_adc_s6_wo = Entry(sub_adc_, justify='right', width=12) 
ee_adc_s7_wo = Entry(sub_adc_, justify='right', width=12) 
ee_adc_s8_wo = Entry(sub_adc_, justify='right', width=12) 
#
ee_adc_s1_wo.insert(0,"0x00000000")
ee_adc_s2_wo.insert(0,"0x00000000")
ee_adc_s3_wo.insert(0,"0x00000000")
ee_adc_s4_wo.insert(0,"0x00000000")
ee_adc_s5_wo.insert(0,"0x00000000")
ee_adc_s6_wo.insert(0,"0x00000000")
ee_adc_s7_wo.insert(0,"0x00000000")
ee_adc_s8_wo.insert(0,"0x00000000")
#
# ADC values (voltage)
ee_adc_volt_s1_ch2 = Entry(sub_adc_, justify='right', width=12); ee_adc_volt_s1_ch1 = Entry(sub_adc_, justify='right', width=12)
ee_adc_volt_s2_ch2 = Entry(sub_adc_, justify='right', width=12); ee_adc_volt_s2_ch1 = Entry(sub_adc_, justify='right', width=12)
ee_adc_volt_s3_ch2 = Entry(sub_adc_, justify='right', width=12); ee_adc_volt_s3_ch1 = Entry(sub_adc_, justify='right', width=12)
ee_adc_volt_s4_ch2 = Entry(sub_adc_, justify='right', width=12); ee_adc_volt_s4_ch1 = Entry(sub_adc_, justify='right', width=12)
ee_adc_volt_s5_ch2 = Entry(sub_adc_, justify='right', width=12); ee_adc_volt_s5_ch1 = Entry(sub_adc_, justify='right', width=12)
ee_adc_volt_s6_ch2 = Entry(sub_adc_, justify='right', width=12); ee_adc_volt_s6_ch1 = Entry(sub_adc_, justify='right', width=12)
ee_adc_volt_s7_ch2 = Entry(sub_adc_, justify='right', width=12); ee_adc_volt_s7_ch1 = Entry(sub_adc_, justify='right', width=12)
ee_adc_volt_s8_ch2 = Entry(sub_adc_, justify='right', width=12); ee_adc_volt_s8_ch1 = Entry(sub_adc_, justify='right', width=12)
#
ee_adc_volt_s1_ch2.insert(0,"0"); ee_adc_volt_s1_ch1.insert(0,"0")
ee_adc_volt_s2_ch2.insert(0,"0"); ee_adc_volt_s2_ch1.insert(0,"0")
ee_adc_volt_s3_ch2.insert(0,"0"); ee_adc_volt_s3_ch1.insert(0,"0")
ee_adc_volt_s4_ch2.insert(0,"0"); ee_adc_volt_s4_ch1.insert(0,"0")
ee_adc_volt_s5_ch2.insert(0,"0"); ee_adc_volt_s5_ch1.insert(0,"0")
ee_adc_volt_s6_ch2.insert(0,"0"); ee_adc_volt_s6_ch1.insert(0,"0")
ee_adc_volt_s7_ch2.insert(0,"0"); ee_adc_volt_s7_ch1.insert(0,"0")
ee_adc_volt_s8_ch2.insert(0,"0"); ee_adc_volt_s8_ch1.insert(0,"0")
#
# ADC_ACC codes : packed as ch2,ch1
ee_adc_acc_s1_wo = Entry(sub_adc_, justify='right', width=12) 
ee_adc_acc_s2_wo = Entry(sub_adc_, justify='right', width=12) 
ee_adc_acc_s3_wo = Entry(sub_adc_, justify='right', width=12) 
ee_adc_acc_s4_wo = Entry(sub_adc_, justify='right', width=12) 
ee_adc_acc_s5_wo = Entry(sub_adc_, justify='right', width=12) 
ee_adc_acc_s6_wo = Entry(sub_adc_, justify='right', width=12) 
ee_adc_acc_s7_wo = Entry(sub_adc_, justify='right', width=12) 
ee_adc_acc_s8_wo = Entry(sub_adc_, justify='right', width=12) 
#
ee_adc_acc_s1_wo.insert(0,"0x00000000")
ee_adc_acc_s2_wo.insert(0,"0x00000000")
ee_adc_acc_s3_wo.insert(0,"0x00000000")
ee_adc_acc_s4_wo.insert(0,"0x00000000")
ee_adc_acc_s5_wo.insert(0,"0x00000000")
ee_adc_acc_s6_wo.insert(0,"0x00000000")
ee_adc_acc_s7_wo.insert(0,"0x00000000")
ee_adc_acc_s8_wo.insert(0,"0x00000000")
#
# ADC_ACC values (voltage)
ee_adc_acc_volt_s1_ch2 = Entry(sub_adc_, justify='right', width=12); ee_adc_acc_volt_s1_ch1 = Entry(sub_adc_, justify='right', width=12)
ee_adc_acc_volt_s2_ch2 = Entry(sub_adc_, justify='right', width=12); ee_adc_acc_volt_s2_ch1 = Entry(sub_adc_, justify='right', width=12)
ee_adc_acc_volt_s3_ch2 = Entry(sub_adc_, justify='right', width=12); ee_adc_acc_volt_s3_ch1 = Entry(sub_adc_, justify='right', width=12)
ee_adc_acc_volt_s4_ch2 = Entry(sub_adc_, justify='right', width=12); ee_adc_acc_volt_s4_ch1 = Entry(sub_adc_, justify='right', width=12)
ee_adc_acc_volt_s5_ch2 = Entry(sub_adc_, justify='right', width=12); ee_adc_acc_volt_s5_ch1 = Entry(sub_adc_, justify='right', width=12)
ee_adc_acc_volt_s6_ch2 = Entry(sub_adc_, justify='right', width=12); ee_adc_acc_volt_s6_ch1 = Entry(sub_adc_, justify='right', width=12)
ee_adc_acc_volt_s7_ch2 = Entry(sub_adc_, justify='right', width=12); ee_adc_acc_volt_s7_ch1 = Entry(sub_adc_, justify='right', width=12)
ee_adc_acc_volt_s8_ch2 = Entry(sub_adc_, justify='right', width=12); ee_adc_acc_volt_s8_ch1 = Entry(sub_adc_, justify='right', width=12)
#
ee_adc_acc_volt_s1_ch2.insert(0,"0"); ee_adc_acc_volt_s1_ch1.insert(0,"0")
ee_adc_acc_volt_s2_ch2.insert(0,"0"); ee_adc_acc_volt_s2_ch1.insert(0,"0")
ee_adc_acc_volt_s3_ch2.insert(0,"0"); ee_adc_acc_volt_s3_ch1.insert(0,"0")
ee_adc_acc_volt_s4_ch2.insert(0,"0"); ee_adc_acc_volt_s4_ch1.insert(0,"0")
ee_adc_acc_volt_s5_ch2.insert(0,"0"); ee_adc_acc_volt_s5_ch1.insert(0,"0")
ee_adc_acc_volt_s6_ch2.insert(0,"0"); ee_adc_acc_volt_s6_ch1.insert(0,"0")
ee_adc_acc_volt_s7_ch2.insert(0,"0"); ee_adc_acc_volt_s7_ch1.insert(0,"0")
ee_adc_acc_volt_s8_ch2.insert(0,"0"); ee_adc_acc_volt_s8_ch1.insert(0,"0")
#

## gui variables for EXT-TRIG
#ee_ = Entry(sub_trig, justify='right', width=12) 
ee_EXT_TRIG_CON_WI_ = Entry(sub_trig, justify='right', width=12) 
ee_EXT_TRIG_PARA_WI = Entry(sub_trig, justify='right', width=12) 
ee_EXT_TRIG_AUX_WI_ = Entry(sub_trig, justify='right', width=12) 
#
ee_EXT_TRIG_CON_WI_.insert(0,"0x00000000")
ee_EXT_TRIG_PARA_WI.insert(0,"0x00000000")
ee_EXT_TRIG_AUX_WI_.insert(0,"0x00000000")
#
ee_EXT_TRIG_CONF_M_TRIG     = Entry(sub_trig, justify='right', width=6) 
ee_EXT_TRIG_CONF_M_PRE_TRIG = Entry(sub_trig, justify='right', width=6) 
ee_EXT_TRIG_CONF_AUX_TRIG   = Entry(sub_trig, justify='right', width=6) 
#
ee_EXT_TRIG_CONF_M_TRIG    .insert(0,"0x3")
ee_EXT_TRIG_CONF_M_PRE_TRIG.insert(0,"0x3")
ee_EXT_TRIG_CONF_AUX_TRIG  .insert(0,"0x3")
#
ee_EXT_TRIG_ADC__DELAY_CNT = Entry(sub_trig, justify='right', width=6) 
ee_EXT_TRIG_DAC__DELAY_CNT = Entry(sub_trig, justify='right', width=6) 
ee_EXT_TRIG_SPIO_DELAY_CNT = Entry(sub_trig, justify='right', width=6) 
#
ee_EXT_TRIG_ADC__DELAY_CNT.insert(0,"00000")
ee_EXT_TRIG_DAC__DELAY_CNT.insert(0,"00000")
ee_EXT_TRIG_SPIO_DELAY_CNT.insert(0,"00000")
#
var_EXT_TRIG_BLOCK_EN = IntVar(value=1) 
var_ALL_TRIGS_PIN_DIS = IntVar(value=0) 
var_M_TRIG_____PIN_EN = IntVar(value=0) 
var_M_PRE_TRIG_PIN_EN = IntVar(value=0) 
var_AUX_TRIG___PIN_EN = IntVar(value=0) 


### GUI functions ###

## master board status 

#def update_wo_to_ee (ee, wo_adrs, wo_mask=0xFFFFFFFF, bit_shift=0):
#	print('>> update_wo_to_ee')
#	#
#	# read endpoint
#	dev.UpdateWireOuts()
#	#
#	wo_data = dev.GetWireOutValue(wo_adrs, wo_mask) 	
#	#
#	wo_data_shift = wo_data >> bit_shift
#	#
#	print('{} = 0x{:02X}'.format('wo_adrs', wo_adrs))
#	print('{} = 0x{:08X}'.format('wo_data_shift', wo_data_shift))
#	#
#	# set ee 
#	ee_adc_acc_s8_wo.delete(0,END)
#	ee_adc_s1_wo.insert(0,adc_skt1_wo__hexstr)
#	#
#	pass
	
	
def master_bb_update_board_status_wo():
	print('>>> master_bb_update_board_status_wo')
	#
	## read endpoint
	dev.UpdateWireOuts()
	#
	wo20_data = dev.GetWireOutValue(0x20)
	wo21_data = dev.GetWireOutValue(0x21)
	wo22_data = dev.GetWireOutValue(0x22)
	wo23_data = dev.GetWireOutValue(0x23)
	#
	print('{} = 0x{:08X}'.format('wo20_data', wo20_data))
	print('{} = 0x{:08X}'.format('wo21_data', wo21_data))
	print('{} = 0x{:08X}'.format('wo22_data', wo22_data))
	print('{} = 0x{:08X}'.format('wo23_data', wo23_data))
	#
	ee_FPGA_IMAGE_ID_.delete(0,END)
	ee_TEST_FLAG_____.delete(0,END)
	ee_Slave_SPI_FLAG.delete(0,END)
	ee_MON_XADC______.delete(0,END)
	ee_slot_id_______.delete(0,END)
	ee_board_status__.delete(0,END)
	ee_temp_fpga_____.delete(0,END)
	#
	ee_FPGA_IMAGE_ID_.insert(0,'0x{:08X}'.format(wo20_data))
	ee_TEST_FLAG_____.insert(0,'0x{:08X}'.format(wo21_data))
	ee_Slave_SPI_FLAG.insert(0,'0x{:08X}'.format(wo22_data))
	ee_MON_XADC______.insert(0,'0x{:08X}'.format(wo23_data))
	#
	slot_id_______ = (wo22_data>>4)&0x0F
	board_status__ = (wo22_data>>8)&0xFF
	temp_fpga_____ = wo23_data/1000
	#
	ee_slot_id_______.insert(0,'0x{:01X}'.format(slot_id_______))
	ee_board_status__.insert(0,'0x{:02X}'.format(board_status__))
	ee_temp_fpga_____.insert(0,'{}'.format(temp_fpga_____))
	#

	
	## dac status read test 
	readback_DAC0_con,readback_DAC0_con_flags = dac_read_reg_control(sub_ch_en_b4=0x1)
	readback_DAC0_pcn,readback_DAC0_pcn_flags = dac_read_reg_pwr_ctl(sub_ch_en_b4=0x1)
	
	readback_DAC1_con,readback_DAC1_con_flags = dac_read_reg_control(sub_ch_en_b4=0x2)
	readback_DAC1_pcn,readback_DAC1_pcn_flags = dac_read_reg_pwr_ctl(sub_ch_en_b4=0x2)

	readback_DAC2_con,readback_DAC2_con_flags = dac_read_reg_control(sub_ch_en_b4=0x4)
	readback_DAC2_pcn,readback_DAC2_pcn_flags = dac_read_reg_pwr_ctl(sub_ch_en_b4=0x4)

	readback_DAC3_con,readback_DAC3_con_flags = dac_read_reg_control(sub_ch_en_b4=0x8)
	readback_DAC3_pcn,readback_DAC3_pcn_flags = dac_read_reg_pwr_ctl(sub_ch_en_b4=0x8)
	
	print('{} = 0x{:04X}, {} = 0x{:04X}'.format('readback_DAC0_con', readback_DAC0_con,'readback_DAC0_pcn', readback_DAC0_pcn))
	print('{} = 0x{:04X}, {} = 0x{:04X}'.format('readback_DAC1_con', readback_DAC1_con,'readback_DAC1_pcn', readback_DAC1_pcn))
	print('{} = 0x{:04X}, {} = 0x{:04X}'.format('readback_DAC2_con', readback_DAC2_con,'readback_DAC2_pcn', readback_DAC2_pcn))
	print('{} = 0x{:04X}, {} = 0x{:04X}'.format('readback_DAC3_con', readback_DAC3_con,'readback_DAC3_pcn', readback_DAC3_pcn))
	
	print('{} = {}, {} = {}'.format('readback_DAC0_con_flags', readback_DAC0_con_flags,'readback_DAC0_pcn_flags', readback_DAC0_pcn_flags))
	print('{} = {}, {} = {}'.format('readback_DAC1_con_flags', readback_DAC1_con_flags,'readback_DAC1_pcn_flags', readback_DAC1_pcn_flags))
	print('{} = {}, {} = {}'.format('readback_DAC2_con_flags', readback_DAC2_con_flags,'readback_DAC2_pcn_flags', readback_DAC2_pcn_flags))
	print('{} = {}, {} = {}'.format('readback_DAC3_con_flags', readback_DAC3_con_flags,'readback_DAC3_pcn_flags', readback_DAC3_pcn_flags))

	ee_DAC0_con______.delete(0,END)
	ee_DAC1_con______.delete(0,END)
	ee_DAC2_con______.delete(0,END)
	ee_DAC3_con______.delete(0,END)
	ee_DAC0_alert____.delete(0,END)
	ee_DAC1_alert____.delete(0,END)
	ee_DAC2_alert____.delete(0,END)
	ee_DAC3_alert____.delete(0,END)

	ee_DAC0_con______.insert(0,'0x{:01X}'.format(readback_DAC0_con))
	ee_DAC1_con______.insert(0,'0x{:01X}'.format(readback_DAC1_con))
	ee_DAC2_con______.insert(0,'0x{:01X}'.format(readback_DAC2_con))
	ee_DAC3_con______.insert(0,'0x{:01X}'.format(readback_DAC3_con))
	ee_DAC0_alert____.insert(0,'0x{:03X}'.format(readback_DAC0_pcn))
	ee_DAC1_alert____.insert(0,'0x{:03X}'.format(readback_DAC1_pcn))
	ee_DAC2_alert____.insert(0,'0x{:03X}'.format(readback_DAC2_pcn))
	ee_DAC3_alert____.insert(0,'0x{:03X}'.format(readback_DAC3_pcn))

	return

## TODO: MHVSU SPIO functions 

## example
##	## send SPIO send frame
##	spio_send_frame(
##		socket_en=socket_en_data, 
##		cs_en=0x01, # for CS0
##		rd__wr_bar=0, # for write 
##		reg_adrs=0x12, # IO data reg
##		data_out=out_data)
##	
##	## test read back 
##	spio_send_frame(
##		socket_en=socket_en_data, 
##		cs_en=0x01, # for CS0
##		rd__wr_bar=1, # for read 
##		reg_adrs=0x12, # IO data reg
##		data_out=0)

def spio_send_frame(socket_en, cs_en, rd__wr_bar, reg_adrs, data_out):
	data_readback = 0x0000
	#
	
	## send SPIO send frame
	
	# set socket/cs 
	# SPIO_CON_WI
	# ep05 = 0xFF070001 // w_SPIO_CON_WI // socket/cs enable 
	#cs_en_data = 0x02 # for CS1
	#wire_data = 0
	wire_data = (socket_en<<24) + (cs_en<<16) + 1
	dev.SetWireInValue(0x05,wire_data,0xFFFFFFFF) # (ep,val,mask)
	dev.UpdateWireIns()
	
	# make frame data
	#reg_adrs = 0x0012
	frame_data = 0
	frame_data = (rd__wr_bar<<24)+ (reg_adrs<<16) + data_out
	# SPIO_FDAT_WI
	# ep04 = 0x00120080 // {GPA,GPB}
	dev.SetWireInValue(0x04,frame_data,0xFFFFFFFF) # (ep,val,mask)
	dev.UpdateWireIns()

	## clear trig out
	dev.UpdateTriggerOuts()

	# ep45[1]
	dev.ActivateTriggerIn(0x45, 1)
	# ep65[1] // w_SPIO_TRIG_TO; // init done
	cnt_loop = 0;
	while 1:
		# First, query all XEM Trigger Outs.
		dev.UpdateTriggerOuts()
		# check trigger out
		if dev.IsTriggered(0x65, 0x02) == True: # // (ep, mask)
			break
		cnt_loop += 1;
		print('{} = {}'.format('cnt_loop', cnt_loop))
		if (cnt_loop>MAX_count):
			break
	print('{} = {}'.format('cnt_loop', cnt_loop))	
	
	## readback 
	dev.UpdateWireOuts()
	readback = dev.GetWireOutValue(0x25)
	data_readback = (readback & 0x0000FFFF)
	print('{} = 0x{:04X}'.format('data_readback', data_readback))
	
	#
	return data_readback

def spio_init():
	print('\n>>>>>> spio_init')
	
	## set IO as all outputs 
	spio_send_frame(
		socket_en=0xFF,  # for all sockets
		cs_en=0x07, # for all CS's
		rd__wr_bar=0, # for write 
		reg_adrs=0x00, # IO dir reg
		data_out=0x0000)
		
	## readback 
	spio_send_frame(
		socket_en=0xFF,  # for all sockets
		cs_en=0x07, # for all CS's
		rd__wr_bar=1, # for write 
		reg_adrs=0x00, # IO dir reg
		data_out=0x0000)
	
def spio_close():
	print('\n>>>>>> spio_close')
	# ep05 = 0xFF070001 // w_SPIO_CON_WI // socket/cs enable 
	dev.SetWireInValue(0x05,0x00000000,0xFFFFFFFF) # (ep,val,mask)
	dev.UpdateWireIns()

def spio_test_led_on():
	print('\n>>>>>> spio_test_led_on')
	# ep05 = 0xFF070001 // w_SPIO_CON_WI // socket/cs enable // all selected
	dev.SetWireInValue(0x05,0xFF070001,0xFFFFFFFF) # (ep,val,mask)
	dev.UpdateWireIns()
	# ep04 = 0x00120080 // {GPA,GPB}
	dev.SetWireInValue(0x04,0x00120080,0xFFFFFFFF) # (ep,val,mask)
	dev.UpdateWireIns()

	## clear trig out
	dev.UpdateTriggerOuts()

	# ep45[1]
	dev.ActivateTriggerIn(0x45, 1)
	# ep65[1] // w_SPIO_TRIG_TO; // init done
	cnt_loop = 0;
	while 1:
		# First, query all XEM Trigger Outs.
		dev.UpdateTriggerOuts()
		# check trigger out
		if dev.IsTriggered(0x65, 0x02) == True: # // (ep, mask)
			break
		cnt_loop += 1;
		print('{} = {}'.format('cnt_loop', cnt_loop))
		if (cnt_loop>MAX_count):
			break
	print('{} = {}'.format('cnt_loop', cnt_loop))

def spio_test_led_off():
	print('\n>>>>>> spio_test_led_off')
	# ep05 = 0xFF070001 // w_SPIO_CON_WI // socket/cs enable // all selected
	dev.SetWireInValue(0x05,0xFF070001,0xFFFFFFFF) # (ep,val,mask)
	dev.UpdateWireIns()
	# ep04 = 0x00120080 // {GPA,GPB}
	dev.SetWireInValue(0x04,0x00120000,0xFFFFFFFF) # (ep,val,mask)
	dev.UpdateWireIns()
	
	## clear trig out
	dev.UpdateTriggerOuts()
	
	# ep45[1]
	dev.ActivateTriggerIn(0x45, 1)
	# ep65[1] // w_SPIO_TRIG_TO; // init done
	cnt_loop = 0;
	while 1:
		# First, query all XEM Trigger Outs.
		dev.UpdateTriggerOuts()
		# check trigger out
		if dev.IsTriggered(0x65, 0x02) == True: # // (ep, mask)
			break
		cnt_loop += 1;
		print('{} = {}'.format('cnt_loop', cnt_loop))
		if (cnt_loop>MAX_count):
			break
	print('{} = {}'.format('cnt_loop', cnt_loop))

def spio_bb_update_cs0():
	print('\n>>>>>> spio_bb_update_cs0')
	## read check box
	print('\n>>>>>>')
	# read socket enable data
	print('{} = {}'.format('var_SKT1_EN', var_SKT1_EN.get()))
	print('{} = {}'.format('var_SKT2_EN', var_SKT2_EN.get()))
	print('{} = {}'.format('var_SKT3_EN', var_SKT3_EN.get()))
	print('{} = {}'.format('var_SKT4_EN', var_SKT4_EN.get()))
	print('{} = {}'.format('var_SKT5_EN', var_SKT5_EN.get()))
	print('{} = {}'.format('var_SKT6_EN', var_SKT6_EN.get()))
	print('{} = {}'.format('var_SKT7_EN', var_SKT7_EN.get()))
	print('{} = {}'.format('var_SKT8_EN', var_SKT8_EN.get()))
	# read io data
	print('{} = {}'.format('var_CS0_GPB7', var_CS0_GPB7.get()))
	print('{} = {}'.format('var_CS0_GPB6', var_CS0_GPB6.get()))
	print('{} = {}'.format('var_CS0_GPB5', var_CS0_GPB5.get()))
	print('{} = {}'.format('var_CS0_GPB4', var_CS0_GPB4.get()))
	print('{} = {}'.format('var_CS0_GPB3', var_CS0_GPB3.get()))
	print('{} = {}'.format('var_CS0_GPB2', var_CS0_GPB2.get()))
	print('{} = {}'.format('var_CS0_GPB1', var_CS0_GPB1.get()))
	print('{} = {}'.format('var_CS0_GPB0', var_CS0_GPB0.get()))
	print('{} = {}'.format('var_CS0_GPA7', var_CS0_GPA7.get()))
	print('{} = {}'.format('var_CS0_GPA6', var_CS0_GPA6.get()))
	print('{} = {}'.format('var_CS0_GPA5', var_CS0_GPA5.get()))
	print('{} = {}'.format('var_CS0_GPA4', var_CS0_GPA4.get()))
	print('{} = {}'.format('var_CS0_GPA3', var_CS0_GPA3.get()))
	print('{} = {}'.format('var_CS0_GPA2', var_CS0_GPA2.get()))
	print('{} = {}'.format('var_CS0_GPA1', var_CS0_GPA1.get()))
	print('{} = {}'.format('var_CS0_GPA0', var_CS0_GPA0.get()))
	#
	socket_en_data = 0x00
	if var_SKT1_EN.get()==1:	socket_en_data += 0x01
	if var_SKT2_EN.get()==1:	socket_en_data += 0x02
	if var_SKT3_EN.get()==1:	socket_en_data += 0x04
	if var_SKT4_EN.get()==1:	socket_en_data += 0x08
	if var_SKT5_EN.get()==1:	socket_en_data += 0x10
	if var_SKT6_EN.get()==1:	socket_en_data += 0x20
	if var_SKT7_EN.get()==1:	socket_en_data += 0x40
	if var_SKT8_EN.get()==1:	socket_en_data += 0x80
	#
	out_data = 0x0000
	if var_CS0_GPB7.get()==1:	out_data += 0x0080
	if var_CS0_GPB6.get()==1:	out_data += 0x0040
	if var_CS0_GPB5.get()==1:	out_data += 0x0020
	if var_CS0_GPB4.get()==1:	out_data += 0x0010
	if var_CS0_GPB3.get()==1:	out_data += 0x0008
	if var_CS0_GPB2.get()==1:	out_data += 0x0004
	if var_CS0_GPB1.get()==1:	out_data += 0x0002
	if var_CS0_GPB0.get()==1:	out_data += 0x0001
	if var_CS0_GPA7.get()==1:	out_data += 0x8000
	if var_CS0_GPA6.get()==1:	out_data += 0x4000
	if var_CS0_GPA5.get()==1:	out_data += 0x2000
	if var_CS0_GPA4.get()==1:	out_data += 0x1000
	if var_CS0_GPA3.get()==1:	out_data += 0x0800
	if var_CS0_GPA2.get()==1:	out_data += 0x0400
	if var_CS0_GPA1.get()==1:	out_data += 0x0200
	if var_CS0_GPA0.get()==1:	out_data += 0x0100
	
	## send SPIO send frame
	spio_send_frame(
		socket_en=socket_en_data, 
		cs_en=0x01, # for CS0
		rd__wr_bar=0, # for write 
		reg_adrs=0x12, # IO data reg
		data_out=out_data)
	
	## test read back 
	spio_send_frame(
		socket_en=socket_en_data, 
		cs_en=0x01, # for CS0
		rd__wr_bar=1, # for read 
		reg_adrs=0x12, # IO data reg
		data_out=0)
		
	return

def spio_bb_update_cs1():
	print('\n>>>>>> spio_bb_update_cs1')
	## read check box
	print('\n>>>>>>')
	# read socket enable data
	print('{} = {}'.format('var_SKT1_EN', var_SKT1_EN.get()))
	print('{} = {}'.format('var_SKT2_EN', var_SKT2_EN.get()))
	print('{} = {}'.format('var_SKT3_EN', var_SKT3_EN.get()))
	print('{} = {}'.format('var_SKT4_EN', var_SKT4_EN.get()))
	print('{} = {}'.format('var_SKT5_EN', var_SKT5_EN.get()))
	print('{} = {}'.format('var_SKT6_EN', var_SKT6_EN.get()))
	print('{} = {}'.format('var_SKT7_EN', var_SKT7_EN.get()))
	print('{} = {}'.format('var_SKT8_EN', var_SKT8_EN.get()))
	# read io data
	print('{} = {}'.format('var_CS1_GPB7', var_CS1_GPB7.get()))
	print('{} = {}'.format('var_CS1_GPB6', var_CS1_GPB6.get()))
	print('{} = {}'.format('var_CS1_GPB5', var_CS1_GPB5.get()))
	print('{} = {}'.format('var_CS1_GPB4', var_CS1_GPB4.get()))
	print('{} = {}'.format('var_CS1_GPB3', var_CS1_GPB3.get()))
	print('{} = {}'.format('var_CS1_GPB2', var_CS1_GPB2.get()))
	print('{} = {}'.format('var_CS1_GPB1', var_CS1_GPB1.get()))
	print('{} = {}'.format('var_CS1_GPB0', var_CS1_GPB0.get()))
	print('{} = {}'.format('var_CS1_GPA7', var_CS1_GPA7.get()))
	print('{} = {}'.format('var_CS1_GPA6', var_CS1_GPA6.get()))
	print('{} = {}'.format('var_CS1_GPA5', var_CS1_GPA5.get()))
	print('{} = {}'.format('var_CS1_GPA4', var_CS1_GPA4.get()))
	print('{} = {}'.format('var_CS1_GPA3', var_CS1_GPA3.get()))
	print('{} = {}'.format('var_CS1_GPA2', var_CS1_GPA2.get()))
	print('{} = {}'.format('var_CS1_GPA1', var_CS1_GPA1.get()))
	print('{} = {}'.format('var_CS1_GPA0', var_CS1_GPA0.get()))
	#
	socket_en_data = 0x00
	if var_SKT1_EN.get()==1:	socket_en_data += 0x01
	if var_SKT2_EN.get()==1:	socket_en_data += 0x02
	if var_SKT3_EN.get()==1:	socket_en_data += 0x04
	if var_SKT4_EN.get()==1:	socket_en_data += 0x08
	if var_SKT5_EN.get()==1:	socket_en_data += 0x10
	if var_SKT6_EN.get()==1:	socket_en_data += 0x20
	if var_SKT7_EN.get()==1:	socket_en_data += 0x40
	if var_SKT8_EN.get()==1:	socket_en_data += 0x80
	#
	out_data = 0x0000
	if var_CS1_GPB7.get()==1:	out_data += 0x0080
	if var_CS1_GPB6.get()==1:	out_data += 0x0040
	if var_CS1_GPB5.get()==1:	out_data += 0x0020
	if var_CS1_GPB4.get()==1:	out_data += 0x0010
	if var_CS1_GPB3.get()==1:	out_data += 0x0008
	if var_CS1_GPB2.get()==1:	out_data += 0x0004
	if var_CS1_GPB1.get()==1:	out_data += 0x0002
	if var_CS1_GPB0.get()==1:	out_data += 0x0001
	if var_CS1_GPA7.get()==1:	out_data += 0x8000
	if var_CS1_GPA6.get()==1:	out_data += 0x4000
	if var_CS1_GPA5.get()==1:	out_data += 0x2000
	if var_CS1_GPA4.get()==1:	out_data += 0x1000
	if var_CS1_GPA3.get()==1:	out_data += 0x0800
	if var_CS1_GPA2.get()==1:	out_data += 0x0400
	if var_CS1_GPA1.get()==1:	out_data += 0x0200
	if var_CS1_GPA0.get()==1:	out_data += 0x0100
	
	## send SPIO send frame
	spio_send_frame(
		socket_en=socket_en_data, 
		cs_en=0x02, # for CS0
		rd__wr_bar=0, # for write 
		reg_adrs=0x12, # IO data reg
		data_out=out_data)
	
	## test read back 
	spio_send_frame(
		socket_en=socket_en_data, 
		cs_en=0x02, # for CS0
		rd__wr_bar=1, # for read 
		reg_adrs=0x12, # IO data reg
		data_out=0)

	return

def spio_bb_update_cs2():
	print('\n>>>>>> spio_bb_update_cs2')
	## read check box
	print('\n>>>>>>')
	# read socket enable data
	print('{} = {}'.format('var_SKT1_EN', var_SKT1_EN.get()))
	print('{} = {}'.format('var_SKT2_EN', var_SKT2_EN.get()))
	print('{} = {}'.format('var_SKT3_EN', var_SKT3_EN.get()))
	print('{} = {}'.format('var_SKT4_EN', var_SKT4_EN.get()))
	print('{} = {}'.format('var_SKT5_EN', var_SKT5_EN.get()))
	print('{} = {}'.format('var_SKT6_EN', var_SKT6_EN.get()))
	print('{} = {}'.format('var_SKT7_EN', var_SKT7_EN.get()))
	print('{} = {}'.format('var_SKT8_EN', var_SKT8_EN.get()))
	# read io data
	print('{} = {}'.format('var_CS1_GPB7', var_CS2_GPB7.get()))
	print('{} = {}'.format('var_CS1_GPB6', var_CS2_GPB6.get()))
	print('{} = {}'.format('var_CS1_GPB5', var_CS2_GPB5.get()))
	print('{} = {}'.format('var_CS1_GPB4', var_CS2_GPB4.get()))
	print('{} = {}'.format('var_CS1_GPB3', var_CS2_GPB3.get()))
	print('{} = {}'.format('var_CS1_GPB2', var_CS2_GPB2.get()))
	print('{} = {}'.format('var_CS1_GPB1', var_CS2_GPB1.get()))
	print('{} = {}'.format('var_CS1_GPB0', var_CS2_GPB0.get()))
	print('{} = {}'.format('var_CS1_GPA7', var_CS2_GPA7.get()))
	print('{} = {}'.format('var_CS1_GPA6', var_CS2_GPA6.get()))
	print('{} = {}'.format('var_CS1_GPA5', var_CS2_GPA5.get()))
	print('{} = {}'.format('var_CS1_GPA4', var_CS2_GPA4.get()))
	print('{} = {}'.format('var_CS1_GPA3', var_CS2_GPA3.get()))
	print('{} = {}'.format('var_CS1_GPA2', var_CS2_GPA2.get()))
	print('{} = {}'.format('var_CS1_GPA1', var_CS2_GPA1.get()))
	print('{} = {}'.format('var_CS1_GPA0', var_CS2_GPA0.get()))
	#
	socket_en_data = 0x00
	if var_SKT1_EN.get()==1:	socket_en_data += 0x01
	if var_SKT2_EN.get()==1:	socket_en_data += 0x02
	if var_SKT3_EN.get()==1:	socket_en_data += 0x04
	if var_SKT4_EN.get()==1:	socket_en_data += 0x08
	if var_SKT5_EN.get()==1:	socket_en_data += 0x10
	if var_SKT6_EN.get()==1:	socket_en_data += 0x20
	if var_SKT7_EN.get()==1:	socket_en_data += 0x40
	if var_SKT8_EN.get()==1:	socket_en_data += 0x80
	#
	out_data = 0x0000
	if var_CS2_GPB7.get()==1:	out_data += 0x0080
	if var_CS2_GPB6.get()==1:	out_data += 0x0040
	if var_CS2_GPB5.get()==1:	out_data += 0x0020
	if var_CS2_GPB4.get()==1:	out_data += 0x0010
	if var_CS2_GPB3.get()==1:	out_data += 0x0008
	if var_CS2_GPB2.get()==1:	out_data += 0x0004
	if var_CS2_GPB1.get()==1:	out_data += 0x0002
	if var_CS2_GPB0.get()==1:	out_data += 0x0001
	if var_CS2_GPA7.get()==1:	out_data += 0x8000
	if var_CS2_GPA6.get()==1:	out_data += 0x4000
	if var_CS2_GPA5.get()==1:	out_data += 0x2000
	if var_CS2_GPA4.get()==1:	out_data += 0x1000
	if var_CS2_GPA3.get()==1:	out_data += 0x0800
	if var_CS2_GPA2.get()==1:	out_data += 0x0400
	if var_CS2_GPA1.get()==1:	out_data += 0x0200
	if var_CS2_GPA0.get()==1:	out_data += 0x0100
	
	## send SPIO send frame
	spio_send_frame(
		socket_en=socket_en_data, 
		cs_en=0x04, # for CS0
		rd__wr_bar=0, # for write 
		reg_adrs=0x12, # IO data reg
		data_out=out_data)
	
	## test read back 
	spio_send_frame(
		socket_en=socket_en_data, 
		cs_en=0x04, # for CS0
		rd__wr_bar=1, # for read 
		reg_adrs=0x12, # IO data reg
		data_out=0)

	return

def spio_bb_update_forced_mode():
	print('\n>>>>>> spio_bb_update_forced_mode')

	# var_forced_mode_en  
	# var_forced_sig_mosi 
	# var_forced_sig_sclk 
	# var_forced_sig_csel 
	# var_forced_cs0      
	# var_forced_cs1      
	# var_forced_cs2      
	
	#
	socket_en_data = 0x00
	if var_SKT1_EN.get()==1:	socket_en_data += 0x01
	if var_SKT2_EN.get()==1:	socket_en_data += 0x02
	if var_SKT3_EN.get()==1:	socket_en_data += 0x04
	if var_SKT4_EN.get()==1:	socket_en_data += 0x08
	if var_SKT5_EN.get()==1:	socket_en_data += 0x10
	if var_SKT6_EN.get()==1:	socket_en_data += 0x20
	if var_SKT7_EN.get()==1:	socket_en_data += 0x40
	if var_SKT8_EN.get()==1:	socket_en_data += 0x80

	cs_en_data = 0x0
	if var_forced_cs0.get()==1:	cs_en_data += 0x01
	if var_forced_cs1.get()==1:	cs_en_data += 0x02
	if var_forced_cs2.get()==1:	cs_en_data += 0x04
	
	forced_mode_en  = 0
	if var_forced_mode_en.get()==1:	forced_mode_en += 0x01
	
	forced_sig_mosi = 0
	if var_forced_sig_mosi.get()==1:	forced_sig_mosi += 0x01
	
	forced_sig_sclk = 0
	if var_forced_sig_sclk.get()==1:	forced_sig_sclk += 0x01
	
	forced_sig_csel = 0
	if var_forced_sig_csel.get()==1:	forced_sig_csel += 0x01
	
	# SPIO_CON_WI
	# ep05 
	wire_data = (socket_en_data<<24)  + \
			    (cs_en_data<<16)      + \
			    (forced_sig_csel<<11) + \
			    (forced_sig_sclk<<10) + \
			    (forced_sig_mosi<< 9) + \
			    (forced_mode_en<< 8)  + \
			    1
	dev.SetWireInValue(0x05,wire_data,0xFFFFFFFF) # (ep,val,mask)
	dev.UpdateWireIns()

	#
	print('{} = 0x{:X}'.format('socket_en_data ', socket_en_data ))
	print('{} = 0x{:X}'.format('cs_en_data     ', cs_en_data     ))
	print('{} = 0x{:X}'.format('forced_sig_csel', forced_sig_csel))
	print('{} = 0x{:X}'.format('forced_sig_sclk', forced_sig_sclk))
	print('{} = 0x{:X}'.format('forced_sig_mosi', forced_sig_mosi))
	print('{} = 0x{:X}'.format('forced_mode_en ', forced_mode_en ))

	pass


## TODO: MHVSU DAC functions 

def dac_en():
	print('\n>>>>>> dac_en')
	# ep06 = 0x00000001 // w_DAC_CON_WI // dac enable 
	dev.SetWireInValue(0x06,0x00000001,0x00000001) # (ep,val,mask)
	dev.UpdateWireIns()
	
def dac_dis():
	print('\n>>>>>> dac_dis')
	# ep06 = 0x00000001 // w_DAC_CON_WI // dac enable 
	dev.SetWireInValue(0x06,0x00000000,0x00000001) # (ep,val,mask)
	dev.UpdateWireIns()

def dac_set_sub_ch_en(sub_ch_en_b4=0xF):
	print('\n>>>>>> dac_set_sub_ch_en')
	# ep06 = 0x000000F1 // w_DAC_CON_WI // dac enable // all dac3/2/1/0 enabled
	wi   = sub_ch_en_b4<<4
	mask = 0xF<<4
	dev.SetWireInValue(0x06,wi,mask) # (ep,val,mask)
	dev.UpdateWireIns()

def dac_set_frame_data(frame_data_in_b24):
	print('\n>>>>>> dac_set_frame_data')
	# ep06 = 0xFFFF_FF00 // w_DAC_CON_WI // frame data
	wi   = frame_data_in_b24<<8
	mask = 0xFFFFFF00
	dev.SetWireInValue(0x06,wi,mask) # (ep,val,mask)
	dev.UpdateWireIns()


def dac_read_flag_wo():
	print('\n>>>>>> dac_read_flag_wo')
	# w_DAC_FLAG_WO[31:8] = w_test_sdo_pdata or muxed; // 24 bits
	# w_DAC_FLAG_WO[7]    = w_busy_DAC_update;
	# w_DAC_FLAG_WO[6:4]  = 3'b0;
	# w_DAC_FLAG_WO[3]    = w_done_DAC_update;
	# w_DAC_FLAG_WO[2]    = w_done_DAC_init;
	# w_DAC_FLAG_WO[1]    = w_done_DAC_SPI_frame;
	# w_DAC_FLAG_WO[0]    = w_DAC_en;
	
	DAC_FLAG_WO = 0
	
	dev.UpdateWireOuts()
	DAC_FLAG_WO = dev.GetWireOutValue(0x26) # // DAC_FLAG_WO 
	print('{} = 0x{:08X}'.format('DAC_FLAG_WO', DAC_FLAG_WO))
	
	return DAC_FLAG_WO

def dac_send_trig_with_check(loc_bit):
	print('\n>>>>>> dac_send_trig_with_check')

	# clear trig out
	dev.UpdateTriggerOuts()
	
	# ep46[loc_bit] // w_DAC_TRIG_TI 
	dev.ActivateTriggerIn(0x46, loc_bit)
	#
	# ep66[loc_bit] // w_DAC_TRIG_TO // done check
	cnt_loop = 0;
	while 1:
		# First, query all XEM Trigger Outs.
		dev.UpdateTriggerOuts()
		# check trigger out
		if dev.IsTriggered(0x66, 0x1<<loc_bit) == True: # // (ep, mask)
			break
		cnt_loop += 1;
		print('{} = {}'.format('cnt_loop', cnt_loop))
		if (cnt_loop>MAX_count):
			break
	print('{} = {}'.format('cnt_loop', cnt_loop))
	
	return
	

def dac_send_frame(sub_ch_en_b4=0xF, frame_data_in_b24=0):
	print('\n>>>>>> dac_send_frame')
	
	dac_set_sub_ch_en(sub_ch_en_b4)
	dac_set_frame_data(frame_data_in_b24)
	
	## trig frame 
	# ep46[1] // w_DAC_TRIG_TI // frame
	# ep66[1] // w_DAC_TRIG_TO // frame done check
	dac_send_trig_with_check(1) # bit_loc
	return

## AD5754BREZ input reg format 
#
# DB[23]    = R/W_bar 
# DB[22]    = 0
# DB[21:19] =  REG[2:0]
# DB[18:16] =    A[2:0]
# DB[15:0]  = data[15:0] 
#
# REG[2:0] = 000 // DAC reg
# REG[2:0] = 001 // output range sel reg
# REG[2:0] = 010 // power control reg
# REG[2:0] = 011 // control reg
#
# A[2:0] = 000 // DAC A
# A[2:0] = 001 // DAC B
# A[2:0] = 010 // DAC C
# A[2:0] = 011 // DAC D
# A[2:0] = 100 // DAC all

def gen_frame_data(rw_bar=1,reg_b3=0x0,adrs_b3=0x0,data_b16=0x0000):
	ret_b24 = 0
	ret_b24 = ret_b24 + (rw_bar  <<23)
	ret_b24 = ret_b24 + (reg_b3  <<19)
	ret_b24 = ret_b24 + (adrs_b3 <<16)
	ret_b24 = ret_b24 + (data_b16<< 0)
	ret_b24 = ret_b24 & 0x00FFFFFF
	return ret_b24


def dac_send_frame_data(sub_ch_en_b4=0xF, rw_bar=1,reg_b3=0x0,adrs_b3=0x0,data_b16=0x0000):
	print('\n>>>>>> dac_send_frame_data')
	frame_data_in_b24 = gen_frame_data(rw_bar=rw_bar,reg_b3=reg_b3,adrs_b3=adrs_b3,data_b16=data_b16) 
	
	dac_send_frame(sub_ch_en_b4, frame_data_in_b24)
	ret = (dac_read_flag_wo())>>8

	return ret


## send DAC X data 	
def	dac_send_reg_data (sub_ch_en_b4=0x1, adrs_b3=0x0, data_b16=0x0000):
	print('\n>>> dac_send_reg_data')
	# adrs_b3 
	# A[2:0] = 000 // DAC A
	# A[2:0] = 001 // DAC B
	# A[2:0] = 010 // DAC C
	# A[2:0] = 011 // DAC D
	
	# write reg_data
	dac_send_frame_data(sub_ch_en_b4=sub_ch_en_b4, 
		rw_bar=0,reg_b3=0x0,
		adrs_b3=adrs_b3,data_b16=data_b16)
	
	return

## read DAC X data 	
def dac_read_reg_data(sub_ch_en_b4=0x1, adrs_b3=0x0):
	print('\n>>> dac_read_reg_data')
	# adrs_b3 
	# A[2:0] = 000 // DAC A
	# A[2:0] = 001 // DAC B
	# A[2:0] = 010 // DAC C
	# A[2:0] = 011 // DAC D

	## read reg_data
	ret = dac_send_frame_data(sub_ch_en_b4=sub_ch_en_b4, 
		rw_bar=1,reg_b3=0x0,
		adrs_b3=adrs_b3)
	
	return ret


## AD5754BREZ control reg format 
#
# DB[23]    = R/W_bar 
# DB[22]    = 0
# DB[21:19] =  REG[2:0]
# DB[18:16] =    A[2:0]
# DB[15:0]  = data[15:0] 
#
# REG[2:0] = 011 // control reg
#
# A[2:0] = 000 // NOP command
# A[2:0] = 001 // control options on data[3:0] //
#                 data[3] = TSD enable
#                 data[2] = Current clamp enable
#                 data[1] = CLR value select 
#                 data[0] = SDO disable
# A[2:0] = 100 // CLEAR command
# A[2:0] = 101 // LOAD  command

## send control reg
def dac_send_reg_control(sub_ch_en_b4=0x1, data_b16=0x0000):
	print('\n>>> dac_read_reg_control')

	## read control reg options
	dac_send_frame_data(sub_ch_en_b4=sub_ch_en_b4, 
		rw_bar=0,reg_b3=0x3,adrs_b3=0x1,data_b16=data_b16)
		
	return

## read control reg
def dac_read_reg_control(sub_ch_en_b4=0x1):
	print('\n>>> dac_read_reg_control')

	## read control reg options
	ret = dac_send_frame_data(sub_ch_en_b4=sub_ch_en_b4, 
		rw_bar=1,reg_b3=0x3,adrs_b3=0x1)
	
	TSD_EN  = 1 if ret&0x08 else 0
	CC_EN   = 1 if ret&0x04 else 0
	CLR_SEL = 1 if ret&0x02 else 0
	SDO_DIS = 1 if ret&0x01 else 0
	
	flags_decode = [TSD_EN, CC_EN, CLR_SEL, SDO_DIS]
	
	return ret,flags_decode

## AD5754BREZ POWER control reg format 
#
# DB[23]    = R/W_bar 
# DB[22]    = 0
# DB[21:19] =  REG[2:0]
# DB[18:16] =    A[2:0]
# DB[15:0]  = data[15:0] 
#
# REG[2:0] = 010 // power control reg
#
# A[2:0] = 000 // fixed // flags on data[10:0]
#                 data[10] = Over Current D
#                 data[9]  = Over Current C 
#                 data[8]  = Over Current B
#                 data[7]  = Over Current A
#                 data[6]  = 0
#                 data[5]  = TSD
#                 data[4]  = 0
#                 data[3]  = Power-Up D
#                 data[2]  = Power-Up C
#                 data[1]  = Power-Up B
#                 data[0]  = Power-Up A
#

## read power control reg
def dac_read_reg_pwr_ctl(sub_ch_en_b4=0x1):
	print('\n>>> dac_read_reg_pwr_ctl')
	
	## read power control reg options
	ret = dac_send_frame_data(sub_ch_en_b4=sub_ch_en_b4, 
		rw_bar=1,reg_b3=0x2,adrs_b3=0x0)
		
	OC_D = 1 if ret&(0x01<<10) else 0
	OC_C = 1 if ret&(0x01<<9 ) else 0
	OC_B = 1 if ret&(0x01<<8 ) else 0
	OC_A = 1 if ret&(0x01<<7 ) else 0
	TSD  = 1 if ret&(0x01<<5 ) else 0
	PU_D = 1 if ret&(0x01<<3 ) else 0
	PU_C = 1 if ret&(0x01<<2 ) else 0
	PU_B = 1 if ret&(0x01<<1 ) else 0
	PU_A = 1 if ret&(0x01<<0 ) else 0
		
	flags_decode = [
		OC_D,
		OC_C,
		OC_B,
		OC_A,
		TSD ,
		PU_D,
		PU_C,
		PU_B,
		PU_A]
	
	return ret,flags_decode

def dac_init():
	print('\n>>> dac_init')
	# ep06 = 0x000000F1 // w_DAC_CON_WI // dac enable // all dac0/1/2/3 enabled
	#dev.SetWireInValue(0x06,0x000000F1,0xFFFFFFFF) # (ep,val,mask)
	#dev.UpdateWireIns()
	
	dac_en()
	dac_set_sub_ch_en(0xF)
	
	# ep46[2] // w_DAC_TRIG_TI // initialize
	# ep66[2] // w_DAC_TRIG_TO // init done check
	dac_send_trig_with_check(2) # bit_loc
	
	return 

def dac_close():
	print('\n>>> dac_close')
	# ep06 = 0x000000F1 // w_DAC_CON_WI // dac enable // all dac0/1/2/3 enabled
	dev.SetWireInValue(0x06,0x00000000,0xFFFFFFFF) # (ep,val,mask)
	dev.UpdateWireIns()

def dac_test():
	print('\n>>> dac_test')
	## test write on S3
	# ep1A = 0x000A0002 // w_DAC_S3_WI // dac data
	set_data = 0x000A0002
	dev.SetWireInValue(0x1A,set_data,0xFFFFFFFF) # (ep,val,mask)
	dev.UpdateWireIns()	
	
	# select all DAC
	dac_set_sub_ch_en(0xF)
	
	# ep46[3] // w_DAC_TRIG_TI // update
	# ep66[3] // w_DAC_TRIG_TO // update done check
	dac_send_trig_with_check(3)
	
	## test readback from S3
	# ep3A // w_DAC_S3_WO // dac readback
	dev.UpdateWireOuts()
	readback = dev.GetWireOutValue(0x3A)
	#
	print('{} = 0x{:08X}'.format('set_data', set_data))
	print('{} = 0x{:08X}'.format('readback', readback))
	# must ... set_data = readback
	if set_data==readback:
		print('### OK ###')
	else:
		#raise
		print('### NG ###')
		input('')	
	
	## return to '0'
	# ep1A = 0x00000000 // w_DAC_S3_WI // dac data
	set_data = 0x00000000
	dev.SetWireInValue(0x18,set_data,0xFFFFFFFF) # (ep,val,mask)
	dev.SetWireInValue(0x19,set_data,0xFFFFFFFF) # (ep,val,mask)
	dev.SetWireInValue(0x1A,set_data,0xFFFFFFFF) # (ep,val,mask)
	dev.SetWireInValue(0x1B,set_data,0xFFFFFFFF) # (ep,val,mask)
	dev.SetWireInValue(0x1C,set_data,0xFFFFFFFF) # (ep,val,mask)
	dev.SetWireInValue(0x1D,set_data,0xFFFFFFFF) # (ep,val,mask)
	dev.SetWireInValue(0x1E,set_data,0xFFFFFFFF) # (ep,val,mask)
	dev.SetWireInValue(0x1F,set_data,0xFFFFFFFF) # (ep,val,mask)
	dev.UpdateWireIns()	
	
	
	# ep46[3] // w_DAC_TRIG_TI // update
	# ep66[3] // w_DAC_TRIG_TO // update done check
	dac_send_trig_with_check(3)
	
	
	#### test frame 

	## enable DAC 
	#dac_dis()
	#dac_en()
	
	#dac_set_sub_ch_en(0xF) # enable all DAC3/2/1/0
	#dac_set_sub_ch_en(0x1) # DAC0
	#dac_set_sub_ch_en(0x2) # DAC1
	#dac_set_sub_ch_en(0x4) # DAC2
	#dac_set_sub_ch_en(0x8) # DAC3
	#dac_init() 
	
	
	## test DAC frame 
	
	# dac data reg test 
	set_data = 0x0012
	dac_send_frame_data(sub_ch_en_b4=0x4, rw_bar=0,reg_b3=0x0,adrs_b3=0x0,data_b16=set_data)
	readback=dac_send_frame_data(sub_ch_en_b4=0x4, rw_bar=1,reg_b3=0x0,adrs_b3=0x0)
	#
	print('{} = 0x{:08X}'.format('set_data', set_data))
	print('{} = 0x{:08X}'.format('readback', readback))
	# must ... set_data = readback
	if set_data==readback:
		print('### OK ###')
	else:
		#raise
		print('### NG ###')
		input('')	
	
	# dac data reg test rev 
	set_data_DAC0_A = 0x0012
	set_data_DAC0_B = 0x0031
	set_data_DAC0_C = 0x0021
	set_data_DAC0_D = 0x0013
	set_data_DAC1_A = 0x0023
	set_data_DAC1_B = 0x0032
	set_data_DAC1_C = 0x0012
	set_data_DAC1_D = 0x0031
	set_data_DAC2_A = 0x0021
	set_data_DAC2_B = 0x0013
	set_data_DAC2_C = 0x0023
	set_data_DAC2_D = 0x0032
	set_data_DAC3_A = 0x0012
	set_data_DAC3_B = 0x0031
	set_data_DAC3_C = 0x0021
	set_data_DAC3_D = 0x0013
	#                       
	dac_send_reg_data (sub_ch_en_b4=0x1, adrs_b3=0x0, data_b16=set_data_DAC0_A) # DAC0_A
	dac_send_reg_data (sub_ch_en_b4=0x1, adrs_b3=0x1, data_b16=set_data_DAC0_B) # DAC0_B
	dac_send_reg_data (sub_ch_en_b4=0x1, adrs_b3=0x2, data_b16=set_data_DAC0_C) # DAC0_C
	dac_send_reg_data (sub_ch_en_b4=0x1, adrs_b3=0x3, data_b16=set_data_DAC0_D) # DAC0_D
	dac_send_reg_data (sub_ch_en_b4=0x2, adrs_b3=0x0, data_b16=set_data_DAC1_A) # DAC1_A
	dac_send_reg_data (sub_ch_en_b4=0x2, adrs_b3=0x1, data_b16=set_data_DAC1_B) # DAC1_B
	dac_send_reg_data (sub_ch_en_b4=0x2, adrs_b3=0x2, data_b16=set_data_DAC1_C) # DAC1_C
	dac_send_reg_data (sub_ch_en_b4=0x2, adrs_b3=0x3, data_b16=set_data_DAC1_D) # DAC1_D
	dac_send_reg_data (sub_ch_en_b4=0x4, adrs_b3=0x0, data_b16=set_data_DAC2_A) # DAC2_A
	dac_send_reg_data (sub_ch_en_b4=0x4, adrs_b3=0x1, data_b16=set_data_DAC2_B) # DAC2_B
	dac_send_reg_data (sub_ch_en_b4=0x4, adrs_b3=0x2, data_b16=set_data_DAC2_C) # DAC2_C
	dac_send_reg_data (sub_ch_en_b4=0x4, adrs_b3=0x3, data_b16=set_data_DAC2_D) # DAC2_D
	dac_send_reg_data (sub_ch_en_b4=0x8, adrs_b3=0x0, data_b16=set_data_DAC3_A) # DAC3_A
	dac_send_reg_data (sub_ch_en_b4=0x8, adrs_b3=0x1, data_b16=set_data_DAC3_B) # DAC3_B
	dac_send_reg_data (sub_ch_en_b4=0x8, adrs_b3=0x2, data_b16=set_data_DAC3_C) # DAC3_C
	dac_send_reg_data (sub_ch_en_b4=0x8, adrs_b3=0x3, data_b16=set_data_DAC3_D) # DAC3_D
	#
	readback_DAC0_A = dac_read_reg_data (sub_ch_en_b4=0x1, adrs_b3=0x0)                   
	readback_DAC0_B = dac_read_reg_data (sub_ch_en_b4=0x1, adrs_b3=0x1)                   
	readback_DAC0_C = dac_read_reg_data (sub_ch_en_b4=0x1, adrs_b3=0x2)                   
	readback_DAC0_D = dac_read_reg_data (sub_ch_en_b4=0x1, adrs_b3=0x3)                   
	readback_DAC1_A = dac_read_reg_data (sub_ch_en_b4=0x2, adrs_b3=0x0)                   
	readback_DAC1_B = dac_read_reg_data (sub_ch_en_b4=0x2, adrs_b3=0x1)                   
	readback_DAC1_C = dac_read_reg_data (sub_ch_en_b4=0x2, adrs_b3=0x2)                   
	readback_DAC1_D = dac_read_reg_data (sub_ch_en_b4=0x2, adrs_b3=0x3)                   
	readback_DAC2_A = dac_read_reg_data (sub_ch_en_b4=0x4, adrs_b3=0x0)                   
	readback_DAC2_B = dac_read_reg_data (sub_ch_en_b4=0x4, adrs_b3=0x1)                   
	readback_DAC2_C = dac_read_reg_data (sub_ch_en_b4=0x4, adrs_b3=0x2)                   
	readback_DAC2_D = dac_read_reg_data (sub_ch_en_b4=0x4, adrs_b3=0x3)                   
	readback_DAC3_A = dac_read_reg_data (sub_ch_en_b4=0x8, adrs_b3=0x0)                   
	readback_DAC3_B = dac_read_reg_data (sub_ch_en_b4=0x8, adrs_b3=0x1)                   
	readback_DAC3_C = dac_read_reg_data (sub_ch_en_b4=0x8, adrs_b3=0x2)                   
	readback_DAC3_D = dac_read_reg_data (sub_ch_en_b4=0x8, adrs_b3=0x3)                   
	
	print('{} = 0x{:04X}, {} = 0x{:04X}'.format('set_data_DAC0_A', set_data_DAC0_A,'readback_DAC0_A', readback_DAC0_A))
	print('{} = 0x{:04X}, {} = 0x{:04X}'.format('set_data_DAC0_B', set_data_DAC0_B,'readback_DAC0_B', readback_DAC0_B))
	print('{} = 0x{:04X}, {} = 0x{:04X}'.format('set_data_DAC0_C', set_data_DAC0_C,'readback_DAC0_C', readback_DAC0_C))
	print('{} = 0x{:04X}, {} = 0x{:04X}'.format('set_data_DAC0_D', set_data_DAC0_D,'readback_DAC0_D', readback_DAC0_D))
	print('{} = 0x{:04X}, {} = 0x{:04X}'.format('set_data_DAC1_A', set_data_DAC1_A,'readback_DAC1_A', readback_DAC1_A))
	print('{} = 0x{:04X}, {} = 0x{:04X}'.format('set_data_DAC1_B', set_data_DAC1_B,'readback_DAC1_B', readback_DAC1_B))
	print('{} = 0x{:04X}, {} = 0x{:04X}'.format('set_data_DAC1_C', set_data_DAC1_C,'readback_DAC1_C', readback_DAC1_C))
	print('{} = 0x{:04X}, {} = 0x{:04X}'.format('set_data_DAC1_D', set_data_DAC1_D,'readback_DAC1_D', readback_DAC1_D))
	print('{} = 0x{:04X}, {} = 0x{:04X}'.format('set_data_DAC2_A', set_data_DAC2_A,'readback_DAC2_A', readback_DAC2_A))
	print('{} = 0x{:04X}, {} = 0x{:04X}'.format('set_data_DAC2_B', set_data_DAC2_B,'readback_DAC2_B', readback_DAC2_B))
	print('{} = 0x{:04X}, {} = 0x{:04X}'.format('set_data_DAC2_C', set_data_DAC2_C,'readback_DAC2_C', readback_DAC2_C))
	print('{} = 0x{:04X}, {} = 0x{:04X}'.format('set_data_DAC2_D', set_data_DAC2_D,'readback_DAC2_D', readback_DAC2_D))
	print('{} = 0x{:04X}, {} = 0x{:04X}'.format('set_data_DAC3_A', set_data_DAC3_A,'readback_DAC3_A', readback_DAC3_A))
	print('{} = 0x{:04X}, {} = 0x{:04X}'.format('set_data_DAC3_B', set_data_DAC3_B,'readback_DAC3_B', readback_DAC3_B))
	print('{} = 0x{:04X}, {} = 0x{:04X}'.format('set_data_DAC3_C', set_data_DAC3_C,'readback_DAC3_C', readback_DAC3_C))
	print('{} = 0x{:04X}, {} = 0x{:04X}'.format('set_data_DAC3_D', set_data_DAC3_D,'readback_DAC3_D', readback_DAC3_D))
	
	if  set_data_DAC0_A==readback_DAC0_A and \
		set_data_DAC0_B==readback_DAC0_B and \
		set_data_DAC0_C==readback_DAC0_C and \
		set_data_DAC0_D==readback_DAC0_D and \
		set_data_DAC1_A==readback_DAC1_A and \
		set_data_DAC1_B==readback_DAC1_B and \
		set_data_DAC1_C==readback_DAC1_C and \
		set_data_DAC1_D==readback_DAC1_D and \
		set_data_DAC2_A==readback_DAC2_A and \
		set_data_DAC2_B==readback_DAC2_B and \
		set_data_DAC2_C==readback_DAC2_C and \
		set_data_DAC2_D==readback_DAC2_D and \
		set_data_DAC3_A==readback_DAC3_A and \
		set_data_DAC3_B==readback_DAC3_B and \
		set_data_DAC3_C==readback_DAC3_C and \
		set_data_DAC3_D==readback_DAC3_D :
		print('### OK ###')
	else:
		#raise
		print('### NG ###')
		input('')	
		
	#### test send control 
	## dac_send_reg_control(sub_ch_en_b4=0x1, data_b16=0x0008) # TSD_en, no CC_en
	## dac_send_reg_control(sub_ch_en_b4=0x2, data_b16=0x0008) # TSD_en, no CC_en
	## dac_send_reg_control(sub_ch_en_b4=0x4, data_b16=0x0008) # TSD_en, no CC_en
	## dac_send_reg_control(sub_ch_en_b4=0x8, data_b16=0x0008) # TSD_en, no CC_en

	## master_bb_update_board_status_wo()
	
	#input('')	
	
	return


# TODO: dac error scale 
#__dac_scale__ = 0.632 # 1V --> 0.632V measure
__dac_scale__ = 1.0 # no scale

def conv_dac_code__from_string(tmp_ee_dac_skt1_ch1_val):
	tmp_ee_dac_skt1_ch1_val__float = float(tmp_ee_dac_skt1_ch1_val)/__dac_scale__
	##
	#if tmp_ee_dac_skt1_ch1_val__float >= 0:
	#	var_SKT1_CH1_DAC_VAL__code = int(tmp_ee_dac_skt1_ch1_val__float/10.0*0x7FFF)
	#else:
	#	var_SKT1_CH1_DAC_VAL__code = 0xFFFF # test
	##
	var_SKT1_CH1_DAC_VAL__code = conv_dec_to_bit_2s_comp_16bit(tmp_ee_dac_skt1_ch1_val__float)
	#
	return var_SKT1_CH1_DAC_VAL__code

def conv_string__from_dac_code(var_SKT1_CH1_DAC_RDB__code):
	tmp = var_SKT1_CH1_DAC_RDB__code
	tmp_ee_dac_skt1_ch1_rdb = conv_bit_2s_comp_16bit_to_dec(tmp)*__dac_scale__
	# string format
	tmp_str = '{:.8}'.format(tmp_ee_dac_skt1_ch1_rdb)
	#
	return tmp_str

def dac_bb_update():
	print('\n>>>>>> dac_bb_update')
	## read dac input entry // ee_dac_skt3_ch1_val
	tmp_ee_dac_skt1_ch1_val = ee_dac_skt1_ch1_val.get() # string
	tmp_ee_dac_skt2_ch1_val = ee_dac_skt2_ch1_val.get() # string
	tmp_ee_dac_skt3_ch1_val = ee_dac_skt3_ch1_val.get() # string
	tmp_ee_dac_skt4_ch1_val = ee_dac_skt4_ch1_val.get() # string
	tmp_ee_dac_skt5_ch1_val = ee_dac_skt5_ch1_val.get() # string
	tmp_ee_dac_skt6_ch1_val = ee_dac_skt6_ch1_val.get() # string
	tmp_ee_dac_skt7_ch1_val = ee_dac_skt7_ch1_val.get() # string
	tmp_ee_dac_skt8_ch1_val = ee_dac_skt8_ch1_val.get() # string
	tmp_ee_dac_skt1_ch2_val = ee_dac_skt1_ch2_val.get() # string
	tmp_ee_dac_skt2_ch2_val = ee_dac_skt2_ch2_val.get() # string
	tmp_ee_dac_skt3_ch2_val = ee_dac_skt3_ch2_val.get() # string
	tmp_ee_dac_skt4_ch2_val = ee_dac_skt4_ch2_val.get() # string
	tmp_ee_dac_skt5_ch2_val = ee_dac_skt5_ch2_val.get() # string
	tmp_ee_dac_skt6_ch2_val = ee_dac_skt6_ch2_val.get() # string
	tmp_ee_dac_skt7_ch2_val = ee_dac_skt7_ch2_val.get() # string
	tmp_ee_dac_skt8_ch2_val = ee_dac_skt8_ch2_val.get() # string
	
	## convert hex/int code from string
	var_SKT1_CH1_DAC_VAL__code = conv_dac_code__from_string(tmp_ee_dac_skt1_ch1_val)
	var_SKT2_CH1_DAC_VAL__code = conv_dac_code__from_string(tmp_ee_dac_skt2_ch1_val)
	var_SKT3_CH1_DAC_VAL__code = conv_dac_code__from_string(tmp_ee_dac_skt3_ch1_val)
	var_SKT4_CH1_DAC_VAL__code = conv_dac_code__from_string(tmp_ee_dac_skt4_ch1_val)
	var_SKT5_CH1_DAC_VAL__code = conv_dac_code__from_string(tmp_ee_dac_skt5_ch1_val)
	var_SKT6_CH1_DAC_VAL__code = conv_dac_code__from_string(tmp_ee_dac_skt6_ch1_val)
	var_SKT7_CH1_DAC_VAL__code = conv_dac_code__from_string(tmp_ee_dac_skt7_ch1_val)
	var_SKT8_CH1_DAC_VAL__code = conv_dac_code__from_string(tmp_ee_dac_skt8_ch1_val)
	var_SKT1_CH2_DAC_VAL__code = conv_dac_code__from_string(tmp_ee_dac_skt1_ch2_val)
	var_SKT2_CH2_DAC_VAL__code = conv_dac_code__from_string(tmp_ee_dac_skt2_ch2_val)
	var_SKT3_CH2_DAC_VAL__code = conv_dac_code__from_string(tmp_ee_dac_skt3_ch2_val)
	var_SKT4_CH2_DAC_VAL__code = conv_dac_code__from_string(tmp_ee_dac_skt4_ch2_val)
	var_SKT5_CH2_DAC_VAL__code = conv_dac_code__from_string(tmp_ee_dac_skt5_ch2_val)
	var_SKT6_CH2_DAC_VAL__code = conv_dac_code__from_string(tmp_ee_dac_skt6_ch2_val)
	var_SKT7_CH2_DAC_VAL__code = conv_dac_code__from_string(tmp_ee_dac_skt7_ch2_val)
	var_SKT8_CH2_DAC_VAL__code = conv_dac_code__from_string(tmp_ee_dac_skt8_ch2_val)
	
	## set wire_in 
	set_data_S1 = (var_SKT1_CH2_DAC_VAL__code<<16) + var_SKT1_CH1_DAC_VAL__code # 0 # 0x00000001 # // w_DAC_S1_WI  = ep18wire;
	set_data_S2 = (var_SKT2_CH2_DAC_VAL__code<<16) + var_SKT2_CH1_DAC_VAL__code # 0 # 0x00000002 # // w_DAC_S2_WI  = ep19wire;
	set_data_S3 = (var_SKT3_CH2_DAC_VAL__code<<16) + var_SKT3_CH1_DAC_VAL__code # 0 # 0x00000003 # // w_DAC_S3_WI  = ep1Awire;
	set_data_S4 = (var_SKT4_CH2_DAC_VAL__code<<16) + var_SKT4_CH1_DAC_VAL__code # 0 # 0x00000004 # // w_DAC_S4_WI  = ep1Bwire;
	set_data_S5 = (var_SKT5_CH2_DAC_VAL__code<<16) + var_SKT5_CH1_DAC_VAL__code # 0 # 0x00000005 # // w_DAC_S5_WI  = ep1Cwire;
	set_data_S6 = (var_SKT6_CH2_DAC_VAL__code<<16) + var_SKT6_CH1_DAC_VAL__code # 0 # 0x00000006 # // w_DAC_S6_WI  = ep1Dwire;
	set_data_S7 = (var_SKT7_CH2_DAC_VAL__code<<16) + var_SKT7_CH1_DAC_VAL__code # 0 # 0x00000007 # // w_DAC_S7_WI  = ep1Ewire;
	set_data_S8 = (var_SKT8_CH2_DAC_VAL__code<<16) + var_SKT8_CH1_DAC_VAL__code # 0 # 0x00000008 # // w_DAC_S8_WI  = ep1Fwire;
	dev.SetWireInValue(0x18,set_data_S1,0xFFFFFFFF) # (ep,val,mask)
	dev.SetWireInValue(0x19,set_data_S2,0xFFFFFFFF) # (ep,val,mask)
	dev.SetWireInValue(0x1A,set_data_S3,0xFFFFFFFF) # (ep,val,mask)
	dev.SetWireInValue(0x1B,set_data_S4,0xFFFFFFFF) # (ep,val,mask)
	dev.SetWireInValue(0x1C,set_data_S5,0xFFFFFFFF) # (ep,val,mask)
	dev.SetWireInValue(0x1D,set_data_S6,0xFFFFFFFF) # (ep,val,mask)
	dev.SetWireInValue(0x1E,set_data_S7,0xFFFFFFFF) # (ep,val,mask)
	dev.SetWireInValue(0x1F,set_data_S8,0xFFFFFFFF) # (ep,val,mask)
	dev.UpdateWireIns()	
	
	# select all DAC
	dac_set_sub_ch_en(0xF)
	
	## trig update 
	# ep46[3] // w_DAC_TRIG_TI // update
	# ep66[3] // w_DAC_TRIG_TO // update done check
	dac_send_trig_with_check(3) # bit_loc
	
	## readback wire_out
	dev.UpdateWireOuts()
	readback_S1 = dev.GetWireOutValue(0x38) # // ep38wire = w_DAC_S1_WO 
	readback_S2 = dev.GetWireOutValue(0x39) # // ep39wire = w_DAC_S2_WO 
	readback_S3 = dev.GetWireOutValue(0x3A) # // ep3Awire = w_DAC_S3_WO 
	readback_S4 = dev.GetWireOutValue(0x3B) # // ep3Bwire = w_DAC_S4_WO 
	readback_S5 = dev.GetWireOutValue(0x3C) # // ep3Cwire = w_DAC_S5_WO 
	readback_S6 = dev.GetWireOutValue(0x3D) # // ep3Dwire = w_DAC_S6_WO 
	readback_S7 = dev.GetWireOutValue(0x3E) # // ep3Ewire = w_DAC_S7_WO 
	readback_S8 = dev.GetWireOutValue(0x3F) # // ep3Fwire = w_DAC_S8_WO 
	
	## readback codes
	var_SKT1_CH1_DAC_RDB__code = (readback_S1 >>  0) & 0x0000FFFF
	var_SKT2_CH1_DAC_RDB__code = (readback_S2 >>  0) & 0x0000FFFF
	var_SKT3_CH1_DAC_RDB__code = (readback_S3 >>  0) & 0x0000FFFF
	var_SKT4_CH1_DAC_RDB__code = (readback_S4 >>  0) & 0x0000FFFF
	var_SKT5_CH1_DAC_RDB__code = (readback_S5 >>  0) & 0x0000FFFF
	var_SKT6_CH1_DAC_RDB__code = (readback_S6 >>  0) & 0x0000FFFF
	var_SKT7_CH1_DAC_RDB__code = (readback_S7 >>  0) & 0x0000FFFF
	var_SKT8_CH1_DAC_RDB__code = (readback_S8 >>  0) & 0x0000FFFF
	var_SKT1_CH2_DAC_RDB__code = (readback_S1 >> 16) & 0x0000FFFF
	var_SKT2_CH2_DAC_RDB__code = (readback_S2 >> 16) & 0x0000FFFF
	var_SKT3_CH2_DAC_RDB__code = (readback_S3 >> 16) & 0x0000FFFF
	var_SKT4_CH2_DAC_RDB__code = (readback_S4 >> 16) & 0x0000FFFF
	var_SKT5_CH2_DAC_RDB__code = (readback_S5 >> 16) & 0x0000FFFF
	var_SKT6_CH2_DAC_RDB__code = (readback_S6 >> 16) & 0x0000FFFF
	var_SKT7_CH2_DAC_RDB__code = (readback_S7 >> 16) & 0x0000FFFF
	var_SKT8_CH2_DAC_RDB__code = (readback_S8 >> 16) & 0x0000FFFF
	
	## convert string from hex/int code
	tmp_ee_dac_skt1_ch1_rdb = conv_string__from_dac_code(var_SKT1_CH1_DAC_RDB__code)
	tmp_ee_dac_skt2_ch1_rdb = conv_string__from_dac_code(var_SKT2_CH1_DAC_RDB__code)
	tmp_ee_dac_skt3_ch1_rdb = conv_string__from_dac_code(var_SKT3_CH1_DAC_RDB__code)
	tmp_ee_dac_skt4_ch1_rdb = conv_string__from_dac_code(var_SKT4_CH1_DAC_RDB__code)
	tmp_ee_dac_skt5_ch1_rdb = conv_string__from_dac_code(var_SKT5_CH1_DAC_RDB__code)
	tmp_ee_dac_skt6_ch1_rdb = conv_string__from_dac_code(var_SKT6_CH1_DAC_RDB__code)
	tmp_ee_dac_skt7_ch1_rdb = conv_string__from_dac_code(var_SKT7_CH1_DAC_RDB__code)
	tmp_ee_dac_skt8_ch1_rdb = conv_string__from_dac_code(var_SKT8_CH1_DAC_RDB__code)
	tmp_ee_dac_skt1_ch2_rdb = conv_string__from_dac_code(var_SKT1_CH2_DAC_RDB__code)
	tmp_ee_dac_skt2_ch2_rdb = conv_string__from_dac_code(var_SKT2_CH2_DAC_RDB__code)
	tmp_ee_dac_skt3_ch2_rdb = conv_string__from_dac_code(var_SKT3_CH2_DAC_RDB__code)
	tmp_ee_dac_skt4_ch2_rdb = conv_string__from_dac_code(var_SKT4_CH2_DAC_RDB__code)
	tmp_ee_dac_skt5_ch2_rdb = conv_string__from_dac_code(var_SKT5_CH2_DAC_RDB__code)
	tmp_ee_dac_skt6_ch2_rdb = conv_string__from_dac_code(var_SKT6_CH2_DAC_RDB__code)
	tmp_ee_dac_skt7_ch2_rdb = conv_string__from_dac_code(var_SKT7_CH2_DAC_RDB__code)
	tmp_ee_dac_skt8_ch2_rdb = conv_string__from_dac_code(var_SKT8_CH2_DAC_RDB__code)
	
	## convert float // var_SKTn_CHn_DAC_RDB
	
	
	## set readback entry // ee_dac_skt3_ch1_rdb
	ee_dac_skt1_ch1_rdb.delete(0,END)
	ee_dac_skt2_ch1_rdb.delete(0,END)
	ee_dac_skt3_ch1_rdb.delete(0,END)
	ee_dac_skt4_ch1_rdb.delete(0,END)
	ee_dac_skt5_ch1_rdb.delete(0,END)
	ee_dac_skt6_ch1_rdb.delete(0,END)
	ee_dac_skt7_ch1_rdb.delete(0,END)
	ee_dac_skt8_ch1_rdb.delete(0,END)
	ee_dac_skt1_ch2_rdb.delete(0,END)
	ee_dac_skt2_ch2_rdb.delete(0,END)
	ee_dac_skt3_ch2_rdb.delete(0,END)
	ee_dac_skt4_ch2_rdb.delete(0,END)
	ee_dac_skt5_ch2_rdb.delete(0,END)
	ee_dac_skt6_ch2_rdb.delete(0,END)
	ee_dac_skt7_ch2_rdb.delete(0,END)
	ee_dac_skt8_ch2_rdb.delete(0,END)
	#
	ee_dac_skt1_ch1_rdb.insert(0,tmp_ee_dac_skt1_ch1_rdb)
	ee_dac_skt2_ch1_rdb.insert(0,tmp_ee_dac_skt2_ch1_rdb)
	ee_dac_skt3_ch1_rdb.insert(0,tmp_ee_dac_skt3_ch1_rdb)
	ee_dac_skt4_ch1_rdb.insert(0,tmp_ee_dac_skt4_ch1_rdb)
	ee_dac_skt5_ch1_rdb.insert(0,tmp_ee_dac_skt5_ch1_rdb)
	ee_dac_skt6_ch1_rdb.insert(0,tmp_ee_dac_skt6_ch1_rdb)
	ee_dac_skt7_ch1_rdb.insert(0,tmp_ee_dac_skt7_ch1_rdb)
	ee_dac_skt8_ch1_rdb.insert(0,tmp_ee_dac_skt8_ch1_rdb)
	ee_dac_skt1_ch2_rdb.insert(0,tmp_ee_dac_skt1_ch2_rdb)
	ee_dac_skt2_ch2_rdb.insert(0,tmp_ee_dac_skt2_ch2_rdb)
	ee_dac_skt3_ch2_rdb.insert(0,tmp_ee_dac_skt3_ch2_rdb)
	ee_dac_skt4_ch2_rdb.insert(0,tmp_ee_dac_skt4_ch2_rdb)
	ee_dac_skt5_ch2_rdb.insert(0,tmp_ee_dac_skt5_ch2_rdb)
	ee_dac_skt6_ch2_rdb.insert(0,tmp_ee_dac_skt6_ch2_rdb)
	ee_dac_skt7_ch2_rdb.insert(0,tmp_ee_dac_skt7_ch2_rdb)
	ee_dac_skt8_ch2_rdb.insert(0,tmp_ee_dac_skt8_ch2_rdb)

	## report
	print('\n>>>>>>')
	print('{} = {}'.format('tmp_ee_dac_skt1_ch1_val', tmp_ee_dac_skt1_ch1_val))
	print('{} = {}'.format('tmp_ee_dac_skt2_ch1_val', tmp_ee_dac_skt2_ch1_val))
	print('{} = {}'.format('tmp_ee_dac_skt3_ch1_val', tmp_ee_dac_skt3_ch1_val))
	print('{} = {}'.format('tmp_ee_dac_skt4_ch1_val', tmp_ee_dac_skt4_ch1_val))
	print('{} = {}'.format('tmp_ee_dac_skt5_ch1_val', tmp_ee_dac_skt5_ch1_val))
	print('{} = {}'.format('tmp_ee_dac_skt6_ch1_val', tmp_ee_dac_skt6_ch1_val))
	print('{} = {}'.format('tmp_ee_dac_skt7_ch1_val', tmp_ee_dac_skt7_ch1_val))
	print('{} = {}'.format('tmp_ee_dac_skt8_ch1_val', tmp_ee_dac_skt8_ch1_val))
	print('{} = {}'.format('tmp_ee_dac_skt1_ch2_val', tmp_ee_dac_skt1_ch2_val))
	print('{} = {}'.format('tmp_ee_dac_skt2_ch2_val', tmp_ee_dac_skt2_ch2_val))
	print('{} = {}'.format('tmp_ee_dac_skt3_ch2_val', tmp_ee_dac_skt3_ch2_val))
	print('{} = {}'.format('tmp_ee_dac_skt4_ch2_val', tmp_ee_dac_skt4_ch2_val))
	print('{} = {}'.format('tmp_ee_dac_skt5_ch2_val', tmp_ee_dac_skt5_ch2_val))
	print('{} = {}'.format('tmp_ee_dac_skt6_ch2_val', tmp_ee_dac_skt6_ch2_val))
	print('{} = {}'.format('tmp_ee_dac_skt7_ch2_val', tmp_ee_dac_skt7_ch2_val))
	print('{} = {}'.format('tmp_ee_dac_skt8_ch2_val', tmp_ee_dac_skt8_ch2_val))
	print('\n>>>>>>')
	print('{} = 0x{:04X}'.format('var_SKT1_CH1_DAC_VAL__code', var_SKT1_CH1_DAC_VAL__code))
	print('{} = 0x{:04X}'.format('var_SKT2_CH1_DAC_VAL__code', var_SKT2_CH1_DAC_VAL__code))
	print('{} = 0x{:04X}'.format('var_SKT3_CH1_DAC_VAL__code', var_SKT3_CH1_DAC_VAL__code))
	print('{} = 0x{:04X}'.format('var_SKT4_CH1_DAC_VAL__code', var_SKT4_CH1_DAC_VAL__code))
	print('{} = 0x{:04X}'.format('var_SKT5_CH1_DAC_VAL__code', var_SKT5_CH1_DAC_VAL__code))
	print('{} = 0x{:04X}'.format('var_SKT6_CH1_DAC_VAL__code', var_SKT6_CH1_DAC_VAL__code))
	print('{} = 0x{:04X}'.format('var_SKT7_CH1_DAC_VAL__code', var_SKT7_CH1_DAC_VAL__code))
	print('{} = 0x{:04X}'.format('var_SKT8_CH1_DAC_VAL__code', var_SKT8_CH1_DAC_VAL__code))
	print('{} = 0x{:04X}'.format('var_SKT1_CH2_DAC_VAL__code', var_SKT1_CH2_DAC_VAL__code))
	print('{} = 0x{:04X}'.format('var_SKT2_CH2_DAC_VAL__code', var_SKT2_CH2_DAC_VAL__code))
	print('{} = 0x{:04X}'.format('var_SKT3_CH2_DAC_VAL__code', var_SKT3_CH2_DAC_VAL__code))
	print('{} = 0x{:04X}'.format('var_SKT4_CH2_DAC_VAL__code', var_SKT4_CH2_DAC_VAL__code))
	print('{} = 0x{:04X}'.format('var_SKT5_CH2_DAC_VAL__code', var_SKT5_CH2_DAC_VAL__code))
	print('{} = 0x{:04X}'.format('var_SKT6_CH2_DAC_VAL__code', var_SKT6_CH2_DAC_VAL__code))
	print('{} = 0x{:04X}'.format('var_SKT7_CH2_DAC_VAL__code', var_SKT7_CH2_DAC_VAL__code))
	print('{} = 0x{:04X}'.format('var_SKT8_CH2_DAC_VAL__code', var_SKT8_CH2_DAC_VAL__code))
	print('\n>>>>>>')
	print('{} = 0x{:08X}'.format('set_data_S1', set_data_S1))
	print('{} = 0x{:08X}'.format('set_data_S2', set_data_S2))
	print('{} = 0x{:08X}'.format('set_data_S3', set_data_S3))
	print('{} = 0x{:08X}'.format('set_data_S4', set_data_S4))
	print('{} = 0x{:08X}'.format('set_data_S5', set_data_S5))
	print('{} = 0x{:08X}'.format('set_data_S6', set_data_S6))
	print('{} = 0x{:08X}'.format('set_data_S7', set_data_S7))
	print('{} = 0x{:08X}'.format('set_data_S8', set_data_S8))
	print('\n>>>>>>')
	print('{} = 0x{:08X}'.format('readback_S1', readback_S1))
	print('{} = 0x{:08X}'.format('readback_S2', readback_S2))
	print('{} = 0x{:08X}'.format('readback_S3', readback_S3))
	print('{} = 0x{:08X}'.format('readback_S4', readback_S4))
	print('{} = 0x{:08X}'.format('readback_S5', readback_S5))
	print('{} = 0x{:08X}'.format('readback_S6', readback_S6))
	print('{} = 0x{:08X}'.format('readback_S7', readback_S7))
	print('{} = 0x{:08X}'.format('readback_S8', readback_S8))
	#
	print('\n>>>>>>')
	print('{} = 0x{:04X}'.format('var_SKT1_CH1_DAC_RDB__code', var_SKT1_CH1_DAC_RDB__code))
	print('{} = 0x{:04X}'.format('var_SKT2_CH1_DAC_RDB__code', var_SKT2_CH1_DAC_RDB__code))
	print('{} = 0x{:04X}'.format('var_SKT3_CH1_DAC_RDB__code', var_SKT3_CH1_DAC_RDB__code))
	print('{} = 0x{:04X}'.format('var_SKT4_CH1_DAC_RDB__code', var_SKT4_CH1_DAC_RDB__code))
	print('{} = 0x{:04X}'.format('var_SKT5_CH1_DAC_RDB__code', var_SKT5_CH1_DAC_RDB__code))
	print('{} = 0x{:04X}'.format('var_SKT6_CH1_DAC_RDB__code', var_SKT6_CH1_DAC_RDB__code))
	print('{} = 0x{:04X}'.format('var_SKT7_CH1_DAC_RDB__code', var_SKT7_CH1_DAC_RDB__code))
	print('{} = 0x{:04X}'.format('var_SKT8_CH1_DAC_RDB__code', var_SKT8_CH1_DAC_RDB__code))
	print('{} = 0x{:04X}'.format('var_SKT1_CH2_DAC_RDB__code', var_SKT1_CH2_DAC_RDB__code))
	print('{} = 0x{:04X}'.format('var_SKT2_CH2_DAC_RDB__code', var_SKT2_CH2_DAC_RDB__code))
	print('{} = 0x{:04X}'.format('var_SKT3_CH2_DAC_RDB__code', var_SKT3_CH2_DAC_RDB__code))
	print('{} = 0x{:04X}'.format('var_SKT4_CH2_DAC_RDB__code', var_SKT4_CH2_DAC_RDB__code))
	print('{} = 0x{:04X}'.format('var_SKT5_CH2_DAC_RDB__code', var_SKT5_CH2_DAC_RDB__code))
	print('{} = 0x{:04X}'.format('var_SKT6_CH2_DAC_RDB__code', var_SKT6_CH2_DAC_RDB__code))
	print('{} = 0x{:04X}'.format('var_SKT7_CH2_DAC_RDB__code', var_SKT7_CH2_DAC_RDB__code))
	print('{} = 0x{:04X}'.format('var_SKT8_CH2_DAC_RDB__code', var_SKT8_CH2_DAC_RDB__code))
	#
	print('\n>>>>>>')
	print('{} = {}'.format('tmp_ee_dac_skt1_ch1_rdb', tmp_ee_dac_skt1_ch1_rdb))
	print('{} = {}'.format('tmp_ee_dac_skt2_ch1_rdb', tmp_ee_dac_skt2_ch1_rdb))
	print('{} = {}'.format('tmp_ee_dac_skt3_ch1_rdb', tmp_ee_dac_skt3_ch1_rdb))
	print('{} = {}'.format('tmp_ee_dac_skt4_ch1_rdb', tmp_ee_dac_skt4_ch1_rdb))
	print('{} = {}'.format('tmp_ee_dac_skt5_ch1_rdb', tmp_ee_dac_skt5_ch1_rdb))
	print('{} = {}'.format('tmp_ee_dac_skt6_ch1_rdb', tmp_ee_dac_skt6_ch1_rdb))
	print('{} = {}'.format('tmp_ee_dac_skt7_ch1_rdb', tmp_ee_dac_skt7_ch1_rdb))
	print('{} = {}'.format('tmp_ee_dac_skt8_ch1_rdb', tmp_ee_dac_skt8_ch1_rdb))
	print('{} = {}'.format('tmp_ee_dac_skt1_ch2_rdb', tmp_ee_dac_skt1_ch2_rdb))
	print('{} = {}'.format('tmp_ee_dac_skt2_ch2_rdb', tmp_ee_dac_skt2_ch2_rdb))
	print('{} = {}'.format('tmp_ee_dac_skt3_ch2_rdb', tmp_ee_dac_skt3_ch2_rdb))
	print('{} = {}'.format('tmp_ee_dac_skt4_ch2_rdb', tmp_ee_dac_skt4_ch2_rdb))
	print('{} = {}'.format('tmp_ee_dac_skt5_ch2_rdb', tmp_ee_dac_skt5_ch2_rdb))
	print('{} = {}'.format('tmp_ee_dac_skt6_ch2_rdb', tmp_ee_dac_skt6_ch2_rdb))
	print('{} = {}'.format('tmp_ee_dac_skt7_ch2_rdb', tmp_ee_dac_skt7_ch2_rdb))
	print('{} = {}'.format('tmp_ee_dac_skt8_ch2_rdb', tmp_ee_dac_skt8_ch2_rdb))
	#
	pass


## TODO: MHVSU ADC functions 

def adc_pwr_on():
	print('\n>>>>>> adc_pwr_on')
	dev.SetWireInValue(0x03,0x01,0x00000001) # (ep,val,mask)
	dev.UpdateWireIns()
	
def adc_pwr_off():
	print('\n>>>>>> adc_pwr_off')
	dev.SetWireInValue(0x03,0x00,0x00000001) # (ep,val,mask)
	dev.UpdateWireIns()

def adc_bb_update_pwr_con():
	print('\n>>>>>> adc_bb_update_pwr_con')
	#
	# read gui variable
	print('{} = {}'.format('var_ADC_PWR_ON', var_ADC_PWR_ON.get()))
	#
	if var_ADC_PWR_ON.get()==1:	
		adc_pwr_on()
	else:
		adc_pwr_off()
	#
	pass

def adc_min_port_en():
	print('\n>>>>>> adc_min_port_en')
	#
	adc_con_wi = 0x00000100
	
	# set wire in 
	dev.SetWireInValue(0x07,adc_con_wi,0x00000100) # (ep,val,mask)
	dev.UpdateWireIns()
	#
	pass
def adc_min_port_dis():
	print('\n>>>>>> adc_min_port_dis')
	#
	adc_con_wi = 0x00000000
	
	# set wire in 
	dev.SetWireInValue(0x07,adc_con_wi,0x00000100) # (ep,val,mask)
	dev.UpdateWireIns()
	#
	pass
def adc_max_port_en():
	print('\n>>>>>> adc_max_port_en')
	#
	adc_con_wi = 0x00000200
	
	# set wire in 
	dev.SetWireInValue(0x07,adc_con_wi,0x00000200) # (ep,val,mask)
	dev.UpdateWireIns()
	#
	pass
def adc_max_port_dis():
	print('\n>>>>>> adc_max_port_dis')
	#
	adc_con_wi = 0x00000000
	
	# set wire in 
	dev.SetWireInValue(0x07,adc_con_wi,0x00000200) # (ep,val,mask)
	dev.UpdateWireIns()
	#
	pass


def adc_bb_update_acc_con():
	print('\n>>>>>> adc_bb_update_acc_con')
	#
	# read gui variable
	print('{} = {}'.format('var_ADC_ACC_disable    ', var_ADC_ACC_disable    .get()))
	print('{} = {}'.format('var_ADC_ACC_16bit_shift', var_ADC_ACC_16bit_shift.get()))
	#
	adc_con_wi = 0x00000000
	#
	if var_ADC_ACC_disable.get()==1:	
		adc_con_wi += 0x00000010
	if var_ADC_ACC_16bit_shift.get()==1:	
		adc_con_wi += 0x00000020
	#
	
	# set wire in 
	dev.SetWireInValue(0x07,adc_con_wi,0x00000030) # (ep,val,mask)
	dev.UpdateWireIns()
	
	#
	pass

def adc_bb_update_port_con():
	print('\n>>>>>> adc_bb_update_port_con')
	#
	# read gui variable
	print('{} = {}'.format('var_ADC_MIN_ON', var_ADC_MIN_ON.get()))
	print('{} = {}'.format('var_ADC_MAX_ON', var_ADC_MAX_ON.get()))
	#
	if var_ADC_MIN_ON.get()==1:	
		adc_min_port_en()
	else:
		adc_min_port_dis()
	#
	if var_ADC_MAX_ON.get()==1:	
		adc_max_port_en()
	else:
		adc_max_port_dis()
	#
	pass



def adc_bb_enable_normal_mode():
	print('\n>>>>>> adc_bb_enable_normal_mode')
	#
	adc_con_wi = 0x00000001
	
	# set wire in 
	dev.SetWireInValue(0x07,adc_con_wi,0xFFFFFFFF) # (ep,val,mask)
	dev.UpdateWireIns()
	#
	pass

def adc_bb_disable_normal_mode():
	print('\n>>>>>> adc_bb_disable_normal_mode')
	#
	adc_con_wi = 0x00000000
	
	# set wire in 
	dev.SetWireInValue(0x07,adc_con_wi,0xFFFFFFFF) # (ep,val,mask)
	dev.UpdateWireIns()
	#
	pass

def adc_disable():
	adc_bb_disable_normal_mode()

def adc_bb_update_con_wi():
	print('\n>>>>>> adc_bb_update_con_wi')
	#
	# read entry
	adc_con_wi__hexstr = ee_adc_con_wi.get() # string
	print('{} = {}'.format('adc_con_wi__hexstr', adc_con_wi__hexstr))
	#
	# convert hexstr into int 
	try:
		adc_con_wi = int(adc_con_wi__hexstr,16)
		print('{} = 0x{:08X}'.format('adc_con_wi', adc_con_wi))
	except:
		print('failed when converting hexstr into int')
		return
	#
	# set wire in 
	dev.SetWireInValue(0x07,adc_con_wi,0xFFFFFFFF) # (ep,val,mask)
	dev.UpdateWireIns()
	#
	pass

def adc_bb_update_sta_wo():
	print('\n>>>>>> adc_bb_update_sta_wo')
	#
	# read endpoint
	dev.UpdateWireOuts()
	adc_status_wo = dev.GetWireOutValue(0x27)
	print('{} = 0x{:08X}'.format('adc_status_wo', adc_status_wo))
	#
	# convert string
	adc_status_wo__hexstr = '0x{:08X}'.format(adc_status_wo)
	#
	# delete the old contents
	ee_adc_sta_wo.delete(0,END)
	#
	# update gui variable
	ee_adc_sta_wo.insert(0,adc_status_wo__hexstr)
	#
	pass


def adc__send_force__sclk_conv(sig_sclk, sig_conv, force_mode, enable):
	# forced mode located in bit[16] @ 0x07
	# forced conv located in bit[17] @ 0x07
	# forced sclk located in bit[18] @ 0x07
	#
	wire_data = 0x00000000 # set enable 
	#
	if enable==1:
		wire_data += 0x00000001
	if force_mode==1:
		wire_data += 0x00010000
	if sig_conv==1:
		wire_data += 0x00020000
	if sig_sclk==1:
		wire_data += 0x00040000
	#forced sclk / cnv / force mdoe
	dev.SetWireInValue(0x07,wire_data,0x00070001) # (ep,val,mask)
	dev.UpdateWireIns()
	pass


def adc_bb_enable_force_mode():
	print('\n>>>>>> adc_bb_enable_force_mode')
	#
	adc__send_force__sclk_conv(sig_sclk=0, sig_conv=1, force_mode=1, enable=1)
	#
	pass
	
def adc_bb_disable_force_mode():
	print('\n>>>>>> adc_bb_disable_force_mode')
	#
	adc__send_force__sclk_conv(sig_sclk=0, sig_conv=0, force_mode=0, enable=1)
	#
	pass
	
def adc_bb_trig_conv_pulse():
	print('\n>>>>>> adc_bb_trig_conv_pulse')
	#
	adc__send_force__sclk_conv(sig_sclk=0, sig_conv=1, force_mode=1, enable=1)
	#
	adc__send_force__sclk_conv(sig_sclk=0, sig_conv=0, force_mode=1, enable=1)
	#
	adc__send_force__sclk_conv(sig_sclk=0, sig_conv=1, force_mode=1, enable=1)
	#
	pass

def update__adc_data__from_serial_bit(serial_data, update_bit):
	ret = (serial_data<<1) + (update_bit&0x0001)
	return ret

def adc_bb_trig_sclk_pulses():
	print('\n>>>>>> adc_bb_trig_sclk_pulses')
	#
	# forced mode located in bit[16] @ 0x07
	# forced conv located in bit[17] @ 0x07
	# forced sclk located in bit[18] @ 0x07
	#
	mon__ADC0_SDO_A = 0;
	mon__ADC0_SDO_B = 0;
	mon__ADC0_SDO_C = 0;
	mon__ADC0_SDO_D = 0;
	mon__ADC1_SDO_A = 0;
	mon__ADC1_SDO_B = 0;
	mon__ADC1_SDO_C = 0;
	mon__ADC1_SDO_D = 0;
	mon__ADC2_SDO_A = 0;
	mon__ADC2_SDO_B = 0;
	mon__ADC2_SDO_C = 0;
	mon__ADC2_SDO_D = 0;
	mon__ADC3_SDO_A = 0;
	mon__ADC3_SDO_B = 0;
	mon__ADC3_SDO_C = 0;
	mon__ADC3_SDO_D = 0;
	#
	num_repeat = 8
	#
	cnt_repeat = 0 
	#
	while True:
		# read endpoint
		dev.UpdateWireOuts()
		adc_status_wo = dev.GetWireOutValue(0x27)
		print('{} LO: {} = 0x{:08X}'.format(cnt_repeat, 'adc_status_wo', adc_status_wo))
		#
		# update serial data 
		mon__ADC0_SDO_A = update__adc_data__from_serial_bit(mon__ADC0_SDO_A, adc_status_wo>>16);
		mon__ADC0_SDO_B = update__adc_data__from_serial_bit(mon__ADC0_SDO_B, adc_status_wo>>17);
		mon__ADC0_SDO_C = update__adc_data__from_serial_bit(mon__ADC0_SDO_C, adc_status_wo>>18);
		mon__ADC0_SDO_D = update__adc_data__from_serial_bit(mon__ADC0_SDO_D, adc_status_wo>>19);
		mon__ADC1_SDO_A = update__adc_data__from_serial_bit(mon__ADC1_SDO_A, adc_status_wo>>20);
		mon__ADC1_SDO_B = update__adc_data__from_serial_bit(mon__ADC1_SDO_B, adc_status_wo>>21);
		mon__ADC1_SDO_C = update__adc_data__from_serial_bit(mon__ADC1_SDO_C, adc_status_wo>>22);
		mon__ADC1_SDO_D = update__adc_data__from_serial_bit(mon__ADC1_SDO_D, adc_status_wo>>23);
		mon__ADC2_SDO_A = update__adc_data__from_serial_bit(mon__ADC2_SDO_A, adc_status_wo>>24);
		mon__ADC2_SDO_B = update__adc_data__from_serial_bit(mon__ADC2_SDO_B, adc_status_wo>>25);
		mon__ADC2_SDO_C = update__adc_data__from_serial_bit(mon__ADC2_SDO_C, adc_status_wo>>26);
		mon__ADC2_SDO_D = update__adc_data__from_serial_bit(mon__ADC2_SDO_D, adc_status_wo>>27);
		mon__ADC3_SDO_A = update__adc_data__from_serial_bit(mon__ADC3_SDO_A, adc_status_wo>>28);
		mon__ADC3_SDO_B = update__adc_data__from_serial_bit(mon__ADC3_SDO_B, adc_status_wo>>29);
		mon__ADC3_SDO_C = update__adc_data__from_serial_bit(mon__ADC3_SDO_C, adc_status_wo>>30);
		mon__ADC3_SDO_D = update__adc_data__from_serial_bit(mon__ADC3_SDO_D, adc_status_wo>>31);
		#
		# forced sclk high / cnv high
		adc__send_force__sclk_conv(sig_sclk=1, sig_conv=1, force_mode=1, enable=1)
		#
		# read endpoint
		dev.UpdateWireOuts()
		adc_status_wo = dev.GetWireOutValue(0x27)
		print('{} HI: {} = 0x{:08X}'.format(cnt_repeat, 'adc_status_wo', adc_status_wo))
		#
		# update serial data 
		mon__ADC0_SDO_A = update__adc_data__from_serial_bit(mon__ADC0_SDO_A, adc_status_wo>>16);
		mon__ADC0_SDO_B = update__adc_data__from_serial_bit(mon__ADC0_SDO_B, adc_status_wo>>17);
		mon__ADC0_SDO_C = update__adc_data__from_serial_bit(mon__ADC0_SDO_C, adc_status_wo>>18);
		mon__ADC0_SDO_D = update__adc_data__from_serial_bit(mon__ADC0_SDO_D, adc_status_wo>>19);
		mon__ADC1_SDO_A = update__adc_data__from_serial_bit(mon__ADC1_SDO_A, adc_status_wo>>20);
		mon__ADC1_SDO_B = update__adc_data__from_serial_bit(mon__ADC1_SDO_B, adc_status_wo>>21);
		mon__ADC1_SDO_C = update__adc_data__from_serial_bit(mon__ADC1_SDO_C, adc_status_wo>>22);
		mon__ADC1_SDO_D = update__adc_data__from_serial_bit(mon__ADC1_SDO_D, adc_status_wo>>23);
		mon__ADC2_SDO_A = update__adc_data__from_serial_bit(mon__ADC2_SDO_A, adc_status_wo>>24);
		mon__ADC2_SDO_B = update__adc_data__from_serial_bit(mon__ADC2_SDO_B, adc_status_wo>>25);
		mon__ADC2_SDO_C = update__adc_data__from_serial_bit(mon__ADC2_SDO_C, adc_status_wo>>26);
		mon__ADC2_SDO_D = update__adc_data__from_serial_bit(mon__ADC2_SDO_D, adc_status_wo>>27);
		mon__ADC3_SDO_A = update__adc_data__from_serial_bit(mon__ADC3_SDO_A, adc_status_wo>>28);
		mon__ADC3_SDO_B = update__adc_data__from_serial_bit(mon__ADC3_SDO_B, adc_status_wo>>29);
		mon__ADC3_SDO_C = update__adc_data__from_serial_bit(mon__ADC3_SDO_C, adc_status_wo>>30);
		mon__ADC3_SDO_D = update__adc_data__from_serial_bit(mon__ADC3_SDO_D, adc_status_wo>>31);
		#
		# forced sclk low / cnv high
		adc__send_force__sclk_conv(sig_sclk=0, sig_conv=1, force_mode=1, enable=1)
		#
		# repeat
		cnt_repeat += 1
		if cnt_repeat >= num_repeat:
			break
	#
	# check serial data on each channel : adc_status_wo[31:16]
	print('{} = 0x{:04X}'.format('mon__ADC0_SDO_A', mon__ADC0_SDO_A))
	print('{} = 0x{:04X}'.format('mon__ADC0_SDO_B', mon__ADC0_SDO_B))
	print('{} = 0x{:04X}'.format('mon__ADC0_SDO_C', mon__ADC0_SDO_C))
	print('{} = 0x{:04X}'.format('mon__ADC0_SDO_D', mon__ADC0_SDO_D))
	print('{} = 0x{:04X}'.format('mon__ADC1_SDO_A', mon__ADC1_SDO_A))
	print('{} = 0x{:04X}'.format('mon__ADC1_SDO_B', mon__ADC1_SDO_B))
	print('{} = 0x{:04X}'.format('mon__ADC1_SDO_C', mon__ADC1_SDO_C))
	print('{} = 0x{:04X}'.format('mon__ADC1_SDO_D', mon__ADC1_SDO_D))
	print('{} = 0x{:04X}'.format('mon__ADC2_SDO_A', mon__ADC2_SDO_A))
	print('{} = 0x{:04X}'.format('mon__ADC2_SDO_B', mon__ADC2_SDO_B))
	print('{} = 0x{:04X}'.format('mon__ADC2_SDO_C', mon__ADC2_SDO_C))
	print('{} = 0x{:04X}'.format('mon__ADC2_SDO_D', mon__ADC2_SDO_D))
	print('{} = 0x{:04X}'.format('mon__ADC3_SDO_A', mon__ADC3_SDO_A))
	print('{} = 0x{:04X}'.format('mon__ADC3_SDO_B', mon__ADC3_SDO_B))
	print('{} = 0x{:04X}'.format('mon__ADC3_SDO_C', mon__ADC3_SDO_C))
	print('{} = 0x{:04X}'.format('mon__ADC3_SDO_D', mon__ADC3_SDO_D))
	
	#
	pass


def adc_bb_trig_reset():
	print('\n>>>>>> adc_bb_trig_reset')

	## trig update 
	# ep47[0] // w_ADC_TRIG_TI 
	dev.ActivateTriggerIn(0x47, 0)
	
	## no done check 
	pass

def adc_bb_update_last_values_wo():
	print('\n>>>>>> adc_bb_update_last_values_wo')
	#
	# read endpoint
	dev.UpdateWireOuts()
	#
	adc_skt1_wo = dev.GetWireOutValue(0x30) # ADC_Sn_WO
	adc_skt2_wo = dev.GetWireOutValue(0x31) # ADC_Sn_WO
	adc_skt3_wo = dev.GetWireOutValue(0x32) # ADC_Sn_WO
	adc_skt4_wo = dev.GetWireOutValue(0x33) # ADC_Sn_WO
	adc_skt5_wo = dev.GetWireOutValue(0x34) # ADC_Sn_WO
	adc_skt6_wo = dev.GetWireOutValue(0x35) # ADC_Sn_WO
	adc_skt7_wo = dev.GetWireOutValue(0x36) # ADC_Sn_WO
	adc_skt8_wo = dev.GetWireOutValue(0x37) # ADC_Sn_WO
	#
	adc_acc_skt1_wo = dev.GetWireOutValue(0x28) # ADC_Sn_ACC_WO
	adc_acc_skt2_wo = dev.GetWireOutValue(0x29) # ADC_Sn_ACC_WO
	adc_acc_skt3_wo = dev.GetWireOutValue(0x2A) # ADC_Sn_ACC_WO
	adc_acc_skt4_wo = dev.GetWireOutValue(0x2B) # ADC_Sn_ACC_WO
	adc_acc_skt5_wo = dev.GetWireOutValue(0x2C) # ADC_Sn_ACC_WO
	adc_acc_skt6_wo = dev.GetWireOutValue(0x2D) # ADC_Sn_ACC_WO
	adc_acc_skt7_wo = dev.GetWireOutValue(0x2E) # ADC_Sn_ACC_WO
	adc_acc_skt8_wo = dev.GetWireOutValue(0x2F) # ADC_Sn_ACC_WO
	#
	print('{} = 0x{:08X}'.format('adc_skt1_wo', adc_skt1_wo))
	print('{} = 0x{:08X}'.format('adc_skt2_wo', adc_skt2_wo))
	print('{} = 0x{:08X}'.format('adc_skt3_wo', adc_skt3_wo))
	print('{} = 0x{:08X}'.format('adc_skt4_wo', adc_skt4_wo))
	print('{} = 0x{:08X}'.format('adc_skt5_wo', adc_skt5_wo))
	print('{} = 0x{:08X}'.format('adc_skt6_wo', adc_skt6_wo))
	print('{} = 0x{:08X}'.format('adc_skt7_wo', adc_skt7_wo))
	print('{} = 0x{:08X}'.format('adc_skt8_wo', adc_skt8_wo))
	#
	print('{} = 0x{:08X}'.format('adc_acc_skt1_wo', adc_acc_skt1_wo))
	print('{} = 0x{:08X}'.format('adc_acc_skt2_wo', adc_acc_skt2_wo))
	print('{} = 0x{:08X}'.format('adc_acc_skt3_wo', adc_acc_skt3_wo))
	print('{} = 0x{:08X}'.format('adc_acc_skt4_wo', adc_acc_skt4_wo))
	print('{} = 0x{:08X}'.format('adc_acc_skt5_wo', adc_acc_skt5_wo))
	print('{} = 0x{:08X}'.format('adc_acc_skt6_wo', adc_acc_skt6_wo))
	print('{} = 0x{:08X}'.format('adc_acc_skt7_wo', adc_acc_skt7_wo))
	print('{} = 0x{:08X}'.format('adc_acc_skt8_wo', adc_acc_skt8_wo))
	#
	# convert string
	adc_skt1_wo__hexstr = '0x{:08X}'.format(adc_skt1_wo)
	adc_skt2_wo__hexstr = '0x{:08X}'.format(adc_skt2_wo)
	adc_skt3_wo__hexstr = '0x{:08X}'.format(adc_skt3_wo)
	adc_skt4_wo__hexstr = '0x{:08X}'.format(adc_skt4_wo)
	adc_skt5_wo__hexstr = '0x{:08X}'.format(adc_skt5_wo)
	adc_skt6_wo__hexstr = '0x{:08X}'.format(adc_skt6_wo)
	adc_skt7_wo__hexstr = '0x{:08X}'.format(adc_skt7_wo)
	adc_skt8_wo__hexstr = '0x{:08X}'.format(adc_skt8_wo)
	#
	adc_acc_skt1_wo__hexstr = '0x{:08X}'.format(adc_acc_skt1_wo)
	adc_acc_skt2_wo__hexstr = '0x{:08X}'.format(adc_acc_skt2_wo)
	adc_acc_skt3_wo__hexstr = '0x{:08X}'.format(adc_acc_skt3_wo)
	adc_acc_skt4_wo__hexstr = '0x{:08X}'.format(adc_acc_skt4_wo)
	adc_acc_skt5_wo__hexstr = '0x{:08X}'.format(adc_acc_skt5_wo)
	adc_acc_skt6_wo__hexstr = '0x{:08X}'.format(adc_acc_skt6_wo)
	adc_acc_skt7_wo__hexstr = '0x{:08X}'.format(adc_acc_skt7_wo)
	adc_acc_skt8_wo__hexstr = '0x{:08X}'.format(adc_acc_skt8_wo)
	#
	# delete the old contents
	ee_adc_s1_wo.delete(0,END)
	ee_adc_s2_wo.delete(0,END)
	ee_adc_s3_wo.delete(0,END)
	ee_adc_s4_wo.delete(0,END)
	ee_adc_s5_wo.delete(0,END)
	ee_adc_s6_wo.delete(0,END)
	ee_adc_s7_wo.delete(0,END)
	ee_adc_s8_wo.delete(0,END)
	#
	ee_adc_acc_s1_wo.delete(0,END)
	ee_adc_acc_s2_wo.delete(0,END)
	ee_adc_acc_s3_wo.delete(0,END)
	ee_adc_acc_s4_wo.delete(0,END)
	ee_adc_acc_s5_wo.delete(0,END)
	ee_adc_acc_s6_wo.delete(0,END)
	ee_adc_acc_s7_wo.delete(0,END)
	ee_adc_acc_s8_wo.delete(0,END)
	#
	# update gui variable
	ee_adc_s1_wo.insert(0,adc_skt1_wo__hexstr)
	ee_adc_s2_wo.insert(0,adc_skt2_wo__hexstr)
	ee_adc_s3_wo.insert(0,adc_skt3_wo__hexstr)
	ee_adc_s4_wo.insert(0,adc_skt4_wo__hexstr)
	ee_adc_s5_wo.insert(0,adc_skt5_wo__hexstr)
	ee_adc_s6_wo.insert(0,adc_skt6_wo__hexstr)
	ee_adc_s7_wo.insert(0,adc_skt7_wo__hexstr)
	ee_adc_s8_wo.insert(0,adc_skt8_wo__hexstr)
	#	
	ee_adc_acc_s1_wo.insert(0,adc_acc_skt1_wo__hexstr)
	ee_adc_acc_s2_wo.insert(0,adc_acc_skt2_wo__hexstr)
	ee_adc_acc_s3_wo.insert(0,adc_acc_skt3_wo__hexstr)
	ee_adc_acc_s4_wo.insert(0,adc_acc_skt4_wo__hexstr)
	ee_adc_acc_s5_wo.insert(0,adc_acc_skt5_wo__hexstr)
	ee_adc_acc_s6_wo.insert(0,adc_acc_skt6_wo__hexstr)
	ee_adc_acc_s7_wo.insert(0,adc_acc_skt7_wo__hexstr)
	ee_adc_acc_s8_wo.insert(0,adc_acc_skt8_wo__hexstr)
	#	
	pass

def adc_bb_trig_single():
	print('\n>>>>>> adc_bb_trig_single')
	
	## clear trig out
	dev.UpdateTriggerOuts()

	## trig update 
	# ep47[1] // w_ADC_TRIG_TI // update
	dev.ActivateTriggerIn(0x47, 1)
	
	# ep67[0] // w_ADC_TRIG_TO // update done check
	cnt_loop = 0;
	while 1:
		# First, query all XEM Trigger Outs.
		dev.UpdateTriggerOuts()
		# check trigger out
		if dev.IsTriggered(0x67, 0x01) == True: # // (ep, mask)
			break
		cnt_loop += 1;
		print('{} = {}'.format('cnt_loop', cnt_loop))
		if (cnt_loop>MAX_count):
			break
	print('{} = {}'.format('cnt_loop', cnt_loop))
	#
	
	## update adc wire out
	adc_bb_update_last_values_wo()
	pass

def adc_bb_update_par_wi():
	print('\n>>>>>> adc_bb_update_par_wi')
	## #
	## # read entry
	## adc_con_wi__hexstr = ee_adc_con_wi.get() # string
	## print('{} = {}'.format('adc_con_wi__hexstr', adc_con_wi__hexstr))
	## #
	## # convert hexstr into int 
	## try:
	## 	adc_con_wi = int(adc_con_wi__hexstr,16)
	## 	print('{} = 0x{:08X}'.format('adc_con_wi', adc_con_wi))
	## except:
	## 	print('failed when converting hexstr into int')
	## 	return
	## #
	## # set wire in 
	## dev.SetWireInValue(0x07,adc_con_wi,0xFFFFFFFF) # (ep,val,mask)
	## dev.UpdateWireIns()
	## #
	pass

def adc_bb_update_period_wi():
	print('\n>>>>>> adc_bb_update_period_wi')
	#
	# read entry
	adc_period_wi__decstr = ee_adc_period_wi.get() # string
	print('{} = {}'.format('adc_period_wi__decstr', adc_period_wi__decstr))
	#
	# convert decstr into int 
	try:
		adc_period_wi = int(adc_period_wi__decstr)
		print('{} = {}'.format('adc_period_wi', adc_period_wi))
	except:
		print('failed when converting decstr into int')
		return
	#
	# set wire in // w_count_period_div4 = w_ADC_PAR_WI[15:0 ] // ep10wire
	dev.SetWireInValue(0x10, int(adc_period_wi/4), 0x0000FFFF) # (ep,val,mask)
	dev.UpdateWireIns()
	#
	pass

def adc_bb_update_num_samples_wi():
	print('\n>>>>>> adc_bb_update_num_samples_wi')
	#
	# read entry
	adc_num_samples_wi__decstr = ee_adc_num_samples_wi.get() # string
	print('{} = {}'.format('adc_num_samples_wi__decstr', adc_num_samples_wi__decstr))
	#
	# convert decstr into int 
	try:
		adc_num_samples_wi = int(adc_num_samples_wi__decstr)
		print('{} = {}'.format('adc_num_samples_wi', adc_num_samples_wi))
	except:
		print('failed when converting decstr into int')
		return
	#
	# set wire in // w_count_conv_div4   = w_ADC_PAR_WI[31:16] // ep10wire
	dev.SetWireInValue(0x10, (int(adc_num_samples_wi/4) << 16), 0xFFFF0000) # (ep,val,mask)
	dev.UpdateWireIns()
	#
	pass

def adc_bb_trig_run():
	print('\n>>>>>> adc_bb_trig_run')
	
	## set parameters:
	adc_bb_update_period_wi()
	adc_bb_update_num_samples_wi()
	
	## clear trig out
	dev.UpdateTriggerOuts()
	
	## trig update 
	# ep47[2] // w_ADC_TRIG_TI // update
	dev.ActivateTriggerIn(0x47, 2)
	
	# ep67[0] // w_ADC_TRIG_TO // update done check
	cnt_loop = 0;
	while 1:
		# First, query all XEM Trigger Outs.
		dev.UpdateTriggerOuts()
		# check trigger out
		if dev.IsTriggered(0x67, 0x01) == True: # // (ep, mask)
			break
		cnt_loop += 1;
		print('{} = {}'.format('cnt_loop', cnt_loop))
		if (cnt_loop>MAX_count):
			break
	print('{} = {}'.format('cnt_loop', cnt_loop))
	#
	
	## update adc wire out
	adc_bb_update_last_values_wo()
	pass

def adc_bb_conv_code_to_volt():
	print('\n>>>>>> adc_bb_conv_code_to_volt')
	
	# https://www.analog.com/media/en/technical-documentation/data-sheets/232516fa.pdf
	# Figure 11. Fully-Differential Transfer Function
	# V_ref    = 4.096V
	# max_code = 32767
	# code_measure // 16 bit, 2's complement 
	# V_measure = code_measure / max_code * V_ref
	# if V_measure>4.096V, V_measure = V_measure - 4.096V // for negative numbers
	
	# ee_adc_volt_s1_ch2 .set  ee_adc_volt_s1_ch1 set
	
	## read wire_out 
	dev.UpdateWireOuts()
	#
	read_ADC_S1_codes = dev.GetWireOutValue(0x30) # // ep30wire = w_ADC_S1_WO
	read_ADC_S2_codes = dev.GetWireOutValue(0x31) # // ep31wire = w_ADC_S2_WO
	read_ADC_S3_codes = dev.GetWireOutValue(0x32) # // ep32wire = w_ADC_S3_WO
	read_ADC_S4_codes = dev.GetWireOutValue(0x33) # // ep33wire = w_ADC_S4_WO
	read_ADC_S5_codes = dev.GetWireOutValue(0x34) # // ep34wire = w_ADC_S5_WO
	read_ADC_S6_codes = dev.GetWireOutValue(0x35) # // ep35wire = w_ADC_S6_WO
	read_ADC_S7_codes = dev.GetWireOutValue(0x36) # // ep36wire = w_ADC_S7_WO
	read_ADC_S8_codes = dev.GetWireOutValue(0x37) # // ep37wire = w_ADC_S8_WO
	#
	read_ADC_ACC_S1_codes = dev.GetWireOutValue(0x28) # // ep28wire = w_ADC_S1_ACC_WO
	read_ADC_ACC_S2_codes = dev.GetWireOutValue(0x29) # // ep29wire = w_ADC_S2_ACC_WO
	read_ADC_ACC_S3_codes = dev.GetWireOutValue(0x2A) # // ep2Awire = w_ADC_S3_ACC_WO
	read_ADC_ACC_S4_codes = dev.GetWireOutValue(0x2B) # // ep2Bwire = w_ADC_S4_ACC_WO
	read_ADC_ACC_S5_codes = dev.GetWireOutValue(0x2C) # // ep2Cwire = w_ADC_S5_ACC_WO
	read_ADC_ACC_S6_codes = dev.GetWireOutValue(0x2D) # // ep2Dwire = w_ADC_S6_ACC_WO
	read_ADC_ACC_S7_codes = dev.GetWireOutValue(0x2E) # // ep2Ewire = w_ADC_S7_ACC_WO
	read_ADC_ACC_S8_codes = dev.GetWireOutValue(0x2F) # // ep2Fwire = w_ADC_S8_ACC_WO
	
	## separate codes
	var_ADC_S1_CH1_code = (read_ADC_S1_codes >>  0) & 0x0000FFFF
	var_ADC_S2_CH1_code = (read_ADC_S2_codes >>  0) & 0x0000FFFF
	var_ADC_S3_CH1_code = (read_ADC_S3_codes >>  0) & 0x0000FFFF
	var_ADC_S4_CH1_code = (read_ADC_S4_codes >>  0) & 0x0000FFFF
	var_ADC_S5_CH1_code = (read_ADC_S5_codes >>  0) & 0x0000FFFF
	var_ADC_S6_CH1_code = (read_ADC_S6_codes >>  0) & 0x0000FFFF
	var_ADC_S7_CH1_code = (read_ADC_S7_codes >>  0) & 0x0000FFFF
	var_ADC_S8_CH1_code = (read_ADC_S8_codes >>  0) & 0x0000FFFF
	#
	var_ADC_S1_CH2_code = (read_ADC_S1_codes >> 16) & 0x0000FFFF
	var_ADC_S2_CH2_code = (read_ADC_S2_codes >> 16) & 0x0000FFFF
	var_ADC_S3_CH2_code = (read_ADC_S3_codes >> 16) & 0x0000FFFF
	var_ADC_S4_CH2_code = (read_ADC_S4_codes >> 16) & 0x0000FFFF
	var_ADC_S5_CH2_code = (read_ADC_S5_codes >> 16) & 0x0000FFFF
	var_ADC_S6_CH2_code = (read_ADC_S6_codes >> 16) & 0x0000FFFF
	var_ADC_S7_CH2_code = (read_ADC_S7_codes >> 16) & 0x0000FFFF
	var_ADC_S8_CH2_code = (read_ADC_S8_codes >> 16) & 0x0000FFFF
	#
	var_ADC_ACC_S1_CH1_code = (read_ADC_ACC_S1_codes >>  0) & 0x0000FFFF
	var_ADC_ACC_S2_CH1_code = (read_ADC_ACC_S2_codes >>  0) & 0x0000FFFF
	var_ADC_ACC_S3_CH1_code = (read_ADC_ACC_S3_codes >>  0) & 0x0000FFFF
	var_ADC_ACC_S4_CH1_code = (read_ADC_ACC_S4_codes >>  0) & 0x0000FFFF
	var_ADC_ACC_S5_CH1_code = (read_ADC_ACC_S5_codes >>  0) & 0x0000FFFF
	var_ADC_ACC_S6_CH1_code = (read_ADC_ACC_S6_codes >>  0) & 0x0000FFFF
	var_ADC_ACC_S7_CH1_code = (read_ADC_ACC_S7_codes >>  0) & 0x0000FFFF
	var_ADC_ACC_S8_CH1_code = (read_ADC_ACC_S8_codes >>  0) & 0x0000FFFF
	#
	var_ADC_ACC_S1_CH2_code = (read_ADC_ACC_S1_codes >> 16) & 0x0000FFFF
	var_ADC_ACC_S2_CH2_code = (read_ADC_ACC_S2_codes >> 16) & 0x0000FFFF
	var_ADC_ACC_S3_CH2_code = (read_ADC_ACC_S3_codes >> 16) & 0x0000FFFF
	var_ADC_ACC_S4_CH2_code = (read_ADC_ACC_S4_codes >> 16) & 0x0000FFFF
	var_ADC_ACC_S5_CH2_code = (read_ADC_ACC_S5_codes >> 16) & 0x0000FFFF
	var_ADC_ACC_S6_CH2_code = (read_ADC_ACC_S6_codes >> 16) & 0x0000FFFF
	var_ADC_ACC_S7_CH2_code = (read_ADC_ACC_S7_codes >> 16) & 0x0000FFFF
	var_ADC_ACC_S8_CH2_code = (read_ADC_ACC_S8_codes >> 16) & 0x0000FFFF

	print('\n>>>>>>')
	#
	print('{} = {}'.format('var_ADC_S1_CH1_code', var_ADC_S1_CH1_code))
	print('{} = {}'.format('var_ADC_S2_CH1_code', var_ADC_S2_CH1_code))
	print('{} = {}'.format('var_ADC_S3_CH1_code', var_ADC_S3_CH1_code))
	print('{} = {}'.format('var_ADC_S4_CH1_code', var_ADC_S4_CH1_code))
	print('{} = {}'.format('var_ADC_S5_CH1_code', var_ADC_S5_CH1_code))
	print('{} = {}'.format('var_ADC_S6_CH1_code', var_ADC_S6_CH1_code))
	print('{} = {}'.format('var_ADC_S7_CH1_code', var_ADC_S7_CH1_code))
	print('{} = {}'.format('var_ADC_S8_CH1_code', var_ADC_S8_CH1_code))
	#
	print('{} = {}'.format('var_ADC_S1_CH2_code', var_ADC_S1_CH2_code))
	print('{} = {}'.format('var_ADC_S2_CH2_code', var_ADC_S2_CH2_code))
	print('{} = {}'.format('var_ADC_S3_CH2_code', var_ADC_S3_CH2_code))
	print('{} = {}'.format('var_ADC_S4_CH2_code', var_ADC_S4_CH2_code))
	print('{} = {}'.format('var_ADC_S5_CH2_code', var_ADC_S5_CH2_code))
	print('{} = {}'.format('var_ADC_S6_CH2_code', var_ADC_S6_CH2_code))
	print('{} = {}'.format('var_ADC_S7_CH2_code', var_ADC_S7_CH2_code))
	print('{} = {}'.format('var_ADC_S8_CH2_code', var_ADC_S8_CH2_code))
	#
	print('{} = {}'.format('var_ADC_ACC_S1_CH1_code', var_ADC_ACC_S1_CH1_code))
	print('{} = {}'.format('var_ADC_ACC_S2_CH1_code', var_ADC_ACC_S2_CH1_code))
	print('{} = {}'.format('var_ADC_ACC_S3_CH1_code', var_ADC_ACC_S3_CH1_code))
	print('{} = {}'.format('var_ADC_ACC_S4_CH1_code', var_ADC_ACC_S4_CH1_code))
	print('{} = {}'.format('var_ADC_ACC_S5_CH1_code', var_ADC_ACC_S5_CH1_code))
	print('{} = {}'.format('var_ADC_ACC_S6_CH1_code', var_ADC_ACC_S6_CH1_code))
	print('{} = {}'.format('var_ADC_ACC_S7_CH1_code', var_ADC_ACC_S7_CH1_code))
	print('{} = {}'.format('var_ADC_ACC_S8_CH1_code', var_ADC_ACC_S8_CH1_code))
	#
	print('{} = {}'.format('var_ADC_ACC_S1_CH2_code', var_ADC_ACC_S1_CH2_code))
	print('{} = {}'.format('var_ADC_ACC_S2_CH2_code', var_ADC_ACC_S2_CH2_code))
	print('{} = {}'.format('var_ADC_ACC_S3_CH2_code', var_ADC_ACC_S3_CH2_code))
	print('{} = {}'.format('var_ADC_ACC_S4_CH2_code', var_ADC_ACC_S4_CH2_code))
	print('{} = {}'.format('var_ADC_ACC_S5_CH2_code', var_ADC_ACC_S5_CH2_code))
	print('{} = {}'.format('var_ADC_ACC_S6_CH2_code', var_ADC_ACC_S6_CH2_code))
	print('{} = {}'.format('var_ADC_ACC_S7_CH2_code', var_ADC_ACC_S7_CH2_code))
	print('{} = {}'.format('var_ADC_ACC_S8_CH2_code', var_ADC_ACC_S8_CH2_code))
		
	# test
	#tmp1=conv_bit_2s_comp_16bit_to_dec(bit_2s_comp=0x7FFF, full_scale=4.096*2)
	#tmp2=conv_bit_2s_comp_16bit_to_dec(bit_2s_comp=0x0100, full_scale=4.096*2)
	#tmp3=conv_bit_2s_comp_16bit_to_dec(bit_2s_comp=0xFF00, full_scale=4.096*2)
	#tmp4=conv_bit_2s_comp_16bit_to_dec(bit_2s_comp=0x8000, full_scale=4.096*2)
	#print('\n>>>>>>')
	#print('{} = {:.8}'.format('tmp1', tmp1))
	#print('{} = {:.8}'.format('tmp2', tmp2))
	#print('{} = {:.8}'.format('tmp3', tmp3))
	#print('{} = {:.8}'.format('tmp4', tmp4))
	
	# convert code to value 
	V_ref =4.096
	#
	var_ADC_S1_CH1_volt = conv_bit_2s_comp_16bit_to_dec(bit_2s_comp=var_ADC_S1_CH1_code, full_scale=V_ref*2)
	var_ADC_S2_CH1_volt = conv_bit_2s_comp_16bit_to_dec(bit_2s_comp=var_ADC_S2_CH1_code, full_scale=V_ref*2)
	var_ADC_S3_CH1_volt = conv_bit_2s_comp_16bit_to_dec(bit_2s_comp=var_ADC_S3_CH1_code, full_scale=V_ref*2)
	var_ADC_S4_CH1_volt = conv_bit_2s_comp_16bit_to_dec(bit_2s_comp=var_ADC_S4_CH1_code, full_scale=V_ref*2)
	var_ADC_S5_CH1_volt = conv_bit_2s_comp_16bit_to_dec(bit_2s_comp=var_ADC_S5_CH1_code, full_scale=V_ref*2)
	var_ADC_S6_CH1_volt = conv_bit_2s_comp_16bit_to_dec(bit_2s_comp=var_ADC_S6_CH1_code, full_scale=V_ref*2)
	var_ADC_S7_CH1_volt = conv_bit_2s_comp_16bit_to_dec(bit_2s_comp=var_ADC_S7_CH1_code, full_scale=V_ref*2)
	var_ADC_S8_CH1_volt = conv_bit_2s_comp_16bit_to_dec(bit_2s_comp=var_ADC_S8_CH1_code, full_scale=V_ref*2)
	#
	var_ADC_S1_CH2_volt = conv_bit_2s_comp_16bit_to_dec(bit_2s_comp=var_ADC_S1_CH2_code, full_scale=V_ref*2)
	var_ADC_S2_CH2_volt = conv_bit_2s_comp_16bit_to_dec(bit_2s_comp=var_ADC_S2_CH2_code, full_scale=V_ref*2)
	var_ADC_S3_CH2_volt = conv_bit_2s_comp_16bit_to_dec(bit_2s_comp=var_ADC_S3_CH2_code, full_scale=V_ref*2)
	var_ADC_S4_CH2_volt = conv_bit_2s_comp_16bit_to_dec(bit_2s_comp=var_ADC_S4_CH2_code, full_scale=V_ref*2)
	var_ADC_S5_CH2_volt = conv_bit_2s_comp_16bit_to_dec(bit_2s_comp=var_ADC_S5_CH2_code, full_scale=V_ref*2)
	var_ADC_S6_CH2_volt = conv_bit_2s_comp_16bit_to_dec(bit_2s_comp=var_ADC_S6_CH2_code, full_scale=V_ref*2)
	var_ADC_S7_CH2_volt = conv_bit_2s_comp_16bit_to_dec(bit_2s_comp=var_ADC_S7_CH2_code, full_scale=V_ref*2)
	var_ADC_S8_CH2_volt = conv_bit_2s_comp_16bit_to_dec(bit_2s_comp=var_ADC_S8_CH2_code, full_scale=V_ref*2)
	#
	var_ADC_ACC_S1_CH1_volt = conv_bit_2s_comp_16bit_to_dec(bit_2s_comp=var_ADC_ACC_S1_CH1_code, full_scale=V_ref*2)
	var_ADC_ACC_S2_CH1_volt = conv_bit_2s_comp_16bit_to_dec(bit_2s_comp=var_ADC_ACC_S2_CH1_code, full_scale=V_ref*2)
	var_ADC_ACC_S3_CH1_volt = conv_bit_2s_comp_16bit_to_dec(bit_2s_comp=var_ADC_ACC_S3_CH1_code, full_scale=V_ref*2)
	var_ADC_ACC_S4_CH1_volt = conv_bit_2s_comp_16bit_to_dec(bit_2s_comp=var_ADC_ACC_S4_CH1_code, full_scale=V_ref*2)
	var_ADC_ACC_S5_CH1_volt = conv_bit_2s_comp_16bit_to_dec(bit_2s_comp=var_ADC_ACC_S5_CH1_code, full_scale=V_ref*2)
	var_ADC_ACC_S6_CH1_volt = conv_bit_2s_comp_16bit_to_dec(bit_2s_comp=var_ADC_ACC_S6_CH1_code, full_scale=V_ref*2)
	var_ADC_ACC_S7_CH1_volt = conv_bit_2s_comp_16bit_to_dec(bit_2s_comp=var_ADC_ACC_S7_CH1_code, full_scale=V_ref*2)
	var_ADC_ACC_S8_CH1_volt = conv_bit_2s_comp_16bit_to_dec(bit_2s_comp=var_ADC_ACC_S8_CH1_code, full_scale=V_ref*2)
	#
	var_ADC_ACC_S1_CH2_volt = conv_bit_2s_comp_16bit_to_dec(bit_2s_comp=var_ADC_ACC_S1_CH2_code, full_scale=V_ref*2)
	var_ADC_ACC_S2_CH2_volt = conv_bit_2s_comp_16bit_to_dec(bit_2s_comp=var_ADC_ACC_S2_CH2_code, full_scale=V_ref*2)
	var_ADC_ACC_S3_CH2_volt = conv_bit_2s_comp_16bit_to_dec(bit_2s_comp=var_ADC_ACC_S3_CH2_code, full_scale=V_ref*2)
	var_ADC_ACC_S4_CH2_volt = conv_bit_2s_comp_16bit_to_dec(bit_2s_comp=var_ADC_ACC_S4_CH2_code, full_scale=V_ref*2)
	var_ADC_ACC_S5_CH2_volt = conv_bit_2s_comp_16bit_to_dec(bit_2s_comp=var_ADC_ACC_S5_CH2_code, full_scale=V_ref*2)
	var_ADC_ACC_S6_CH2_volt = conv_bit_2s_comp_16bit_to_dec(bit_2s_comp=var_ADC_ACC_S6_CH2_code, full_scale=V_ref*2)
	var_ADC_ACC_S7_CH2_volt = conv_bit_2s_comp_16bit_to_dec(bit_2s_comp=var_ADC_ACC_S7_CH2_code, full_scale=V_ref*2)
	var_ADC_ACC_S8_CH2_volt = conv_bit_2s_comp_16bit_to_dec(bit_2s_comp=var_ADC_ACC_S8_CH2_code, full_scale=V_ref*2)
	
	print('\n>>>>>>')
	#
	print('{} = {:.8}'.format('var_ADC_S1_CH1_volt', var_ADC_S1_CH1_volt))
	print('{} = {:.8}'.format('var_ADC_S2_CH1_volt', var_ADC_S2_CH1_volt))
	print('{} = {:.8}'.format('var_ADC_S3_CH1_volt', var_ADC_S3_CH1_volt))
	print('{} = {:.8}'.format('var_ADC_S4_CH1_volt', var_ADC_S4_CH1_volt))
	print('{} = {:.8}'.format('var_ADC_S5_CH1_volt', var_ADC_S5_CH1_volt))
	print('{} = {:.8}'.format('var_ADC_S6_CH1_volt', var_ADC_S6_CH1_volt))
	print('{} = {:.8}'.format('var_ADC_S7_CH1_volt', var_ADC_S7_CH1_volt))
	print('{} = {:.8}'.format('var_ADC_S8_CH1_volt', var_ADC_S8_CH1_volt))
	#
	print('{} = {:.8}'.format('var_ADC_S1_CH2_volt', var_ADC_S1_CH2_volt))
	print('{} = {:.8}'.format('var_ADC_S2_CH2_volt', var_ADC_S2_CH2_volt))
	print('{} = {:.8}'.format('var_ADC_S3_CH2_volt', var_ADC_S3_CH2_volt))
	print('{} = {:.8}'.format('var_ADC_S4_CH2_volt', var_ADC_S4_CH2_volt))
	print('{} = {:.8}'.format('var_ADC_S5_CH2_volt', var_ADC_S5_CH2_volt))
	print('{} = {:.8}'.format('var_ADC_S6_CH2_volt', var_ADC_S6_CH2_volt))
	print('{} = {:.8}'.format('var_ADC_S7_CH2_volt', var_ADC_S7_CH2_volt))
	print('{} = {:.8}'.format('var_ADC_S8_CH2_volt', var_ADC_S8_CH2_volt))
	#
	print('{} = {:.8}'.format('var_ADC_ACC_S1_CH1_volt', var_ADC_ACC_S1_CH1_volt))
	print('{} = {:.8}'.format('var_ADC_ACC_S2_CH1_volt', var_ADC_ACC_S2_CH1_volt))
	print('{} = {:.8}'.format('var_ADC_ACC_S3_CH1_volt', var_ADC_ACC_S3_CH1_volt))
	print('{} = {:.8}'.format('var_ADC_ACC_S4_CH1_volt', var_ADC_ACC_S4_CH1_volt))
	print('{} = {:.8}'.format('var_ADC_ACC_S5_CH1_volt', var_ADC_ACC_S5_CH1_volt))
	print('{} = {:.8}'.format('var_ADC_ACC_S6_CH1_volt', var_ADC_ACC_S6_CH1_volt))
	print('{} = {:.8}'.format('var_ADC_ACC_S7_CH1_volt', var_ADC_ACC_S7_CH1_volt))
	print('{} = {:.8}'.format('var_ADC_ACC_S8_CH1_volt', var_ADC_ACC_S8_CH1_volt))
	#
	print('{} = {:.8}'.format('var_ADC_ACC_S1_CH2_volt', var_ADC_ACC_S1_CH2_volt))
	print('{} = {:.8}'.format('var_ADC_ACC_S2_CH2_volt', var_ADC_ACC_S2_CH2_volt))
	print('{} = {:.8}'.format('var_ADC_ACC_S3_CH2_volt', var_ADC_ACC_S3_CH2_volt))
	print('{} = {:.8}'.format('var_ADC_ACC_S4_CH2_volt', var_ADC_ACC_S4_CH2_volt))
	print('{} = {:.8}'.format('var_ADC_ACC_S5_CH2_volt', var_ADC_ACC_S5_CH2_volt))
	print('{} = {:.8}'.format('var_ADC_ACC_S6_CH2_volt', var_ADC_ACC_S6_CH2_volt))
	print('{} = {:.8}'.format('var_ADC_ACC_S7_CH2_volt', var_ADC_ACC_S7_CH2_volt))
	print('{} = {:.8}'.format('var_ADC_ACC_S8_CH2_volt', var_ADC_ACC_S8_CH2_volt))
	
	# convert string 
	var_ADC_S1_CH1_volt_str = '{:.8}'.format(var_ADC_S1_CH1_volt)
	var_ADC_S2_CH1_volt_str = '{:.8}'.format(var_ADC_S2_CH1_volt)
	var_ADC_S3_CH1_volt_str = '{:.8}'.format(var_ADC_S3_CH1_volt)
	var_ADC_S4_CH1_volt_str = '{:.8}'.format(var_ADC_S4_CH1_volt)
	var_ADC_S5_CH1_volt_str = '{:.8}'.format(var_ADC_S5_CH1_volt)
	var_ADC_S6_CH1_volt_str = '{:.8}'.format(var_ADC_S6_CH1_volt)
	var_ADC_S7_CH1_volt_str = '{:.8}'.format(var_ADC_S7_CH1_volt)
	var_ADC_S8_CH1_volt_str = '{:.8}'.format(var_ADC_S8_CH1_volt)
	#
	var_ADC_S1_CH2_volt_str = '{:.8}'.format(var_ADC_S1_CH2_volt)
	var_ADC_S2_CH2_volt_str = '{:.8}'.format(var_ADC_S2_CH2_volt)
	var_ADC_S3_CH2_volt_str = '{:.8}'.format(var_ADC_S3_CH2_volt)
	var_ADC_S4_CH2_volt_str = '{:.8}'.format(var_ADC_S4_CH2_volt)
	var_ADC_S5_CH2_volt_str = '{:.8}'.format(var_ADC_S5_CH2_volt)
	var_ADC_S6_CH2_volt_str = '{:.8}'.format(var_ADC_S6_CH2_volt)
	var_ADC_S7_CH2_volt_str = '{:.8}'.format(var_ADC_S7_CH2_volt)
	var_ADC_S8_CH2_volt_str = '{:.8}'.format(var_ADC_S8_CH2_volt)
	#
	var_ADC_ACC_S1_CH1_volt_str = '{:.8}'.format(var_ADC_ACC_S1_CH1_volt)
	var_ADC_ACC_S2_CH1_volt_str = '{:.8}'.format(var_ADC_ACC_S2_CH1_volt)
	var_ADC_ACC_S3_CH1_volt_str = '{:.8}'.format(var_ADC_ACC_S3_CH1_volt)
	var_ADC_ACC_S4_CH1_volt_str = '{:.8}'.format(var_ADC_ACC_S4_CH1_volt)
	var_ADC_ACC_S5_CH1_volt_str = '{:.8}'.format(var_ADC_ACC_S5_CH1_volt)
	var_ADC_ACC_S6_CH1_volt_str = '{:.8}'.format(var_ADC_ACC_S6_CH1_volt)
	var_ADC_ACC_S7_CH1_volt_str = '{:.8}'.format(var_ADC_ACC_S7_CH1_volt)
	var_ADC_ACC_S8_CH1_volt_str = '{:.8}'.format(var_ADC_ACC_S8_CH1_volt)
	#
	var_ADC_ACC_S1_CH2_volt_str = '{:.8}'.format(var_ADC_ACC_S1_CH2_volt)
	var_ADC_ACC_S2_CH2_volt_str = '{:.8}'.format(var_ADC_ACC_S2_CH2_volt)
	var_ADC_ACC_S3_CH2_volt_str = '{:.8}'.format(var_ADC_ACC_S3_CH2_volt)
	var_ADC_ACC_S4_CH2_volt_str = '{:.8}'.format(var_ADC_ACC_S4_CH2_volt)
	var_ADC_ACC_S5_CH2_volt_str = '{:.8}'.format(var_ADC_ACC_S5_CH2_volt)
	var_ADC_ACC_S6_CH2_volt_str = '{:.8}'.format(var_ADC_ACC_S6_CH2_volt)
	var_ADC_ACC_S7_CH2_volt_str = '{:.8}'.format(var_ADC_ACC_S7_CH2_volt)
	var_ADC_ACC_S8_CH2_volt_str = '{:.8}'.format(var_ADC_ACC_S8_CH2_volt)
	
	# update box 
	ee_adc_volt_s1_ch1.delete(0,END)
	ee_adc_volt_s2_ch1.delete(0,END)
	ee_adc_volt_s3_ch1.delete(0,END)
	ee_adc_volt_s4_ch1.delete(0,END)
	ee_adc_volt_s5_ch1.delete(0,END)
	ee_adc_volt_s6_ch1.delete(0,END)
	ee_adc_volt_s7_ch1.delete(0,END)
	ee_adc_volt_s8_ch1.delete(0,END)
	#
	ee_adc_volt_s1_ch2.delete(0,END)
	ee_adc_volt_s2_ch2.delete(0,END)
	ee_adc_volt_s3_ch2.delete(0,END)
	ee_adc_volt_s4_ch2.delete(0,END)
	ee_adc_volt_s5_ch2.delete(0,END)
	ee_adc_volt_s6_ch2.delete(0,END)
	ee_adc_volt_s7_ch2.delete(0,END)
	ee_adc_volt_s8_ch2.delete(0,END)
	#
	ee_adc_volt_s1_ch1.insert(0,var_ADC_S1_CH1_volt_str)
	ee_adc_volt_s2_ch1.insert(0,var_ADC_S2_CH1_volt_str)
	ee_adc_volt_s3_ch1.insert(0,var_ADC_S3_CH1_volt_str)
	ee_adc_volt_s4_ch1.insert(0,var_ADC_S4_CH1_volt_str)
	ee_adc_volt_s5_ch1.insert(0,var_ADC_S5_CH1_volt_str)
	ee_adc_volt_s6_ch1.insert(0,var_ADC_S6_CH1_volt_str)
	ee_adc_volt_s7_ch1.insert(0,var_ADC_S7_CH1_volt_str)
	ee_adc_volt_s8_ch1.insert(0,var_ADC_S8_CH1_volt_str)
	#
	ee_adc_volt_s1_ch2.insert(0,var_ADC_S1_CH2_volt_str)
	ee_adc_volt_s2_ch2.insert(0,var_ADC_S2_CH2_volt_str)
	ee_adc_volt_s3_ch2.insert(0,var_ADC_S3_CH2_volt_str)
	ee_adc_volt_s4_ch2.insert(0,var_ADC_S4_CH2_volt_str)
	ee_adc_volt_s5_ch2.insert(0,var_ADC_S5_CH2_volt_str)
	ee_adc_volt_s6_ch2.insert(0,var_ADC_S6_CH2_volt_str)
	ee_adc_volt_s7_ch2.insert(0,var_ADC_S7_CH2_volt_str)
	ee_adc_volt_s8_ch2.insert(0,var_ADC_S8_CH2_volt_str)
	#
	ee_adc_acc_volt_s1_ch1.delete(0,END)
	ee_adc_acc_volt_s2_ch1.delete(0,END)
	ee_adc_acc_volt_s3_ch1.delete(0,END)
	ee_adc_acc_volt_s4_ch1.delete(0,END)
	ee_adc_acc_volt_s5_ch1.delete(0,END)
	ee_adc_acc_volt_s6_ch1.delete(0,END)
	ee_adc_acc_volt_s7_ch1.delete(0,END)
	ee_adc_acc_volt_s8_ch1.delete(0,END)
	#
	ee_adc_acc_volt_s1_ch2.delete(0,END)
	ee_adc_acc_volt_s2_ch2.delete(0,END)
	ee_adc_acc_volt_s3_ch2.delete(0,END)
	ee_adc_acc_volt_s4_ch2.delete(0,END)
	ee_adc_acc_volt_s5_ch2.delete(0,END)
	ee_adc_acc_volt_s6_ch2.delete(0,END)
	ee_adc_acc_volt_s7_ch2.delete(0,END)
	ee_adc_acc_volt_s8_ch2.delete(0,END)
	#
	ee_adc_acc_volt_s1_ch1.insert(0,var_ADC_ACC_S1_CH1_volt_str)
	ee_adc_acc_volt_s2_ch1.insert(0,var_ADC_ACC_S2_CH1_volt_str)
	ee_adc_acc_volt_s3_ch1.insert(0,var_ADC_ACC_S3_CH1_volt_str)
	ee_adc_acc_volt_s4_ch1.insert(0,var_ADC_ACC_S4_CH1_volt_str)
	ee_adc_acc_volt_s5_ch1.insert(0,var_ADC_ACC_S5_CH1_volt_str)
	ee_adc_acc_volt_s6_ch1.insert(0,var_ADC_ACC_S6_CH1_volt_str)
	ee_adc_acc_volt_s7_ch1.insert(0,var_ADC_ACC_S7_CH1_volt_str)
	ee_adc_acc_volt_s8_ch1.insert(0,var_ADC_ACC_S8_CH1_volt_str)
	#
	ee_adc_acc_volt_s1_ch2.insert(0,var_ADC_ACC_S1_CH2_volt_str)
	ee_adc_acc_volt_s2_ch2.insert(0,var_ADC_ACC_S2_CH2_volt_str)
	ee_adc_acc_volt_s3_ch2.insert(0,var_ADC_ACC_S3_CH2_volt_str)
	ee_adc_acc_volt_s4_ch2.insert(0,var_ADC_ACC_S4_CH2_volt_str)
	ee_adc_acc_volt_s5_ch2.insert(0,var_ADC_ACC_S5_CH2_volt_str)
	ee_adc_acc_volt_s6_ch2.insert(0,var_ADC_ACC_S6_CH2_volt_str)
	ee_adc_acc_volt_s7_ch2.insert(0,var_ADC_ACC_S7_CH2_volt_str)
	ee_adc_acc_volt_s8_ch2.insert(0,var_ADC_ACC_S8_CH2_volt_str)

	pass

def adc_bb_load_fifo(skt=0, chn=0):
	print('\n>>>>>> adc_bb_load_fifo')
	#
	print('> {} = {}'.format('skt',skt))
	print('> {} = {}'.format('chn',chn))
	
	## pipeout ep address
	# skt1 ch1 epA0
	# skt2 ch1 epA1
	# skt3 ch1 epA2
	# skt4 ch1 epA3
	# skt5 ch1 epA4
	# skt6 ch1 epA5
	# skt7 ch1 epA6
	# skt8 ch1 epA7
	# skt1 ch2 epA8
	# skt2 ch2 epA9
	# skt3 ch2 epAA
	# skt4 ch2 epAB
	# skt5 ch2 epAC
	# skt6 ch2 epAD
	# skt7 ch2 epAE
	# skt8 ch2 epAF
	#
	# set po 
	if   skt == 1 and chn == 1: 
		po = 0xA0
	elif skt == 2 and chn == 1: 
		po = 0xA1
	elif skt == 3 and chn == 1: 
		po = 0xA2
	elif skt == 4 and chn == 1: 
		po = 0xA3
	elif skt == 5 and chn == 1: 
		po = 0xA4
	elif skt == 6 and chn == 1: 
		po = 0xA5
	elif skt == 7 and chn == 1: 
		po = 0xA6
	elif skt == 8 and chn == 1: 
		po = 0xA7
	elif skt == 1 and chn == 2: 
		po = 0xA8
	elif skt == 2 and chn == 2: 
		po = 0xA9
	elif skt == 3 and chn == 2: 
		po = 0xAA
	elif skt == 4 and chn == 2: 
		po = 0xAB
	elif skt == 5 and chn == 2: 
		po = 0xAC
	elif skt == 6 and chn == 2: 
		po = 0xAD
	elif skt == 7 and chn == 2: 
		po = 0xAE
	elif skt == 8 and chn == 2: 
		po = 0xAF
	else: 
		po = 0xFF
		return
	#
	print('> {} = 0x{:02X}'.format('po',po))
	
	## set num of bytes to read 
	# read entry
	adc_num_samples_wi__decstr = ee_adc_num_samples_wi.get() # string
	print('{} = {}'.format('adc_num_samples_wi__decstr', adc_num_samples_wi__decstr))
	#
	# convert decstr into int 
	try:
		adc_num_samples_wi = int(adc_num_samples_wi__decstr)
		print('{} = {}'.format('adc_num_samples_wi', adc_num_samples_wi))
	except:
		print('failed when converting decstr into int')
		return
	#
	bytes_in_one_sample = 4 # for USB 32 bit end-point
	num_bytes_from_fifo = adc_num_samples_wi * bytes_in_one_sample
	print('{} = {}'.format('num_bytes_from_fifo', num_bytes_from_fifo))
	
	## setup data buffer for fifo data
	dataout = bytearray([0] * num_bytes_from_fifo)
	
	## call api function to read pipeout data
	data_count = dev.ReadFromPipeOut(po, dataout)
	print('{} : {}'.format('data_count [byte]',data_count))
	if data_count<0:
		#return
		# set test data 
		adc_num_samples_wi = 40
		data_count = adc_num_samples_wi * bytes_in_one_sample
		data_int_list = [1,2,3, 2, 1, -1 ]
		data_bytes_list = [x.to_bytes(bytes_in_one_sample,byteorder='little',signed=True) for x in data_int_list]
		print('{} = {}'.format('data_bytes_list', data_bytes_list))
		#dataout = b'\x01\x00\x00\x00\x02\x00\x00\x00'
		dataout = b''.join(data_bytes_list)
	
	## convert bytearray to 32-bit data 
	data_fifo_int = []
	for ii in range(0,adc_num_samples_wi):
		temp_data = int.from_bytes(dataout[ii*bytes_in_one_sample:(ii+1)*bytes_in_one_sample], byteorder='little', signed=True)
		data_fifo_int += [temp_data]
	
	## print out 
	print('{} = {}'.format('data_fifo_int', data_fifo_int))
	
	## display
	import matplotlib.pyplot as plt
	
	plt.ion() # matplotlib interactive mode 
	#
	FIG_NUM = None # for new figure windows
	#FIG_NUM = 1 # for only one figure window
	fig = plt.figure(FIG_NUM,figsize=(6,6)) # (12,9)
	
	ax1 = plt.subplot(211) ### 
	x_list = list(range(len(data_fifo_int)))
	y_list = data_fifo_int
	plt.plot(x_list, y_list, 'r-')
	plt.title('skt{}-chn{}: ADC codes(red)'.format(skt,chn))
	plt.ylabel('Codes')
	plt.xlabel('Samples')
	plt.grid(True)
	
	ax2 = plt.subplot(212) ### 
	
	# read entry for sampling period 
	try:
		adc_period_wi = int(ee_adc_period_wi.get())
		print('{} = {}'.format('adc_period_wi', adc_period_wi))
	except:
		print('failed when converting into int')
		return
	base_freq_adc = 192e6 # ADC base frequency 192MHz
	FS = base_freq_adc/adc_period_wi
	print('{} = {}'.format('FS', FS))
	NFFT=2**14 # default 2**13
	_,_,line_psd0 = plt.psd(y_list, NFFT, FS, noverlap=0, color='r', label='PSD_ADC', visible=True, alpha=0.7, return_line=True)
	
	# compact display
	ax1.autoscale(enable=True, axis='x', tight=True)
	#ax2.autoscale(enable=True, axis='x', tight=True)
	fig.tight_layout()
	#plt.show()	
	
	pass
	

## TODO: MHVSU EXT-TRIG functions 
# wire [31:0] w_EXT_TRIG_CON_WI  = ep14wire; // sspi adrs 0x050
# wire [31:0] w_EXT_TRIG_PARA_WI = ep15wire; // sspi adrs 0x054
# wire [31:0] w_EXT_TRIG_AUX_WI  = ep16wire; // sspi adrs 0x058
# wire [31:0] w_EXT_TRIG_TI = ep44trig; assign ep44ck = sys_clk; // {..., sw_aux_trig, sw_m_pre_trig, sw_m_trig, reset_trig}
# wire [31:0] w_EXT_TRIG_TO ; assign ep64trig = w_EXT_TRIG_TO; assign ep64ck = sys_clk;

def update_wi_from_ee(ee, wi_adrs,wi_mask=0xFFFFFFFF):
	print('>>> update_wi_from_ee')
	#
	# read entry
	wi_data__hexstr = ee.get() # string
	print('{} = {}'.format('wi_data__hexstr', wi_data__hexstr))
	#
	# convert hexstr into int 
	try:
		wi_data = int(wi_data__hexstr,16)
		print('{} = 0x{:02X}'.format('wi_adrs', wi_adrs))
		print('{} = 0x{:08X}'.format('wi_data', wi_data))
		print('{} = 0x{:08X}'.format('wi_mask', wi_mask))
	except:
		print('failed when converting hexstr into int')
		return
	#
	# set wire in 
	dev.SetWireInValue(wi_adrs,wi_data,wi_mask) # (ep,val,mask)
	dev.UpdateWireIns()
	#
	pass

def trig_bb_EXT_TRIG_CON_WI_():
	print('\n>>>>>> trig_bb_EXT_TRIG_CON_WI_')
	update_wi_from_ee(ee=ee_EXT_TRIG_CON_WI_, wi_adrs=0x14,wi_mask=0xFFFFFFFF)
	pass
def trig_bb_EXT_TRIG_PARA_WI():
	print('\n>>>>>> trig_bb_EXT_TRIG_PARA_WI')
	update_wi_from_ee(ee=ee_EXT_TRIG_PARA_WI, wi_adrs=0x15,wi_mask=0xFFFFFFFF)
	pass
def trig_bb_EXT_TRIG_AUX_WI_():
	print('\n>>>>>> trig_bb_EXT_TRIG_AUX_WI_')
	update_wi_from_ee(ee=ee_EXT_TRIG_AUX_WI_, wi_adrs=0x16,wi_mask=0xFFFFFFFF)
	pass


def trig_ti (ti_adrs, bit_loc):
	print('>>> trig_ti')
	print('{} = 0x{:02X}'.format('ti_adrs', ti_adrs))
	print('{} = {}'.format('bit_loc', bit_loc))

	## trig update 
	dev.ActivateTriggerIn(ti_adrs, bit_loc)
	pass

def trig_bb_reset_trig___():
	print('\n>>>>>> trig_bb_reset_trig___')
	trig_ti(ti_adrs=0x44,bit_loc=0)
	pass
def trig_bb_sw_m_trig____():
	print('\n>>>>>> trig_bb_sw_m_trig____')
	trig_ti(ti_adrs=0x44,bit_loc=1)
	pass
def trig_bb_sw_m_pre_trig():
	print('\n>>>>>> trig_bb_sw_m_pre_trig')
	trig_ti(ti_adrs=0x44,bit_loc=2)
	pass
def trig_bb_sw_aux_trig__():
	print('\n>>>>>> trig_bb_sw_aux_trig__')
	trig_ti(ti_adrs=0x44,bit_loc=3)
	pass


def update_wi(wi_adrs, wi_data, wi_mask=0xFFFFFFFF, bit_shift=0):
	print('>>> update_wi')
	#
	wi_data_shift = wi_data<<bit_shift
	wi_mask_shift = wi_mask<<bit_shift
	#
	print('{} = 0x{:02X}'.format('wi_adrs', wi_adrs))
	print('{} = 0x{:08X}'.format('wi_data_shift', wi_data_shift))
	print('{} = 0x{:08X}'.format('wi_mask_shift', wi_mask_shift))
	#
	# set wire in 
	dev.SetWireInValue(wi_adrs,wi_data_shift,wi_mask_shift) # (ep,val,mask)
	dev.UpdateWireIns()
	#
	pass

def trig_bb_EXT_TRIG_CONFIG___(): 
	print('\n>>>>>> trig_bb_EXT_TRIG_CONFIG___')
	
	# var_EXT_TRIG_BLOCK_EN
	update_wi(wi_adrs=0x14,wi_data=var_EXT_TRIG_BLOCK_EN.get(),wi_mask=1,bit_shift=0)
	# var_M_TRIG_____PIN_EN
	update_wi(wi_adrs=0x14,wi_data=var_M_TRIG_____PIN_EN.get(),wi_mask=1,bit_shift=1)
	# var_M_PRE_TRIG_PIN_EN
	update_wi(wi_adrs=0x14,wi_data=var_M_PRE_TRIG_PIN_EN.get(),wi_mask=1,bit_shift=2)
	# var_AUX_TRIG___PIN_EN
	update_wi(wi_adrs=0x14,wi_data=var_AUX_TRIG___PIN_EN.get(),wi_mask=1,bit_shift=3)
	# var_ALL_TRIGS_PIN_DIS
	update_wi(wi_adrs=0x14,wi_data=var_ALL_TRIGS_PIN_DIS.get(),wi_mask=1,bit_shift=8)
	
	# ee_EXT_TRIG_CONF_M_TRIG    
	update_wi(wi_adrs=0x14,wi_data=int(ee_EXT_TRIG_CONF_M_TRIG.get(),16),wi_mask=0x7,bit_shift=16)
	# ee_EXT_TRIG_CONF_M_PRE_TRIG
	update_wi(wi_adrs=0x14,wi_data=int(ee_EXT_TRIG_CONF_M_PRE_TRIG.get(),16),wi_mask=0x7,bit_shift=20)
	# ee_EXT_TRIG_CONF_AUX_TRIG  
	update_wi(wi_adrs=0x14,wi_data=int(ee_EXT_TRIG_CONF_AUX_TRIG.get(),16),wi_mask=0x7,bit_shift=24)
	
	#
	pass
	
def trig_bb_EXT_TRIG_DELAY_CNT(): 
	print('\n>>>>>> trig_bb_EXT_TRIG_DELAY_CNT')
	
	# ee_EXT_TRIG_ADC__DELAY_CNT.
	update_wi(wi_adrs=0x15,wi_data=int(ee_EXT_TRIG_ADC__DELAY_CNT.get()),wi_mask=0xFFFF,bit_shift=0)
	# ee_EXT_TRIG_DAC__DELAY_CNT.
	update_wi(wi_adrs=0x15,wi_data=int(ee_EXT_TRIG_DAC__DELAY_CNT.get()),wi_mask=0xFFFF,bit_shift=16)
	# ee_EXT_TRIG_SPIO_DELAY_CNT.
	update_wi(wi_adrs=0x16,wi_data=int(ee_EXT_TRIG_SPIO_DELAY_CNT.get()),wi_mask=0xFFFF,bit_shift=0)

	pass


## TODO: EEPROM functions 
#  // w_MEM_WI   
#  assign w_num_bytes_DAT               = w_MEM_WI[12:0]; // 12-bit 
#  assign w_con_disable_SBP             = w_MEM_WI[15]; // 1-bit
#  assign w_con_fifo_path__L_sspi_H_lan = w_MEM_WI[16]; // 1-bit
#  assign w_con_port__L_MEM_SIO__H_TP   = w_MEM_WI[17]; // 1-bit
#  
#  // w_MEM_FDAT_WI
#  assign w_frame_data_CMD              = w_MEM_FDAT_WI[ 7: 0]; // 8-bit
#  assign w_frame_data_STA_in           = w_MEM_FDAT_WI[15: 8]; // 8-bit
#  assign w_frame_data_ADL              = w_MEM_FDAT_WI[23:16]; // 8-bit
#  assign w_frame_data_ADH              = w_MEM_FDAT_WI[31:24]; // 8-bit
#  
#  // w_MEM_TI
#  assign w_MEM_rst      = w_MEM_TI[0];
#  assign w_MEM_fifo_rst = w_MEM_TI[1];
#  assign w_trig_frame   = w_MEM_TI[2];
#  
#  // w_MEM_TO
#  assign w_MEM_TO[0]     = w_MEM_valid    ;
#  assign w_MEM_TO[1]     = w_done_frame   ;
#  assign w_MEM_TO[2]     = w_done_frame_TO; //$$ rev
#  assign w_MEM_TO[15: 8] = w_frame_data_STA_out; 
#  
#  // w_MEM_PI
#  assign w_frame_data_DAT_wr    = w_MEM_PI[7:0]; // 8-bit
#  assign w_frame_data_DAT_wr_en = w_MEM_PI_wr;
#  
#  // w_MEM_PO
#  assign w_MEM_PO[7:0]          = w_frame_data_DAT_rd; // 8-bit
#  assign w_MEM_PO[31:8]         = 24'b0; // unused
#  assign w_frame_data_DAT_rd_en = w_MEM_PO_rd;

def eeprom_send_frame_ep (MEM_WI, MEM_FDAT_WI):
	## //// end-point map :
	## // wire [31:0] w_MEM_WI      = ep13wire;
	## // wire [31:0] w_MEM_FDAT_WI = ep12wire;
	## // wire [31:0] w_MEM_TI = ep53trig; assign ep53ck = sys_clk;
	## // wire [31:0] w_MEM_TO; assign ep73trig = w_MEM_TO; assign ep73ck = sys_clk;
	## // wire [31:0] w_MEM_PI = ep93pipe; wire w_MEM_PI_wr = ep93wr; 
	## // wire [31:0] w_MEM_PO; assign epB3pipe = w_MEM_PO; wire w_MEM_PO_rd = epB3rd; 	

	##$$ print('{} = 0x{:08X}'.format('MEM_WI', MEM_WI))
	dev.SetWireInValue(0x13,MEM_WI,0xFFFFFFFF) # (ep,val,mask)
	#dev.UpdateWireIns()	
	
	##$$ print('{} = 0x{:08X}'.format('MEM_FDAT_WI', MEM_FDAT_WI))
	dev.SetWireInValue(0x12,MEM_FDAT_WI,0xFFFFFFFF) # (ep,val,mask)
	dev.UpdateWireIns()	
	
	# clear TO 
	dev.UpdateTriggerOuts()
	ret=dev.GetTriggerOutVector(0x73)
	##$$ print('{} = 0x{:08X}'.format('ret', ret))


	# act TI 
	dev.ActivateTriggerIn(0x53, 2)	## (ep, loc)
	
	# check frame done
	cnt_loop = 0;
	while 1:
		# First, query all XEM Trigger Outs.
		dev.UpdateTriggerOuts()
		# check trigger out //$$  0x01 w_MEM_TO[0]  or  0x04 w_MEM_TO[2] 
		if dev.IsTriggered(0x73, 0x04) == True: # // (ep, mask)
			break
		cnt_loop += 1;
		##$$ print('{} = {}'.format('cnt_loop', cnt_loop))
		if (cnt_loop>MAX_count):
			break
	##$$ print('{} = {}'.format('cnt_loop', cnt_loop))

	# # read again TO 
	# dev.UpdateTriggerOuts()
	# ret=dev.GetTriggerOutVector(0x73)
	# print('{} = 0x{:08X}'.format('ret', ret))

	#
	return ret

#global for eeprom 
g_EEPROM__LAN_access = 1;
g_EEPROM__on_TP      = 1;
g_EEPROM__buf_2KB = [0]*2048;

def eeprom_set_g_var (EEPROM__LAN_access=1, EEPROM__on_TP=1):
	print('\n>>>>>> eeprom_set_g_var')
	global g_EEPROM__LAN_access
	global g_EEPROM__on_TP
	#
	g_EEPROM__LAN_access = EEPROM__LAN_access
	g_EEPROM__on_TP      = EEPROM__on_TP
	#
	ret = (g_EEPROM__LAN_access<<8) + (g_EEPROM__on_TP<<9)
	
	# update wire
	dev.SetWireInValue(0x11,ret,0x00000300) # (ep,val,mask)
	dev.UpdateWireIns()	
	
	#
	return ret


def eeprom_send_frame (CMD=0x05, STA_in=0, ADL=0, ADH=0, num_bytes_DAT=1, con_disable_SBP=0):
	## 
	#num_bytes_DAT               = 1
	#con_disable_SBP             = 0
	
	global g_EEPROM__LAN_access
	#con_fifo_path__L_sspi_H_lan = 1 # LAN access
	#con_fifo_path__L_sspi_H_lan = 0 # slave spi access
	con_fifo_path__L_sspi_H_lan = g_EEPROM__LAN_access

	global g_EEPROM__on_TP
	#con_port__L_MEM_SIO__H_TP   = 1 # test TP	
	#con_port__L_MEM_SIO__H_TP   = 0 # test MEM_SIO
	con_port__L_MEM_SIO__H_TP   = g_EEPROM__on_TP
	
	#
	set_data_WI = (con_port__L_MEM_SIO__H_TP<<17) + (con_fifo_path__L_sspi_H_lan<<16) + (con_disable_SBP<<15) + num_bytes_DAT
	
	frame_data_CMD     = CMD    ## 0x05
	frame_data_STA_in  = STA_in ## 0x00
	frame_data_ADL     = ADL    ## 0x00
	frame_data_ADH     = ADH    ## 0x00
	#
	set_data_FDAT_WI = (frame_data_ADH<<24) + (frame_data_ADL<<16) + (frame_data_STA_in<<8) + frame_data_CMD
	
	ret = eeprom_send_frame_ep (MEM_WI=set_data_WI, MEM_FDAT_WI=set_data_FDAT_WI)
	#
	return ret


## write enable or disable 
def eeprom_write_enable():
	##$$ print('\n>>>>>> eeprom_write_enable')
	#
	## // CMD_WREN__96 
	##$$ print('\n>>> CMD_WREN__96')
	eeprom_send_frame (CMD=0x96, con_disable_SBP=1)

def eeprom_write_disable():
	print('\n>>>>>> eeprom_write_disable')
	#
	## // CMD_WRDI__91 
	print('\n>>> CMD_WRDI__91')
	eeprom_send_frame (CMD=0x91)

## manage status 
def eeprom_read_status():
	print('\n>>>>>> eeprom_read_status')
	#
	
	## // CMD_RDSR__05 
	print('\n>>> CMD_RDSR__05')
	eeprom_send_frame (CMD=0x05) 

	# clear TO 
	dev.UpdateTriggerOuts()
	ret=dev.GetTriggerOutVector(0x73)
	print('{} = 0x{:08X}'.format('ret', ret))

	# read again TO 
	dev.UpdateTriggerOuts()
	ret=dev.GetTriggerOutVector(0x73)
	print('{} = 0x{:08X}'.format('ret', ret))
	
	MUST_ZEROS = (ret>>12)&0x0F
	
	BP1 = (ret>>11)&0x01
	BP0 = (ret>>10)&0x01
	WEL = (ret>> 9)&0x01
	WIP = (ret>> 8)&0x01
	
	#
	return [BP1, BP0, WEL, WIP, MUST_ZEROS]
	
def eeprom_write_status(BP1, BP0):
	print('\n>>>>>> eeprom_write_status')
	#

	## // CMD_WREN__96 
	#print('\n>>> CMD_WREN__96')
	#eeprom_send_frame (CMD=0x96)
	eeprom_write_enable()
	
	##
	STA_in = (BP1<<3) + (BP0<<2)
	
	## // CMD_WRSR__6E
	print('\n>>> CMD_WRSR__6E')
	#eeprom_send_frame (CMD=0x6E, STA_in=0x0C)
	#eeprom_send_frame (CMD=0x6E, STA_in=0x08)
	#eeprom_send_frame (CMD=0x6E, STA_in=0x04)
	#eeprom_send_frame (CMD=0x6E, STA_in=0x00)
	eeprom_send_frame (CMD=0x6E, STA_in=STA_in)
	
	#
	return None

def is_eeprom_available():
	print('\n>>>>>> is_eeprom_available')
	ret = 1
	
	## initialize by sending stand-by pulse 
	eeprom_write_disable() # SBP
	#
	[BP1, BP0, WEL, WIP, _] = eeprom_read_status()
	print('{}={}'.format('WEL',WEL))
	#
	if WEL==0:
		ret = ret*1
	else:
		ret = ret*0
	
	## 
	eeprom_write_enable() ## No SBP
	#
	[BP1, BP0, WEL, WIP, _] = eeprom_read_status()
	print('{}={}'.format('WEL',WEL))
	#
	if WEL==1:
		ret = ret*1
	else:
		ret = ret*0
		
	##
	if (ret==1):
		return True
	else:
		return False


## erase or fill ones
def eeprom_erase_all():
	print('\n>>>>>> eeprom_erase_all')
	#
	
	eeprom_write_enable()
	
	## // CMD_ERAL__6D
	print('\n>>> CMD_ERAL__6D')
	eeprom_send_frame (CMD=0x6D)

	pass
	
def eeprom_set_all():
	print('\n>>>>>> eeprom_set_all')
	#
	
	eeprom_write_enable()
	
	## // CMD_SETAL_67
	print('\n>>> CMD_SETAL_67')
	eeprom_send_frame (CMD=0x67)

	pass
	

## manage fifo 
def eeprom_reset_fifo():
	print('\n>>>>>> eeprom_reset_fifo')
	
	#  // w_MEM_TI
	#  assign w_MEM_rst      = w_MEM_TI[0];
	#  assign w_MEM_fifo_rst = w_MEM_TI[1];
	#  assign w_trig_frame   = w_MEM_TI[2];	
	
	# act TI 
	dev.ActivateTriggerIn(0x53, 1)	## (ep, loc)
	
	pass

def eeprom_read_fifo(num_data=1):
	print('\n>>>>>> eeprom_read_fifo')
	#
	
	bytes_in_one_sample = 4 # for 32-bit end-point
	num_bytes_from_fifo = num_data * bytes_in_one_sample
	print('{} = {}'.format('num_bytes_from_fifo', num_bytes_from_fifo))
	
	## setup data buffer for fifo data
	dataout = bytearray([0] * num_bytes_from_fifo)
	
	## call api function to read pipeout data
	data_count = dev.ReadFromPipeOut(0xB3, dataout)
	print('{} : {}'.format('data_count [byte]',data_count))
	
	##  if data_count<0:
	##  	#return
	##  	# set test data 
	##  	num_data = 40
	##  	data_count = num_data * bytes_in_one_sample
	##  	data_int_list = [1,2,3, 2, 1, -1 ]
	##  	data_bytes_list = [x.to_bytes(bytes_in_one_sample,byteorder='little',signed=True) for x in data_int_list]
	##  	print('{} = {}'.format('data_bytes_list', data_bytes_list))
	##  	#dataout = b'\x01\x00\x00\x00\x02\x00\x00\x00'
	##  	dataout = b''.join(data_bytes_list)
	
	## convert bytearray to 32-bit data : high 24 bits to be ignored due to 8-bit fifo
	data_fifo_int_list = []
	for ii in range(0,num_data):
		temp_data = int.from_bytes(dataout[ii*bytes_in_one_sample:(ii+1)*bytes_in_one_sample], byteorder='little', signed=True)
		data_fifo_int_list += [temp_data&0x000000FF] # mask low 8-bit
	
	
	## print out 
	#if __debug__:print('{} = {}'.format('data_fifo_int_list', data_fifo_int_list))
	
	return data_fifo_int_list


def eeprom_write_fifo(datain__int_list=[0]):
	print('\n>>>>>> eeprom_write_fifo')
	#

	## convert 32-bit data to bytearray
	bytes_in_one_sample = 4 # for 32-bit end-point
	num_data = len(datain__int_list)
	num_bytes_to_fifo = num_data * bytes_in_one_sample
	datain = bytearray([0] * num_bytes_to_fifo)
	
	# convert bytes list : high 24 bits to be ignored due to 8-bit fifo
	datain__bytes_list = [x.to_bytes(bytes_in_one_sample,byteorder='little',signed=True) for x in datain__int_list]
	if __debug__:print('{} = {}'.format('datain__bytes_list', datain__bytes_list[:20]))
	
	# take out bytearray from list
	datain = b''.join(datain__bytes_list) 
	if __debug__:print('{} = {}'.format('datain', datain[:20]))
	
	## call api for pipein
	data_count = dev.WriteToPipeIn(0x93, datain)
	
	return 


## read/write data
def eeprom_read_data(ADRS_b16=0x0000, num_bytes_DAT=1):
	print('\n>>>>>> eeprom_read_data')
	#
	
	## reset fifo test 
	eeprom_reset_fifo()

	## convert address
	ADL = (ADRS_b16>>0)&0x00FF 
	ADH = (ADRS_b16>>8)&0x00FF
	print('{} = 0x{:08X}'.format('ADRS_b16', ADRS_b16))
	print('{} = 0x{:04X}'.format('ADH', ADH))
	print('{} = 0x{:04X}'.format('ADL', ADL))
	
	## // CMD_READ__03 
	print('\n>>> CMD_READ__03')
	eeprom_send_frame (CMD=0x03, ADL=ADL, ADH=ADH, num_bytes_DAT=num_bytes_DAT)

	## call fifo
	ret = eeprom_read_fifo(num_data=num_bytes_DAT)

	#
	return ret

	
def eeprom_read_data_current(num_bytes_DAT=1):
	print('\n>>>>>> eeprom_read_data_current')
	#
	
	## reset fifo test 
	eeprom_reset_fifo()

	## // CMD_CRRD__06 
	print('\n>>> CMD_CRRD__06')
	eeprom_send_frame (CMD=0x06, num_bytes_DAT=num_bytes_DAT)

	## call fifo
	ret = eeprom_read_fifo(num_data=num_bytes_DAT)

	#
	return ret

#def eeprom_write_data_16B(ADRS_b16=0x0000, num_bytes_DAT=16, data8b_in=[0]*16) :
def eeprom_write_data_16B(ADRS_b16=0x0000, num_bytes_DAT=16) :
	##$$ print('\n>>>>>> eeprom_write_data_16B')
	
	## call fifo 
	#eeprom_write_fifo(datain__int_list=data8b_in)
	
	## write enble
	eeprom_write_enable()
	
	## convert address
	ADL = (ADRS_b16>>0)&0x00FF 
	ADH = (ADRS_b16>>8)&0x00FF
	##$$ print('{} = 0x{:08X}'.format('ADRS_b16', ADRS_b16))
	##$$ print('{} = 0x{:04X}'.format('ADH', ADH))
	##$$ print('{} = 0x{:04X}'.format('ADL', ADL))
	
	## // CMD_WRITE_6C 
	##$$ print('\n>>> CMD_WRITE_6C')
	eeprom_send_frame (CMD=0x6C, ADL=ADL, ADH=ADH, num_bytes_DAT=num_bytes_DAT, con_disable_SBP=1)
	pass


def eeprom_write_data(ADRS_b16=0x0000, num_bytes_DAT=1, data8b_in=[0]):
	##$$ print('\n>>>>>> eeprom_write_data')

	##  The 11XX features a 16-byte page buffer, meaning that
	##  up to 16 bytes can be written at one time. To utilize this
	##  feature, the master can transmit up to 16 data bytes to
	##  the 11XX, which are temporarily stored in the page buffer.
	##  After each data byte, the master sends a MAK, indicating
	##  whether or not another data byte is to follow. A
	##  NoMAK indicates that no more data is to follow, and as
	##  such will initiate the internal write cycle.
	
	## reset fifo test 
	eeprom_reset_fifo()

	if num_bytes_DAT <= 16:
		eeprom_write_fifo(datain__int_list=data8b_in)
		#eeprom_write_data_16B(ADRS_b16=ADRS_b16, num_bytes_DAT=num_bytes_DAT, data8b_in=data8b_in)
		eeprom_write_data_16B(ADRS_b16=ADRS_b16, num_bytes_DAT=num_bytes_DAT)
	else:
		## call fifo : 8-bit width, depth 2048
		## note buf size 2048 
		## note 8-bit --> 32-bit conversion ... 4x loss
		## fifo size will be 2048/4=512
		#fifo_size = 512; # OK
		#fifo_size = 1024; # OK with 2048+56 buf fpga-side
		#fifo_size = 1024+512; # OK with 4096+512 buf fpga-side
		fifo_size = 2048; # OK with 4096*4+128 buf fpga-side
		#eeprom_write_fifo(datain__int_list=data8b_in)
		for ii in range(0,num_bytes_DAT,fifo_size):
			if __debug__: print('{} = {}'.format('ii', ii))
			eeprom_write_fifo(datain__int_list=data8b_in[(ii):(ii+fifo_size)])
			
		##  16-byte page buffer operation support
		for ii in range(0,num_bytes_DAT,16):
			#eeprom_write_data_16B(ADRS_b16=ADRS_b16+ii, data8b_in=data8b_in[(ii):(ii+16)])
			eeprom_write_data_16B(ADRS_b16=ADRS_b16+ii)
	pass


def eeprom_read_all():
	global g_EEPROM__buf_2KB
	
	g_EEPROM__buf_2KB = eeprom_read_data(ADRS_b16=0x0000, num_bytes_DAT=2048)
	
	return g_EEPROM__buf_2KB

def hex_txt_display(mem_data__list, offset=0x0000):
	# display : every 16 bytes
	#print(mem_data_2KB__list)
	# 014  0x00E0  FF FF FF FF FF FF FF FF  FF FF FF FF FF FF FF FF  ................
	# 015  0x00F0  FF FF FF FF FF FF FF FF  FF FF FF FF FF FF FF FF  ................
	# 016  0x0100  FF FF FF FF FF FF FF FF  FF FF FF FF FF FF FF FF  ................
	# 017  0x0110  FF FF FF FF FF FF FF FF  FF FF FF FF FF FF FF FF  ................
	# 018  0x0120  FF FF FF FE DC BA FF FF  FF FF FF FF FF FF FF FF  ................	
	
	mem_data_2KB__list = mem_data__list
	adrs_ofs = offset
	
	num_bytes_in_MEM = len(mem_data_2KB__list)
	num_bytes_in_a_display_line = 16
	
	output_display = ''
	for ii in range(0,int(num_bytes_in_MEM/num_bytes_in_a_display_line)):
		xx              = mem_data_2KB__list[(ii*16):(ii*16+16)]                                 # load line data
		output_display += '{:03d}  '  .format(ii)                                                # line number 
		output_display += '0x{:04X}  '.format(ii*16+adrs_ofs)                                             # start address each line
		output_display += ''.join([ '{:02X} '.format(jj) for jj in xx[0:8 ] ])                   # hex code 
		output_display += ' '
		output_display += ''.join([ '{:02X} '.format(jj) for jj in xx[8:16] ])                   # hex code 
		output_display += ' '
		output_display += ''.join([ chr(jj) if (jj>= 0x20 and jj<=0x7E) else '.' for jj in xx ]) # printable code
		output_display += '\n'                                                                   # line feed
	#
	return output_display


def cal_checksum (data_b8_list):
	ret = sum(data_b8_list) & 0xFF
	return ret
	
def gen_checksum (data_b8_list):
	ret = 0x100 - sum(data_b8_list) & 0xFF
	return ret
	

def eeprom_test():
	print('\n>>>>>> eeprom_test')

	## //// frame cases :
	## // CMD_READ__03
	## // CMD_CRRD__06
	## // CMD_WRITE_6C
	## // CMD_WREN__96
	## // CMD_WRDI__91 // auto disable ... after WRDI, WRSR, WRITE, ERAL, SETAL ...
	## // CMD_RDSR__05
	## // CMD_WRSR__6E
	## // CMD_ERAL__6D
	## // CMD_SETAL_67
	
	## //// status bits :
	## // ST[3] = BP1
	## // ST[2] = BP0
	## // ST[1] = WEL
	## // ST[0] = WIP
	

	## eeprom check 
	## eeprom connection check with write enable 
	eeprom_set_g_var(EEPROM__LAN_access=1, EEPROM__on_TP=0) # EEPROM on MEM_SIO
	ret = is_eeprom_available()
	if ret==True:
		print('>>> EEPROM on MEM_SIO is available.')
	else:
		eeprom_set_g_var(EEPROM__LAN_access=1, EEPROM__on_TP=1) # EEPROM on TP
		ret = is_eeprom_available()
		if ret==True:
			print('>>> EEPROM on TP is available.')
		else: 
			print('>>> EEPROM is NOT available.')
			input('')
	
	##input('')


	READ_STATUS_TEST_EN = 0
	if READ_STATUS_TEST_EN: 
		## read status
		[BP1, BP0, WEL, WIP, MUST_ZEROS] = eeprom_read_status()
		print('{}={}'.format('MUST_ZEROS',MUST_ZEROS))
		print('{}={}'.format('BP1',BP1))
		print('{}={}'.format('BP0',BP0))
		print('{}={}'.format('WEL',WEL))
		print('{}={}'.format('WIP',WIP))
		
		## write enable check 
		eeprom_write_enable()
		
		[BP1, BP0, WEL, WIP, _] = eeprom_read_status()
		print('{}={}'.format('WEL',WEL))
		
		if WEL==1:
			print('### OK ###')
		else:
			#raise
			print('### NG ###')
			input('')
			
		eeprom_write_disable()
	
		[BP1, BP0, WEL, WIP, _] = eeprom_read_status()
		print('{}={}'.format('WEL',WEL))
	
		if WEL==0:
			print('### OK ###')
		else:
			#raise
			print('### NG ###')
			input('')
	
		## status check 
		BP_test = [[0,0],[0,1],[1,0],[1,1],[0,0]]
		for xx in BP_test:
			eeprom_write_status(BP1=xx[0], BP0=xx[1])
			#
			[BP1, BP0, WEL, WIP, _] = eeprom_read_status()
			print('{}={}'.format('BP1',BP1))
			print('{}={}'.format('BP0',BP0))
			print('{}={}'.format('WEL',WEL))
			print('{}={}'.format('WIP',WIP))
			#
			if BP1==xx[0] and BP0==xx[1]:
				print('### OK ###')
			else:
				#raise
				print('### NG ###')
				input('')
			#
	
	## protection 
	#eeprom_write_status(BP1=0, BP0=0);
	

	##  ## erase test 
	##  eeprom_erase_all()
	##  
	##  [BP1, BP0, WEL, WIP] = eeprom_read_status()
	##  print('{}={}'.format('WIP',WIP))
	##  
	##  #[BP1, BP0, WEL, WIP] = eeprom_read_status()
	##  #print('{}={}'.format('WIP',WIP))
	##  #
	##  #[BP1, BP0, WEL, WIP] = eeprom_read_status()
	##  #print('{}={}'.format('WIP',WIP))
	##  #
	##  #[BP1, BP0, WEL, WIP] = eeprom_read_status()
	##  #print('{}={}'.format('WIP',WIP))
	##  
	##  
	##  ## set all test 
	##  eeprom_set_all()
	##  
	##  [BP1, BP0, WEL, WIP] = eeprom_read_status()
	##  print('{}={}'.format('WIP',WIP))
	##  
	##  #[BP1, BP0, WEL, WIP] = eeprom_read_status()
	##  #print('{}={}'.format('WIP',WIP))
	##  #
	##  #[BP1, BP0, WEL, WIP] = eeprom_read_status()
	##  #print('{}={}'.format('WIP',WIP))
	##  #
	##  #[BP1, BP0, WEL, WIP] = eeprom_read_status()
	##  #print('{}={}'.format('WIP',WIP))
	##  
	##  
	
	
	## erase test 
	ERASE_TEST_EN = 0
	if ERASE_TEST_EN: 
		eeprom_erase_all()
		#eeprom_set_all()
	

	## all block write test 
	ALL_BLOCK_WRITE_TEST_EN = 0
	if ALL_BLOCK_WRITE_TEST_EN: 
		mem_data_2KB = [xx+0xCD for xx in range(0,2048)] # 
		#mem_data_2KB = [xx+0xCD for xx in range(0,512)] # 0 data NG ... PI LAN packet check..
		#mem_data_2KB = [xx+0xCD for xx in range(0,256)] # OK 
		#mem_data_2KB = [xx+0xCD for xx in range(0,128)] # OK 
		#mem_data_2KB = [xx+0xCD for xx in range(0,64)] # OK 
		#mem_data_2KB = [xx+0xCD for xx in range(0,32)] # OK 
		
		mem_data_2KB[0x00:0x10] = [ ord(xx) for xx in 'MHVSU_BASE_#0000'] # info text // on TP
		mem_data_2KB[0x10:0x20] = [ xx      for xx in [192,168,168,143, 255,255,255,0, 192,168,168,1,  0,0,0,0] ] # info IP
		mem_data_2KB[0x20:0x30] = [ ord(xx) for xx in '0008dc00abcd____'] # info text
		mem_data_2KB[0x30:0x40] = [ ord(xx) for xx in '----------------'] # info text
		
		#mem_data_2KB = [
		#	0xED, 0xBC, 0x56, 0x67, 0xAB, 0xCD, 0x12, 0x34,
		#	0xDE, 0xCB, 0x65, 0x76, 0xBA, 0xDC, 0x21, 0x43,
		#	0xAB, 0xBC, 0x56, 0x34, 0xED, 0xCD, 0x12, 0x67,
		#	0xBA, 0xCB, 0x65, 0x43, 0xDE, 0xDC, 0x21, 0x76]
		
		print(len(mem_data_2KB))
		
		eeprom_write_data(ADRS_b16=0x0000, num_bytes_DAT=len(mem_data_2KB), data8b_in=mem_data_2KB)
	
	## write mem header test 
	HEADER_WRITE_TEST_EN = 0
	if HEADER_WRITE_TEST_EN: 
	
		# info 
		#board_id = 11 # board-REV2 #11
		#board_id = 12 # board-REV2 #12
		#board_id = 13 # board-REV2 #13
		board_id = 14 # for EEPROM from TP test
		#board_id = 15 # for EEPROM from TP
		#SIP = [192,168,168,143] # 128 + 15
		SIP = [192,168,168,128+board_id] # 128 + board_id
		SUB = [255,255,255,  0]
		GAR = [192,168,168,  1]
		DNS = [  0,  0,  0,  0]
		#MAC = '0008dc00abcd'
		MAC = '0008dc00ab' '{:02x}'.format(0xcd + board_id) # cd + board_id
		BID = '{:02d}'.format(board_id) # board id 
		slot_id = board_id
		SID = '{:02d}'.format(slot_id) # slot_id # 0 for loading from mother board later
		UID = '$'
		CKS = '#'
	
		mem_header = [0]*(16*4)
		#mem_header[0x00:0x10] = [ ord(xx) for xx in 'MHVSU_BASE_#0000'] # info text on TP
		mem_header[0x00:0x10] = [ ord(xx) for xx in 'MHVSU_BASE_#00'+BID ] # info text // on Board ID 
		mem_header[0x10:0x20] = [ xx      for xx in SIP+SUB+GAR+DNS ] # info IP
		mem_header[0x20:0x30] = [ ord(xx) for xx in MAC+SID+UID+CKS ] # info text : MAC(12 hex char) + Slot ID(2 bytes) + USER ID(1 bytes) + IP checksum (1 byte)
		mem_header[0x30:0x40] = [ ord(xx) for xx in '-_-_-_-_-_-_-_-_'] # info text
		
		print(mem_header[0x10:0x30])
		print(cal_checksum(mem_header[0x10:0x30]))
		mem_header[0x2F] = gen_checksum(mem_header[0x10:0x2F]) # update zero checksum CKS
		print(mem_header[0x2F])
		print(cal_checksum(mem_header[0x10:0x30]))
		
		eeprom_write_data(ADRS_b16=0x0000, num_bytes_DAT=len(mem_header), data8b_in=mem_header)
	
	## read mem header test 
	mem_data__list = eeprom_read_data(ADRS_b16=0x0000, num_bytes_DAT=16*4)
	output_display = hex_txt_display(mem_data__list)
	print(output_display)
	

	##  ##  write data test with random string 
	##  import random
	##  test_str = ''.join( [random.choice('0123456789ABCDEF') for i in range(0,32)] )
	##  test_int_list = [ord(xx) for xx in test_str]
	##  print(test_str)
	##  print(test_int_list)
	##  
	##  eeprom_write_data(ADRS_b16=0x0030, num_bytes_DAT=16, data8b_in=test_int_list[0:16])
	##  eeprom_write_data(ADRS_b16=0x0040, num_bytes_DAT= 8, data8b_in=test_int_list[0:8 ])
	##  eeprom_write_data(ADRS_b16=0x0050, num_bytes_DAT=24, data8b_in=test_int_list[0:24])
	##  eeprom_write_data(ADRS_b16=0x0070, num_bytes_DAT=32, data8b_in=test_int_list[0:32])
	##  #
	##  ret_list = eeprom_read_data(ADRS_b16=0x0030, num_bytes_DAT=16*6)
	##  output_display = hex_txt_display(mem_data__list=ret_list, offset=0x0030)
	##  print(output_display)
	##  
	##  print(test_str)
	##  
	##  #input('')

	## all block read test 
	#mem_data_2KB__list = eeprom_read_data(ADRS_b16=0x0000, num_bytes_DAT=2048)
	mem_data_2KB__list = eeprom_read_all()
	
	# 2KB read time stamp (- 3238772115 + 3248971415) / (10MHz) = 1.01993 s
	
	output_display = hex_txt_display(mem_data_2KB__list)
	print(output_display)
	
	
	#if __debug__: input('stop')
	
	pass


## TODO: gui setup ##
def tk_win_setup__spio(sub_spio, row_SPIO):
	#### sub_spio ####
	Label(sub_spio, text="===[SPIO]===").grid(row=row_SPIO, column= 0, sticky=W, columnspan = 4)
	
	## buttons for SPIO 
	Label(sub_spio, text="[SPIO] SKT# selection:  ").grid(row=row_SPIO+1, column=0, sticky=W, columnspan = 1)
	Label(sub_spio, text="[SPIO] CS0 outputs:     ").grid(row=row_SPIO+1, column=1, sticky=W, columnspan = 1)
	Label(sub_spio, text="[SPIO] CS1 outputs:     ").grid(row=row_SPIO+1, column=2, sticky=W, columnspan = 1)
	Label(sub_spio, text="[SPIO] CS2 outputs:     ").grid(row=row_SPIO+1, column=3, sticky=W, columnspan = 1)
	# 
	Checkbutton(sub_spio, text="SKT1 ENABLE", variable=var_SKT1_EN).grid(row=row_SPIO+1+1, column=0, sticky=W)
	Checkbutton(sub_spio, text="SKT2 ENABLE", variable=var_SKT2_EN).grid(row=row_SPIO+1+2, column=0, sticky=W)
	Checkbutton(sub_spio, text="SKT3 ENABLE", variable=var_SKT3_EN).grid(row=row_SPIO+1+3, column=0, sticky=W)
	Checkbutton(sub_spio, text="SKT4 ENABLE", variable=var_SKT4_EN).grid(row=row_SPIO+1+4, column=0, sticky=W)
	Checkbutton(sub_spio, text="SKT5 ENABLE", variable=var_SKT5_EN).grid(row=row_SPIO+1+5, column=0, sticky=W)
	Checkbutton(sub_spio, text="SKT6 ENABLE", variable=var_SKT6_EN).grid(row=row_SPIO+1+6, column=0, sticky=W)
	Checkbutton(sub_spio, text="SKT7 ENABLE", variable=var_SKT7_EN).grid(row=row_SPIO+1+7, column=0, sticky=W)
	Checkbutton(sub_spio, text="SKT8 ENABLE", variable=var_SKT8_EN).grid(row=row_SPIO+1+8, column=0, sticky=W)
	#
	Label(sub_spio, text="[SPIO] forced mode:").grid(row=row_SPIO+12+0, column= 0, sticky=W, columnspan = 1)
	Checkbutton(sub_spio, text="forced_mode_en    ", variable=var_forced_mode_en ).grid(row=row_SPIO+12+1, column=0, sticky=W)
	Checkbutton(sub_spio, text="forced_sig_mosi   ", variable=var_forced_sig_mosi).grid(row=row_SPIO+12+2, column=0, sticky=W)
	Checkbutton(sub_spio, text="forced_sig_sclk   ", variable=var_forced_sig_sclk).grid(row=row_SPIO+12+3, column=0, sticky=W)
	Checkbutton(sub_spio, text="forced_sig_csel   ", variable=var_forced_sig_csel).grid(row=row_SPIO+12+4, column=0, sticky=W)
	Checkbutton(sub_spio, text="forced_cs0        ", variable=var_forced_cs0     ).grid(row=row_SPIO+12+5, column=0, sticky=W)
	Checkbutton(sub_spio, text="forced_cs1        ", variable=var_forced_cs1     ).grid(row=row_SPIO+12+6, column=0, sticky=W)
	Checkbutton(sub_spio, text="forced_cs2        ", variable=var_forced_cs2     ).grid(row=row_SPIO+12+7, column=0, sticky=W)
	#
	Button(sub_spio, text='update SPIO forced mode', command=spio_bb_update_forced_mode) .grid(row=row_SPIO+12+8, column=0, columnspan = 1, sticky=W+E, pady=4)	
	
	# sub v1
	#Checkbutton(sub_spio, text="CS0 GPB7 CHECK_LED0     ", variable=var_CS0_GPB7).grid(row=row_SPIO+1+ 1, column=1, sticky=W) # CS0 GPB7 CHECK_LED0     
	#Checkbutton(sub_spio, text="CS0 GPB6 CH2_ADC_VM     ", variable=var_CS0_GPB6).grid(row=row_SPIO+1+ 2, column=1, sticky=W) # CS0 GPB6 CH2_ADC_VM     
	#Checkbutton(sub_spio, text="CS0 GPB5 CH2_GAIN_X20   ", variable=var_CS0_GPB5).grid(row=row_SPIO+1+ 3, column=1, sticky=W) # CS0 GPB5 CH2_GAIN_X20   
	#Checkbutton(sub_spio, text="CS0 GPB4 CH2_GAIN_X2    ", variable=var_CS0_GPB4).grid(row=row_SPIO+1+ 4, column=1, sticky=W) # CS0 GPB4 CH2_GAIN_X2    
	#Checkbutton(sub_spio, text="CS0 GPB3 NA             ", variable=var_CS0_GPB3).grid(row=row_SPIO+1+ 5, column=1, sticky=W) # CS0 GPB3 NA             
	#Checkbutton(sub_spio, text="CS0 GPB2 CH2_RANGE_20uA ", variable=var_CS0_GPB2).grid(row=row_SPIO+1+ 6, column=1, sticky=W) # CS0 GPB2 CH2_RANGE_20uA 
	#Checkbutton(sub_spio, text="CS0 GPB1 CH2_RANGE_2mA  ", variable=var_CS0_GPB1).grid(row=row_SPIO+1+ 7, column=1, sticky=W) # CS0 GPB1 CH2_RANGE_2mA  
	#Checkbutton(sub_spio, text="CS0 GPB0 CH2_RANGE_50mA ", variable=var_CS0_GPB0).grid(row=row_SPIO+1+ 8, column=1, sticky=W) # CS0 GPB0 CH2_RANGE_50mA 
	#Checkbutton(sub_spio, text="CS0 GPA7 NA             ", variable=var_CS0_GPA7).grid(row=row_SPIO+1+11, column=1, sticky=W) # CS0 GPA7 NA             
	#Checkbutton(sub_spio, text="CS0 GPA6 CH1_ADC_VM     ", variable=var_CS0_GPA6).grid(row=row_SPIO+1+12, column=1, sticky=W) # CS0 GPA6 CH1_ADC_VM     
	#Checkbutton(sub_spio, text="CS0 GPA5 CH1_GAIN_X20   ", variable=var_CS0_GPA5).grid(row=row_SPIO+1+13, column=1, sticky=W) # CS0 GPA5 CH1_GAIN_X20   
	#Checkbutton(sub_spio, text="CS0 GPA4 CH1_GAIN_X2    ", variable=var_CS0_GPA4).grid(row=row_SPIO+1+14, column=1, sticky=W) # CS0 GPA4 CH1_GAIN_X2    
	#Checkbutton(sub_spio, text="CS0 GPA3 NA             ", variable=var_CS0_GPA3).grid(row=row_SPIO+1+15, column=1, sticky=W) # CS0 GPA3 NA             
	#Checkbutton(sub_spio, text="CS0 GPA2 CH1_RANGE_20uA ", variable=var_CS0_GPA2).grid(row=row_SPIO+1+16, column=1, sticky=W) # CS0 GPA2 CH1_RANGE_20uA 
	#Checkbutton(sub_spio, text="CS0 GPA1 CH1_RANGE_2mA  ", variable=var_CS0_GPA1).grid(row=row_SPIO+1+17, column=1, sticky=W) # CS0 GPA1 CH1_RANGE_2mA  
	#Checkbutton(sub_spio, text="CS0 GPA0 CH1_RANGE_50mA ", variable=var_CS0_GPA0).grid(row=row_SPIO+1+18, column=1, sticky=W) # CS0 GPA0 CH1_RANGE_50mA 
	# sub demo
	Checkbutton(sub_spio, text="CS0 GPB7 CH1_BYPASS_ON   ", variable=var_CS0_GPB7).grid(row=row_SPIO+1+ 1, column=1, sticky=W) # CS0 GPB7 CHECK_LED0     
	Checkbutton(sub_spio, text="CS0 GPB6 CH1_GAIN_X10    ", variable=var_CS0_GPB6).grid(row=row_SPIO+1+ 2, column=1, sticky=W) # CS0 GPB6 CH2_ADC_VM     
	Checkbutton(sub_spio, text="CS0 GPB5 CH1_LIMIT_50mA  ", variable=var_CS0_GPB5).grid(row=row_SPIO+1+ 3, column=1, sticky=W) # CS0 GPB5 CH2_GAIN_X20   
	Checkbutton(sub_spio, text="CS0 GPB4 CH1_RANGE_200nA ", variable=var_CS0_GPB4).grid(row=row_SPIO+1+ 4, column=1, sticky=W) # CS0 GPB4 CH2_GAIN_X2    
	Checkbutton(sub_spio, text="CS0 GPB3 CH1_RANGE_2uA   ", variable=var_CS0_GPB3).grid(row=row_SPIO+1+ 5, column=1, sticky=W) # CS0 GPB3 NA             
	Checkbutton(sub_spio, text="CS0 GPB2 CH1_RANGE_20uA  ", variable=var_CS0_GPB2).grid(row=row_SPIO+1+ 6, column=1, sticky=W) # CS0 GPB2 CH2_RANGE_20uA 
	Checkbutton(sub_spio, text="CS0 GPB1 CH1_RANGE_200uA ", variable=var_CS0_GPB1).grid(row=row_SPIO+1+ 7, column=1, sticky=W) # CS0 GPB1 CH2_RANGE_2mA  
	Checkbutton(sub_spio, text="CS0 GPB0 CH1_RANGE_2mA   ", variable=var_CS0_GPB0).grid(row=row_SPIO+1+ 8, column=1, sticky=W) # CS0 GPB0 CH2_RANGE_50mA 
	Checkbutton(sub_spio, text="CS0 GPA7 CH1_ADC_IM      ", variable=var_CS0_GPA7).grid(row=row_SPIO+1+11, column=1, sticky=W) # CS0 GPA7 NA             
	Checkbutton(sub_spio, text="CS0 GPA6 CH1_ADC_VM      ", variable=var_CS0_GPA6).grid(row=row_SPIO+1+12, column=1, sticky=W) # CS0 GPA6 CH1_ADC_VM     
	Checkbutton(sub_spio, text="CS0 GPA5 CH1_CMP_ON1     ", variable=var_CS0_GPA5).grid(row=row_SPIO+1+13, column=1, sticky=W) # CS0 GPA5 CH1_GAIN_X20   
	Checkbutton(sub_spio, text="CS0 GPA4 CH1_CMP_ON2     ", variable=var_CS0_GPA4).grid(row=row_SPIO+1+14, column=1, sticky=W) # CS0 GPA4 CH1_GAIN_X2    
	Checkbutton(sub_spio, text="CS0 GPA3 NA              ", variable=var_CS0_GPA3).grid(row=row_SPIO+1+15, column=1, sticky=W) # CS0 GPA3 NA             
	Checkbutton(sub_spio, text="CS0 GPA2 NA              ", variable=var_CS0_GPA2).grid(row=row_SPIO+1+16, column=1, sticky=W) # CS0 GPA2 CH1_RANGE_20uA 
	Checkbutton(sub_spio, text="CS0 GPA1 NA              ", variable=var_CS0_GPA1).grid(row=row_SPIO+1+17, column=1, sticky=W) # CS0 GPA1 CH1_RANGE_2mA  
	Checkbutton(sub_spio, text="CS0 GPA0 CHECK_LED0      ", variable=var_CS0_GPA0).grid(row=row_SPIO+1+18, column=1, sticky=W) # CS0 GPA0 CH1_RANGE_50mA 
	
	# sub v1
	#Checkbutton(sub_spio, text="CS1 GPB7 CHECK_LED1      ", variable=var_CS1_GPB7).grid(row=row_SPIO+1+ 1, column=2, sticky=W) # CS1 GPB7 CHECK_LED1       
	#Checkbutton(sub_spio, text="CS1 GPB6 CH2_RELAY_DIAG  ", variable=var_CS1_GPB6).grid(row=row_SPIO+1+ 2, column=2, sticky=W) # CS1 GPB6 CH2_RELAY_DIAG   
	#Checkbutton(sub_spio, text="CS1 GPB5 CH2_RELAY_OUTPUT", variable=var_CS1_GPB5).grid(row=row_SPIO+1+ 3, column=2, sticky=W) # CS1 GPB5 CH2_RELAY_OUTPUT 
	#Checkbutton(sub_spio, text="CS1 GPB4 CH2_LIMIT_50mA  ", variable=var_CS1_GPB4).grid(row=row_SPIO+1+ 4, column=2, sticky=W) # CS1 GPB4 CH2_LIMIT_50mA   
	#Checkbutton(sub_spio, text="CS1 GPB3 NA              ", variable=var_CS1_GPB3).grid(row=row_SPIO+1+ 5, column=2, sticky=W) # CS1 GPB3 NA               
	#Checkbutton(sub_spio, text="CS1 GPB2 NA              ", variable=var_CS1_GPB2).grid(row=row_SPIO+1+ 6, column=2, sticky=W) # CS1 GPB2 NA               
	#Checkbutton(sub_spio, text="CS1 GPB1 CH2_CMP_330p    ", variable=var_CS1_GPB1).grid(row=row_SPIO+1+ 7, column=2, sticky=W) # CS1 GPB1 CH2_CMP_330p     
	#Checkbutton(sub_spio, text="CS1 GPB0 CH2_CMP_100p    ", variable=var_CS1_GPB0).grid(row=row_SPIO+1+ 8, column=2, sticky=W) # CS1 GPB0 CH2_CMP_100p     
	#Checkbutton(sub_spio, text="CS1 GPA7 NA              ", variable=var_CS1_GPA7).grid(row=row_SPIO+1+11, column=2, sticky=W) # CS1 GPA7 NA               
	#Checkbutton(sub_spio, text="CS1 GPA6 CH1_RELAY_DIAG  ", variable=var_CS1_GPA6).grid(row=row_SPIO+1+12, column=2, sticky=W) # CS1 GPA6 CH1_RELAY_DIAG   
	#Checkbutton(sub_spio, text="CS1 GPA5 CH1_RELAY_OUTPUT", variable=var_CS1_GPA5).grid(row=row_SPIO+1+13, column=2, sticky=W) # CS1 GPA5 CH1_RELAY_OUTPUT 
	#Checkbutton(sub_spio, text="CS1 GPA4 CH1_LIMIT_50mA  ", variable=var_CS1_GPA4).grid(row=row_SPIO+1+14, column=2, sticky=W) # CS1 GPA4 CH1_LIMIT_50mA   
	#Checkbutton(sub_spio, text="CS1 GPA3 NA              ", variable=var_CS1_GPA3).grid(row=row_SPIO+1+15, column=2, sticky=W) # CS1 GPA3 NA               
	#Checkbutton(sub_spio, text="CS1 GPA2 NA              ", variable=var_CS1_GPA2).grid(row=row_SPIO+1+16, column=2, sticky=W) # CS1 GPA2 NA               
	#Checkbutton(sub_spio, text="CS1 GPA1 CH1_CMP_330p    ", variable=var_CS1_GPA1).grid(row=row_SPIO+1+17, column=2, sticky=W) # CS1 GPA1 CH1_CMP_330p     
	#Checkbutton(sub_spio, text="CS1 GPA0 CH1_CMP_100p    ", variable=var_CS1_GPA0).grid(row=row_SPIO+1+18, column=2, sticky=W) # CS1 GPA0 CH1_CMP_100p     
	# sub demo
	Checkbutton(sub_spio, text="CS1 GPB7 NA   ", variable=var_CS1_GPB7).grid(row=row_SPIO+1+ 1, column=2, sticky=W) # CS1 GPB7 CHECK_LED1       
	Checkbutton(sub_spio, text="CS1 GPB6 NA   ", variable=var_CS1_GPB6).grid(row=row_SPIO+1+ 2, column=2, sticky=W) # CS1 GPB6 CH2_RELAY_DIAG   
	Checkbutton(sub_spio, text="CS1 GPB5 NA   ", variable=var_CS1_GPB5).grid(row=row_SPIO+1+ 3, column=2, sticky=W) # CS1 GPB5 CH2_RELAY_OUTPUT 
	Checkbutton(sub_spio, text="CS1 GPB4 NA   ", variable=var_CS1_GPB4).grid(row=row_SPIO+1+ 4, column=2, sticky=W) # CS1 GPB4 CH2_LIMIT_50mA   
	Checkbutton(sub_spio, text="CS1 GPB3 NA   ", variable=var_CS1_GPB3).grid(row=row_SPIO+1+ 5, column=2, sticky=W) # CS1 GPB3 NA               
	Checkbutton(sub_spio, text="CS1 GPB2 NA   ", variable=var_CS1_GPB2).grid(row=row_SPIO+1+ 6, column=2, sticky=W) # CS1 GPB2 NA               
	Checkbutton(sub_spio, text="CS1 GPB1 NA   ", variable=var_CS1_GPB1).grid(row=row_SPIO+1+ 7, column=2, sticky=W) # CS1 GPB1 CH2_CMP_330p     
	Checkbutton(sub_spio, text="CS1 GPB0 NA   ", variable=var_CS1_GPB0).grid(row=row_SPIO+1+ 8, column=2, sticky=W) # CS1 GPB0 CH2_CMP_100p     
	Checkbutton(sub_spio, text="CS1 GPA7 NA   ", variable=var_CS1_GPA7).grid(row=row_SPIO+1+11, column=2, sticky=W) # CS1 GPA7 NA               
	Checkbutton(sub_spio, text="CS1 GPA6 NA   ", variable=var_CS1_GPA6).grid(row=row_SPIO+1+12, column=2, sticky=W) # CS1 GPA6 CH1_RELAY_DIAG   
	Checkbutton(sub_spio, text="CS1 GPA5 NA   ", variable=var_CS1_GPA5).grid(row=row_SPIO+1+13, column=2, sticky=W) # CS1 GPA5 CH1_RELAY_OUTPUT 
	Checkbutton(sub_spio, text="CS1 GPA4 NA   ", variable=var_CS1_GPA4).grid(row=row_SPIO+1+14, column=2, sticky=W) # CS1 GPA4 CH1_LIMIT_50mA   
	Checkbutton(sub_spio, text="CS1 GPA3 NA   ", variable=var_CS1_GPA3).grid(row=row_SPIO+1+15, column=2, sticky=W) # CS1 GPA3 NA               
	Checkbutton(sub_spio, text="CS1 GPA2 NA   ", variable=var_CS1_GPA2).grid(row=row_SPIO+1+16, column=2, sticky=W) # CS1 GPA2 NA               
	Checkbutton(sub_spio, text="CS1 GPA1 NA   ", variable=var_CS1_GPA1).grid(row=row_SPIO+1+17, column=2, sticky=W) # CS1 GPA1 CH1_CMP_330p     
	Checkbutton(sub_spio, text="CS1 GPA0 NA   ", variable=var_CS1_GPA0).grid(row=row_SPIO+1+18, column=2, sticky=W) # CS1 GPA0 CH1_CMP_100p     
	
	#
	Checkbutton(sub_spio, text="CS2 GPB7 NA   ", variable=var_CS2_GPB7).grid(row=row_SPIO+1+ 1, column=3, sticky=W) # CS2 GPB7 NA
	Checkbutton(sub_spio, text="CS2 GPB6 NA   ", variable=var_CS2_GPB6).grid(row=row_SPIO+1+ 2, column=3, sticky=W) # CS2 GPB6 NA
	Checkbutton(sub_spio, text="CS2 GPB5 NA   ", variable=var_CS2_GPB5).grid(row=row_SPIO+1+ 3, column=3, sticky=W) # CS2 GPB5 NA
	Checkbutton(sub_spio, text="CS2 GPB4 NA   ", variable=var_CS2_GPB4).grid(row=row_SPIO+1+ 4, column=3, sticky=W) # CS2 GPB4 NA
	Checkbutton(sub_spio, text="CS2 GPB3 NA   ", variable=var_CS2_GPB3).grid(row=row_SPIO+1+ 5, column=3, sticky=W) # CS2 GPB3 NA
	Checkbutton(sub_spio, text="CS2 GPB2 NA   ", variable=var_CS2_GPB2).grid(row=row_SPIO+1+ 6, column=3, sticky=W) # CS2 GPB2 NA
	Checkbutton(sub_spio, text="CS2 GPB1 NA   ", variable=var_CS2_GPB1).grid(row=row_SPIO+1+ 7, column=3, sticky=W) # CS2 GPB1 NA
	Checkbutton(sub_spio, text="CS2 GPB0 NA   ", variable=var_CS2_GPB0).grid(row=row_SPIO+1+ 8, column=3, sticky=W) # CS2 GPB0 NA
	Checkbutton(sub_spio, text="CS2 GPA7 NA   ", variable=var_CS2_GPA7).grid(row=row_SPIO+1+11, column=3, sticky=W) # CS2 GPA7 NA
	Checkbutton(sub_spio, text="CS2 GPA6 NA   ", variable=var_CS2_GPA6).grid(row=row_SPIO+1+12, column=3, sticky=W) # CS2 GPA6 NA
	Checkbutton(sub_spio, text="CS2 GPA5 NA   ", variable=var_CS2_GPA5).grid(row=row_SPIO+1+13, column=3, sticky=W) # CS2 GPA5 NA
	Checkbutton(sub_spio, text="CS2 GPA4 NA   ", variable=var_CS2_GPA4).grid(row=row_SPIO+1+14, column=3, sticky=W) # CS2 GPA4 NA
	Checkbutton(sub_spio, text="CS2 GPA3 NA   ", variable=var_CS2_GPA3).grid(row=row_SPIO+1+15, column=3, sticky=W) # CS2 GPA3 NA
	Checkbutton(sub_spio, text="CS2 GPA2 NA   ", variable=var_CS2_GPA2).grid(row=row_SPIO+1+16, column=3, sticky=W) # CS2 GPA2 NA
	Checkbutton(sub_spio, text="CS2 GPA1 NA   ", variable=var_CS2_GPA1).grid(row=row_SPIO+1+17, column=3, sticky=W) # CS2 GPA1 NA
	Checkbutton(sub_spio, text="CS2 GPA0 NA   ", variable=var_CS2_GPA0).grid(row=row_SPIO+1+18, column=3, sticky=W) # CS2 GPA0 NA
	
	# update SPIO values
	bb_spio_cs0_update = Button(sub_spio, text='update SPIO CS0 values', command=spio_bb_update_cs0) .grid(row=row_SPIO+20, column=1, columnspan = 1, sticky=W+E, pady=4)
	bb_spio_cs1_update = Button(sub_spio, text='update SPIO CS1 values', command=spio_bb_update_cs1) .grid(row=row_SPIO+20, column=2, columnspan = 1, sticky=W+E, pady=4)
	bb_spio_cs2_update = Button(sub_spio, text='update SPIO CS2 values', command=spio_bb_update_cs2) .grid(row=row_SPIO+20, column=3, columnspan = 1, sticky=W+E, pady=4)
	pass

def tk_win_setup__dac_(sub_dac_, row_DAC):
	#### sub_dac_ ####
	Label(sub_dac_, text="===[DAC]===") .grid(row=row_DAC , column= 0, sticky=W, columnspan = 4)
	
	## buttons/labels/entries for DAC
	#
	Label(sub_dac_, text="[DAC] CH1 Value-in:     ").grid(row=row_DAC+1, column= 0, sticky=W, columnspan = 2)
	Label(sub_dac_, text="[DAC] CH2 Value-in:     ").grid(row=row_DAC+1, column= 2, sticky=W, columnspan = 2)
	Label(sub_dac_, text="[DAC] CH1 Readback:     ").grid(row=row_DAC+1, column= 4, sticky=W, columnspan = 2)
	Label(sub_dac_, text="[DAC] CH2 Readback:     ").grid(row=row_DAC+1, column= 6, sticky=W, columnspan = 2)
	# DAC values
	Label(sub_dac_, text=" S1_CH1_W=").grid(row=row_DAC+1+1, column=0, sticky=E)
	Label(sub_dac_, text=" S2_CH1_W=").grid(row=row_DAC+1+2, column=0, sticky=E)
	Label(sub_dac_, text=" S3_CH1_W=").grid(row=row_DAC+1+3, column=0, sticky=E)
	Label(sub_dac_, text=" S4_CH1_W=").grid(row=row_DAC+1+4, column=0, sticky=E)
	Label(sub_dac_, text=" S5_CH1_W=").grid(row=row_DAC+1+5, column=0, sticky=E)
	Label(sub_dac_, text=" S6_CH1_W=").grid(row=row_DAC+1+6, column=0, sticky=E)
	Label(sub_dac_, text=" S7_CH1_W=").grid(row=row_DAC+1+7, column=0, sticky=E)
	Label(sub_dac_, text=" S8_CH1_W=").grid(row=row_DAC+1+8, column=0, sticky=E)
	ee_dac_skt1_ch1_val                     .grid(row=row_DAC+1+1, column=1)
	ee_dac_skt2_ch1_val                     .grid(row=row_DAC+1+2, column=1)
	ee_dac_skt3_ch1_val                     .grid(row=row_DAC+1+3, column=1)
	ee_dac_skt4_ch1_val                     .grid(row=row_DAC+1+4, column=1)
	ee_dac_skt5_ch1_val                     .grid(row=row_DAC+1+5, column=1)
	ee_dac_skt6_ch1_val                     .grid(row=row_DAC+1+6, column=1)
	ee_dac_skt7_ch1_val                     .grid(row=row_DAC+1+7, column=1)
	ee_dac_skt8_ch1_val                     .grid(row=row_DAC+1+8, column=1)
	Label(sub_dac_, text=" S1_CH2_W=").grid(row=row_DAC+1+1, column=2, sticky=E)
	Label(sub_dac_, text=" S2_CH2_W=").grid(row=row_DAC+1+2, column=2, sticky=E)
	Label(sub_dac_, text=" S3_CH2_W=").grid(row=row_DAC+1+3, column=2, sticky=E)
	Label(sub_dac_, text=" S4_CH2_W=").grid(row=row_DAC+1+4, column=2, sticky=E)
	Label(sub_dac_, text=" S5_CH2_W=").grid(row=row_DAC+1+5, column=2, sticky=E)
	Label(sub_dac_, text=" S6_CH2_W=").grid(row=row_DAC+1+6, column=2, sticky=E)
	Label(sub_dac_, text=" S7_CH2_W=").grid(row=row_DAC+1+7, column=2, sticky=E)
	Label(sub_dac_, text=" S8_CH2_W=").grid(row=row_DAC+1+8, column=2, sticky=E)
	ee_dac_skt1_ch2_val                     .grid(row=row_DAC+1+1, column=3)
	ee_dac_skt2_ch2_val                     .grid(row=row_DAC+1+2, column=3)
	ee_dac_skt3_ch2_val                     .grid(row=row_DAC+1+3, column=3)
	ee_dac_skt4_ch2_val                     .grid(row=row_DAC+1+4, column=3)
	ee_dac_skt5_ch2_val                     .grid(row=row_DAC+1+5, column=3)
	ee_dac_skt6_ch2_val                     .grid(row=row_DAC+1+6, column=3)
	ee_dac_skt7_ch2_val                     .grid(row=row_DAC+1+7, column=3)
	ee_dac_skt8_ch2_val                     .grid(row=row_DAC+1+8, column=3)
	Label(sub_dac_, text=" S1_CH1_R=").grid(row=row_DAC+1+1, column=4, sticky=E)
	Label(sub_dac_, text=" S2_CH1_R=").grid(row=row_DAC+1+2, column=4, sticky=E)
	Label(sub_dac_, text=" S3_CH1_R=").grid(row=row_DAC+1+3, column=4, sticky=E)
	Label(sub_dac_, text=" S4_CH1_R=").grid(row=row_DAC+1+4, column=4, sticky=E)
	Label(sub_dac_, text=" S5_CH1_R=").grid(row=row_DAC+1+5, column=4, sticky=E)
	Label(sub_dac_, text=" S6_CH1_R=").grid(row=row_DAC+1+6, column=4, sticky=E)
	Label(sub_dac_, text=" S7_CH1_R=").grid(row=row_DAC+1+7, column=4, sticky=E)
	Label(sub_dac_, text=" S8_CH1_R=").grid(row=row_DAC+1+8, column=4, sticky=E)
	ee_dac_skt1_ch1_rdb                     .grid(row=row_DAC+1+1, column=5)
	ee_dac_skt2_ch1_rdb                     .grid(row=row_DAC+1+2, column=5)
	ee_dac_skt3_ch1_rdb                     .grid(row=row_DAC+1+3, column=5)
	ee_dac_skt4_ch1_rdb                     .grid(row=row_DAC+1+4, column=5)
	ee_dac_skt5_ch1_rdb                     .grid(row=row_DAC+1+5, column=5)
	ee_dac_skt6_ch1_rdb                     .grid(row=row_DAC+1+6, column=5)
	ee_dac_skt7_ch1_rdb                     .grid(row=row_DAC+1+7, column=5)
	ee_dac_skt8_ch1_rdb                     .grid(row=row_DAC+1+8, column=5)
	Label(sub_dac_, text=" S1_CH2_R=").grid(row=row_DAC+1+1, column=6, sticky=E)
	Label(sub_dac_, text=" S2_CH2_R=").grid(row=row_DAC+1+2, column=6, sticky=E)
	Label(sub_dac_, text=" S3_CH2_R=").grid(row=row_DAC+1+3, column=6, sticky=E)
	Label(sub_dac_, text=" S4_CH2_R=").grid(row=row_DAC+1+4, column=6, sticky=E)
	Label(sub_dac_, text=" S5_CH2_R=").grid(row=row_DAC+1+5, column=6, sticky=E)
	Label(sub_dac_, text=" S6_CH2_R=").grid(row=row_DAC+1+6, column=6, sticky=E)
	Label(sub_dac_, text=" S7_CH2_R=").grid(row=row_DAC+1+7, column=6, sticky=E)
	Label(sub_dac_, text=" S8_CH2_R=").grid(row=row_DAC+1+8, column=6, sticky=E)
	ee_dac_skt1_ch2_rdb                     .grid(row=row_DAC+1+1, column=7)
	ee_dac_skt2_ch2_rdb                     .grid(row=row_DAC+1+2, column=7)
	ee_dac_skt3_ch2_rdb                     .grid(row=row_DAC+1+3, column=7)
	ee_dac_skt4_ch2_rdb                     .grid(row=row_DAC+1+4, column=7)
	ee_dac_skt5_ch2_rdb                     .grid(row=row_DAC+1+5, column=7)
	ee_dac_skt6_ch2_rdb                     .grid(row=row_DAC+1+6, column=7)
	ee_dac_skt7_ch2_rdb                     .grid(row=row_DAC+1+7, column=7)
	ee_dac_skt8_ch2_rdb                     .grid(row=row_DAC+1+8, column=7)
	# update DAC values
	bb_dac_update = Button(sub_dac_, text='update DAC values', command=dac_bb_update) .grid(row=row_DAC+20, column=0, columnspan = 4, sticky=W+E, pady=4)
	pass

def tk_win_setup__adc_(sub_adc_, row_ADC):
	#### sub_adc_ ####
	Label(sub_adc_, text="===[ADC]===") .grid(row=row_ADC , column= 0, sticky=W, columnspan = 4)
	
	## buttons/labels/entries for ADC
	#
	Label(sub_adc_, text="[ADC] Power control:    ").grid(row=row_ADC+1, column= 0, sticky=W, columnspan = 2)
	Label(sub_adc_, text="[ADC] Control in (HEX): ").grid(row=row_ADC+1, column= 2, sticky=W, columnspan = 2)
	Label(sub_adc_, text="[ADC] Status out (HEX): ").grid(row=row_ADC+1, column= 4, sticky=W, columnspan = 2)
	#
	# [ADC] Power control:
	Checkbutton(sub_adc_, text="ADC POWER ON", variable=var_ADC_PWR_ON)                .grid(row=row_ADC+1+1, column=0, sticky=W)
	Button     (sub_adc_, text="update power control", command=adc_bb_update_pwr_con)  .grid(row=row_ADC+1+1, column=1, columnspan = 1, sticky=W, pady=4)
	#
	# [ADC] Control in:
	ee_adc_con_wi .grid(row=row_ADC+1+1, column=2, sticky=E)
	Button     (sub_adc_, text="update control in", command=adc_bb_update_con_wi)     .grid(row=row_ADC+1+1, column=3, columnspan = 1, sticky=W, pady=4)
	# [ADC] Status out:
	ee_adc_sta_wo .grid(row=row_ADC+1+1, column=4, sticky=E)
	Button     (sub_adc_, text="update status out", command=adc_bb_update_sta_wo)     .grid(row=row_ADC+1+1, column=5, columnspan = 1, sticky=W, pady=4)
	#
	#
	Label(sub_adc_, text="[ADC] Forced mode tests          : ").grid(row=row_ADC+1+2, column= 0, sticky=W, columnspan = 2)
	Label(sub_adc_, text="[ADC] Normal mode tests          : ").grid(row=row_ADC+1+2, column= 2, sticky=W, columnspan = 2)
	Label(sub_adc_, text="[ADC] Last ADC codes (HEX code)  : ").grid(row=row_ADC+1+2, column= 4, sticky=W, columnspan = 2)
	Label(sub_adc_, text="[ADC] Last ADC voltages {CH2,CH1}: ").grid(row=row_ADC+1+2, column= 6, sticky=W, columnspan = 2)
	#
	# [ADC] Forced mode tests:
	Label  (sub_adc_, text="Enable forced mode: ")                                    .grid(row=row_ADC+1+3, column= 0, sticky=W, columnspan = 1)
	Button (sub_adc_, text="enable forced mode  ", command=adc_bb_enable_force_mode)  .grid(row=row_ADC+1+3, column= 1, columnspan = 1, sticky=W, pady=4)
	Label  (sub_adc_, text="Send one CONV pulse:")                                    .grid(row=row_ADC+1+4, column= 0, sticky=W, columnspan = 1)
	Button (sub_adc_, text="trigger a conv pulse", command=adc_bb_trig_conv_pulse)    .grid(row=row_ADC+1+4, column= 1, columnspan = 1, sticky=W, pady=4)
	Label  (sub_adc_, text="Send SCLK pulses:   ")                                    .grid(row=row_ADC+1+5, column= 0, sticky=W, columnspan = 1)
	Button (sub_adc_, text="trigger sclk pulses ", command=adc_bb_trig_sclk_pulses)   .grid(row=row_ADC+1+5, column= 1, columnspan = 1, sticky=W, pady=4)
	Label  (sub_adc_, text="Disable forced mode:")                                    .grid(row=row_ADC+1+6, column= 0, sticky=W, columnspan = 1)
	Button (sub_adc_, text="disable forced mode ", command=adc_bb_disable_force_mode) .grid(row=row_ADC+1+6, column= 1, columnspan = 1, sticky=W, pady=4)
	# 
	# [ADC] Normal mode tests: 
	Label  (sub_adc_, text="Enable ADC control        : ")                            .grid(row=row_ADC+1+3, column= 2, sticky=W, columnspan = 1)
	Button (sub_adc_, text="enable  normal mode ", command=adc_bb_enable_normal_mode ).grid(row=row_ADC+1+3, column= 3, columnspan = 1, sticky=W+E, pady=4)
	Label  (sub_adc_, text="Trigger ADC reset         : ")                            .grid(row=row_ADC+1+4, column= 2, sticky=W, columnspan = 1)
	Button (sub_adc_, text="ADC  reset trigger ", command=adc_bb_trig_reset )         .grid(row=row_ADC+1+4, column= 3, columnspan = 1, sticky=W+E, pady=4)
	Label  (sub_adc_, text="Trigger ADC single measure: ")                            .grid(row=row_ADC+1+5, column= 2, sticky=W, columnspan = 1)
	Button (sub_adc_, text="ADC single trigger ", command=adc_bb_trig_single)         .grid(row=row_ADC+1+5, column= 3, columnspan = 1, sticky=W+E, pady=4)
	#
	Label  (sub_adc_, text="Sampling period (clock counts) : ")               .grid(row=row_ADC+1+6, column= 2, sticky=W, columnspan = 1)
	ee_adc_period_wi                                                          .grid(row=row_ADC+1+6, column= 3)
	Label  (sub_adc_, text="Number of samples to acquire : ")                 .grid(row=row_ADC+1+7, column= 2, sticky=W, columnspan = 1)
	ee_adc_num_samples_wi                                                     .grid(row=row_ADC+1+7, column= 3)
	Label  (sub_adc_, text="Trigger ADC contiguous run: ")                    .grid(row=row_ADC+1+8, column= 2, sticky=W, columnspan = 1)
	Button (sub_adc_, text="ADC    run trigger ", command=adc_bb_trig_run   ) .grid(row=row_ADC+1+8, column= 3, columnspan = 1, sticky=W+E, pady=4)
	
	# set ACC bit shift ...
	Checkbutton(sub_adc_, text="Disable ACC bit-shift        ", variable=var_ADC_ACC_disable    ).grid(row=row_ADC+1+ 9, column=2, sticky=W)
	Checkbutton(sub_adc_, text="Set ACC 16-bit shift or 8-bit", variable=var_ADC_ACC_16bit_shift).grid(row=row_ADC+1+10, column=2, sticky=W)
	Button     (sub_adc_, text="update ACC bit-shift control ", command=adc_bb_update_acc_con)  .grid(row=row_ADC+1+ 9, column=3, sticky=W+E+S+N, pady=4, rowspan = 2) ## rowspan
	
	# switch port 
	Checkbutton(sub_adc_, text="Enable MIN port instead VAL", variable=var_ADC_MIN_ON)    .grid(row=row_ADC+1+11, column=2, sticky=W)
	Checkbutton(sub_adc_, text="Enable MAX port instead ACC", variable=var_ADC_MAX_ON)    .grid(row=row_ADC+1+12, column=2, sticky=W)
	Button     (sub_adc_, text="update ADC port control", command=adc_bb_update_port_con) .grid(row=row_ADC+1+11, column=3, sticky=W+E+S+N, pady=4, rowspan = 2) ## rowspan
	
	# loading fifo
	Button     (sub_adc_, text="________load fifo S1 CH2________", command=partial(adc_bb_load_fifo,skt=1, chn=2) ) .grid(row=row_ADC+1+ 12 + 1, column=2, sticky=W+E+S+N, pady=4, rowspan = 1) 
	Button     (sub_adc_, text="________load fifo S2 CH2________", command=partial(adc_bb_load_fifo,skt=2, chn=2) ) .grid(row=row_ADC+1+ 12 + 2, column=2, sticky=W+E+S+N, pady=4, rowspan = 1) 
	Button     (sub_adc_, text="________load fifo S3 CH2________", command=partial(adc_bb_load_fifo,skt=3, chn=2) ) .grid(row=row_ADC+1+ 12 + 3, column=2, sticky=W+E+S+N, pady=4, rowspan = 1) 
	Button     (sub_adc_, text="________load fifo S4 CH2________", command=partial(adc_bb_load_fifo,skt=4, chn=2) ) .grid(row=row_ADC+1+ 12 + 4, column=2, sticky=W+E+S+N, pady=4, rowspan = 1) 
	Button     (sub_adc_, text="________load fifo S5 CH2________", command=partial(adc_bb_load_fifo,skt=5, chn=2) ) .grid(row=row_ADC+1+ 12 + 5, column=2, sticky=W+E+S+N, pady=4, rowspan = 1) 
	Button     (sub_adc_, text="________load fifo S6 CH2________", command=partial(adc_bb_load_fifo,skt=6, chn=2) ) .grid(row=row_ADC+1+ 12 + 6, column=2, sticky=W+E+S+N, pady=4, rowspan = 1) 
	Button     (sub_adc_, text="________load fifo S7 CH2________", command=partial(adc_bb_load_fifo,skt=7, chn=2) ) .grid(row=row_ADC+1+ 12 + 7, column=2, sticky=W+E+S+N, pady=4, rowspan = 1) 
	Button     (sub_adc_, text="________load fifo S8 CH2________", command=partial(adc_bb_load_fifo,skt=8, chn=2) ) .grid(row=row_ADC+1+ 12 + 8, column=2, sticky=W+E+S+N, pady=4, rowspan = 1) 
	#                                                           
	Button     (sub_adc_, text="________load fifo S1 CH1________", command=partial(adc_bb_load_fifo,skt=1, chn=1) ) .grid(row=row_ADC+1+ 12 + 1, column=3, sticky=W+E+S+N, pady=4, rowspan = 1) 
	Button     (sub_adc_, text="________load fifo S2 CH1________", command=partial(adc_bb_load_fifo,skt=2, chn=1) ) .grid(row=row_ADC+1+ 12 + 2, column=3, sticky=W+E+S+N, pady=4, rowspan = 1) 
	Button     (sub_adc_, text="________load fifo S3 CH1________", command=partial(adc_bb_load_fifo,skt=3, chn=1) ) .grid(row=row_ADC+1+ 12 + 3, column=3, sticky=W+E+S+N, pady=4, rowspan = 1) 
	Button     (sub_adc_, text="________load fifo S4 CH1________", command=partial(adc_bb_load_fifo,skt=4, chn=1) ) .grid(row=row_ADC+1+ 12 + 4, column=3, sticky=W+E+S+N, pady=4, rowspan = 1) 
	Button     (sub_adc_, text="________load fifo S5 CH1________", command=partial(adc_bb_load_fifo,skt=5, chn=1) ) .grid(row=row_ADC+1+ 12 + 5, column=3, sticky=W+E+S+N, pady=4, rowspan = 1) 
	Button     (sub_adc_, text="________load fifo S6 CH1________", command=partial(adc_bb_load_fifo,skt=6, chn=1) ) .grid(row=row_ADC+1+ 12 + 6, column=3, sticky=W+E+S+N, pady=4, rowspan = 1) 
	Button     (sub_adc_, text="________load fifo S7 CH1________", command=partial(adc_bb_load_fifo,skt=7, chn=1) ) .grid(row=row_ADC+1+ 12 + 7, column=3, sticky=W+E+S+N, pady=4, rowspan = 1) 
	Button     (sub_adc_, text="________load fifo S8 CH1________", command=partial(adc_bb_load_fifo,skt=8, chn=1) ) .grid(row=row_ADC+1+ 12 + 8, column=3, sticky=W+E+S+N, pady=4, rowspan = 1) 
	
		
	# [ADC] Read ADC ports (HEX code):
	Label  (sub_adc_, text="ADC_VAL/MIN_S1_WO : ")  .grid(row=row_ADC+1+ 3 , column= 4, sticky=W, columnspan = 1, pady=4)
	Label  (sub_adc_, text="ADC_VAL/MIN_S2_WO : ")  .grid(row=row_ADC+1+ 4 , column= 4, sticky=W, columnspan = 1, pady=4)
	Label  (sub_adc_, text="ADC_VAL/MIN_S3_WO : ")  .grid(row=row_ADC+1+ 5 , column= 4, sticky=W, columnspan = 1, pady=4)
	Label  (sub_adc_, text="ADC_VAL/MIN_S4_WO : ")  .grid(row=row_ADC+1+ 6 , column= 4, sticky=W, columnspan = 1, pady=4)
	Label  (sub_adc_, text="ADC_VAL/MIN_S5_WO : ")  .grid(row=row_ADC+1+ 7 , column= 4, sticky=W, columnspan = 1, pady=4)
	Label  (sub_adc_, text="ADC_VAL/MIN_S6_WO : ")  .grid(row=row_ADC+1+ 8 , column= 4, sticky=W, columnspan = 1, pady=4)
	Label  (sub_adc_, text="ADC_VAL/MIN_S7_WO : ")  .grid(row=row_ADC+1+ 9 , column= 4, sticky=W, columnspan = 1, pady=4)
	Label  (sub_adc_, text="ADC_VAL/MIN_S8_WO : ")  .grid(row=row_ADC+1+10 , column= 4, sticky=W, columnspan = 1, pady=4)
	Label  (sub_adc_, text="ADC_ACC/MAX_S1_WO : ")  .grid(row=row_ADC+1+13 , column= 4, sticky=W, columnspan = 1, pady=4)
	Label  (sub_adc_, text="ADC_ACC/MAX_S2_WO : ")  .grid(row=row_ADC+1+14 , column= 4, sticky=W, columnspan = 1, pady=4)
	Label  (sub_adc_, text="ADC_ACC/MAX_S3_WO : ")  .grid(row=row_ADC+1+15 , column= 4, sticky=W, columnspan = 1, pady=4)
	Label  (sub_adc_, text="ADC_ACC/MAX_S4_WO : ")  .grid(row=row_ADC+1+16 , column= 4, sticky=W, columnspan = 1, pady=4)
	Label  (sub_adc_, text="ADC_ACC/MAX_S5_WO : ")  .grid(row=row_ADC+1+17 , column= 4, sticky=W, columnspan = 1, pady=4)
	Label  (sub_adc_, text="ADC_ACC/MAX_S6_WO : ")  .grid(row=row_ADC+1+18 , column= 4, sticky=W, columnspan = 1, pady=4)
	Label  (sub_adc_, text="ADC_ACC/MAX_S7_WO : ")  .grid(row=row_ADC+1+19 , column= 4, sticky=W, columnspan = 1, pady=4)
	Label  (sub_adc_, text="ADC_ACC/MAX_S8_WO : ")  .grid(row=row_ADC+1+20 , column= 4, sticky=W, columnspan = 1, pady=4)
	#
	ee_adc_s1_wo                                .grid(row=row_ADC+1+ 3 , column= 5, sticky=E, pady=4)
	ee_adc_s2_wo                                .grid(row=row_ADC+1+ 4 , column= 5, sticky=E, pady=4)
	ee_adc_s3_wo                                .grid(row=row_ADC+1+ 5 , column= 5, sticky=E, pady=4)
	ee_adc_s4_wo                                .grid(row=row_ADC+1+ 6 , column= 5, sticky=E, pady=4)
	ee_adc_s5_wo                                .grid(row=row_ADC+1+ 7 , column= 5, sticky=E, pady=4)
	ee_adc_s6_wo                                .grid(row=row_ADC+1+ 8 , column= 5, sticky=E, pady=4)
	ee_adc_s7_wo                                .grid(row=row_ADC+1+ 9 , column= 5, sticky=E, pady=4)
	ee_adc_s8_wo                                .grid(row=row_ADC+1+10 , column= 5, sticky=E, pady=4)
	ee_adc_acc_s1_wo                            .grid(row=row_ADC+1+13 , column= 5, sticky=E, pady=4)
	ee_adc_acc_s2_wo                            .grid(row=row_ADC+1+14 , column= 5, sticky=E, pady=4)
	ee_adc_acc_s3_wo                            .grid(row=row_ADC+1+15 , column= 5, sticky=E, pady=4)
	ee_adc_acc_s4_wo                            .grid(row=row_ADC+1+16 , column= 5, sticky=E, pady=4)
	ee_adc_acc_s5_wo                            .grid(row=row_ADC+1+17 , column= 5, sticky=E, pady=4)
	ee_adc_acc_s6_wo                            .grid(row=row_ADC+1+18 , column= 5, sticky=E, pady=4)
	ee_adc_acc_s7_wo                            .grid(row=row_ADC+1+19 , column= 5, sticky=E, pady=4)
	ee_adc_acc_s8_wo                            .grid(row=row_ADC+1+20 , column= 5, sticky=E, pady=4)
	#
	Button (sub_adc_, text="update ADC code ports", command=adc_bb_update_last_values_wo).grid(row=row_ADC+1+30, column= 4, columnspan = 2, sticky=W+E, pady=4)
	#
	# [ADC] Last ADC values (voltage) :
	ee_adc_volt_s1_ch2     .grid(row=row_ADC+1+ 3 , column= 6, sticky=E)
	ee_adc_volt_s2_ch2     .grid(row=row_ADC+1+ 4 , column= 6, sticky=E)
	ee_adc_volt_s3_ch2     .grid(row=row_ADC+1+ 5 , column= 6, sticky=E)
	ee_adc_volt_s4_ch2     .grid(row=row_ADC+1+ 6 , column= 6, sticky=E)
	ee_adc_volt_s5_ch2     .grid(row=row_ADC+1+ 7 , column= 6, sticky=E)
	ee_adc_volt_s6_ch2     .grid(row=row_ADC+1+ 8 , column= 6, sticky=E)
	ee_adc_volt_s7_ch2     .grid(row=row_ADC+1+ 9 , column= 6, sticky=E)
	ee_adc_volt_s8_ch2     .grid(row=row_ADC+1+10 , column= 6, sticky=E)
	ee_adc_acc_volt_s1_ch2 .grid(row=row_ADC+1+13 , column= 6, sticky=E)
	ee_adc_acc_volt_s2_ch2 .grid(row=row_ADC+1+14 , column= 6, sticky=E)
	ee_adc_acc_volt_s3_ch2 .grid(row=row_ADC+1+15 , column= 6, sticky=E)
	ee_adc_acc_volt_s4_ch2 .grid(row=row_ADC+1+16 , column= 6, sticky=E)
	ee_adc_acc_volt_s5_ch2 .grid(row=row_ADC+1+17 , column= 6, sticky=E)
	ee_adc_acc_volt_s6_ch2 .grid(row=row_ADC+1+18 , column= 6, sticky=E)
	ee_adc_acc_volt_s7_ch2 .grid(row=row_ADC+1+19 , column= 6, sticky=E)
	ee_adc_acc_volt_s8_ch2 .grid(row=row_ADC+1+20 , column= 6, sticky=E)
	#
	ee_adc_volt_s1_ch1     .grid(row=row_ADC+1+ 3 , column= 7, sticky=E)
	ee_adc_volt_s2_ch1     .grid(row=row_ADC+1+ 4 , column= 7, sticky=E)
	ee_adc_volt_s3_ch1     .grid(row=row_ADC+1+ 5 , column= 7, sticky=E)
	ee_adc_volt_s4_ch1     .grid(row=row_ADC+1+ 6 , column= 7, sticky=E)
	ee_adc_volt_s5_ch1     .grid(row=row_ADC+1+ 7 , column= 7, sticky=E)
	ee_adc_volt_s6_ch1     .grid(row=row_ADC+1+ 8 , column= 7, sticky=E)
	ee_adc_volt_s7_ch1     .grid(row=row_ADC+1+ 9 , column= 7, sticky=E)
	ee_adc_volt_s8_ch1     .grid(row=row_ADC+1+10 , column= 7, sticky=E)
	ee_adc_acc_volt_s1_ch1 .grid(row=row_ADC+1+13 , column= 7, sticky=E)
	ee_adc_acc_volt_s2_ch1 .grid(row=row_ADC+1+14 , column= 7, sticky=E)
	ee_adc_acc_volt_s3_ch1 .grid(row=row_ADC+1+15 , column= 7, sticky=E)
	ee_adc_acc_volt_s4_ch1 .grid(row=row_ADC+1+16 , column= 7, sticky=E)
	ee_adc_acc_volt_s5_ch1 .grid(row=row_ADC+1+17 , column= 7, sticky=E)
	ee_adc_acc_volt_s6_ch1 .grid(row=row_ADC+1+18 , column= 7, sticky=E)
	ee_adc_acc_volt_s7_ch1 .grid(row=row_ADC+1+19 , column= 7, sticky=E)
	ee_adc_acc_volt_s8_ch1 .grid(row=row_ADC+1+20 , column= 7, sticky=E)
	#
	Button (sub_adc_, text="convert ADC codes to voltages ", command=adc_bb_conv_code_to_volt).grid(row=row_ADC+1+30, column= 6, columnspan = 2, sticky=W+E, pady=4)
	#
	pass

### EXT-TRIG control ###
def tk_win_setup__trig(sub_trig, row_TRIG):
	#### sub_trig ####
	Label(sub_trig, text="===[EXT-TRIG]===") .grid(row=row_TRIG+0, column= 0, sticky=W, columnspan = 4)
	
	# wire in buttons 
	ee_EXT_TRIG_CON_WI_                                                               .grid(row=row_TRIG+1, column=0,                 sticky=E)
	Button(sub_trig, text="update EXT_TRIG_CON_WI_", command=trig_bb_EXT_TRIG_CON_WI_).grid(row=row_TRIG+1, column=1, columnspan = 1, sticky=W+E, pady=4)
	ee_EXT_TRIG_PARA_WI                                                               .grid(row=row_TRIG+2, column=0,                 sticky=E)
	Button(sub_trig, text="update EXT_TRIG_PARA_WI", command=trig_bb_EXT_TRIG_PARA_WI).grid(row=row_TRIG+2, column=1, columnspan = 1, sticky=W+E, pady=4)
	ee_EXT_TRIG_AUX_WI_                                                               .grid(row=row_TRIG+3, column=0,                 sticky=E)
	Button(sub_trig, text="update EXT_TRIG_AUX_WI_", command=trig_bb_EXT_TRIG_AUX_WI_).grid(row=row_TRIG+3, column=1, columnspan = 1, sticky=W+E, pady=4)
	
	# trig in buttons
	Label (sub_trig, text="Reset         :")                          .grid(row=row_TRIG+1, column=2,  sticky=E)
	Label (sub_trig, text="M_TRIG_SW     :")                          .grid(row=row_TRIG+2, column=2,  sticky=E)
	Label (sub_trig, text="M_PRE_TRIG_SW :")                          .grid(row=row_TRIG+3, column=2,  sticky=E)
	Label (sub_trig, text="AUX_TRIG_SW   :")                          .grid(row=row_TRIG+4, column=2,  sticky=E)
	Button(sub_trig, text="trigger reset_trig___", command=trig_bb_reset_trig___).grid(row=row_TRIG+1, column=3, columnspan = 1, sticky=W+E, pady=4)
	Button(sub_trig, text="trigger sw_m_trig____", command=trig_bb_sw_m_trig____).grid(row=row_TRIG+2, column=3, columnspan = 1, sticky=W+E, pady=4)
	Button(sub_trig, text="trigger sw_m_pre_trig", command=trig_bb_sw_m_pre_trig).grid(row=row_TRIG+3, column=3, columnspan = 1, sticky=W+E, pady=4)
	Button(sub_trig, text="trigger sw_aux_trig__", command=trig_bb_sw_aux_trig__).grid(row=row_TRIG+4, column=3, columnspan = 1, sticky=W+E, pady=4)
	
	# update button for trigger conf
	Button(sub_trig, text="update EXT_TRIG_CONFIG", command=trig_bb_EXT_TRIG_CONFIG___).grid(row=row_TRIG+0, column=4, columnspan = 3, sticky=W+E, pady=4)
	
	# trigger pin enable, but sw triggers are always available.
	Checkbutton(sub_trig, text="Enable EXT-TRIG block      ", variable=var_EXT_TRIG_BLOCK_EN).grid(row=row_TRIG+1, column=4, sticky=W)
	Checkbutton(sub_trig, text="Enable M_TRIG_PIN trig     ", variable=var_M_TRIG_____PIN_EN).grid(row=row_TRIG+2, column=4, sticky=W)
	Checkbutton(sub_trig, text="Enable M_PRE_TRIG_PIN trig ", variable=var_M_PRE_TRIG_PIN_EN).grid(row=row_TRIG+3, column=4, sticky=W)
	Checkbutton(sub_trig, text="Enable AUX_TRIG_PIN trig   ", variable=var_AUX_TRIG___PIN_EN).grid(row=row_TRIG+4, column=4, sticky=W)
	Checkbutton(sub_trig, text="Disable all trig pins      ", variable=var_ALL_TRIGS_PIN_DIS).grid(row=row_TRIG+5, column=4, sticky=W)
	
	# trig connection configuration: {ADC,DAC,SPIO}
	Label (sub_trig, text="PIN/SW trig config to {SPIO,DAC,ADC} ") .grid(row=row_TRIG+1, column=5, sticky=E+W, columnspan = 2)
	#
	Label (sub_trig, text="M_TRIG     connection conf:") .grid(row=row_TRIG+2, column=5, sticky=E)
	Label (sub_trig, text="M_PRE_TRIG connection conf:") .grid(row=row_TRIG+3, column=5, sticky=E)
	Label (sub_trig, text="AUX_TRIG   connection conf:") .grid(row=row_TRIG+4, column=5, sticky=E)
	#
	ee_EXT_TRIG_CONF_M_TRIG                          .grid(row=row_TRIG+2, column=6, sticky=W)
	ee_EXT_TRIG_CONF_M_PRE_TRIG	                     .grid(row=row_TRIG+3, column=6, sticky=W)
	ee_EXT_TRIG_CONF_AUX_TRIG  	                     .grid(row=row_TRIG+4, column=6, sticky=W)

	# update button for trigger delay counts
	Button(sub_trig, text="update EXT_TRIG_DELAY_CNT", command=trig_bb_EXT_TRIG_DELAY_CNT).grid(row=row_TRIG+0, column=7, columnspan = 2, sticky=W+E, pady=4)
	Label (sub_trig, text="10us-step delay counts ")              .grid(row=row_TRIG+1, column=7, columnspan = 2, sticky=W+E)
	#
	Label (sub_trig, text="ADC  delay counts:")     .grid(row=row_TRIG+2, column=7,  sticky=E)
	Label (sub_trig, text="DAC  delay counts:")     .grid(row=row_TRIG+3, column=7,  sticky=E)
	Label (sub_trig, text="SPIO delay counts:")     .grid(row=row_TRIG+4, column=7,  sticky=E)
	#
	ee_EXT_TRIG_ADC__DELAY_CNT                       .grid(row=row_TRIG+2, column=8, sticky=W)
	ee_EXT_TRIG_DAC__DELAY_CNT	                     .grid(row=row_TRIG+3, column=8, sticky=W)
	ee_EXT_TRIG_SPIO_DELAY_CNT	                     .grid(row=row_TRIG+4, column=8, sticky=W)

	pass


def tk_win_setup(master,sub_spio,sub_dac_,sub_adc_):
	
	#### master ####
	
	## title lines
	row_title = 0
	Label(master, text="== MHVSU controls : [SPIO] [DAC] [ADC] == ").grid(row=row_title, column= 0, sticky=W, columnspan = 4)
	
	## ee for wireout registers 
	Label(master, text=" FPGA_IMG# =").grid(row=row_title+1+1, column=0, sticky=E) # (wo20)
	Label(master, text=" TEST_FLAG =").grid(row=row_title+1+2, column=0, sticky=E) # (wo21)
	Label(master, text=" SSPI_FLAG =").grid(row=row_title+1+3, column=0, sticky=E) # (wo22)
	Label(master, text=" MON_XADC  =").grid(row=row_title+1+4, column=0, sticky=E) # (wo23)
	ee_FPGA_IMAGE_ID_                       .grid(row=row_title+1+1, column=1)
	ee_TEST_FLAG_____                       .grid(row=row_title+1+2, column=1)
	ee_Slave_SPI_FLAG                       .grid(row=row_title+1+3, column=1)
	ee_MON_XADC______                       .grid(row=row_title+1+4, column=1)
	#
	Label(master, text=" slot_id      =").grid(row=row_title+1+2, column=2, sticky=E)
	Label(master, text=" board_status =").grid(row=row_title+1+3, column=2, sticky=E)
	Label(master, text=" temp[C](wo23)=").grid(row=row_title+1+4, column=2, sticky=E)
	ee_slot_id_______                    .grid(row=row_title+1+2, column=3)
	ee_board_status__                    .grid(row=row_title+1+3, column=3)
	ee_temp_fpga_____                    .grid(row=row_title+1+4, column=3)
	
	Label(master, text="{TSD_en,CC_en,CLR_sl,SDO_dis}:").grid(row=row_title, column= 4, sticky=W, columnspan = 2)
	#  (TSD_EN,CC_EN,CLR_sel,SDO_DIS)
	Label(master, text=" DAC0 con =").grid(row=row_title+1+1, column=4, sticky=E)
	Label(master, text=" DAC1 con =").grid(row=row_title+1+2, column=4, sticky=E)
	Label(master, text=" DAC2 con =").grid(row=row_title+1+3, column=4, sticky=E)
	Label(master, text=" DAC3 con =").grid(row=row_title+1+4, column=4, sticky=E)
	ee_DAC0_con______                .grid(row=row_title+1+1, column=5)
	ee_DAC1_con______                .grid(row=row_title+1+2, column=5)
	ee_DAC2_con______                .grid(row=row_title+1+3, column=5)
	ee_DAC3_con______                .grid(row=row_title+1+4, column=5)
	
	Label(master, text="{OC_dcba,0,TSD,0,PU_dcba}:").grid(row=row_title, column= 6, sticky=W, columnspan = 2)
	# (OC_DCBA,0,TSD,0,PU_DCBA)
	Label(master, text=" DAC0 flag =").grid(row=row_title+1+1, column=6, sticky=E)
	Label(master, text=" DAC1 flag =").grid(row=row_title+1+2, column=6, sticky=E)
	Label(master, text=" DAC2 flag =").grid(row=row_title+1+3, column=6, sticky=E)
	Label(master, text=" DAC3 flag =").grid(row=row_title+1+4, column=6, sticky=E)
	ee_DAC0_alert____                  .grid(row=row_title+1+1, column=7)
	ee_DAC1_alert____                  .grid(row=row_title+1+2, column=7)
	ee_DAC2_alert____                  .grid(row=row_title+1+3, column=7)
	ee_DAC3_alert____                  .grid(row=row_title+1+4, column=7)
	
	
	## update button for wireout registers
	Button(master, text='update board status', command=master_bb_update_board_status_wo) .grid(row=10,  sticky=W+E, pady=4, columnspan = 8)
	
	
	## buttons common 
	# quit
	bb_quit = Button(master, text='Quit', command=master.quit) .grid(row=100,  sticky=W+E, pady=4, columnspan = 8)


	##################
	
	#### sub_spio ####
	tk_win_setup__spio(sub_spio, row_SPIO=1)
	
	#### sub_dac_ ####
	tk_win_setup__dac_(sub_dac_, row_DAC=1)
	
	#### sub_adc_ ####
	tk_win_setup__adc_(sub_adc_, row_ADC=1)

	#### sub_trig ####
	tk_win_setup__trig(sub_trig, row_TRIG=1)
	
	return
#


########## start test #################

### pre-test ###

##  ## TODO: check board info
##  # test read endpoint 
##  dev.UpdateWireOuts()
##  #
##  wo20_data = dev.GetWireOutValue(0x20)
##  wo21_data = dev.GetWireOutValue(0x21)
##  wo22_data = dev.GetWireOutValue(0x22)
##  wo23_data = dev.GetWireOutValue(0x23)
##  	
##  ## TODO: check SPIO
##  spio_init() # NG??
##  
##  ## TODO: check DAC
##  #dac_init() #
##  
##  ## TODO: check ADC
##  #adc_pwr_on()
##  #adc_bb_trig_single() #
##  adc_pwr_off()
##  
##  input('>>> test done~~')

### some initialization ###

## initialize SPIO 
spio_init()

## test update SPIO 
spio_test_led_on()

####

## initialize DAC 
dac_init() 

## test update DAC
#dac_test()


####
	
## test eeprom
eeprom_test()

#input('stop')

### call windows setup ###

tk_win_setup(master,sub_spio,sub_dac_,sub_adc_)


#### window locations and sizes ####

#master  .geometry('100x100+0+0')
#sub_spio.geometry('100x100+0+100')
#sub_dac_.geometry('100x100+0+200')
#sub_adc_.geometry('100x100+0+300')

#master  .geometry('300x70+0+0')
#sub_spio.geometry('710x470+0+100')
#sub_dac_.geometry('710x270+0+600')
#sub_adc_.geometry('1170x670+720+100')
#sub_trig.geometry('1170x200+720+800')

master  .geometry('710x190+0+0')
sub_spio.geometry('710x470+0+220')
sub_dac_.geometry('710x270+0+720')
#
sub_adc_.geometry('1170x670+720+100')
sub_trig.geometry('1170x200+720+800')


## update board status 
master_bb_update_board_status_wo()

## main loop for gui : until quit()
mainloop() 


### finish test ###

## wait for a while
sleep(0.5)


## test update SPIO 
spio_test_led_off()
spio_close()

## clear DAC
#dac_test()
dac_close()

## disable and power off ADC
adc_disable()
adc_pwr_off()

## OK close
dev.Close()


##



