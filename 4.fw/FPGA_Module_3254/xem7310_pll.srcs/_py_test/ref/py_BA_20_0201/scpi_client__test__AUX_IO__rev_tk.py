## scpi_client__test__AUX_IO__rev_tk.py

import socket
#import time
from time import sleep
import sys

## socket module
# connect(), close() ... style
# https://docs.python.org/3/library/socket.html
# https://docs.python.org/2/library/socket.html

## tkinter GUI module 
# https://www.tutorialspoint.com/python3/python_gui_programming.htm
# https://www.tutorialspoint.com/python3/tk_button.htm
# https://www.tutorialspoint.com/python3/tk_checkbutton.htm
# https://www.python-course.eu/tkinter_checkboxes.php
# https://www.tutorialspoint.com/python3/tk_entry.htm
# https://www.python-course.eu/tkinter_entry_widgets.php ... entry text number...

## control parameters 

HOST = '192.168.168.123'  # The server's hostname or IP address
HOST119 = '192.168.168.119'  # The server's hostname or IP address // firmware test ip
HOST122 = '192.168.168.122'  # The server's hostname or IP address
HOST123 = '192.168.168.123'  # The server's hostname or IP address
HOST124 = '192.168.168.124'  # The server's hostname or IP address
HOST2 = '192.168.172.1'  # The server's hostname or IP address
PORT = 5025               # The port used by the server
#TIMEOUT = 5.3 # socket timeout
TIMEOUT = 1000 # socket timeout // for debug 1000s
SO_SNDBUF = 2048
SO_RCVBUF = 32768
INTVAL = 0.1 # sec for waiting before recv()
BUF_SIZE_NORMAL = 2048
BUF_SIZE_LARGE = 16384
TIMEOUT_LARGE = TIMEOUT*10

ss = None # socket

## command strings ##############################################################
cmd_str__IDN = b'*IDN?\n'
cmd_str__RST = b'*RST\n'
#
cmd_str__PGEP_EN = b':PGEP:EN'
#
cmd_str__PGU_PWR            = b':PGU:PWR'
cmd_str__PGU_OUTP           = b':PGU:OUTP'
cmd_str__PGU_AUX_OUTP       = b':PGU:AUX:OUTP'
cmd_str__PGU_DCS_TRIG       = b':PGU:DCS:TRIG'
cmd_str__PGU_DCS_DAC0_PNT   = b':PGU:DCS:DAC0:PNT'
cmd_str__PGU_DCS_DAC1_PNT   = b':PGU:DCS:DAC1:PNT'
cmd_str__PGU_DCS_RPT        = b':PGU:DCS:RPT'
cmd_str__PGU_FDCS_TRIG      = b':PGU:FDCS:TRIG'
cmd_str__PGU_FDCS_DAC0      = b':PGU:FDCS:DAC0'
cmd_str__PGU_FDCS_DAC1      = b':PGU:FDCS:DAC1'
cmd_str__PGU_FDCS_RPT       = b':PGU:FDCS:RPT'
cmd_str__PGU_FREQ           = b':PGU:FREQ'
cmd_str__PGU_OFST_DAC0      = b':PGU:OFST:DAC0'
cmd_str__PGU_OFST_DAC1      = b':PGU:OFST:DAC1'
cmd_str__PGU_GAIN_DAC0      = b':PGU:GAIN:DAC0'
cmd_str__PGU_GAIN_DAC1      = b':PGU:GAIN:DAC1'
#


## functions ####################################################################

def scpi_open (timeout=TIMEOUT):
	try:
		ss = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		ss.settimeout(timeout)
		ss.setsockopt(socket.SOL_SOCKET, socket.SO_SNDBUF, SO_SNDBUF)
		ss.setsockopt(socket.SOL_SOCKET, socket.SO_RCVBUF, SO_RCVBUF) # 8192 16384 32768 65536
	except OSError as msg:
		ss = None
		print('error in socket: ', msg)
		raise
	return ss

def scpi_connect (ss, HOST, PORT):
	try:
		ss.connect((HOST, PORT))
	except OSError as msg:
		ss.close()
		ss = None
		print('error in connect: ', msg)
		raise


def scpi_close (ss):
	try:
		ss.close()
	except:
		if ss == None:
			print('error: ss==None')
		raise

def scpi_comm_resp_ss (ss, cmd_str, buf_size=BUF_SIZE_NORMAL, intval=INTVAL) :
	try:
		print('Send:', repr(cmd_str))
		ss.sendall(cmd_str)
	except:
		print('error in sendall')
		raise
	##
	sleep(intval)
	#
	# recv data until finding the sentinel '\n'
	try:
		data = ss.recv(buf_size) # try 1024 131072 524288
		# try   
		while (1):
			if (chr(data[-1])=='\n'): # check the sentinel '\n' 
				break
			data = data + ss.recv(buf_size)
	except:
		print('error in recv')
		raise
	#
	## check response 
	if (len(data)>20):
		print('Received:', repr(data[0:20]),  ' (first 20 bytes)')
	else:
		print('Received:', repr(data))
	#
	# NG response check 
	if data[0:2]==b'NG':
		print('Received: NG as response')
		#input('Press Enter key!')
		#sleep(3)
	#
	return data


# scpi command for numeric block response
def scpi_comm_resp_numb_ss (ss, cmd_str, buf_size=BUF_SIZE_LARGE, intval=INTVAL, timeout_large=TIMEOUT_LARGE) :
	try:
		print('Send:', repr(cmd_str))
		ss.sendall(cmd_str)
	except:
		print('error in sendall')
		raise
	##
	sleep(intval)
	#
	# cmd: ":PGEP:PO#HBC 524288\n"
	# rsp: "#4_001024_rrrrrrrrrr...rrrrrrrrrr\n"
	#
	# recv data until finding the sentinel '\n' 
	# but check the sentinel after the data byte count is met.
	#
	# read timeout
	to = ss.gettimeout()
	#print(to)
	# increase timeout
	ss.settimeout(timeout_large)
	#
	try:
		# find the numeric head : must 10 in data 
		data = ss.recv(buf_size)
		while True:
			if len(data)>=10:
				break
			data = data + ss.recv(buf_size)
		#
		#print('header: ', repr(data[0:10])) # header
		#
		# find byte count 
		byte_count = int(data[3:9])
		#print('byte_count=', repr(byte_count)) 
		#
		# collect all data by byte count
		count_to_recv = byte_count + 10 + 1# add header count #add /n
		while True:
			if len(data)>=count_to_recv:
				break
			data = data + ss.recv(buf_size)
		#
		# check the sentinel 
		while True:
			if (chr(data[-1])=='\n'): # check the sentinel '\n' 
				break
			data = data + ss.recv(buf_size)
		#
	except:
		print('error in recv')
		raise
	#
	if (len(data)>20):
		print('Received:', repr(data[0:20]),  ' (first 20 bytes)')
	else:
		print('Received:', repr(data))
	#
	# timeout back to prev
	ss.settimeout(to)
	#
	data = data[10:(10+byte_count)]
	print('data:', data[0:20].hex(),  ' (first 20 bytes)')
	#
	return [byte_count, data]
	
	

## generate pulse info for FDCS command

def conv_dec_to_bit_2s_comp_16bit(dec, full_scale=20):
	if dec > full_scale/2-full_scale/2**16 :
		dec = full_scale/2-full_scale/2**16
	if dec < -full_scale/2-full_scale/2**16 :
		dec = -full_scale/2-full_scale/2**16
	bit_2s_comp = int( 0x10000 * ( dec + full_scale/2)    / full_scale ) + 0x8000
	if bit_2s_comp > 0xFFFF :
		bit_2s_comp -= 0x10000
	return bit_2s_comp

def gen_pulse_info_num_block__inc_step(code_start, code_step, num_steps, code_duration):
	# num_steps : including last step
	#
	pulse_info_num_block_str = b''
	#
	num_codes = num_steps
	#
	# set number of bytes to send : 8 bytes * (number of codes)
	pulse_info_num_block_str += ' #N_{:06d}'.format(num_codes*8).encode()
	#
	code_value = code_start
	#
	for ii in range(num_codes):
		# merge dac code and duration
		test_value = (code_value<<16) + code_duration
		# convert into text bytes
		test_str = '_{:08X}'.format(test_value).encode()
		# append it to numberic block
		pulse_info_num_block_str += test_str
		#
		# update next code 
		code_value += code_step
		if code_value > 0xFFFF:
			code_value -= 0x10000
		#
	#
	# add sentinel
	pulse_info_num_block_str += b' \n'
	#
	return pulse_info_num_block_str


## test tk ########################

#### Button
##  from tkinter import *
##  
##  from tkinter import messagebox
##  
##  top = Tk()
##  top.geometry("100x100")
##  def helloCallBack():
##     msg = messagebox.showinfo( "Hello Python", "Hello World")
##  
##  bb = Button(top, text = "Hello", command = helloCallBack)
##  bb.place(x = 50,y = 50)
##  top.mainloop()




## test parameters #########################################################################
# TODO: USER VAR


## test routines ###########################################################################


## open socket and connect scpi server ####################################################

## try:
## 	ss = scpi_open()
## 	scpi_connect(ss, HOST, PORT)
## except socket.timeout:
## 	ss = scpi_open()
## 	scpi_connect(ss, HOST2, PORT)
## except:
## 	raise

def my_open(host, port):
	#
	ss = scpi_open()
	try:
		print('>> try to connect : {}:{}'.format(host, port))
		scpi_connect(ss, host, port)
	except socket.timeout:
		ss = None
	except:
		raise
	return ss

# TODO: IP setup

##  # firmware test
##  ss = my_open(HOST119,PORT) # firmware test
##  if ss == None :
##  	raise

# browse ip
ss = my_open(HOST122,PORT)
if ss == None:	ss = my_open(HOST123,PORT)
if ss == None:	ss = my_open(HOST124,PORT)
if ss == None:	raise


###########################################################################

### scpi : *IDN?
print('\n>>> {} : {}'.format('Test',cmd_str__IDN))
rsp = scpi_comm_resp_ss(ss, cmd_str__IDN)
print('hex code rcvd: ' + rsp.hex())
print('string rcvd: ' + repr(rsp))


### :PGEP:EN
# :PGEP:EN ON|OFF <NL>			
# :PGEP:EN? <NL>			
#
# ":PGEP:EN ON\n"
# ":PGEP:EN OFF\n"
# ":PGEP:EN?"
print('\n>>> {} : {}'.format('Test',cmd_str__PGEP_EN))
scpi_comm_resp_ss(ss, cmd_str__PGEP_EN+b' ON\n')
scpi_comm_resp_ss(ss, cmd_str__PGEP_EN+b'?\n')


### scpi command: ":PGU:PWR"
# :PGU:PWR ON|OFF <NL>			
# :PGU:PWR? <NL>			
#
# ":PGU:PWR ON\n"
# ":PGU:PWR OFF\n"
# ":PGU:PWR?"
#

### power on 
print('\n>>> {} : {}'.format('Test',cmd_str__PGU_PWR))
scpi_comm_resp_ss(ss, cmd_str__PGU_PWR+b'?\n')
scpi_comm_resp_ss(ss, cmd_str__PGU_PWR+b' ON\n')
scpi_comm_resp_ss(ss, cmd_str__PGU_PWR+b'?\n')

### scpi command: ":PGU:OUTP" #### // TODO: cmd_str__PGU_OUTP
# :PGU:OUTP ON|OFF <NL>			
# :PGU:OUTP? <NL>			
#
# ":PGU:OUTP ON\n"
# ":PGU:OUTP OFF\n"
# ":PGU:OUTP?"
#

### output on or off
print('\n>>> {} : {}'.format('Test',cmd_str__PGU_OUTP))
scpi_comm_resp_ss(ss, cmd_str__PGU_OUTP+b'?\n')
#scpi_comm_resp_ss(ss, cmd_str__PGU_OUTP+b' ON\n')
scpi_comm_resp_ss(ss, cmd_str__PGU_OUTP+b' OFF\n')
scpi_comm_resp_ss(ss, cmd_str__PGU_OUTP+b'?\n')

###########################################################################

## 
print('>>> Use tk GUI control to send AUX IO command.')


#### Checkbutton
##  from tkinter import *
##  
##  import tkinter
##      
##  top = Tk()
##  CheckVar1 = IntVar()
##  CheckVar2 = IntVar()
##  C1 = Checkbutton(top, text = "Music", variable = CheckVar1, \
##                   onvalue = 1, offvalue = 0, height=5, \
##                   width = 20, )
##  C2 = Checkbutton(top, text = "Video", variable = CheckVar2, \
##                   onvalue = 1, offvalue = 0, height=5, \
##                   width = 20)
##  C1.pack()
##  C2.pack()
##  top.mainloop()

#### Checkbutton and Button
from tkinter import *
master = Tk()
#
def write_aux_io():
	#print("\n male: %d,\nfemale: %d" % (var1.get(), var2.get()))
	print('\n>>>>>>')
	print('{} = {}'.format('var_ch2_gain__', var_ch2_gain__.get()))
	print('{} = {}'.format('var_ch1_gain__', var_ch1_gain__.get()))
	print('{} = {}'.format('var_ch2_be____', var_ch2_be____.get()))
	print('{} = {}'.format('var_ch2_fe____', var_ch2_fe____.get()))
	print('{} = {}'.format('var_ch1_be____', var_ch1_be____.get()))
	print('{} = {}'.format('var_ch1_fe____', var_ch1_fe____.get()))
	print('{} = {}'.format('var_ch2_sleepn', var_ch2_sleepn.get()))
	print('{} = {}'.format('var_ch1_sleepn', var_ch1_sleepn.get()))
	#
	# generate control frame 
	# 
	# var_ch2_gain__ = 0x8000
	# var_ch1_gain__ = 0x4000
	# var_ch2_be____ = 0x2000
	# var_ch2_fe____ = 0x1000
	# var_ch1_be____ = 0x0800
	# var_ch1_fe____ = 0x0400
	# var_ch2_sleepn = 0x0200
	# var_ch1_sleepn = 0x0100
	#
	para_ctrl = 0x0000
	if var_ch2_gain__.get()==1:	para_ctrl += 0x8000
	if var_ch1_gain__.get()==1:	para_ctrl += 0x4000
	if var_ch2_be____.get()==1:	para_ctrl += 0x2000
	if var_ch2_fe____.get()==1:	para_ctrl += 0x1000
	if var_ch1_be____.get()==1:	para_ctrl += 0x0800
	if var_ch1_fe____.get()==1:	para_ctrl += 0x0400
	if var_ch2_sleepn.get()==1:	para_ctrl += 0x0200
	if var_ch1_sleepn.get()==1:	para_ctrl += 0x0100
	#
	para_ctrl_str = '#H{:04X}'.format(para_ctrl).encode()
	print('{} = {}'.format('para_ctrl_str', para_ctrl_str))
	#
	# send command 
	scpi_comm_resp_ss(ss, cmd_str__PGU_AUX_OUTP+ b' ' +para_ctrl_str + b'\n')
#
def read_aux_io():
	print('\n>>>>>>')
	rsp = scpi_comm_resp_ss(ss, cmd_str__PGU_AUX_OUTP+ b'?\n')
	#print('hex code rcvd: ' + rsp.hex())	
	#print('rsp: ' + rsp.decode())
	print('rsp: ' + rsp.decode()[2:6])
	#
	rsp_hexa = int(rsp.decode()[2:6],base=16)
	print( 'rsp_hexa : 0x{:04X} '.format(rsp_hexa) )
	#
	var_ch2_gain__.set(1) if rsp_hexa&0x8000 != 0 else var_ch2_gain__.set(0)
	var_ch1_gain__.set(1) if rsp_hexa&0x4000 != 0 else var_ch1_gain__.set(0)
	var_ch2_be____.set(1) if rsp_hexa&0x2000 != 0 else var_ch2_be____.set(0)
	var_ch2_fe____.set(1) if rsp_hexa&0x1000 != 0 else var_ch2_fe____.set(0)
	var_ch1_be____.set(1) if rsp_hexa&0x0800 != 0 else var_ch1_be____.set(0)
	var_ch1_fe____.set(1) if rsp_hexa&0x0400 != 0 else var_ch1_fe____.set(0)
	var_ch2_sleepn.set(1) if rsp_hexa&0x0200 != 0 else var_ch2_sleepn.set(0)
	var_ch1_sleepn.set(1) if rsp_hexa&0x0100 != 0 else var_ch1_sleepn.set(0)
#
#
Label(master, text="AUX IO:").grid(row=0, sticky=W)
#
var_ch2_gain__ = IntVar()
var_ch1_gain__ = IntVar()
var_ch2_be____ = IntVar()
var_ch2_fe____ = IntVar()
var_ch1_be____ = IntVar()
var_ch1_fe____ = IntVar()
var_ch2_sleepn = IntVar()
var_ch1_sleepn = IntVar()
#
# must update aux io 
read_aux_io()
#
Checkbutton(master, text="var_ch2_gain__", variable=var_ch2_gain__).grid(row=1, sticky=W)
Checkbutton(master, text="var_ch1_gain__", variable=var_ch1_gain__).grid(row=2, sticky=W)
Checkbutton(master, text="var_ch2_be____", variable=var_ch2_be____).grid(row=3, sticky=W)
Checkbutton(master, text="var_ch2_fe____", variable=var_ch2_fe____).grid(row=4, sticky=W)
Checkbutton(master, text="var_ch1_be____", variable=var_ch1_be____).grid(row=5, sticky=W)
Checkbutton(master, text="var_ch1_fe____", variable=var_ch1_fe____).grid(row=6, sticky=W)
Checkbutton(master, text="var_ch2_sleepn", variable=var_ch2_sleepn).grid(row=7, sticky=W)
Checkbutton(master, text="var_ch1_sleepn", variable=var_ch1_sleepn).grid(row=8, sticky=W)
#
Button(master, text='Quit        ', command=master.quit).grid(row=9,  sticky=W, pady=4)
Button(master, text='Send control', command=write_aux_io ).grid(row=10, sticky=W, pady=4)
Button(master, text='Read control', command=read_aux_io).grid(row=11, sticky=W, pady=4)
#
mainloop() ####


################################################################################

## wait for an input  ######################################################
#input('Enter any key to test AUX IO')

### // TODO: cmd_str__PGU_AUX_OUTP
## # cmd_str__PGU_AUX_OUTP       = b':PGU:AUX:OUTP'
## print('\n>>> {} : {}'.format('Test',cmd_str__PGU_AUX_OUTP))
## scpi_comm_resp_ss(ss, cmd_str__PGU_AUX_OUTP+b'?\n')
## #scpi_comm_resp_ss(ss, cmd_str__PGU_AUX_OUTP+b' #H0300 \n') # for both sleep 2/1 ON // safe state
## #scpi_comm_resp_ss(ss, cmd_str__PGU_AUX_OUTP+b' #H0700 \n') # for both sleep 2/1 ON, ch1_fe_con ON
## #scpi_comm_resp_ss(ss, cmd_str__PGU_AUX_OUTP+b' #H0B00 \n') # for both sleep 2/1 ON, ch1_be_con ON
## #scpi_comm_resp_ss(ss, cmd_str__PGU_AUX_OUTP+b' #H1300 \n') # for both sleep 2/1 ON, ch2_fe_con ON
## #scpi_comm_resp_ss(ss, cmd_str__PGU_AUX_OUTP+b' #H2300 \n') # for both sleep 2/1 ON, ch2_be_con ON
## scpi_comm_resp_ss(ss, cmd_str__PGU_AUX_OUTP+b' #H4300 \n') # for both sleep 2/1 ON, ch1_gain_con ON 
## #scpi_comm_resp_ss(ss, cmd_str__PGU_AUX_OUTP+b' #H8300 \n') # for both sleep 2/1 ON, ch2_gain_con ON
## #scpi_comm_resp_ss(ss, cmd_str__PGU_AUX_OUTP+b' #H0000 \n') # for all off
## scpi_comm_resp_ss(ss, cmd_str__PGU_AUX_OUTP+b'?\n')
## scpi_comm_resp_ss(ss, cmd_str__PGU_AUX_OUTP+b' #H0300 \n') # for both sleep 2/1 ON // safe state
## scpi_comm_resp_ss(ss, cmd_str__PGU_AUX_OUTP+b'?\n')


################################################################################

## wait for an input  ######################################################
#input('Enter any key to stop')


################################################################################

## wait for a while
sleep(1.0)

### power off 
print('\n>>> {} : {}'.format('Test',cmd_str__PGU_PWR))
scpi_comm_resp_ss(ss, cmd_str__PGU_PWR+b' OFF\n')
scpi_comm_resp_ss(ss, cmd_str__PGU_PWR+b'?\n')



## PGEP disable
# print('\n>>> {} : {}'.format('Test',cmd_str__PGEP_EN))
# scpi_comm_resp_ss(ss, cmd_str__PGEP_EN+b' OFF\n')
# scpi_comm_resp_ss(ss, cmd_str__PGEP_EN+b'?\n')


## close socket
scpi_close(ss)


##



