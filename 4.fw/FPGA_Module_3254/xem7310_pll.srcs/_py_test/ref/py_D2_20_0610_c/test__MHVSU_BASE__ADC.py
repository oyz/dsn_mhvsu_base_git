## test__MHVSU_BASE__ADC.py
# ADC test to add 
#
# SPIO / DAC controls

# rev 6/9 note: txt change ... ADC_IM --> ADC_VM

from time import sleep
import sys


## tkinter GUI module 
# https://www.tutorialspoint.com/python3/python_gui_programming.htm
# https://www.tutorialspoint.com/python3/tk_button.htm
# https://www.tutorialspoint.com/python3/tk_checkbutton.htm
# https://www.python-course.eu/tkinter_checkboxes.php
# https://www.tutorialspoint.com/python3/tk_entry.htm
# https://www.python-course.eu/tkinter_entry_widgets.php ... entry text number...



## common converter

def conv_dec_to_bit_2s_comp_16bit(dec, full_scale=20):
	if dec > full_scale/2-full_scale/2**16 :
		dec = full_scale/2-full_scale/2**16
	if dec < -full_scale/2-full_scale/2**16 :
		dec = -full_scale/2-full_scale/2**16
	#bit_2s_comp = int( 0x10000 * ( dec + full_scale/2)    / full_scale ) + 0x8000
	bit_2s_comp = int( 0x10000 * ( dec + full_scale/2)    / full_scale +0.5) + 0x8000
	if bit_2s_comp > 0xFFFF :
		bit_2s_comp -= 0x10000
	return bit_2s_comp

#test_codes = [ conv_dec_to_bit_2s_comp_16bit(x) for x in [-10,-5,0,5,10] ]
#print(test_codes)
	
def conv_bit_2s_comp_16bit_to_dec(bit_2s_comp, full_scale=20):
	if bit_2s_comp >= 0x8000:
		bit_2s_comp -= 0x8000
		#dec = full_scale * (bit_2s_comp) / 0x10000 -10
		dec = full_scale * (bit_2s_comp) / 0x10000 - full_scale/2
	else :
		dec = full_scale * (bit_2s_comp) / 0x10000
		if dec == full_scale/2-full_scale/2**16 :
			dec = full_scale/2
	return dec

#test_codes2 = [ conv_bit_2s_comp_16bit_to_dec(x) for x in test_codes ]
#print(test_codes2)

	


###########################################################################
## open OK USB device  ####################################################

import ok

# OK init
dev = ok.okCFrontPanel()

# OK open
DeviceCount = dev.GetDeviceCount() # dev.GetCount()
print(' {}={}'.format('DeviceCount',DeviceCount))
dev.OpenBySerial("") # dev.Open()
DevSR = dev.GetSerialNumber()
print(' {}={}'.format('DevSR',DevSR))

## see https://library.opalkelly.com/library/FrontPanelAPI/structokTDeviceInfo.html
## see https://opalkelly.com/examples/open-two-devices/#tab-python

# OK check device 
infoDev = ok.okTDeviceInfo();
dev.GetDeviceInfo(infoDev);
#
DevDSR = infoDev.serialNumber
DevDID = infoDev.deviceID
DevPNM = infoDev.productName
DevPID = infoDev.productID
print(' {}={}'.format('DevDSR',DevDSR))
print(' {}={}'.format('DevDID',DevDID))
print(' {}={}'.format('DevPNM',DevPNM))
print(' {}={}'.format('DevPID',DevPID))
#
if dev.IsOpen():
	print('>>> device opened.')
else:
	print('>>> device is not open.')
	## raise
	input('')


# check MHVSU FPGA image ID 
__FPGA_IMG_IMP_INFO__ = 0xD220 # adc test
#__FPGA_IMG_IMP_DATE__ = 0x0604
__FPGA_IMG_IMP_DATE__ = 0x0610
#
dev.UpdateWireOuts()
FPGA_image_id = dev.GetWireOutValue(0x20)
print('{} = 0x{:08X}'.format('FPGA_image_id', FPGA_image_id))
# configure FPGA
if (FPGA_image_id==0): 
	BIT_FILENAME = './img_h_D2_20_0604/xem7310__mhvsu_base__top.bit'
	error = dev.ConfigureFPGA(BIT_FILENAME)
	if error !=0:
		print('ConfigureFPGA Error {} : {}'.format(error,dev.GetErrorString(error)))
		## raise
		input('')
	# read again
	dev.UpdateWireOuts()
	FPGA_image_id = dev.GetWireOutValue(0x20)
	print('{} = 0x{:08X}'.format('FPGA_image_id', FPGA_image_id))
#
if not (FPGA_image_id>>16 == __FPGA_IMG_IMP_INFO__):
	print('>>> Please check FPGA iamge ID : ADC test.')
	## raise
	input('')	
#
if not (FPGA_image_id & 0xFFFF == __FPGA_IMG_IMP_DATE__):
	print('>>> Please check FPGA iamge ID : implmentation date.')
	## raise
	input('')


## flag check max retry count
MAX_count = 20 # 20000

###########################################################################

## 
print('>>> tk GUI control')


#### GUI: Checkbutton and Button
from tkinter import *
from functools import partial

master = Tk()


## gui variables for SPIO
# socket selections
# output nets

var_SKT1_EN = IntVar(value=1)  #SKT1 ENABLE
var_SKT2_EN = IntVar(value=1)  #SKT2 ENABLE
var_SKT3_EN = IntVar(value=1)  #SKT3 ENABLE
var_SKT4_EN = IntVar(value=1)  #SKT4 ENABLE
var_SKT5_EN = IntVar(value=1)  #SKT5 ENABLE
var_SKT6_EN = IntVar(value=1)  #SKT6 ENABLE
var_SKT7_EN = IntVar(value=1)  #SKT7 ENABLE
var_SKT8_EN = IntVar(value=1)  #SKT8 ENABLE

var_CS0_GPB7 = IntVar()  #CS0 GPB7 CHECK_LED0
var_CS0_GPB6 = IntVar()  #CS0 GPB6 CH2_ADC_VM
var_CS0_GPB5 = IntVar()  #CS0 GPB5 CH2_GAIN_X20
var_CS0_GPB4 = IntVar()  #CS0 GPB4 CH2_GAIN_X2
var_CS0_GPB3 = IntVar()  #CS0 GPB3 NA
var_CS0_GPB2 = IntVar()  #CS0 GPB2 CH2_RANGE_20uA
var_CS0_GPB1 = IntVar()  #CS0 GPB1 CH2_RANGE_2mA
var_CS0_GPB0 = IntVar()  #CS0 GPB0 CH2_RANGE_50mA
var_CS0_GPA7 = IntVar()  #CS0 GPA7 NA
var_CS0_GPA6 = IntVar()  #CS0 GPA6 CH1_ADC_VM
var_CS0_GPA5 = IntVar()  #CS0 GPA5 CH1_GAIN_X20
var_CS0_GPA4 = IntVar()  #CS0 GPA4 CH1_GAIN_X2
var_CS0_GPA3 = IntVar()  #CS0 GPA3 NA
var_CS0_GPA2 = IntVar()  #CS0 GPA2 CH1_RANGE_20uA
var_CS0_GPA1 = IntVar()  #CS0 GPA1 CH1_RANGE_2mA
var_CS0_GPA0 = IntVar()  #CS0 GPA0 CH1_RANGE_50mA

var_CS1_GPB7 = IntVar()  #CS1 GPB7 CHECK_LED1
var_CS1_GPB6 = IntVar()  #CS1 GPB6 CH2_RELAY_DIAG
var_CS1_GPB5 = IntVar()  #CS1 GPB5 CH2_RELAY_OUTPUT
var_CS1_GPB4 = IntVar()  #CS1 GPB4 CH2_LIMIT_50mA
var_CS1_GPB3 = IntVar()  #CS1 GPB3 NA
var_CS1_GPB2 = IntVar()  #CS1 GPB2 NA
var_CS1_GPB1 = IntVar()  #CS1 GPB1 CH2_CMP_330p
var_CS1_GPB0 = IntVar()  #CS1 GPB0 CH2_CMP_100p
var_CS1_GPA7 = IntVar()  #CS1 GPA7 NA
var_CS1_GPA6 = IntVar()  #CS1 GPA6 CH1_RELAY_DIAG
var_CS1_GPA5 = IntVar()  #CS1 GPA5 CH1_RELAY_OUTPUT
var_CS1_GPA4 = IntVar()  #CS1 GPA4 CH1_LIMIT_50mA
var_CS1_GPA3 = IntVar()  #CS1 GPA3 NA
var_CS1_GPA2 = IntVar()  #CS1 GPA2 NA
var_CS1_GPA1 = IntVar()  #CS1 GPA1 CH1_CMP_330p
var_CS1_GPA0 = IntVar()  #CS1 GPA0 CH1_CMP_100p

var_CS2_GPB7 = IntVar()  #CS2 GPB7
var_CS2_GPB6 = IntVar()  #CS2 GPB6
var_CS2_GPB5 = IntVar()  #CS2 GPB5
var_CS2_GPB4 = IntVar()  #CS2 GPB4
var_CS2_GPB3 = IntVar()  #CS2 GPB3
var_CS2_GPB2 = IntVar()  #CS2 GPB2
var_CS2_GPB1 = IntVar()  #CS2 GPB1
var_CS2_GPB0 = IntVar()  #CS2 GPB0
var_CS2_GPA7 = IntVar()  #CS2 GPA7
var_CS2_GPA6 = IntVar()  #CS2 GPA6
var_CS2_GPA5 = IntVar()  #CS2 GPA5
var_CS2_GPA4 = IntVar()  #CS2 GPA4
var_CS2_GPA3 = IntVar()  #CS2 GPA3
var_CS2_GPA2 = IntVar()  #CS2 GPA2
var_CS2_GPA1 = IntVar()  #CS2 GPA1
var_CS2_GPA0 = IntVar()  #CS2 GPA0

## gui variables for DAC
# DAC value input 
# DAC readback

ee_dac_skt1_ch1_val = Entry(master, justify='right') 
ee_dac_skt2_ch1_val = Entry(master, justify='right') 
ee_dac_skt3_ch1_val = Entry(master, justify='right') 
ee_dac_skt4_ch1_val = Entry(master, justify='right') 
ee_dac_skt5_ch1_val = Entry(master, justify='right') 
ee_dac_skt6_ch1_val = Entry(master, justify='right') 
ee_dac_skt7_ch1_val = Entry(master, justify='right') 
ee_dac_skt8_ch1_val = Entry(master, justify='right') 
ee_dac_skt1_ch2_val = Entry(master, justify='right') 
ee_dac_skt2_ch2_val = Entry(master, justify='right') 
ee_dac_skt3_ch2_val = Entry(master, justify='right') 
ee_dac_skt4_ch2_val = Entry(master, justify='right') 
ee_dac_skt5_ch2_val = Entry(master, justify='right') 
ee_dac_skt6_ch2_val = Entry(master, justify='right') 
ee_dac_skt7_ch2_val = Entry(master, justify='right') 
ee_dac_skt8_ch2_val = Entry(master, justify='right') 

ee_dac_skt1_ch1_rdb = Entry(master, justify='right')  
ee_dac_skt2_ch1_rdb = Entry(master, justify='right')  
ee_dac_skt3_ch1_rdb = Entry(master, justify='right')  
ee_dac_skt4_ch1_rdb = Entry(master, justify='right')  
ee_dac_skt5_ch1_rdb = Entry(master, justify='right')  
ee_dac_skt6_ch1_rdb = Entry(master, justify='right')  
ee_dac_skt7_ch1_rdb = Entry(master, justify='right')  
ee_dac_skt8_ch1_rdb = Entry(master, justify='right')  
ee_dac_skt1_ch2_rdb = Entry(master, justify='right') 
ee_dac_skt2_ch2_rdb = Entry(master, justify='right')  
ee_dac_skt3_ch2_rdb = Entry(master, justify='right')  
ee_dac_skt4_ch2_rdb = Entry(master, justify='right')  
ee_dac_skt5_ch2_rdb = Entry(master, justify='right')  
ee_dac_skt6_ch2_rdb = Entry(master, justify='right')  
ee_dac_skt7_ch2_rdb = Entry(master, justify='right')  
ee_dac_skt8_ch2_rdb = Entry(master, justify='right')  

ee_dac_skt1_ch1_val.insert(0,"0")
ee_dac_skt2_ch1_val.insert(0,"0")
ee_dac_skt3_ch1_val.insert(0,"0")
ee_dac_skt4_ch1_val.insert(0,"0")
ee_dac_skt5_ch1_val.insert(0,"0")
ee_dac_skt6_ch1_val.insert(0,"0")
ee_dac_skt7_ch1_val.insert(0,"0")
ee_dac_skt8_ch1_val.insert(0,"0")
ee_dac_skt1_ch2_val.insert(0,"0")
ee_dac_skt2_ch2_val.insert(0,"0")
ee_dac_skt3_ch2_val.insert(0,"0")
ee_dac_skt4_ch2_val.insert(0,"0")
ee_dac_skt5_ch2_val.insert(0,"0")
ee_dac_skt6_ch2_val.insert(0,"0")
ee_dac_skt7_ch2_val.insert(0,"0")
ee_dac_skt8_ch2_val.insert(0,"0")

ee_dac_skt1_ch1_rdb.insert(0,"0")
ee_dac_skt2_ch1_rdb.insert(0,"0")
ee_dac_skt3_ch1_rdb.insert(0,"0")
ee_dac_skt4_ch1_rdb.insert(0,"0")
ee_dac_skt5_ch1_rdb.insert(0,"0")
ee_dac_skt6_ch1_rdb.insert(0,"0")
ee_dac_skt7_ch1_rdb.insert(0,"0")
ee_dac_skt8_ch1_rdb.insert(0,"0")
ee_dac_skt1_ch2_rdb.insert(0,"0")
ee_dac_skt2_ch2_rdb.insert(0,"0")
ee_dac_skt3_ch2_rdb.insert(0,"0")
ee_dac_skt4_ch2_rdb.insert(0,"0")
ee_dac_skt5_ch2_rdb.insert(0,"0")
ee_dac_skt6_ch2_rdb.insert(0,"0")
ee_dac_skt7_ch2_rdb.insert(0,"0")
ee_dac_skt8_ch2_rdb.insert(0,"0")


## gui variables for ADC
var_ADC_PWR_ON = IntVar(value=1) # ADC power on 
#
ee_adc_con_wi = Entry(master, justify='right') # ADC control in 
ee_adc_con_wi.insert(0,"0x00000000")
#
ee_adc_sta_wo = Entry(master, justify='right') # ADC status out
ee_adc_sta_wo.insert(0,"0x00000000")
#
ee_adc_period_wi = Entry(master, justify='right') # ADC period in 
ee_adc_period_wi.insert(0,"21000")
#
ee_adc_num_samples_wi = Entry(master, justify='right') # ADC number of samples in 
ee_adc_num_samples_wi.insert(0,"168")
#
# ADC codes : packed as ch2,ch1
ee_adc_s1_wo = Entry(master, justify='right') 
ee_adc_s2_wo = Entry(master, justify='right') 
ee_adc_s3_wo = Entry(master, justify='right') 
ee_adc_s4_wo = Entry(master, justify='right') 
ee_adc_s5_wo = Entry(master, justify='right') 
ee_adc_s6_wo = Entry(master, justify='right') 
ee_adc_s7_wo = Entry(master, justify='right') 
ee_adc_s8_wo = Entry(master, justify='right') 
#
ee_adc_s1_wo.insert(0,"0x00000000")
ee_adc_s2_wo.insert(0,"0x00000000")
ee_adc_s3_wo.insert(0,"0x00000000")
ee_adc_s4_wo.insert(0,"0x00000000")
ee_adc_s5_wo.insert(0,"0x00000000")
ee_adc_s6_wo.insert(0,"0x00000000")
ee_adc_s7_wo.insert(0,"0x00000000")
ee_adc_s8_wo.insert(0,"0x00000000")
#
# ADC values (voltage)
ee_adc_volt_s1_ch2 = Entry(master, justify='right'); ee_adc_volt_s1_ch1 = Entry(master, justify='right')
ee_adc_volt_s2_ch2 = Entry(master, justify='right'); ee_adc_volt_s2_ch1 = Entry(master, justify='right')
ee_adc_volt_s3_ch2 = Entry(master, justify='right'); ee_adc_volt_s3_ch1 = Entry(master, justify='right')
ee_adc_volt_s4_ch2 = Entry(master, justify='right'); ee_adc_volt_s4_ch1 = Entry(master, justify='right')
ee_adc_volt_s5_ch2 = Entry(master, justify='right'); ee_adc_volt_s5_ch1 = Entry(master, justify='right')
ee_adc_volt_s6_ch2 = Entry(master, justify='right'); ee_adc_volt_s6_ch1 = Entry(master, justify='right')
ee_adc_volt_s7_ch2 = Entry(master, justify='right'); ee_adc_volt_s7_ch1 = Entry(master, justify='right')
ee_adc_volt_s8_ch2 = Entry(master, justify='right'); ee_adc_volt_s8_ch1 = Entry(master, justify='right')
#
ee_adc_volt_s1_ch2.insert(0,"0"); ee_adc_volt_s1_ch1.insert(0,"0")
ee_adc_volt_s2_ch2.insert(0,"0"); ee_adc_volt_s2_ch1.insert(0,"0")
ee_adc_volt_s3_ch2.insert(0,"0"); ee_adc_volt_s3_ch1.insert(0,"0")
ee_adc_volt_s4_ch2.insert(0,"0"); ee_adc_volt_s4_ch1.insert(0,"0")
ee_adc_volt_s5_ch2.insert(0,"0"); ee_adc_volt_s5_ch1.insert(0,"0")
ee_adc_volt_s6_ch2.insert(0,"0"); ee_adc_volt_s6_ch1.insert(0,"0")
ee_adc_volt_s7_ch2.insert(0,"0"); ee_adc_volt_s7_ch1.insert(0,"0")
ee_adc_volt_s8_ch2.insert(0,"0"); ee_adc_volt_s8_ch1.insert(0,"0")

## MHVSU SPIO functions 

def spio_init():
	print('\n>>>>>> spio_init')
	# ep05 = 0xFF070001 // w_SPIO_CON_WI // socket/cs enable 
	dev.SetWireInValue(0x05,0xFF070001,0xFFFFFFFF) # (ep,val,mask)
	dev.UpdateWireIns()
	# ep04 = 0x0000FF7F // w_SPIO_FDAT_WI // set IO direction // led only
	# ep04 = 0x00000000 // w_SPIO_FDAT_WI // set IO direction // all outputs
	dev.SetWireInValue(0x04,0x00000000,0xFFFFFFFF) # (ep,val,mask)
	dev.UpdateWireIns()
	# ep45[1] // w_SPIO_TRIG_TI // init trig
	dev.ActivateTriggerIn(0x45, 1)
	# ep65[1] // w_SPIO_TRIG_TO; // init done
	cnt_loop = 0;
	while 1:
		# First, query all XEM Trigger Outs.
		dev.UpdateTriggerOuts();
		# check trigger out
		if dev.IsTriggered(0x65, 0x02) == True: # // (ep, mask)
			break
		cnt_loop += 1;
		print('{} = {}'.format('cnt_loop', cnt_loop))
		if (cnt_loop>MAX_count):
			break
	print('{} = {}'.format('cnt_loop', cnt_loop))

def spio_close():
	print('\n>>>>>> spio_close')
	# ep05 = 0xFF070001 // w_SPIO_CON_WI // socket/cs enable 
	dev.SetWireInValue(0x05,0x00000000,0xFFFFFFFF) # (ep,val,mask)
	dev.UpdateWireIns()

def spio_test_led_on():
	print('\n>>>>>> spio_test_led_on')
	# ep05 = 0xFF070001 // w_SPIO_CON_WI // socket/cs enable // all selected
	dev.SetWireInValue(0x05,0xFF070001,0xFFFFFFFF) # (ep,val,mask)
	dev.UpdateWireIns()
	# ep04 = 0x00120080 // {GPA,GPB}
	dev.SetWireInValue(0x04,0x00120080,0xFFFFFFFF) # (ep,val,mask)
	dev.UpdateWireIns()
	# ep45[1]
	dev.ActivateTriggerIn(0x45, 1)
	# ep65[1] // w_SPIO_TRIG_TO; // init done
	cnt_loop = 0;
	while 1:
		# First, query all XEM Trigger Outs.
		dev.UpdateTriggerOuts();
		# check trigger out
		if dev.IsTriggered(0x65, 0x02) == True: # // (ep, mask)
			break
		cnt_loop += 1;
		print('{} = {}'.format('cnt_loop', cnt_loop))
		if (cnt_loop>MAX_count):
			break
	print('{} = {}'.format('cnt_loop', cnt_loop))

def spio_test_led_off():
	print('\n>>>>>> spio_test_led_off')
	# ep05 = 0xFF070001 // w_SPIO_CON_WI // socket/cs enable // all selected
	dev.SetWireInValue(0x05,0xFF070001,0xFFFFFFFF) # (ep,val,mask)
	dev.UpdateWireIns()
	# ep04 = 0x00120080 // {GPA,GPB}
	dev.SetWireInValue(0x04,0x00120000,0xFFFFFFFF) # (ep,val,mask)
	dev.UpdateWireIns()
	# ep45[1]
	dev.ActivateTriggerIn(0x45, 1)
	# ep65[1] // w_SPIO_TRIG_TO; // init done
	cnt_loop = 0;
	while 1:
		# First, query all XEM Trigger Outs.
		dev.UpdateTriggerOuts();
		# check trigger out
		if dev.IsTriggered(0x65, 0x02) == True: # // (ep, mask)
			break
		cnt_loop += 1;
		print('{} = {}'.format('cnt_loop', cnt_loop))
		if (cnt_loop>MAX_count):
			break
	print('{} = {}'.format('cnt_loop', cnt_loop))

def spio_bb_update_cs0():
	print('\n>>>>>> spio_bb_update_cs0')
	## read check box
	print('\n>>>>>>')
	# read socket enable data
	print('{} = {}'.format('var_SKT1_EN', var_SKT1_EN.get()))
	print('{} = {}'.format('var_SKT2_EN', var_SKT2_EN.get()))
	print('{} = {}'.format('var_SKT3_EN', var_SKT3_EN.get()))
	print('{} = {}'.format('var_SKT4_EN', var_SKT4_EN.get()))
	print('{} = {}'.format('var_SKT5_EN', var_SKT5_EN.get()))
	print('{} = {}'.format('var_SKT6_EN', var_SKT6_EN.get()))
	print('{} = {}'.format('var_SKT7_EN', var_SKT7_EN.get()))
	print('{} = {}'.format('var_SKT8_EN', var_SKT8_EN.get()))
	# read io data
	print('{} = {}'.format('var_CS0_GPB7', var_CS0_GPB7.get()))
	print('{} = {}'.format('var_CS0_GPB6', var_CS0_GPB6.get()))
	print('{} = {}'.format('var_CS0_GPB5', var_CS0_GPB5.get()))
	print('{} = {}'.format('var_CS0_GPB4', var_CS0_GPB4.get()))
	print('{} = {}'.format('var_CS0_GPB3', var_CS0_GPB3.get()))
	print('{} = {}'.format('var_CS0_GPB2', var_CS0_GPB2.get()))
	print('{} = {}'.format('var_CS0_GPB1', var_CS0_GPB1.get()))
	print('{} = {}'.format('var_CS0_GPB0', var_CS0_GPB0.get()))
	print('{} = {}'.format('var_CS0_GPA7', var_CS0_GPA7.get()))
	print('{} = {}'.format('var_CS0_GPA6', var_CS0_GPA6.get()))
	print('{} = {}'.format('var_CS0_GPA5', var_CS0_GPA5.get()))
	print('{} = {}'.format('var_CS0_GPA4', var_CS0_GPA4.get()))
	print('{} = {}'.format('var_CS0_GPA3', var_CS0_GPA3.get()))
	print('{} = {}'.format('var_CS0_GPA2', var_CS0_GPA2.get()))
	print('{} = {}'.format('var_CS0_GPA1', var_CS0_GPA1.get()))
	print('{} = {}'.format('var_CS0_GPA0', var_CS0_GPA0.get()))
	#
	socket_en_data = 0x00
	if var_SKT1_EN.get()==1:	socket_en_data += 0x01
	if var_SKT2_EN.get()==1:	socket_en_data += 0x02
	if var_SKT3_EN.get()==1:	socket_en_data += 0x04
	if var_SKT4_EN.get()==1:	socket_en_data += 0x08
	if var_SKT5_EN.get()==1:	socket_en_data += 0x10
	if var_SKT6_EN.get()==1:	socket_en_data += 0x20
	if var_SKT7_EN.get()==1:	socket_en_data += 0x40
	if var_SKT8_EN.get()==1:	socket_en_data += 0x80
	#
	out_data = 0x0000
	if var_CS0_GPB7.get()==1:	out_data += 0x0080
	if var_CS0_GPB6.get()==1:	out_data += 0x0040
	if var_CS0_GPB5.get()==1:	out_data += 0x0020
	if var_CS0_GPB4.get()==1:	out_data += 0x0010
	if var_CS0_GPB3.get()==1:	out_data += 0x0008
	if var_CS0_GPB2.get()==1:	out_data += 0x0004
	if var_CS0_GPB1.get()==1:	out_data += 0x0002
	if var_CS0_GPB0.get()==1:	out_data += 0x0001
	if var_CS0_GPA7.get()==1:	out_data += 0x8000
	if var_CS0_GPA6.get()==1:	out_data += 0x4000
	if var_CS0_GPA5.get()==1:	out_data += 0x2000
	if var_CS0_GPA4.get()==1:	out_data += 0x1000
	if var_CS0_GPA3.get()==1:	out_data += 0x0800
	if var_CS0_GPA2.get()==1:	out_data += 0x0400
	if var_CS0_GPA1.get()==1:	out_data += 0x0200
	if var_CS0_GPA0.get()==1:	out_data += 0x0100
	
	## send SPIO send frame
	# set socket/cs 
	# ep05 = 0xFF070001 // w_SPIO_CON_WI // socket/cs enable 
	cs_en_data = 0x01 # for CS0
	wire_data = 0
	wire_data = (socket_en_data<<24) + (cs_en_data<<16) + 1
	dev.SetWireInValue(0x05,wire_data,0xFFFFFFFF) # (ep,val,mask)
	dev.UpdateWireIns()
	# make frame data
	reg_adrs = 0x0012
	frame_data = 0
	frame_data = (reg_adrs<<16) + out_data
	# ep04 = 0x00120080 // {GPA,GPB}
	dev.SetWireInValue(0x04,frame_data,0xFFFFFFFF) # (ep,val,mask)
	dev.UpdateWireIns()
	# ep45[1]
	dev.ActivateTriggerIn(0x45, 1)
	# ep65[1] // w_SPIO_TRIG_TO; // init done
	cnt_loop = 0;
	while 1:
		# First, query all XEM Trigger Outs.
		dev.UpdateTriggerOuts();
		# check trigger out
		if dev.IsTriggered(0x65, 0x02) == True: # // (ep, mask)
			break
		cnt_loop += 1;
		print('{} = {}'.format('cnt_loop', cnt_loop))
		if (cnt_loop>MAX_count):
			break
	print('{} = {}'.format('cnt_loop', cnt_loop))
	return

def spio_bb_update_cs1():
	print('\n>>>>>> spio_bb_update_cs1')
	## read check box
	print('\n>>>>>>')
	# read socket enable data
	print('{} = {}'.format('var_SKT1_EN', var_SKT1_EN.get()))
	print('{} = {}'.format('var_SKT2_EN', var_SKT2_EN.get()))
	print('{} = {}'.format('var_SKT3_EN', var_SKT3_EN.get()))
	print('{} = {}'.format('var_SKT4_EN', var_SKT4_EN.get()))
	print('{} = {}'.format('var_SKT5_EN', var_SKT5_EN.get()))
	print('{} = {}'.format('var_SKT6_EN', var_SKT6_EN.get()))
	print('{} = {}'.format('var_SKT7_EN', var_SKT7_EN.get()))
	print('{} = {}'.format('var_SKT8_EN', var_SKT8_EN.get()))
	# read io data
	print('{} = {}'.format('var_CS1_GPB7', var_CS1_GPB7.get()))
	print('{} = {}'.format('var_CS1_GPB6', var_CS1_GPB6.get()))
	print('{} = {}'.format('var_CS1_GPB5', var_CS1_GPB5.get()))
	print('{} = {}'.format('var_CS1_GPB4', var_CS1_GPB4.get()))
	print('{} = {}'.format('var_CS1_GPB3', var_CS1_GPB3.get()))
	print('{} = {}'.format('var_CS1_GPB2', var_CS1_GPB2.get()))
	print('{} = {}'.format('var_CS1_GPB1', var_CS1_GPB1.get()))
	print('{} = {}'.format('var_CS1_GPB0', var_CS1_GPB0.get()))
	print('{} = {}'.format('var_CS1_GPA7', var_CS1_GPA7.get()))
	print('{} = {}'.format('var_CS1_GPA6', var_CS1_GPA6.get()))
	print('{} = {}'.format('var_CS1_GPA5', var_CS1_GPA5.get()))
	print('{} = {}'.format('var_CS1_GPA4', var_CS1_GPA4.get()))
	print('{} = {}'.format('var_CS1_GPA3', var_CS1_GPA3.get()))
	print('{} = {}'.format('var_CS1_GPA2', var_CS1_GPA2.get()))
	print('{} = {}'.format('var_CS1_GPA1', var_CS1_GPA1.get()))
	print('{} = {}'.format('var_CS1_GPA0', var_CS1_GPA0.get()))
	#
	socket_en_data = 0x00
	if var_SKT1_EN.get()==1:	socket_en_data += 0x01
	if var_SKT2_EN.get()==1:	socket_en_data += 0x02
	if var_SKT3_EN.get()==1:	socket_en_data += 0x04
	if var_SKT4_EN.get()==1:	socket_en_data += 0x08
	if var_SKT5_EN.get()==1:	socket_en_data += 0x10
	if var_SKT6_EN.get()==1:	socket_en_data += 0x20
	if var_SKT7_EN.get()==1:	socket_en_data += 0x40
	if var_SKT8_EN.get()==1:	socket_en_data += 0x80
	#
	out_data = 0x0000
	if var_CS1_GPB7.get()==1:	out_data += 0x0080
	if var_CS1_GPB6.get()==1:	out_data += 0x0040
	if var_CS1_GPB5.get()==1:	out_data += 0x0020
	if var_CS1_GPB4.get()==1:	out_data += 0x0010
	if var_CS1_GPB3.get()==1:	out_data += 0x0008
	if var_CS1_GPB2.get()==1:	out_data += 0x0004
	if var_CS1_GPB1.get()==1:	out_data += 0x0002
	if var_CS1_GPB0.get()==1:	out_data += 0x0001
	if var_CS1_GPA7.get()==1:	out_data += 0x8000
	if var_CS1_GPA6.get()==1:	out_data += 0x4000
	if var_CS1_GPA5.get()==1:	out_data += 0x2000
	if var_CS1_GPA4.get()==1:	out_data += 0x1000
	if var_CS1_GPA3.get()==1:	out_data += 0x0800
	if var_CS1_GPA2.get()==1:	out_data += 0x0400
	if var_CS1_GPA1.get()==1:	out_data += 0x0200
	if var_CS1_GPA0.get()==1:	out_data += 0x0100
	
	## send SPIO send frame
	# set socket/cs 
	# ep05 = 0xFF070001 // w_SPIO_CON_WI // socket/cs enable 
	cs_en_data = 0x02 # for CS1
	wire_data = 0
	wire_data = (socket_en_data<<24) + (cs_en_data<<16) + 1
	dev.SetWireInValue(0x05,wire_data,0xFFFFFFFF) # (ep,val,mask)
	dev.UpdateWireIns()
	# make frame data
	reg_adrs = 0x0012
	frame_data = 0
	frame_data = (reg_adrs<<16) + out_data
	# ep04 = 0x00120080 // {GPA,GPB}
	dev.SetWireInValue(0x04,frame_data,0xFFFFFFFF) # (ep,val,mask)
	dev.UpdateWireIns()
	# ep45[1]
	dev.ActivateTriggerIn(0x45, 1)
	# ep65[1] // w_SPIO_TRIG_TO; // init done
	cnt_loop = 0;
	while 1:
		# First, query all XEM Trigger Outs.
		dev.UpdateTriggerOuts();
		# check trigger out
		if dev.IsTriggered(0x65, 0x02) == True: # // (ep, mask)
			break
		cnt_loop += 1;
		print('{} = {}'.format('cnt_loop', cnt_loop))
		if (cnt_loop>MAX_count):
			break
	print('{} = {}'.format('cnt_loop', cnt_loop))
	return

def spio_bb_update_cs2():
	pass


## MHVSU DAC functions 

def dac_init():
	print('\n>>>>>> dac_init')
	# ep06 = 0x000000F1 // w_DAC_CON_WI // dac enable // all dac0/1/2/3 enabled
	dev.SetWireInValue(0x06,0x000000F1,0xFFFFFFFF) # (ep,val,mask)
	dev.UpdateWireIns()
	
	# clear trig out
	#dev.UpdateTriggerOuts();
	
	# ep46[2] // w_DAC_TRIG_TI // initialize
	dev.ActivateTriggerIn(0x46, 2)
	#
	# ep66[2] // w_DAC_TRIG_TO // init done check
	cnt_loop = 0;
	while 1:
		# First, query all XEM Trigger Outs.
		dev.UpdateTriggerOuts();
		# check trigger out
		if dev.IsTriggered(0x66, 0x04) == True: # // (ep, mask)
			break
		cnt_loop += 1;
		print('{} = {}'.format('cnt_loop', cnt_loop))
		if (cnt_loop>MAX_count):
			break
	print('{} = {}'.format('cnt_loop', cnt_loop))
	pass

def dac_close():
	print('\n>>>>>> dac_close')
	# ep06 = 0x000000F1 // w_DAC_CON_WI // dac enable // all dac0/1/2/3 enabled
	dev.SetWireInValue(0x06,0x00000000,0xFFFFFFFF) # (ep,val,mask)
	dev.UpdateWireIns()

def dac_test():
	print('\n>>>>>> dac_test')
	## test write on S3
	# ep1A = 0x000A0002 // w_DAC_S3_WI // dac data
	set_data = 0x000A0002
	dev.SetWireInValue(0x1A,set_data,0xFFFFFFFF) # (ep,val,mask)
	dev.UpdateWireIns()	
	# ep46[3] // w_DAC_TRIG_TI // update
	dev.ActivateTriggerIn(0x46, 3)
	# ep66[3] // w_DAC_TRIG_TO // update done check
	cnt_loop = 0;
	while 1:
		# First, query all XEM Trigger Outs.
		dev.UpdateTriggerOuts();
		# check trigger out
		if dev.IsTriggered(0x66, 0x08) == True: # // (ep, mask)
			break
		cnt_loop += 1;
		print('{} = {}'.format('cnt_loop', cnt_loop))
		if (cnt_loop>MAX_count):
			break
	print('{} = {}'.format('cnt_loop', cnt_loop))
	
	## test readback from S3
	# ep3A // w_DAC_S3_WO // dac readback
	dev.UpdateWireOuts()
	readback = dev.GetWireOutValue(0x3A)
	#
	print('{} = 0x{:08X}'.format('set_data', set_data))
	print('{} = 0x{:08X}'.format('readback', readback))
	# must ... set_data = readback
	
	## return to '0'
	# ep1A = 0x00000000 // w_DAC_S3_WI // dac data
	set_data = 0x00000000
	dev.SetWireInValue(0x18,set_data,0xFFFFFFFF) # (ep,val,mask)
	dev.SetWireInValue(0x19,set_data,0xFFFFFFFF) # (ep,val,mask)
	dev.SetWireInValue(0x1A,set_data,0xFFFFFFFF) # (ep,val,mask)
	dev.SetWireInValue(0x1B,set_data,0xFFFFFFFF) # (ep,val,mask)
	dev.SetWireInValue(0x1C,set_data,0xFFFFFFFF) # (ep,val,mask)
	dev.SetWireInValue(0x1D,set_data,0xFFFFFFFF) # (ep,val,mask)
	dev.SetWireInValue(0x1E,set_data,0xFFFFFFFF) # (ep,val,mask)
	dev.SetWireInValue(0x1F,set_data,0xFFFFFFFF) # (ep,val,mask)
	dev.UpdateWireIns()	
	# ep46[3] // w_DAC_TRIG_TI // update
	dev.ActivateTriggerIn(0x46, 3)
	# ep66[3] // w_DAC_TRIG_TO // update done check
	cnt_loop = 0;
	while 1:
		# First, query all XEM Trigger Outs.
		dev.UpdateTriggerOuts();
		# check trigger out
		if dev.IsTriggered(0x66, 0x08) == True: # // (ep, mask)
			break
		cnt_loop += 1;
		print('{} = {}'.format('cnt_loop', cnt_loop))
		if (cnt_loop>MAX_count):
			break
	print('{} = {}'.format('cnt_loop', cnt_loop))
	#
	pass


# TODO: dac error scale 
#__dac_scale__ = 0.632 # 1V --> 0.632V measure
__dac_scale__ = 1.0 # no scale

def conv_dac_code__from_string(tmp_ee_dac_skt1_ch1_val):
	tmp_ee_dac_skt1_ch1_val__float = float(tmp_ee_dac_skt1_ch1_val)/__dac_scale__
	##
	#if tmp_ee_dac_skt1_ch1_val__float >= 0:
	#	var_SKT1_CH1_DAC_VAL__code = int(tmp_ee_dac_skt1_ch1_val__float/10.0*0x7FFF)
	#else:
	#	var_SKT1_CH1_DAC_VAL__code = 0xFFFF # test
	##
	var_SKT1_CH1_DAC_VAL__code = conv_dec_to_bit_2s_comp_16bit(tmp_ee_dac_skt1_ch1_val__float)
	#
	return var_SKT1_CH1_DAC_VAL__code

def  conv_string__from_dac_code(var_SKT1_CH1_DAC_RDB__code):
	tmp = var_SKT1_CH1_DAC_RDB__code
	tmp_ee_dac_skt1_ch1_rdb = conv_bit_2s_comp_16bit_to_dec(tmp)*__dac_scale__
	#
	return tmp_ee_dac_skt1_ch1_rdb

def dac_bb_update():
	print('\n>>>>>> dac_bb_update')
	## read dac input entry // ee_dac_skt3_ch1_val
	tmp_ee_dac_skt1_ch1_val = ee_dac_skt1_ch1_val.get() # string
	tmp_ee_dac_skt2_ch1_val = ee_dac_skt2_ch1_val.get() # string
	tmp_ee_dac_skt3_ch1_val = ee_dac_skt3_ch1_val.get() # string
	tmp_ee_dac_skt4_ch1_val = ee_dac_skt4_ch1_val.get() # string
	tmp_ee_dac_skt5_ch1_val = ee_dac_skt5_ch1_val.get() # string
	tmp_ee_dac_skt6_ch1_val = ee_dac_skt6_ch1_val.get() # string
	tmp_ee_dac_skt7_ch1_val = ee_dac_skt7_ch1_val.get() # string
	tmp_ee_dac_skt8_ch1_val = ee_dac_skt8_ch1_val.get() # string
	tmp_ee_dac_skt1_ch2_val = ee_dac_skt1_ch2_val.get() # string
	tmp_ee_dac_skt2_ch2_val = ee_dac_skt2_ch2_val.get() # string
	tmp_ee_dac_skt3_ch2_val = ee_dac_skt3_ch2_val.get() # string
	tmp_ee_dac_skt4_ch2_val = ee_dac_skt4_ch2_val.get() # string
	tmp_ee_dac_skt5_ch2_val = ee_dac_skt5_ch2_val.get() # string
	tmp_ee_dac_skt6_ch2_val = ee_dac_skt6_ch2_val.get() # string
	tmp_ee_dac_skt7_ch2_val = ee_dac_skt7_ch2_val.get() # string
	tmp_ee_dac_skt8_ch2_val = ee_dac_skt8_ch2_val.get() # string
	
	## convert hex/int code from string
	var_SKT1_CH1_DAC_VAL__code = conv_dac_code__from_string(tmp_ee_dac_skt1_ch1_val)
	var_SKT2_CH1_DAC_VAL__code = conv_dac_code__from_string(tmp_ee_dac_skt2_ch1_val)
	var_SKT3_CH1_DAC_VAL__code = conv_dac_code__from_string(tmp_ee_dac_skt3_ch1_val)
	var_SKT4_CH1_DAC_VAL__code = conv_dac_code__from_string(tmp_ee_dac_skt4_ch1_val)
	var_SKT5_CH1_DAC_VAL__code = conv_dac_code__from_string(tmp_ee_dac_skt5_ch1_val)
	var_SKT6_CH1_DAC_VAL__code = conv_dac_code__from_string(tmp_ee_dac_skt6_ch1_val)
	var_SKT7_CH1_DAC_VAL__code = conv_dac_code__from_string(tmp_ee_dac_skt7_ch1_val)
	var_SKT8_CH1_DAC_VAL__code = conv_dac_code__from_string(tmp_ee_dac_skt8_ch1_val)
	var_SKT1_CH2_DAC_VAL__code = conv_dac_code__from_string(tmp_ee_dac_skt1_ch2_val)
	var_SKT2_CH2_DAC_VAL__code = conv_dac_code__from_string(tmp_ee_dac_skt2_ch2_val)
	var_SKT3_CH2_DAC_VAL__code = conv_dac_code__from_string(tmp_ee_dac_skt3_ch2_val)
	var_SKT4_CH2_DAC_VAL__code = conv_dac_code__from_string(tmp_ee_dac_skt4_ch2_val)
	var_SKT5_CH2_DAC_VAL__code = conv_dac_code__from_string(tmp_ee_dac_skt5_ch2_val)
	var_SKT6_CH2_DAC_VAL__code = conv_dac_code__from_string(tmp_ee_dac_skt6_ch2_val)
	var_SKT7_CH2_DAC_VAL__code = conv_dac_code__from_string(tmp_ee_dac_skt7_ch2_val)
	var_SKT8_CH2_DAC_VAL__code = conv_dac_code__from_string(tmp_ee_dac_skt8_ch2_val)
	
	## set wire_in 
	set_data_S1 = (var_SKT1_CH2_DAC_VAL__code<<16) + var_SKT1_CH1_DAC_VAL__code # 0 # 0x00000001 # // w_DAC_S1_WI  = ep18wire;
	set_data_S2 = (var_SKT2_CH2_DAC_VAL__code<<16) + var_SKT2_CH1_DAC_VAL__code # 0 # 0x00000002 # // w_DAC_S2_WI  = ep19wire;
	set_data_S3 = (var_SKT3_CH2_DAC_VAL__code<<16) + var_SKT3_CH1_DAC_VAL__code # 0 # 0x00000003 # // w_DAC_S3_WI  = ep1Awire;
	set_data_S4 = (var_SKT4_CH2_DAC_VAL__code<<16) + var_SKT4_CH1_DAC_VAL__code # 0 # 0x00000004 # // w_DAC_S4_WI  = ep1Bwire;
	set_data_S5 = (var_SKT5_CH2_DAC_VAL__code<<16) + var_SKT5_CH1_DAC_VAL__code # 0 # 0x00000005 # // w_DAC_S5_WI  = ep1Cwire;
	set_data_S6 = (var_SKT6_CH2_DAC_VAL__code<<16) + var_SKT6_CH1_DAC_VAL__code # 0 # 0x00000006 # // w_DAC_S6_WI  = ep1Dwire;
	set_data_S7 = (var_SKT7_CH2_DAC_VAL__code<<16) + var_SKT7_CH1_DAC_VAL__code # 0 # 0x00000007 # // w_DAC_S7_WI  = ep1Ewire;
	set_data_S8 = (var_SKT8_CH2_DAC_VAL__code<<16) + var_SKT8_CH1_DAC_VAL__code # 0 # 0x00000008 # // w_DAC_S8_WI  = ep1Fwire;
	dev.SetWireInValue(0x18,set_data_S1,0xFFFFFFFF) # (ep,val,mask)
	dev.SetWireInValue(0x19,set_data_S2,0xFFFFFFFF) # (ep,val,mask)
	dev.SetWireInValue(0x1A,set_data_S3,0xFFFFFFFF) # (ep,val,mask)
	dev.SetWireInValue(0x1B,set_data_S4,0xFFFFFFFF) # (ep,val,mask)
	dev.SetWireInValue(0x1C,set_data_S5,0xFFFFFFFF) # (ep,val,mask)
	dev.SetWireInValue(0x1D,set_data_S6,0xFFFFFFFF) # (ep,val,mask)
	dev.SetWireInValue(0x1E,set_data_S7,0xFFFFFFFF) # (ep,val,mask)
	dev.SetWireInValue(0x1F,set_data_S8,0xFFFFFFFF) # (ep,val,mask)
	dev.UpdateWireIns()	
	
	## trig update 
	# ep46[3] // w_DAC_TRIG_TI // update
	dev.ActivateTriggerIn(0x46, 3)
	# ep66[3] // w_DAC_TRIG_TO // update done check
	cnt_loop = 0;
	while 1:
		# First, query all XEM Trigger Outs.
		dev.UpdateTriggerOuts();
		# check trigger out
		if dev.IsTriggered(0x66, 0x08) == True: # // (ep, mask)
			break
		cnt_loop += 1;
		print('{} = {}'.format('cnt_loop', cnt_loop))
		if (cnt_loop>MAX_count):
			break
	print('{} = {}'.format('cnt_loop', cnt_loop))
	
	## readback wire_out
	dev.UpdateWireOuts()
	readback_S1 = dev.GetWireOutValue(0x38) # // ep38wire = w_DAC_S1_WO 
	readback_S2 = dev.GetWireOutValue(0x39) # // ep39wire = w_DAC_S2_WO 
	readback_S3 = dev.GetWireOutValue(0x3A) # // ep3Awire = w_DAC_S3_WO 
	readback_S4 = dev.GetWireOutValue(0x3B) # // ep3Bwire = w_DAC_S4_WO 
	readback_S5 = dev.GetWireOutValue(0x3C) # // ep3Cwire = w_DAC_S5_WO 
	readback_S6 = dev.GetWireOutValue(0x3D) # // ep3Dwire = w_DAC_S6_WO 
	readback_S7 = dev.GetWireOutValue(0x3E) # // ep3Ewire = w_DAC_S7_WO 
	readback_S8 = dev.GetWireOutValue(0x3F) # // ep3Fwire = w_DAC_S8_WO 
	
	## readback codes
	var_SKT1_CH1_DAC_RDB__code = (readback_S1 >>  0) & 0x0000FFFF
	var_SKT2_CH1_DAC_RDB__code = (readback_S2 >>  0) & 0x0000FFFF
	var_SKT3_CH1_DAC_RDB__code = (readback_S3 >>  0) & 0x0000FFFF
	var_SKT4_CH1_DAC_RDB__code = (readback_S4 >>  0) & 0x0000FFFF
	var_SKT5_CH1_DAC_RDB__code = (readback_S5 >>  0) & 0x0000FFFF
	var_SKT6_CH1_DAC_RDB__code = (readback_S6 >>  0) & 0x0000FFFF
	var_SKT7_CH1_DAC_RDB__code = (readback_S7 >>  0) & 0x0000FFFF
	var_SKT8_CH1_DAC_RDB__code = (readback_S8 >>  0) & 0x0000FFFF
	var_SKT1_CH2_DAC_RDB__code = (readback_S1 >> 16) & 0x0000FFFF
	var_SKT2_CH2_DAC_RDB__code = (readback_S2 >> 16) & 0x0000FFFF
	var_SKT3_CH2_DAC_RDB__code = (readback_S3 >> 16) & 0x0000FFFF
	var_SKT4_CH2_DAC_RDB__code = (readback_S4 >> 16) & 0x0000FFFF
	var_SKT5_CH2_DAC_RDB__code = (readback_S5 >> 16) & 0x0000FFFF
	var_SKT6_CH2_DAC_RDB__code = (readback_S6 >> 16) & 0x0000FFFF
	var_SKT7_CH2_DAC_RDB__code = (readback_S7 >> 16) & 0x0000FFFF
	var_SKT8_CH2_DAC_RDB__code = (readback_S8 >> 16) & 0x0000FFFF
	
	## convert string from hex/int code
	tmp_ee_dac_skt1_ch1_rdb = conv_string__from_dac_code(var_SKT1_CH1_DAC_RDB__code)
	tmp_ee_dac_skt2_ch1_rdb = conv_string__from_dac_code(var_SKT2_CH1_DAC_RDB__code)
	tmp_ee_dac_skt3_ch1_rdb = conv_string__from_dac_code(var_SKT3_CH1_DAC_RDB__code)
	tmp_ee_dac_skt4_ch1_rdb = conv_string__from_dac_code(var_SKT4_CH1_DAC_RDB__code)
	tmp_ee_dac_skt5_ch1_rdb = conv_string__from_dac_code(var_SKT5_CH1_DAC_RDB__code)
	tmp_ee_dac_skt6_ch1_rdb = conv_string__from_dac_code(var_SKT6_CH1_DAC_RDB__code)
	tmp_ee_dac_skt7_ch1_rdb = conv_string__from_dac_code(var_SKT7_CH1_DAC_RDB__code)
	tmp_ee_dac_skt8_ch1_rdb = conv_string__from_dac_code(var_SKT8_CH1_DAC_RDB__code)
	tmp_ee_dac_skt1_ch2_rdb = conv_string__from_dac_code(var_SKT1_CH2_DAC_RDB__code)
	tmp_ee_dac_skt2_ch2_rdb = conv_string__from_dac_code(var_SKT2_CH2_DAC_RDB__code)
	tmp_ee_dac_skt3_ch2_rdb = conv_string__from_dac_code(var_SKT3_CH2_DAC_RDB__code)
	tmp_ee_dac_skt4_ch2_rdb = conv_string__from_dac_code(var_SKT4_CH2_DAC_RDB__code)
	tmp_ee_dac_skt5_ch2_rdb = conv_string__from_dac_code(var_SKT5_CH2_DAC_RDB__code)
	tmp_ee_dac_skt6_ch2_rdb = conv_string__from_dac_code(var_SKT6_CH2_DAC_RDB__code)
	tmp_ee_dac_skt7_ch2_rdb = conv_string__from_dac_code(var_SKT7_CH2_DAC_RDB__code)
	tmp_ee_dac_skt8_ch2_rdb = conv_string__from_dac_code(var_SKT8_CH2_DAC_RDB__code)
	
	## convert float // var_SKTn_CHn_DAC_RDB
	
	
	## set readback entry // ee_dac_skt3_ch1_rdb
	ee_dac_skt1_ch1_rdb.delete(0,END)
	ee_dac_skt2_ch1_rdb.delete(0,END)
	ee_dac_skt3_ch1_rdb.delete(0,END)
	ee_dac_skt4_ch1_rdb.delete(0,END)
	ee_dac_skt5_ch1_rdb.delete(0,END)
	ee_dac_skt6_ch1_rdb.delete(0,END)
	ee_dac_skt7_ch1_rdb.delete(0,END)
	ee_dac_skt8_ch1_rdb.delete(0,END)
	ee_dac_skt1_ch2_rdb.delete(0,END)
	ee_dac_skt2_ch2_rdb.delete(0,END)
	ee_dac_skt3_ch2_rdb.delete(0,END)
	ee_dac_skt4_ch2_rdb.delete(0,END)
	ee_dac_skt5_ch2_rdb.delete(0,END)
	ee_dac_skt6_ch2_rdb.delete(0,END)
	ee_dac_skt7_ch2_rdb.delete(0,END)
	ee_dac_skt8_ch2_rdb.delete(0,END)
	#
	ee_dac_skt1_ch1_rdb.insert(0,tmp_ee_dac_skt1_ch1_rdb)
	ee_dac_skt2_ch1_rdb.insert(0,tmp_ee_dac_skt2_ch1_rdb)
	ee_dac_skt3_ch1_rdb.insert(0,tmp_ee_dac_skt3_ch1_rdb)
	ee_dac_skt4_ch1_rdb.insert(0,tmp_ee_dac_skt4_ch1_rdb)
	ee_dac_skt5_ch1_rdb.insert(0,tmp_ee_dac_skt5_ch1_rdb)
	ee_dac_skt6_ch1_rdb.insert(0,tmp_ee_dac_skt6_ch1_rdb)
	ee_dac_skt7_ch1_rdb.insert(0,tmp_ee_dac_skt7_ch1_rdb)
	ee_dac_skt8_ch1_rdb.insert(0,tmp_ee_dac_skt8_ch1_rdb)
	ee_dac_skt1_ch2_rdb.insert(0,tmp_ee_dac_skt1_ch2_rdb)
	ee_dac_skt2_ch2_rdb.insert(0,tmp_ee_dac_skt2_ch2_rdb)
	ee_dac_skt3_ch2_rdb.insert(0,tmp_ee_dac_skt3_ch2_rdb)
	ee_dac_skt4_ch2_rdb.insert(0,tmp_ee_dac_skt4_ch2_rdb)
	ee_dac_skt5_ch2_rdb.insert(0,tmp_ee_dac_skt5_ch2_rdb)
	ee_dac_skt6_ch2_rdb.insert(0,tmp_ee_dac_skt6_ch2_rdb)
	ee_dac_skt7_ch2_rdb.insert(0,tmp_ee_dac_skt7_ch2_rdb)
	ee_dac_skt8_ch2_rdb.insert(0,tmp_ee_dac_skt8_ch2_rdb)

	## report
	print('\n>>>>>>')
	print('{} = {}'.format('tmp_ee_dac_skt1_ch1_val', tmp_ee_dac_skt1_ch1_val))
	print('{} = {}'.format('tmp_ee_dac_skt2_ch1_val', tmp_ee_dac_skt2_ch1_val))
	print('{} = {}'.format('tmp_ee_dac_skt3_ch1_val', tmp_ee_dac_skt3_ch1_val))
	print('{} = {}'.format('tmp_ee_dac_skt4_ch1_val', tmp_ee_dac_skt4_ch1_val))
	print('{} = {}'.format('tmp_ee_dac_skt5_ch1_val', tmp_ee_dac_skt5_ch1_val))
	print('{} = {}'.format('tmp_ee_dac_skt6_ch1_val', tmp_ee_dac_skt6_ch1_val))
	print('{} = {}'.format('tmp_ee_dac_skt7_ch1_val', tmp_ee_dac_skt7_ch1_val))
	print('{} = {}'.format('tmp_ee_dac_skt8_ch1_val', tmp_ee_dac_skt8_ch1_val))
	print('{} = {}'.format('tmp_ee_dac_skt1_ch2_val', tmp_ee_dac_skt1_ch2_val))
	print('{} = {}'.format('tmp_ee_dac_skt2_ch2_val', tmp_ee_dac_skt2_ch2_val))
	print('{} = {}'.format('tmp_ee_dac_skt3_ch2_val', tmp_ee_dac_skt3_ch2_val))
	print('{} = {}'.format('tmp_ee_dac_skt4_ch2_val', tmp_ee_dac_skt4_ch2_val))
	print('{} = {}'.format('tmp_ee_dac_skt5_ch2_val', tmp_ee_dac_skt5_ch2_val))
	print('{} = {}'.format('tmp_ee_dac_skt6_ch2_val', tmp_ee_dac_skt6_ch2_val))
	print('{} = {}'.format('tmp_ee_dac_skt7_ch2_val', tmp_ee_dac_skt7_ch2_val))
	print('{} = {}'.format('tmp_ee_dac_skt8_ch2_val', tmp_ee_dac_skt8_ch2_val))
	print('\n>>>>>>')
	print('{} = 0x{:04X}'.format('var_SKT1_CH1_DAC_VAL__code', var_SKT1_CH1_DAC_VAL__code))
	print('{} = 0x{:04X}'.format('var_SKT2_CH1_DAC_VAL__code', var_SKT2_CH1_DAC_VAL__code))
	print('{} = 0x{:04X}'.format('var_SKT3_CH1_DAC_VAL__code', var_SKT3_CH1_DAC_VAL__code))
	print('{} = 0x{:04X}'.format('var_SKT4_CH1_DAC_VAL__code', var_SKT4_CH1_DAC_VAL__code))
	print('{} = 0x{:04X}'.format('var_SKT5_CH1_DAC_VAL__code', var_SKT5_CH1_DAC_VAL__code))
	print('{} = 0x{:04X}'.format('var_SKT6_CH1_DAC_VAL__code', var_SKT6_CH1_DAC_VAL__code))
	print('{} = 0x{:04X}'.format('var_SKT7_CH1_DAC_VAL__code', var_SKT7_CH1_DAC_VAL__code))
	print('{} = 0x{:04X}'.format('var_SKT8_CH1_DAC_VAL__code', var_SKT8_CH1_DAC_VAL__code))
	print('{} = 0x{:04X}'.format('var_SKT1_CH2_DAC_VAL__code', var_SKT1_CH2_DAC_VAL__code))
	print('{} = 0x{:04X}'.format('var_SKT2_CH2_DAC_VAL__code', var_SKT2_CH2_DAC_VAL__code))
	print('{} = 0x{:04X}'.format('var_SKT3_CH2_DAC_VAL__code', var_SKT3_CH2_DAC_VAL__code))
	print('{} = 0x{:04X}'.format('var_SKT4_CH2_DAC_VAL__code', var_SKT4_CH2_DAC_VAL__code))
	print('{} = 0x{:04X}'.format('var_SKT5_CH2_DAC_VAL__code', var_SKT5_CH2_DAC_VAL__code))
	print('{} = 0x{:04X}'.format('var_SKT6_CH2_DAC_VAL__code', var_SKT6_CH2_DAC_VAL__code))
	print('{} = 0x{:04X}'.format('var_SKT7_CH2_DAC_VAL__code', var_SKT7_CH2_DAC_VAL__code))
	print('{} = 0x{:04X}'.format('var_SKT8_CH2_DAC_VAL__code', var_SKT8_CH2_DAC_VAL__code))
	print('\n>>>>>>')
	print('{} = 0x{:08X}'.format('set_data_S1', set_data_S1))
	print('{} = 0x{:08X}'.format('set_data_S2', set_data_S2))
	print('{} = 0x{:08X}'.format('set_data_S3', set_data_S3))
	print('{} = 0x{:08X}'.format('set_data_S4', set_data_S4))
	print('{} = 0x{:08X}'.format('set_data_S5', set_data_S5))
	print('{} = 0x{:08X}'.format('set_data_S6', set_data_S6))
	print('{} = 0x{:08X}'.format('set_data_S7', set_data_S7))
	print('{} = 0x{:08X}'.format('set_data_S8', set_data_S8))
	print('\n>>>>>>')
	print('{} = 0x{:08X}'.format('readback_S1', readback_S1))
	print('{} = 0x{:08X}'.format('readback_S2', readback_S2))
	print('{} = 0x{:08X}'.format('readback_S3', readback_S3))
	print('{} = 0x{:08X}'.format('readback_S4', readback_S4))
	print('{} = 0x{:08X}'.format('readback_S5', readback_S5))
	print('{} = 0x{:08X}'.format('readback_S6', readback_S6))
	print('{} = 0x{:08X}'.format('readback_S7', readback_S7))
	print('{} = 0x{:08X}'.format('readback_S8', readback_S8))
	#
	print('\n>>>>>>')
	print('{} = 0x{:04X}'.format('var_SKT1_CH1_DAC_RDB__code', var_SKT1_CH1_DAC_RDB__code))
	print('{} = 0x{:04X}'.format('var_SKT2_CH1_DAC_RDB__code', var_SKT2_CH1_DAC_RDB__code))
	print('{} = 0x{:04X}'.format('var_SKT3_CH1_DAC_RDB__code', var_SKT3_CH1_DAC_RDB__code))
	print('{} = 0x{:04X}'.format('var_SKT4_CH1_DAC_RDB__code', var_SKT4_CH1_DAC_RDB__code))
	print('{} = 0x{:04X}'.format('var_SKT5_CH1_DAC_RDB__code', var_SKT5_CH1_DAC_RDB__code))
	print('{} = 0x{:04X}'.format('var_SKT6_CH1_DAC_RDB__code', var_SKT6_CH1_DAC_RDB__code))
	print('{} = 0x{:04X}'.format('var_SKT7_CH1_DAC_RDB__code', var_SKT7_CH1_DAC_RDB__code))
	print('{} = 0x{:04X}'.format('var_SKT8_CH1_DAC_RDB__code', var_SKT8_CH1_DAC_RDB__code))
	print('{} = 0x{:04X}'.format('var_SKT1_CH2_DAC_RDB__code', var_SKT1_CH2_DAC_RDB__code))
	print('{} = 0x{:04X}'.format('var_SKT2_CH2_DAC_RDB__code', var_SKT2_CH2_DAC_RDB__code))
	print('{} = 0x{:04X}'.format('var_SKT3_CH2_DAC_RDB__code', var_SKT3_CH2_DAC_RDB__code))
	print('{} = 0x{:04X}'.format('var_SKT4_CH2_DAC_RDB__code', var_SKT4_CH2_DAC_RDB__code))
	print('{} = 0x{:04X}'.format('var_SKT5_CH2_DAC_RDB__code', var_SKT5_CH2_DAC_RDB__code))
	print('{} = 0x{:04X}'.format('var_SKT6_CH2_DAC_RDB__code', var_SKT6_CH2_DAC_RDB__code))
	print('{} = 0x{:04X}'.format('var_SKT7_CH2_DAC_RDB__code', var_SKT7_CH2_DAC_RDB__code))
	print('{} = 0x{:04X}'.format('var_SKT8_CH2_DAC_RDB__code', var_SKT8_CH2_DAC_RDB__code))
	#
	print('\n>>>>>>')
	print('{} = {}'.format('tmp_ee_dac_skt1_ch1_rdb', tmp_ee_dac_skt1_ch1_rdb))
	print('{} = {}'.format('tmp_ee_dac_skt2_ch1_rdb', tmp_ee_dac_skt2_ch1_rdb))
	print('{} = {}'.format('tmp_ee_dac_skt3_ch1_rdb', tmp_ee_dac_skt3_ch1_rdb))
	print('{} = {}'.format('tmp_ee_dac_skt4_ch1_rdb', tmp_ee_dac_skt4_ch1_rdb))
	print('{} = {}'.format('tmp_ee_dac_skt5_ch1_rdb', tmp_ee_dac_skt5_ch1_rdb))
	print('{} = {}'.format('tmp_ee_dac_skt6_ch1_rdb', tmp_ee_dac_skt6_ch1_rdb))
	print('{} = {}'.format('tmp_ee_dac_skt7_ch1_rdb', tmp_ee_dac_skt7_ch1_rdb))
	print('{} = {}'.format('tmp_ee_dac_skt8_ch1_rdb', tmp_ee_dac_skt8_ch1_rdb))
	print('{} = {}'.format('tmp_ee_dac_skt1_ch2_rdb', tmp_ee_dac_skt1_ch2_rdb))
	print('{} = {}'.format('tmp_ee_dac_skt2_ch2_rdb', tmp_ee_dac_skt2_ch2_rdb))
	print('{} = {}'.format('tmp_ee_dac_skt3_ch2_rdb', tmp_ee_dac_skt3_ch2_rdb))
	print('{} = {}'.format('tmp_ee_dac_skt4_ch2_rdb', tmp_ee_dac_skt4_ch2_rdb))
	print('{} = {}'.format('tmp_ee_dac_skt5_ch2_rdb', tmp_ee_dac_skt5_ch2_rdb))
	print('{} = {}'.format('tmp_ee_dac_skt6_ch2_rdb', tmp_ee_dac_skt6_ch2_rdb))
	print('{} = {}'.format('tmp_ee_dac_skt7_ch2_rdb', tmp_ee_dac_skt7_ch2_rdb))
	print('{} = {}'.format('tmp_ee_dac_skt8_ch2_rdb', tmp_ee_dac_skt8_ch2_rdb))
	#
	pass


## MHVSU ADC functions 

def adc_pwr_on():
	print('\n>>>>>> adc_pwr_on')
	dev.SetWireInValue(0x03,0x01,0x00000001) # (ep,val,mask)
	dev.UpdateWireIns()
	
def adc_pwr_off():
	print('\n>>>>>> adc_pwr_off')
	dev.SetWireInValue(0x03,0x00,0x00000001) # (ep,val,mask)
	dev.UpdateWireIns()

def adc_bb_update_pwr_con():
	print('\n>>>>>> adc_bb_update_pwr_con')
	#
	# read gui variable
	print('{} = {}'.format('var_ADC_PWR_ON', var_ADC_PWR_ON.get()))
	#
	if var_ADC_PWR_ON.get()==1:	
		adc_pwr_on()
	else:
		adc_pwr_off()
	#
	pass


def adc_bb_update_con_wi():
	print('\n>>>>>> adc_bb_update_con_wi')
	#
	# read entry
	adc_con_wi__hexstr = ee_adc_con_wi.get() # string
	print('{} = {}'.format('adc_con_wi__hexstr', adc_con_wi__hexstr))
	#
	# convert hexstr into int 
	try:
		adc_con_wi = int(adc_con_wi__hexstr,16)
		print('{} = 0x{:08X}'.format('adc_con_wi', adc_con_wi))
	except:
		print('failed when converting hexstr into int')
		return
	#
	# set wire in 
	dev.SetWireInValue(0x07,adc_con_wi,0xFFFFFFFF) # (ep,val,mask)
	dev.UpdateWireIns()
	#
	pass

def adc_bb_update_sta_wo():
	print('\n>>>>>> adc_bb_update_sta_wo')
	#
	# read endpoint
	dev.UpdateWireOuts()
	adc_status_wo = dev.GetWireOutValue(0x27)
	print('{} = 0x{:08X}'.format('adc_status_wo', adc_status_wo))
	#
	# convert string
	adc_status_wo__hexstr = '0x{:08X}'.format(adc_status_wo)
	#
	# delete the old contents
	ee_adc_sta_wo.delete(0,END)
	#
	# update gui variable
	ee_adc_sta_wo.insert(0,adc_status_wo__hexstr)
	#
	pass


def adc__send_force__sclk_conv(sig_sclk, sig_conv, force_mode, enable):
	# forced mode located in bit[16] @ 0x07
	# forced conv located in bit[17] @ 0x07
	# forced sclk located in bit[18] @ 0x07
	#
	wire_data = 0x00000000 # set enable 
	#
	if enable==1:
		wire_data += 0x00000001
	if force_mode==1:
		wire_data += 0x00010000
	if sig_conv==1:
		wire_data += 0x00020000
	if sig_sclk==1:
		wire_data += 0x00040000
	#forced sclk / cnv / force mdoe
	dev.SetWireInValue(0x07,wire_data,0x00070001) # (ep,val,mask)
	dev.UpdateWireIns()
	pass


def adc_bb_enable_force_mode():
	print('\n>>>>>> adc_bb_enable_force_mode')
	#
	adc__send_force__sclk_conv(sig_sclk=0, sig_conv=1, force_mode=1, enable=1)
	#
	pass
	
def adc_bb_disable_force_mode():
	print('\n>>>>>> adc_bb_disable_force_mode')
	#
	adc__send_force__sclk_conv(sig_sclk=0, sig_conv=0, force_mode=0, enable=1)
	#
	pass
	
def adc_bb_trig_conv_pulse():
	print('\n>>>>>> adc_bb_trig_conv_pulse')
	#
	adc__send_force__sclk_conv(sig_sclk=0, sig_conv=1, force_mode=1, enable=1)
	#
	adc__send_force__sclk_conv(sig_sclk=0, sig_conv=0, force_mode=1, enable=1)
	#
	adc__send_force__sclk_conv(sig_sclk=0, sig_conv=1, force_mode=1, enable=1)
	#
	pass

def update__adc_data__from_serial_bit(serial_data, update_bit):
	ret = (serial_data<<1) + (update_bit&0x0001)
	return ret

def adc_bb_trig_sclk_pulses():
	print('\n>>>>>> adc_bb_trig_sclk_pulses')
	#
	# forced mode located in bit[16] @ 0x07
	# forced conv located in bit[17] @ 0x07
	# forced sclk located in bit[18] @ 0x07
	#
	mon__ADC0_SDO_A = 0;
	mon__ADC0_SDO_B = 0;
	mon__ADC0_SDO_C = 0;
	mon__ADC0_SDO_D = 0;
	mon__ADC1_SDO_A = 0;
	mon__ADC1_SDO_B = 0;
	mon__ADC1_SDO_C = 0;
	mon__ADC1_SDO_D = 0;
	mon__ADC2_SDO_A = 0;
	mon__ADC2_SDO_B = 0;
	mon__ADC2_SDO_C = 0;
	mon__ADC2_SDO_D = 0;
	mon__ADC3_SDO_A = 0;
	mon__ADC3_SDO_B = 0;
	mon__ADC3_SDO_C = 0;
	mon__ADC3_SDO_D = 0;
	#
	num_repeat = 8
	#
	cnt_repeat = 0 
	#
	while True:
		# read endpoint
		dev.UpdateWireOuts()
		adc_status_wo = dev.GetWireOutValue(0x27)
		print('{} LO: {} = 0x{:08X}'.format(cnt_repeat, 'adc_status_wo', adc_status_wo))
		#
		# update serial data 
		mon__ADC0_SDO_A = update__adc_data__from_serial_bit(mon__ADC0_SDO_A, adc_status_wo>>16);
		mon__ADC0_SDO_B = update__adc_data__from_serial_bit(mon__ADC0_SDO_B, adc_status_wo>>17);
		mon__ADC0_SDO_C = update__adc_data__from_serial_bit(mon__ADC0_SDO_C, adc_status_wo>>18);
		mon__ADC0_SDO_D = update__adc_data__from_serial_bit(mon__ADC0_SDO_D, adc_status_wo>>19);
		mon__ADC1_SDO_A = update__adc_data__from_serial_bit(mon__ADC1_SDO_A, adc_status_wo>>20);
		mon__ADC1_SDO_B = update__adc_data__from_serial_bit(mon__ADC1_SDO_B, adc_status_wo>>21);
		mon__ADC1_SDO_C = update__adc_data__from_serial_bit(mon__ADC1_SDO_C, adc_status_wo>>22);
		mon__ADC1_SDO_D = update__adc_data__from_serial_bit(mon__ADC1_SDO_D, adc_status_wo>>23);
		mon__ADC2_SDO_A = update__adc_data__from_serial_bit(mon__ADC2_SDO_A, adc_status_wo>>24);
		mon__ADC2_SDO_B = update__adc_data__from_serial_bit(mon__ADC2_SDO_B, adc_status_wo>>25);
		mon__ADC2_SDO_C = update__adc_data__from_serial_bit(mon__ADC2_SDO_C, adc_status_wo>>26);
		mon__ADC2_SDO_D = update__adc_data__from_serial_bit(mon__ADC2_SDO_D, adc_status_wo>>27);
		mon__ADC3_SDO_A = update__adc_data__from_serial_bit(mon__ADC3_SDO_A, adc_status_wo>>28);
		mon__ADC3_SDO_B = update__adc_data__from_serial_bit(mon__ADC3_SDO_B, adc_status_wo>>29);
		mon__ADC3_SDO_C = update__adc_data__from_serial_bit(mon__ADC3_SDO_C, adc_status_wo>>30);
		mon__ADC3_SDO_D = update__adc_data__from_serial_bit(mon__ADC3_SDO_D, adc_status_wo>>31);
		#
		# forced sclk high / cnv high
		adc__send_force__sclk_conv(sig_sclk=1, sig_conv=1, force_mode=1, enable=1)
		#
		# read endpoint
		dev.UpdateWireOuts()
		adc_status_wo = dev.GetWireOutValue(0x27)
		print('{} HI: {} = 0x{:08X}'.format(cnt_repeat, 'adc_status_wo', adc_status_wo))
		#
		# update serial data 
		mon__ADC0_SDO_A = update__adc_data__from_serial_bit(mon__ADC0_SDO_A, adc_status_wo>>16);
		mon__ADC0_SDO_B = update__adc_data__from_serial_bit(mon__ADC0_SDO_B, adc_status_wo>>17);
		mon__ADC0_SDO_C = update__adc_data__from_serial_bit(mon__ADC0_SDO_C, adc_status_wo>>18);
		mon__ADC0_SDO_D = update__adc_data__from_serial_bit(mon__ADC0_SDO_D, adc_status_wo>>19);
		mon__ADC1_SDO_A = update__adc_data__from_serial_bit(mon__ADC1_SDO_A, adc_status_wo>>20);
		mon__ADC1_SDO_B = update__adc_data__from_serial_bit(mon__ADC1_SDO_B, adc_status_wo>>21);
		mon__ADC1_SDO_C = update__adc_data__from_serial_bit(mon__ADC1_SDO_C, adc_status_wo>>22);
		mon__ADC1_SDO_D = update__adc_data__from_serial_bit(mon__ADC1_SDO_D, adc_status_wo>>23);
		mon__ADC2_SDO_A = update__adc_data__from_serial_bit(mon__ADC2_SDO_A, adc_status_wo>>24);
		mon__ADC2_SDO_B = update__adc_data__from_serial_bit(mon__ADC2_SDO_B, adc_status_wo>>25);
		mon__ADC2_SDO_C = update__adc_data__from_serial_bit(mon__ADC2_SDO_C, adc_status_wo>>26);
		mon__ADC2_SDO_D = update__adc_data__from_serial_bit(mon__ADC2_SDO_D, adc_status_wo>>27);
		mon__ADC3_SDO_A = update__adc_data__from_serial_bit(mon__ADC3_SDO_A, adc_status_wo>>28);
		mon__ADC3_SDO_B = update__adc_data__from_serial_bit(mon__ADC3_SDO_B, adc_status_wo>>29);
		mon__ADC3_SDO_C = update__adc_data__from_serial_bit(mon__ADC3_SDO_C, adc_status_wo>>30);
		mon__ADC3_SDO_D = update__adc_data__from_serial_bit(mon__ADC3_SDO_D, adc_status_wo>>31);
		#
		# forced sclk low / cnv high
		adc__send_force__sclk_conv(sig_sclk=0, sig_conv=1, force_mode=1, enable=1)
		#
		# repeat
		cnt_repeat += 1
		if cnt_repeat >= num_repeat:
			break
	#
	# check serial data on each channel : adc_status_wo[31:16]
	print('{} = 0x{:04X}'.format('mon__ADC0_SDO_A', mon__ADC0_SDO_A))
	print('{} = 0x{:04X}'.format('mon__ADC0_SDO_B', mon__ADC0_SDO_B))
	print('{} = 0x{:04X}'.format('mon__ADC0_SDO_C', mon__ADC0_SDO_C))
	print('{} = 0x{:04X}'.format('mon__ADC0_SDO_D', mon__ADC0_SDO_D))
	print('{} = 0x{:04X}'.format('mon__ADC1_SDO_A', mon__ADC1_SDO_A))
	print('{} = 0x{:04X}'.format('mon__ADC1_SDO_B', mon__ADC1_SDO_B))
	print('{} = 0x{:04X}'.format('mon__ADC1_SDO_C', mon__ADC1_SDO_C))
	print('{} = 0x{:04X}'.format('mon__ADC1_SDO_D', mon__ADC1_SDO_D))
	print('{} = 0x{:04X}'.format('mon__ADC2_SDO_A', mon__ADC2_SDO_A))
	print('{} = 0x{:04X}'.format('mon__ADC2_SDO_B', mon__ADC2_SDO_B))
	print('{} = 0x{:04X}'.format('mon__ADC2_SDO_C', mon__ADC2_SDO_C))
	print('{} = 0x{:04X}'.format('mon__ADC2_SDO_D', mon__ADC2_SDO_D))
	print('{} = 0x{:04X}'.format('mon__ADC3_SDO_A', mon__ADC3_SDO_A))
	print('{} = 0x{:04X}'.format('mon__ADC3_SDO_B', mon__ADC3_SDO_B))
	print('{} = 0x{:04X}'.format('mon__ADC3_SDO_C', mon__ADC3_SDO_C))
	print('{} = 0x{:04X}'.format('mon__ADC3_SDO_D', mon__ADC3_SDO_D))
	
	#
	pass


def adc_bb_trig_reset():
	print('\n>>>>>> adc_bb_trig_reset')

	## trig update 
	# ep47[0] // w_ADC_TRIG_TI 
	dev.ActivateTriggerIn(0x47, 0)
	
	## no done check 
	pass

def adc_bb_update_last_values_wo():
	print('\n>>>>>> adc_bb_update_last_values_wo')
	#
	# read endpoint
	dev.UpdateWireOuts()
	adc_skt1_wo = dev.GetWireOutValue(0x30) # ADC_Sn_WO
	adc_skt2_wo = dev.GetWireOutValue(0x31) # ADC_Sn_WO
	adc_skt3_wo = dev.GetWireOutValue(0x32) # ADC_Sn_WO
	adc_skt4_wo = dev.GetWireOutValue(0x33) # ADC_Sn_WO
	adc_skt5_wo = dev.GetWireOutValue(0x34) # ADC_Sn_WO
	adc_skt6_wo = dev.GetWireOutValue(0x35) # ADC_Sn_WO
	adc_skt7_wo = dev.GetWireOutValue(0x36) # ADC_Sn_WO
	adc_skt8_wo = dev.GetWireOutValue(0x37) # ADC_Sn_WO
	print('{} = 0x{:08X}'.format('adc_skt1_wo', adc_skt1_wo))
	print('{} = 0x{:08X}'.format('adc_skt2_wo', adc_skt2_wo))
	print('{} = 0x{:08X}'.format('adc_skt3_wo', adc_skt3_wo))
	print('{} = 0x{:08X}'.format('adc_skt4_wo', adc_skt4_wo))
	print('{} = 0x{:08X}'.format('adc_skt5_wo', adc_skt5_wo))
	print('{} = 0x{:08X}'.format('adc_skt6_wo', adc_skt6_wo))
	print('{} = 0x{:08X}'.format('adc_skt7_wo', adc_skt7_wo))
	print('{} = 0x{:08X}'.format('adc_skt8_wo', adc_skt8_wo))
	#
	# convert string
	adc_skt1_wo__hexstr = '0x{:08X}'.format(adc_skt1_wo)
	adc_skt2_wo__hexstr = '0x{:08X}'.format(adc_skt2_wo)
	adc_skt3_wo__hexstr = '0x{:08X}'.format(adc_skt3_wo)
	adc_skt4_wo__hexstr = '0x{:08X}'.format(adc_skt4_wo)
	adc_skt5_wo__hexstr = '0x{:08X}'.format(adc_skt5_wo)
	adc_skt6_wo__hexstr = '0x{:08X}'.format(adc_skt6_wo)
	adc_skt7_wo__hexstr = '0x{:08X}'.format(adc_skt7_wo)
	adc_skt8_wo__hexstr = '0x{:08X}'.format(adc_skt8_wo)
	#
	# delete the old contents
	ee_adc_s1_wo.delete(0,END)
	ee_adc_s2_wo.delete(0,END)
	ee_adc_s3_wo.delete(0,END)
	ee_adc_s4_wo.delete(0,END)
	ee_adc_s5_wo.delete(0,END)
	ee_adc_s6_wo.delete(0,END)
	ee_adc_s7_wo.delete(0,END)
	ee_adc_s8_wo.delete(0,END)
	#
	# update gui variable
	ee_adc_s1_wo.insert(0,adc_skt1_wo__hexstr)
	ee_adc_s2_wo.insert(0,adc_skt2_wo__hexstr)
	ee_adc_s3_wo.insert(0,adc_skt3_wo__hexstr)
	ee_adc_s4_wo.insert(0,adc_skt4_wo__hexstr)
	ee_adc_s5_wo.insert(0,adc_skt5_wo__hexstr)
	ee_adc_s6_wo.insert(0,adc_skt6_wo__hexstr)
	ee_adc_s7_wo.insert(0,adc_skt7_wo__hexstr)
	ee_adc_s8_wo.insert(0,adc_skt8_wo__hexstr)
	#	
	pass

def adc_bb_trig_single():
	print('\n>>>>>> adc_bb_trig_single')
	
	## trig update 
	# ep47[1] // w_ADC_TRIG_TI // update
	dev.ActivateTriggerIn(0x47, 1)
	
	# ep67[0] // w_ADC_TRIG_TO // update done check
	cnt_loop = 0;
	while 1:
		# First, query all XEM Trigger Outs.
		dev.UpdateTriggerOuts();
		# check trigger out
		if dev.IsTriggered(0x67, 0x01) == True: # // (ep, mask)
			break
		cnt_loop += 1;
		print('{} = {}'.format('cnt_loop', cnt_loop))
		if (cnt_loop>MAX_count):
			break
	print('{} = {}'.format('cnt_loop', cnt_loop))
	#
	
	## update adc wire out
	adc_bb_update_last_values_wo()
	pass

def adc_bb_update_par_wi():
	print('\n>>>>>> adc_bb_update_par_wi')
	## #
	## # read entry
	## adc_con_wi__hexstr = ee_adc_con_wi.get() # string
	## print('{} = {}'.format('adc_con_wi__hexstr', adc_con_wi__hexstr))
	## #
	## # convert hexstr into int 
	## try:
	## 	adc_con_wi = int(adc_con_wi__hexstr,16)
	## 	print('{} = 0x{:08X}'.format('adc_con_wi', adc_con_wi))
	## except:
	## 	print('failed when converting hexstr into int')
	## 	return
	## #
	## # set wire in 
	## dev.SetWireInValue(0x07,adc_con_wi,0xFFFFFFFF) # (ep,val,mask)
	## dev.UpdateWireIns()
	## #
	pass

def adc_bb_update_period_wi():
	print('\n>>>>>> adc_bb_update_period_wi')
	#
	# read entry
	adc_period_wi__decstr = ee_adc_period_wi.get() # string
	print('{} = {}'.format('adc_period_wi__decstr', adc_period_wi__decstr))
	#
	# convert decstr into int 
	try:
		adc_period_wi = int(adc_period_wi__decstr)
		print('{} = {}'.format('adc_period_wi', adc_period_wi))
	except:
		print('failed when converting decstr into int')
		return
	#
	# set wire in // w_count_period_div4 = w_ADC_PAR_WI[15:0 ] // ep10wire
	dev.SetWireInValue(0x10, int(adc_period_wi/4), 0x0000FFFF) # (ep,val,mask)
	dev.UpdateWireIns()
	#
	pass

def adc_bb_update_num_samples_wi():
	print('\n>>>>>> adc_bb_update_num_samples_wi')
	#
	# read entry
	adc_num_samples_wi__decstr = ee_adc_num_samples_wi.get() # string
	print('{} = {}'.format('adc_num_samples_wi__decstr', adc_num_samples_wi__decstr))
	#
	# convert decstr into int 
	try:
		adc_num_samples_wi = int(adc_num_samples_wi__decstr)
		print('{} = {}'.format('adc_num_samples_wi', adc_num_samples_wi))
	except:
		print('failed when converting decstr into int')
		return
	#
	# set wire in // w_count_conv_div4   = w_ADC_PAR_WI[31:16] // ep10wire
	dev.SetWireInValue(0x10, (int(adc_num_samples_wi/4) << 16), 0xFFFF0000) # (ep,val,mask)
	dev.UpdateWireIns()
	#
	pass

def adc_bb_trig_run():
	print('\n>>>>>> adc_bb_trig_run')
	
	## set parameters:
	adc_bb_update_period_wi()
	adc_bb_update_num_samples_wi()
	
	## trig update 
	# ep47[2] // w_ADC_TRIG_TI // update
	dev.ActivateTriggerIn(0x47, 2)
	
	# ep67[0] // w_ADC_TRIG_TO // update done check
	cnt_loop = 0;
	while 1:
		# First, query all XEM Trigger Outs.
		dev.UpdateTriggerOuts();
		# check trigger out
		if dev.IsTriggered(0x67, 0x01) == True: # // (ep, mask)
			break
		cnt_loop += 1;
		print('{} = {}'.format('cnt_loop', cnt_loop))
		if (cnt_loop>MAX_count):
			break
	print('{} = {}'.format('cnt_loop', cnt_loop))
	#
	
	## update adc wire out
	adc_bb_update_last_values_wo()
	pass

def adc_bb_conv_code_to_volt():
	print('\n>>>>>> adc_bb_conv_code_to_volt')
	
	# https://www.analog.com/media/en/technical-documentation/data-sheets/232516fa.pdf
	# Figure 11. Fully-Differential Transfer Function
	# V_ref    = 4.096V
	# max_code = 32767
	# code_measure // 16 bit, 2's complement 
	# V_measure = code_measure / max_code * V_ref
	# if V_measure>4.096V, V_measure = V_measure - 4.096V // for negative numbers
	
	# ee_adc_volt_s1_ch2 .set  ee_adc_volt_s1_ch1 set
	
	## read wire_out 
	dev.UpdateWireOuts()
	read_ADC_S1_codes = dev.GetWireOutValue(0x30) # // ep30wire = w_ADC_S1_WO
	read_ADC_S2_codes = dev.GetWireOutValue(0x31) # // ep31wire = w_ADC_S2_WO
	read_ADC_S3_codes = dev.GetWireOutValue(0x32) # // ep32wire = w_ADC_S3_WO
	read_ADC_S4_codes = dev.GetWireOutValue(0x33) # // ep33wire = w_ADC_S4_WO
	read_ADC_S5_codes = dev.GetWireOutValue(0x34) # // ep34wire = w_ADC_S5_WO
	read_ADC_S6_codes = dev.GetWireOutValue(0x35) # // ep35wire = w_ADC_S6_WO
	read_ADC_S7_codes = dev.GetWireOutValue(0x36) # // ep36wire = w_ADC_S7_WO
	read_ADC_S8_codes = dev.GetWireOutValue(0x37) # // ep37wire = w_ADC_S8_WO
	
	## separate codes
	var_ADC_S1_CH1_code = (read_ADC_S1_codes >>  0) & 0x0000FFFF
	var_ADC_S2_CH1_code = (read_ADC_S2_codes >>  0) & 0x0000FFFF
	var_ADC_S3_CH1_code = (read_ADC_S3_codes >>  0) & 0x0000FFFF
	var_ADC_S4_CH1_code = (read_ADC_S4_codes >>  0) & 0x0000FFFF
	var_ADC_S5_CH1_code = (read_ADC_S5_codes >>  0) & 0x0000FFFF
	var_ADC_S6_CH1_code = (read_ADC_S6_codes >>  0) & 0x0000FFFF
	var_ADC_S7_CH1_code = (read_ADC_S7_codes >>  0) & 0x0000FFFF
	var_ADC_S8_CH1_code = (read_ADC_S8_codes >>  0) & 0x0000FFFF
	#
	var_ADC_S1_CH2_code = (read_ADC_S1_codes >> 16) & 0x0000FFFF
	var_ADC_S2_CH2_code = (read_ADC_S2_codes >> 16) & 0x0000FFFF
	var_ADC_S3_CH2_code = (read_ADC_S3_codes >> 16) & 0x0000FFFF
	var_ADC_S4_CH2_code = (read_ADC_S4_codes >> 16) & 0x0000FFFF
	var_ADC_S5_CH2_code = (read_ADC_S5_codes >> 16) & 0x0000FFFF
	var_ADC_S6_CH2_code = (read_ADC_S6_codes >> 16) & 0x0000FFFF
	var_ADC_S7_CH2_code = (read_ADC_S7_codes >> 16) & 0x0000FFFF
	var_ADC_S8_CH2_code = (read_ADC_S8_codes >> 16) & 0x0000FFFF

	print('\n>>>>>>')
	print('{} = {}'.format('var_ADC_S1_CH1_code', var_ADC_S1_CH1_code))
	print('{} = {}'.format('var_ADC_S2_CH1_code', var_ADC_S2_CH1_code))
	print('{} = {}'.format('var_ADC_S3_CH1_code', var_ADC_S3_CH1_code))
	print('{} = {}'.format('var_ADC_S4_CH1_code', var_ADC_S4_CH1_code))
	print('{} = {}'.format('var_ADC_S5_CH1_code', var_ADC_S5_CH1_code))
	print('{} = {}'.format('var_ADC_S6_CH1_code', var_ADC_S6_CH1_code))
	print('{} = {}'.format('var_ADC_S7_CH1_code', var_ADC_S7_CH1_code))
	print('{} = {}'.format('var_ADC_S8_CH1_code', var_ADC_S8_CH1_code))
	#
	print('{} = {}'.format('var_ADC_S1_CH2_code', var_ADC_S1_CH2_code))
	print('{} = {}'.format('var_ADC_S2_CH2_code', var_ADC_S2_CH2_code))
	print('{} = {}'.format('var_ADC_S3_CH2_code', var_ADC_S3_CH2_code))
	print('{} = {}'.format('var_ADC_S4_CH2_code', var_ADC_S4_CH2_code))
	print('{} = {}'.format('var_ADC_S5_CH2_code', var_ADC_S5_CH2_code))
	print('{} = {}'.format('var_ADC_S6_CH2_code', var_ADC_S6_CH2_code))
	print('{} = {}'.format('var_ADC_S7_CH2_code', var_ADC_S7_CH2_code))
	print('{} = {}'.format('var_ADC_S8_CH2_code', var_ADC_S8_CH2_code))
		
	# test
	#tmp1=conv_bit_2s_comp_16bit_to_dec(bit_2s_comp=0x7FFF, full_scale=4.096*2)
	#tmp2=conv_bit_2s_comp_16bit_to_dec(bit_2s_comp=0x0100, full_scale=4.096*2)
	#tmp3=conv_bit_2s_comp_16bit_to_dec(bit_2s_comp=0xFF00, full_scale=4.096*2)
	#tmp4=conv_bit_2s_comp_16bit_to_dec(bit_2s_comp=0x8000, full_scale=4.096*2)
	#print('\n>>>>>>')
	#print('{} = {:.8}'.format('tmp1', tmp1))
	#print('{} = {:.8}'.format('tmp2', tmp2))
	#print('{} = {:.8}'.format('tmp3', tmp3))
	#print('{} = {:.8}'.format('tmp4', tmp4))
	
	# convert code to value 
	V_ref =4.096
	#
	var_ADC_S1_CH1_volt = conv_bit_2s_comp_16bit_to_dec(bit_2s_comp=var_ADC_S1_CH1_code, full_scale=V_ref*2)
	var_ADC_S2_CH1_volt = conv_bit_2s_comp_16bit_to_dec(bit_2s_comp=var_ADC_S2_CH1_code, full_scale=V_ref*2)
	var_ADC_S3_CH1_volt = conv_bit_2s_comp_16bit_to_dec(bit_2s_comp=var_ADC_S3_CH1_code, full_scale=V_ref*2)
	var_ADC_S4_CH1_volt = conv_bit_2s_comp_16bit_to_dec(bit_2s_comp=var_ADC_S4_CH1_code, full_scale=V_ref*2)
	var_ADC_S5_CH1_volt = conv_bit_2s_comp_16bit_to_dec(bit_2s_comp=var_ADC_S5_CH1_code, full_scale=V_ref*2)
	var_ADC_S6_CH1_volt = conv_bit_2s_comp_16bit_to_dec(bit_2s_comp=var_ADC_S6_CH1_code, full_scale=V_ref*2)
	var_ADC_S7_CH1_volt = conv_bit_2s_comp_16bit_to_dec(bit_2s_comp=var_ADC_S7_CH1_code, full_scale=V_ref*2)
	var_ADC_S8_CH1_volt = conv_bit_2s_comp_16bit_to_dec(bit_2s_comp=var_ADC_S8_CH1_code, full_scale=V_ref*2)
	#
	var_ADC_S1_CH2_volt = conv_bit_2s_comp_16bit_to_dec(bit_2s_comp=var_ADC_S1_CH2_code, full_scale=V_ref*2)
	var_ADC_S2_CH2_volt = conv_bit_2s_comp_16bit_to_dec(bit_2s_comp=var_ADC_S2_CH2_code, full_scale=V_ref*2)
	var_ADC_S3_CH2_volt = conv_bit_2s_comp_16bit_to_dec(bit_2s_comp=var_ADC_S3_CH2_code, full_scale=V_ref*2)
	var_ADC_S4_CH2_volt = conv_bit_2s_comp_16bit_to_dec(bit_2s_comp=var_ADC_S4_CH2_code, full_scale=V_ref*2)
	var_ADC_S5_CH2_volt = conv_bit_2s_comp_16bit_to_dec(bit_2s_comp=var_ADC_S5_CH2_code, full_scale=V_ref*2)
	var_ADC_S6_CH2_volt = conv_bit_2s_comp_16bit_to_dec(bit_2s_comp=var_ADC_S6_CH2_code, full_scale=V_ref*2)
	var_ADC_S7_CH2_volt = conv_bit_2s_comp_16bit_to_dec(bit_2s_comp=var_ADC_S7_CH2_code, full_scale=V_ref*2)
	var_ADC_S8_CH2_volt = conv_bit_2s_comp_16bit_to_dec(bit_2s_comp=var_ADC_S8_CH2_code, full_scale=V_ref*2)
	
	print('\n>>>>>>')
	print('{} = {:.8}'.format('var_ADC_S1_CH1_volt', var_ADC_S1_CH1_volt))
	print('{} = {:.8}'.format('var_ADC_S2_CH1_volt', var_ADC_S2_CH1_volt))
	print('{} = {:.8}'.format('var_ADC_S3_CH1_volt', var_ADC_S3_CH1_volt))
	print('{} = {:.8}'.format('var_ADC_S4_CH1_volt', var_ADC_S4_CH1_volt))
	print('{} = {:.8}'.format('var_ADC_S5_CH1_volt', var_ADC_S5_CH1_volt))
	print('{} = {:.8}'.format('var_ADC_S6_CH1_volt', var_ADC_S6_CH1_volt))
	print('{} = {:.8}'.format('var_ADC_S7_CH1_volt', var_ADC_S7_CH1_volt))
	print('{} = {:.8}'.format('var_ADC_S8_CH1_volt', var_ADC_S8_CH1_volt))
	#
	print('{} = {:.8}'.format('var_ADC_S1_CH2_volt', var_ADC_S1_CH2_volt))
	print('{} = {:.8}'.format('var_ADC_S2_CH2_volt', var_ADC_S2_CH2_volt))
	print('{} = {:.8}'.format('var_ADC_S3_CH2_volt', var_ADC_S3_CH2_volt))
	print('{} = {:.8}'.format('var_ADC_S4_CH2_volt', var_ADC_S4_CH2_volt))
	print('{} = {:.8}'.format('var_ADC_S5_CH2_volt', var_ADC_S5_CH2_volt))
	print('{} = {:.8}'.format('var_ADC_S6_CH2_volt', var_ADC_S6_CH2_volt))
	print('{} = {:.8}'.format('var_ADC_S7_CH2_volt', var_ADC_S7_CH2_volt))
	print('{} = {:.8}'.format('var_ADC_S8_CH2_volt', var_ADC_S8_CH2_volt))
	
	# convert string 
	var_ADC_S1_CH1_volt_str = '{:.8}'.format(var_ADC_S1_CH1_volt)
	var_ADC_S2_CH1_volt_str = '{:.8}'.format(var_ADC_S2_CH1_volt)
	var_ADC_S3_CH1_volt_str = '{:.8}'.format(var_ADC_S3_CH1_volt)
	var_ADC_S4_CH1_volt_str = '{:.8}'.format(var_ADC_S4_CH1_volt)
	var_ADC_S5_CH1_volt_str = '{:.8}'.format(var_ADC_S5_CH1_volt)
	var_ADC_S6_CH1_volt_str = '{:.8}'.format(var_ADC_S6_CH1_volt)
	var_ADC_S7_CH1_volt_str = '{:.8}'.format(var_ADC_S7_CH1_volt)
	var_ADC_S8_CH1_volt_str = '{:.8}'.format(var_ADC_S8_CH1_volt)
	#
	var_ADC_S1_CH2_volt_str = '{:.8}'.format(var_ADC_S1_CH2_volt)
	var_ADC_S2_CH2_volt_str = '{:.8}'.format(var_ADC_S2_CH2_volt)
	var_ADC_S3_CH2_volt_str = '{:.8}'.format(var_ADC_S3_CH2_volt)
	var_ADC_S4_CH2_volt_str = '{:.8}'.format(var_ADC_S4_CH2_volt)
	var_ADC_S5_CH2_volt_str = '{:.8}'.format(var_ADC_S5_CH2_volt)
	var_ADC_S6_CH2_volt_str = '{:.8}'.format(var_ADC_S6_CH2_volt)
	var_ADC_S7_CH2_volt_str = '{:.8}'.format(var_ADC_S7_CH2_volt)
	var_ADC_S8_CH2_volt_str = '{:.8}'.format(var_ADC_S8_CH2_volt)
	
	# update box 
	ee_adc_volt_s1_ch1.delete(0,END)
	ee_adc_volt_s2_ch1.delete(0,END)
	ee_adc_volt_s3_ch1.delete(0,END)
	ee_adc_volt_s4_ch1.delete(0,END)
	ee_adc_volt_s5_ch1.delete(0,END)
	ee_adc_volt_s6_ch1.delete(0,END)
	ee_adc_volt_s7_ch1.delete(0,END)
	ee_adc_volt_s8_ch1.delete(0,END)
	#
	ee_adc_volt_s1_ch2.delete(0,END)
	ee_adc_volt_s2_ch2.delete(0,END)
	ee_adc_volt_s3_ch2.delete(0,END)
	ee_adc_volt_s4_ch2.delete(0,END)
	ee_adc_volt_s5_ch2.delete(0,END)
	ee_adc_volt_s6_ch2.delete(0,END)
	ee_adc_volt_s7_ch2.delete(0,END)
	ee_adc_volt_s8_ch2.delete(0,END)
	#
	ee_adc_volt_s1_ch1.insert(0,var_ADC_S1_CH1_volt_str)
	ee_adc_volt_s2_ch1.insert(0,var_ADC_S2_CH1_volt_str)
	ee_adc_volt_s3_ch1.insert(0,var_ADC_S3_CH1_volt_str)
	ee_adc_volt_s4_ch1.insert(0,var_ADC_S4_CH1_volt_str)
	ee_adc_volt_s5_ch1.insert(0,var_ADC_S5_CH1_volt_str)
	ee_adc_volt_s6_ch1.insert(0,var_ADC_S6_CH1_volt_str)
	ee_adc_volt_s7_ch1.insert(0,var_ADC_S7_CH1_volt_str)
	ee_adc_volt_s8_ch1.insert(0,var_ADC_S8_CH1_volt_str)
	#
	ee_adc_volt_s1_ch2.insert(0,var_ADC_S1_CH2_volt_str)
	ee_adc_volt_s2_ch2.insert(0,var_ADC_S2_CH2_volt_str)
	ee_adc_volt_s3_ch2.insert(0,var_ADC_S3_CH2_volt_str)
	ee_adc_volt_s4_ch2.insert(0,var_ADC_S4_CH2_volt_str)
	ee_adc_volt_s5_ch2.insert(0,var_ADC_S5_CH2_volt_str)
	ee_adc_volt_s6_ch2.insert(0,var_ADC_S6_CH2_volt_str)
	ee_adc_volt_s7_ch2.insert(0,var_ADC_S7_CH2_volt_str)
	ee_adc_volt_s8_ch2.insert(0,var_ADC_S8_CH2_volt_str)
		
	pass

## gui setup
def tk_win_setup(master):
	
	## initialize SPIO 
	spio_init()
	
	## test update SPIO 
	spio_test_led_on()
	

	## initialize DAC 
	dac_init()
	
	## test update DAC
	dac_test()
	
	## title lines
	row_title = 0
	row_SPIO  = 1
	row_DAC   = 30 
	row_ADC   = 60 
	Label(master, text="== MHVSU controls : [SPIO] [DAC] [ADC] == ").grid(row=row_title, column= 0, sticky=W, columnspan = 4)
	Label(master, text="===[SPIO]===").grid(row=row_SPIO, column= 0, sticky=W, columnspan = 4)
	Label(master, text="===[DAC]===") .grid(row=row_DAC , column= 0, sticky=W, columnspan = 4)
	Label(master, text="===[ADC]===") .grid(row=row_ADC , column= 0, sticky=W, columnspan = 4)
	
	## buttons for SPIO 
	Label(master, text="[SPIO] SKT# selection:  ").grid(row=row_SPIO+1, column= 0, sticky=W, columnspan = 1)
	Label(master, text="[SPIO] CS0 outputs:     ").grid(row=row_SPIO+1, column= 1, sticky=W, columnspan = 1)
	Label(master, text="[SPIO] CS1 outputs:     ").grid(row=row_SPIO+1, column= 2, sticky=W, columnspan = 1)
	Label(master, text="[SPIO] CS2 outputs:     ").grid(row=row_SPIO+1, column= 3, sticky=W, columnspan = 1)
	# 
	Checkbutton(master, text="SKT1 ENABLE", variable=var_SKT1_EN).grid(row=row_SPIO+1+1, column=0, sticky=W)
	Checkbutton(master, text="SKT2 ENABLE", variable=var_SKT2_EN).grid(row=row_SPIO+1+2, column=0, sticky=W)
	Checkbutton(master, text="SKT3 ENABLE", variable=var_SKT3_EN).grid(row=row_SPIO+1+3, column=0, sticky=W)
	Checkbutton(master, text="SKT4 ENABLE", variable=var_SKT4_EN).grid(row=row_SPIO+1+4, column=0, sticky=W)
	Checkbutton(master, text="SKT5 ENABLE", variable=var_SKT5_EN).grid(row=row_SPIO+1+5, column=0, sticky=W)
	Checkbutton(master, text="SKT6 ENABLE", variable=var_SKT6_EN).grid(row=row_SPIO+1+6, column=0, sticky=W)
	Checkbutton(master, text="SKT7 ENABLE", variable=var_SKT7_EN).grid(row=row_SPIO+1+7, column=0, sticky=W)
	Checkbutton(master, text="SKT8 ENABLE", variable=var_SKT8_EN).grid(row=row_SPIO+1+8, column=0, sticky=W)
	#
	Checkbutton(master, text="CS0 GPB7 CHECK_LED0     ", variable=var_CS0_GPB7).grid(row=row_SPIO+1+ 1, column=1, sticky=W) # CS0 GPB7 CHECK_LED0     
	Checkbutton(master, text="CS0 GPB6 CH2_ADC_VM     ", variable=var_CS0_GPB6).grid(row=row_SPIO+1+ 2, column=1, sticky=W) # CS0 GPB6 CH2_ADC_VM     
	Checkbutton(master, text="CS0 GPB5 CH2_GAIN_X20   ", variable=var_CS0_GPB5).grid(row=row_SPIO+1+ 3, column=1, sticky=W) # CS0 GPB5 CH2_GAIN_X20   
	Checkbutton(master, text="CS0 GPB4 CH2_GAIN_X2    ", variable=var_CS0_GPB4).grid(row=row_SPIO+1+ 4, column=1, sticky=W) # CS0 GPB4 CH2_GAIN_X2    
	Checkbutton(master, text="CS0 GPB3 NA             ", variable=var_CS0_GPB3).grid(row=row_SPIO+1+ 5, column=1, sticky=W) # CS0 GPB3 NA             
	Checkbutton(master, text="CS0 GPB2 CH2_RANGE_20uA ", variable=var_CS0_GPB2).grid(row=row_SPIO+1+ 6, column=1, sticky=W) # CS0 GPB2 CH2_RANGE_20uA 
	Checkbutton(master, text="CS0 GPB1 CH2_RANGE_2mA  ", variable=var_CS0_GPB1).grid(row=row_SPIO+1+ 7, column=1, sticky=W) # CS0 GPB1 CH2_RANGE_2mA  
	Checkbutton(master, text="CS0 GPB0 CH2_RANGE_50mA ", variable=var_CS0_GPB0).grid(row=row_SPIO+1+ 8, column=1, sticky=W) # CS0 GPB0 CH2_RANGE_50mA 
	Checkbutton(master, text="CS0 GPA7 NA             ", variable=var_CS0_GPA7).grid(row=row_SPIO+1+11, column=1, sticky=W) # CS0 GPA7 NA             
	Checkbutton(master, text="CS0 GPA6 CH1_ADC_VM     ", variable=var_CS0_GPA6).grid(row=row_SPIO+1+12, column=1, sticky=W) # CS0 GPA6 CH1_ADC_VM     
	Checkbutton(master, text="CS0 GPA5 CH1_GAIN_X20   ", variable=var_CS0_GPA5).grid(row=row_SPIO+1+13, column=1, sticky=W) # CS0 GPA5 CH1_GAIN_X20   
	Checkbutton(master, text="CS0 GPA4 CH1_GAIN_X2    ", variable=var_CS0_GPA4).grid(row=row_SPIO+1+14, column=1, sticky=W) # CS0 GPA4 CH1_GAIN_X2    
	Checkbutton(master, text="CS0 GPA3 NA             ", variable=var_CS0_GPA3).grid(row=row_SPIO+1+15, column=1, sticky=W) # CS0 GPA3 NA             
	Checkbutton(master, text="CS0 GPA2 CH1_RANGE_20uA ", variable=var_CS0_GPA2).grid(row=row_SPIO+1+16, column=1, sticky=W) # CS0 GPA2 CH1_RANGE_20uA 
	Checkbutton(master, text="CS0 GPA1 CH1_RANGE_2mA  ", variable=var_CS0_GPA1).grid(row=row_SPIO+1+17, column=1, sticky=W) # CS0 GPA1 CH1_RANGE_2mA  
	Checkbutton(master, text="CS0 GPA0 CH1_RANGE_50mA ", variable=var_CS0_GPA0).grid(row=row_SPIO+1+18, column=1, sticky=W) # CS0 GPA0 CH1_RANGE_50mA 
	#
	Checkbutton(master, text="CS1 GPB7 CHECK_LED1      ", variable=var_CS1_GPB7).grid(row=row_SPIO+1+ 1, column=2, sticky=W) # CS1 GPB7 CHECK_LED1       
	Checkbutton(master, text="CS1 GPB6 CH2_RELAY_DIAG  ", variable=var_CS1_GPB6).grid(row=row_SPIO+1+ 2, column=2, sticky=W) # CS1 GPB6 CH2_RELAY_DIAG   
	Checkbutton(master, text="CS1 GPB5 CH2_RELAY_OUTPUT", variable=var_CS1_GPB5).grid(row=row_SPIO+1+ 3, column=2, sticky=W) # CS1 GPB5 CH2_RELAY_OUTPUT 
	Checkbutton(master, text="CS1 GPB4 CH2_LIMIT_50mA  ", variable=var_CS1_GPB4).grid(row=row_SPIO+1+ 4, column=2, sticky=W) # CS1 GPB4 CH2_LIMIT_50mA   
	Checkbutton(master, text="CS1 GPB3 NA              ", variable=var_CS1_GPB3).grid(row=row_SPIO+1+ 5, column=2, sticky=W) # CS1 GPB3 NA               
	Checkbutton(master, text="CS1 GPB2 NA              ", variable=var_CS1_GPB2).grid(row=row_SPIO+1+ 6, column=2, sticky=W) # CS1 GPB2 NA               
	Checkbutton(master, text="CS1 GPB1 CH2_CMP_330p    ", variable=var_CS1_GPB1).grid(row=row_SPIO+1+ 7, column=2, sticky=W) # CS1 GPB1 CH2_CMP_330p     
	Checkbutton(master, text="CS1 GPB0 CH2_CMP_100p    ", variable=var_CS1_GPB0).grid(row=row_SPIO+1+ 8, column=2, sticky=W) # CS1 GPB0 CH2_CMP_100p     
	Checkbutton(master, text="CS1 GPA7 NA              ", variable=var_CS1_GPA7).grid(row=row_SPIO+1+11, column=2, sticky=W) # CS1 GPA7 NA               
	Checkbutton(master, text="CS1 GPA6 CH1_RELAY_DIAG  ", variable=var_CS1_GPA6).grid(row=row_SPIO+1+12, column=2, sticky=W) # CS1 GPA6 CH1_RELAY_DIAG   
	Checkbutton(master, text="CS1 GPA5 CH1_RELAY_OUTPUT", variable=var_CS1_GPA5).grid(row=row_SPIO+1+13, column=2, sticky=W) # CS1 GPA5 CH1_RELAY_OUTPUT 
	Checkbutton(master, text="CS1 GPA4 CH1_LIMIT_50mA  ", variable=var_CS1_GPA4).grid(row=row_SPIO+1+14, column=2, sticky=W) # CS1 GPA4 CH1_LIMIT_50mA   
	Checkbutton(master, text="CS1 GPA3 NA              ", variable=var_CS1_GPA3).grid(row=row_SPIO+1+15, column=2, sticky=W) # CS1 GPA3 NA               
	Checkbutton(master, text="CS1 GPA2 NA              ", variable=var_CS1_GPA2).grid(row=row_SPIO+1+16, column=2, sticky=W) # CS1 GPA2 NA               
	Checkbutton(master, text="CS1 GPA1 CH1_CMP_330p    ", variable=var_CS1_GPA1).grid(row=row_SPIO+1+17, column=2, sticky=W) # CS1 GPA1 CH1_CMP_330p     
	Checkbutton(master, text="CS1 GPA0 CH1_CMP_100p    ", variable=var_CS1_GPA0).grid(row=row_SPIO+1+18, column=2, sticky=W) # CS1 GPA0 CH1_CMP_100p     
	#
	Checkbutton(master, text="CS2 GPB7 NA   ", variable=var_CS2_GPB7).grid(row=row_SPIO+1+ 1, column=3, sticky=W) # CS2 GPB7 NA
	Checkbutton(master, text="CS2 GPB6 NA   ", variable=var_CS2_GPB6).grid(row=row_SPIO+1+ 2, column=3, sticky=W) # CS2 GPB6 NA
	Checkbutton(master, text="CS2 GPB5 NA   ", variable=var_CS2_GPB5).grid(row=row_SPIO+1+ 3, column=3, sticky=W) # CS2 GPB5 NA
	Checkbutton(master, text="CS2 GPB4 NA   ", variable=var_CS2_GPB4).grid(row=row_SPIO+1+ 4, column=3, sticky=W) # CS2 GPB4 NA
	Checkbutton(master, text="CS2 GPB3 NA   ", variable=var_CS2_GPB3).grid(row=row_SPIO+1+ 5, column=3, sticky=W) # CS2 GPB3 NA
	Checkbutton(master, text="CS2 GPB2 NA   ", variable=var_CS2_GPB2).grid(row=row_SPIO+1+ 6, column=3, sticky=W) # CS2 GPB2 NA
	Checkbutton(master, text="CS2 GPB1 NA   ", variable=var_CS2_GPB1).grid(row=row_SPIO+1+ 7, column=3, sticky=W) # CS2 GPB1 NA
	Checkbutton(master, text="CS2 GPB0 NA   ", variable=var_CS2_GPB0).grid(row=row_SPIO+1+ 8, column=3, sticky=W) # CS2 GPB0 NA
	Checkbutton(master, text="CS2 GPA7 NA   ", variable=var_CS2_GPA7).grid(row=row_SPIO+1+11, column=3, sticky=W) # CS2 GPA7 NA
	Checkbutton(master, text="CS2 GPA6 NA   ", variable=var_CS2_GPA6).grid(row=row_SPIO+1+12, column=3, sticky=W) # CS2 GPA6 NA
	Checkbutton(master, text="CS2 GPA5 NA   ", variable=var_CS2_GPA5).grid(row=row_SPIO+1+13, column=3, sticky=W) # CS2 GPA5 NA
	Checkbutton(master, text="CS2 GPA4 NA   ", variable=var_CS2_GPA4).grid(row=row_SPIO+1+14, column=3, sticky=W) # CS2 GPA4 NA
	Checkbutton(master, text="CS2 GPA3 NA   ", variable=var_CS2_GPA3).grid(row=row_SPIO+1+15, column=3, sticky=W) # CS2 GPA3 NA
	Checkbutton(master, text="CS2 GPA2 NA   ", variable=var_CS2_GPA2).grid(row=row_SPIO+1+16, column=3, sticky=W) # CS2 GPA2 NA
	Checkbutton(master, text="CS2 GPA1 NA   ", variable=var_CS2_GPA1).grid(row=row_SPIO+1+17, column=3, sticky=W) # CS2 GPA1 NA
	Checkbutton(master, text="CS2 GPA0 NA   ", variable=var_CS2_GPA0).grid(row=row_SPIO+1+18, column=3, sticky=W) # CS2 GPA0 NA
	
	# update SPIO values
	bb_spio_cs0_update = Button(master, text='update SPIO CS0 values', command=spio_bb_update_cs0) .grid(row=row_SPIO+20, column=1, columnspan = 1, sticky=W+E, pady=4)
	bb_spio_cs1_update = Button(master, text='update SPIO CS1 values', command=spio_bb_update_cs1) .grid(row=row_SPIO+20, column=2, columnspan = 1, sticky=W+E, pady=4)
	bb_spio_cs2_update = Button(master, text='update SPIO CS2 values', command=spio_bb_update_cs2) .grid(row=row_SPIO+20, column=3, columnspan = 1, sticky=W+E, pady=4)
	
	
	## buttons/labels/entries for DAC
	#
	Label(master, text="[DAC] CH1 Value-in:     ").grid(row=row_DAC+1, column= 0, sticky=W, columnspan = 2)
	Label(master, text="[DAC] CH2 Value-in:     ").grid(row=row_DAC+1, column= 2, sticky=W, columnspan = 2)
	Label(master, text="[DAC] CH1 Readback:     ").grid(row=row_DAC+1, column= 4, sticky=W, columnspan = 2)
	Label(master, text="[DAC] CH2 Readback:     ").grid(row=row_DAC+1, column= 6, sticky=W, columnspan = 2)
	# DAC values
	Label(master, text=" DAC_SKT1_CH1_VAL=").grid(row=row_DAC+1+1, column=0, sticky=E)
	Label(master, text=" DAC_SKT2_CH1_VAL=").grid(row=row_DAC+1+2, column=0, sticky=E)
	Label(master, text=" DAC_SKT3_CH1_VAL=").grid(row=row_DAC+1+3, column=0, sticky=E)
	Label(master, text=" DAC_SKT4_CH1_VAL=").grid(row=row_DAC+1+4, column=0, sticky=E)
	Label(master, text=" DAC_SKT5_CH1_VAL=").grid(row=row_DAC+1+5, column=0, sticky=E)
	Label(master, text=" DAC_SKT6_CH1_VAL=").grid(row=row_DAC+1+6, column=0, sticky=E)
	Label(master, text=" DAC_SKT7_CH1_VAL=").grid(row=row_DAC+1+7, column=0, sticky=E)
	Label(master, text=" DAC_SKT8_CH1_VAL=").grid(row=row_DAC+1+8, column=0, sticky=E)
	ee_dac_skt1_ch1_val                     .grid(row=row_DAC+1+1, column=1)
	ee_dac_skt2_ch1_val                     .grid(row=row_DAC+1+2, column=1)
	ee_dac_skt3_ch1_val                     .grid(row=row_DAC+1+3, column=1)
	ee_dac_skt4_ch1_val                     .grid(row=row_DAC+1+4, column=1)
	ee_dac_skt5_ch1_val                     .grid(row=row_DAC+1+5, column=1)
	ee_dac_skt6_ch1_val                     .grid(row=row_DAC+1+6, column=1)
	ee_dac_skt7_ch1_val                     .grid(row=row_DAC+1+7, column=1)
	ee_dac_skt8_ch1_val                     .grid(row=row_DAC+1+8, column=1)
	Label(master, text=" DAC_SKT1_CH2_VAL=").grid(row=row_DAC+1+1, column=2, sticky=E)
	Label(master, text=" DAC_SKT2_CH2_VAL=").grid(row=row_DAC+1+2, column=2, sticky=E)
	Label(master, text=" DAC_SKT3_CH2_VAL=").grid(row=row_DAC+1+3, column=2, sticky=E)
	Label(master, text=" DAC_SKT4_CH2_VAL=").grid(row=row_DAC+1+4, column=2, sticky=E)
	Label(master, text=" DAC_SKT5_CH2_VAL=").grid(row=row_DAC+1+5, column=2, sticky=E)
	Label(master, text=" DAC_SKT6_CH2_VAL=").grid(row=row_DAC+1+6, column=2, sticky=E)
	Label(master, text=" DAC_SKT7_CH2_VAL=").grid(row=row_DAC+1+7, column=2, sticky=E)
	Label(master, text=" DAC_SKT8_CH2_VAL=").grid(row=row_DAC+1+8, column=2, sticky=E)
	ee_dac_skt1_ch2_val                     .grid(row=row_DAC+1+1, column=3)
	ee_dac_skt2_ch2_val                     .grid(row=row_DAC+1+2, column=3)
	ee_dac_skt3_ch2_val                     .grid(row=row_DAC+1+3, column=3)
	ee_dac_skt4_ch2_val                     .grid(row=row_DAC+1+4, column=3)
	ee_dac_skt5_ch2_val                     .grid(row=row_DAC+1+5, column=3)
	ee_dac_skt6_ch2_val                     .grid(row=row_DAC+1+6, column=3)
	ee_dac_skt7_ch2_val                     .grid(row=row_DAC+1+7, column=3)
	ee_dac_skt8_ch2_val                     .grid(row=row_DAC+1+8, column=3)
	Label(master, text=" DAC_SKT1_CH1_RDB=").grid(row=row_DAC+1+1, column=4, sticky=E)
	Label(master, text=" DAC_SKT2_CH1_RDB=").grid(row=row_DAC+1+2, column=4, sticky=E)
	Label(master, text=" DAC_SKT3_CH1_RDB=").grid(row=row_DAC+1+3, column=4, sticky=E)
	Label(master, text=" DAC_SKT4_CH1_RDB=").grid(row=row_DAC+1+4, column=4, sticky=E)
	Label(master, text=" DAC_SKT5_CH1_RDB=").grid(row=row_DAC+1+5, column=4, sticky=E)
	Label(master, text=" DAC_SKT6_CH1_RDB=").grid(row=row_DAC+1+6, column=4, sticky=E)
	Label(master, text=" DAC_SKT7_CH1_RDB=").grid(row=row_DAC+1+7, column=4, sticky=E)
	Label(master, text=" DAC_SKT8_CH1_RDB=").grid(row=row_DAC+1+8, column=4, sticky=E)
	ee_dac_skt1_ch1_rdb                     .grid(row=row_DAC+1+1, column=5)
	ee_dac_skt2_ch1_rdb                     .grid(row=row_DAC+1+2, column=5)
	ee_dac_skt3_ch1_rdb                     .grid(row=row_DAC+1+3, column=5)
	ee_dac_skt4_ch1_rdb                     .grid(row=row_DAC+1+4, column=5)
	ee_dac_skt5_ch1_rdb                     .grid(row=row_DAC+1+5, column=5)
	ee_dac_skt6_ch1_rdb                     .grid(row=row_DAC+1+6, column=5)
	ee_dac_skt7_ch1_rdb                     .grid(row=row_DAC+1+7, column=5)
	ee_dac_skt8_ch1_rdb                     .grid(row=row_DAC+1+8, column=5)
	Label(master, text=" DAC_SKT1_CH2_RDB=").grid(row=row_DAC+1+1, column=6, sticky=E)
	Label(master, text=" DAC_SKT2_CH2_RDB=").grid(row=row_DAC+1+2, column=6, sticky=E)
	Label(master, text=" DAC_SKT3_CH2_RDB=").grid(row=row_DAC+1+3, column=6, sticky=E)
	Label(master, text=" DAC_SKT4_CH2_RDB=").grid(row=row_DAC+1+4, column=6, sticky=E)
	Label(master, text=" DAC_SKT5_CH2_RDB=").grid(row=row_DAC+1+5, column=6, sticky=E)
	Label(master, text=" DAC_SKT6_CH2_RDB=").grid(row=row_DAC+1+6, column=6, sticky=E)
	Label(master, text=" DAC_SKT7_CH2_RDB=").grid(row=row_DAC+1+7, column=6, sticky=E)
	Label(master, text=" DAC_SKT8_CH2_RDB=").grid(row=row_DAC+1+8, column=6, sticky=E)
	ee_dac_skt1_ch2_rdb                     .grid(row=row_DAC+1+1, column=7)
	ee_dac_skt2_ch2_rdb                     .grid(row=row_DAC+1+2, column=7)
	ee_dac_skt3_ch2_rdb                     .grid(row=row_DAC+1+3, column=7)
	ee_dac_skt4_ch2_rdb                     .grid(row=row_DAC+1+4, column=7)
	ee_dac_skt5_ch2_rdb                     .grid(row=row_DAC+1+5, column=7)
	ee_dac_skt6_ch2_rdb                     .grid(row=row_DAC+1+6, column=7)
	ee_dac_skt7_ch2_rdb                     .grid(row=row_DAC+1+7, column=7)
	ee_dac_skt8_ch2_rdb                     .grid(row=row_DAC+1+8, column=7)
	# update DAC values
	bb_dac_update = Button(master, text='update DAC values', command=dac_bb_update) .grid(row=row_DAC+20, column=0, columnspan = 4, sticky=W+E, pady=4)
	
	
	## buttons/labels/entries for ADC
	#
	#
	Label(master, text="[ADC] Power control:    ").grid(row=row_ADC+1, column= 0, sticky=W, columnspan = 2)
	Label(master, text="[ADC] Control in (HEX): ").grid(row=row_ADC+1, column= 2, sticky=W, columnspan = 2)
	Label(master, text="[ADC] Status out (HEX): ").grid(row=row_ADC+1, column= 4, sticky=W, columnspan = 2)
	#
	# [ADC] Power control:
	Checkbutton(master, text="ADC POWER ON", variable=var_ADC_PWR_ON).grid(row=row_ADC+1+1, column=0, sticky=W)
	Button     (master, text="update power control", command=adc_bb_update_pwr_con)     .grid(row=row_ADC+1+1, column=1, columnspan = 1, sticky=W, pady=4)
	#
	# [ADC] Control in:
	ee_adc_con_wi .grid(row=row_ADC+1+1, column=2, sticky=E)
	Button     (master, text="update control in", command=adc_bb_update_con_wi)     .grid(row=row_ADC+1+1, column=3, columnspan = 1, sticky=W, pady=4)
	# [ADC] Status out:
	ee_adc_sta_wo .grid(row=row_ADC+1+1, column=4, sticky=E)
	Button     (master, text="update status out", command=adc_bb_update_sta_wo)     .grid(row=row_ADC+1+1, column=5, columnspan = 1, sticky=W, pady=4)
	#
	#
	Label(master, text="[ADC] Forced mode tests          : ").grid(row=row_ADC+1+2, column= 0, sticky=W, columnspan = 2)
	Label(master, text="[ADC] Normal mode tests          : ").grid(row=row_ADC+1+2, column= 2, sticky=W, columnspan = 2)
	Label(master, text="[ADC] Last ADC codes (HEX code)  : ").grid(row=row_ADC+1+2, column= 4, sticky=W, columnspan = 2)
	Label(master, text="[ADC] Last ADC voltages {CH2,CH1}: ").grid(row=row_ADC+1+2, column= 6, sticky=W, columnspan = 2)
	#
	# [ADC] Forced mode tests:
	Label  (master, text="Enable forced mode: ")                                    .grid(row=row_ADC+1+3, column= 0, sticky=W, columnspan = 1)
	Button (master, text="enable forced mode  ", command=adc_bb_enable_force_mode)  .grid(row=row_ADC+1+3, column= 1, columnspan = 1, sticky=W, pady=4)
	Label  (master, text="Send one CONV pulse:")                                    .grid(row=row_ADC+1+4, column= 0, sticky=W, columnspan = 1)
	Button (master, text="trigger a conv pulse", command=adc_bb_trig_conv_pulse)    .grid(row=row_ADC+1+4, column= 1, columnspan = 1, sticky=W, pady=4)
	Label  (master, text="Send SCLK pulses:   ")                                    .grid(row=row_ADC+1+5, column= 0, sticky=W, columnspan = 1)
	Button (master, text="trigger sclk pulses ", command=adc_bb_trig_sclk_pulses)   .grid(row=row_ADC+1+5, column= 1, columnspan = 1, sticky=W, pady=4)
	Label  (master, text="Disable forced mode:")                                    .grid(row=row_ADC+1+6, column= 0, sticky=W, columnspan = 1)
	Button (master, text="disable forced mode ", command=adc_bb_disable_force_mode) .grid(row=row_ADC+1+6, column= 1, columnspan = 1, sticky=W, pady=4)
	# 
	# [ADC] Normal mode tests:
	Label  (master, text="Trigger ADC reset         : ")                      .grid(row=row_ADC+1+3, column= 2, sticky=W, columnspan = 1)
	Button (master, text="ADC  reset trigger ", command=adc_bb_trig_reset )   .grid(row=row_ADC+1+3, column= 3, columnspan = 1, sticky=W+E, pady=4)
	Label  (master, text="Trigger ADC single measure: ")                      .grid(row=row_ADC+1+4, column= 2, sticky=W, columnspan = 1)
	Button (master, text="ADC single trigger ", command=adc_bb_trig_single)   .grid(row=row_ADC+1+4, column= 3, columnspan = 1, sticky=W+E, pady=4)
	Label  (master, text="Set ADC sampling period   : ")                      .grid(row=row_ADC+1+5, column= 2, sticky=W, columnspan = 1)
	ee_adc_period_wi                                                          .grid(row=row_ADC+1+5, column= 3)
	Label  (master, text="Set ADC samples to acquire: ")                      .grid(row=row_ADC+1+6, column= 2, sticky=W, columnspan = 1)
	ee_adc_num_samples_wi                                                     .grid(row=row_ADC+1+6, column= 3)
	Label  (master, text="Trigger ADC contiguous run: ")                      .grid(row=row_ADC+1+7, column= 2, sticky=W, columnspan = 1)
	Button (master, text="ADC    run trigger ", command=adc_bb_trig_run   )   .grid(row=row_ADC+1+7, column= 3, columnspan = 1, sticky=W+E, pady=4)
	#
	# [ADC] Last ADC values (HEX code):
	Label  (master, text="Read ADC_S1_WO  : ")  .grid(row=row_ADC+1+3 , column= 4, sticky=W, columnspan = 1, pady=4)
	ee_adc_s1_wo                                .grid(row=row_ADC+1+3 , column= 5, sticky=E)
	Label  (master, text="Read ADC_S2_WO  : ")  .grid(row=row_ADC+1+4 , column= 4, sticky=W, columnspan = 1, pady=4)
	ee_adc_s2_wo                                .grid(row=row_ADC+1+4 , column= 5, sticky=E)
	Label  (master, text="Read ADC_S3_WO  : ")  .grid(row=row_ADC+1+5 , column= 4, sticky=W, columnspan = 1, pady=4)
	ee_adc_s3_wo                                .grid(row=row_ADC+1+5 , column= 5, sticky=E)
	Label  (master, text="Read ADC_S4_WO  : ")  .grid(row=row_ADC+1+6 , column= 4, sticky=W, columnspan = 1, pady=4)
	ee_adc_s4_wo                                .grid(row=row_ADC+1+6 , column= 5, sticky=E)
	Label  (master, text="Read ADC_S5_WO  : ")  .grid(row=row_ADC+1+7 , column= 4, sticky=W, columnspan = 1, pady=4)
	ee_adc_s5_wo                                .grid(row=row_ADC+1+7 , column= 5, sticky=E)
	Label  (master, text="Read ADC_S6_WO  : ")  .grid(row=row_ADC+1+8 , column= 4, sticky=W, columnspan = 1, pady=4)
	ee_adc_s6_wo                                .grid(row=row_ADC+1+8 , column= 5, sticky=E)
	Label  (master, text="Read ADC_S7_WO  : ")  .grid(row=row_ADC+1+9 , column= 4, sticky=W, columnspan = 1, pady=4)
	ee_adc_s7_wo                                .grid(row=row_ADC+1+9 , column= 5, sticky=E)
	Label  (master, text="Read ADC_S8_WO  : ")  .grid(row=row_ADC+1+10, column= 4, sticky=W, columnspan = 1, pady=4)
	ee_adc_s8_wo                                .grid(row=row_ADC+1+10, column= 5, sticky=E)
	#
	# [ADC] Last ADC values (voltage) :
	ee_adc_volt_s1_ch2 .grid(row=row_ADC+1+3 , column= 6, sticky=E)
	ee_adc_volt_s1_ch1 .grid(row=row_ADC+1+3 , column= 7, sticky=E)
	ee_adc_volt_s2_ch2 .grid(row=row_ADC+1+4 , column= 6, sticky=E)
	ee_adc_volt_s2_ch1 .grid(row=row_ADC+1+4 , column= 7, sticky=E)
	ee_adc_volt_s3_ch2 .grid(row=row_ADC+1+5 , column= 6, sticky=E)
	ee_adc_volt_s3_ch1 .grid(row=row_ADC+1+5 , column= 7, sticky=E)
	ee_adc_volt_s4_ch2 .grid(row=row_ADC+1+6 , column= 6, sticky=E)
	ee_adc_volt_s4_ch1 .grid(row=row_ADC+1+6 , column= 7, sticky=E)
	ee_adc_volt_s5_ch2 .grid(row=row_ADC+1+7 , column= 6, sticky=E)
	ee_adc_volt_s5_ch1 .grid(row=row_ADC+1+7 , column= 7, sticky=E)
	ee_adc_volt_s6_ch2 .grid(row=row_ADC+1+8 , column= 6, sticky=E)
	ee_adc_volt_s6_ch1 .grid(row=row_ADC+1+8 , column= 7, sticky=E)
	ee_adc_volt_s7_ch2 .grid(row=row_ADC+1+9 , column= 6, sticky=E)
	ee_adc_volt_s7_ch1 .grid(row=row_ADC+1+9 , column= 7, sticky=E)
	ee_adc_volt_s8_ch2 .grid(row=row_ADC+1+10, column= 6, sticky=E)
	ee_adc_volt_s8_ch1 .grid(row=row_ADC+1+10, column= 7, sticky=E)
	#
	Button (master, text="convert ADC codes to voltages ", command=adc_bb_conv_code_to_volt).grid(row=row_ADC+1+11, column= 6, columnspan = 2, sticky=W+E, pady=4)
	
	
	## buttons common 
	# quit
	bb_quit = Button(master, text='Quit', command=master.quit) .grid(row=100,  sticky=W+E, pady=4, columnspan = 8)
	
	#
	return
#
tk_win_setup(master)

## main loop for gui
mainloop() ####


################################################################################

## wait for an input  ######################################################
#input('Enter any key to stop')


################################################################################

## wait for a while
sleep(0.5)



## test update SPIO 
spio_test_led_off()
spio_close()

## clear DAC
dac_test()
dac_close()

## reset ADC 
# ... to come 
## power off ADC 
adc_pwr_off()

## OK close
dev.Close()


##



