## test__MHVSU_BASE.py

from time import sleep
import sys


## tkinter GUI module 
# https://www.tutorialspoint.com/python3/python_gui_programming.htm
# https://www.tutorialspoint.com/python3/tk_button.htm
# https://www.tutorialspoint.com/python3/tk_checkbutton.htm
# https://www.python-course.eu/tkinter_checkboxes.php
# https://www.tutorialspoint.com/python3/tk_entry.htm
# https://www.python-course.eu/tkinter_entry_widgets.php ... entry text number...



## common converter

def conv_dec_to_bit_2s_comp_16bit(dec, full_scale=20):
	if dec > full_scale/2-full_scale/2**16 :
		dec = full_scale/2-full_scale/2**16
	if dec < -full_scale/2-full_scale/2**16 :
		dec = -full_scale/2-full_scale/2**16
	#bit_2s_comp = int( 0x10000 * ( dec + full_scale/2)    / full_scale ) + 0x8000
	bit_2s_comp = int( 0x10000 * ( dec + full_scale/2)    / full_scale +0.5) + 0x8000
	if bit_2s_comp > 0xFFFF :
		bit_2s_comp -= 0x10000
	return bit_2s_comp

#test_codes = [ conv_dec_to_bit_2s_comp_16bit(x) for x in [-10,-5,0,5,10] ]
#print(test_codes)
	
def conv_bit_2s_comp_16bit_to_dec(bit_2s_comp, full_scale=20):
	if bit_2s_comp >= 0x8000:
		bit_2s_comp -= 0x8000
		#dec = full_scale * (bit_2s_comp) / 0x10000 -10
		dec = full_scale * (bit_2s_comp) / 0x10000 - full_scale/2
	else :
		dec = full_scale * (bit_2s_comp) / 0x10000
		if dec == full_scale/2-full_scale/2**16 :
			dec = full_scale/2
	return dec

#test_codes2 = [ conv_bit_2s_comp_16bit_to_dec(x) for x in test_codes ]
#print(test_codes2)

	


###########################################################################
## open OK USB device  ####################################################

import ok

# OK init
dev = ok.okCFrontPanel()

# OK open
DeviceCount = dev.GetDeviceCount()
print(' {}={}'.format('DeviceCount',DeviceCount))
dev.OpenBySerial("")
DevSR = dev.GetSerialNumber()
print(' {}={}'.format('DevSR',DevSR))




###########################################################################

## 
print('>>> tk GUI control')


#### GUI: Checkbutton and Button
from tkinter import *
from functools import partial

master = Tk()

## gui variables 
var_ch2_gain__ = IntVar()
var_ch1_gain__ = IntVar()
var_ch2_be____ = IntVar()
var_ch2_fe____ = IntVar()
var_ch1_be____ = IntVar()
var_ch1_fe____ = IntVar()
var_ch2_sleepn = IntVar()
var_ch1_sleepn = IntVar()

## gui variables for SPIO
# socket selections
# output nets

var_SKT1_EN = IntVar(value=1)  #SKT1 ENABLE
var_SKT2_EN = IntVar(value=1)  #SKT2 ENABLE
var_SKT3_EN = IntVar(value=1)  #SKT3 ENABLE
var_SKT4_EN = IntVar(value=1)  #SKT4 ENABLE
var_SKT5_EN = IntVar(value=1)  #SKT5 ENABLE
var_SKT6_EN = IntVar(value=1)  #SKT6 ENABLE
var_SKT7_EN = IntVar(value=1)  #SKT7 ENABLE
var_SKT8_EN = IntVar(value=1)  #SKT8 ENABLE

var_CS0_GPB7 = IntVar()  #CS0 GPB7 CHECK_LED0
var_CS0_GPB6 = IntVar()  #CS0 GPB6 CH2_ADC_IM
var_CS0_GPB5 = IntVar()  #CS0 GPB5 CH2_GAIN_X20
var_CS0_GPB4 = IntVar()  #CS0 GPB4 CH2_GAIN_X2
var_CS0_GPB3 = IntVar()  #CS0 GPB3 NA
var_CS0_GPB2 = IntVar()  #CS0 GPB2 CH2_RANGE_20uA
var_CS0_GPB1 = IntVar()  #CS0 GPB1 CH2_RANGE_2mA
var_CS0_GPB0 = IntVar()  #CS0 GPB0 CH2_RANGE_50mA
var_CS0_GPA7 = IntVar()  #CS0 GPA7 NA
var_CS0_GPA6 = IntVar()  #CS0 GPA6 CH1_ADC_IM
var_CS0_GPA5 = IntVar()  #CS0 GPA5 CH1_GAIN_X20
var_CS0_GPA4 = IntVar()  #CS0 GPA4 CH1_GAIN_X2
var_CS0_GPA3 = IntVar()  #CS0 GPA3 NA
var_CS0_GPA2 = IntVar()  #CS0 GPA2 CH1_RANGE_20uA
var_CS0_GPA1 = IntVar()  #CS0 GPA1 CH1_RANGE_2mA
var_CS0_GPA0 = IntVar()  #CS0 GPA0 CH1_RANGE_50mA

var_CS1_GPB7 = IntVar()  #CS1 GPB7 CHECK_LED1
var_CS1_GPB6 = IntVar()  #CS1 GPB6 CH2_RELAY_DIAG
var_CS1_GPB5 = IntVar()  #CS1 GPB5 CH2_RELAY_OUTPUT
var_CS1_GPB4 = IntVar()  #CS1 GPB4 CH2_LIMIT_50mA
var_CS1_GPB3 = IntVar()  #CS1 GPB3 NA
var_CS1_GPB2 = IntVar()  #CS1 GPB2 NA
var_CS1_GPB1 = IntVar()  #CS1 GPB1 CH2_CMP_330p
var_CS1_GPB0 = IntVar()  #CS1 GPB0 CH2_CMP_100p
var_CS1_GPA7 = IntVar()  #CS1 GPA7 NA
var_CS1_GPA6 = IntVar()  #CS1 GPA6 CH1_RELAY_DIAG
var_CS1_GPA5 = IntVar()  #CS1 GPA5 CH1_RELAY_OUTPUT
var_CS1_GPA4 = IntVar()  #CS1 GPA4 CH1_LIMIT_50mA
var_CS1_GPA3 = IntVar()  #CS1 GPA3 NA
var_CS1_GPA2 = IntVar()  #CS1 GPA2 NA
var_CS1_GPA1 = IntVar()  #CS1 GPA1 CH1_CMP_330p
var_CS1_GPA0 = IntVar()  #CS1 GPA0 CH1_CMP_100p

var_CS2_GPB7 = IntVar()  #CS2 GPB7
var_CS2_GPB6 = IntVar()  #CS2 GPB6
var_CS2_GPB5 = IntVar()  #CS2 GPB5
var_CS2_GPB4 = IntVar()  #CS2 GPB4
var_CS2_GPB3 = IntVar()  #CS2 GPB3
var_CS2_GPB2 = IntVar()  #CS2 GPB2
var_CS2_GPB1 = IntVar()  #CS2 GPB1
var_CS2_GPB0 = IntVar()  #CS2 GPB0
var_CS2_GPA7 = IntVar()  #CS2 GPA7
var_CS2_GPA6 = IntVar()  #CS2 GPA6
var_CS2_GPA5 = IntVar()  #CS2 GPA5
var_CS2_GPA4 = IntVar()  #CS2 GPA4
var_CS2_GPA3 = IntVar()  #CS2 GPA3
var_CS2_GPA2 = IntVar()  #CS2 GPA2
var_CS2_GPA1 = IntVar()  #CS2 GPA1
var_CS2_GPA0 = IntVar()  #CS2 GPA0

## gui variables for DAC
# DAC value input 
# DAC readback

ee_dac_skt1_ch1_val = Entry(master, justify='right') 
ee_dac_skt2_ch1_val = Entry(master, justify='right') 
ee_dac_skt3_ch1_val = Entry(master, justify='right') 
ee_dac_skt4_ch1_val = Entry(master, justify='right') 
ee_dac_skt5_ch1_val = Entry(master, justify='right') 
ee_dac_skt6_ch1_val = Entry(master, justify='right') 
ee_dac_skt7_ch1_val = Entry(master, justify='right') 
ee_dac_skt8_ch1_val = Entry(master, justify='right') 
ee_dac_skt1_ch2_val = Entry(master, justify='right') 
ee_dac_skt2_ch2_val = Entry(master, justify='right') 
ee_dac_skt3_ch2_val = Entry(master, justify='right') 
ee_dac_skt4_ch2_val = Entry(master, justify='right') 
ee_dac_skt5_ch2_val = Entry(master, justify='right') 
ee_dac_skt6_ch2_val = Entry(master, justify='right') 
ee_dac_skt7_ch2_val = Entry(master, justify='right') 
ee_dac_skt8_ch2_val = Entry(master, justify='right') 

ee_dac_skt1_ch1_rdb = Entry(master, justify='right')  
ee_dac_skt2_ch1_rdb = Entry(master, justify='right')  
ee_dac_skt3_ch1_rdb = Entry(master, justify='right')  
ee_dac_skt4_ch1_rdb = Entry(master, justify='right')  
ee_dac_skt5_ch1_rdb = Entry(master, justify='right')  
ee_dac_skt6_ch1_rdb = Entry(master, justify='right')  
ee_dac_skt7_ch1_rdb = Entry(master, justify='right')  
ee_dac_skt8_ch1_rdb = Entry(master, justify='right')  
ee_dac_skt1_ch2_rdb = Entry(master, justify='right')  
ee_dac_skt2_ch2_rdb = Entry(master, justify='right')  
ee_dac_skt3_ch2_rdb = Entry(master, justify='right')  
ee_dac_skt4_ch2_rdb = Entry(master, justify='right')  
ee_dac_skt5_ch2_rdb = Entry(master, justify='right')  
ee_dac_skt6_ch2_rdb = Entry(master, justify='right')  
ee_dac_skt7_ch2_rdb = Entry(master, justify='right')  
ee_dac_skt8_ch2_rdb = Entry(master, justify='right')  

var_SKT1_CH1_DAC_VAL = DoubleVar()  #SKT1 CH1 DAC INP 
var_SKT2_CH1_DAC_VAL = DoubleVar()  #SKT2 CH1 DAC INP 
var_SKT3_CH1_DAC_VAL = DoubleVar()  #SKT3 CH1 DAC INP 
var_SKT4_CH1_DAC_VAL = DoubleVar()  #SKT4 CH1 DAC INP 
var_SKT5_CH1_DAC_VAL = DoubleVar()  #SKT5 CH1 DAC INP 
var_SKT6_CH1_DAC_VAL = DoubleVar()  #SKT6 CH1 DAC INP 
var_SKT7_CH1_DAC_VAL = DoubleVar()  #SKT7 CH1 DAC INP 
var_SKT8_CH1_DAC_VAL = DoubleVar()  #SKT8 CH1 DAC INP 
var_SKT1_CH2_DAC_VAL = DoubleVar()  #SKT1 CH2 DAC INP 
var_SKT2_CH2_DAC_VAL = DoubleVar()  #SKT2 CH2 DAC INP 
var_SKT3_CH2_DAC_VAL = DoubleVar()  #SKT3 CH2 DAC INP 
var_SKT4_CH2_DAC_VAL = DoubleVar()  #SKT4 CH2 DAC INP 
var_SKT5_CH2_DAC_VAL = DoubleVar()  #SKT5 CH2 DAC INP 
var_SKT6_CH2_DAC_VAL = DoubleVar()  #SKT6 CH2 DAC INP 
var_SKT7_CH2_DAC_VAL = DoubleVar()  #SKT7 CH2 DAC INP 
var_SKT8_CH2_DAC_VAL = DoubleVar()  #SKT8 CH2 DAC INP 

var_SKT1_CH1_DAC_RBK = DoubleVar()  #SKT1 CH1 DAC READBACK 
var_SKT2_CH1_DAC_RBK = DoubleVar()  #SKT2 CH1 DAC READBACK 
var_SKT3_CH1_DAC_RBK = DoubleVar()  #SKT3 CH1 DAC READBACK 
var_SKT4_CH1_DAC_RBK = DoubleVar()  #SKT4 CH1 DAC READBACK 
var_SKT5_CH1_DAC_RBK = DoubleVar()  #SKT5 CH1 DAC READBACK 
var_SKT6_CH1_DAC_RBK = DoubleVar()  #SKT6 CH1 DAC READBACK 
var_SKT7_CH1_DAC_RBK = DoubleVar()  #SKT7 CH1 DAC READBACK 
var_SKT8_CH1_DAC_RBK = DoubleVar()  #SKT8 CH1 DAC READBACK 
var_SKT1_CH2_DAC_RBK = DoubleVar()  #SKT1 CH2 DAC READBACK 
var_SKT2_CH2_DAC_RBK = DoubleVar()  #SKT2 CH2 DAC READBACK 
var_SKT3_CH2_DAC_RBK = DoubleVar()  #SKT3 CH2 DAC READBACK 
var_SKT4_CH2_DAC_RBK = DoubleVar()  #SKT4 CH2 DAC READBACK 
var_SKT5_CH2_DAC_RBK = DoubleVar()  #SKT5 CH2 DAC READBACK 
var_SKT6_CH2_DAC_RBK = DoubleVar()  #SKT6 CH2 DAC READBACK 
var_SKT7_CH2_DAC_RBK = DoubleVar()  #SKT7 CH2 DAC READBACK 
var_SKT8_CH2_DAC_RBK = DoubleVar()  #SKT8 CH2 DAC READBACK 



## MHVSU SPIO functions 

def spio_init():
	# ep05 = 0xFF070001 // w_SPIO_CON_WI // socket/cs enable 
	dev.SetWireInValue(0x05,0xFF070001,0xFFFFFFFF) # (ep,val,mask)
	dev.UpdateWireIns()
	# ep04 = 0x0000FF7F // w_SPIO_FDAT_WI // set IO direction // led only
	# ep04 = 0x00000000 // w_SPIO_FDAT_WI // set IO direction // all outputs
	dev.SetWireInValue(0x04,0x00000000,0xFFFFFFFF) # (ep,val,mask)
	dev.UpdateWireIns()
	# ep45[1] // w_SPIO_TRIG_TI
	dev.ActivateTriggerIn(0x45, 1)

def spio_test_led_on():
	# ep05 = 0xFF070001 // w_SPIO_CON_WI // socket/cs enable // all selected
	dev.SetWireInValue(0x05,0xFF070001,0xFFFFFFFF) # (ep,val,mask)
	dev.UpdateWireIns()
	# ep04 = 0x00120080 // {GPA,GPB}
	dev.SetWireInValue(0x04,0x00120080,0xFFFFFFFF) # (ep,val,mask)
	dev.UpdateWireIns()
	# ep45[1]
	dev.ActivateTriggerIn(0x45, 1)

def spio_test_led_off():
	# ep05 = 0xFF070001 // w_SPIO_CON_WI // socket/cs enable // all selected
	dev.SetWireInValue(0x05,0xFF070001,0xFFFFFFFF) # (ep,val,mask)
	dev.UpdateWireIns()
	# ep04 = 0x00120080 // {GPA,GPB}
	dev.SetWireInValue(0x04,0x00120000,0xFFFFFFFF) # (ep,val,mask)
	dev.UpdateWireIns()
	# ep45[1]
	dev.ActivateTriggerIn(0x45, 1)

def spio_bb_update_cs0():
	## read check box
	print('\n>>>>>>')
	# read socket enable data
	print('{} = {}'.format('var_SKT1_EN', var_SKT1_EN.get()))
	print('{} = {}'.format('var_SKT2_EN', var_SKT2_EN.get()))
	print('{} = {}'.format('var_SKT3_EN', var_SKT3_EN.get()))
	print('{} = {}'.format('var_SKT4_EN', var_SKT4_EN.get()))
	print('{} = {}'.format('var_SKT5_EN', var_SKT5_EN.get()))
	print('{} = {}'.format('var_SKT6_EN', var_SKT6_EN.get()))
	print('{} = {}'.format('var_SKT7_EN', var_SKT7_EN.get()))
	print('{} = {}'.format('var_SKT8_EN', var_SKT8_EN.get()))
	# read io data
	print('{} = {}'.format('var_CS0_GPB7', var_CS0_GPB7.get()))
	print('{} = {}'.format('var_CS0_GPB6', var_CS0_GPB6.get()))
	print('{} = {}'.format('var_CS0_GPB5', var_CS0_GPB5.get()))
	print('{} = {}'.format('var_CS0_GPB4', var_CS0_GPB4.get()))
	print('{} = {}'.format('var_CS0_GPB3', var_CS0_GPB3.get()))
	print('{} = {}'.format('var_CS0_GPB2', var_CS0_GPB2.get()))
	print('{} = {}'.format('var_CS0_GPB1', var_CS0_GPB1.get()))
	print('{} = {}'.format('var_CS0_GPB0', var_CS0_GPB0.get()))
	print('{} = {}'.format('var_CS0_GPA7', var_CS0_GPA7.get()))
	print('{} = {}'.format('var_CS0_GPA6', var_CS0_GPA6.get()))
	print('{} = {}'.format('var_CS0_GPA5', var_CS0_GPA5.get()))
	print('{} = {}'.format('var_CS0_GPA4', var_CS0_GPA4.get()))
	print('{} = {}'.format('var_CS0_GPA3', var_CS0_GPA3.get()))
	print('{} = {}'.format('var_CS0_GPA2', var_CS0_GPA2.get()))
	print('{} = {}'.format('var_CS0_GPA1', var_CS0_GPA1.get()))
	print('{} = {}'.format('var_CS0_GPA0', var_CS0_GPA0.get()))
	#
	socket_en_data = 0x00
	if var_SKT1_EN.get()==1:	socket_en_data += 0x01
	if var_SKT2_EN.get()==1:	socket_en_data += 0x02
	if var_SKT3_EN.get()==1:	socket_en_data += 0x04
	if var_SKT4_EN.get()==1:	socket_en_data += 0x08
	if var_SKT5_EN.get()==1:	socket_en_data += 0x10
	if var_SKT6_EN.get()==1:	socket_en_data += 0x20
	if var_SKT7_EN.get()==1:	socket_en_data += 0x40
	if var_SKT8_EN.get()==1:	socket_en_data += 0x80
	#
	out_data = 0x0000
	if var_CS0_GPB7.get()==1:	out_data += 0x0080
	if var_CS0_GPB6.get()==1:	out_data += 0x0040
	if var_CS0_GPB5.get()==1:	out_data += 0x0020
	if var_CS0_GPB4.get()==1:	out_data += 0x0010
	if var_CS0_GPB3.get()==1:	out_data += 0x0008
	if var_CS0_GPB2.get()==1:	out_data += 0x0004
	if var_CS0_GPB1.get()==1:	out_data += 0x0002
	if var_CS0_GPB0.get()==1:	out_data += 0x0001
	if var_CS0_GPA7.get()==1:	out_data += 0x8000
	if var_CS0_GPA6.get()==1:	out_data += 0x4000
	if var_CS0_GPA5.get()==1:	out_data += 0x2000
	if var_CS0_GPA4.get()==1:	out_data += 0x1000
	if var_CS0_GPA3.get()==1:	out_data += 0x0800
	if var_CS0_GPA2.get()==1:	out_data += 0x0400
	if var_CS0_GPA1.get()==1:	out_data += 0x0200
	if var_CS0_GPA0.get()==1:	out_data += 0x0100
	
	## send SPIO send frame
	# set socket/cs 
	# ep05 = 0xFF070001 // w_SPIO_CON_WI // socket/cs enable 
	cs_en_data = 0x01 # for CS0
	wire_data = 0
	wire_data = (socket_en_data<<24) + (cs_en_data<<16) + 1
	dev.SetWireInValue(0x05,wire_data,0xFFFFFFFF) # (ep,val,mask)
	dev.UpdateWireIns()
	# make frame data
	reg_adrs = 0x0012
	frame_data = 0
	frame_data = (reg_adrs<<16) + out_data
	# ep04 = 0x00120080 // {GPA,GPB}
	dev.SetWireInValue(0x04,frame_data,0xFFFFFFFF) # (ep,val,mask)
	dev.UpdateWireIns()
	# ep45[1]
	dev.ActivateTriggerIn(0x45, 1)
	return

def spio_bb_update_cs1():
## read check box
	print('\n>>>>>>')
	# read socket enable data
	print('{} = {}'.format('var_SKT1_EN', var_SKT1_EN.get()))
	print('{} = {}'.format('var_SKT2_EN', var_SKT2_EN.get()))
	print('{} = {}'.format('var_SKT3_EN', var_SKT3_EN.get()))
	print('{} = {}'.format('var_SKT4_EN', var_SKT4_EN.get()))
	print('{} = {}'.format('var_SKT5_EN', var_SKT5_EN.get()))
	print('{} = {}'.format('var_SKT6_EN', var_SKT6_EN.get()))
	print('{} = {}'.format('var_SKT7_EN', var_SKT7_EN.get()))
	print('{} = {}'.format('var_SKT8_EN', var_SKT8_EN.get()))
	# read io data
	print('{} = {}'.format('var_CS1_GPB7', var_CS1_GPB7.get()))
	print('{} = {}'.format('var_CS1_GPB6', var_CS1_GPB6.get()))
	print('{} = {}'.format('var_CS1_GPB5', var_CS1_GPB5.get()))
	print('{} = {}'.format('var_CS1_GPB4', var_CS1_GPB4.get()))
	print('{} = {}'.format('var_CS1_GPB3', var_CS1_GPB3.get()))
	print('{} = {}'.format('var_CS1_GPB2', var_CS1_GPB2.get()))
	print('{} = {}'.format('var_CS1_GPB1', var_CS1_GPB1.get()))
	print('{} = {}'.format('var_CS1_GPB0', var_CS1_GPB0.get()))
	print('{} = {}'.format('var_CS1_GPA7', var_CS1_GPA7.get()))
	print('{} = {}'.format('var_CS1_GPA6', var_CS1_GPA6.get()))
	print('{} = {}'.format('var_CS1_GPA5', var_CS1_GPA5.get()))
	print('{} = {}'.format('var_CS1_GPA4', var_CS1_GPA4.get()))
	print('{} = {}'.format('var_CS1_GPA3', var_CS1_GPA3.get()))
	print('{} = {}'.format('var_CS1_GPA2', var_CS1_GPA2.get()))
	print('{} = {}'.format('var_CS1_GPA1', var_CS1_GPA1.get()))
	print('{} = {}'.format('var_CS1_GPA0', var_CS1_GPA0.get()))
	#
	socket_en_data = 0x00
	if var_SKT1_EN.get()==1:	socket_en_data += 0x01
	if var_SKT2_EN.get()==1:	socket_en_data += 0x02
	if var_SKT3_EN.get()==1:	socket_en_data += 0x04
	if var_SKT4_EN.get()==1:	socket_en_data += 0x08
	if var_SKT5_EN.get()==1:	socket_en_data += 0x10
	if var_SKT6_EN.get()==1:	socket_en_data += 0x20
	if var_SKT7_EN.get()==1:	socket_en_data += 0x40
	if var_SKT8_EN.get()==1:	socket_en_data += 0x80
	#
	out_data = 0x0000
	if var_CS1_GPB7.get()==1:	out_data += 0x0080
	if var_CS1_GPB6.get()==1:	out_data += 0x0040
	if var_CS1_GPB5.get()==1:	out_data += 0x0020
	if var_CS1_GPB4.get()==1:	out_data += 0x0010
	if var_CS1_GPB3.get()==1:	out_data += 0x0008
	if var_CS1_GPB2.get()==1:	out_data += 0x0004
	if var_CS1_GPB1.get()==1:	out_data += 0x0002
	if var_CS1_GPB0.get()==1:	out_data += 0x0001
	if var_CS1_GPA7.get()==1:	out_data += 0x8000
	if var_CS1_GPA6.get()==1:	out_data += 0x4000
	if var_CS1_GPA5.get()==1:	out_data += 0x2000
	if var_CS1_GPA4.get()==1:	out_data += 0x1000
	if var_CS1_GPA3.get()==1:	out_data += 0x0800
	if var_CS1_GPA2.get()==1:	out_data += 0x0400
	if var_CS1_GPA1.get()==1:	out_data += 0x0200
	if var_CS1_GPA0.get()==1:	out_data += 0x0100
	
	## send SPIO send frame
	# set socket/cs 
	# ep05 = 0xFF070001 // w_SPIO_CON_WI // socket/cs enable 
	cs_en_data = 0x02 # for CS1
	wire_data = 0
	wire_data = (socket_en_data<<24) + (cs_en_data<<16) + 1
	dev.SetWireInValue(0x05,wire_data,0xFFFFFFFF) # (ep,val,mask)
	dev.UpdateWireIns()
	# make frame data
	reg_adrs = 0x0012
	frame_data = 0
	frame_data = (reg_adrs<<16) + out_data
	# ep04 = 0x00120080 // {GPA,GPB}
	dev.SetWireInValue(0x04,frame_data,0xFFFFFFFF) # (ep,val,mask)
	dev.UpdateWireIns()
	# ep45[1]
	dev.ActivateTriggerIn(0x45, 1)
	return

def spio_bb_update_cs2():
	pass


## MHVSU DAC functions 

def dac_init():
	# ep06 = 0x000000F1 // w_DAC_CON_WI // dac enable // all dac0/1/2/3 enabled
	dev.SetWireInValue(0x06,0x000000F1,0xFFFFFFFF) # (ep,val,mask)
	dev.UpdateWireIns()
	# ep46[2] // w_DAC_TRIG_TI // initialize
	dev.ActivateTriggerIn(0x46, 2)
	#
	pass

def dac_test():
	## test write on S3
	# ep1A = 0x000A0002 // w_DAC_S3_WI // dac data
	set_data = 0x000A0002
	dev.SetWireInValue(0x1A,set_data,0xFFFFFFFF) # (ep,val,mask)
	dev.UpdateWireIns()	
	# ep46[3] // w_DAC_TRIG_TI // update
	dev.ActivateTriggerIn(0x46, 3)
	
	## test readback from S3
	# ep3A // w_DAC_S3_WO // dac readback
	dev.UpdateWireOuts()
	readback = dev.GetWireOutValue(0x3A)
	#
	print('\n>>>>>>')
	print('{} = 0x{:08X}'.format('set_data', set_data))
	print('{} = 0x{:08X}'.format('readback', readback))
	# must ... set_data = readback
	#
	## return to '0'
	# ep1A = 0x00000000 // w_DAC_S3_WI // dac data
	set_data = 0x00000000
	dev.SetWireInValue(0x18,set_data,0xFFFFFFFF) # (ep,val,mask)
	dev.SetWireInValue(0x19,set_data,0xFFFFFFFF) # (ep,val,mask)
	dev.SetWireInValue(0x1A,set_data,0xFFFFFFFF) # (ep,val,mask)
	dev.SetWireInValue(0x1B,set_data,0xFFFFFFFF) # (ep,val,mask)
	dev.SetWireInValue(0x1C,set_data,0xFFFFFFFF) # (ep,val,mask)
	dev.SetWireInValue(0x1D,set_data,0xFFFFFFFF) # (ep,val,mask)
	dev.SetWireInValue(0x1E,set_data,0xFFFFFFFF) # (ep,val,mask)
	dev.SetWireInValue(0x1F,set_data,0xFFFFFFFF) # (ep,val,mask)
	dev.UpdateWireIns()	
	# ep46[3] // w_DAC_TRIG_TI // update
	dev.ActivateTriggerIn(0x46, 3)
	pass


# dac error scale 
#__dac_scale__ = 0.632 # 1V --> 0.632V measure
__dac_scale__ = 1.0 # no scale

def conv_dac_code__from_string(tmp_ee_dac_skt1_ch1_val):
	tmp_ee_dac_skt1_ch1_val__float = float(tmp_ee_dac_skt1_ch1_val)/__dac_scale__
	##
	#if tmp_ee_dac_skt1_ch1_val__float >= 0:
	#	var_SKT1_CH1_DAC_VAL__code = int(tmp_ee_dac_skt1_ch1_val__float/10.0*0x7FFF)
	#else:
	#	var_SKT1_CH1_DAC_VAL__code = 0xFFFF # test
	##
	var_SKT1_CH1_DAC_VAL__code = conv_dec_to_bit_2s_comp_16bit(tmp_ee_dac_skt1_ch1_val__float)
	#
	return var_SKT1_CH1_DAC_VAL__code

def  conv_string__from_dac_code(var_SKT1_CH1_DAC_RDB__code):
	tmp = var_SKT1_CH1_DAC_RDB__code
	tmp_ee_dac_skt1_ch1_rdb = conv_bit_2s_comp_16bit_to_dec(tmp)*__dac_scale__
	#
	return tmp_ee_dac_skt1_ch1_rdb

def dac_bb_update():
	## read dac input entry // ee_dac_skt3_ch1_val
	tmp_ee_dac_skt1_ch1_val = ee_dac_skt1_ch1_val.get() # string
	tmp_ee_dac_skt2_ch1_val = ee_dac_skt2_ch1_val.get() # string
	tmp_ee_dac_skt3_ch1_val = ee_dac_skt3_ch1_val.get() # string
	tmp_ee_dac_skt4_ch1_val = ee_dac_skt4_ch1_val.get() # string
	tmp_ee_dac_skt5_ch1_val = ee_dac_skt5_ch1_val.get() # string
	tmp_ee_dac_skt6_ch1_val = ee_dac_skt6_ch1_val.get() # string
	tmp_ee_dac_skt7_ch1_val = ee_dac_skt7_ch1_val.get() # string
	tmp_ee_dac_skt8_ch1_val = ee_dac_skt8_ch1_val.get() # string
	tmp_ee_dac_skt1_ch2_val = ee_dac_skt1_ch2_val.get() # string
	tmp_ee_dac_skt2_ch2_val = ee_dac_skt2_ch2_val.get() # string
	tmp_ee_dac_skt3_ch2_val = ee_dac_skt3_ch2_val.get() # string
	tmp_ee_dac_skt4_ch2_val = ee_dac_skt4_ch2_val.get() # string
	tmp_ee_dac_skt5_ch2_val = ee_dac_skt5_ch2_val.get() # string
	tmp_ee_dac_skt6_ch2_val = ee_dac_skt6_ch2_val.get() # string
	tmp_ee_dac_skt7_ch2_val = ee_dac_skt7_ch2_val.get() # string
	tmp_ee_dac_skt8_ch2_val = ee_dac_skt8_ch2_val.get() # string
	
	## convert float from string // var_SKTn_CHn_DAC_VAL # double // not must
	var_SKT1_CH1_DAC_VAL.set(float(tmp_ee_dac_skt1_ch1_val))
	var_SKT2_CH1_DAC_VAL.set(float(tmp_ee_dac_skt2_ch1_val))
	var_SKT3_CH1_DAC_VAL.set(float(tmp_ee_dac_skt3_ch1_val))
	var_SKT4_CH1_DAC_VAL.set(float(tmp_ee_dac_skt4_ch1_val))
	var_SKT5_CH1_DAC_VAL.set(float(tmp_ee_dac_skt5_ch1_val))
	var_SKT6_CH1_DAC_VAL.set(float(tmp_ee_dac_skt6_ch1_val))
	var_SKT7_CH1_DAC_VAL.set(float(tmp_ee_dac_skt7_ch1_val))
	var_SKT8_CH1_DAC_VAL.set(float(tmp_ee_dac_skt8_ch1_val))
	var_SKT1_CH2_DAC_VAL.set(float(tmp_ee_dac_skt1_ch2_val))
	var_SKT2_CH2_DAC_VAL.set(float(tmp_ee_dac_skt2_ch2_val))
	var_SKT3_CH2_DAC_VAL.set(float(tmp_ee_dac_skt3_ch2_val))
	var_SKT4_CH2_DAC_VAL.set(float(tmp_ee_dac_skt4_ch2_val))
	var_SKT5_CH2_DAC_VAL.set(float(tmp_ee_dac_skt5_ch2_val))
	var_SKT6_CH2_DAC_VAL.set(float(tmp_ee_dac_skt6_ch2_val))
	var_SKT7_CH2_DAC_VAL.set(float(tmp_ee_dac_skt7_ch2_val))
	var_SKT8_CH2_DAC_VAL.set(float(tmp_ee_dac_skt8_ch2_val))
	
	## convert hex/int code from string
	var_SKT1_CH1_DAC_VAL__code = conv_dac_code__from_string(tmp_ee_dac_skt1_ch1_val)
	var_SKT2_CH1_DAC_VAL__code = conv_dac_code__from_string(tmp_ee_dac_skt2_ch1_val)
	var_SKT3_CH1_DAC_VAL__code = conv_dac_code__from_string(tmp_ee_dac_skt3_ch1_val)
	var_SKT4_CH1_DAC_VAL__code = conv_dac_code__from_string(tmp_ee_dac_skt4_ch1_val)
	var_SKT5_CH1_DAC_VAL__code = conv_dac_code__from_string(tmp_ee_dac_skt5_ch1_val)
	var_SKT6_CH1_DAC_VAL__code = conv_dac_code__from_string(tmp_ee_dac_skt6_ch1_val)
	var_SKT7_CH1_DAC_VAL__code = conv_dac_code__from_string(tmp_ee_dac_skt7_ch1_val)
	var_SKT8_CH1_DAC_VAL__code = conv_dac_code__from_string(tmp_ee_dac_skt8_ch1_val)
	var_SKT1_CH2_DAC_VAL__code = conv_dac_code__from_string(tmp_ee_dac_skt1_ch2_val)
	var_SKT2_CH2_DAC_VAL__code = conv_dac_code__from_string(tmp_ee_dac_skt2_ch2_val)
	var_SKT3_CH2_DAC_VAL__code = conv_dac_code__from_string(tmp_ee_dac_skt3_ch2_val)
	var_SKT4_CH2_DAC_VAL__code = conv_dac_code__from_string(tmp_ee_dac_skt4_ch2_val)
	var_SKT5_CH2_DAC_VAL__code = conv_dac_code__from_string(tmp_ee_dac_skt5_ch2_val)
	var_SKT6_CH2_DAC_VAL__code = conv_dac_code__from_string(tmp_ee_dac_skt6_ch2_val)
	var_SKT7_CH2_DAC_VAL__code = conv_dac_code__from_string(tmp_ee_dac_skt7_ch2_val)
	var_SKT8_CH2_DAC_VAL__code = conv_dac_code__from_string(tmp_ee_dac_skt8_ch2_val)
	
	## set wire_in 
	set_data_S1 = (var_SKT1_CH2_DAC_VAL__code<<16) + var_SKT1_CH1_DAC_VAL__code # 0 # 0x00000001 # // w_DAC_S1_WI  = ep18wire;
	set_data_S2 = (var_SKT2_CH2_DAC_VAL__code<<16) + var_SKT2_CH1_DAC_VAL__code # 0 # 0x00000002 # // w_DAC_S2_WI  = ep19wire;
	set_data_S3 = (var_SKT3_CH2_DAC_VAL__code<<16) + var_SKT3_CH1_DAC_VAL__code # 0 # 0x00000003 # // w_DAC_S3_WI  = ep1Awire;
	set_data_S4 = (var_SKT4_CH2_DAC_VAL__code<<16) + var_SKT4_CH1_DAC_VAL__code # 0 # 0x00000004 # // w_DAC_S4_WI  = ep1Bwire;
	set_data_S5 = (var_SKT5_CH2_DAC_VAL__code<<16) + var_SKT5_CH1_DAC_VAL__code # 0 # 0x00000005 # // w_DAC_S5_WI  = ep1Cwire;
	set_data_S6 = (var_SKT6_CH2_DAC_VAL__code<<16) + var_SKT6_CH1_DAC_VAL__code # 0 # 0x00000006 # // w_DAC_S6_WI  = ep1Dwire;
	set_data_S7 = (var_SKT7_CH2_DAC_VAL__code<<16) + var_SKT7_CH1_DAC_VAL__code # 0 # 0x00000007 # // w_DAC_S7_WI  = ep1Ewire;
	set_data_S8 = (var_SKT8_CH2_DAC_VAL__code<<16) + var_SKT8_CH1_DAC_VAL__code # 0 # 0x00000008 # // w_DAC_S8_WI  = ep1Fwire;
	dev.SetWireInValue(0x18,set_data_S1,0xFFFFFFFF) # (ep,val,mask)
	dev.SetWireInValue(0x19,set_data_S2,0xFFFFFFFF) # (ep,val,mask)
	dev.SetWireInValue(0x1A,set_data_S3,0xFFFFFFFF) # (ep,val,mask)
	dev.SetWireInValue(0x1B,set_data_S4,0xFFFFFFFF) # (ep,val,mask)
	dev.SetWireInValue(0x1C,set_data_S5,0xFFFFFFFF) # (ep,val,mask)
	dev.SetWireInValue(0x1D,set_data_S6,0xFFFFFFFF) # (ep,val,mask)
	dev.SetWireInValue(0x1E,set_data_S7,0xFFFFFFFF) # (ep,val,mask)
	dev.SetWireInValue(0x1F,set_data_S8,0xFFFFFFFF) # (ep,val,mask)
	dev.UpdateWireIns()	
	
	## trig update 
	# ep46[3] // w_DAC_TRIG_TI // update
	dev.ActivateTriggerIn(0x46, 3)
	
	## readback wire_out
	dev.UpdateWireOuts()
	readback_S1 = dev.GetWireOutValue(0x38) # // ep38wire = w_DAC_S1_WO 
	readback_S2 = dev.GetWireOutValue(0x39) # // ep39wire = w_DAC_S2_WO 
	readback_S3 = dev.GetWireOutValue(0x3A) # // ep3Awire = w_DAC_S3_WO 
	readback_S4 = dev.GetWireOutValue(0x3B) # // ep3Bwire = w_DAC_S4_WO 
	readback_S5 = dev.GetWireOutValue(0x3C) # // ep3Cwire = w_DAC_S5_WO 
	readback_S6 = dev.GetWireOutValue(0x3D) # // ep3Dwire = w_DAC_S6_WO 
	readback_S7 = dev.GetWireOutValue(0x3E) # // ep3Ewire = w_DAC_S7_WO 
	readback_S8 = dev.GetWireOutValue(0x3F) # // ep3Fwire = w_DAC_S8_WO 
	
	## readback codes
	var_SKT1_CH1_DAC_RDB__code = (readback_S1 >>  0) & 0x0000FFFF
	var_SKT2_CH1_DAC_RDB__code = (readback_S2 >>  0) & 0x0000FFFF
	var_SKT3_CH1_DAC_RDB__code = (readback_S3 >>  0) & 0x0000FFFF
	var_SKT4_CH1_DAC_RDB__code = (readback_S4 >>  0) & 0x0000FFFF
	var_SKT5_CH1_DAC_RDB__code = (readback_S5 >>  0) & 0x0000FFFF
	var_SKT6_CH1_DAC_RDB__code = (readback_S6 >>  0) & 0x0000FFFF
	var_SKT7_CH1_DAC_RDB__code = (readback_S7 >>  0) & 0x0000FFFF
	var_SKT8_CH1_DAC_RDB__code = (readback_S8 >>  0) & 0x0000FFFF
	var_SKT1_CH2_DAC_RDB__code = (readback_S1 >> 16) & 0x0000FFFF
	var_SKT2_CH2_DAC_RDB__code = (readback_S2 >> 16) & 0x0000FFFF
	var_SKT3_CH2_DAC_RDB__code = (readback_S3 >> 16) & 0x0000FFFF
	var_SKT4_CH2_DAC_RDB__code = (readback_S4 >> 16) & 0x0000FFFF
	var_SKT5_CH2_DAC_RDB__code = (readback_S5 >> 16) & 0x0000FFFF
	var_SKT6_CH2_DAC_RDB__code = (readback_S6 >> 16) & 0x0000FFFF
	var_SKT7_CH2_DAC_RDB__code = (readback_S7 >> 16) & 0x0000FFFF
	var_SKT8_CH2_DAC_RDB__code = (readback_S8 >> 16) & 0x0000FFFF
	
	## convert string from hex/int code
	tmp_ee_dac_skt1_ch1_rdb = conv_string__from_dac_code(var_SKT1_CH1_DAC_RDB__code)
	tmp_ee_dac_skt2_ch1_rdb = conv_string__from_dac_code(var_SKT2_CH1_DAC_RDB__code)
	tmp_ee_dac_skt3_ch1_rdb = conv_string__from_dac_code(var_SKT3_CH1_DAC_RDB__code)
	tmp_ee_dac_skt4_ch1_rdb = conv_string__from_dac_code(var_SKT4_CH1_DAC_RDB__code)
	tmp_ee_dac_skt5_ch1_rdb = conv_string__from_dac_code(var_SKT5_CH1_DAC_RDB__code)
	tmp_ee_dac_skt6_ch1_rdb = conv_string__from_dac_code(var_SKT6_CH1_DAC_RDB__code)
	tmp_ee_dac_skt7_ch1_rdb = conv_string__from_dac_code(var_SKT7_CH1_DAC_RDB__code)
	tmp_ee_dac_skt8_ch1_rdb = conv_string__from_dac_code(var_SKT8_CH1_DAC_RDB__code)
	tmp_ee_dac_skt1_ch2_rdb = conv_string__from_dac_code(var_SKT1_CH2_DAC_RDB__code)
	tmp_ee_dac_skt2_ch2_rdb = conv_string__from_dac_code(var_SKT2_CH2_DAC_RDB__code)
	tmp_ee_dac_skt3_ch2_rdb = conv_string__from_dac_code(var_SKT3_CH2_DAC_RDB__code)
	tmp_ee_dac_skt4_ch2_rdb = conv_string__from_dac_code(var_SKT4_CH2_DAC_RDB__code)
	tmp_ee_dac_skt5_ch2_rdb = conv_string__from_dac_code(var_SKT5_CH2_DAC_RDB__code)
	tmp_ee_dac_skt6_ch2_rdb = conv_string__from_dac_code(var_SKT6_CH2_DAC_RDB__code)
	tmp_ee_dac_skt7_ch2_rdb = conv_string__from_dac_code(var_SKT7_CH2_DAC_RDB__code)
	tmp_ee_dac_skt8_ch2_rdb = conv_string__from_dac_code(var_SKT8_CH2_DAC_RDB__code)
	
	## convert float // var_SKTn_CHn_DAC_RDB
	
	
	## set readback entry // ee_dac_skt3_ch1_rdb
	ee_dac_skt1_ch1_rdb.delete(0,END)
	ee_dac_skt2_ch1_rdb.delete(0,END)
	ee_dac_skt3_ch1_rdb.delete(0,END)
	ee_dac_skt4_ch1_rdb.delete(0,END)
	ee_dac_skt5_ch1_rdb.delete(0,END)
	ee_dac_skt6_ch1_rdb.delete(0,END)
	ee_dac_skt7_ch1_rdb.delete(0,END)
	ee_dac_skt8_ch1_rdb.delete(0,END)
	ee_dac_skt1_ch2_rdb.delete(0,END)
	ee_dac_skt2_ch2_rdb.delete(0,END)
	ee_dac_skt3_ch2_rdb.delete(0,END)
	ee_dac_skt4_ch2_rdb.delete(0,END)
	ee_dac_skt5_ch2_rdb.delete(0,END)
	ee_dac_skt6_ch2_rdb.delete(0,END)
	ee_dac_skt7_ch2_rdb.delete(0,END)
	ee_dac_skt8_ch2_rdb.delete(0,END)
	#
	ee_dac_skt1_ch1_rdb.insert(0,tmp_ee_dac_skt1_ch1_rdb)
	ee_dac_skt2_ch1_rdb.insert(0,tmp_ee_dac_skt2_ch1_rdb)
	ee_dac_skt3_ch1_rdb.insert(0,tmp_ee_dac_skt3_ch1_rdb)
	ee_dac_skt4_ch1_rdb.insert(0,tmp_ee_dac_skt4_ch1_rdb)
	ee_dac_skt5_ch1_rdb.insert(0,tmp_ee_dac_skt5_ch1_rdb)
	ee_dac_skt6_ch1_rdb.insert(0,tmp_ee_dac_skt6_ch1_rdb)
	ee_dac_skt7_ch1_rdb.insert(0,tmp_ee_dac_skt7_ch1_rdb)
	ee_dac_skt8_ch1_rdb.insert(0,tmp_ee_dac_skt8_ch1_rdb)
	ee_dac_skt1_ch2_rdb.insert(0,tmp_ee_dac_skt1_ch2_rdb)
	ee_dac_skt2_ch2_rdb.insert(0,tmp_ee_dac_skt2_ch2_rdb)
	ee_dac_skt3_ch2_rdb.insert(0,tmp_ee_dac_skt3_ch2_rdb)
	ee_dac_skt4_ch2_rdb.insert(0,tmp_ee_dac_skt4_ch2_rdb)
	ee_dac_skt5_ch2_rdb.insert(0,tmp_ee_dac_skt5_ch2_rdb)
	ee_dac_skt6_ch2_rdb.insert(0,tmp_ee_dac_skt6_ch2_rdb)
	ee_dac_skt7_ch2_rdb.insert(0,tmp_ee_dac_skt7_ch2_rdb)
	ee_dac_skt8_ch2_rdb.insert(0,tmp_ee_dac_skt8_ch2_rdb)

	## report
	print('\n>>>>>>')
	print('{} = {}'.format('tmp_ee_dac_skt1_ch1_val', tmp_ee_dac_skt1_ch1_val))
	print('{} = {}'.format('tmp_ee_dac_skt2_ch1_val', tmp_ee_dac_skt2_ch1_val))
	print('{} = {}'.format('tmp_ee_dac_skt3_ch1_val', tmp_ee_dac_skt3_ch1_val))
	print('{} = {}'.format('tmp_ee_dac_skt4_ch1_val', tmp_ee_dac_skt4_ch1_val))
	print('{} = {}'.format('tmp_ee_dac_skt5_ch1_val', tmp_ee_dac_skt5_ch1_val))
	print('{} = {}'.format('tmp_ee_dac_skt6_ch1_val', tmp_ee_dac_skt6_ch1_val))
	print('{} = {}'.format('tmp_ee_dac_skt7_ch1_val', tmp_ee_dac_skt7_ch1_val))
	print('{} = {}'.format('tmp_ee_dac_skt8_ch1_val', tmp_ee_dac_skt8_ch1_val))
	print('{} = {}'.format('tmp_ee_dac_skt1_ch2_val', tmp_ee_dac_skt1_ch2_val))
	print('{} = {}'.format('tmp_ee_dac_skt2_ch2_val', tmp_ee_dac_skt2_ch2_val))
	print('{} = {}'.format('tmp_ee_dac_skt3_ch2_val', tmp_ee_dac_skt3_ch2_val))
	print('{} = {}'.format('tmp_ee_dac_skt4_ch2_val', tmp_ee_dac_skt4_ch2_val))
	print('{} = {}'.format('tmp_ee_dac_skt5_ch2_val', tmp_ee_dac_skt5_ch2_val))
	print('{} = {}'.format('tmp_ee_dac_skt6_ch2_val', tmp_ee_dac_skt6_ch2_val))
	print('{} = {}'.format('tmp_ee_dac_skt7_ch2_val', tmp_ee_dac_skt7_ch2_val))
	print('{} = {}'.format('tmp_ee_dac_skt8_ch2_val', tmp_ee_dac_skt8_ch2_val))
	print('\n>>>>>>')
	print('{} = {}'.format('var_SKT1_CH1_DAC_VAL', var_SKT1_CH1_DAC_VAL.get()))
	print('{} = {}'.format('var_SKT2_CH1_DAC_VAL', var_SKT2_CH1_DAC_VAL.get()))
	print('{} = {}'.format('var_SKT3_CH1_DAC_VAL', var_SKT3_CH1_DAC_VAL.get()))
	print('{} = {}'.format('var_SKT4_CH1_DAC_VAL', var_SKT4_CH1_DAC_VAL.get()))
	print('{} = {}'.format('var_SKT5_CH1_DAC_VAL', var_SKT5_CH1_DAC_VAL.get()))
	print('{} = {}'.format('var_SKT6_CH1_DAC_VAL', var_SKT6_CH1_DAC_VAL.get()))
	print('{} = {}'.format('var_SKT7_CH1_DAC_VAL', var_SKT7_CH1_DAC_VAL.get()))
	print('{} = {}'.format('var_SKT8_CH1_DAC_VAL', var_SKT8_CH1_DAC_VAL.get()))
	print('{} = {}'.format('var_SKT1_CH2_DAC_VAL', var_SKT1_CH2_DAC_VAL.get()))
	print('{} = {}'.format('var_SKT2_CH2_DAC_VAL', var_SKT2_CH2_DAC_VAL.get()))
	print('{} = {}'.format('var_SKT3_CH2_DAC_VAL', var_SKT3_CH2_DAC_VAL.get()))
	print('{} = {}'.format('var_SKT4_CH2_DAC_VAL', var_SKT4_CH2_DAC_VAL.get()))
	print('{} = {}'.format('var_SKT5_CH2_DAC_VAL', var_SKT5_CH2_DAC_VAL.get()))
	print('{} = {}'.format('var_SKT6_CH2_DAC_VAL', var_SKT6_CH2_DAC_VAL.get()))
	print('{} = {}'.format('var_SKT7_CH2_DAC_VAL', var_SKT7_CH2_DAC_VAL.get()))
	print('{} = {}'.format('var_SKT8_CH2_DAC_VAL', var_SKT8_CH2_DAC_VAL.get()))
	print('\n>>>>>>')
	print('{} = 0x{:04X}'.format('var_SKT1_CH1_DAC_VAL__code', var_SKT1_CH1_DAC_VAL__code))
	print('{} = 0x{:04X}'.format('var_SKT2_CH1_DAC_VAL__code', var_SKT2_CH1_DAC_VAL__code))
	print('{} = 0x{:04X}'.format('var_SKT3_CH1_DAC_VAL__code', var_SKT3_CH1_DAC_VAL__code))
	print('{} = 0x{:04X}'.format('var_SKT4_CH1_DAC_VAL__code', var_SKT4_CH1_DAC_VAL__code))
	print('{} = 0x{:04X}'.format('var_SKT5_CH1_DAC_VAL__code', var_SKT5_CH1_DAC_VAL__code))
	print('{} = 0x{:04X}'.format('var_SKT6_CH1_DAC_VAL__code', var_SKT6_CH1_DAC_VAL__code))
	print('{} = 0x{:04X}'.format('var_SKT7_CH1_DAC_VAL__code', var_SKT7_CH1_DAC_VAL__code))
	print('{} = 0x{:04X}'.format('var_SKT8_CH1_DAC_VAL__code', var_SKT8_CH1_DAC_VAL__code))
	print('{} = 0x{:04X}'.format('var_SKT1_CH2_DAC_VAL__code', var_SKT1_CH2_DAC_VAL__code))
	print('{} = 0x{:04X}'.format('var_SKT2_CH2_DAC_VAL__code', var_SKT2_CH2_DAC_VAL__code))
	print('{} = 0x{:04X}'.format('var_SKT3_CH2_DAC_VAL__code', var_SKT3_CH2_DAC_VAL__code))
	print('{} = 0x{:04X}'.format('var_SKT4_CH2_DAC_VAL__code', var_SKT4_CH2_DAC_VAL__code))
	print('{} = 0x{:04X}'.format('var_SKT5_CH2_DAC_VAL__code', var_SKT5_CH2_DAC_VAL__code))
	print('{} = 0x{:04X}'.format('var_SKT6_CH2_DAC_VAL__code', var_SKT6_CH2_DAC_VAL__code))
	print('{} = 0x{:04X}'.format('var_SKT7_CH2_DAC_VAL__code', var_SKT7_CH2_DAC_VAL__code))
	print('{} = 0x{:04X}'.format('var_SKT8_CH2_DAC_VAL__code', var_SKT8_CH2_DAC_VAL__code))
	print('\n>>>>>>')
	print('{} = 0x{:08X}'.format('set_data_S1', set_data_S1))
	print('{} = 0x{:08X}'.format('set_data_S2', set_data_S2))
	print('{} = 0x{:08X}'.format('set_data_S3', set_data_S3))
	print('{} = 0x{:08X}'.format('set_data_S4', set_data_S4))
	print('{} = 0x{:08X}'.format('set_data_S5', set_data_S5))
	print('{} = 0x{:08X}'.format('set_data_S6', set_data_S6))
	print('{} = 0x{:08X}'.format('set_data_S7', set_data_S7))
	print('{} = 0x{:08X}'.format('set_data_S8', set_data_S8))
	print('\n>>>>>>')
	print('{} = 0x{:08X}'.format('readback_S1', readback_S1))
	print('{} = 0x{:08X}'.format('readback_S2', readback_S2))
	print('{} = 0x{:08X}'.format('readback_S3', readback_S3))
	print('{} = 0x{:08X}'.format('readback_S4', readback_S4))
	print('{} = 0x{:08X}'.format('readback_S5', readback_S5))
	print('{} = 0x{:08X}'.format('readback_S6', readback_S6))
	print('{} = 0x{:08X}'.format('readback_S7', readback_S7))
	print('{} = 0x{:08X}'.format('readback_S8', readback_S8))
	#
	print('\n>>>>>>')
	print('{} = 0x{:04X}'.format('var_SKT1_CH1_DAC_RDB__code', var_SKT1_CH1_DAC_RDB__code))
	print('{} = 0x{:04X}'.format('var_SKT2_CH1_DAC_RDB__code', var_SKT2_CH1_DAC_RDB__code))
	print('{} = 0x{:04X}'.format('var_SKT3_CH1_DAC_RDB__code', var_SKT3_CH1_DAC_RDB__code))
	print('{} = 0x{:04X}'.format('var_SKT4_CH1_DAC_RDB__code', var_SKT4_CH1_DAC_RDB__code))
	print('{} = 0x{:04X}'.format('var_SKT5_CH1_DAC_RDB__code', var_SKT5_CH1_DAC_RDB__code))
	print('{} = 0x{:04X}'.format('var_SKT6_CH1_DAC_RDB__code', var_SKT6_CH1_DAC_RDB__code))
	print('{} = 0x{:04X}'.format('var_SKT7_CH1_DAC_RDB__code', var_SKT7_CH1_DAC_RDB__code))
	print('{} = 0x{:04X}'.format('var_SKT8_CH1_DAC_RDB__code', var_SKT8_CH1_DAC_RDB__code))
	print('{} = 0x{:04X}'.format('var_SKT1_CH2_DAC_RDB__code', var_SKT1_CH2_DAC_RDB__code))
	print('{} = 0x{:04X}'.format('var_SKT2_CH2_DAC_RDB__code', var_SKT2_CH2_DAC_RDB__code))
	print('{} = 0x{:04X}'.format('var_SKT3_CH2_DAC_RDB__code', var_SKT3_CH2_DAC_RDB__code))
	print('{} = 0x{:04X}'.format('var_SKT4_CH2_DAC_RDB__code', var_SKT4_CH2_DAC_RDB__code))
	print('{} = 0x{:04X}'.format('var_SKT5_CH2_DAC_RDB__code', var_SKT5_CH2_DAC_RDB__code))
	print('{} = 0x{:04X}'.format('var_SKT6_CH2_DAC_RDB__code', var_SKT6_CH2_DAC_RDB__code))
	print('{} = 0x{:04X}'.format('var_SKT7_CH2_DAC_RDB__code', var_SKT7_CH2_DAC_RDB__code))
	print('{} = 0x{:04X}'.format('var_SKT8_CH2_DAC_RDB__code', var_SKT8_CH2_DAC_RDB__code))
	#
	print('\n>>>>>>')
	print('{} = {}'.format('tmp_ee_dac_skt1_ch1_rdb', tmp_ee_dac_skt1_ch1_rdb))
	print('{} = {}'.format('tmp_ee_dac_skt2_ch1_rdb', tmp_ee_dac_skt2_ch1_rdb))
	print('{} = {}'.format('tmp_ee_dac_skt3_ch1_rdb', tmp_ee_dac_skt3_ch1_rdb))
	print('{} = {}'.format('tmp_ee_dac_skt4_ch1_rdb', tmp_ee_dac_skt4_ch1_rdb))
	print('{} = {}'.format('tmp_ee_dac_skt5_ch1_rdb', tmp_ee_dac_skt5_ch1_rdb))
	print('{} = {}'.format('tmp_ee_dac_skt6_ch1_rdb', tmp_ee_dac_skt6_ch1_rdb))
	print('{} = {}'.format('tmp_ee_dac_skt7_ch1_rdb', tmp_ee_dac_skt7_ch1_rdb))
	print('{} = {}'.format('tmp_ee_dac_skt8_ch1_rdb', tmp_ee_dac_skt8_ch1_rdb))
	print('{} = {}'.format('tmp_ee_dac_skt1_ch2_rdb', tmp_ee_dac_skt1_ch2_rdb))
	print('{} = {}'.format('tmp_ee_dac_skt2_ch2_rdb', tmp_ee_dac_skt2_ch2_rdb))
	print('{} = {}'.format('tmp_ee_dac_skt3_ch2_rdb', tmp_ee_dac_skt3_ch2_rdb))
	print('{} = {}'.format('tmp_ee_dac_skt4_ch2_rdb', tmp_ee_dac_skt4_ch2_rdb))
	print('{} = {}'.format('tmp_ee_dac_skt5_ch2_rdb', tmp_ee_dac_skt5_ch2_rdb))
	print('{} = {}'.format('tmp_ee_dac_skt6_ch2_rdb', tmp_ee_dac_skt6_ch2_rdb))
	print('{} = {}'.format('tmp_ee_dac_skt7_ch2_rdb', tmp_ee_dac_skt7_ch2_rdb))
	print('{} = {}'.format('tmp_ee_dac_skt8_ch2_rdb', tmp_ee_dac_skt8_ch2_rdb))
	#
	pass


## gui setup
def tk_win_setup(master):
	
	## initialize SPIO 
	spio_init()
	
	## test update SPIO 
	spio_test_led_on()
	

	## initialize DAC 
	dac_init()
	
	## test update DAC
	dac_test()
	
	
	## buttons for SPIO 
	Label(master, text="[SPIO] SKT# selection:  ").grid(row=0, column= 0, sticky=W, columnspan = 1)
	Label(master, text="[SPIO] CS0 outputs:     ").grid(row=0, column= 1, sticky=W, columnspan = 1)
	Label(master, text="[SPIO] CS1 outputs:     ").grid(row=0, column= 2, sticky=W, columnspan = 1)
	Label(master, text="[SPIO] CS2 outputs:     ").grid(row=0, column= 3, sticky=W, columnspan = 1)
	# 
	Checkbutton(master, text="SKT1 ENABLE", variable=var_SKT1_EN).grid(row=1, column=0, sticky=W)
	Checkbutton(master, text="SKT2 ENABLE", variable=var_SKT2_EN).grid(row=2, column=0, sticky=W)
	Checkbutton(master, text="SKT3 ENABLE", variable=var_SKT3_EN).grid(row=3, column=0, sticky=W)
	Checkbutton(master, text="SKT4 ENABLE", variable=var_SKT4_EN).grid(row=4, column=0, sticky=W)
	Checkbutton(master, text="SKT5 ENABLE", variable=var_SKT5_EN).grid(row=5, column=0, sticky=W)
	Checkbutton(master, text="SKT6 ENABLE", variable=var_SKT6_EN).grid(row=6, column=0, sticky=W)
	Checkbutton(master, text="SKT7 ENABLE", variable=var_SKT7_EN).grid(row=7, column=0, sticky=W)
	Checkbutton(master, text="SKT8 ENABLE", variable=var_SKT8_EN).grid(row=8, column=0, sticky=W)
	#
	Checkbutton(master, text="CS0 GPB7 CHECK_LED0     ", variable=var_CS0_GPB7).grid(row= 1, column=1, sticky=W) # CS0 GPB7 CHECK_LED0     
	Checkbutton(master, text="CS0 GPB6 CH2_ADC_IM     ", variable=var_CS0_GPB6).grid(row= 2, column=1, sticky=W) # CS0 GPB6 CH2_ADC_IM     
	Checkbutton(master, text="CS0 GPB5 CH2_GAIN_X20   ", variable=var_CS0_GPB5).grid(row= 3, column=1, sticky=W) # CS0 GPB5 CH2_GAIN_X20   
	Checkbutton(master, text="CS0 GPB4 CH2_GAIN_X2    ", variable=var_CS0_GPB4).grid(row= 4, column=1, sticky=W) # CS0 GPB4 CH2_GAIN_X2    
	Checkbutton(master, text="CS0 GPB3 NA             ", variable=var_CS0_GPB3).grid(row= 5, column=1, sticky=W) # CS0 GPB3 NA             
	Checkbutton(master, text="CS0 GPB2 CH2_RANGE_20uA ", variable=var_CS0_GPB2).grid(row= 6, column=1, sticky=W) # CS0 GPB2 CH2_RANGE_20uA 
	Checkbutton(master, text="CS0 GPB1 CH2_RANGE_2mA  ", variable=var_CS0_GPB1).grid(row= 7, column=1, sticky=W) # CS0 GPB1 CH2_RANGE_2mA  
	Checkbutton(master, text="CS0 GPB0 CH2_RANGE_50mA ", variable=var_CS0_GPB0).grid(row= 8, column=1, sticky=W) # CS0 GPB0 CH2_RANGE_50mA 
	Checkbutton(master, text="CS0 GPA7 NA             ", variable=var_CS0_GPA7).grid(row=11, column=1, sticky=W) # CS0 GPA7 NA             
	Checkbutton(master, text="CS0 GPA6 CH1_ADC_IM     ", variable=var_CS0_GPA6).grid(row=12, column=1, sticky=W) # CS0 GPA6 CH1_ADC_IM     
	Checkbutton(master, text="CS0 GPA5 CH1_GAIN_X20   ", variable=var_CS0_GPA5).grid(row=13, column=1, sticky=W) # CS0 GPA5 CH1_GAIN_X20   
	Checkbutton(master, text="CS0 GPA4 CH1_GAIN_X2    ", variable=var_CS0_GPA4).grid(row=14, column=1, sticky=W) # CS0 GPA4 CH1_GAIN_X2    
	Checkbutton(master, text="CS0 GPA3 NA             ", variable=var_CS0_GPA3).grid(row=15, column=1, sticky=W) # CS0 GPA3 NA             
	Checkbutton(master, text="CS0 GPA2 CH1_RANGE_20uA ", variable=var_CS0_GPA2).grid(row=16, column=1, sticky=W) # CS0 GPA2 CH1_RANGE_20uA 
	Checkbutton(master, text="CS0 GPA1 CH1_RANGE_2mA  ", variable=var_CS0_GPA1).grid(row=17, column=1, sticky=W) # CS0 GPA1 CH1_RANGE_2mA  
	Checkbutton(master, text="CS0 GPA0 CH1_RANGE_50mA ", variable=var_CS0_GPA0).grid(row=18, column=1, sticky=W) # CS0 GPA0 CH1_RANGE_50mA 
	#
	Checkbutton(master, text="CS1 GPB7 CHECK_LED1      ", variable=var_CS1_GPB7).grid(row= 1, column=2, sticky=W) # CS1 GPB7 CHECK_LED1       
	Checkbutton(master, text="CS1 GPB6 CH2_RELAY_DIAG  ", variable=var_CS1_GPB6).grid(row= 2, column=2, sticky=W) # CS1 GPB6 CH2_RELAY_DIAG   
	Checkbutton(master, text="CS1 GPB5 CH2_RELAY_OUTPUT", variable=var_CS1_GPB5).grid(row= 3, column=2, sticky=W) # CS1 GPB5 CH2_RELAY_OUTPUT 
	Checkbutton(master, text="CS1 GPB4 CH2_LIMIT_50mA  ", variable=var_CS1_GPB4).grid(row= 4, column=2, sticky=W) # CS1 GPB4 CH2_LIMIT_50mA   
	Checkbutton(master, text="CS1 GPB3 NA              ", variable=var_CS1_GPB3).grid(row= 5, column=2, sticky=W) # CS1 GPB3 NA               
	Checkbutton(master, text="CS1 GPB2 NA              ", variable=var_CS1_GPB2).grid(row= 6, column=2, sticky=W) # CS1 GPB2 NA               
	Checkbutton(master, text="CS1 GPB1 CH2_CMP_330p    ", variable=var_CS1_GPB1).grid(row= 7, column=2, sticky=W) # CS1 GPB1 CH2_CMP_330p     
	Checkbutton(master, text="CS1 GPB0 CH2_CMP_100p    ", variable=var_CS1_GPB0).grid(row= 8, column=2, sticky=W) # CS1 GPB0 CH2_CMP_100p     
	Checkbutton(master, text="CS1 GPA7 NA              ", variable=var_CS1_GPA7).grid(row=11, column=2, sticky=W) # CS1 GPA7 NA               
	Checkbutton(master, text="CS1 GPA6 CH1_RELAY_DIAG  ", variable=var_CS1_GPA6).grid(row=12, column=2, sticky=W) # CS1 GPA6 CH1_RELAY_DIAG   
	Checkbutton(master, text="CS1 GPA5 CH1_RELAY_OUTPUT", variable=var_CS1_GPA5).grid(row=13, column=2, sticky=W) # CS1 GPA5 CH1_RELAY_OUTPUT 
	Checkbutton(master, text="CS1 GPA4 CH1_LIMIT_50mA  ", variable=var_CS1_GPA4).grid(row=14, column=2, sticky=W) # CS1 GPA4 CH1_LIMIT_50mA   
	Checkbutton(master, text="CS1 GPA3 NA              ", variable=var_CS1_GPA3).grid(row=15, column=2, sticky=W) # CS1 GPA3 NA               
	Checkbutton(master, text="CS1 GPA2 NA              ", variable=var_CS1_GPA2).grid(row=16, column=2, sticky=W) # CS1 GPA2 NA               
	Checkbutton(master, text="CS1 GPA1 CH1_CMP_330p    ", variable=var_CS1_GPA1).grid(row=17, column=2, sticky=W) # CS1 GPA1 CH1_CMP_330p     
	Checkbutton(master, text="CS1 GPA0 CH1_CMP_100p    ", variable=var_CS1_GPA0).grid(row=18, column=2, sticky=W) # CS1 GPA0 CH1_CMP_100p     
	#
	Checkbutton(master, text="CS2 GPB7 NA   ", variable=var_CS2_GPB7).grid(row= 1, column=3, sticky=W) # CS2 GPB7 NA
	Checkbutton(master, text="CS2 GPB6 NA   ", variable=var_CS2_GPB6).grid(row= 2, column=3, sticky=W) # CS2 GPB6 NA
	Checkbutton(master, text="CS2 GPB5 NA   ", variable=var_CS2_GPB5).grid(row= 3, column=3, sticky=W) # CS2 GPB5 NA
	Checkbutton(master, text="CS2 GPB4 NA   ", variable=var_CS2_GPB4).grid(row= 4, column=3, sticky=W) # CS2 GPB4 NA
	Checkbutton(master, text="CS2 GPB3 NA   ", variable=var_CS2_GPB3).grid(row= 5, column=3, sticky=W) # CS2 GPB3 NA
	Checkbutton(master, text="CS2 GPB2 NA   ", variable=var_CS2_GPB2).grid(row= 6, column=3, sticky=W) # CS2 GPB2 NA
	Checkbutton(master, text="CS2 GPB1 NA   ", variable=var_CS2_GPB1).grid(row= 7, column=3, sticky=W) # CS2 GPB1 NA
	Checkbutton(master, text="CS2 GPB0 NA   ", variable=var_CS2_GPB0).grid(row= 8, column=3, sticky=W) # CS2 GPB0 NA
	Checkbutton(master, text="CS2 GPA7 NA   ", variable=var_CS2_GPA7).grid(row=11, column=3, sticky=W) # CS2 GPA7 NA
	Checkbutton(master, text="CS2 GPA6 NA   ", variable=var_CS2_GPA6).grid(row=12, column=3, sticky=W) # CS2 GPA6 NA
	Checkbutton(master, text="CS2 GPA5 NA   ", variable=var_CS2_GPA5).grid(row=13, column=3, sticky=W) # CS2 GPA5 NA
	Checkbutton(master, text="CS2 GPA4 NA   ", variable=var_CS2_GPA4).grid(row=14, column=3, sticky=W) # CS2 GPA4 NA
	Checkbutton(master, text="CS2 GPA3 NA   ", variable=var_CS2_GPA3).grid(row=15, column=3, sticky=W) # CS2 GPA3 NA
	Checkbutton(master, text="CS2 GPA2 NA   ", variable=var_CS2_GPA2).grid(row=16, column=3, sticky=W) # CS2 GPA2 NA
	Checkbutton(master, text="CS2 GPA1 NA   ", variable=var_CS2_GPA1).grid(row=17, column=3, sticky=W) # CS2 GPA1 NA
	Checkbutton(master, text="CS2 GPA0 NA   ", variable=var_CS2_GPA0).grid(row=18, column=3, sticky=W) # CS2 GPA0 NA
	
	# update SPIO values
	bb_spio_cs0_update = Button(master, text='update SPIO CS0 values', command=spio_bb_update_cs0) .grid(row=20, column=1, columnspan = 1, sticky=W+E, pady=4)
	bb_spio_cs1_update = Button(master, text='update SPIO CS1 values', command=spio_bb_update_cs1) .grid(row=20, column=2, columnspan = 1, sticky=W+E, pady=4)
	bb_spio_cs2_update = Button(master, text='update SPIO CS2 values', command=spio_bb_update_cs2) .grid(row=20, column=3, columnspan = 1, sticky=W+E, pady=4)
	
	
	## buttons/labels/entries for DAC
	#
	Label(master, text="[DAC] CH1 Value-in:     ").grid(row=30, column= 0, sticky=E, columnspan = 2)
	Label(master, text="[DAC] CH2 Value-in:     ").grid(row=30, column= 2, sticky=E, columnspan = 2)
	Label(master, text="[DAC] CH1 Readback:     ").grid(row=30, column= 4, sticky=E, columnspan = 2)
	Label(master, text="[DAC] CH2 Readback:     ").grid(row=30, column= 6, sticky=E, columnspan = 2)
	# DAC values
	Label(master, text=" DAC_SKT1_CH1_VAL=").grid(row=31, column=0, sticky=W)
	Label(master, text=" DAC_SKT2_CH1_VAL=").grid(row=32, column=0, sticky=W)
	Label(master, text=" DAC_SKT3_CH1_VAL=").grid(row=33, column=0, sticky=W)
	Label(master, text=" DAC_SKT4_CH1_VAL=").grid(row=34, column=0, sticky=W)
	Label(master, text=" DAC_SKT5_CH1_VAL=").grid(row=35, column=0, sticky=W)
	Label(master, text=" DAC_SKT6_CH1_VAL=").grid(row=36, column=0, sticky=W)
	Label(master, text=" DAC_SKT7_CH1_VAL=").grid(row=37, column=0, sticky=W)
	Label(master, text=" DAC_SKT8_CH1_VAL=").grid(row=38, column=0, sticky=W)
	ee_dac_skt1_ch1_val                     .grid(row=31, column=1)
	ee_dac_skt2_ch1_val                     .grid(row=32, column=1)
	ee_dac_skt3_ch1_val                     .grid(row=33, column=1)
	ee_dac_skt4_ch1_val                     .grid(row=34, column=1)
	ee_dac_skt5_ch1_val                     .grid(row=35, column=1)
	ee_dac_skt6_ch1_val                     .grid(row=36, column=1)
	ee_dac_skt7_ch1_val                     .grid(row=37, column=1)
	ee_dac_skt8_ch1_val                     .grid(row=38, column=1)
	ee_dac_skt1_ch1_val  .insert(0,"0")
	ee_dac_skt2_ch1_val  .insert(0,"0")
	ee_dac_skt3_ch1_val  .insert(0,"0")
	ee_dac_skt4_ch1_val  .insert(0,"0")
	ee_dac_skt5_ch1_val  .insert(0,"0")
	ee_dac_skt6_ch1_val  .insert(0,"0")
	ee_dac_skt7_ch1_val  .insert(0,"0")
	ee_dac_skt8_ch1_val  .insert(0,"0")
	Label(master, text=" DAC_SKT1_CH2_VAL=").grid(row=31, column=2, sticky=W)
	Label(master, text=" DAC_SKT2_CH2_VAL=").grid(row=32, column=2, sticky=W)
	Label(master, text=" DAC_SKT3_CH2_VAL=").grid(row=33, column=2, sticky=W)
	Label(master, text=" DAC_SKT4_CH2_VAL=").grid(row=34, column=2, sticky=W)
	Label(master, text=" DAC_SKT5_CH2_VAL=").grid(row=35, column=2, sticky=W)
	Label(master, text=" DAC_SKT6_CH2_VAL=").grid(row=36, column=2, sticky=W)
	Label(master, text=" DAC_SKT7_CH2_VAL=").grid(row=37, column=2, sticky=W)
	Label(master, text=" DAC_SKT8_CH2_VAL=").grid(row=38, column=2, sticky=W)
	ee_dac_skt1_ch2_val                     .grid(row=31, column=3)
	ee_dac_skt2_ch2_val                     .grid(row=32, column=3)
	ee_dac_skt3_ch2_val                     .grid(row=33, column=3)
	ee_dac_skt4_ch2_val                     .grid(row=34, column=3)
	ee_dac_skt5_ch2_val                     .grid(row=35, column=3)
	ee_dac_skt6_ch2_val                     .grid(row=36, column=3)
	ee_dac_skt7_ch2_val                     .grid(row=37, column=3)
	ee_dac_skt8_ch2_val                     .grid(row=38, column=3)
	ee_dac_skt1_ch2_val  .insert(0,"0")
	ee_dac_skt2_ch2_val  .insert(0,"0")
	ee_dac_skt3_ch2_val  .insert(0,"0")
	ee_dac_skt4_ch2_val  .insert(0,"0")
	ee_dac_skt5_ch2_val  .insert(0,"0")
	ee_dac_skt6_ch2_val  .insert(0,"0")
	ee_dac_skt7_ch2_val  .insert(0,"0")
	ee_dac_skt8_ch2_val  .insert(0,"0")
	Label(master, text=" DAC_SKT1_CH1_RDB=").grid(row=31, column=4, sticky=W)
	Label(master, text=" DAC_SKT2_CH1_RDB=").grid(row=32, column=4, sticky=W)
	Label(master, text=" DAC_SKT3_CH1_RDB=").grid(row=33, column=4, sticky=W)
	Label(master, text=" DAC_SKT4_CH1_RDB=").grid(row=34, column=4, sticky=W)
	Label(master, text=" DAC_SKT5_CH1_RDB=").grid(row=35, column=4, sticky=W)
	Label(master, text=" DAC_SKT6_CH1_RDB=").grid(row=36, column=4, sticky=W)
	Label(master, text=" DAC_SKT7_CH1_RDB=").grid(row=37, column=4, sticky=W)
	Label(master, text=" DAC_SKT8_CH1_RDB=").grid(row=38, column=4, sticky=W)
	ee_dac_skt1_ch1_rdb                     .grid(row=31, column=5)
	ee_dac_skt2_ch1_rdb                     .grid(row=32, column=5)
	ee_dac_skt3_ch1_rdb                     .grid(row=33, column=5)
	ee_dac_skt4_ch1_rdb                     .grid(row=34, column=5)
	ee_dac_skt5_ch1_rdb                     .grid(row=35, column=5)
	ee_dac_skt6_ch1_rdb                     .grid(row=36, column=5)
	ee_dac_skt7_ch1_rdb                     .grid(row=37, column=5)
	ee_dac_skt8_ch1_rdb                     .grid(row=38, column=5)
	ee_dac_skt1_ch1_rdb  .insert(0,"0")
	ee_dac_skt2_ch1_rdb  .insert(0,"0")
	ee_dac_skt3_ch1_rdb  .insert(0,"0")
	ee_dac_skt4_ch1_rdb  .insert(0,"0")
	ee_dac_skt5_ch1_rdb  .insert(0,"0")
	ee_dac_skt6_ch1_rdb  .insert(0,"0")
	ee_dac_skt7_ch1_rdb  .insert(0,"0")
	ee_dac_skt8_ch1_rdb  .insert(0,"0")
	Label(master, text=" DAC_SKT1_CH2_RDB=").grid(row=31, column=6, sticky=W)
	Label(master, text=" DAC_SKT2_CH2_RDB=").grid(row=32, column=6, sticky=W)
	Label(master, text=" DAC_SKT3_CH2_RDB=").grid(row=33, column=6, sticky=W)
	Label(master, text=" DAC_SKT4_CH2_RDB=").grid(row=34, column=6, sticky=W)
	Label(master, text=" DAC_SKT5_CH2_RDB=").grid(row=35, column=6, sticky=W)
	Label(master, text=" DAC_SKT6_CH2_RDB=").grid(row=36, column=6, sticky=W)
	Label(master, text=" DAC_SKT7_CH2_RDB=").grid(row=37, column=6, sticky=W)
	Label(master, text=" DAC_SKT8_CH2_RDB=").grid(row=38, column=6, sticky=W)
	ee_dac_skt1_ch2_rdb                     .grid(row=31, column=7)
	ee_dac_skt2_ch2_rdb                     .grid(row=32, column=7)
	ee_dac_skt3_ch2_rdb                     .grid(row=33, column=7)
	ee_dac_skt4_ch2_rdb                     .grid(row=34, column=7)
	ee_dac_skt5_ch2_rdb                     .grid(row=35, column=7)
	ee_dac_skt6_ch2_rdb                     .grid(row=36, column=7)
	ee_dac_skt7_ch2_rdb                     .grid(row=37, column=7)
	ee_dac_skt8_ch2_rdb                     .grid(row=38, column=7)
	ee_dac_skt1_ch2_rdb  .insert(0,"0")
	ee_dac_skt2_ch2_rdb  .insert(0,"0")
	ee_dac_skt3_ch2_rdb  .insert(0,"0")
	ee_dac_skt4_ch2_rdb  .insert(0,"0")
	ee_dac_skt5_ch2_rdb  .insert(0,"0")
	ee_dac_skt6_ch2_rdb  .insert(0,"0")
	ee_dac_skt7_ch2_rdb  .insert(0,"0")
	ee_dac_skt8_ch2_rdb  .insert(0,"0")
	# update DAC values
	bb_dac_update = Button(master, text='update DAC values', command=dac_bb_update) .grid(row=40, column=0, columnspan = 4, sticky=W+E, pady=4)
	
	
	## buttons common 
	# quit
	bb_quit = Button(master, text='Quit', command=master.quit) .grid(row=100,  sticky=W+E, pady=4, columnspan = 8)
	
	#
	return
#
tk_win_setup(master)

## main loop for gui
mainloop() ####



################################################################################

## wait for an input  ######################################################
#input('Enter any key to stop')


################################################################################

## wait for a while
sleep(0.5)



## test update SPIO 
spio_test_led_off()

## clear dac
dac_test()

## OK close
dev.Close()


##



