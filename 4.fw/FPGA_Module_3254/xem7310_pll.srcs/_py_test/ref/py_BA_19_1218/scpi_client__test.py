## scpi_client__test.py
import socket
#import time
from time import sleep
import sys

# connect(), close() ... style

# https://docs.python.org/3/library/socket.html
# https://docs.python.org/2/library/socket.html

## control parameters 

HOST = '192.168.168.123'  # The server's hostname or IP address
HOST2 = '192.168.172.1'  # The server's hostname or IP address
PORT = 5025               # The port used by the server
TIMEOUT = 5.3 # socket timeout
SO_SNDBUF = 2048
SO_RCVBUF = 32768
INTVAL = 0.1 # sec for waiting before recv()
BUF_SIZE_NORMAL = 2048
BUF_SIZE_LARGE = 16384
TIMEOUT_LARGE = TIMEOUT*10

ss = None # socket

## command strings
cmd_str__IDN = b'*IDN?\n'
cmd_str__RST = b'*RST\n'
#
cmd_str__PGEP_EN = b':PGEP:EN'
#
cmd_str__PGEP_MKWI = b':PGEP:MKWI'
cmd_str__PGEP_MKWO = b':PGEP:MKWO'
cmd_str__PGEP_MKTI = b':PGEP:MKTI'
cmd_str__PGEP_MKTO = b':PGEP:MKTO'
#
cmd_str__PGEP_WI   = b':PGEP:WI'
cmd_str__PGEP_WO   = b':PGEP:WO'
cmd_str__PGEP_TI   = b':PGEP:TI'
cmd_str__PGEP_TO   = b':PGEP:TO'
cmd_str__PGEP_TAC  = b':PGEP:TAC'
cmd_str__PGEP_TMO  = b':PGEP:TMO'
cmd_str__PGEP_PI   = b':PGEP:PI'
cmd_str__PGEP_PO   = b':PGEP:PO'
#
cmd_str__PGEP_WMI  = b':PGEP:WMI'
cmd_str__PGEP_WMO  = b':PGEP:WMO'
#
cmd_str__PGU_PWR            = b':PGU:PWR'
cmd_str__PGU_DCS_TRIG       = b':PGU:DCS:TRIG'
cmd_str__PGU_DCS_DAC0_PNT   = b':PGU:DCS:DAC0:PNT'
cmd_str__PGU_DCS_DAC1_PNT   = b':PGU:DCS:DAC1:PNT'
cmd_str__PGU_DCS_RPT        = b':PGU:DCS:RPT'
cmd_str__PGU_FDCS_TRIG      = b':PGU:FDCS:TRIG'
cmd_str__PGU_FDCS_DAC0      = b':PGU:FDCS:DAC0'
cmd_str__PGU_FDCS_DAC1      = b':PGU:FDCS:DAC1'
cmd_str__PGU_FDCS_RPT       = b':PGU:FDCS:RPT'
cmd_str__PGU_FREQ           = b':PGU:FREQ'
cmd_str__PGU_OFST_DAC0      = b':PGU:OFST:DAC0'
cmd_str__PGU_OFST_DAC1      = b':PGU:OFST:DAC1'
cmd_str__PGU_GAIN_DAC0      = b':PGU:GAIN:DAC0'
cmd_str__PGU_GAIN_DAC1      = b':PGU:GAIN:DAC1'
#


## functions


def scpi_open (timeout=TIMEOUT):
	try:
		ss = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		ss.settimeout(timeout)
		ss.setsockopt(socket.SOL_SOCKET, socket.SO_SNDBUF, SO_SNDBUF)
		ss.setsockopt(socket.SOL_SOCKET, socket.SO_RCVBUF, SO_RCVBUF) # 8192 16384 32768 65536
	except OSError as msg:
		ss = None
		print('error in socket: ', msg)
		raise
	return ss

def scpi_connect (ss, HOST, PORT):
	try:
		ss.connect((HOST, PORT))
	except OSError as msg:
		ss.close()
		ss = None
		print('error in connect: ', msg)
		raise


def scpi_close (ss):
	try:
		ss.close()
	except:
		if ss == None:
			print('error: ss==None')
		raise

def scpi_comm_resp_ss (ss, cmd_str, buf_size=BUF_SIZE_NORMAL, intval=INTVAL) :
	try:
		print('Send:', repr(cmd_str))
		ss.sendall(cmd_str)
	except:
		print('error in sendall')
		raise
	##
	sleep(intval)
	#
	# recv data until finding the sentinel '\n'
	try:
		data = ss.recv(buf_size) # try 1024 131072 524288
		# try   
		while (1):
			if (chr(data[-1])=='\n'): # check the sentinel '\n' 
				break
			data = data + ss.recv(buf_size)
	except:
		print('error in recv')
		raise
	#
	## check response 
	if (len(data)>20):
		print('Received:', repr(data[0:20]),  ' (first 20 bytes)')
	else:
		print('Received:', repr(data))
	#
	# NG response check 
	if data[0:2]==b'NG':
		print('Received: NG as response')
		#input('Press Enter key!')
		#sleep(3)
	#
	return data


# scpi command for numeric block response
def scpi_comm_resp_numb_ss (ss, cmd_str, buf_size=BUF_SIZE_LARGE, intval=INTVAL, timeout_large=TIMEOUT_LARGE) :
	try:
		print('Send:', repr(cmd_str))
		ss.sendall(cmd_str)
	except:
		print('error in sendall')
		raise
	##
	sleep(intval)
	#
	# cmd: ":PGEP:PO#HBC 524288\n"
	# rsp: "#4_001024_rrrrrrrrrr...rrrrrrrrrr\n"
	#
	# recv data until finding the sentinel '\n' // TODO: numeric block
	# but check the sentinel after the data byte count is met.
	#
	# read timeout
	to = ss.gettimeout()
	#print(to)
	# increase timeout
	ss.settimeout(timeout_large)
	#
	try:
		# find the numeric head : must 10 in data 
		data = ss.recv(buf_size)
		while True:
			if len(data)>=10:
				break
			data = data + ss.recv(buf_size)
		#
		#print('header: ', repr(data[0:10])) # header
		#
		# find byte count 
		byte_count = int(data[3:9])
		#print('byte_count=', repr(byte_count)) 
		#
		# collect all data by byte count
		count_to_recv = byte_count + 10 + 1# add header count #add /n
		while True:
			if len(data)>=count_to_recv:
				break
			data = data + ss.recv(buf_size)
		#
		# check the sentinel 
		while True:
			if (chr(data[-1])=='\n'): # check the sentinel '\n' 
				break
			data = data + ss.recv(buf_size)
		#
	except:
		print('error in recv')
		raise
	#
	if (len(data)>20):
		print('Received:', repr(data[0:20]),  ' (first 20 bytes)')
	else:
		print('Received:', repr(data))
	#
	# timeout back to prev
	ss.settimeout(to)
	#
	data = data[10:(10+byte_count)]
	print('data:', data[0:20].hex(),  ' (first 20 bytes)')
	#
	return [byte_count, data]
	
## test routines

## open socket and connect scpi server
try:
	ss = scpi_open()
	scpi_connect(ss, HOST, PORT)
except socket.timeout:
	ss = scpi_open()
	scpi_connect(ss, HOST2, PORT)
except:
	raise

#01 scpi : *IDN?
print('\n>>> {} : {}'.format('Test',cmd_str__IDN))
rsp = scpi_comm_resp_ss(ss, cmd_str__IDN)
print('hex code rcvd: ' + rsp.hex())
print('string rcvd: ' + repr(rsp))

###   # scpi : NG...
###   cmd_str__TEST = b'WH?\n'
###   print('\n>>> {} : {}'.format('Test',cmd_str__TEST))
###   scpi_comm_resp_ss(ss, cmd_str__TEST)
###   
###   #02 scpi : *RST? // reset board vs network IC
###   print('\n>>> {} : {}'.format('Test',cmd_str__RST))
###   #scpi_comm(cmd_str__RST)
###   scpi_comm_resp_ss(ss, cmd_str__RST)
###   
###   # scpi : NG...
###   cmd_str__TEST = b'test ??? test'
###   print('\n>>> {} : {}'.format('Test',cmd_str__TEST))
###   scpi_comm_resp_ss(ss, cmd_str__TEST)

#03 :PGEP:EN
# :PGEP:EN ON|OFF <NL>			
# :PGEP:EN? <NL>			
#
# ":PGEP:EN ON\n"
# ":PGEP:EN OFF\n"
# ":PGEP:EN?"
print('\n>>> {} : {}'.format('Test',cmd_str__PGEP_EN))
scpi_comm_resp_ss(ss, cmd_str__PGEP_EN+b' OFF\n')
scpi_comm_resp_ss(ss, cmd_str__PGEP_EN+b'?\n')
scpi_comm_resp_ss(ss, cmd_str__PGEP_EN+b' ON\n')
scpi_comm_resp_ss(ss, cmd_str__PGEP_EN+b'?\n')


### scpi command: ":PGU:PWR"
# :PGU:PWR ON|OFF <NL>			
# :PGU:PWR? <NL>			
#
# ":PGU:PWR ON\n"
# ":PGU:PWR OFF\n"
# ":PGU:PWR?"
#

### power on 
print('\n>>> {} : {}'.format('Test',cmd_str__PGU_PWR))
scpi_comm_resp_ss(ss, cmd_str__PGU_PWR+b' ON\n')
scpi_comm_resp_ss(ss, cmd_str__PGU_PWR+b'?\n')



### scpi command: ":PGU:FREQ" 
# set DAC update freq based on 100kHz
#
# :PGU:FREQ nnnn <NL>			
#
# ":PGU:FREQ 4000 \n"  // for 400.0 MHz
# ":PGU:FREQ 2000 \n"  // for 200.0 MHz
# ":PGU:FREQ 1000 \n"  // for 100.0 MHz
# ":PGU:FREQ 0800 \n"  // for 080.0 MHz
# ":PGU:FREQ 0500 \n"  // for 050.0 MHz
# ":PGU:FREQ 0200 \n"  // for 020.0 MHz
# ":PGU:FREQ 0100 \n"  // for 010.0 MHz NG FPGA PLL out spec??
# ":PGU:FREQ 0050 \n"  // for 005.0 MHz NG FPGA PLL out spec??
# ":PGU:FREQ 0025 \n"  // for 002.5 MHz NG FPGA PLL out spec??
#
print('\n>>> {} : {}'.format('Test',cmd_str__PGU_FREQ))
#scpi_comm_resp_ss(ss, cmd_str__PGU_FREQ+b' 4000 \n') #OK
#scpi_comm_resp_ss(ss, cmd_str__PGU_FREQ+b' 2000 \n') #OK
#scpi_comm_resp_ss(ss, cmd_str__PGU_FREQ+b' 1000 \n') #OK
#scpi_comm_resp_ss(ss, cmd_str__PGU_FREQ+b' 0800 \n') #OK
#scpi_comm_resp_ss(ss, cmd_str__PGU_FREQ+b' 0500 \n') #OK
#scpi_comm_resp_ss(ss, cmd_str__PGU_FREQ+b' 0200 \n') #OK
#scpi_comm_resp_ss(ss, cmd_str__PGU_FREQ+b' 0100 \n') # NG 
#
scpi_comm_resp_ss(ss, cmd_str__PGU_FREQ+b' 2000 \n') #OK


### scpi command: ":PGU:OFST:DAC0" 
# cmd_str__PGU_OFST_DAC0      = b':PGU:OFST:DAC0'
#
# :PGU:OFST:DAC0? <NL>			
# :PGU:OFST:DAC0 #Hnnnnnnnn <NL>			
#
# // data = {DAC_ch1_aux, DAC_ch2_aux}
# // DAC_ch#_aux = {PN_Pol_sel, Source_Sink_sel, 0000, 10 bit data}
# //                PN_Pol_sel      = 0/1 for P/N
# //                Source_Sink_sel = 0/1 for Source/Sink
#
# offset DAC : 0x140 0.625mA, AUX2N active[7] (1) , sink current[6] (1) <<< offset 1.81mV
print('\n>>> {} : {}'.format('Test',cmd_str__PGU_OFST_DAC0))
scpi_comm_resp_ss(ss, cmd_str__PGU_OFST_DAC0+b'?\n')
scpi_comm_resp_ss(ss, cmd_str__PGU_OFST_DAC0+b' #HC141C140 \n')
scpi_comm_resp_ss(ss, cmd_str__PGU_OFST_DAC0+b'?\n')

### scpi command: ":PGU:OFST:DAC1" 
# cmd_str__PGU_OFST_DAC1      = b':PGU:OFST:DAC1'
print('\n>>> {} : {}'.format('Test',cmd_str__PGU_OFST_DAC1))
scpi_comm_resp_ss(ss, cmd_str__PGU_OFST_DAC1+b'?\n')
scpi_comm_resp_ss(ss, cmd_str__PGU_OFST_DAC1+b' #HC141C140 \n')
scpi_comm_resp_ss(ss, cmd_str__PGU_OFST_DAC1+b'?\n')

### scpi command: ":PGU:GAIN:DAC0" 
# cmd_str__PGU_GAIN_DAC0      = b':PGU:GAIN:DAC0'
#
# ":PGU:GAIN:DAC0? \n" 
# ":PGU:GAIN:DAC0 #H02D002D0 \n" 
#
# // data = {DAC_ch1_fsc, DAC_ch2_fsc}
# // DAC_ch#_fsc = {000000, 10 bit data}
#
# full scale DAC : 28.1mA  @ 0x02D0scpi_comm_resp_ss(ss, cmd_str__PGU_GAIN_DAC0+b'?\n')
print('\n>>> {} : {}'.format('Test',cmd_str__PGU_GAIN_DAC0))
scpi_comm_resp_ss(ss, cmd_str__PGU_GAIN_DAC0+b'?\n')
scpi_comm_resp_ss(ss, cmd_str__PGU_GAIN_DAC0+b' #H02D102D0 \n')
scpi_comm_resp_ss(ss, cmd_str__PGU_GAIN_DAC0+b'?\n')

### scpi command: ":PGU:GAIN:DAC1" 
# cmd_str__PGU_GAIN_DAC1      = b':PGU:GAIN:DAC1'
print('\n>>> {} : {}'.format('Test',cmd_str__PGU_GAIN_DAC1))
scpi_comm_resp_ss(ss, cmd_str__PGU_GAIN_DAC1+b'?\n')
scpi_comm_resp_ss(ss, cmd_str__PGU_GAIN_DAC1+b' #H02D102D0 \n')
scpi_comm_resp_ss(ss, cmd_str__PGU_GAIN_DAC1+b'?\n')


### scpi command: ":PGU:DCS:DAC0:PNT#H" 
# :PGU:DCS:DAC0:PNT#Hnnnn? <NL>			
# :PGU:DCS:DAC0:PNT#Hnnnn #Hnnnnnnnn <NL>			
#
# // DACn repeat data = {16-bit DAC1 repeat count, 16-bit DAC0 repeat count}
#
# ":PGU:DCS:DAC0:PNT#H0001? \n"
# ":PGU:DCS:DAC0:PNT#H0001 #H00040001 \n"
#
print('\n>>> {} : {}'.format('Test',cmd_str__PGU_DCS_DAC0_PNT))
scpi_comm_resp_ss(ss, cmd_str__PGU_DCS_DAC0_PNT+b'#H0000?\n')
scpi_comm_resp_ss(ss, cmd_str__PGU_DCS_DAC0_PNT+b'#H0001?\n')
scpi_comm_resp_ss(ss, cmd_str__PGU_DCS_DAC0_PNT+b'#H0000 #H3FFF0007\n')
scpi_comm_resp_ss(ss, cmd_str__PGU_DCS_DAC0_PNT+b'#H0001 #H7FFF0011\n')
scpi_comm_resp_ss(ss, cmd_str__PGU_DCS_DAC0_PNT+b'#H0000?\n')
scpi_comm_resp_ss(ss, cmd_str__PGU_DCS_DAC0_PNT+b'#H0001?\n')


### scpi command: ":PGU:DCS:DAC1:PNT#H" 
# :PGU:DCS:DAC1:PNT#Hnnnn? <NL>			
# :PGU:DCS:DAC1:PNT#Hnnnn #Hnnnnnnnn <NL>			
#
# ":PGU:DCS:DAC1:PNT#H0001? \n"
# ":PGU:DCS:DAC1:PNT#H0001 #H00040001 \n"
#
print('\n>>> {} : {}'.format('Test',cmd_str__PGU_DCS_DAC1_PNT))
scpi_comm_resp_ss(ss, cmd_str__PGU_DCS_DAC1_PNT+b'#H0000?\n')
scpi_comm_resp_ss(ss, cmd_str__PGU_DCS_DAC1_PNT+b'#H0001?\n')
scpi_comm_resp_ss(ss, cmd_str__PGU_DCS_DAC1_PNT+b'#H0000 #H3FFF0003\n')
scpi_comm_resp_ss(ss, cmd_str__PGU_DCS_DAC1_PNT+b'#H0001 #H7FFF0005\n')
scpi_comm_resp_ss(ss, cmd_str__PGU_DCS_DAC1_PNT+b'#H0000?\n')
scpi_comm_resp_ss(ss, cmd_str__PGU_DCS_DAC1_PNT+b'#H0001?\n')


### scpi command: ":PGU:DCS:RPT"
# :PGU:DCS:RPT? <NL>			
# :PGU:DCS:RPT #Hnnnnnnnn <NL>			
#
# // DACn repeat count = {16-bit DAC1 repeat count, 16-bit DAC0 repeat count}
#
# ":PGU:DCS:RPT? \n"
# ":PGU:DCS:RPT #H00020003 \n"
#
print('\n>>> {} : {}'.format('Test',cmd_str__PGU_DCS_RPT))
scpi_comm_resp_ss(ss, cmd_str__PGU_DCS_RPT+b'?\n')
#scpi_comm_resp_ss(ss, cmd_str__PGU_DCS_RPT+b' #H00020003\n')
#scpi_comm_resp_ss(ss, cmd_str__PGU_DCS_RPT+b' #H04010001\n')
#scpi_comm_resp_ss(ss, cmd_str__PGU_DCS_RPT+b' #H03E80001\n')
scpi_comm_resp_ss(ss, cmd_str__PGU_DCS_RPT+b' #H00640001\n')
#scpi_comm_resp_ss(ss, cmd_str__PGU_DCS_RPT+b' #H000A0001\n')
scpi_comm_resp_ss(ss, cmd_str__PGU_DCS_RPT+b'?\n')


### TODO: cmd_str__PGU_FDCS_DAC0      = b':PGU:FDCS:DAC0'
# :PGU:FDCS:DAC0 #N_nnnnnn_hhhhhhhh_hhhhhhhh_..._hhhhhhhh <NL>
# :PGU:FDCS:DAC0 #4_nnnnnn_rrrrrrrr...rrrr <NL> (reserved)
#
# :PGU:FDCS:DAC0 #N_000064_3FFF0008_7FFF0010_3FFF0008_00000004_C0000008_80000010_C0000008_00000004 <NL>
# :PGU:FDCS:DAC0 #4_000032_rrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr <NL> (reserved)
#
print('\n>>> {} : {}'.format('Test',cmd_str__PGU_FDCS_DAC0))
scpi_comm_resp_ss(ss, cmd_str__PGU_FDCS_DAC0+b' #N_000064_3FFF0008_7FFF0010_3FFF0008_00000004_C0000008_80000010_C0000008_00000004 \n')


### TODO: cmd_str__PGU_FDCS_DAC1      = b':PGU:FDCS:DAC1'
# :PGU:FDCS:DAC1 #N_nnnnnn_hhhhhhhh_hhhhhhhh_..._hhhhhhhh <NL>
# :PGU:FDCS:DAC1 #4_nnnnnn_rrrrrrrr...rrrr <NL> (reserved)
#
# :PGU:FDCS:DAC1 #N_000064_3FFF0002_7FFF0004_3FFF0002_00000001_C0000002_80000004_C0000002_00000001 <NL>
# :PGU:FDCS:DAC1 #4_000032_rrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr <NL> (reserved)
#
print('\n>>> {} : {}'.format('Test',cmd_str__PGU_FDCS_DAC1))
scpi_comm_resp_ss(ss, cmd_str__PGU_FDCS_DAC1+b' #N_000064_3FFF0002_7FFF0004_3FFF0002_00000001_C0000002_80000004_C0000002_00000001 \n')


### TODO: cmd_str__PGU_FDCS_RPT       = b':PGU:FDCS:RPT'
### scpi command: ":PGU:FDCS:RPT"
# :PGU:FDCS:RPT? <NL>			
# :PGU:FDCS:RPT #Hnnnnnnnn <NL>			
#
# // DACn repeat count = {16-bit DAC1 repeat count, 16-bit DAC0 repeat count}
#
# ":PGU:FDCS:RPT? \n"
# ":PGU:FDCS:RPT #H00010003 \n"
print('\n>>> {} : {}'.format('Test',cmd_str__PGU_FDCS_RPT))
scpi_comm_resp_ss(ss, cmd_str__PGU_FDCS_RPT+b'?\n')
scpi_comm_resp_ss(ss, cmd_str__PGU_FDCS_RPT+b' #H00010003\n')
scpi_comm_resp_ss(ss, cmd_str__PGU_FDCS_RPT+b'?\n')


## wait for an input  ######################################################
input('Enter any key to trigger')


### scpi command: ":PGU:DCS:TRIG"
# :PGU:DCS:TRIG ON|OFF <NL>			
#
# ":PGU:DCS:TRIG ON\n"
# ":PGU:DCS:TRIG OFF\n"
#

### trig on : DCS mode
print('\n>>> {} : {}'.format('Test',cmd_str__PGU_DCS_TRIG))
scpi_comm_resp_ss(ss, cmd_str__PGU_DCS_TRIG+b' ON\n')

## wait for a while
sleep(1)

### trig off : DCS mode
print('\n>>> {} : {}'.format('Test',cmd_str__PGU_DCS_TRIG))
scpi_comm_resp_ss(ss, cmd_str__PGU_DCS_TRIG+b' OFF\n')


### scpi command: ":PGU:FDCS:TRIG"
# :PGU:FDCS:TRIG ON|OFF <NL>			
#
# ":PGU:FDCS:TRIG ON\n"
# ":PGU:FDCS:TRIG OFF\n"
#

### trig on : FDCS mode
print('\n>>> {} : {}'.format('Test',cmd_str__PGU_FDCS_TRIG))
scpi_comm_resp_ss(ss, cmd_str__PGU_FDCS_TRIG+b' ON\n')

## wait for a while
sleep(1)

### trig off : FDCS mode
print('\n>>> {} : {}'.format('Test',cmd_str__PGU_FDCS_TRIG))
scpi_comm_resp_ss(ss, cmd_str__PGU_FDCS_TRIG+b' OFF\n')


### power off 
print('\n>>> {} : {}'.format('Test',cmd_str__PGU_PWR))
scpi_comm_resp_ss(ss, cmd_str__PGU_PWR+b' OFF\n')
scpi_comm_resp_ss(ss, cmd_str__PGU_PWR+b'?\n')

##############################################################################
###   
###   
###   #04 :PGEP:MKWI
###   # :PGEP:MKWI  #Hnnnnnnnn <NL>
###   # :PGEP:MKWI? <NL>
###   #
###   # ":PGEP:MKWI #HFFCCAAFF\n"
###   # ":PGEP:MKWI?\n"
###   print('>>> {} : {}'.format('Test',cmd_str__PGEP_MKWI))
###   scpi_comm_resp_ss(ss, cmd_str__PGEP_MKWI+b' #HFFCCAA33\n')
###   scpi_comm_resp_ss(ss, cmd_str__PGEP_MKWI+b'?\n')
###   
###   #05 :PGEP:MKWO
###   # :PGEP:MKWO  #Hnnnnnnnn <NL>
###   # :PGEP:MKWO? <NL>
###   #
###   # ":PGEP:MKWO #HFF3355FF\n"
###   # ":PGEP:MKWO?\n"
###   print('>>> {} : {}'.format('Test',cmd_str__PGEP_MKWO))
###   scpi_comm_resp_ss(ss, cmd_str__PGEP_MKWO+b' #HFF3355FF\n')
###   scpi_comm_resp_ss(ss, cmd_str__PGEP_MKWO+b'?\n')
###   
###   #06 :PGEP:MKTI 
###   # :PGEP:MKTI  #Hnnnnnnnn <NL>
###   # :PGEP:MKTI? <NL>
###   #
###   # ":PGEP:MKTI #HFA3355FF\n"
###   # ":PGEP:MKTI?\n"
###   print('>>> {} : {}'.format('Test',cmd_str__PGEP_MKTI))
###   scpi_comm_resp_ss(ss, cmd_str__PGEP_MKTI+b' #HFA3355FF\n')
###   scpi_comm_resp_ss(ss, cmd_str__PGEP_MKTI+b'?\n')
###   
###   #07 :PGEP:MKTO 
###   # :PGEP:MKTO  #Hnnnnnnnn <NL>
###   # :PGEP:MKTO? <NL>
###   #
###   # ":PGEP:MKTO #HFA3355AF\n"
###   # ":PGEP:MKTO?\n"
###   print('>>> {} : {}'.format('Test',cmd_str__PGEP_MKTO))
###   scpi_comm_resp_ss(ss, cmd_str__PGEP_MKTO+b' #HFA3355AF\n')
###   scpi_comm_resp_ss(ss, cmd_str__PGEP_MKTO+b'?\n')
###   
###   
###   #08 :PGEP:WI
###   # :PGEP:WI#Hnn  #Hnnnnnnnn <NL>
###   # :PGEP:WI#Hnn? <NL>
###   #
###   # ":PGEP:WI#H00 #H00001234\n"
###   # ":PGEP:WI#H00?\n"
###   print('>>> {} : {}'.format('Test',cmd_str__PGEP_WI))
###   scpi_comm_resp_ss(ss, cmd_str__PGEP_WI+b' #H00 #H00001234\n')
###   scpi_comm_resp_ss(ss, cmd_str__PGEP_WI+b'#H00?\n')
###   scpi_comm_resp_ss(ss, cmd_str__PGEP_MKWI+b' #HFFFFFFFF\n')
###   scpi_comm_resp_ss(ss, cmd_str__PGEP_MKWI+b'?\n')
###   scpi_comm_resp_ss(ss, cmd_str__PGEP_WI+b'#H00 #HABCD1234\n')
###   scpi_comm_resp_ss(ss, cmd_str__PGEP_WI+b'#H00?\n')
###   scpi_comm_resp_ss(ss, cmd_str__PGEP_MKWI+b' #HFFFF0000\n')
###   scpi_comm_resp_ss(ss, cmd_str__PGEP_MKWI+b'?\n')
###   scpi_comm_resp_ss(ss, cmd_str__PGEP_WI+b'#H00 #H00000000\n')
###   scpi_comm_resp_ss(ss, cmd_str__PGEP_WI+b'#H00?\n')
###   scpi_comm_resp_ss(ss, cmd_str__PGEP_MKWI+b' #H0000FFFF\n')
###   scpi_comm_resp_ss(ss, cmd_str__PGEP_MKWI+b'?\n')
###   scpi_comm_resp_ss(ss, cmd_str__PGEP_WI+b'#H00 #H00000000\n')
###   scpi_comm_resp_ss(ss, cmd_str__PGEP_WI+b'#H00?\n')
###   
###   #09 :PGEP:WO
###   # :PGEP:WO#Hnn? <NL>
###   #
###   # ":PGEP:WO#H20?\n"
###   print('>>> {} : {}'.format('Test',cmd_str__PGEP_WO))
###   scpi_comm_resp_ss(ss, cmd_str__PGEP_MKWO+b' #HFFFFFFFF\n')
###   scpi_comm_resp_ss(ss, cmd_str__PGEP_MKWO+b'?\n')
###   scpi_comm_resp_ss(ss, cmd_str__PGEP_WO+b'#H20?\n')
###   scpi_comm_resp_ss(ss, cmd_str__PGEP_WO+b'#H23?\n')
###   scpi_comm_resp_ss(ss, cmd_str__PGEP_WO+b'#H39?\n')
###   scpi_comm_resp_ss(ss, cmd_str__PGEP_WO+b'#H21?\n')
###   
###   #10 :PGEP:TI
###   # :PGEP:TI#Hnn  #Hnnnnnnnn <NL>
###   # :PGEP:TI#Hnn? <NL>
###   #
###   # ":PGEP:TI#H40 #H00000002\n"
###   # ":PGEP:TI#H40?\n"
###   print('>>> {} : {}'.format('Test',cmd_str__PGEP_TI))
###   scpi_comm_resp_ss(ss, cmd_str__PGEP_TI+b'#H40 #H00000002\n') # count2 up
###   scpi_comm_resp_ss(ss, cmd_str__PGEP_TI+b'#H40?\n')
###   scpi_comm_resp_ss(ss, cmd_str__PGEP_WO+b'#H21?\n') # {count2,count1}
###   scpi_comm_resp_ss(ss, cmd_str__PGEP_TI+b'#H40 #H00000002\n') # count2 up
###   scpi_comm_resp_ss(ss, cmd_str__PGEP_TI+b'#H40?\n')
###   scpi_comm_resp_ss(ss, cmd_str__PGEP_TI+b'#H40 #H00000002\n') # count2 up
###   scpi_comm_resp_ss(ss, cmd_str__PGEP_TI+b'#H40?\n')
###   scpi_comm_resp_ss(ss, cmd_str__PGEP_WO+b'#H21?\n') # {count2,count1}
###   
###   #11 :PGEP:TO
###   # :PGEP:TO#Hnn? <NL>
###   #
###   # ":PGEP:TO#H60?\n"
###   print('>>> {} : {}'.format('Test',cmd_str__PGEP_TO))
###   scpi_comm_resp_ss(ss, cmd_str__PGEP_MKTO+b' #HFFFFFFFF\n')
###   scpi_comm_resp_ss(ss, cmd_str__PGEP_MKTO+b'?\n')
###   scpi_comm_resp_ss(ss, cmd_str__PGEP_TO+b'#H60?\n')
###   scpi_comm_resp_ss(ss, cmd_str__PGEP_TI+b'#H40 #H00000001\n') # count2 reset
###   scpi_comm_resp_ss(ss, cmd_str__PGEP_TI+b'#H40?\n')
###   scpi_comm_resp_ss(ss, cmd_str__PGEP_TI+b'#H40 #H00000004\n') # count2 down
###   scpi_comm_resp_ss(ss, cmd_str__PGEP_TI+b'#H40?\n')
###   scpi_comm_resp_ss(ss, cmd_str__PGEP_WO+b'#H21?\n') # {count2,count1}
###   scpi_comm_resp_ss(ss, cmd_str__PGEP_TO+b'#H60?\n')
###   scpi_comm_resp_ss(ss, cmd_str__PGEP_TO+b'#H60?\n')
###   scpi_comm_resp_ss(ss, cmd_str__PGEP_TI+b'#H40 #H00000001\n') # count2 reset
###   scpi_comm_resp_ss(ss, cmd_str__PGEP_TO+b'#H60?\n')
###   scpi_comm_resp_ss(ss, cmd_str__PGEP_TO+b'#H60?\n')
###   
###   #12 :PGEP:TAC
###   # // void activate_mcs_ep_ti(u32 offset, u32 bit_loc);
###   # :PGEP:TAC#Hnn  #Hnn<NL>
###   #
###   # ":PGEP:TAC#H40 #H01\n"  vs ":PGEP:TAC#H40 01\n"
###   print('>>> {} : {}'.format('Test',cmd_str__PGEP_TAC))
###   scpi_comm_resp_ss(ss, cmd_str__PGEP_TAC+b'#H40 #H02\n')
###   scpi_comm_resp_ss(ss, cmd_str__PGEP_WO+b'#H21?\n') # {count2,count1}
###   scpi_comm_resp_ss(ss, cmd_str__PGEP_TO+b'#H60?\n')
###   scpi_comm_resp_ss(ss, cmd_str__PGEP_TAC+b'#H40 #H00\n')
###   scpi_comm_resp_ss(ss, cmd_str__PGEP_WO+b'#H21?\n') # {count2,count1}
###   scpi_comm_resp_ss(ss, cmd_str__PGEP_TO+b'#H60?\n')
###   scpi_comm_resp_ss(ss, cmd_str__PGEP_TO+b'#H60?\n')
###   
###   #13 :PGEP:TMO
###   # // u32 is_triggered_mcs_ep_to(u32 offset, u32 mask);
###   # :PGEP:TMO#Hnn  #Hnnnnnnnn<NL>
###   #
###   # cmd: ":PGEP:TMO#H60 #HFFFF0000\n" vs ":PGEP:TMO#H60? #HFFFF0000\n"
###   # rsp: "ON\n" or "OFF\n"
###   print('>>> {} : {}'.format('Test',cmd_str__PGEP_TMO))
###   scpi_comm_resp_ss(ss, cmd_str__PGEP_TMO+b'#H60 #HFFFF0000\n')
###   scpi_comm_resp_ss(ss, cmd_str__PGEP_TAC+b'#H40 #H02\n')
###   scpi_comm_resp_ss(ss, cmd_str__PGEP_WO+b'#H21?\n') # {count2,count1}
###   scpi_comm_resp_ss(ss, cmd_str__PGEP_TAC+b'#H40 #H00\n')
###   scpi_comm_resp_ss(ss, cmd_str__PGEP_TMO+b'#H60 #HFFFF0000\n')
###   scpi_comm_resp_ss(ss, cmd_str__PGEP_TMO+b'#H60 #HFFFF0000\n')
###   
###   #14 :PGEP:PI
###   # :PGEP:PI#Hnn  #m_nnnnnn_rrrrrrrrrrrrrrrrrrrr <NL>
###   # m : byte number in unit = 4
###   # nnnnnn :  data byte length  ~< 001024 
###   # rrr..rrr : byte data 
###   #
###   # ":PGEP:PI#Hnn #4_001024_rrrrrrrrrr...rrrrrrrrrr\n"
###   # cmd: ":PGEP:PI#H8A #4_001024_rrrrrrrrrr...rrrrrrrrrr\n"  // fifo @ 0x8A ... max 4096 depth = 2^12
###   print('>>> {} : {}'.format('Test',cmd_str__PGEP_PI))
###   #
###   #(1024).to_bytes(4, byteorder='little') #Return an array of bytes representing an integer.
###   # >>> (1024).to_bytes(4, byteorder='little')
###   # b'\x00\x04\x00\x00'
###   #
###   bdata = b''
###   #for ii in [0,1,2,3,4,5,6,7,8,9]:
###   for ii in [	0xDCBA0DF0,
###   			0x12341DF1,
###   			0x87652DF2,
###   			0xDCBA3DF3,
###   			0x12344DF4,
###   			0x87655DF5,
###   			0xDCBA6DF6,
###   			0x12347DF7,
###   			0x87658DF8,
###   			0x12349DF9]:
###   	bdata = bdata + ii.to_bytes(4, byteorder='little')
###   print('bdata to send: ' + bdata.hex()) # we can see data in little-endian.
###   scpi_comm_resp_ss(ss, cmd_str__PGEP_PI+b'#H8A #4_000040_'+ bdata + b'\n')
###   #              8c                 20c
###   # issue buffer check... little .. big order in.. buffer / fifo... must re-order 
###   # MCS IO mem on buf8 - buf32 location constraint is must.... 4byte-->u32*
###   #
###   #scpi_comm_resp_ss(ss, cmd_str__PGEP_WO+b'#HAA?\n')
###   #scpi_comm_resp_ss(ss, cmd_str__PGEP_WO+b'#HAA?\n')
###   #scpi_comm_resp_ss(ss, cmd_str__PGEP_WO+b'#HAA?\n')
###   #scpi_comm_resp_ss(ss, cmd_str__PGEP_WO+b'#HAA?\n')
###   #scpi_comm_resp_ss(ss, cmd_str__PGEP_WO+b'#HAA?\n')
###   #scpi_comm_resp_ss(ss, cmd_str__PGEP_WO+b'#HAA?\n')
###   #scpi_comm_resp_ss(ss, cmd_str__PGEP_WO+b'#HAA?\n')
###   #scpi_comm_resp_ss(ss, cmd_str__PGEP_WO+b'#HAA?\n')
###   #scpi_comm_resp_ss(ss, cmd_str__PGEP_WO+b'#HAA?\n')
###   #scpi_comm_resp_ss(ss, cmd_str__PGEP_WO+b'#HAA?\n')
###   #scpi_comm_resp_ss(ss, cmd_str__PGEP_WO+b'#HAA?\n')
###   #scpi_comm_resp_ss(ss, cmd_str__PGEP_WO+b'#HAA?\n')
###   #scpi_comm_resp_ss(ss, cmd_str__PGEP_WO+b'#HAA?\n')
###   #scpi_comm_resp_ss(ss, cmd_str__PGEP_WO+b'#HAA?\n')
###   
###   
###   #15 :PGEP:PO
###   # :PGEP:PO#Hnn nnnnnn<NL>
###   #
###   # cmd: ":PGEP:PO#HAA 000040\n"
###   # cmd: ":PGEP:PO#HAA 001024\n"
###   # cmd: ":PGEP:PO#HBC 524288\n"
###   # rsp: "#4_001024_rrrrrrrrrr...rrrrrrrrrr\n"
###   print('>>> {} : {}'.format('Test',cmd_str__PGEP_PO))
###   [count,rsp] = scpi_comm_resp_numb_ss (ss, cmd_str__PGEP_PO+b'#HAA 000040\n', 2048)
###   print('len rcvd: ' + str(len(rsp)))
###   print('hex code rcvd: ' + rsp.hex())
###   #
###   #scpi_comm_resp_ss(ss, cmd_str__PGEP_PO+b'#HAA 001024\n')
###   cmd_str = cmd_str__PGEP_PO+b'#HBC 524288\n'
###   print(cmd_str)
###   [count,rsp] = scpi_comm_resp_numb_ss (ss, cmd_str) # 
###   #rsp = scpi_comm_resp_ss(ss, cmd_str__PGEP_PO+b'#HBC 052428\n', 52428) #
###   #rsp = scpi_comm_resp_ss(ss, cmd_str__PGEP_PO+b'#HBC 005242\n', 5242) # 
###   #rsp = scpi_comm_resp_ss(ss, cmd_str__PGEP_PO+b'#HBC 000524\n', 524) # OK
###   print('len rcvd: ' + str(len(rsp)))
###   #
###   # recv size issue ...
###   #   Note that both the IP header and the tcp header can have 
###   #   a size greater than 20, when using options, and so you may 
###   #   still encounter fragmentation with message shorter than 1460 bytes.
###   #   fragmentation with message shorter than 1460 bytes
###   #   python socket 1460 bytes fragmentation
###   #   
###   #   MTU in w5500 // see https://cdn.sparkfun.com/datasheets/Dev/Arduino/Shields/W5500_datasheet_v1.0.2_1.pdf
###   #      ... 1460 bytes is max of w5500 
###   #
###   #   how to receive fragmented packet in a single recv() in socket? --> use multiple recv()
###   #   tcp socket multiple message to recv()
###   #       https://www.software7.com/blog/tcp-and-fragmented-messages/
###   #
###   #        int numLeft = lengthOfMessage;
###   #                int len = 0;
###   #                int numBytes;
###   #                do {
###   #                    numBytes = recv(socket, buf + len, numLeft, 0);
###   #                    len += numBytes;
###   #                    numLeft -= numBytes;
###   #                } while(numBytes > 0 && len < lengthOfMessage);
###   #        
###   #                //check result of recv for closed socket (numBytes == 0) and errors (numBytes < 0)
###   #                //if no errors, then...
###   #   message ... may split 1024 bytes ...
###   #           ... may have a head message and data messages....
###   #
###   # int.from_bytes(b'\xF1\x10', byteorder='big')
###   # >>> int.from_bytes(b'\x01\xFF', byteorder='big')
###   # 511
###   # >>> int.from_bytes(b'\xFF\x01', byteorder='little')
###   # 511
###   #read_back = []
###   #for ii in range(0,10):
###   #    dat = int.from_bytes(bdata[ii*4:ii*4+4], byteorder='little')
###   #    #print(dat)
###   #    read_back += [dat]
###   #print(read_back)
###   #
###   
###   #16 :PGEP:WMI
###   # :PGEP:WMI#Hnn  #Hnnnnnnnn #Hmmmmmmmm<NL>
###   #
###   # ":PGEP:WMI#H00 #HABCD1234 #HFF00FF00\n"
###   print('>>> {} : {}'.format('Test',cmd_str__PGEP_WMI))
###   scpi_comm_resp_ss(ss, cmd_str__PGEP_WMI+b'#H00 #HABCD1234 #HFF00FF00\n')
###   
###   
###   #17 :PGEP:WMO
###   # :PGEP:WMO#Hnn  #Hmmmmmmmm<NL>
###   #
###   # ":PGEP:WMO#H20 #HFFFF0000\n"
###   scpi_comm_resp_ss(ss, cmd_str__PGEP_WMO+b'#H20 #HFFFF0000\n')
###   
###   
###   
##############################################################################

## PGEP disable
print('\n>>> {} : {}'.format('Test',cmd_str__PGEP_EN))
scpi_comm_resp_ss(ss, cmd_str__PGEP_EN+b' OFF\n')
scpi_comm_resp_ss(ss, cmd_str__PGEP_EN+b'?\n')


## close socket
scpi_close(ss)


##



