## test__MHVSU_BASE__lan.py

# ADC test to add 
# SPIO / DAC controls
# rev 6/9 note: txt change ... ADC_IM --> ADC_VM
# change control USB to LAN

from time import sleep
import sys


## tkinter GUI module 
# https://www.tutorialspoint.com/python3/python_gui_programming.htm
# https://www.tutorialspoint.com/python3/tk_button.htm
# https://www.tutorialspoint.com/python3/tk_checkbutton.htm
# https://www.python-course.eu/tkinter_checkboxes.php
# https://www.tutorialspoint.com/python3/tk_entry.htm
# https://www.python-course.eu/tkinter_entry_widgets.php ... entry text number...


### common ###


import platform    # For getting the operating system name
import subprocess  # For executing a shell command

def ping(host):
	"""
	Returns True if host (str) responds to a ping request.
	Remember that a host may not respond to a ping (ICMP) request even if the host name is valid.
	"""
	
	# Option for the number of packets as a function of
	param1 = '-n' if platform.system().lower()=='windows' else '-c'
	param2 = '-w' if platform.system().lower()=='windows' else '-i'
	value2 = '50' if platform.system().lower()=='windows' else '0.05'
	
	# Building the command. Ex: "ping -c 1 google.com"
	command = ['ping', param1, '1', param2, value2, host]
	
	return subprocess.call(command) == 0




###########################################################################
## common converter

def conv_dec_to_bit_2s_comp_16bit(dec, full_scale=20):
	if dec > full_scale/2-full_scale/2**16 :
		dec = full_scale/2-full_scale/2**16
	if dec < -full_scale/2-full_scale/2**16 :
		dec = -full_scale/2-full_scale/2**16
	#bit_2s_comp = int( 0x10000 * ( dec + full_scale/2)    / full_scale ) + 0x8000
	bit_2s_comp = int( 0x10000 * ( dec + full_scale/2)    / full_scale +0.5) + 0x8000
	if bit_2s_comp > 0xFFFF :
		bit_2s_comp -= 0x10000
	return bit_2s_comp

#test_codes = [ conv_dec_to_bit_2s_comp_16bit(x) for x in [-10,-5,0,5,10] ]
#print(test_codes)
	
def conv_bit_2s_comp_16bit_to_dec(bit_2s_comp, full_scale=20):
	if bit_2s_comp >= 0x8000:
		bit_2s_comp -= 0x8000
		#dec = full_scale * (bit_2s_comp) / 0x10000 -10
		dec = full_scale * (bit_2s_comp) / 0x10000 - full_scale/2
	else :
		dec = full_scale * (bit_2s_comp) / 0x10000
		if dec == full_scale/2-full_scale/2**16 :
			dec = full_scale/2
	return dec

#test_codes2 = [ conv_bit_2s_comp_16bit_to_dec(x) for x in test_codes ]
#print(test_codes2)

	


###########################################################################
## open LAN socket  ####################################################

import socket

## socket control parameters 

HOST = '192.168.168.143'  # The server's hostname or IP address // MHVSU-BASE test ip
HOST143 = '192.168.168.143'  # The server's hostname or IP address // MHVSU-BASE test ip
PORT = 5025               # The port used by the server
#TIMEOUT = 5.3 # socket timeout
TIMEOUT = 500 # socket timeout // for debug 
#TIMEOUT = 1000 # socket timeout // for debug 1000s
SO_SNDBUF = 2048
SO_RCVBUF = 32768
INTVAL = 0.1 # sec for waiting before recv()
BUF_SIZE_NORMAL = 2048
BUF_SIZE_LARGE = 16384
TIMEOUT_LARGE = TIMEOUT*10

ss = None # socket




## command strings ##############################################################
cmd_str__IDN      = b'*IDN?\n'
cmd_str__RST      = b'*RST\n'
cmd_str__EPS_EN   = b':EPS:EN'
cmd_str__EPS_WMI  = b':EPS:WMI'
cmd_str__EPS_WMO  = b':EPS:WMO'
cmd_str__EPS_TAC  = b':EPS:TAC'
cmd_str__EPS_TMO  = b':EPS:TMO' 
cmd_str__EPS_TWO  = b':EPS:TWO' ## new
cmd_str__EPS_PI   = b':EPS:PI'
cmd_str__EPS_PO   = b':EPS:PO'

##  cmd_str__EPS_MKWI = b':EPS:MKWI'
##  cmd_str__EPS_MKWO = b':EPS:MKWO'
##  cmd_str__EPS_MKTI = b':EPS:MKTI'
##  cmd_str__EPS_MKTO = b':EPS:MKTO'
##  cmd_str__EPS_WI   = b':EPS:WI'
##  cmd_str__EPS_WO   = b':EPS:WO'
##  cmd_str__EPS_TI   = b':EPS:TI'
##  cmd_str__EPS_TO   = b':EPS:TO'
#

## scpi functions ####################################################################

def scpi_open (timeout=TIMEOUT):
	try:
		ss = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		ss.settimeout(timeout)
		ss.setsockopt(socket.SOL_SOCKET, socket.SO_SNDBUF, SO_SNDBUF)
		ss.setsockopt(socket.SOL_SOCKET, socket.SO_RCVBUF, SO_RCVBUF) # 8192 16384 32768 65536
	except OSError as msg:
		ss = None
		print('error in socket: ', msg)
		raise
	return ss

def scpi_connect (ss, HOST, PORT):
	try:
		ss.connect((HOST, PORT))
	except OSError as msg:
		ss.close()
		ss = None
		print('error in connect: ', msg)
		raise


def scpi_close (ss):
	try:
		ss.close()
	except:
		if ss == None:
			print('error: ss==None')
		raise

def scpi_comm_resp_ss (ss, cmd_str, buf_size=BUF_SIZE_NORMAL, intval=INTVAL) :
	try:
		if __debug__:print('Send:', repr(cmd_str))
		ss.sendall(cmd_str)
	except:
		if __debug__:print('error in sendall')
		raise
	##
	sleep(intval)
	#
	# recv data until finding the sentinel '\n'
	try:
		data = ss.recv(buf_size) # try 1024 131072 524288
		# try   
		while (1):
			if (chr(data[-1])=='\n'): # check the sentinel '\n' ## not with numeric block
				break
			data = data + ss.recv(buf_size)
	except:
		if __debug__:print('error in recv')
		raise
	#
	## check response 
	if (len(data)>20):
		if __debug__:print('Received:', repr(data[0:20]),  ' (first 20 bytes)')
	else:
		if __debug__:print('Received:', repr(data))
	#
	# NG response check 
	if data[0:2]==b'NG':
		if __debug__:print('Received: NG as response')
		#input('Press Enter key!')
		#sleep(3)
	#
	return data


# scpi command for numeric block response
def scpi_comm_resp_numb_ss (ss, cmd_str, buf_size=BUF_SIZE_LARGE, intval=INTVAL, timeout_large=TIMEOUT_LARGE) :
	try:
		if __debug__:print('Send:', repr(cmd_str))
		ss.sendall(cmd_str)
	except:
		if __debug__:print('error in sendall')
		raise
	##
	sleep(intval)
	#
	# cmd: ":PGEP:PO#HBC 524288\n"
	# rsp: "#4_001024_rrrrrrrrrr...rrrrrrrrrr\n"
	#
	# recv data until finding the sentinel '\n' 
	# but check the sentinel after the data byte count is met.
	#
	# read timeout
	to = ss.gettimeout()
	#print(to)
	# increase timeout
	ss.settimeout(timeout_large)
	#
	try:
		# find the numeric head : must 10 in data 
		data = ss.recv(buf_size)
		while True:
			if len(data)>=10:
				break
			data = data + ss.recv(buf_size)
		#
		#print('header: ', repr(data[0:10])) # header
		#
		# find byte count 
		byte_count = int(data[3:9])
		#print('byte_count=', repr(byte_count)) 
		#
		# collect all data by byte count
		count_to_recv = byte_count + 10 + 1# add header count #add /n
		while True:
			if len(data)>=count_to_recv:
				break
			data = data + ss.recv(buf_size)
		#
		# check the sentinel 
		while True:
			if (chr(data[-1])=='\n'): # check the sentinel '\n' 
				break
			data = data + ss.recv(buf_size)
		#
	except:
		if __debug__:print('error in recv')
		raise
	#
	if (len(data)>20):
		if __debug__:print('Received:', repr(data[0:20]),  ' (first 20 bytes)')
	else:
		if __debug__:print('Received:', repr(data))
	#
	# timeout back to prev
	ss.settimeout(to)
	#
	data = data[10:(10+byte_count)]
	if __debug__:print('data:', data[0:20].hex(),  ' (first 20 bytes)')
	#
	return [byte_count, data]
	



###########################################################################
## open OK USB device  ####################################################

##  import ok
##  
##  # OK init
##  dev = ok.okCFrontPanel()
##  
##  # OK open
##  DeviceCount = dev.GetDeviceCount()
##  print(' {}={}'.format('DeviceCount',DeviceCount))
##  dev.OpenBySerial("")
##  DevSR = dev.GetSerialNumber()
##  print(' {}={}'.format('DevSR',DevSR))


###########################################################################
## EPS_Dev replaces USB dev class  ########################################

class EPS_Dev:
	dev_count = 0
	idn = []
	ss = None # socket
	#
	f_scpi_open = scpi_open
	f_scpi_connect = scpi_connect
	f_scpi_close = scpi_close
	f_scpi_cmd = scpi_comm_resp_ss
	f_scpi_cmd_numb = scpi_comm_resp_numb_ss
	#
	def _test(self):
		return '_class__EPS_Dev_'
	#
	def GetDeviceCount(self):
		# must update from ping ip ... or else 
		self.dev_count = 1
		return self.dev_count 
	#
	def Init(self):
		# nothing
		pass
	def Open(self, hh=HOST, pp=PORT):
		##  # open scpi
		##  self.ss = LAN_CMU_Dev.f_scpi_open()		
		##  # connect scpi
		##  LAN_CMU_Dev.f_scpi_connect(self.ss,hh,pp)
		##  # board reset 
		##  ret = LAN_CMU_Dev.f_scpi_cmd(self.ss, cmd_str__RST).decode()
		##  # LAN end-point control enable
		##  ret = LAN_CMU_Dev.f_scpi_cmd(self.ss, cmd_str__CMEP_EN+b' ON\n').decode()
		##  return ret
		
		# 
		self.ss = EPS_Dev.f_scpi_open()
		try:
			print('>> try to connect : {}:{}'.format(hh,pp))
			EPS_Dev.f_scpi_connect(self.ss,hh,pp)
		except socket.timeout:
			self.ss = None
		except ConnectionRefusedError:
			self.ss = None
		except:
			raise
		return self.ss
	#
	def IsOpen(self):
		if self.ss == None:
			ret = False
		else:
			ret = True
		return ret	
	#
	def GetSerialNumber(self):
		ret = EPS_Dev.f_scpi_cmd(self.ss, cmd_str__IDN).decode() # will revise
		return ret # must come from board later 
	def ConfigureFPGA(self, opt=[]):
		# not support
		pass
		return 0
	def GetErrorString(self, opt):
		# not support
		pass
		return []
	def Close(self):
		##  ret = EPS_Dev.f_scpi_cmd(self.ss, cmd_str__CMEP_EN+b' OFF\n').decode()
		# close scpi
		EPS_Dev.f_scpi_close(self.ss)
		self.ss = None
		return ret
	#
	def GetWireOutValue(self, adrs, mask=0xFFFFFFFF):
		# :EPS:WMO#Hnn  #Hmmmmmmmm<NL>
		cmd_str = cmd_str__EPS_WMO + ('#H{:02X} #H{:08X}\n'.format(adrs,mask)).encode()
		rsp_str = EPS_Dev.f_scpi_cmd(self.ss, cmd_str)
		#
		rsp = rsp_str.decode()
		# assume hex decimal response: #HF3190306<NL>
		rsp = '0x' + rsp[2:-1] # convert "#HF3190306<NL>" --> "0xF3190306"
		rsp = int(rsp,16) # convert hex into int
		return rsp
	def UpdateWireOuts(self):
		# no global update : nothing to do.
		pass
	def SetWireInValue(self, adrs, data, mask=0xFFFFFFFF):
		# :EPS:WMI#Hnn  #Hnnnnnnnn #Hmmmmmmmm<NL>
		cmd_str = cmd_str__EPS_WMI + ('#H{:02X} #H{:08X} #H{:08X}\n'.format(adrs,data,mask)).encode()
		rsp_str = EPS_Dev.f_scpi_cmd(self.ss, cmd_str)
		#
		rsp = rsp_str.decode()
		return rsp
	def UpdateWireIns(self):
		# no global update : nothing to do.
		pass
	#
	def ActivateTriggerIn(self, adrs, loc_bit):
		## activate trig 
		# :EPS:TAC#Hnn  #Hnn<NL>
		#
		cmd_str = cmd_str__EPS_TAC + ('#H{:02X} #H{:02X}\n'.format(adrs,loc_bit)).encode()
		rsp_str = EPS_Dev.f_scpi_cmd(self.ss, cmd_str)
		rsp = rsp_str.decode()
		return rsp
	#
	def UpdateTriggerOuts(self) :
		# no global update : nothing to do.
		pass
	#
	def IsTriggered (self, adrs, mask):
		# cmd: ":EPS:TMO#H60 #H0000FFFF\n"
		# rsp: "ON\n" or "OFF\n"
		cmd_str = cmd_str__EPS_TMO + ('#H{:02X} #H{:08X}\n'.format(adrs,mask)).encode()
		rsp_str = EPS_Dev.f_scpi_cmd(self.ss, cmd_str)
		#
		rsp = rsp_str.decode()
		#
		if rsp[0:3]=='OFF':
			ret =  False
		elif rsp[0:2]=='ON':
			ret =  True
		else:
			ret =  None
		#
		return ret
	#
	def GetTriggerOutVector(self, adrs, mask=0xFFFFFFFF):
		# cmd: ":EPS:TWO#H60 #H0000FFFF\n"
		# rsp: "#H000O3245\n" 
		cmd_str = cmd_str__EPS_TWO + ('#H{:02X} #H{:08X}\n'.format(adrs,mask)).encode()
		rsp_str = EPS_Dev.f_scpi_cmd(self.ss, cmd_str)
		#
		rsp = rsp_str.decode()
		# assume hex decimal response: #HF3190306<NL>
		rsp = '0x' + rsp[2:-1] # convert "#HF3190306<NL>" --> "0xF3190306"
		rsp = int(rsp,16) # convert hex into int
		return rsp
	#
	def ReadFromPipeOut(self, adrs, data_bytearray):
		## read pipeout
		# cmd: ":EPS:PO#HAA 001024\n"
		# rsp: "#4_001024_rrrrrrrrrr...rrrrrrrrrr\n"		
		#
		byte_count = len(data_bytearray)
		#
		cmd_str = cmd_str__EPS_PO + ('#H{:02X} {:06d}\n'.format(adrs,byte_count)).encode()
		if __debug__:print(cmd_str)
		#
		[rsp_cnt, rsp_str] = EPS_Dev.f_scpi_cmd_numb(self.ss, cmd_str)
		#
		# assume numeric block : "#4_001024_rrrrrrrrrr...rrrrrrrrrr\n"
		# assume rsp_str is data part.
		# copy data 
		for ii in range(0,rsp_cnt): 
			data_bytearray[ii] = rsp_str[ii]
		#
		return rsp_cnt

	def WriteToPipeIn(self, adrs, data_bytearray):
		## write pipein
		# cmd: ":EPS:PI#H8A #4_001024_rrrrrrrrrr...rrrrrrrrrr\n"
		# rsp: "OK\n"		
		
		# check later : byte_count is multiple of 4 
		
		#
		byte_count = len(data_bytearray)
		#
		#cmd_str = cmd_str__EPS_PI + ('#H{:02X} #4_{:06d}_{}\n'.format(adrs,byte_count,data_bytearray)).encode() # NG
		cmd_str = cmd_str__EPS_PI + ('#H{:02X} #4_{:06d}_'.format(adrs,byte_count)).encode() + data_bytearray + b'\n'
		if __debug__:print(cmd_str)
		
		rsp_str = EPS_Dev.f_scpi_cmd(self.ss, cmd_str)
		
		rsp = rsp_str.decode() # OK or NG
		
		#
		return rsp
		

# class init
dev = EPS_Dev()
# test
print(dev._test())


###########################################################################
# check debug mode
if __debug__:
	print('>>> In debug mode ... ')


###########################################################################
## load shell parameters for IP 

# example command in shell: 
#    python test__MHVSU_BASE__lan.py  192.168.168.143  5025
#    slot id -- default ip address 
#    0x0     -- 192.168.168.128
#    0x1     -- 192.168.168.129
#    0x2     -- 192.168.168.130
#    0x3     -- 192.168.168.131
#    0x4     -- 192.168.168.132
#    0x5     -- 192.168.168.133
#    0x6     -- 192.168.168.134
#    0x7     -- 192.168.168.135
#    0x8     -- 192.168.168.136
#    0x9     -- 192.168.168.137
#    0xA     -- 192.168.168.138
#    0xB     -- 192.168.168.139
#    0xC     -- 192.168.168.140
#    0xE     -- 192.168.168.141
#    0xD     -- 192.168.168.142
#    0xF     -- 192.168.168.143 (slot id is not detected)
_host_names_ = [
	'192.168.168.143',
	'192.168.168.128',
	'192.168.168.129',
	'192.168.168.130',
	'192.168.168.131',
	'192.168.168.132',
	'192.168.168.133',
	'192.168.168.134',
	'192.168.168.135',
	'192.168.168.136',
	'192.168.168.137',
	'192.168.168.138',
	'192.168.168.139',
	'192.168.168.140',
	'192.168.168.141',
	'192.168.168.142']

print(sys.argv[0])
argc=len(sys.argv)
print(argc)
#
if argc>1:
	_host_                    = sys.argv[1]         # ex: 192.168.168.143
else:
	#_host_                    = '192.168.168.143'
	for xx in _host_names_:
		if ping(xx):
			_host_ = xx
			print('{} is available.'.format(xx))
			break 
		else:
			print('{} is NOT available.'.format(xx))
#
if argc>2:
	_port_                    = int  (sys.argv[2])  # ex: 5025
else:
	_port_                    = 5025
#


###########################################################################
## open socket and connect scpi server ####################################

#ss = dev.Open(HOST143,PORT) # firmware test
ss = dev.Open(_host_,_port_) # firmware test

#if ss == None :
#	raise


## flag check max retry count
MAX_count = 50
##MAX_count = 500 # 20000 # 280 for 192000 samples
#MAX_count = 5000 # 1500 for 25600 samples @ 12500 count period

## note 
# (1 / ((192 MHz) / 12500)) * 2560 = 166.666667 milliseconds
# (1 / ((192 MHz) / 12500)) * 5120 = 333.333333 milliseconds


if dev.IsOpen():
	print('>>> device opened.')
else:
	print('>>> device is not open.')
	## raise
	input('')
	MAX_count = 3


ret = dev.GetSerialNumber()
print(ret)

# input('stop!!')

###########################################################################

### scpi : *RST
print('\n>>> {} : {}'.format('Test',cmd_str__RST))
rsp = scpi_comm_resp_ss(ss, cmd_str__RST)
print('hex code rcvd: ' + rsp.hex())
print('string rcvd: ' + repr(rsp))

### scpi : *IDN?
print('\n>>> {} : {}'.format('Test',cmd_str__IDN))
rsp = scpi_comm_resp_ss(ss, cmd_str__IDN)
print('hex code rcvd: ' + rsp.hex())
print('string rcvd: ' + repr(rsp))


### scpi : ":EPS:EN?"
print('\n>>> {} : {}'.format('Test',cmd_str__EPS_EN))
rsp = scpi_comm_resp_ss(ss, cmd_str__EPS_EN +b'?\n')
#print('hex code rcvd: ' + rsp.hex())
print('string rcvd: ' + repr(rsp))


#### check fpga ID through dedicated EP vs PORT EP
#    ADRS_FPGA_IMAGE_MHVSU
#    ADRS_PORT_WO_20_MHVSU : ":EPS:WMO#H23 #HFFFFFFFF\n"

### scpi : ":EPS:WMO#H20 #HFFFFFFFF\n"
print('\n>>> {} : {}'.format('Test',cmd_str__EPS_WMO))
rsp = scpi_comm_resp_ss(ss, cmd_str__EPS_WMO +b'#H20 #HFFFFFFFF\n')
#print('hex code rcvd: ' + rsp.hex())
print('string rcvd: ' + repr(rsp))

dev.UpdateWireOuts()
FPGA_image_id = dev.GetWireOutValue(0x20)
print('{} = 0x{:08X}'.format('FPGA_image_id', FPGA_image_id))

input('')

#### test LED 

print('\n>>> {} : {}'.format('Test',cmd_str__EPS_WMI))
### scpi : ":EPS:WMI#H03 #H00000000 #H0000000F\n"
### scpi : ":EPS:WMI#H03 #H0000000E #H0000000F\n"
### scpi : ":EPS:WMI#H03 #H0000000B #H0000000F\n"
### scpi : ":EPS:WMI#H03 #H00000007 #H0000000F\n"
### scpi : ":EPS:WMI#H03 #H0000000D #H0000000F\n"
### scpi : ":EPS:WMI#H03 #H0000000F #H0000000F\n"
rsp = scpi_comm_resp_ss(ss, cmd_str__EPS_WMI +b'#H03 #H00000000 #H0000000F\n')
sleep(0.1)
rsp = scpi_comm_resp_ss(ss, cmd_str__EPS_WMI +b'#H03 #H0000000E #H0000000F\n')
sleep(0.1)
rsp = scpi_comm_resp_ss(ss, cmd_str__EPS_WMI +b'#H03 #H0000000B #H0000000F\n')
sleep(0.1)
rsp = scpi_comm_resp_ss(ss, cmd_str__EPS_WMI +b'#H03 #H00000007 #H0000000F\n')
sleep(0.1)
rsp = scpi_comm_resp_ss(ss, cmd_str__EPS_WMI +b'#H03 #H0000000D #H0000000F\n')
sleep(0.1)
rsp = scpi_comm_resp_ss(ss, cmd_str__EPS_WMI +b'#H03 #H0000000F #H0000000F\n')
sleep(0.1)

#### test count2 for trigger 

#	xil_printf(">>> test count :  clear autocount2 \r\n");
#	write_mcs_ep_wi(ADRS_BASE_MHVSU, 0x01, 0x00000000, 0x00000004); // adrs_base, EP_offset_EP, data, mask
rsp = scpi_comm_resp_ss(ss, cmd_str__EPS_WMI +b'#H01 #H00000000 #H00000004\n')
sleep(0.2)
#
#	xil_printf(">>> test count2 \r\n");
#	// read  : test counts at WO21 
#	value = read_mcs_ep_wo(ADRS_BASE_MHVSU, 0x21, 0x0000FFFF);
rsp = scpi_comm_resp_ss(ss, cmd_str__EPS_WMO +b'#H21 #H0000FFFF\n')
#	xil_printf("WO21 rd: 0x%08X \r\n", value );
#	//
#	activate_mcs_ep_ti(ADRS_BASE_MHVSU,0x41,0); // reset : test count2 TI41[0]
rsp = scpi_comm_resp_ss(ss, cmd_str__EPS_TAC +b'#H41 #H00\n')
#	value = read_mcs_ep_wo(ADRS_BASE_MHVSU, 0x21, 0x0000FFFF);
rsp = scpi_comm_resp_ss(ss, cmd_str__EPS_WMO +b'#H21 #H0000FFFF\n')
#	xil_printf("WO21 rd: 0x%08X \r\n", value );
#	//
#	activate_mcs_ep_ti(ADRS_BASE_MHVSU,0x41,1); // up    : test count2 TI41[1] 
rsp = scpi_comm_resp_ss(ss, cmd_str__EPS_TAC +b'#H41 #H01\n')
#	value = read_mcs_ep_wo(ADRS_BASE_MHVSU, 0x21, 0x0000FFFF);
rsp = scpi_comm_resp_ss(ss, cmd_str__EPS_WMO +b'#H21 #H0000FFFF\n')
#	xil_printf("WO21 rd: 0x%08X \r\n", value );
#	//
#	activate_mcs_ep_ti(ADRS_BASE_MHVSU,0x41,2); // down  : test count2 TI41[2] 
rsp = scpi_comm_resp_ss(ss, cmd_str__EPS_TAC +b'#H41 #H02\n')
#	value = read_mcs_ep_wo(ADRS_BASE_MHVSU, 0x21, 0x0000FFFF);
rsp = scpi_comm_resp_ss(ss, cmd_str__EPS_WMO +b'#H21 #H0000FFFF\n')
#	xil_printf("WO21 rd: 0x%08X \r\n", value );
#
#	// test count :  set autocount2
#	xil_printf(">>> test count :  set autocount2 \r\n");
#	write_mcs_ep_wi(ADRS_BASE_MHVSU, 0x01, 0x00000004, 0x00000004); // adrs_base, EP_offset_EP, data, mask
rsp = scpi_comm_resp_ss(ss, cmd_str__EPS_WMI +b'#H01 #H00000004 #H00000004\n')


######################################################
## TODO: check MHVSU FPGA image ID 

### adc test ###
#__FPGA_IMG_IMP_INFO__ = 0xD220 
#__FPGA_IMG_IMP_DATE__ = 0x0610
#BIT_FILENAME = './img_h_D2_20_0610_c/xem7310__mhvsu_base__top.bit'
### adc-acc test ###
#__FPGA_IMG_IMP_INFO__ = 0xD220 
#__FPGA_IMG_IMP_DATE__ = 0x0618
#BIT_FILENAME = ''
### adc-acc 192MHz test ###
#__FPGA_IMG_IMP_INFO__ = 0xD220 
#__FPGA_IMG_IMP_DATE__ = 0x0619
#BIT_FILENAME = ''
### adc-p_clk test ###
#__FPGA_IMG_IMP_INFO__ = 0xD320 
#__FPGA_IMG_IMP_DATE__ = 0x0622
#BIT_FILENAME = ''
### adc-min-max test ###
#__FPGA_IMG_IMP_INFO__ = 0xD320 
#__FPGA_IMG_IMP_DATE__ = 0x0623
#BIT_FILENAME = ''
### adc-fifo test ###
#__FPGA_IMG_IMP_INFO__ = 0xD320 
#__FPGA_IMG_IMP_DATE__ = 0x0629
#BIT_FILENAME = ''
### ext-trig test ###
#__FPGA_IMG_IMP_INFO__ = 0xD320 
#__FPGA_IMG_IMP_DATE__ = 0x0709
#BIT_FILENAME = ''
### ext-trig test + M1-enabled + hs adc test pattern ###
#__FPGA_IMG_IMP_INFO__ = 0xD520 
#__FPGA_IMG_IMP_DATE__ = 0x0710
#BIT_FILENAME = ''
### acc 32bit support + sspi wo ###
#__FPGA_IMG_IMP_INFO__ = 0xD620 
#__FPGA_IMG_IMP_DATE__ = 0x0712
#BIT_FILENAME = ''
### SPIO forced mode support ###
#__FPGA_IMG_IMP_INFO__ = 0xD620 
#__FPGA_IMG_IMP_DATE__ = 0x0714
#BIT_FILENAME = ''
### LAN support ###
#__FPGA_IMG_IMP_INFO__ = 0xD820 
#__FPGA_IMG_IMP_DATE__ = 0x0828
#BIT_FILENAME = ''
### EEPROM test ###
__FPGA_IMG_IMP_INFO__ = 0xD920 
__FPGA_IMG_IMP_DATE__ = 0x0923
BIT_FILENAME = ''
#

#
dev.UpdateWireOuts()
FPGA_image_id = dev.GetWireOutValue(0x20)
print('{} = 0x{:08X}'.format('FPGA_image_id', FPGA_image_id))
# TODO: configure FPGA
if (FPGA_image_id==0): 
	error = dev.ConfigureFPGA(BIT_FILENAME)
	if error !=0:
		print('ConfigureFPGA Error {} : {}'.format(error,dev.GetErrorString(error)))
		## raise
		input('')
	# read again
	dev.UpdateWireOuts()
	FPGA_image_id = dev.GetWireOutValue(0x20)
	print('{} = 0x{:08X}'.format('FPGA_image_id', FPGA_image_id))
#
if not (FPGA_image_id>>16 == __FPGA_IMG_IMP_INFO__):
	print('>>> Please check FPGA iamge ID : ADC test.')
	## raise
	input('')	
#
if not (FPGA_image_id & 0xFFFF == __FPGA_IMG_IMP_DATE__):
	print('>>> Please check FPGA iamge ID : implmentation date.')
	## raise
	input('')


	
## input('stop!!')



## TODO: EEPROM functions 
#  // w_MEM_WI   
#  assign w_num_bytes_DAT               = w_MEM_WI[12:0]; // 12-bit 
#  assign w_con_disable_SBP             = w_MEM_WI[15]; // 1-bit
#  assign w_con_fifo_path__L_sspi_H_lan = w_MEM_WI[16]; // 1-bit
#  assign w_con_port__L_MEM_SIO__H_TP   = w_MEM_WI[17]; // 1-bit
#  
#  // w_MEM_FDAT_WI
#  assign w_frame_data_CMD              = w_MEM_FDAT_WI[ 7: 0]; // 8-bit
#  assign w_frame_data_STA_in           = w_MEM_FDAT_WI[15: 8]; // 8-bit
#  assign w_frame_data_ADL              = w_MEM_FDAT_WI[23:16]; // 8-bit
#  assign w_frame_data_ADH              = w_MEM_FDAT_WI[31:24]; // 8-bit
#  
#  // w_MEM_TI
#  assign w_MEM_rst      = w_MEM_TI[0];
#  assign w_MEM_fifo_rst = w_MEM_TI[1];
#  assign w_trig_frame   = w_MEM_TI[2];
#  
#  // w_MEM_TO
#  assign w_MEM_TO[0]     = w_MEM_valid    ;
#  assign w_MEM_TO[1]     = w_done_frame   ;
#  assign w_MEM_TO[2]     = w_done_frame_TO; //$$ rev
#  assign w_MEM_TO[15: 8] = w_frame_data_STA_out; 
#  
#  // w_MEM_PI
#  assign w_frame_data_DAT_wr    = w_MEM_PI[7:0]; // 8-bit
#  assign w_frame_data_DAT_wr_en = w_MEM_PI_wr;
#  
#  // w_MEM_PO
#  assign w_MEM_PO[7:0]          = w_frame_data_DAT_rd; // 8-bit
#  assign w_MEM_PO[31:8]         = 24'b0; // unused
#  assign w_frame_data_DAT_rd_en = w_MEM_PO_rd;

def eeprom_send_frame_ep (MEM_WI, MEM_FDAT_WI):
	## //// end-point map :
	## // wire [31:0] w_MEM_WI      = ep13wire;
	## // wire [31:0] w_MEM_FDAT_WI = ep12wire;
	## // wire [31:0] w_MEM_TI = ep53trig; assign ep53ck = sys_clk;
	## // wire [31:0] w_MEM_TO; assign ep73trig = w_MEM_TO; assign ep73ck = sys_clk;
	## // wire [31:0] w_MEM_PI = ep93pipe; wire w_MEM_PI_wr = ep93wr; 
	## // wire [31:0] w_MEM_PO; assign epB3pipe = w_MEM_PO; wire w_MEM_PO_rd = epB3rd; 	

	print('{} = 0x{:08X}'.format('MEM_WI', MEM_WI))
	dev.SetWireInValue(0x13,MEM_WI,0xFFFFFFFF) # (ep,val,mask)
	#dev.UpdateWireIns()	
	
	print('{} = 0x{:08X}'.format('MEM_FDAT_WI', MEM_FDAT_WI))
	dev.SetWireInValue(0x12,MEM_FDAT_WI,0xFFFFFFFF) # (ep,val,mask)
	dev.UpdateWireIns()	
	
	# clear TO 
	dev.UpdateTriggerOuts()
	ret=dev.GetTriggerOutVector(0x73)
	print('{} = 0x{:08X}'.format('ret', ret))


	# act TI 
	dev.ActivateTriggerIn(0x53, 2)	## (ep, loc)
	
	# check frame done
	cnt_loop = 0;
	while 1:
		# First, query all XEM Trigger Outs.
		dev.UpdateTriggerOuts()
		# check trigger out //$$  0x01 w_MEM_TO[0]  or  0x04 w_MEM_TO[2] 
		if dev.IsTriggered(0x73, 0x04) == True: # // (ep, mask)
			break
		cnt_loop += 1;
		print('{} = {}'.format('cnt_loop', cnt_loop))
		if (cnt_loop>MAX_count):
			break
	print('{} = {}'.format('cnt_loop', cnt_loop))

	# # read again TO 
	# dev.UpdateTriggerOuts()
	# ret=dev.GetTriggerOutVector(0x73)
	# print('{} = 0x{:08X}'.format('ret', ret))

	#
	return ret

def eeprom_send_frame (CMD=0x05, STA_in=0, ADL=0, ADH=0, num_bytes_DAT=1, con_disable_SBP=0):
	## 
	#num_bytes_DAT               = 1
	#con_disable_SBP             = 0
	
	con_fifo_path__L_sspi_H_lan = 1 # LAN access
	#con_fifo_path__L_sspi_H_lan = 0 # slave spi access

	con_port__L_MEM_SIO__H_TP   = 1 # test TP	
	#con_port__L_MEM_SIO__H_TP   = 0 # test MEM_SIO
	
	#
	set_data_WI = (con_port__L_MEM_SIO__H_TP<<17) + (con_fifo_path__L_sspi_H_lan<<16) + (con_disable_SBP<<15) + num_bytes_DAT
	
	frame_data_CMD     = CMD    ## 0x05
	frame_data_STA_in  = STA_in ## 0x00
	frame_data_ADL     = ADL    ## 0x00
	frame_data_ADH     = ADH    ## 0x00
	#
	set_data_FDAT_WI = (frame_data_ADH<<24) + (frame_data_ADL<<16) + (frame_data_STA_in<<8) + frame_data_CMD
	
	ret = eeprom_send_frame_ep (MEM_WI=set_data_WI, MEM_FDAT_WI=set_data_FDAT_WI)
	#
	return ret


## write enable or disable 
def eeprom_write_enable():
	print('\n>>>>>> eeprom_write_enable')
	#
	## // CMD_WREN__96 
	print('\n>>> CMD_WREN__96')
	eeprom_send_frame (CMD=0x96, con_disable_SBP=1)

def eeprom_write_disable():
	print('\n>>>>>> eeprom_write_disable')
	#
	## // CMD_WRDI__91 
	print('\n>>> CMD_WRDI__91')
	eeprom_send_frame (CMD=0x91)

## manage status 
def eeprom_read_status():
	print('\n>>>>>> eeprom_read_status')
	#
	
	## // CMD_RDSR__05 
	print('\n>>> CMD_RDSR__05')
	eeprom_send_frame (CMD=0x05) 

	# clear TO 
	dev.UpdateTriggerOuts()
	ret=dev.GetTriggerOutVector(0x73)
	print('{} = 0x{:08X}'.format('ret', ret))

	# read again TO 
	dev.UpdateTriggerOuts()
	ret=dev.GetTriggerOutVector(0x73)
	print('{} = 0x{:08X}'.format('ret', ret))
	
	BP1 = (ret>>11)&0x01
	BP0 = (ret>>10)&0x01
	WEL = (ret>> 9)&0x01
	WIP = (ret>> 8)&0x01
	
	#
	return [BP1, BP0, WEL, WIP]
	
def eeprom_write_status(BP1, BP0):
	print('\n>>>>>> eeprom_write_status')
	#

	## // CMD_WREN__96 
	#print('\n>>> CMD_WREN__96')
	#eeprom_send_frame (CMD=0x96)
	eeprom_write_enable()
	
	##
	STA_in = (BP1<<3) + (BP0<<2)
	
	## // CMD_WRSR__6E
	print('\n>>> CMD_WRSR__6E')
	#eeprom_send_frame (CMD=0x6E, STA_in=0x0C)
	#eeprom_send_frame (CMD=0x6E, STA_in=0x08)
	#eeprom_send_frame (CMD=0x6E, STA_in=0x04)
	#eeprom_send_frame (CMD=0x6E, STA_in=0x00)
	eeprom_send_frame (CMD=0x6E, STA_in=STA_in)
	
	#
	return None


## erase or fill ones
def eeprom_erase_all():
	print('\n>>>>>> eeprom_erase_all')
	#
	
	eeprom_write_enable()
	
	## // CMD_ERAL__6D
	print('\n>>> CMD_ERAL__6D')
	eeprom_send_frame (CMD=0x6D)

	pass
	
def eeprom_set_all():
	print('\n>>>>>> eeprom_set_all')
	#
	
	eeprom_write_enable()
	
	## // CMD_SETAL_67
	print('\n>>> CMD_SETAL_67')
	eeprom_send_frame (CMD=0x67)

	pass
	

## manage fifo 
def eeprom_reset_fifo():
	print('\n>>>>>> eeprom_reset_fifo')
	
	#  // w_MEM_TI
	#  assign w_MEM_rst      = w_MEM_TI[0];
	#  assign w_MEM_fifo_rst = w_MEM_TI[1];
	#  assign w_trig_frame   = w_MEM_TI[2];	
	
	# act TI 
	dev.ActivateTriggerIn(0x53, 1)	## (ep, loc)
	
	pass

def eeprom_read_fifo(num_data=1):
	print('\n>>>>>> eeprom_read_fifo')
	#
	
	bytes_in_one_sample = 4 # for 32-bit end-point
	num_bytes_from_fifo = num_data * bytes_in_one_sample
	print('{} = {}'.format('num_bytes_from_fifo', num_bytes_from_fifo))
	
	## setup data buffer for fifo data
	dataout = bytearray([0] * num_bytes_from_fifo)
	
	## call api function to read pipeout data
	data_count = dev.ReadFromPipeOut(0xB3, dataout)
	print('{} : {}'.format('data_count [byte]',data_count))
	
	##  if data_count<0:
	##  	#return
	##  	# set test data 
	##  	num_data = 40
	##  	data_count = num_data * bytes_in_one_sample
	##  	data_int_list = [1,2,3, 2, 1, -1 ]
	##  	data_bytes_list = [x.to_bytes(bytes_in_one_sample,byteorder='little',signed=True) for x in data_int_list]
	##  	print('{} = {}'.format('data_bytes_list', data_bytes_list))
	##  	#dataout = b'\x01\x00\x00\x00\x02\x00\x00\x00'
	##  	dataout = b''.join(data_bytes_list)
	
	## convert bytearray to 32-bit data : high 24 bits to be ignored due to 8-bit fifo
	data_fifo_int_list = []
	for ii in range(0,num_data):
		temp_data = int.from_bytes(dataout[ii*bytes_in_one_sample:(ii+1)*bytes_in_one_sample], byteorder='little', signed=True)
		data_fifo_int_list += [temp_data&0x000000FF] # mask low 8-bit
	
	
	## print out 
	#if __debug__:print('{} = {}'.format('data_fifo_int_list', data_fifo_int_list))
	
	return data_fifo_int_list


def eeprom_write_fifo(datain__int_list=[0]):

	## convert 32-bit data to bytearray
	bytes_in_one_sample = 4 # for 32-bit end-point
	num_data = len(datain__int_list)
	num_bytes_to_fifo = num_data * bytes_in_one_sample
	datain = bytearray([0] * num_bytes_to_fifo)
	
	# convert bytes list : high 24 bits to be ignored due to 8-bit fifo
	datain__bytes_list = [x.to_bytes(bytes_in_one_sample,byteorder='little',signed=True) for x in datain__int_list]
	if __debug__:print('{} = {}'.format('datain__bytes_list', datain__bytes_list))
	
	# take out bytearray from list
	datain = b''.join(datain__bytes_list) 
	if __debug__:print('{} = {}'.format('datain', datain))
	
	## call api for pipein
	data_count = dev.WriteToPipeIn(0x93, datain)
	
	return 


## read/write data
def eeprom_read_data(ADRS_16b=0x0000, num_bytes_DAT=1):
	print('\n>>>>>> eeprom_read_data')
	#
	
	## reset fifo test 
	eeprom_reset_fifo()

	## convert address
	ADL = (ADRS_16b>>0)&0x00FF 
	ADH = (ADRS_16b>>8)&0x00FF
	print('{} = 0x{:08X}'.format('ADRS_16b', ADRS_16b))
	print('{} = 0x{:04X}'.format('ADH', ADH))
	print('{} = 0x{:04X}'.format('ADL', ADL))
	
	## // CMD_READ__03 
	print('\n>>> CMD_READ__03')
	eeprom_send_frame (CMD=0x03, ADL=ADL, ADH=ADH, num_bytes_DAT=num_bytes_DAT)

	## call fifo
	ret = eeprom_read_fifo(num_data=num_bytes_DAT)

	#
	return ret

	
def eeprom_read_data_current(num_bytes_DAT=1):
	print('\n>>>>>> eeprom_read_data_current')
	#
	
	## reset fifo test 
	eeprom_reset_fifo()

	## // CMD_CRRD__06 
	print('\n>>> CMD_CRRD__06')
	eeprom_send_frame (CMD=0x06, num_bytes_DAT=num_bytes_DAT)

	## call fifo
	ret = eeprom_read_fifo(num_data=num_bytes_DAT)

	#
	return ret


def eeprom_write_data_16B(ADRS_16b=0x0000, num_bytes_DAT=16, data8b_in=[0]*16) :
	print('\n>>>>>> eeprom_write_data_16B')
	
	## call fifo 
	#eeprom_write_fifo(datain__int_list=data8b_in)
	
	## write enble
	eeprom_write_enable()
	
	## convert address
	ADL = (ADRS_16b>>0)&0x00FF 
	ADH = (ADRS_16b>>8)&0x00FF
	print('{} = 0x{:08X}'.format('ADRS_16b', ADRS_16b))
	print('{} = 0x{:04X}'.format('ADH', ADH))
	print('{} = 0x{:04X}'.format('ADL', ADL))
	
	## // CMD_WRITE_6C 
	print('\n>>> CMD_WRITE_6C')
	eeprom_send_frame (CMD=0x6C, ADL=ADL, ADH=ADH, num_bytes_DAT=num_bytes_DAT, con_disable_SBP=1)
	pass


def eeprom_write_data(ADRS_16b=0x0000, num_bytes_DAT=1, data8b_in=[0]):
	print('\n>>>>>> eeprom_write_data')

	##  The 11XX features a 16-byte page buffer, meaning that
	##  up to 16 bytes can be written at one time. To utilize this
	##  feature, the master can transmit up to 16 data bytes to
	##  the 11XX, which are temporarily stored in the page buffer.
	##  After each data byte, the master sends a MAK, indicating
	##  whether or not another data byte is to follow. A
	##  NoMAK indicates that no more data is to follow, and as
	##  such will initiate the internal write cycle.
	
	## reset fifo test 
	eeprom_reset_fifo()

	if num_bytes_DAT <= 16:
		eeprom_write_fifo(datain__int_list=data8b_in)
		eeprom_write_data_16B(ADRS_16b=ADRS_16b, num_bytes_DAT=num_bytes_DAT, data8b_in=data8b_in)
	else:
		## call fifo 
		fifo_size = 256;
		#eeprom_write_fifo(datain__int_list=data8b_in)
		for ii in range(0,num_bytes_DAT,fifo_size):
			eeprom_write_fifo(datain__int_list=data8b_in[(ii):(ii+fifo_size)])
			
		##  16-byte page buffer operation support
		for ii in range(0,num_bytes_DAT,16):
			eeprom_write_data_16B(ADRS_16b=ADRS_16b+ii, data8b_in=data8b_in[(ii):(ii+16)])
	pass


def eeprom_txt_display(mem_data__list, offset=0x0000):
	# display : every 16 bytes
	#print(mem_data_2KB__list)
	# 014  0x00E0  FF FF FF FF FF FF FF FF  FF FF FF FF FF FF FF FF  ................
	# 015  0x00F0  FF FF FF FF FF FF FF FF  FF FF FF FF FF FF FF FF  ................
	# 016  0x0100  FF FF FF FF FF FF FF FF  FF FF FF FF FF FF FF FF  ................
	# 017  0x0110  FF FF FF FF FF FF FF FF  FF FF FF FF FF FF FF FF  ................
	# 018  0x0120  FF FF FF FE DC BA FF FF  FF FF FF FF FF FF FF FF  ................	
	
	mem_data_2KB__list = mem_data__list
	adrs_ofs = offset
	
	num_bytes_in_MEM = len(mem_data_2KB__list)
	num_bytes_in_a_display_line = 16
	
	output_display = ''
	for ii in range(0,int(num_bytes_in_MEM/num_bytes_in_a_display_line)):
		xx              = mem_data_2KB__list[(ii*16):(ii*16+16)]                                 # load line data
		output_display += '{:03d}  '  .format(ii)                                                # line number 
		output_display += '0x{:04X}  '.format(ii*16+adrs_ofs)                                             # start address each line
		output_display += ''.join([ '{:02X} '.format(jj) for jj in xx[0:8 ] ])                   # hex code 
		output_display += ' '
		output_display += ''.join([ '{:02X} '.format(jj) for jj in xx[8:16] ])                   # hex code 
		output_display += ' '
		output_display += ''.join([ chr(jj) if (jj>= 0x20 and jj<=0x7E) else '.' for jj in xx ]) # printable code
		output_display += '\n'                                                                   # line feed
	#
	return output_display


### start test ###

### finish test ###

## wait for a while
sleep(0.5)



## OK close
dev.Close()


##



