/*
 * Empty C++ Application
 */

#include "xil_printf.h" // print() for pure string; xil_printf() for formatted string // not for floating

using namespace std;

// define class
class Box {
   public:
      double length;         // Length of a box
      double breadth;        // Breadth of a box
      double height;         // Height of a box

      // Member functions declaration
      double getVolume(void);
      void setLength( double len );
      void setBreadth( double bre );
      void setHeight( double hei );
};

// Member functions definitions
double Box::getVolume(void) {
   return length * breadth * height;
}
void Box::setLength( double len ) {
   length = len;
}
void Box::setBreadth( double bre ) {
   breadth = bre;
}
void Box::setHeight( double hei ) {
   height = hei;
}



int main()
{

	xil_printf("> Go MHVSU BASE!! C++ \r\n");


	Box Box1;
	double volume = 0.0;     // Store the volume of a box here

	// box 1 specification
	Box1.setLength(6.0);
	Box1.setBreadth(7.0);
	Box1.setHeight(5.0);

	// volume of box 1
	volume = Box1.getVolume();

	xil_printf("volume = %d \r\n",int(volume));


	return 0;
}


// Simple C++ program to display "Hello World"

// Header file for input output functions
//#include<iostream>

//using namespace std;

// main function -
// where the execution of program begins
//int main()
//{
    // prints hello world
//    cout<<"Hello World";

    //return 0;
//}
