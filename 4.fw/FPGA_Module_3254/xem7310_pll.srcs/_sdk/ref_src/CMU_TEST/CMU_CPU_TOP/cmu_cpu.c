//#include "xparameters.h"
//#include "xil_cache.h"
#include "microblaze_sleep.h"

#include "cmu_cpu.h"

#include "ioLibrary/Ethernet/W5500/w5500.h" // for w5500 io functions
//
#define __W5500_SPI_VDM_OP_          0x00
#define __W5500_SPI_FDM_OP_LEN1_     0x01
#define __W5500_SPI_FDM_OP_LEN2_     0x02
#define __W5500_SPI_FDM_OP_LEN4_     0x03


// common subfunctions
u32 hexchr2data_u32(u8 hexchr) {
	// '0' -->  0
	// 'A' --> 10
	u32 val;
	s32 val_L;
	s32 val_H;
	//
	val_L = (s32)hexchr - (s32)'0';
	//if ((val_L>=0)&&(val_L<10)) {
	if (val_L<10) {
		val = (u32)val_L;
	}
	else {
		val_H = (s32)hexchr - (s32)'A';
		//if ((val_H>=0)&&(val_H<6)) {
		//	val = (u32)val_H+10;
		// }
		//else {
		//	val = (u32)(-1); // no hex code.
		// }
		val = (u32)val_H+10;
	}
	//
	return val; 
}

u32 hexstr2data_u32(u8* hexstr, u32 len) {
	u32 val;
	u32 loc;
	u32 ii;
	loc = 0;
	val = 0;
	for (ii=0;ii<len;ii++) {
		val = (val<<4) + hexchr2data_u32(hexstr[loc++]);
	}
	return val;
}

u32 decchr2data_u32(u8 decchr) {
	// '0' -->  0
	u32 val;
	s32 val_t;
	//
	val_t = (s32)decchr - (s32)'0';
	if (val_t<10) {
		val = (u32)val_t;
	}
	else {
		val = (u32)(-1); // no valid code.
	}
	//
	return val; 
}

u32 decstr2data_u32(u8* decstr, u32 len) {
	u32 val;
	u32 loc;
	u32 ii;
	loc = 0;
	val = 0;
	for (ii=0;ii<len;ii++) {
		val = (val*10) + decchr2data_u32(decstr[loc++]);
	}
	return val;
}

void dcopy_pipe8_to_buf8  (u32 adrs_p8, u8 *p_buf_u8, u32 len) {
	u32 ii;
	u8 val;
	//
	for (ii=0;ii<len;ii++) {
		val = (u8)XIomodule_In32 (adrs_p8);
		p_buf_u8[ii] = val;
	}
}
	
void dcopy_buf8_to_pipe8  (u8 *p_buf_u8, u32 adrs_p8, u32 len) {
	u32 ii;
	u32 val;
	//
	for (ii=0;ii<len;ii++) {
		val = p_buf_u8[ii]&0x000000FF;
		XIomodule_Out32 (adrs_p8, val);
	}
}

void dcopy_pipe32_to_buf32(u32 adrs_p32, u32 *p_buf_u32, u32 len_byte) { // (src,dst,len)
	u32 ii;
	u32 len;
	//
	len = len_byte>>2;
	for (ii=0;ii<len;ii++) {
		p_buf_u32[ii] = XIomodule_In32 (adrs_p32);
	}
}

void dcopy_buf32_to_pipe32(u32 *p_buf_u32, u32 adrs_p32, u32 len_byte) { // (src,dst,len)
	u32 ii;
	u32 val;
	u32 len;
	//
	len = len_byte>>2;
	for (ii=0;ii<len;ii++) {
		val = p_buf_u32[ii];
		XIomodule_Out32 (adrs_p32, val);
	}
}

void dcopy_buf8_to_pipe32(u8 *p_buf_u8, u32 adrs_p32, u32 len_byte) { // (src,dst,len_byte)
	u32 ii;
	u32 val;
	//u32 *p_val;
	u8  *p_val_u8;
	u32 len;
	//
	//p_val = (u32*)p_buf_u8; //$$ NG ...  MCS IO mem on buf8 -> buf32 location has some constraint.
	p_val_u8 = (u8*)&val;
	//
	len = len_byte>>2;
	for (ii=0;ii<len;ii++) {
		p_val_u8[0] = p_buf_u8[ii*4+0];
		p_val_u8[1] = p_buf_u8[ii*4+1];
		p_val_u8[2] = p_buf_u8[ii*4+2];
		p_val_u8[3] = p_buf_u8[ii*4+3];
		//
		//xil_printf("check: 0x%08X\r\n",(unsigned int)val); //$$ test
		//
		XIomodule_Out32 (adrs_p32, val);
	}
}

void dcopy_pipe32_to_pipe32(u32 src_adrs_p32, u32 dst_adrs_p32, u32 len_byte) { // (src,dst,len)
	u32 ii;
	u32 len;
	//
	len = len_byte>>2;
	for (ii=0;ii<len;ii++) {
		XIomodule_Out32 (dst_adrs_p32, XIomodule_In32 (src_adrs_p32));
	}
}

void dcopy_pipe8_to_pipe32 (u32 src_adrs_p8,  u32 dst_adrs_p32, u32 len_byte) { // (src,dst,len)
	// src_adrs_p8          dst_adrs_p32
	// {AA},{BB},{CC},{DD}  {DD,CC,BB,AA}   
	u32 ii;
	u32 len;
	u32 val;
	//
	len = len_byte>>2;
	for (ii=0;ii<len;ii++) {
		val = 0   +  ((XIomodule_In32 (src_adrs_p8))&0x000000FF)     ;
		val = val + (((XIomodule_In32 (src_adrs_p8))&0x000000FF)<< 8);
		val = val + (((XIomodule_In32 (src_adrs_p8))&0x000000FF)<<16);
		val = val + (((XIomodule_In32 (src_adrs_p8))&0x000000FF)<<24);
		XIomodule_Out32 (dst_adrs_p32, val);
	}
}

void dcopy_pipe32_to_pipe8 (u32 src_adrs_p32, u32 dst_adrs_p8,  u32 len_byte) { // (src,dst,len)
	// src_adrs_p32    dst_adrs_p8
	// {DD,CC,BB,AA}   {AA},{BB},{CC},{DD}
	u32 ii;
	u32 len;
	u32 val;
	//
	len = len_byte>>2;
	for (ii=0;ii<len;ii++) {
		val = XIomodule_In32 (src_adrs_p32);
		XIomodule_Out32 (dst_adrs_p8, (val&0x000000FF));
		val = val >> 8;
		XIomodule_Out32 (dst_adrs_p8, (val&0x000000FF));
		val = val >> 8;
		XIomodule_Out32 (dst_adrs_p8, (val&0x000000FF));
		val = val >> 8;
		XIomodule_Out32 (dst_adrs_p8, (val&0x000000FF));
	}
}



// wiznet 850io functions 

u8 hw_reset__wz850() {
	u32 adrs;
	u32 value;
	
	//// trig LAN RESET @  master_spi_wz850
	//xil_printf(">>> trig LAN RESET @  master_spi_wz850: \n\r");
	//
	adrs = ADRS_PORT_WO_20; // {...,w_LAN_INTn,w_LAN_SSNn,w_LAN_RSTn,w_LAN_valid}
	value = XIomodule_In32 (adrs);
	//xil_printf(">done: rd: 0x%08X @ 0x%08X \n\r", value, adrs);
	//
	adrs = ADRS_PORT_WI_00;
	value = 0x00000001;
	XIomodule_Out32 (adrs, value);
	//xil_printf(">done: wr: 0x%08X @ 0x%08X \n\r", value, adrs);
	//
	adrs = ADRS_PORT_WO_20; // {...,w_LAN_INTn,w_LAN_SSNn,w_LAN_RSTn,w_LAN_valid}
	value = XIomodule_In32 (adrs);
	//xil_printf(">done: rd: 0x%08X @ 0x%08X \n\r", value, adrs);
	//
	adrs = ADRS_PORT_WI_00;
	value = 0x00000000;
	XIomodule_Out32 (adrs, value);
	//xil_printf(">done: wr: 0x%08X @ 0x%08X \n\r", value, adrs);
	//
		while (1) {
		//
		adrs = ADRS_PORT_WO_20; // {...,w_LAN_INTn,w_LAN_SSNn,w_LAN_RSTn,w_LAN_valid}
		value = XIomodule_In32 (adrs);
		//xil_printf(">done: rd: 0x%08X @ 0x%08X \n\r", value, adrs);
		//
		if ((value & 0x03) == 0x03) break;
		//
		//usleep(100); // 1us*100 sleep
		//
		}
	//
	return 0;
}

// for  uint8_t  WIZCHIP_READ(uint32_t AddrSel)
u8    read_data__wz850 (u32 AddrSel) {
	u8 ret;
	u32 adrs;
	u32 value;

	// set up mode
	AddrSel |= (_W5500_SPI_READ_ | __W5500_SPI_VDM_OP_);

	//// set up frame
	adrs = ADRS_PORT_WI_01;
	value = AddrSel&0x00FFFFFF;
	XIomodule_Out32 (adrs, value);
	//
	adrs = ADRS_PORT_WI_02;
	value = 1;
	XIomodule_Out32 (adrs, value);
	//
	adrs = ADRS_PORT_WI_00;
	value = 0x0002; // ... , trig_frame, trig_reset
	XIomodule_Out32 (adrs, value);
	//
	adrs = ADRS_PORT_WI_00;
	value = 0x0000; // ... , trig_frame, trig_reset
	XIomodule_Out32 (adrs, value);
	//
		while (1) {
		//
		adrs = ADRS_PORT_WO_20; // {...,w_done_SPI_frame,w_LAN_INTn,w_LAN_SCSn,w_LAN_RSTn,w_done_LAN_reset}
		value = XIomodule_In32 (adrs);
		//
		if ((value & 0x10) == 0x10) break;
		//
		//usleep(100); // 1us*100 sleep
		//
		}
	//

	//// read fifo data
	adrs = ADRS_PORT_PO_A0;
	value = XIomodule_In32 (adrs);
	//
	ret = value & (0x000000ff);
	return ret;
}

// for  void WIZCHIP_WRITE(uint32_t AddrSel, uint8_t wb )
void write_data__wz850 (u32 AddrSel, u8 wb) {
	u32 adrs;
	u32 value;

	// set up mode
	AddrSel |= (_W5500_SPI_WRITE_ | __W5500_SPI_VDM_OP_);

	// write fifo data
	adrs = ADRS_PORT_PI_80;
	value = 0x000000FF & wb;
	XIomodule_Out32 (adrs, value);

	//// set up frame
	adrs = ADRS_PORT_WI_01;
	value = AddrSel&0x00FFFFFF;
	XIomodule_Out32 (adrs, value);
	//
	adrs = ADRS_PORT_WI_02;
	value = 1;
	XIomodule_Out32 (adrs, value);
	//
	adrs = ADRS_PORT_WI_00;
	value = 0x0002; // ... , trig_frame, trig_reset
	XIomodule_Out32 (adrs, value);
	//
	adrs = ADRS_PORT_WI_00;
	value = 0x0000; // ... , trig_frame, trig_reset
	XIomodule_Out32 (adrs, value);
	//
		while (1) {
		//
		adrs = ADRS_PORT_WO_20; // {...,w_done_SPI_frame,w_LAN_INTn,w_LAN_SCSn,w_LAN_RSTn,w_done_LAN_reset}
		value = XIomodule_In32 (adrs);
		//
		if ((value & 0x10) == 0x10) break;
		//
		//usleep(100); // 1us*100 sleep
		//
		}
	//
}

// for  void WIZCHIP_READ_BUF (uint32_t AddrSel, uint8_t* pBuf, uint16_t len)
// read data from LAN-fifo to buffer  // recv
void  read_data_buf__wz850 (u32 AddrSel, u8* pBuf, u16 len) {
	u32 adrs;
	u32 value;

	// set up mode
	AddrSel |= (_W5500_SPI_READ_ | __W5500_SPI_VDM_OP_);

	//// set up frame
	adrs = ADRS_PORT_WI_01;
	value = AddrSel&0x00FFFFFF;
	XIomodule_Out32 (adrs, value);
	//
	adrs = ADRS_PORT_WI_02;
	value = len;
	XIomodule_Out32 (adrs, value);
	//
	adrs = ADRS_PORT_WI_00;
	value = 0x0002; // ... , trig_frame, trig_reset
	XIomodule_Out32 (adrs, value);
	//
	adrs = ADRS_PORT_WI_00;
	value = 0x0000; // ... , trig_frame, trig_reset
	XIomodule_Out32 (adrs, value);
	//
	while (1) {
		//
		adrs = ADRS_PORT_WO_20; // {...,w_done_SPI_frame,w_LAN_INTn,w_LAN_SCSn,w_LAN_RSTn,w_done_LAN_reset}
		value = XIomodule_In32 (adrs);
		//
		if ((value & 0x10) == 0x10) break;
		//
		//usleep(100); // 1us*100 sleep
		//
		}
	//

	//// read fifo data : dcopy pipe_8b to buf_8b
	dcopy_pipe8_to_buf8  (ADRS_PORT_PO_A0, pBuf, len);
	//
}

// for  void WIZCHIP_WRITE_BUF(uint32_t AddrSel, uint8_t* pBuf, uint16_t len)
// write data from buffer to LAN-fifo // send
void write_data_buf__wz850 (u32 AddrSel, u8* pBuf, u16 len) {
	u32 adrs;
	u32 value;
	
	// set up mode
	AddrSel |= (_W5500_SPI_WRITE_ | __W5500_SPI_VDM_OP_);

	// write buffer data to LAN-fifo : dcopy buf_8b to pipe_8b
	dcopy_buf8_to_pipe8(pBuf, ADRS_PORT_PI_80, len); 
	
	//// set up frame
	adrs = ADRS_PORT_WI_01;
	value = AddrSel&0x00FFFFFF;
	XIomodule_Out32 (adrs, value);
	//
	adrs = ADRS_PORT_WI_02;
	value = len;
	XIomodule_Out32 (adrs, value);
	//
	adrs = ADRS_PORT_WI_00;
	value = 0x0002; // ... , trig_frame, trig_reset
	XIomodule_Out32 (adrs, value);
	//
	adrs = ADRS_PORT_WI_00;
	value = 0x0000; // ... , trig_frame, trig_reset
	XIomodule_Out32 (adrs, value);
	//
	while (1) {
		//
		adrs = ADRS_PORT_WO_20; // {...,w_done_SPI_frame,w_LAN_INTn,w_LAN_SCSn,w_LAN_RSTn,w_done_LAN_reset}
		value = XIomodule_In32 (adrs);
		//
		if ((value & 0x10) == 0x10) break;
		//
		//usleep(100); // 1us*100 sleep
		//
		}
	//	
}

// FROM w5500.c : WIZCHIP_READ_BUF
// FROM w5500.c : void wiz_recv_data(uint8_t sn, uint8_t *wizdata, uint16_t len)
// FROM socket.c: int32_t recv(uint8_t sn, uint8_t * buf, uint16_t len)
// FROM socket.c: int32_t recvfrom(uint8_t sn, uint8_t * buf, uint16_t len, uint8_t * addr, uint16_t *port)
// ...
// TO    WIZCHIP_READ_PIPE // test 
// TO    void wiz_recv_data_pipe(uint8_t sn, uint8_t *wizdata, uint32_t len_u32)
// TO    int32_t recv_pipe(uint8_t sn, uint8_t * buf, uint32_t len_u32)
// TO    int32_t recvfrom_pipe(uint8_t sn, uint8_t * buf, uint32_t len_u32, uint8_t * addr, uint16_t *port)
//
// read data from LAN-fifo to pipe // recv
void  read_data_pipe__wz850 (u32 AddrSel, u32 ep_offset, u32 len_u32) { // test
	u32 adrs;
	u32 value;
	u32 ii;
	//
	u32 len_4byte;
	u32 data_u32;
	u8  *p_data_u8;
	
	// set up mode
	AddrSel |= (_W5500_SPI_READ_ | __W5500_SPI_VDM_OP_);

 	// length based on 4 bytes 
	len_4byte = len_u32>>2; // len_u32 must be muliple of 4.

	//// set up frame
	adrs = ADRS_PORT_WI_01;
	value = AddrSel&0x00FFFFFF;
	XIomodule_Out32 (adrs, value);
	//
	adrs = ADRS_PORT_WI_02;
	value = len_u32; //$$
	XIomodule_Out32 (adrs, value);
	//
	adrs = ADRS_PORT_WI_00;
	value = 0x0002; // ... , trig_frame, trig_reset
	XIomodule_Out32 (adrs, value);
	//
	adrs = ADRS_PORT_WI_00;
	value = 0x0000; // ... , trig_frame, trig_reset
	XIomodule_Out32 (adrs, value);
	//
		while (1) {
		//
		adrs = ADRS_PORT_WO_20; // {...,w_done_SPI_frame,w_LAN_INTn,w_LAN_SCSn,w_LAN_RSTn,w_done_LAN_reset}
		value = XIomodule_In32 (adrs);
		//
		if ((value & 0x10) == 0x10) break;
		//
		//usleep(100); // 1us*100 sleep
		//
		}
	//

	//// read fifo data from LAN to pipe 
	//
	// address of pipe end-point
	adrs = ADRS_BASE_CMU + (ep_offset<<4); // MCS1 
	//
	p_data_u8 = (u8*)&data_u32;
	for (ii=0;ii<len_4byte;ii++) {
		value = XIomodule_In32 (ADRS_PORT_PO_A0); // read data0 from LAN-fifo  
		p_data_u8[0] = (u8) (value & 0x00FF);
		//
		value = XIomodule_In32 (ADRS_PORT_PO_A0); // read data1 from LAN-fifo  
		p_data_u8[1] = (u8) (value & 0x00FF);
		//
		value = XIomodule_In32 (ADRS_PORT_PO_A0); // read data2 from LAN-fifo  
		p_data_u8[2] = (u8) (value & 0x00FF);
		//
		value = XIomodule_In32 (ADRS_PORT_PO_A0); // read data3 from LAN-fifo  
		p_data_u8[3] = (u8) (value & 0x00FF);
		//
		XIomodule_Out32 (adrs, data_u32);// write a 32-bit value to pipe end-point
	}
	//
}

// FROM w5500.c : WIZCHIP_WRITE_BUF
// FROM w5500.c : void wiz_send_data(uint8_t sn, uint8_t *wizdata, uint16_t len)
// FROM socket.c: int32_t send(uint8_t sn, uint8_t * buf, uint16_t len)
// FROM socket.c: int32_t sendto(uint8_t sn, uint8_t * buf, uint16_t len, uint8_t * addr, uint16_t port)
// ...
// TO    WIZCHIP_WRITE_PIPE // test  
// TO    void wiz_send_data_pipe(uint8_t sn, uint8_t *wizdata, uint32_t len_u32)
// TO    int32_t send_pipe(uint8_t sn, uint8_t * buf, uint32_t len_u32)
// TO    int32_t sendto_pipe(uint8_t sn, uint8_t * buf, uint32_t len_u32, uint8_t * addr, uint16_t port)
//
// write data from pipe to LAN-fifo // send
void write_data_pipe__wz850 (u32 AddrSel, u32 src_adrs_p32, u32 len_u32) { 
	u32 adrs;
	u32 value;
	u32 ii;
	//
	u32 len_4byte;
	u32 data_u32;
	u8  *p_data_u8;
	
	// set up LAN spi mode
	AddrSel |= (_W5500_SPI_WRITE_ | __W5500_SPI_VDM_OP_);
	
	// address of pipe end-point
	//adrs = ADRS_BASE_CMU + (ep_offset<<4); // MCS1 

 	// length based on 4 bytes 
	len_4byte = len_u32>>2; // len_u32 must be muliple of 4.
	
	// note that 32 bits from pipe-out and 8 bits into LAN-fifo 
	// need to send 4 bytes byte-by-byte ...
	p_data_u8 = (u8*)&data_u32;
	for (ii=0;ii<len_4byte;ii++) {
		//// read from data pipe such as adc pipe
		data_u32 = XIomodule_In32 (src_adrs_p32); // read a 32-bit value from pipe end-point
		//...
		//// write the value to LAN pipe end-point
		// send data in the order of ... p_data_u8[0], p_data_u8[1], p_data_u8[2], p_data_u8[3]
		value = 0x000000FF & p_data_u8[0];
		XIomodule_Out32 (ADRS_PORT_PI_80, value); // write data0 to LAN-fifo  
		//
		value = 0x000000FF & p_data_u8[1];
		XIomodule_Out32 (ADRS_PORT_PI_80, value); // write data1 to LAN-fifo  
		//
		value = 0x000000FF & p_data_u8[2];
		XIomodule_Out32 (ADRS_PORT_PI_80, value); // write data2 to LAN-fifo  
		//
		value = 0x000000FF & p_data_u8[3];
		XIomodule_Out32 (ADRS_PORT_PI_80, value); // write data3 to LAN-fifo  
		
	}
	
	//// set up frame
	adrs = ADRS_PORT_WI_01;
	value = AddrSel&0x00FFFFFF;
	XIomodule_Out32 (adrs, value);
	//
	adrs = ADRS_PORT_WI_02;
	value = len_u32; //$$ 
	XIomodule_Out32 (adrs, value);
	//
	adrs = ADRS_PORT_WI_00;
	value = 0x0002; // ... , trig_frame, trig_reset
	XIomodule_Out32 (adrs, value);
	//
	adrs = ADRS_PORT_WI_00;
	value = 0x0000; // ... , trig_frame, trig_reset
	XIomodule_Out32 (adrs, value);
	//
		while (1) {
		//
		adrs = ADRS_PORT_WO_20; // {...,w_done_SPI_frame,w_LAN_INTn,w_LAN_SCSn,w_LAN_RSTn,w_done_LAN_reset}
		value = XIomodule_In32 (adrs);
		//
		if ((value & 0x10) == 0x10) break;
		//
		//usleep(100); // 1us*100 sleep
		//
		}
	// 	
}


// == MCS io access functions ==
u32   read_mcs_fpga_img_id() {
	//u32 adrs;
	//u32 value;
	//adrs = ADRS_FPGA_IMAGE_CMU;
	//value = XIomodule_In32 (adrs);
	//return value;
	return XIomodule_In32 (ADRS_FPGA_IMAGE_CMU);
}

u32   read_mcs_test_reg() {
	return XIomodule_In32 (ADRS_TEST_REG___CMU);
}

void write_mcs_test_reg(u32 data) {
	XIomodule_Out32 (ADRS_TEST_REG___CMU, data);
}


// === MCS-EP access functions ===

// * common subfunctions :
// note that mask should be careful... MCS and LAN as well...

void  enable_mcs_ep() {
	//adrs = ADRS_PORT_WI_10;
	//value = 0x0000003F;
	//XIomodule_Out32 (adrs, value);
	XIomodule_Out32 (ADRS_MASK_WI,    0x0000003F); // write mask
	XIomodule_Out32 (ADRS_PORT_WI_10, 0x0000003F); // write MCS0
	XIomodule_Out32 (ADRS_MASK_WI,      MASK_ALL); // reset mask
}

void disable_mcs_ep() {
	//adrs = ADRS_PORT_WI_10;
	//value = 0x00000000;
	//XIomodule_Out32 (adrs, value);
	XIomodule_Out32 (ADRS_MASK_WI,    0x0000003F); // write mask
	XIomodule_Out32 (ADRS_PORT_WI_10, 0x00000000); // write MCS0
	XIomodule_Out32 (ADRS_MASK_WI,      MASK_ALL); // reset mask
}

void  reset_mcs_ep() {
	XIomodule_Out32 (ADRS_MASK_WI,    0x00010000); // write mask
	XIomodule_Out32 (ADRS_PORT_WI_10, 0x00010000); // write MCS0
	usleep(100);
	XIomodule_Out32 (ADRS_PORT_WI_10, 0x00000000); // write MCS0
	XIomodule_Out32 (ADRS_MASK_WI,      MASK_ALL); // reset mask
}

void  reset_io_dev() {
	// adc / dwave / bias / spo
	XIomodule_Out32 (ADRS_MASK_WI,    0x001E0000); // write mask
	XIomodule_Out32 (ADRS_PORT_WI_10, 0x001E0000); // write MCS0
	usleep(100);
	XIomodule_Out32 (ADRS_PORT_WI_10, 0x00000000); // write MCS0
	XIomodule_Out32 (ADRS_MASK_WI,      MASK_ALL); // reset mask
}


u32 is_enabled_mcs_ep() {
	u32 val;
	u32 ret;
	val = XIomodule_In32 (ADRS_PORT_WI_10); // assume MASK_ALL
	if ((val&0x003F)==0x003F) {
		ret = 1;
	}
	else ret = 0;
	return ret;
}


// * io-mask subfunctions :

u32  read_mcs_ep_wi_mask() {
	return XIomodule_In32 (ADRS_MASK_WI____CMU);
}

u32 write_mcs_ep_wi_mask(u32 mask) {
	XIomodule_Out32 (ADRS_MASK_WI____CMU, mask); // mask 
	return mask;
}

u32  read_mcs_ep_wo_mask() {
	return XIomodule_In32 (ADRS_MASK_WO____CMU);
}

u32 write_mcs_ep_wo_mask(u32 mask) {
	XIomodule_Out32 (ADRS_MASK_WO____CMU, mask); // mask 
	return mask;
}

u32  read_mcs_ep_ti_mask() {
	return XIomodule_In32 (ADRS_MASK_TI____CMU);
}

u32 write_mcs_ep_ti_mask(u32 mask) {
	XIomodule_Out32 (ADRS_MASK_TI____CMU, mask); // mask 
	return mask;
}

u32  read_mcs_ep_to_mask() {
	return XIomodule_In32 (ADRS_MASK_TO____CMU);
}

u32 write_mcs_ep_to_mask(u32 mask) {
	XIomodule_Out32 (ADRS_MASK_TO____CMU, mask); // mask 
	return mask;
}


u32  read_mcs_ep_xx_data(u32 offset) {
	u32 adrs;
	u32 value;
	adrs = ADRS_BASE_CMU + (offset<<4); // MCS1
	value = XIomodule_In32 (adrs);
	return value;	
}

u32 write_mcs_ep_xx_data(u32 offset, u32 data) {
	u32 adrs;
	adrs = ADRS_BASE_CMU + (offset<<4); // MCS1
	XIomodule_Out32 (adrs, data);
	return data;
}


u32  read_mcs_ep_wi_data(u32 offset) {
	return read_mcs_ep_xx_data(offset);
}

u32 write_mcs_ep_wi_data(u32 offset, u32 data) {
	return write_mcs_ep_xx_data(offset, data);
}

u32  read_mcs_ep_wo_data(u32 offset) {
	return read_mcs_ep_xx_data(offset);
}

// u32 write_mcs_ep_wo_data(u32 offset, u32 data) // NA

u32  read_mcs_ep_ti_data(u32 offset) {
	return read_mcs_ep_xx_data(offset);
}

u32 write_mcs_ep_ti_data(u32 offset, u32 data) {
	return write_mcs_ep_xx_data(offset, data);
}

u32  read_mcs_ep_to_data(u32 offset) {
	return read_mcs_ep_xx_data(offset);
}

// u32 write_mcs_ep_to_data(u32 offset, u32 data) // NA


//// PIPE-DATA direct

// u32  read_mcs_ep_pi_data() // NA

u32 write_mcs_ep_pi_data(u32 offset, u32 data) { // OK
	return write_mcs_ep_xx_data(offset, data);
}

u32  read_mcs_ep_po_data(u32 offset) { // OK
	return read_mcs_ep_xx_data(offset);
}

// u32 write_mcs_ep_po_data() // NA



// * end-point functions 

u32   read_mcs_ep_wi(u32 offset) {
	return read_mcs_ep_wi_data(offset);
}

void write_mcs_ep_wi(u32 offset, u32 data, u32 mask) {
	write_mcs_ep_wi_mask(mask);
	write_mcs_ep_wi_data(offset, data);
}

u32   read_mcs_ep_wo(u32 offset, u32 mask) {
	write_mcs_ep_wo_mask(mask);
	return read_mcs_ep_wo_data(offset);
}

u32   read_mcs_ep_ti(u32 offset) {
	return read_mcs_ep_ti_data(offset);
}

void write_mcs_ep_ti(u32 offset, u32 data, u32 mask) {
	write_mcs_ep_ti_mask(mask);
	write_mcs_ep_ti_data(offset, data);
}

void activate_mcs_ep_ti(u32 offset, u32 bit_loc) { 
	u32 value;
	value = 0x00000001 << bit_loc;
	write_mcs_ep_ti(offset, value, value);
}

u32   read_mcs_ep_to(u32 offset, u32 mask) {
	write_mcs_ep_to_mask(mask);
	return read_mcs_ep_to_data(offset);
}

u32 is_triggered_mcs_ep_to(u32 offset, u32 mask) {
	u32 ret;
	u32 value;
	//
	value = read_mcs_ep_to(offset, mask);
	if (value==0) ret = 0;
	else ret = 1;
	return ret;
}


//// PIPE-DATA in buffer 

// write data from buffer to pipe-in 
u32 write_mcs_ep_pi_buf(u32 offset, u32 len_byte, u8 *p_data) {
	u32 adrs;
	//
	adrs = ADRS_BASE_CMU + (offset<<4); // MCS1
	dcopy_buf32_to_pipe32((u32*)p_data, adrs, len_byte);
	//
	return (len_byte&0xFFFFFFFC);
}

// read data from pipe-out to buffer 
u32  read_mcs_ep_po_buf(u32 offset, u32 len_byte, u8 *p_data) { 
	u32 adrs;
	//
	adrs = ADRS_BASE_CMU + (offset<<4); // MCS1
	dcopy_pipe32_to_buf32(adrs, (u32*)p_data, len_byte);
	//
	return (len_byte&0xFFFFFFFC);
}


//// PIPE-DATA in fifo 

// write data from fifo to pipe-in 
//u32 write_mcs_ep_pi_fifo(u32 offset, u32 len_byte, u8 *p_data) { // test
//	return 0;
//}

// read data from pipe-out to fifo 
//u32  read_mcs_ep_po_fifo(u32 offset, u32 len_byte, u8 *p_data) { // test
//	return 0;
//}


//
//  GetWireOutValue
//  UpdateWireOuts 
//  SetWireInValue
//  UpdateWireIns // dummy
//  ActivateTriggerIn // ActivateTriggerIn(int epAddr, int bit)
//  UpdateTriggerOuts // not much // dummy
//  IsTriggered // not much // IsTriggered(int epAddr, UINT32 mask)
//  ReadFromPipeOut
//  WriteToPipeIn // not much
//  // https://library.opalkelly.com/library/FrontPanelAPI/classokCFrontPanel-members.html


//// CMU-CPU functions

// common

u32 cmu_read_fpga_image_id() {
	u32 fpga_image_id;
	//dev = cmu_ctrl.dev
	//EP_ADRS = conf.OK_EP_ADRS_CONFIG
	//#
	//print('\n>> {}'.format('Read FPGA image ID'))
#ifdef _MCS_DEBUG_
	xil_printf("\r\n>> %s", "Read FPGA image ID");
#endif
	//#
	//dev.UpdateWireOuts()
	//fpga_image_id = dev.GetWireOutValue(EP_ADRS['FPGA_IMAGE_ID'])
	fpga_image_id = read_mcs_ep_wo(EP_ADRS__FPGA_IMAGE_ID, MASK_ALL);
	//#
	//print('{} = {:#10x}'.format('fpga_image_id',fpga_image_id))
#ifdef _MCS_DEBUG_
	xil_printf("\r\n> %s = 0x%08X", "fpga_image_id", fpga_image_id);
	//#
	//fpga_image_id_expected = EP_ADRS['ver']
	//print('{} = {}'.format('fpga_image_id_expected',fpga_image_id_expected))
	xil_printf("\r\n> %s = 0x%08X", "EP_ADRS__ver", EP_ADRS__ver);
	//#
	//if form_hex_32b(fpga_image_id) == fpga_image_id_expected:
	if (fpga_image_id == EP_ADRS__ver) 
		xil_printf("\r\n> %s", "fpga_image_id is same as expected!");
	//else:
	//	print('> fpga_image_id is NOT same as expected!')
	else 
		xil_printf("\r\n> %s", "fpga_image_id is NOT same as expected!");
#endif
	//#
	return fpga_image_id;	
}

u32 cmu_monitor_fpga() {
	u32 mon_fpga_temp_mC;
	//dev = cmu_ctrl.dev
	//EP_ADRS = conf.OK_EP_ADRS_CONFIG
	//#
	//print('\n>> {}'.format('Monitor FPGA'))
#ifdef _MCS_DEBUG_
	xil_printf("\r\n>> %s", "Monitor FPGA");
#endif
	//#
	//dev.UpdateWireOuts()
	//mon_fpga_temp_mC = dev.GetWireOutValue(EP_ADRS['XADC_TEMP'])
	mon_fpga_temp_mC =  read_mcs_ep_wo(EP_ADRS__XADC_TEMP, MASK_ALL);
	//mon_fpga_volt_mV = dev.GetWireOutValue(EP_ADRS['XADC_VOLT'])
	//#
	//print('{}: {}\r'.format('FPGA temp[C]',mon_fpga_temp_mC/1000))
#ifdef _MCS_DEBUG_
	xil_printf("\r\n> %s : %d", "FPGA temp[C]", mon_fpga_temp_mC/1000);
#endif
	//print('{}: {}\r'.format('FPGA volt[V]',mon_fpga_volt_mV/1000))
	//#
	//return [mon_fpga_temp_mC, mon_fpga_volt_mV]
	return mon_fpga_temp_mC;
}

u32 cmu_test_counter(u32 opt) {
	//dev = cmu_ctrl.dev
	//EP_ADRS = conf.OK_EP_ADRS_CONFIG
	//#
	//if not opt:
	//	opt = 'OFF'
	//#
	//print('\n>> {}'.format('Test counter'))
#ifdef _MCS_DEBUG_
	xil_printf("\r\n>> %s", "Test counter");
#endif
	//#
	//# clear reset1
	//dev.SetWireInValue(EP_ADRS['TEST_CON'],0x00,0x01) # (ep,val,mask)
	write_mcs_ep_wi(EP_ADRS__TEST_CON, 0x00, 0x01);
	//# clear disable1
	//dev.SetWireInValue(EP_ADRS['TEST_CON'],0x00,0x02) # (ep,val,mask)
	write_mcs_ep_wi(EP_ADRS__TEST_CON, 0x00, 0x02);
	//#
	//if opt.upper()=='ON':
	if (opt==CMU_PAR__ON) {
	//	# set autocount2
	//	dev.SetWireInValue(EP_ADRS['TEST_CON'],0x04,0x04) # (ep,val,mask)
		write_mcs_ep_wi(EP_ADRS__TEST_CON, 0x04, 0x04);
	}
	//elif opt.upper()=='OFF':
	else if (opt==CMU_PAR__OFF) {
	//	# clear autocount2
	//	dev.SetWireInValue(EP_ADRS['TEST_CON'],0x00,0x04) # (ep,val,mask)
		write_mcs_ep_wi(EP_ADRS__TEST_CON, 0x00, 0x04);
	}
	//else:
	//	pass
	//#
	//dev.UpdateWireIns()
	//#
	//#
	//if opt.upper()=='RESET':
	if (opt==CMU_PAR__RESET) {
	//	# set reset2 // reset counter #2
	//	dev.ActivateTriggerIn(EP_ADRS['TEST_TI'], 0) # (ep,bit)
		activate_mcs_ep_ti(EP_ADRS__TEST_TI, 0);
	}
	//else:
	//	pass
	//#
#ifdef _MCS_DEBUG_
	//# read counters 
	u32 test_out;
	u32 count1;
	u32 count2;
	//dev.UpdateWireOuts()
	//test_out = dev.GetWireOutValue(EP_ADRS['TEST_OUT'])
	test_out =  read_mcs_ep_wo(EP_ADRS__TEST_OUT, MASK_ALL);
	//count1 = test_out & 0xFF
	count1 = test_out & 0xFF;
	//count2 = (test_out>>8) & 0xFF
	count2 = (test_out>>8) & 0xFF;
	xil_printf("\r\n> %s : %d", "count1", count1);
	xil_printf("\r\n> %s : %d", "count2", count2);
#endif
	//#
	//return [count1, count2]
	return 0;
}

u32 cmu_xxx_enable(const u8* msg_title, u32 wi, u32 wo) {
	xil_printf("\r\n>> %s", msg_title);
	write_mcs_ep_wi(wi, 0x01, 0x01);
	u32 flag;
	flag =  read_mcs_ep_wo(wo, MASK_ALL);
#ifdef _MCS_DEBUG_
	xil_printf("\r\n> %s = 0x%08X", "flag", flag);
#endif
	flag = (flag&0x00000001);
	return flag;
}

u32 cmu_xxx_init(const u8* msg_title, u32 wi, u32 wo) {
	//xil_printf("\r\n>> %s", "Initialize SPO");
	xil_printf("\r\n>> %s", msg_title);
	//# set bit
	//write_mcs_ep_wi(EP_ADRS__SPO_CON, 0x02, 0x02); // set init bit
	write_mcs_ep_wi(wi, 0x02, 0x02); // set init bit
	//# reset bit
	//write_mcs_ep_wi(EP_ADRS__SPO_CON, 0x00, 0x02); // reset init bit
	write_mcs_ep_wi(wi, 0x00, 0x02); // reset init bit
	//#
	u32 cnt_done;
	u32 MAX_CNT;
	u32 flag;
	u32 init_done;
	//# check init_done flag
	cnt_done = 0;
	MAX_CNT = 20000;
	while (1) {
		//flag =  read_mcs_ep_wo(EP_ADRS__SPO_FLAG, MASK_ALL);
		flag =  read_mcs_ep_wo(wo, MASK_ALL);
		init_done = (flag&0x00000002)>>1;
#ifdef _MCS_DEBUG_
		xil_printf("\r\n> %s = 0x%08X", "flag", flag);
#endif
		if (init_done==1) break;
		cnt_done++;
		if (cnt_done>=MAX_CNT) break;
	}
#ifdef _MCS_DEBUG_
	xil_printf("\r\n> %s = 0x%08X", "cnt_done", cnt_done);
	xil_printf("\r\n> %s = 0x%08X", "init_done", init_done);
#endif
	return init_done;
}

u32 cmu_xxx_update(const u8* msg_title, u32 wi, u32 wo) {
	//xil_printf("\r\n>> %s", "Update SPO");
	xil_printf("\r\n>> %s", msg_title);
	//# set bit
	//write_mcs_ep_wi(EP_ADRS__SPO_CON, 0x04, 0x04); // set init bit
	write_mcs_ep_wi(wi, 0x04, 0x04); // set init bit
	//# reset bit
	//write_mcs_ep_wi(EP_ADRS__SPO_CON, 0x00, 0x04); // reset init bit
	write_mcs_ep_wi(wi, 0x00, 0x04); // reset init bit
	//dev.UpdateWireIns()
	//#
	u32 cnt_done;
	u32 MAX_CNT;
	u32 flag;
	u32 update_done;
	//# check update_done flag
	cnt_done = 0;
	MAX_CNT = 20000;
	while (1) {
		//flag =  read_mcs_ep_wo(EP_ADRS__SPO_FLAG, MASK_ALL);
		flag =  read_mcs_ep_wo(wo, MASK_ALL);
		update_done = (flag&0x00000004)>>2;
		if (update_done==1) break;
		cnt_done++;
		if (cnt_done>=MAX_CNT) break;
	}
#ifdef _MCS_DEBUG_
	xil_printf("\r\n> %s = 0x%08X", "cnt_done", cnt_done);
	xil_printf("\r\n> %s = 0x%08X", "update_done", update_done);
#endif
	return update_done;
}

u32 cmu_xxx_disable(const u8* msg_title, u32 wi, u32 wo) {
	xil_printf("\r\n>> %s", msg_title);
	write_mcs_ep_wi(wi, 0x00, 0x01);
	u32 flag;
	flag =  read_mcs_ep_wo(wo, MASK_ALL);
#ifdef _MCS_DEBUG_
	xil_printf("\r\n> %s = 0x%08X", "flag", flag);
#endif
	flag = (flag&0x00000001);
	return flag;
}


// SPO 

u32 cmu_spo_enable() {
	u8* msg_title = (u8*)"Enable SPO";
	u32 wi = EP_ADRS__SPO_CON;
	u32 wo = EP_ADRS__SPO_FLAG;
	u32 flag;
	flag = cmu_xxx_enable(msg_title, wi, wo);
	//xil_printf("\r\n>> %s", "Enable SPO");
	//write_mcs_ep_wi(EP_ADRS__SPO_CON, 0x01, 0x01);
	//u32 flag;
	//flag =  read_mcs_ep_wo(EP_ADRS__SPO_FLAG, MASK_ALL);
	//xil_printf("\r\n> %s = 0x%08X", "flag", flag);
	//flag = (flag&0x00000001);
	return flag;
}

u32 cmu_spo_init() {
	u8* msg_title = (u8*)"Initialize SPO";
	u32 wi = EP_ADRS__SPO_CON;
	u32 wo = EP_ADRS__SPO_FLAG;
	u32 flag;
	//
	//# set parameters adrs_start: 0
	write_mcs_ep_wi(wi, 0x00<<4, 0x07<<4);
	//# set parameters num_bytes: 7
	write_mcs_ep_wi(wi, 0x07<<8, 0x07<<8);
	//
	// clear buffer
	cmu_spo_set_buffer(EP_ADRS__SPO_DIN_B3_H, 0, MASK_ALL);
	cmu_spo_set_buffer(EP_ADRS__SPO_DIN_B3_L, 0, MASK_ALL);
	cmu_spo_set_buffer(EP_ADRS__SPO_DIN_B2_H, 0, MASK_ALL);
	cmu_spo_set_buffer(EP_ADRS__SPO_DIN_B2_L, 0, MASK_ALL);
	cmu_spo_set_buffer(EP_ADRS__SPO_DIN_B1_H, 0, MASK_ALL);
	cmu_spo_set_buffer(EP_ADRS__SPO_DIN_B1_L, 0, MASK_ALL);
	cmu_spo_set_buffer(EP_ADRS__SPO_DIN_B0_H, 0, MASK_ALL);
	cmu_spo_set_buffer(EP_ADRS__SPO_DIN_B0_L, 0, MASK_ALL);
	//
	// common 
	flag = cmu_xxx_init(msg_title, wi, wo);
	return flag;
}

u32 cmu_spo_update() {
	u8* msg_title = (u8*)"Update SPO";
	u32 wi = EP_ADRS__SPO_CON;
	u32 wo = EP_ADRS__SPO_FLAG;
	u32 flag;
	flag = cmu_xxx_update(msg_title, wi, wo);
	return flag;
}

u32 cmu_spo_disable() {
	u8* msg_title = (u8*)"Disable SPO";
	u32 wi = EP_ADRS__SPO_CON;
	u32 wo = EP_ADRS__SPO_FLAG;
	u32 flag;
	flag = cmu_xxx_disable(msg_title, wi, wo);
	return flag;
}

u32 cmu_spo_set_buffer(u32 spo_idx, u32 val, u32 mask) {
//	dev = cmu_ctrl.dev
//	EP_ADRS = conf.OK_EP_ADRS_CONFIG
//	#
//	print('>> {}'.format('Set SPO buffer'))
	xil_printf("\r\n>> %s", "Set SPO buffer");
//	#
//	try:
//		ep = EP_ADRS[spo_idx]
//	except:
//		print('> spo_idx is expected as SPO_DIN_B#_L or SPO_DIN_B#_H.')
//		return False
//	#
#ifdef _MCS_DEBUG_
//	print('{} = 0x{:02X}'.format('ep',ep))
	xil_printf("\r\n> %s = 0x%02X", "spo_idx", spo_idx);
//	print('{} = 0x{:08X}'.format('val',val))
	xil_printf("\r\n> %s = 0x%08X", "val", val);
//	print('{} = 0x{:08X}'.format('mask',mask))
	xil_printf("\r\n> %s = 0x%08X", "mask", mask);
//	#
#endif
//	dev.SetWireInValue(ep,val,mask) 
	write_mcs_ep_wi(spo_idx, val, mask); 
//	dev.UpdateWireIns()
//	#
//	return True
	return 0;
}

u32 cmu_spo_read_buffer(u32 spo_idx) {
//	dev = cmu_ctrl.dev
//	EP_ADRS = conf.OK_EP_ADRS_CONFIG
//	#
//	print('>> {}'.format('Read SPO buffer'))
	xil_printf("\r\n>> %s", "Read SPO buffer");
//	#
//	try:
//		ep = EP_ADRS[spo_idx]
//	except:
//		print('> spo_idx is expected as SPO_MON_B#_L or SPO_MON_B#_H.')
//		return False
//	#
//	dev.UpdateWireOuts()
	u32 val;
//	val = dev.GetWireOutValue(ep)
	val =  read_mcs_ep_wo(spo_idx, MASK_ALL);
//	#
//	print('{} = 0x{:02X}'.format('ep',ep))
//	#
//	return val
	return val;
}

u32 cmu_spo_bit__leds(u32 opt) {
	//print('>>> {} : 0x{:02X}'.format('Control LEDs',opt))
	xil_printf("\r\n>>> %s : 0x%02X", "Control LEDs", opt);
	//#
	//try:
	u32 val;
	//	val = (opt&0xFF)<<8
	val = (opt&0xFF)<<8;
	//except:
	//	print('> opt is expected as 0x00 ~ 0xFF.')
	//	return False
	//mask = 0x0000FF00
	//cmu_spo_set_buffer('SPO_DIN_B3_L',val,mask)
	cmu_spo_set_buffer(EP_ADRS__SPO_DIN_B3_L, val, 0x0000FF00);
	//cmu_spo_update()
	cmu_spo_update();
	//
	// readback buffer
	val = cmu_spo_read_buffer(EP_ADRS__SPO_DIN_B3_L);
#ifdef _MCS_DEBUG_
	xil_printf("\r\n> %s : 0x%08X", "EP_ADRS__SPO_DIN_B3_L", val);
#endif
	//return True
	return 0;
}

u32 cmu_spo_bit__amp_pwr  (u32 opt) {
	//print('>>> {} : {}'.format('Control AMP_PWR',opt))
	xil_printf("\r\n>>> %s : 0x%08X", "Control AMP_PWR", opt);
	//val_dict = {
	//	'OFF' : 0x00000000, 
	//	'ON'  : 0x00000001
	// }
	//try:
	u32 val;
	//	val = val_dict[opt]
	if (opt == CMU_PAR__ON) 
		val = 0x01;
	else 
		val = 0x00;
	//except:
	//	print('> opt is expected as OFF or ON.')
	//	return False
	//mask = 0x00000001
	//cmu_spo_set_buffer('SPO_DIN_B3_L',val,mask)
	cmu_spo_set_buffer(EP_ADRS__SPO_DIN_B3_L, val, 0x00000001);
	//cmu_spo_update()
	cmu_spo_update();
	//return True
	return 0;
}
	
u32 cmu_spo_bit__adc0_gain(u32 opt) {
//	print('>>> {} : {}'.format('Control ADC0 gain',opt))
	xil_printf("\r\n>>> %s : 0x%08X", "Control ADC0 gain", opt);
//	val_dict = {
//		'100X' : 0x00020000, 
//		'10X'  : 0x00010000, 
//		'1X'   : 0x00000000,
//		'OFF'  : 0x00000000
//	}
//	try:
	u32 val;
//		val = val_dict[opt]
	if      (opt == CMU_PAR__GN_100X) 
		val = 0x00020000;
	else if (opt == CMU_PAR__GN_10X) 
		val = 0x00010000;
	else if (opt == CMU_PAR__GN_1X) 
		val = 0x00000000;
	else if (opt == CMU_PAR__GN_OFF) 
		val = 0x00000000;
	else 
		val = 0x00000000;
//	except:
//		print('> opt is expected as 1X, 10X or 100X.')
//		return False
//	mask = 0x000F0000
//	cmu_spo_set_buffer('SPO_DIN_B3_L',val,mask)
	cmu_spo_set_buffer(EP_ADRS__SPO_DIN_B3_L, val, 0x000F0000);
//	cmu_spo_update()
	cmu_spo_update();
//	return True
	return 0;
}

u32 cmu_spo_bit__adc1_gain(u32 opt) {
//	print('>>> {} : {}'.format('Control ADC1 gain',opt))
	xil_printf("\r\n>>> %s : 0x%08X", "Control ADC1 gain", opt);
//	val_dict = {
//		'100X' : 0x00200000, 
//		'10X'  : 0x00100000, 
//		'1X'   : 0x00000000,
//		'OFF'  : 0x00000000
//	}
//	try:
	u32 val;
//		val = val_dict[opt]
	if      (opt == CMU_PAR__GN_100X) 
		val = 0x00020000;
	else if (opt == CMU_PAR__GN_10X) 
		val = 0x00010000;
	else if (opt == CMU_PAR__GN_1X) 
		val = 0x00000000;
	else if (opt == CMU_PAR__GN_OFF) 
		val = 0x00000000;
	else 
		val = 0x00000000;
//	except:
//		print('> opt is expected as 1X, 10X or 100X.')
//		return False
//	mask = 0x00F00000
//	cmu_spo_set_buffer('SPO_DIN_B3_L',val,mask)
	cmu_spo_set_buffer(EP_ADRS__SPO_DIN_B3_L, val, 0x00F00000);
//	cmu_spo_update()
	cmu_spo_update();
//	return True
	return 0;
}

u32 cmu_spo_bit__vi_bw    (u32 opt) {
//	print('>>> {} : {}'.format('Control vi bandwidth',opt))
	xil_printf("\r\n>>> %s : 0x%08X", "Control vi bandwidth", opt);
//	val_dict = {
//		'120K' : 0x08000000, 
//		'1M2'  : 0x04000000, 
//		'12M'  : 0x02000000, 
//		'120M' : 0x01000000, 
//		'OFF'  : 0x00000000
//	}
//	try:
	u32 val;
//		val = val_dict[opt]
	if      (opt == CMU_PAR__BW_120K) 
		val = 0x08000000;
	else if (opt == CMU_PAR__BW_1M2) 
		val = 0x04000000;
	else if (opt == CMU_PAR__BW_12M) 
		val = 0x02000000;
	else if (opt == CMU_PAR__BW_120M) 
		val = 0x01000000;
	else if (opt == CMU_PAR__BW_OFF) 
		val = 0x00000000;
	else 
		val = 0x00000000;
//	except:
//		print('> opt is expected as 1M2, 120K or ...')
//		return False
//	mask = 0x0F000000
//	cmu_spo_set_buffer('SPO_DIN_B3_L',val,mask)
	cmu_spo_set_buffer(EP_ADRS__SPO_DIN_B3_L, val, 0x0F000000);
//	cmu_spo_update()
	cmu_spo_update();
//	return True
	return 0;
}

u32 cmu_spo_bit__vq_bw    (u32 opt) {
//	print('>>> {} : {}'.format('Control vq bandwidth',opt))
	xil_printf("\r\n>>> %s : 0x%08X", "Control vq bandwidth", opt);
//	val_dict = {
//		'120K' : 0x80000000, 
//		'1M2'  : 0x40000000, 
//		'12M'  : 0x20000000, 
//		'120M' : 0x10000000, 
//		'OFF'  : 0x00000000
//	}
//	try:
	u32 val;
//		val = val_dict[opt]
	if      (opt == CMU_PAR__BW_120K) 
		val = 0x08000000;
	else if (opt == CMU_PAR__BW_1M2) 
		val = 0x04000000;
	else if (opt == CMU_PAR__BW_12M) 
		val = 0x02000000;
	else if (opt == CMU_PAR__BW_120M) 
		val = 0x01000000;
	else if (opt == CMU_PAR__BW_OFF) 
		val = 0x00000000;
	else 
		val = 0x00000000;
//	except:
//		print('> opt is expected as 1M2, 120K or ...')
//		return False
//	mask = 0xF0000000
//	cmu_spo_set_buffer('SPO_DIN_B3_L',val,mask)
	cmu_spo_set_buffer(EP_ADRS__SPO_DIN_B3_L, val, 0xF0000000);
//	cmu_spo_update()
	cmu_spo_update();
//	return True
	return 0;
}
	
// DAC_BIAS 
u32 cmu_dac_bias_enable() {
	u8* msg_title = (u8*)"Enable DAC_BIAS";
	u32 wi = EP_ADRS__DAC_BIAS_CON;
	u32 wo = EP_ADRS__DAC_BIAS_FLAG;
	u32 flag;
	flag = cmu_xxx_enable(msg_title, wi, wo);
	return flag;
}

u32 cmu_dac_bias_init() {
	u8* msg_title = (u8*)"Initialize DAC_BIAS";
	u32 wi = EP_ADRS__DAC_BIAS_CON;
	u32 wo = EP_ADRS__DAC_BIAS_FLAG;
	u32 flag;
	//
	//# set parameters ... forced pin output : 0x0B
	u32 val;
	val = 0x0B;
	//dev.SetWireInValue(wi,val<<16,0xFF<<16) # (ep,val,mask) 
	write_mcs_ep_wi(wi,val<<16,0xFF<<16);
	//# set parameters ... range : 0xFF for +/-10V; 0xAA for +/-5V.
	val = 0xFF;
	//dev.SetWireInValue(wi,val<<24,0xFF<<24) # (ep,val,mask) 
	write_mcs_ep_wi(wi,val<<24,0xFF<<24);
	//
	// common 
	flag = cmu_xxx_init(msg_title, wi, wo);
	return flag;
}

u32 cmu_dac_bias_update() {
	u8* msg_title = (u8*)"Update DAC_BIAS";
	u32 wi = EP_ADRS__DAC_BIAS_CON;
	u32 wo = EP_ADRS__DAC_BIAS_FLAG;
	u32 flag;
	flag = cmu_xxx_update(msg_title, wi, wo);
	return flag;	
}

u32 cmu_dac_bias_disable() {
	u8* msg_title = (u8*)"Disable DAC_BIAS";
	u32 wi = EP_ADRS__DAC_BIAS_CON;
	u32 wo = EP_ADRS__DAC_BIAS_FLAG;
	u32 flag;
	flag = cmu_xxx_disable(msg_title, wi, wo);
	return flag;
}

u32 cmu_dac_bias_set_buffer(u32 DAC1, u32 DAC2, u32 DAC3, u32 DAC4) {
	u8* msg_title = (u8*)"Set DAC_BIAS buffer";
	u32 wi21 = EP_ADRS__DAC_BIAS_DIN21;
	u32 wi43 = EP_ADRS__DAC_BIAS_DIN43;
	u32 DAC_21;
	u32 DAC_43;
	//
	xil_printf("\r\n>> %s", msg_title);
	//
	DAC_21 = (DAC2<<16)|DAC1;
	DAC_43 = (DAC4<<16)|DAC3;
	//
#ifdef _MCS_DEBUG_
	xil_printf("\r\n> %s = 0x%08X", "DAC_21", DAC_21);
	xil_printf("\r\n> %s = 0x%08X", "DAC_43", DAC_43);
#endif
	//
	write_mcs_ep_wi(wi21, DAC_21, MASK_ALL);
	write_mcs_ep_wi(wi43, DAC_43, MASK_ALL);
	return 0;
}

u32 cmu_dac_bias_readback(u32 *p_DACx) {
	u8* msg_title = (u8*)"Readback DAC_BIAS";
	u32 wo21 = EP_ADRS__DAC_BIAS_RB21;
	u32 wo43 = EP_ADRS__DAC_BIAS_RB43;
	u32 DAC_21;
	u32 DAC_43;
	u32 DAC1;
	u32 DAC2;
	u32 DAC3;
	u32 DAC4;
	//
	xil_printf("\r\n>> %s", msg_title);
	//
	DAC_21 = read_mcs_ep_wo(wo21, MASK_ALL);
	DAC_43 = read_mcs_ep_wo(wo43, MASK_ALL);
	//
	DAC1 = (DAC_21    )&0xFFFF;
	DAC2 = (DAC_21>>16)&0xFFFF;
	DAC3 = (DAC_43    )&0xFFFF;
	DAC4 = (DAC_43>>16)&0xFFFF;
	//
#ifdef _MCS_DEBUG_
	xil_printf("\r\n> %s = 0x%04X", "DAC1", DAC1);
	xil_printf("\r\n> %s = 0x%04X", "DAC2", DAC2);
	xil_printf("\r\n> %s = 0x%04X", "DAC3", DAC3);
	xil_printf("\r\n> %s = 0x%04X", "DAC4", DAC4);
#endif
	//
	p_DACx[0] = DAC1;
	p_DACx[1] = DAC2;
	p_DACx[2] = DAC3;
	p_DACx[3] = DAC4;
	//
	return 0;
}

// DAC_A2A3
//... 

// DWAVE

u32 cmu_dwave_enable() {
	u8* msg_title = (u8*)"Enable DWAVE";
	u32 wi = EP_ADRS__DWAVE_CON;
	u32 wo = EP_ADRS__DWAVE_FLAG;
	u32 flag;
	flag = cmu_xxx_enable(msg_title, wi, wo);
	return flag;
}

u32 cmu_dwave_disable() {
	u8* msg_title = (u8*)"Disable DWAVE";
	u32 wi = EP_ADRS__DWAVE_CON;
	u32 wo = EP_ADRS__DWAVE_FLAG;
	u32 flag;
	flag = cmu_xxx_disable(msg_title, wi, wo);
	return flag;
}

u32 cmu_dwave_read_base_freq() {
	u8* msg_title = (u8*)"Read DWAVE_BASE_FREQ";
	u32 wo = EP_ADRS__DWAVE_BASE_FREQ;
	u32 dwave_base_freq;
	//
	xil_printf("\r\n>> %s", msg_title);
	dwave_base_freq = read_mcs_ep_wo(wo, MASK_ALL);
#ifdef _MCS_DEBUG_
	xil_printf("\r\n> %s = %d", "dwave_base_freq", dwave_base_freq);
#endif
	return dwave_base_freq;
}

u32 cmu_dwave_rd__trig(u32 bit_loc) {
	u8* msg_title = (u8*)"Trigger read DWAVE";
	u32 ti = EP_ADRS__DWAVE_TI;
	u32 wo = EP_ADRS__DWAVE_DOUT_BY_TRIG;
	u32 val;
	//
	xil_printf("\r\n>> %s TI[%d]", msg_title,bit_loc);
	activate_mcs_ep_ti(ti,bit_loc);
	val = read_mcs_ep_wo(wo, MASK_ALL);
	return val;
}

u32 cmu_dwave_wr__trig(u32 bit_loc, u32 val) {
	u8* msg_title = (u8*)"Trigger write DWAVE";
	u32 ti = EP_ADRS__DWAVE_TI;
	u32 wi = EP_ADRS__DWAVE_DIN_BY_TRIG;
	//
	xil_printf("\r\n>> %s TI[%d] : %d", msg_title,bit_loc,val);
	write_mcs_ep_wi(wi, val, MASK_ALL);
	activate_mcs_ep_ti(ti,bit_loc);
	return val;
}

u32 cmu_dwave_wr_cnt_period(u32 val) {
	u8* msg_title = (u8*)"Write DWAVE cnt_period";
	xil_printf("\r\n>>> %s : %d", msg_title, val);
	cmu_dwave_wr__trig(16, val);
	return 0;
}

u32 cmu_dwave_rd_cnt_period() {
	u8* msg_title = (u8*)"Read DWAVE cnt_period";
	u32 val;
	//
	val = cmu_dwave_rd__trig(24);
	xil_printf("\r\n>>> %s : %d", msg_title, val);
	return val;
}


//....



//// SCPI exec functions // based on MCS-EP subfunctions
// 
// *RST <NL>			
// *IDN? <NL>			
// 			
// :CMEP:EN ON|OFF <NL>			
// :CMEP:EN? <NL>			
// 			
// :CMEP:WIMK  #Hnnnnnnnn <NL>			
// :CMEP:WIMK? <NL>			
// 			
// :CMEP:WOMK  #Hnnnnnnnn <NL>			
// :CMEP:WOMK? <NL>			
// 			
// :CMEP:TIMK  #Hnnnnnnnn <NL>			
// :CMEP:TIMK? <NL>			
// 			
// :CMEP:TOMK  #Hnnnnnnnn <NL>			
// :CMEP:TOMK? <NL>			
// 			
// 			
// :CMEP:WI#Hnn  #Hnnnnnnnn <NL>			
// :CMEP:WI#Hnn? <NL>			
// 			
// :CMEP:WO#Hnn  #Hnnnnnnnn <NL>			
// :CMEP:WO#Hnn? <NL>			
// 			
// :CMEP:TI#Hnn  #Hnnnnnnnn <NL>			
// :CMEP:TI#Hnn? <NL>			
// 			
// :CMEP:TO#Hnn  #Hnnnnnnnn <NL>			
// :CMEP:TO#Hnn? <NL>			
// 			
// :CMEP:PI#Hnn  #m_nnnnnn_ rrrrrrrrrrrrrrrrrrrrr <NL>			
// :CMEP:PI#Hnn? <NL>			
// 			
// :CMEP:PO#Hnn  #Hnnnnnnnn <NL>			
// :CMEP:PO#Hnn? <NL>			






























