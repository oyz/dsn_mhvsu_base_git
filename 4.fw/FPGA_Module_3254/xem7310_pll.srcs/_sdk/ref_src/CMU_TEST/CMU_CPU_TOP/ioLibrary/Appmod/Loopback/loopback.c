#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include "microblaze_sleep.h" // for usleep

#include "../../Ethernet/socket.h"
#include "loopback.h"

//$$ support CMU-CPU and scpi server 
//#include "../CMU_CPU_TOP/cmu_cpu_config.h" // CMU-CPU board config
//#include "../CMU_CPU_TOP/cmu_cpu.h" // for hw_reset
#include "../../../cmu_cpu_config.h" // CMU-CPU board config
#include "../../../cmu_cpu.h" // for hw_reset


//## add scpi tcp server : scpi_tcps()

// scpi parameters / data 
//uint8_t* cmd_str__XXX = (uint8_t*)"XXX";
uint8_t* cmd_str__IDN       = (uint8_t*)"*IDN?\n";
uint8_t* cmd_str__RST       = (uint8_t*)"*RST\n";
uint8_t* cmd_str__CMEP_EN   = (uint8_t*)":CMEP:EN"; 
uint8_t* cmd_str__CMEP_MKWI = (uint8_t*)":CMEP:MKWI";
uint8_t* cmd_str__CMEP_MKWO = (uint8_t*)":CMEP:MKWO";
uint8_t* cmd_str__CMEP_MKTI = (uint8_t*)":CMEP:MKTI";
uint8_t* cmd_str__CMEP_MKTO = (uint8_t*)":CMEP:MKTO";
uint8_t* cmd_str__CMEP_WI   = (uint8_t*)":CMEP:WI";
uint8_t* cmd_str__CMEP_WO   = (uint8_t*)":CMEP:WO";
uint8_t* cmd_str__CMEP_TI   = (uint8_t*)":CMEP:TI";
uint8_t* cmd_str__CMEP_TO   = (uint8_t*)":CMEP:TO";
uint8_t* cmd_str__CMEP_TAC  = (uint8_t*)":CMEP:TAC";
uint8_t* cmd_str__CMEP_TMO  = (uint8_t*)":CMEP:TMO";
uint8_t* cmd_str__CMEP_PI   = (uint8_t*)":CMEP:PI";
uint8_t* cmd_str__CMEP_PO   = (uint8_t*)":CMEP:PO";
uint8_t* cmd_str__CMEP_WMI  = (uint8_t*)":CMEP:WMI";
uint8_t* cmd_str__CMEP_WMO  = (uint8_t*)":CMEP:WMO";

//#define LEN_CMD_STR__XXX   3
#define LEN_CMD_STR__IDN         6 // "*IDN?\n"
#define LEN_CMD_STR__RST         5 // "*RST\n"
#define LEN_CMD_STR__CMEP_EN     8 // ":CMEP:EN"
#define LEN_CMD_STR__CMEP_MKWI  10 // ":CMEP:MKWI"
#define LEN_CMD_STR__CMEP_MKWO  10 // ":CMEP:MKWO"
#define LEN_CMD_STR__CMEP_MKTI  10 // ":CMEP:MKTI"
#define LEN_CMD_STR__CMEP_MKTO  10 // ":CMEP:MKTO"
#define LEN_CMD_STR__CMEP_WI     8 // ":CMEP:WI"
#define LEN_CMD_STR__CMEP_WO     8 // ":CMEP:WO"
#define LEN_CMD_STR__CMEP_TI     8 // ":CMEP:TI"
#define LEN_CMD_STR__CMEP_TO     8 // ":CMEP:TO"
#define LEN_CMD_STR__CMEP_TAC    9 // ":CMEP:TAC"
#define LEN_CMD_STR__CMEP_TMO    9 // ":CMEP:TMO"
#define LEN_CMD_STR__CMEP_PI     8 // ":CMEP:PI"
#define LEN_CMD_STR__CMEP_PO     8 // ":CMEP:PO"
#define LEN_CMD_STR__CMEP_WMI    9 // ":CMEP:WMI"
#define LEN_CMD_STR__CMEP_WMO    9 // ":CMEP:WMO"

// https://mcuoneclipse.com/2013/04/14/text-data-and-bss-code-and-data-size-explained/

//
uint8_t* rsp_str__IDN = (uint8_t*)"CMU-CPU-F5500; SBT " __TIME__ ", " __DATE__;
//
uint8_t* rsp_str__NULL = (uint8_t*)"\0";
uint8_t* rsp_str__OK   = (uint8_t*)"OK\n";
uint8_t* rsp_str__NG   = (uint8_t*)"NG\n";
uint8_t* rsp_str__OFF  = (uint8_t*)"OFF\n";
uint8_t* rsp_str__ON   = (uint8_t*)"ON\n";
uint8_t* rsp_str__NL   = (uint8_t*)"\n"; // sentinel for numberic block

// scpi subfunctions:

// send response all 
int32_t send_response_all(uint8_t sn, uint8_t *p_rsp_str, int32_t size) {
	int32_t sentsize;
	int32_t ret;
	//
	if (size==0)
		return 0;
	//
	sentsize = 0;
	while(size != sentsize) {
		ret = send(sn, p_rsp_str+sentsize, size-sentsize); //$$ send
		if(ret < 0) {
			return ret;
		}
#ifdef _SCPI_DEBUG_
		printf("send size:%d , string size:%d, contents:%s \n",(int)ret,(int)(size-sentsize),(p_rsp_str+sentsize));
#endif
		sentsize += ret; // Don't care SOCKERR_BUSY, because it is zero.
	}
	return ret;
}

// send_response_all_from_pipe32()
//   send data from pipe32 
//   new send_from_pipe32() in socket.c
//   new wiz_send_data_from_pipe32() in w5500.c
//   new WIZCHIP_WRITE_PIPE() in w5500.c
//   new write_data_pipe__wz850() in cmu_cpu.c
//
int32_t send_response_all_from_pipe32(uint8_t sn, uint32_t src_adrs_p32, int32_t size) {
	int32_t sentsize;
	int32_t ret;
	//
	if (size==0)
		return 0;
	//
	sentsize = 0;
	while(size != sentsize) {
		//$$ret = send(sn, p_rsp_str+sentsize, size-sentsize); //$$ send
		ret = send_from_pipe32(sn, src_adrs_p32, size-sentsize); //$$ send
		if(ret < 0) {
			return ret;
		}
#ifdef _SCPI_DEBUG_
		printf("sent size :%d , size to send:%d, prev sent size:%d \n",(int)ret,(int)(size-sentsize),(int)sentsize);
#endif
		sentsize += ret; // Don't care SOCKERR_BUSY, because it is zero.
	}
	return ret;
}


// TCP Loopback Test
int32_t loopback_tcps(uint8_t sn, uint8_t* buf, uint16_t port)
{
   int32_t ret;
   uint16_t size = 0;
   uint16_t sentsize=0;
#ifdef _LOOPBACK_DEBUG_
   uint8_t destip[4];
   uint16_t destport;
#endif
   uint8_t sr; //$$
#ifdef _LOOPBACK_DEBUG_WCMSG_
   uint8_t* msg_welcome = (uint8_t*)"> Loopback TCP server is established: \r\n";
#endif

   switch(sr=getSn_SR(sn))
   {
      case SOCK_ESTABLISHED :
    	 //$$ case of new establish
         if(getSn_IR(sn) & Sn_IR_CON)
         {
#ifdef _LOOPBACK_DEBUG_
        	 getSn_DIPR(sn, destip);
        	 destport = getSn_DPORT(sn);
        	 //
        	 printf("%d:Connected - %d.%d.%d.%d : %d \n",sn, destip[0], destip[1], destip[2], destip[3], destport);
#endif
             setSn_IR(sn,Sn_IR_CON); //$$ clear establish intr.
#ifdef _LOOPBACK_DEBUG_WCMSG_			 
             //$$ send welcome message
             size = strlen((char*)msg_welcome);
             ret = send(sn,msg_welcome,size); //$$ send welcome msg
             if(ret < 0)
             {
                close(sn);
                return ret;
             }
             //$$
#endif			 
         }
         //$$ loopback data
         if((size = getSn_RX_RSR(sn)) > 0) //$$ check received data size
         {
            if(size > DATA_BUF_SIZE) size = DATA_BUF_SIZE;
            ret = recv(sn, buf, size); //$$ read socket data
            if(ret <= 0) return ret;
            sentsize = 0;
            while(size != sentsize)
            {
               ret = send(sn,buf+sentsize,size-sentsize); //$$ send loopback
               if(ret < 0)
               {
                  close(sn);
                  return ret;
               }
               sentsize += ret; // Don't care SOCKERR_BUSY, because it is zero.
            }
         }
         //$$
         break;
      case SOCK_CLOSE_WAIT :
#ifdef _LOOPBACK_DEBUG_
         //printf("%d:CloseWait \n",sn);
#endif
         if((ret=disconnect(sn)) != SOCK_OK) return ret;
#ifdef _LOOPBACK_DEBUG_
         printf("%d:Socket closed \n",sn);
#endif
         break;
      case SOCK_INIT :
#ifdef _LOOPBACK_DEBUG_
    	 printf("%d:Listen, TCP server loopback, port [%d] \n",sn, port);
#endif
         if( (ret = listen(sn)) != SOCK_OK) return ret;
         break;
      case SOCK_CLOSED:
#ifdef _LOOPBACK_DEBUG_
         //printf("%d:TCP server loopback start \n",sn);
#endif
         if((ret=socket(sn, Sn_MR_TCP, port, 0x00)) != sn)
         //if((ret=socket(sn, Sn_MR_TCP, port, Sn_MR_ND)) != sn)
            return ret;
#ifdef _LOOPBACK_DEBUG_
         printf("%d:Socket opened \n",sn);
         //printf("%d:Opened, TCP server loopback, port [%d] \n",sn, port);
#endif
         break;
      case SOCK_LISTEN:
    	 //$$ nothing to do...
    	 break;
      default:
         break;
   }
   return 1;
}

// SCPI server
static int8_t flag_SOCK_ESTABLISHED = 0;
static int8_t flag_get_rx = 0;
static int32_t cnt_stay_SOCK_ESTABLISHED = MAX_CNT_STAY_SOCK_ESTABLISHED;
//
int32_t scpi_tcps(uint8_t sn, uint8_t* buf, uint16_t port) //$$
{
   int32_t ret, ret2;
   uint16_t size = 0;
   //$$uint16_t sentsize=0;
   int32_t ii;
   int32_t flag__found_newline;
   //
#ifdef _SCPI_DEBUG_
   uint8_t destip[4];
   uint16_t destport;
#endif
   uint8_t sr; //$$
#ifdef _SCPI_DEBUG_WCMSG_
   uint8_t* msg_welcome = (uint8_t*)"> SCPI TCP server is established: \r\n";
#endif
   uint8_t rsp_str[128];
   uint8_t* p_rsp_str;

	switch(sr=getSn_SR(sn))
	{
		case SOCK_ESTABLISHED :
			//$$ case of new establish
			if(getSn_IR(sn) & Sn_IR_CON)
			{
#ifdef _SCPI_DEBUG_
			getSn_DIPR(sn, destip);
			destport = getSn_DPORT(sn);
			//
			printf("%d:Connected - %d.%d.%d.%d : %d \n",sn, destip[0], destip[1], destip[2], destip[3], destport);
#endif
			setSn_IR(sn,Sn_IR_CON); //$$ clear establish intr.
			//
			flag_SOCK_ESTABLISHED = 1;
			flag_get_rx = 0;
			cnt_stay_SOCK_ESTABLISHED = MAX_CNT_STAY_SOCK_ESTABLISHED;
			 //
#ifdef _SCPI_DEBUG_WCMSG_			 
			//$$ send welcome message
			size = strlen((char*)msg_welcome);
			ret = send(sn,msg_welcome,size); //$$ send welcome msg
			if(ret < 0)
			{
				close(sn);
				return ret;
			}
			//
#endif 
		}
		//
         //$$ check input buffer and process SCPI commands... 
         if((size = getSn_RX_RSR(sn)) > 0) { //$$ check received data size
			// for reset counter
			flag_get_rx = 1;
			// cnt_stay_SOCK_ESTABLISHED = MAX_CNT_STAY_SOCK_ESTABLISHED;
			// see if size is too small... wait a moment ... check getSn_RX_RSR() again...
			if (size<5) {
#ifdef _SCPI_DEBUG_
				printf("get rx size again. size:%d \n",(int)size);
#endif
				//usleep(100); // wait for 100us
				usleep(10); // wait for 10us
				size = getSn_RX_RSR(sn);
			}
			
			// move data to buf 
            if(size > DATA_BUF_SIZE-1) size = DATA_BUF_SIZE-1; //$$ a space for sentinel
            ret = recv(sn, buf, size); //$$ read socket data, and save them into buf 
            if(ret <= 0) 
				return ret;
			buf[ret] = '\0'; // add sentinel
			
			// see if buf has <NL> or end of command ... repeat recv() for a while...
			// 16KB buffer ... 100Mbps ... 16KB/(100Mbps) = 1.28 milliseconds
			// wait for 320us ... 4KB size 
			flag__found_newline = 0;
			ret2 = ret;
			while (1) {
				// find <NL> from rear-side
				for (ii=0;ii<ret2;ii++) {
					if (buf[ret-1-ii] == '\n') {
						flag__found_newline = 1;
#ifdef _SCPI_DEBUG_
						printf("flag__found_newline:%d, @ii=%d \n",(int)flag__found_newline,(int)ii);
#endif
						if ((ret-2-ii>=0)&&(buf[ret-2-ii]=='\r')) {
							buf[ret-2-ii]='\n'; // convert '\r' --> '\n'
						}
						break;
					}
				}
				if (flag__found_newline) break;
				//
#ifdef _SCPI_DEBUG_
				printf("get more socket data. flag__found_newline:%d \n",(int)flag__found_newline);
#endif
				//usleep(320); // wait for 320us
				usleep(100); // wait for 100us
				size = getSn_RX_RSR(sn);
				//
				if (size==0) {
					break; // no more data; leave!
				}
				//
				ret2 = recv(sn, buf+ret, size); //$$ read socket data, and save them into buf 
				if(ret2 <= 0) 
					return ret2; //$$
#ifdef _SCPI_DEBUG_
				printf("size=%d, ret=%d, ret2=%d \n",(int)size,(int)ret,(int)ret2);
#endif
				ret = ret+ret2;
				buf[ret] = '\0'; // add sentinel
				//
				// if too many try.... close socket and leave.... 
			}
			// note new line check may fail if command has numberic block...
			// need some method ... what if buf starting with '#4_' must be numberic block...?!
			// waiting for whole numberic block...
			
			
			// find scpi command and respond
			//   - case: buf has the completed command 
			//   - case: buf has no valid command 
			// add 
#ifdef _SCPI_DEBUG_
			size = strlen((char*)buf); // assume buf has ascii... not binary...
			printf("recv size:%d , string size:%d, contents:%s \n",(int)ret,(int)size,buf);
#endif
			// TODO: process scpi commands 
			// TODO: case of  cmd_str__IDN
			if (buf[0]=='\n') { // echo '\n'
				// make scpi response string
				p_rsp_str = rsp_str__NL;
			}
			else if (0==strncmp((char*)cmd_str__IDN,(char*)buf,LEN_CMD_STR__IDN)) { // 0 means eq
				u32 val;
				// make scpi response string
				//   - case: *IDN?<NL> --> "CMU-CPU-F5500, "__DATE__" \n"
				//            add FPGA image ID 
				val = XIomodule_In32 (ADRS_FPGA_IMAGE);
				sprintf((char*)rsp_str,"%s; FID#H%08X\n", rsp_str__IDN, (unsigned int)val);
				p_rsp_str = rsp_str;
			}
			// TODO: case of  cmd_str__RST
			else if (0==strncmp((char*)cmd_str__RST,(char*)buf,LEN_CMD_STR__RST)) { // 0 means eq
				// Reset process ... LAN reset (meaningless) vs CMU reset (SPO/DAVE/ADC init...)
				reset_mcs_ep();
				reset_io_dev();
				// make scpi response string
				p_rsp_str = rsp_str__OK;
			}
			// TODO: case of  cmd_str__CMEP_EN
			else if (0==strncmp((char*)cmd_str__CMEP_EN,(char*)buf,LEN_CMD_STR__CMEP_EN)) { // 0 means eq
				// subfunctions:
				//        enable_mcs_ep()
				//       disable_mcs_ep()
				//    is_enabled_mcs_ep()
				//
				u32 loc = LEN_CMD_STR__CMEP_EN;
				u32 val;
				//
				// skip spaces ' ' and tap
				while (1) {
					if      (buf[loc]==' ') loc++;
					else if (buf[loc]=='\t') loc++;
					else break;
				}
				//
#ifdef _SCPI_DEBUG_
				printf("para:%s\n",(char*)buf+loc);
#endif
				//
				// make scpi response string
				if (buf[loc]=='?') {
					val = is_enabled_mcs_ep();
					if (val == 0) p_rsp_str = rsp_str__OFF;
					else          p_rsp_str = rsp_str__ON;
				}
				else if (0==strncmp("ON", (char*)&buf[loc], 2)) {
					// enable
					enable_mcs_ep();
					//
					p_rsp_str = rsp_str__OK;
				}
				else if (0==strncmp("OFF", (char*)&buf[loc], 3)) {
					disable_mcs_ep();
					p_rsp_str = rsp_str__OK;
				}
				else {
					p_rsp_str = rsp_str__NG;
				}
			}
			// TODO: case of  cmd_str__CMEP_WMI  = (uint8_t*)":CMEP:WMI";
			else if (0==strncmp((char*)cmd_str__CMEP_WMI,(char*)buf,LEN_CMD_STR__CMEP_WMI)) { // 0 means eq
				// subfunctions:
				//     write_mcs_ep_wi_mask(msk);
				//     write_mcs_ep_wi_data(ofs,val);
				//
				// # ":CMEP:WMI#H00 #HABCD1234 #HFF00FF00\n"
				// 
				u32 loc = LEN_CMD_STR__CMEP_WMI; //$$
				u32 val;
				u32 ofs; 
				u32 msk;
				//
				// find offset 
				if (0==strncmp("#H", (char*)&buf[loc], 2)) {
					// read offset
					loc = loc + 2; // locate the numeric parameter head
					ofs = hexstr2data_u32((u8*)(buf+loc),2);
					loc = loc + 2; //		
#ifdef _SCPI_DEBUG_
					printf("ofs: 0x%02X\n",(unsigned int)ofs); 
#endif
					// skip spaces ' ' and tap
					while (1) {
						if      (buf[loc]==' ') loc++;
						else if (buf[loc]=='\t') loc++;
						else break;
					}
					
					// find value
					if (0==strncmp("#H", (char*)&buf[loc], 2)) {
						loc = loc + 2; // locate the numeric parameter head
						val = hexstr2data_u32((u8*)(buf+loc),8);
						loc = loc + 8; //	
#ifdef _SCPI_DEBUG_
						printf("val: 0x%08X\n",(unsigned int)val); 
#endif
						// skip spaces ' ' and tap
						while (1) {
							if      (buf[loc]==' ') loc++;
							else if (buf[loc]=='\t') loc++;
							else break;
						}
						
						// find mask 
						if (0==strncmp("#H", (char*)&buf[loc], 2)) {
							loc = loc + 2; // locate the numeric parameter head
							msk = hexstr2data_u32((u8*)(buf+loc),8);
							//loc = loc + 8; //	
#ifdef _SCPI_DEBUG_
							printf("msk: 0x%08X\n",(unsigned int)msk); 
#endif
							write_mcs_ep_wi_mask(msk);
							write_mcs_ep_wi_data(ofs,val); //$$
							p_rsp_str = rsp_str__OK;
						}
						else {
							// return NG 
							p_rsp_str = rsp_str__NG;
						}
					}
					else {
						// return NG 
						p_rsp_str = rsp_str__NG;
					}
				}
				else {
					// return NG 
					p_rsp_str = rsp_str__NG;
				}
			}
			// TODO: case of  cmd_str__CMEP_WMO  = (uint8_t*)":CMEP:WMO";
			else if (0==strncmp((char*)cmd_str__CMEP_WMO,(char*)buf,LEN_CMD_STR__CMEP_WMO)) { // 0 means eq
				// subfunctions:
				//    write_mcs_ep_wo_mask(msk);
				//     read_mcs_ep_wo_data(ofs);
				//
				// # ":CMEP:WMO#H20 #HFFFF0000\n"
				// 
				u32 loc = LEN_CMD_STR__CMEP_WMO; //$$
				u32 val;
				u32 ofs; 
				u32 msk;
				//
				// find offset 
				if (0==strncmp("#H", (char*)&buf[loc], 2)) {
					// read offset
					loc = loc + 2; // locate the numeric parameter head
					ofs = hexstr2data_u32((u8*)(buf+loc),2);
					loc = loc + 2; //		
#ifdef _SCPI_DEBUG_
					printf("ofs: 0x%02X\n",(unsigned int)ofs); 
#endif
					// skip spaces ' ' and tap
					while (1) {
						if      (buf[loc]==' ') loc++;
						else if (buf[loc]=='\t') loc++;
						else break;
					}
					
					// find mask 
					if (0==strncmp("#H", (char*)&buf[loc], 2)) {
						loc = loc + 2; // locate the numeric parameter head
						msk = hexstr2data_u32((u8*)(buf+loc),8);
						//loc = loc + 8; //	
#ifdef _SCPI_DEBUG_
						printf("msk: 0x%08X\n",(unsigned int)msk); 
#endif
						write_mcs_ep_wo_mask(msk); // write mask 
						val = read_mcs_ep_wo_data(ofs); // read wireout
						sprintf((char*)rsp_str,"#H%08X\n",(unsigned int)val); // '\0' added. ex "H00000002\n"
						p_rsp_str = rsp_str;
					}
					else {
						// return NG 
						p_rsp_str = rsp_str__NG;
					}
				}
				else {
					// return NG 
					p_rsp_str = rsp_str__NG;
				}
			}
			// TODO: case of  cmd_str__CMEP_TAC
			else if (0==strncmp((char*)cmd_str__CMEP_TAC,(char*)buf,LEN_CMD_STR__CMEP_TAC)) { // 0 means eq
				// subfunctions:
				//    u32  read_mcs_ep_wi_mask();
				//    u32 write_mcs_ep_wi_mask(u32 mask);
				//    u32  read_mcs_ep_ti_data(u32 offset);
				//    u32 write_mcs_ep_ti_data(u32 offset, u32 data);
				//    void activate_mcs_ep_ti(u32 offset, u32 bit_loc);
				//
				// # ":CMEP:TAC#H40 #H01\n"
				// ==
				// # ":CMEP:MKTI#H40 #H00000002\n"
				// # ":CMEP:TI#H40   #H00000002\n"
				// 
				u32 loc = LEN_CMD_STR__CMEP_TAC; //$$
				u32 val;
				u32 ofs; 
				//
				// find offset 
				if (0==strncmp("#H", (char*)&buf[loc], 2)) {
					// read offset
					loc = loc + 2; // locate the numeric parameter head
					ofs = hexstr2data_u32((u8*)(buf+loc),2);
					loc = loc + 2; //		
#ifdef _SCPI_DEBUG_
					printf("check: 0x%02X\n",(unsigned int)ofs); 
#endif
					// skip spaces ' ' and tap
					while (1) {
						if      (buf[loc]==' ') loc++;
						else if (buf[loc]=='\t') loc++;
						else break;
					}
					// write command
					if (0==strncmp("#H", (char*)&buf[loc], 2)) {
						loc = loc + 2; // locate the numeric parameter head
						val = hexstr2data_u32((u8*)(buf+loc),2); //$$ read 2-byte para
#ifdef _SCPI_DEBUG_
						printf("check: 0x%08X\n",(unsigned int)val); 
#endif
						activate_mcs_ep_ti(ofs,val);
						// convert bit_loc --> mask 
						//val = (0x00000001<<val);
						//write_mcs_ep_wi_mask(val); //$$
						//write_mcs_ep_ti_data(ofs,val); //$$
						p_rsp_str = rsp_str__OK;
					}
					else {
						// return NG 
						p_rsp_str = rsp_str__NG;
					}
				}
				else {
					// return NG 
					p_rsp_str = rsp_str__NG;
				}
			}
			// TODO: case of  cmd_str__CMEP_TMO
			else if (0==strncmp((char*)cmd_str__CMEP_TMO,(char*)buf,LEN_CMD_STR__CMEP_TMO)) { // 0 means eq
				// subfunctions:
				//    u32  read_mcs_ep_to_mask(); //$$
				//    u32 write_mcs_ep_to_mask(u32 mask); //$$
				//    u32  read_mcs_ep_to_data(u32 offset);
				//    u32 is_triggered_mcs_ep_to(u32 offset, u32 mask);
				//
				// # cmd: ":CMEP:TMO#H60 #H0000FFFF\n" vs ":CMEP:TMO#H60? #H0000FFFF\n"
				// # rsp: "ON\n" or "OFF\n"
				// 
				u32 loc = LEN_CMD_STR__CMEP_TMO; //$$
				u32 val;
				u32 ofs; 
				//
				// find offset 
				if (0==strncmp("#H", (char*)&buf[loc], 2)) {
					// read offset
					loc = loc + 2; // locate the numeric parameter head
					ofs = hexstr2data_u32((u8*)(buf+loc),2);
					loc = loc + 2; //		
#ifdef _SCPI_DEBUG_
					printf("check: 0x%02X\n",(unsigned int)ofs); 
#endif
					// skip spaces ' ' and tap
					while (1) {
						if      (buf[loc]==' ') loc++;
						else if (buf[loc]=='\t') loc++;
						else break;
					}
					// command
					if (0==strncmp("#H", (char*)&buf[loc], 2)) {
						loc = loc + 2; // locate the numeric parameter head
						val = hexstr2data_u32((u8*)(buf+loc),8); //$$ read 8-byte para
#ifdef _SCPI_DEBUG_
						printf("check: 0x%08X\n",(unsigned int)val); 
#endif
						val = is_triggered_mcs_ep_to(ofs,val);
						if (val==0) 
							p_rsp_str = rsp_str__OFF;
						else        
							p_rsp_str = rsp_str__ON;
					}
					else {
						// return NG 
						p_rsp_str = rsp_str__NG;
					}
				}
				else {
					// return NG 
					p_rsp_str = rsp_str__NG;
				}
			}
			// TODO: case of  cmd_str__CMEP_PI
			else if (0==strncmp((char*)cmd_str__CMEP_PI,(char*)buf,LEN_CMD_STR__CMEP_PI)) { // 0 means eq
				// subfunctions:
				//    void dcopy_buf8_to_pipe32(u8 *p_buf_u8, u32 adrs_p32, u32 len_byte); // (src,dst,len_byte)
				//
				// # cmd: ":CMEP:PI#H8A #4_001024_rrrrrrrrrr...rrrrrrrrrr\n"
				// 
				u32 loc = LEN_CMD_STR__CMEP_PI; //$$
				//u32 val;
				u32 ofs; 
				u32 len_byte;
				u32 adrs_p32;
				//
				// find offset 
				if (0==strncmp("#H", (char*)&buf[loc], 2)) {
					// read offset
					loc = loc + 2; // locate the numeric parameter head
					ofs = hexstr2data_u32((u8*)(buf+loc),2);
					loc = loc + 2; //		
#ifdef _SCPI_DEBUG_
					printf("check: 0x%02X\n",(unsigned int)ofs); 
#endif
					// skip spaces ' ' and tap
					while (1) {
						if      (buf[loc]==' ') loc++;
						else if (buf[loc]=='\t') loc++;
						else break;
					}
					// write command
					if (0==strncmp("#4_", (char*)&buf[loc], 3)) { //$$ H4 ... numeric block of 4-byte unit
						loc = loc + 3; // locate the numeric parameter head
						len_byte = decstr2data_u32((u8*)(buf+loc),6); //$$ 6 bytes for data byte length
						loc = loc + 7; // skip a char '_'
#ifdef _SCPI_DEBUG_
						printf("check: 0x%06d\n",(unsigned int)len_byte); 
#endif
						// copy buf to pipe 
						adrs_p32 = ADRS_BASE_CMU + (ofs<<4); 
						//dcopy_buf32_to_pipe32((u32*)(buf+loc), adrs_p32, len_byte);
						dcopy_buf8_to_pipe32((u8*)(buf+loc), adrs_p32, len_byte);
						//
						p_rsp_str = rsp_str__OK;
					}
					else {
						// return NG 
						p_rsp_str = rsp_str__NG;
					}
				}
				else {
					// return NG 
					p_rsp_str = rsp_str__NG;
				}
			}
			// TODO: case of  cmd_str__CMEP_PO
			else if (0==strncmp((char*)cmd_str__CMEP_PO,(char*)buf,LEN_CMD_STR__CMEP_PO)) { // 0 means eq
				// subfunctions:
				//    void dcopy_pipe32_to_pipe8 (u32 src_adrs_p32, u32 dst_adrs_p8,  u32 len_byte);
				//
				// # cmd: ":CMEP:PO#HAA 000040\n"
				// # cmd: ":CMEP:PO#HAA 001024\n"
				// # cmd: ":CMEP:PO#HBC 131072\n"
				// # rsp: #4_001024_rrrrrrrrrr...rrrrrrrrrr\n"
				//
				u32 loc = LEN_CMD_STR__CMEP_PO; //$$
				//u32 val;
				u32 ofs; 
				u32 len_byte;
				u32 adrs_p32;
				//
				// find offset 
				if (0==strncmp("#H", (char*)&buf[loc], 2)) {
					// read offset
					loc = loc + 2; // locate the numeric parameter head
					ofs = hexstr2data_u32((u8*)(buf+loc),2);
					loc = loc + 2; //		
#ifdef _SCPI_DEBUG_
					printf("check: 0x%02X\n",(unsigned int)ofs); 
#endif
					// skip spaces ' ' and tap
					while (1) {
						if      (buf[loc]==' ') loc++;
						else if (buf[loc]=='\t') loc++;
						else break;
					}
					// read command
					if (isdigit(buf[loc])) { //$$ isdigit() numeric parameter check
						len_byte = decstr2data_u32((u8*)(buf+loc),6); //$$ 6 bytes for data byte length
						loc = loc + 6; 
#ifdef _SCPI_DEBUG_
						printf("check: %06d\n",(unsigned int)len_byte); 
#endif
						// send numberic block head : #4_nnnnnn_
						sprintf((char*)rsp_str,"#4_%06d_",(int)len_byte); // '\0' added
						size = strlen((char*)rsp_str);
						ret = send_response_all(sn, rsp_str, size); //$$ first message
						if (ret < 0) {
							close(sn);
							return ret;
						}
						
						// send numeric block 
						// ... dcopy_pipe32_to_pipe8
						// send_response_all_from_pipe32() ... 
						adrs_p32 = ADRS_BASE_CMU + (ofs<<4); 
						ret = send_response_all_from_pipe32(sn, adrs_p32, len_byte); //$$ first message block
						if (ret < 0) {
							close(sn);
							return ret;
						}
						
						// return NL
						p_rsp_str = rsp_str__NL; // Newline sentinel. this will be last message block
						//
					}
					else {
						// return NG 
						p_rsp_str = rsp_str__NG;
					}
				}
				else {
					// return NG 
					p_rsp_str = rsp_str__NG;
				}
			}
			// TODO: case of  cmd_str__CMEP_MKWI
			else if (0==strncmp((char*)cmd_str__CMEP_MKWI,(char*)buf,LEN_CMD_STR__CMEP_MKWI)) { // 0 means eq
				// subfunctions:
				//    u32  read_mcs_ep_wi_mask();
				//    u32 write_mcs_ep_wi_mask(u32 mask);
				//
				u32 loc = LEN_CMD_STR__CMEP_MKWI;
				u32 val;
				//
				// skip spaces ' ' and tap
				while (1) {
					if      (buf[loc]==' ') loc++;
					else if (buf[loc]=='\t') loc++;
					else break;
				}
				//
#ifdef _SCPI_DEBUG_
				printf("para:%s\n",(char*)buf+loc);
#endif
				//
				// make scpi response string
				// ex ":CMEP:MKWI?\n"
				if (buf[loc]=='?') { 
					val = read_mcs_ep_wi_mask();
					// "#H00AA00BB\n"
					// rsp_str ...
					//sprintf((char*)rsp_str,"#H%08X\n\0",(unsigned int)val); // ex ""
					//rsp_str[9]='\r'; rsp_str[10]='\r'; rsp_str[11]='\r';
					sprintf((char*)rsp_str,"#H%08X\n",(unsigned int)val); // '\0' added. ex "H00000002\n"
					//printf("0x%02X 0x%02X 0x%02X \n",rsp_str[9],rsp_str[10],rsp_str[11]); // test
					p_rsp_str = rsp_str;
				}
				// ex ":CMEP:MKWI #HFFCCAA33\n"
				else if (0==strncmp("#H", (char*)&buf[loc], 2)) {
					//sscanf((char*)(buf+2),"%8x",(int*)&val); 
					// sscanf issue --> replace it with user subfunctions 
					// test sscanf -- pass with stack 2KB(0x800) and heap 32KB(0x8000).
					// NG 0x500 0x4000 ...  .rodata
					// issue ... sscanf footprint ... too big ...21KB more...
					// bss rodata heap on new mem... then works... how to add new mem...??
					// mem allocation??
					// bss... tcp buffer... make it small...  // looks possible.. large data must use pipe directly...
					// text... come from debuf test of the cmu_cpu... check! not much..
					// MicroBlaze MCS .. 128KB vs MicroBlaze .. 4GB
					//   https://www.xilinx.com/products/design-tools/mb-mcs.html
					//   https://www.xilinx.com/products/design-tools/microblaze.html
					//
					// 0x00050 0x1FFB0 // 0x00050+0x1FFB0=0x20000
					//
					// 0x06D00 0x19300 // 0x06D00+0x19300=0x20000
					// 0x00050 0x06CB0 // 0x00050+0x06CB0=0x6D00
					//
					// 0x06C00 0x19400 // 0x06C00+0x19400=0x20000
					// 0x00050 0x06BB0 // 0x00050+0x06BB0=0x06C00
					//
					// 0x06B00 0x19500 // 0x06B00+0x19500=0x20000
					// 0x00050 0x06AB0 // 0x00050+0x06AB0=0x06B00
					//
					// 0x06A00 0x19600 // 0x06A00+0x19600=0x20000
					// 0x00050 0x069B0 // 0x00050+0x069B0=0x06A00
					//
					loc = loc + 2; // locate the numeric parameter head
					// subfunction : u32 hexstr2data_u32(u8* hexstr, u32 len);
					// subfunction : u32 hexchr2data_u32(u8 hexchr);
					//
					//printf("para:%s\n",(char*)buf+loc);
					////
					//printf("h2d:%d\n",(int)hexchr2data_u32(buf[loc+0]));
					//printf("h2d:%d\n",(int)hexchr2data_u32(buf[loc+1]));
					//printf("h2d:%d\n",(int)hexchr2data_u32(buf[loc+2]));
					//printf("h2d:%d\n",(int)hexchr2data_u32(buf[loc+3]));
					//printf("h2d:%d\n",(int)hexchr2data_u32(buf[loc+4]));
					//printf("h2d:%d\n",(int)hexchr2data_u32(buf[loc+5]));					
					//printf("h2d:%d\n",(int)hexchr2data_u32(buf[loc+6]));
					//printf("h2d:%d\n",(int)hexchr2data_u32(buf[loc+7]));					
					////
					//val = hexchr2data_u32(buf[loc++]);
					////printf("val=%d\n",(int)val);
					//val = (val<<4) + hexchr2data_u32(buf[loc++]);
					//val = (val<<4) + hexchr2data_u32(buf[loc++]);
					//val = (val<<4) + hexchr2data_u32(buf[loc++]);
					//val = (val<<4) + hexchr2data_u32(buf[loc++]);
					//val = (val<<4) + hexchr2data_u32(buf[loc++]);
					//val = (val<<4) + hexchr2data_u32(buf[loc++]);
					//val = (val<<4) + hexchr2data_u32(buf[loc++]);
					//
					val = hexstr2data_u32((u8*)(buf+loc),8);
					//
#ifdef _SCPI_DEBUG_
					printf("check: 0x%08X\n",(unsigned int)val); 
#endif
					//
					// process...
					write_mcs_ep_wi_mask(val);
					//
					p_rsp_str = rsp_str__OK;
				}
				//
				else {
					p_rsp_str = rsp_str__NG;
				}
			}
			// TODO: case of  cmd_str__CMEP_MKWO
			else if (0==strncmp((char*)cmd_str__CMEP_MKWO,(char*)buf,LEN_CMD_STR__CMEP_MKWO)) { // 0 means eq
				// subfunctions:
				//    u32  read_mcs_ep_wo_mask(); //$$
				//    u32 write_mcs_ep_wo_mask(u32 mask); //$$
				//
				u32 loc = LEN_CMD_STR__CMEP_MKWO; //$$
				u32 val;
				//
				// skip spaces ' ' and tap
				while (1) {
					if      (buf[loc]==' ') loc++;
					else if (buf[loc]=='\t') loc++;
					else break;
				}
				//
#ifdef _SCPI_DEBUG_
				printf("para:%s\n",(char*)buf+loc);
#endif
				//
				// make scpi response string
				if (buf[loc]=='?') { 
					val = read_mcs_ep_wo_mask(); //$$
					sprintf((char*)rsp_str,"#H%08X\n",(unsigned int)val); // '\0' added. ex "H00000002\n"
					p_rsp_str = rsp_str;
				}
				else if (0==strncmp("#H", (char*)&buf[loc], 2)) {
					loc = loc + 2; // locate the numeric parameter head
					val = hexstr2data_u32((u8*)(buf+loc),8);
#ifdef _SCPI_DEBUG_
					printf("check: 0x%08X\n",(unsigned int)val); 
#endif
					//
					// process...
					write_mcs_ep_wo_mask(val); //$$
					//
					p_rsp_str = rsp_str__OK;
				}
				//
				else {
					p_rsp_str = rsp_str__NG;
				}
			}
			// TODO: case of  cmd_str__CMEP_MKTI
			else if (0==strncmp((char*)cmd_str__CMEP_MKTI,(char*)buf,LEN_CMD_STR__CMEP_MKTI)) { // 0 means eq
				// subfunctions:
				//    u32  read_mcs_ep_ti_mask(); //$$
				//    u32 write_mcs_ep_ti_mask(u32 mask); //$$
				//
				u32 loc = LEN_CMD_STR__CMEP_MKTI; //$$
				u32 val;
				//
				// skip spaces ' ' and tap
				while (1) {
					if      (buf[loc]==' ') loc++;
					else if (buf[loc]=='\t') loc++;
					else break;
				}
				//
#ifdef _SCPI_DEBUG_
				printf("para:%s\n",(char*)buf+loc);
#endif
				//
				// make scpi response string
				if (buf[loc]=='?') { 
					val = read_mcs_ep_ti_mask(); //$$
					sprintf((char*)rsp_str,"#H%08X\n",(unsigned int)val); // '\0' added. ex "H00000002\n"
					p_rsp_str = rsp_str;
				}
				else if (0==strncmp("#H", (char*)&buf[loc], 2)) {
					loc = loc + 2; // locate the numeric parameter head
					val = hexstr2data_u32((u8*)(buf+loc),8);
#ifdef _SCPI_DEBUG_
					printf("check: 0x%08X\n",(unsigned int)val); 
#endif
					//
					// process...
					write_mcs_ep_ti_mask(val); //$$
					//
					p_rsp_str = rsp_str__OK;
				}
				//
				else {
					p_rsp_str = rsp_str__NG;
				}
			}
			// TODO: case of  cmd_str__CMEP_MKTO
			else if (0==strncmp((char*)cmd_str__CMEP_MKTO,(char*)buf,LEN_CMD_STR__CMEP_MKTO)) { // 0 means eq
				// subfunctions:
				//    u32  read_mcs_ep_to_mask(); //$$
				//    u32 write_mcs_ep_to_mask(u32 mask); //$$
				//
				u32 loc = LEN_CMD_STR__CMEP_MKTO; //$$
				u32 val;
				//
				// skip spaces ' ' and tap
				while (1) {
					if      (buf[loc]==' ') loc++;
					else if (buf[loc]=='\t') loc++;
					else break;
				}
				//
#ifdef _SCPI_DEBUG_
				printf("para:%s\n",(char*)buf+loc);
#endif
				//
				// make scpi response string
				if (buf[loc]=='?') { 
					val = read_mcs_ep_to_mask(); //$$
					sprintf((char*)rsp_str,"#H%08X\n",(unsigned int)val); // '\0' added. ex "H00000002\n"
					p_rsp_str = rsp_str;
				}
				else if (0==strncmp("#H", (char*)&buf[loc], 2)) {
					loc = loc + 2; // locate the numeric parameter head
					val = hexstr2data_u32((u8*)(buf+loc),8);
#ifdef _SCPI_DEBUG_
					printf("check: 0x%08X\n",(unsigned int)val); 
#endif
					//
					// process...
					write_mcs_ep_to_mask(val); //$$
					//
					p_rsp_str = rsp_str__OK;
				}
				//
				else {
					p_rsp_str = rsp_str__NG;
				}
			}
			// TODO: case of  cmd_str__CMEP_WI
			else if (0==strncmp((char*)cmd_str__CMEP_WI,(char*)buf,LEN_CMD_STR__CMEP_WI)) { // 0 means eq
				// subfunctions:
				//    u32  read_mcs_ep_wi_data(u32 offset);
				//    u32 write_mcs_ep_wi_data(u32 offset, u32 data);
				//
				// # ":CMEP:WI#H00 #H00001234\n"
				// # ":CMEP:WI#H00?\n"
				// 
				u32 loc = LEN_CMD_STR__CMEP_WI; //$$
				u32 val;
				u32 ofs; 
				//
				// find offset 
				if (0==strncmp("#H", (char*)&buf[loc], 2)) {
					// read offset
					loc = loc + 2; // locate the numeric parameter head
					ofs = hexstr2data_u32((u8*)(buf+loc),2);
					loc = loc + 2; //		
#ifdef _SCPI_DEBUG_
					printf("check: 0x%02X\n",(unsigned int)ofs); 
#endif
					// skip spaces ' ' and tap
					while (1) {
						if      (buf[loc]==' ') loc++;
						else if (buf[loc]=='\t') loc++;
						else break;
					}
					// read command 
					if (buf[loc]=='?') { 
						val = read_mcs_ep_wi_data(ofs); //$$
						sprintf((char*)rsp_str,"#H%08X\n",(unsigned int)val); // '\0' added. ex "H00000002\n"
						p_rsp_str = rsp_str;
					}
					// write command
					else if (0==strncmp("#H", (char*)&buf[loc], 2)) {
						loc = loc + 2; // locate the numeric parameter head
						val = hexstr2data_u32((u8*)(buf+loc),8);
#ifdef _SCPI_DEBUG_
						printf("check: 0x%08X\n",(unsigned int)val); 
#endif
						write_mcs_ep_wi_data(ofs,val); //$$
						p_rsp_str = rsp_str__OK;
					}
					else {
						// return NG 
						p_rsp_str = rsp_str__NG;
					}
				}
				else {
					// return NG 
					p_rsp_str = rsp_str__NG;
				}
			}
			// TODO: case of  cmd_str__CMEP_WO
			else if (0==strncmp((char*)cmd_str__CMEP_WO,(char*)buf,LEN_CMD_STR__CMEP_WO)) { // 0 means eq
				// subfunctions:
				//    u32  read_mcs_ep_wo_data(u32 offset);
				//
				// # ":CMEP:WO#H20?\n"
				// 
				u32 loc = LEN_CMD_STR__CMEP_WO; //$$
				u32 val;
				u32 ofs; 
				//
				// find offset 
				if (0==strncmp("#H", (char*)&buf[loc], 2)) {
					// read offset
					loc = loc + 2; // locate the numeric parameter head
					ofs = hexstr2data_u32((u8*)(buf+loc),2);
					loc = loc + 2; //		
#ifdef _SCPI_DEBUG_
					printf("check: 0x%02X\n",(unsigned int)ofs); 
#endif
					// skip spaces ' ' and tap
					while (1) {
						if      (buf[loc]==' ') loc++;
						else if (buf[loc]=='\t') loc++;
						else break;
					}
					// read command 
					if (buf[loc]=='?') { 
						val = read_mcs_ep_wo_data(ofs); //$$
						sprintf((char*)rsp_str,"#H%08X\n",(unsigned int)val); // '\0' added. ex "H00000002\n"
						p_rsp_str = rsp_str;
					}
					else {
						// return NG 
						p_rsp_str = rsp_str__NG;
					}
				}
				else {
					// return NG 
					p_rsp_str = rsp_str__NG;
				}
			}
			// TODO: case of  cmd_str__CMEP_TI
			else if (0==strncmp((char*)cmd_str__CMEP_TI,(char*)buf,LEN_CMD_STR__CMEP_TI)) { // 0 means eq
				// subfunctions:
				//    u32  read_mcs_ep_ti_data(u32 offset);
				//    u32 write_mcs_ep_ti_data(u32 offset, u32 data);
				//
				// # ":CMEP:TI#H40 #H00000002\n"
				// # ":CMEP:TI#H40?\n"
				// 
				u32 loc = LEN_CMD_STR__CMEP_TI; //$$
				u32 val;
				u32 ofs; 
				//
				// find offset 
				if (0==strncmp("#H", (char*)&buf[loc], 2)) {
					// read offset
					loc = loc + 2; // locate the numeric parameter head
					ofs = hexstr2data_u32((u8*)(buf+loc),2);
					loc = loc + 2; //		
#ifdef _SCPI_DEBUG_
					printf("check: 0x%02X\n",(unsigned int)ofs); 
#endif
					// skip spaces ' ' and tap
					while (1) {
						if      (buf[loc]==' ') loc++;
						else if (buf[loc]=='\t') loc++;
						else break;
					}
					// read command 
					if (buf[loc]=='?') { 
						val = read_mcs_ep_ti_data(ofs); //$$
						sprintf((char*)rsp_str,"#H%08X\n",(unsigned int)val); // '\0' added. ex "H00000002\n"
						p_rsp_str = rsp_str;
					}
					// write command
					else if (0==strncmp("#H", (char*)&buf[loc], 2)) {
						loc = loc + 2; // locate the numeric parameter head
						val = hexstr2data_u32((u8*)(buf+loc),8);
#ifdef _SCPI_DEBUG_
						printf("check: 0x%08X\n",(unsigned int)val); 
#endif
						write_mcs_ep_ti_data(ofs,val); //$$
						p_rsp_str = rsp_str__OK;
					}
					else {
						// return NG 
						p_rsp_str = rsp_str__NG;
					}
				}
				else {
					// return NG 
					p_rsp_str = rsp_str__NG;
				}
			}
			// TODO: case of  cmd_str__CMEP_TO
			else if (0==strncmp((char*)cmd_str__CMEP_TO,(char*)buf,LEN_CMD_STR__CMEP_TO)) { // 0 means eq
				// subfunctions:
				//    u32  read_mcs_ep_to_data(u32 offset);
				//
				// # ":CMEP:TO#H60?\n"
				// 
				u32 loc = LEN_CMD_STR__CMEP_TO; //$$
				u32 val;
				u32 ofs; 
				//
				// find offset 
				if (0==strncmp("#H", (char*)&buf[loc], 2)) {
					// read offset
					loc = loc + 2; // locate the numeric parameter head
					ofs = hexstr2data_u32((u8*)(buf+loc),2);
					loc = loc + 2; //		
#ifdef _SCPI_DEBUG_
					printf("check: 0x%02X\n",(unsigned int)ofs); 
#endif
					// skip spaces ' ' and tap
					while (1) {
						if      (buf[loc]==' ') loc++;
						else if (buf[loc]=='\t') loc++;
						else break;
					}
					// read command 
					if (buf[loc]=='?') { 
						val = read_mcs_ep_to_data(ofs); //$$
						sprintf((char*)rsp_str,"#H%08X\n",(unsigned int)val); // '\0' added. ex "H00000002\n"
						p_rsp_str = rsp_str;
					}
					else {
						// return NG 
						p_rsp_str = rsp_str__NG;
					}
				}
				else {
					// return NG 
					p_rsp_str = rsp_str__NG;
				}
			}
			// TODO: case of  unknown
			else { // unknown commands 
				//p_rsp_str = rsp_str__NULL;
				p_rsp_str = rsp_str__NG;
			}
			
			// send response
			//
			size = strlen((char*)p_rsp_str);
			ret = send_response_all(sn, p_rsp_str, size); //$$
			if (ret < 0) {
				close(sn);
				return ret;
			}
			//
         }
         //$$
		 else { // recv size 0
			if (flag_get_rx==0) {
				cnt_stay_SOCK_ESTABLISHED = cnt_stay_SOCK_ESTABLISHED - 1;
			}
			else {
				flag_get_rx = 0;
				cnt_stay_SOCK_ESTABLISHED = MAX_CNT_STAY_SOCK_ESTABLISHED;
			}
#ifdef _SCPI_DEBUG_
				printf("connected socket has no recv data: cnt_stay_SOCK_ESTABLISHED:%d \n",(int)cnt_stay_SOCK_ESTABLISHED);
#endif
			if (cnt_stay_SOCK_ESTABLISHED==0) { // 
#ifdef _SCPI_DEBUG_
				printf("connected socket has no activity; force to close: cnt_stay_SOCK_ESTABLISHED:%d \n",(int)cnt_stay_SOCK_ESTABLISHED);
#endif
				// close socket
				close(sn);
			}
		 }
         break;
      case SOCK_CLOSE_WAIT :
#ifdef _SCPI_DEBUG_
         //printf("%d:CloseWait \n",sn);
#endif
         if((ret=disconnect(sn)) != SOCK_OK) return ret;
#ifdef _SCPI_DEBUG_
         printf("%d:Socket closed \n",sn);
#endif
         break;
      case SOCK_INIT :
#ifdef _SCPI_DEBUG_
    	 printf("%d:Listen, TCP server, port [%d] \n",sn, port);
#endif
         if( (ret = listen(sn)) != SOCK_OK) return ret;
         break;
      case SOCK_CLOSED:
#ifdef _SCPI_DEBUG_
         //printf("%d:TCP server start \n",sn);
#endif
		flag_SOCK_ESTABLISHED = 0;
         //if((ret=socket(sn, Sn_MR_TCP, port, 0x00)) != sn)
    	 if((ret=socket(sn, Sn_MR_TCP, port, SF_TCP_NODELAY)) != sn) //$$ fast ack
         //if((ret=socket(sn, Sn_MR_TCP, port, Sn_MR_ND)) != sn)
            return ret;
#ifdef _SCPI_DEBUG_
         printf("%d:Socket opened \n",sn);
         //printf("%d:Opened, TCP server, port [%d] \n",sn, port);
#endif
         break;
      case SOCK_LISTEN:
    	 //$$ nothing to do...
    	 break;
      default:
         break;
   }
   return 1;
}


// UDP Loopback Test
int32_t loopback_udps(uint8_t sn, uint8_t* buf, uint16_t port)
{
   int32_t  ret;
   uint16_t size, sentsize;
   uint8_t  destip[4];
   uint16_t destport;
   //uint8_t  packinfo = 0;
   uint8_t sr; //$$

   switch(sr=getSn_SR(sn))
   {
      case SOCK_UDP :
         if((size = getSn_RX_RSR(sn)) > 0)
         {
            if(size > DATA_BUF_SIZE) size = DATA_BUF_SIZE;
            ret = recvfrom(sn,buf,size,destip,(uint16_t*)&destport);
            if(ret <= 0)
            {
#ifdef _LOOPBACK_DEBUG_
               printf("%d: recvfrom error. %ld \n",sn,ret);
#endif
               return ret;
            }
            size = (uint16_t) ret;
            sentsize = 0;
            while(sentsize != size)
            {
               ret = sendto(sn,buf+sentsize,size-sentsize,destip,destport);
               if(ret < 0)
               {
#ifdef _LOOPBACK_DEBUG_
                  printf("%d: sendto error. %ld \n",sn,ret);
#endif
                  return ret;
               }
               sentsize += ret; // Don't care SOCKERR_BUSY, because it is zero.
            }
         }
         break;
      case SOCK_CLOSED:
#ifdef _LOOPBACK_DEBUG_
         //printf("%d:UDP loopback start \n",sn);
#endif
         if((ret=socket(sn,Sn_MR_UDP,port,0x00)) != sn)
            return ret;
#ifdef _LOOPBACK_DEBUG_
         printf("%d:Opened, UDP loopback, port [%d] \n",sn, port);
#endif
         break;
      default :
         break;
   }
   return 1;
}
