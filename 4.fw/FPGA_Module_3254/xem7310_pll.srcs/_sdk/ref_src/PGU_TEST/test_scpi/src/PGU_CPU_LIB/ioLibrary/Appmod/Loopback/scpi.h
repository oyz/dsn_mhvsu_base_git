
#ifndef  _SCPI_H_
#define  _SCPI_H_

// scpi
////#define _SCPI_DEBUG_ //$$ SCPI server debug
//#define _SCPI_DEBUG_WCMSG_

//#define DATA_BUF_SIZE_SCPI   2048 //$$ init
//#define DATA_BUF_SIZE_SCPI   256 //$$ test
#define DATA_BUF_SIZE_SCPI   2048+56 //$$ final
//#define DATA_BUF_SIZE_SCPI   4096 //$$ try
//#define DATA_BUF_SIZE_SCPI   4096+512 //$$ try
//#define DATA_BUF_SIZE_SCPI   4096+1024 //$$ try

//#define DATA_BUF_SIZE_SCPI_SUB   256 //$$ try


//#define MAX_CNT_STAY_SOCK_ESTABLISHED 0x08800000 // about 57 min stay at system clock 10MHz // 24us per count... // with two sockets opened
#define MAX_CNT_STAY_SOCK_ESTABLISHED 0x01800000 // about 10 min stay at system clock 10MHz // 24us per count... // with two sockets opened
//#define MAX_CNT_STAY_SOCK_ESTABLISHED 0x00800000 // about 3.3 min stay at system clock 10MHz // 24us per count... // with two sockets opened
//#define MAX_CNT_STAY_SOCK_ESTABLISHED 0x00300000 // about 75 sec stay at system clock 10MHz // 24us per count... // with two sockets opened
//#define MAX_CNT_STAY_SOCK_ESTABLISHED 0x00100000 // about 26 sec stay at system clock 10MHz
//#define MAX_CNT_STAY_SOCK_ESTABLISHED 0 // for no time limit to stay established


int32_t scpi_tcps(uint8_t sn, uint8_t* buf, uint16_t port); //$$ scpi server

#endif   // _SCPI_H_
