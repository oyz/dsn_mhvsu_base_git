//#include <stdio.h>
//#include <string.h>

#include "xil_printf.h" // print() for pure string; xil_printf() for formatted string
#include "microblaze_sleep.h" // for usleep()

// //$$ support PGU-CPU
// #include "./PGU_CPU_LIB/platform.h" // for init_platform()
// #include "./PGU_CPU_LIB/pgu_cpu_config.h" // PGU-CPU board config
// #include "./PGU_CPU_LIB/pgu_cpu.h" // for hw_reset

#include "MHVSU_BASE_LIB/platform.h"

#include "MHVSU_BASE_LIB/mhvsu_base_config.h" //$$ board dependent
#include "MHVSU_BASE_LIB/mcs_io_bridge_ext.h" //$$ board dependent

//$$ support W5500
#include "MHVSU_BASE_LIB/ioLibrary/Ethernet/W5500/w5500.h" // for w5500 io functions
#include "MHVSU_BASE_LIB/ioLibrary/Ethernet/socket.h"	// Just include one header for WIZCHIP // for close(SOCK_TCPS) and disconnect(SOCK_TCPS)

//$$ SCPI vs loopback socket
//#define _TEST_LOOPBACK_
#define _TEST_SCPI_

#ifdef _TEST_LOOPBACK_
#include "MHVSU_BASE_LIB/ioLibrary/Appmod/Loopback/loopback.h"
#endif

#ifdef _TEST_SCPI_
#include "MHVSU_BASE_LIB/ioLibrary/Appmod/Loopback/scpi.h" // for scpi server
#endif


/////////////////////////////////////////
// SOCKET NUMBER DEFINION for Examples //
/////////////////////////////////////////
#define SOCK_TCPS        0
//#define SOCK_TCPS_SUB    1

////////////////////////////////////////////////
// Shared Buffer Definition                   //
////////////////////////////////////////////////
#ifdef _TEST_LOOPBACK_
uint8_t gDATABUF[DATA_BUF_SIZE]; // DATA_BUF_SIZE from loopback.h // -->bss
#endif
#ifdef _TEST_SCPI_
uint8_t gDATABUF[DATA_BUF_SIZE_SCPI]; // DATA_BUF_SIZE_SCPI from scpi.h // -->bss
#endif


///////////////////////////////////
// Default Network Configuration //
///////////////////////////////////
// to be updated by HW info
wiz_NetInfo gWIZNETINFO = { .mac = {0x00, 0x08, 0xdc,0x00, 0xab, 0xcd},
							.ip = {192, 168, 168, 143},
							.sn = {255,255,255,0},
							.gw = {192, 168, 168, 1},
							.dns = {0,0,0,0},
							.dhcp = NETINFO_STATIC };
wiz_NetInfo gWIZNETINFO_rb; // read back

// set LAN timeout parameters
u16 gWIZNET_RTR = 2000; // setRTR(2000); // retry time based on 100us unit time.
u16 gWIZNET_RCR = 23  ; // setRCR(23);   // retry count

// network IC buf size : W5500 ... TX 16KB and RX 16KB
//uint8_t gMEMSIZE[2][8] = {{8,0,0,0,0,0,0,0},{8,0,0,0,0,0,0,0}}; // KB => 16KB // 31568
uint8_t gMEMSIZE[2][8] = {{16,0,0,0,0,0,0,0},{16,0,0,0,0,0,0,0}}; // KB

// dummy variable
uint8_t g_tmp_u8;
int32_t g_tmp_s32;

//////////////////////////////////
// For example of ioLibrary_BSD //
//////////////////////////////////
uint8_t network_init(void);								// Initialize Network information and display it
//int32_t loopback_tcps(uint8_t, uint8_t*, uint16_t);	// Loopback TCP server
//int32_t loopback_udps(uint8_t, uint8_t*, uint16_t);	// Loopback UDP server
//////////////////////////////////

//// EEPROM test mem 
uint8_t gEEPROM[2048];

int main(void)
{	
	//// for low-level driver test
	//u32 adrs;
	u32 value;
	//u32 mask;
	//u32 ii;
	u8 *p_tmp_u8;
	u8 tmp_buf[0x80];


	///////////////////////////////////////////
	// Host dependent peripheral initialized //
	///////////////////////////////////////////
	init_platform();


	//// test setup for print on jtag-terminal // stdio with mdm
	xil_printf("> Go MHVSU BASE!! \r\n");
	xil_printf(">>> build_info: ["__TIME__"],[" __DATE__ "]\r\n");
#ifdef  _SCPI_DEBUG_
	xil_printf(">> _SCPI_DEBUG_ mode enabled \r\n");
#endif

	//// test  mcs_io_bridge.v called in lan_endpoint_wrapper.v //{
	xil_printf(">> test mcs_io_bridge.v \r\n");

	// test read ADRS_FPGA_IMAGE_MHVSU
	_test_read_mcs(">>> FPGA_IMAGE_ID: \r\n", ADRS_FPGA_IMAGE_MHVSU);
	
	// test read FPGA_IMAGE_ID from mcs endpoint WO_20
	_test_read_mcs(">>> FPGA_IMAGE_ID from WO_20: \r\n", ADRS_PORT_WO_20_MHVSU);

	// set MASK for WI : ADRS_MASK_WI____MHVSU
	_test_write_mcs(">>> set MASK for WI: \r\n",     ADRS_MASK_WI____MHVSU, 0xFFFFFFFF);
	
	// clear test : ADRS_PORT_WI_01_MHVSU
	_test_write_mcs(">>>clear test control: \r\n",   ADRS_PORT_WI_01_MHVSU, 0x00000000);
	
	// test write LED data onto mcs endpoint WI_03
	_test_write_mcs(">>> write data on WI_03: \r\n", ADRS_PORT_WI_03_MHVSU, 0x00000000);

	// sleep to see LED change
	usleep(150000); // 150ms
	
	// test write LED data onto mcs endpoint WI_03
	_test_write_mcs(">>> write data on WI_03: \r\n", ADRS_PORT_WI_03_MHVSU, 0x0000000E); // RES_NET_0 / R on // ADC_PWR_ON
	
	// sleep to see LED change
	usleep(500000); // 500ms
		
	// test write LED data onto mcs endpoint WI_03
	//_test_write_mcs(">>> write data on WI_03: \r\n", ADRS_PORT_WI_03_MHVSU, 0x0000000B); // RES_NET_2 / G on // M0_SPI_MISO_EN

	// sleep to see LED change
	//usleep(500000); // 500ms
	
	// test write LED data onto mcs endpoint WI_03
	//_test_write_mcs(">>> write data on WI_03: \r\n", ADRS_PORT_WI_03_MHVSU, 0x00000007); // RES_NET_3 / B on // M1_SPI_MISO_EN

	// sleep to see LED change
	//usleep(500000); // 500ms
	
	// test write LED data onto mcs endpoint WI_03
	_test_write_mcs(">>> write data on WI_03: \r\n", ADRS_PORT_WI_03_MHVSU, 0x0000000D); // RES_NET_1 / Y on // only LED

	// sleep to see LED change
	usleep(500000); // 500ms

	// test write LED data onto mcs endpoint WI_03
	//_test_write_mcs(">>> write data on WI_03: \r\n", ADRS_PORT_WI_03_MHVSU, 0x0000000F); // M1_SPI_MISO_EN(1:on), M0_SPI_MISO_EN(1:on), LED(1:off), ADC_PWR_ON(1:on)
	_test_write_mcs(">>> write data on WI_03: \r\n", ADRS_PORT_WI_03_MHVSU, 0x00000000);  // M1_SPI_MISO_EN(0:off), M0_SPI_MISO_EN(0:off), LED(0:on), ADC_PWR_ON(0:off)
	
	//}

	//// test reg //{
	//   ADRS_BASE_MHVSU
	//   ADRS_TEST_REG___OFST
	xil_printf(">> test ADRS_TEST_REG___OFST \r\n");
	//xil_printf("mcs rd: 0x%08X \r\n",  read_mcs_io (ADRS_BASE_MHVSU + ADRS_TEST_REG___OFST)              );
	//xil_printf("mcs wr: 0x%08X \r\n",  write_mcs_io(ADRS_BASE_MHVSU + ADRS_TEST_REG___OFST, 0xABCD1234)  );
	//xil_printf("mcs rd: 0x%08X \r\n",  read_mcs_io (ADRS_BASE_MHVSU + ADRS_TEST_REG___OFST)              );
	//xil_printf("mcs wr: 0x%08X \r\n",  write_mcs_io(ADRS_BASE_MHVSU + ADRS_TEST_REG___OFST, 0xCBCBA5A5)  );
	value = read_mcs_io (ADRS_BASE_MHVSU + ADRS_TEST_REG___OFST);
	if (value==0xACACCDCD) 
		value = 0;
	else 
		value += 1; // count up every process start
	value = write_mcs_io(ADRS_BASE_MHVSU + ADRS_TEST_REG___OFST, value);
	value = read_mcs_io (ADRS_BASE_MHVSU + ADRS_TEST_REG___OFST);
	xil_printf("mcs rd: 0x%08X \r\n", value );
	
	//}
	
	//// test read  MON_XADC_WO or temperature
	_test_read_mcs(">>> FPGA_IMAGE_ID from WO_23: \r\n", ADRS_PORT_WO_23_MHVSU);
	value = read_mcs_io (ADRS_PORT_WO_23_MHVSU);
	xil_printf("mcs rd: %d \r\n", value );
	
	
	//// test counters //{
	
	xil_printf(">>> test count2 \r\n");
	
	// read_mcs_ep_to @ 0x61
	//   assign w_TEST_TO   = {15'b0, count2eqFF, 14'b0, count1eq80, count1eq00};
	value = read_mcs_ep_to(ADRS_BASE_MHVSU, 0x61, 0xFFFFFFFF);
	xil_printf("TO61 rd: 0x%08X \r\n", value );

	// read  : test counts at WO21 
	value = read_mcs_ep_wo(ADRS_BASE_MHVSU, 0x21, 0x0000FFFF);
	xil_printf("WO21 rd: 0x%08X \r\n", value );

	// read_mcs_ep_to @ 0x61
	//   assign w_TEST_TO   = {15'b0, count2eqFF, 14'b0, count1eq80, count1eq00};
	value = read_mcs_ep_to(ADRS_BASE_MHVSU, 0x61, 0xFFFFFFFF);
	xil_printf("TO61 rd: 0x%08X \r\n", value );

	//
	activate_mcs_ep_ti(ADRS_BASE_MHVSU,0x41,0); // reset : test count2 TI41[0]
	value = read_mcs_ep_wo(ADRS_BASE_MHVSU, 0x21, 0x0000FFFF);
	xil_printf("WO21 rd: 0x%08X \r\n", value );
	
	//
	activate_mcs_ep_ti(ADRS_BASE_MHVSU,0x41,1); // up    : test count2 TI41[1] 
	value = read_mcs_ep_wo(ADRS_BASE_MHVSU, 0x21, 0x0000FFFF);
	xil_printf("WO21 rd: 0x%08X \r\n", value );
	//
	activate_mcs_ep_ti(ADRS_BASE_MHVSU,0x41,2); // down  : test count2 TI41[2] 
	value = read_mcs_ep_wo(ADRS_BASE_MHVSU, 0x21, 0x0000FFFF);
	xil_printf("WO21 rd: 0x%08X \r\n", value );
	
	// read_mcs_ep_to @ 0x61
	//   assign w_TEST_TO   = {15'b0, count2eqFF, 14'b0, count1eq80, count1eq00};
	value = read_mcs_ep_to(ADRS_BASE_MHVSU, 0x61, 0xFFFFFFFF);
	xil_printf("TO61 rd: 0x%08X \r\n", value );

	//
	activate_mcs_ep_ti(ADRS_BASE_MHVSU,0x41,2); // down  : test count2 TI41[2] 
	value = read_mcs_ep_wo(ADRS_BASE_MHVSU, 0x21, 0x0000FFFF);
	xil_printf("WO21 rd: 0x%08X \r\n", value );

	// read_mcs_ep_to @ 0x61
	//   assign w_TEST_TO   = {15'b0, count2eqFF, 14'b0, count1eq80, count1eq00};
	value = read_mcs_ep_to(ADRS_BASE_MHVSU, 0x61, 0xFFFFFFFF);
	xil_printf("TO61 rd: 0x%08X \r\n", value );

	// read_mcs_ep_to @ 0x61
	//   assign w_TEST_TO   = {15'b0, count2eqFF, 14'b0, count1eq80, count1eq00};
	value = read_mcs_ep_to(ADRS_BASE_MHVSU, 0x61, 0xFFFFFFFF);
	xil_printf("TO61 rd: 0x%08X \r\n", value );

	//
	activate_mcs_ep_ti(ADRS_BASE_MHVSU,0x41,1); // up    : test count2 TI41[1] 
	value = read_mcs_ep_wo(ADRS_BASE_MHVSU, 0x21, 0x0000FFFF);
	xil_printf("WO21 rd: 0x%08X \r\n", value );

	// read_mcs_ep_to @ 0x61
	//   assign w_TEST_TO   = {15'b0, count2eqFF, 14'b0, count1eq80, count1eq00};
	value = read_mcs_ep_to(ADRS_BASE_MHVSU, 0x61, 0xFFFFFFFF);
	xil_printf("TO61 rd: 0x%08X \r\n", value );

	// read_mcs_ep_to @ 0x61
	//   assign w_TEST_TO   = {15'b0, count2eqFF, 14'b0, count1eq80, count1eq00};
	value = read_mcs_ep_to(ADRS_BASE_MHVSU, 0x61, 0xFFFFFFFF);
	xil_printf("TO61 rd: 0x%08X \r\n", value );

	// test count :  set autocount2
	xil_printf(">>> test count :  set autocount2 \r\n");
	write_mcs_ep_wi(ADRS_BASE_MHVSU, 0x01, 0x00000000, 0x00000004); // adrs_base, EP_offset_EP, data, mask
	write_mcs_ep_wi(ADRS_BASE_MHVSU, 0x01, 0x00000004, 0x00000004); // adrs_base, EP_offset_EP, data, mask
	
	//}

	//// TODO: test eeprom : 11AA160T-I/TT //{
		
	xil_printf(">>> test eeprom  \r\n");
	
	// call eeprom functions in mcs_io_bridge_ext.h
	
	// test ep 
	eeprom_send_frame_ep(0,0); // (u32 MEM_WI_b32, u32 MEM_FDAT_WI_b32)
	
	// 
	value = eeprom_set_g_var(1,0); // (u8 EEPROM__LAN_access, u8 EEPROM__on_TP)
	xil_printf("> eeprom_set_g_var: 0x%08X \r\n", value );
	
	// 
	value = eeprom_set_g_var(1,1); // (u8 EEPROM__LAN_access, u8 EEPROM__on_TP)
	xil_printf("> eeprom_set_g_var: 0x%08X \r\n", value );

	// 
	eeprom_send_frame(0x05, 0, 0, 0, 1, 0); // (u8 CMD_b8, u8 STA_in_b8, u8 ADL_b8, u8 ADH_b8, u16 num_bytes_DAT_b16, u8 con_disable_SBP_b8);

	// read status
	value = eeprom_read_status();
	xil_printf("> eeprom_read_status: 0x%08X \r\n", value );
	
	// check connection
	eeprom_set_g_var(1, 0); // # EEPROM on MEM_SIO
	if (is_eeprom_available()) {
		xil_printf(">> EEPROM on MEM_SIO is available. \r\n");
	}
	else {
		eeprom_set_g_var(1, 1); // # EEPROM on TP
		if (is_eeprom_available()) {
			xil_printf(">> EEPROM on TP is available. \r\n");
		}
		else {
			xil_printf(">> EEPROM is NOT available. \r\n");
		}
	}
	
	//// test eeprom_erase_all()
	//eeprom_erase_all();
	//// test eeprom_set_all()
	//eeprom_set_all();
	
	// test check sum
	value = cal_checksum (32, (u8*)"1234567812345678test try good~!!");
	xil_printf("cal_checksum: 0x%02X \r\n", value );
	value = gen_checksum (32, (u8*)"1234567812345678test try good~!!");
	xil_printf("gen_checksum: 0x%02X \r\n", value );
	// test hex display
	hex_txt_display (32, (u8*)"1234567812345678test try good~!!", 0x0020);

	// test read all from EEPROM to g_EEPROM__buf_2KB
	eeprom_read_all();
	p_tmp_u8 = get_adrs__g_EEPROM__buf_2KB();
	//hex_txt_display (16*4, p_tmp_u8, 0x0000);
	hex_txt_display (2048, p_tmp_u8, 0x0000);
	
	// test eeprom write 
	eeprom_write_data (0x0030, 2, (u8*)"?!"); // (u16 ADRS_b16, u16 num_bytes_DAT_b16, u8 *buf_datain)
	eeprom_read_data (0x0000, 16*4, tmp_buf); // (u16 ADRS_b16, u16 num_bytes_DAT_b16, u8 *buf_dataout)
	hex_txt_display (16*4, tmp_buf, 0x0000);

	//  // test write all from g_EEPROM__buf_2KB to EEPROM
	//  p_tmp_u8 = get_adrs__g_EEPROM__buf_2KB();
	//  p_tmp_u8[0x32] = '@';// touch some 
	//  eeprom_write_all();
	//  eeprom_read_all();
	//  //hex_txt_display (2048, p_tmp_u8, 0x0000);
	//  hex_txt_display (16*20, p_tmp_u8, 0x0000);


#define _NET_INIT_FROM_EEPROM_ 
#ifdef _NET_INIT_FROM_EEPROM_

	// EEPROM header check 
	eeprom_read_all();
	p_tmp_u8 = get_adrs__g_EEPROM__buf_2KB();
	value = cal_checksum (32, &p_tmp_u8[0x10]); // 
	xil_printf("cal_checksum in g_EEPROM[0x10:0x2F] : 0x%02X \r\n", value );


	
	if (value==0) {
		
		value = chk_all_zeros (32, &p_tmp_u8[0x10]); // 
		xil_printf("chk_all_zeros in g_EEPROM[0x10:0x2F] : 0x%02X \r\n", value );
		
		if (value == 0) { // not all zero
		
			//// update IP/MAC ...
			//   SIP = [192,168,168,143]
			//   SUB = [255,255,255,  0]
			//   GAR = [192,168,168,  1]
			//   DNS = [  0,  0,  0,  0]
			//   MAC = '0008dc00abcd'
			//   SID = '06'
			//   UID = '$'
			//   CKS = '#'

			gWIZNETINFO.ip[0]  = p_tmp_u8[0x10];
			gWIZNETINFO.ip[1]  = p_tmp_u8[0x11];
			gWIZNETINFO.ip[2]  = p_tmp_u8[0x12];
			gWIZNETINFO.ip[3]  = p_tmp_u8[0x13];
			//
			gWIZNETINFO.sn[0]  = p_tmp_u8[0x14];
			gWIZNETINFO.sn[1]  = p_tmp_u8[0x15];
			gWIZNETINFO.sn[2]  = p_tmp_u8[0x16];
			gWIZNETINFO.sn[3]  = p_tmp_u8[0x17];
			//
			gWIZNETINFO.gw[0]  = p_tmp_u8[0x18];
			gWIZNETINFO.gw[1]  = p_tmp_u8[0x19];
			gWIZNETINFO.gw[2]  = p_tmp_u8[0x1A];
			gWIZNETINFO.gw[3]  = p_tmp_u8[0x1B];
			//
			gWIZNETINFO.dns[0] = p_tmp_u8[0x1C];
			gWIZNETINFO.dns[1] = p_tmp_u8[0x1D];
			gWIZNETINFO.dns[2] = p_tmp_u8[0x1E];
			gWIZNETINFO.dns[3] = p_tmp_u8[0x1F];
			//
			gWIZNETINFO.mac[0] = hexstr2data_u32(&p_tmp_u8[0x20],2);
			gWIZNETINFO.mac[1] = hexstr2data_u32(&p_tmp_u8[0x22],2);
			gWIZNETINFO.mac[2] = hexstr2data_u32(&p_tmp_u8[0x24],2);
			gWIZNETINFO.mac[3] = hexstr2data_u32(&p_tmp_u8[0x26],2);
			gWIZNETINFO.mac[4] = hexstr2data_u32(&p_tmp_u8[0x28],2);
			gWIZNETINFO.mac[5] = hexstr2data_u32(&p_tmp_u8[0x2A],2);
		
			xil_printf(">> MAC and IP are set by EEPROM info. \r\n");
			
			// 000  0x0000  4D 48 56 53 55 5F 42 41  53 45 5F 23 30 30 31 32  MHVSU_BASE_#0012  // ... BID
			// 001  0x0010  C0 A8 A8 8C FF FF FF 00  C0 A8 A8 01 00 00 00 00  ................
			// 002  0x0020  30 30 30 38 64 63 30 30  61 62 64 39 30 36 24 80  0008dc00abd906$.  // ... SID + UID + CKS
			// 003  0x0030  3F 21 2D 5F 2D 5F 2D 5F  2D 5F 2D 5F 2D 5F 2D 5F  ?!-_-_-_-_-_-_-_			
			
			
			//// update SID and BID into MCS_SETUP_WI @ WI11
			// WI11 :
			//   bit [31:16] = board ID // 0000~9999, set from EEPROM via MCS
			//   bit [10]    = select__L_LAN_on_FPGA_MD__H_LAN_on_BASE_BD // set from MCS
			//   bit [9]     = w_con_port__L_MEM_SIO__H_TP                // set from MCS
			//   bit [8]     = w_con_fifo_path__L_sspi_H_lan              // set from MCS
			//   bit [3:0]   = slot ID  // 00~99, set from EEPROM via MCS
			_test_write_mcs(">>> set BID and SID: \r\n", ADRS_PORT_WI_11_MHVSU, 
				//(p_tmp_u8[0x0E]<<24) + (p_tmp_u8[0x0F]<<16) +  // BID
				(decstr2data_u32(&p_tmp_u8[0x0C],4)<<16) + // BID
				 decstr2data_u32(&p_tmp_u8[0x2C],2)        // SID
				);
			_test_read_mcs (">>> get BID and SID: \r\n", ADRS_PORT_WI_11_MHVSU);
			
			xil_printf(">> BID and SID are set by EEPROM info. \r\n");
			
		}
	}


#endif 	
	


	// eeprom control back to S-SPI
	eeprom_set_g_var(0, 0);	
	
	//}


	//// LAN port and LED running
	// LAN port selection : ADRS_PORT_WI_01_MHVSU bit[16] --> ADRS_PORT_WI_11_MHVSU bit[10]
	// assign select__L_LAN_on_FPGA_MD__H_LAN_on_BASE_BD = w_MCS_SETUP_WI[10];
	// set autocount2     : ADRS_PORT_WI_01_MHVSU bit[2]
	//
	//_test_write_mcs(">>>LAN-on-BASE selected: \r\n",   ADRS_PORT_WI_01_MHVSU, 0x00010004);
	xil_printf(">>>LAN-on-BASE selected: \r\n");
	write_mcs_ep_wi(ADRS_BASE_MHVSU, 0x11, 0x00000400, 0x00000400);
	//
	//_test_write_mcs(">>>LAN-on-FPGA selected: \r\n",   ADRS_PORT_WI_01_MHVSU, 0x00000004);
	//xil_printf(">>>LAN-on-FPGA selected: \r\n");
	//write_mcs_ep_wi(ADRS_BASE_MHVSU, 0x11, 0x00000000, 0x00000400);


	//// hw reset wz850
	xil_printf(">>> hw reset wz850 \r\n");
	hw_reset__wz850();
	
	//// socket setup //{

	/* WIZCHIP SOCKET Buffer initialize */
	if(ctlwizchip(CW_INIT_WIZCHIP,(void*)gMEMSIZE) == -1)
	{
	   xil_printf("WIZCHIP Initialized fail. \n");
	   while(1);
	}

	//  /* PHY link status check */
	//  do
	//  {
	//  	if(ctlwizchip(CW_GET_PHYLINK, (void*)&g_tmp_u8) == -1)
	//  	  xil_printf("Unknown PHY Link status. \n");
	//  }while(g_tmp_u8 == PHY_LINK_OFF);
	
	/* Network initialization */
	g_tmp_u8 = network_init();
	if (g_tmp_u8 == 1) {
		xil_printf(">> LAN is not PRESENT, or MAC is not allowed. \r\n");
		//while(1) ; // stop here
		
		//// try LAN-on-FPGA
		//_test_write_mcs(">>>LAN-on-FPGA selected: \r\n",   ADRS_PORT_WI_01_MHVSU, 0x00000004);
		xil_printf(">>>LAN-on-FPGA selected: \r\n");
		write_mcs_ep_wi(ADRS_BASE_MHVSU, 0x11, 0x00000000, 0x00000400);
		
		hw_reset__wz850();
		ctlwizchip(CW_INIT_WIZCHIP,(void*)gMEMSIZE);
		network_init();
	}
	
	//// if MAC are all FF, network IC is not present

	/* PHY link check */
	ctlwizchip(CW_GET_PHYLINK, (void*)&g_tmp_u8);
	if (g_tmp_u8 == PHY_LINK_OFF) {
		xil_printf(">>> PHY_LINK_OFF \r\n");
		// close socket
		close(SOCK_TCPS);
		//disconnect(SOCK_TCPS); // hanging; must remove.
		
		// turn off running LED 
		write_mcs_ep_wi(ADRS_BASE_MHVSU, 0x01, 0x00000000, 0x00000004);
	
		// wait for PHY_LINK_ON
		do
		{
			ctlwizchip(CW_GET_PHYLINK, (void*)&g_tmp_u8);
		}while(g_tmp_u8 == PHY_LINK_OFF);
	
		// turn on running LED 
		write_mcs_ep_wi(ADRS_BASE_MHVSU, 0x01, 0x00000004, 0x00000004);


		// re-init network
		network_init(); 
		
	}


	
	//// read configuration info 
//#define _NET_INIT_FROM_MCS_ 
//#ifdef _NET_INIT_FROM_MCS_
//	_test_read_mcs(">>> LAN_CONF_00_MHVSU: \r\n", ADRS_LAN_CONF_00_MHVSU);
//	_test_read_mcs(">>> LAN_CONF_01_MHVSU: \r\n", ADRS_LAN_CONF_01_MHVSU);
//	_test_read_mcs(">>> LAN_CONF_02_MHVSU: \r\n", ADRS_LAN_CONF_02_MHVSU);
//	_test_read_mcs(">>> LAN_CONF_03_MHVSU: \r\n", ADRS_LAN_CONF_03_MHVSU);
//	
//	u32 value_ip      ;
//	u32 value_mac_low ;
//	u32 value_mac_high;
//	u32 value_rtr_rcr;
//
//	value_ip       = read_mcs_io (ADRS_BASE_MHVSU + ADRS_LAN_CONF_00_OFST); // ip
//	value_mac_low  = read_mcs_io (ADRS_BASE_MHVSU + ADRS_LAN_CONF_01_OFST); // mac low
//	value_mac_high = read_mcs_io (ADRS_BASE_MHVSU + ADRS_LAN_CONF_02_OFST); // mac high
//	value_rtr_rcr  = read_mcs_io (ADRS_BASE_MHVSU + ADRS_LAN_CONF_03_OFST); // { rtr, rcr }
//	
//	
//	// update  mac ip  address and so on
//	gWIZNETINFO.mac[0] = (value_mac_high >>  8 ) & 0xFF;
//	gWIZNETINFO.mac[1] = (value_mac_high >>  0 ) & 0xFF;
//	gWIZNETINFO.mac[2] = (value_mac_low  >> 24 ) & 0xFF;
//	gWIZNETINFO.mac[3] = (value_mac_low  >> 16 ) & 0xFF;
//	gWIZNETINFO.mac[4] = (value_mac_low  >>  8 ) & 0xFF;
//	gWIZNETINFO.mac[5] = (value_mac_low  >>  0 ) & 0xFF;
//	//
//	gWIZNETINFO.ip[0]  = (value_ip >> 24 ) & 0xFF;
//	gWIZNETINFO.ip[1]  = (value_ip >> 16 ) & 0xFF;
//	gWIZNETINFO.ip[2]  = (value_ip >>  8 ) & 0xFF;
//	gWIZNETINFO.ip[3]  = (value_ip >>  0 ) & 0xFF;
//	//
//	gWIZNETINFO.gw[0]  = (value_ip >> 24 ) & 0xFF;
//	gWIZNETINFO.gw[1]  = (value_ip >> 16 ) & 0xFF;
//	gWIZNETINFO.gw[2]  = (value_ip >>  8 ) & 0xFF;
//	gWIZNETINFO.gw[3]  = 0x01;
//	//
//	gWIZNET_RTR        = (value_rtr_rcr >>  16 ) & 0xFFFF;
//	gWIZNET_RCR        = (value_rtr_rcr >>   0 ) & 0xFFFF;
//#endif 	


	
	//  /* Network initialization */
	//  network_init();

	//}
	

	/*****************************************/
	/* WIZnet W5500 inside                   */
	/*****************************************/


#ifdef _TEST_LOOPBACK_
	/* TODO: Main loop for TCP Loopback test */
	while(1)
	{
		/* Loopback Test */
		// TCP server loopback test
		if( (g_tmp_s32 = loopback_tcps(SOCK_TCPS, gDATABUF, 5025)) < 0) {
			xil_printf("SOCKET ERROR : %ld \n", g_tmp_s32);
		}

		// UDP server loopback test
		// if( (g_tmp_s32 = loopback_udps(SOCK_UDPS, gDATABUF, 3000)) < 0) {
		// 	xil_printf("SOCKET ERROR : %ld \n", g_tmp_s32);
		// }

		/* PHY link check */
		ctlwizchip(CW_GET_PHYLINK, (void*)&g_tmp_u8);
		if (g_tmp_u8 == PHY_LINK_OFF) {
			xil_printf(">>> PHY_LINK_OFF \r\n");
			// close socket
			close(SOCK_TCPS);
			//disconnect(SOCK_TCPS); // hanging; must remove.
			
			// turn off running LED 
			write_mcs_ep_wi(ADRS_BASE_MHVSU, 0x01, 0x00000000, 0x00000004);
		
			// wait for PHY_LINK_ON
			do
			{
				ctlwizchip(CW_GET_PHYLINK, (void*)&g_tmp_u8);
			}while(g_tmp_u8 == PHY_LINK_OFF);
		
			// turn on running LED 
			write_mcs_ep_wi(ADRS_BASE_MHVSU, 0x01, 0x00000004, 0x00000004);
	
			// re-init network
			network_init(); 
		}

	} // end of Main loop
#endif


#ifdef _TEST_SCPI_
	/* TODO: Main loop for TCP socket based SCPI server test */
	while(1)
	{
		//  /* SCPI server Test */
		if( (g_tmp_s32 = scpi_tcps_ep_state(SOCK_TCPS, gDATABUF, 5025)) < 0) {
			xil_printf("SOCKET ERROR : %ld \n", g_tmp_s32);
		}

		//  if( (g_tmp_s32 = scpi_tcps(SOCK_TCPS, gDATABUF, 5025)) < 0) {
		//  	xil_printf("SOCKET ERROR : %ld \n", g_tmp_s32);
		//  }
	
		//  // sub port
		//  if( (g_tmp_s32 = scpi_tcps(SOCK_TCPS_SUB, gDATABUF, 5050)) < 0) {
		//  	xil_printf("SOCKET ERROR : %ld \n", g_tmp_s32);
		//  }
	
	
		/* PHY link check */
		ctlwizchip(CW_GET_PHYLINK, (void*)&g_tmp_u8);
		if (g_tmp_u8 == PHY_LINK_OFF) {
			xil_printf(">>> PHY_LINK_OFF \r\n"); 
			// close socket
			close(SOCK_TCPS);
			//disconnect(SOCK_TCPS); // hanging; must remove.
			
			// turn off running LED 
			write_mcs_ep_wi(ADRS_BASE_MHVSU, 0x01, 0x00000000, 0x00000004);
		
			// wait for PHY_LINK_ON
			do
			{
				ctlwizchip(CW_GET_PHYLINK, (void*)&g_tmp_u8);
			}while(g_tmp_u8 == PHY_LINK_OFF);
		
			// turn on running LED 
			write_mcs_ep_wi(ADRS_BASE_MHVSU, 0x01, 0x00000004, 0x00000004);
	
			// re-init network
			network_init(); 
		}
	
	} // end of Main loop
#endif

    /////
    cleanup_platform();

    // TODO: stay
    //while(1);

} // end of main()

/////////////////////////////////////////////////////////////
// Intialize the network information to be used in WIZCHIP //
/////////////////////////////////////////////////////////////
uint8_t network_init(void)
{
	uint8_t tmpstr[6];
	uint8_t ret;
	
	// set
	ctlnetwork(CN_SET_NETINFO, (void*)&gWIZNETINFO);
	// get
	ctlnetwork(CN_GET_NETINFO, (void*)&gWIZNETINFO_rb);

	// set timeout para
	//setRTR(2000);
	setRTR(gWIZNET_RTR);
	//setRCR(23);
	setRCR(gWIZNET_RCR);

	// Display Network Information
	ctlwizchip(CW_GET_ID,(void*)tmpstr);
	//
	xil_printf("\r\n=== %s NET CONF === \r\n",(char*)tmpstr);
	xil_printf("MAC: %02X:%02X:%02X:%02X:%02X:%02X \r\n",
		gWIZNETINFO_rb.mac[0],gWIZNETINFO_rb.mac[1],gWIZNETINFO_rb.mac[2],
		gWIZNETINFO_rb.mac[3],gWIZNETINFO_rb.mac[4],gWIZNETINFO_rb.mac[5]);
	xil_printf("SIP: %d.%d.%d.%d \r\n", gWIZNETINFO_rb.ip[0],gWIZNETINFO_rb.ip[1],gWIZNETINFO_rb.ip[2],gWIZNETINFO_rb.ip[3]);
	xil_printf("SUB: %d.%d.%d.%d \r\n", gWIZNETINFO_rb.sn[0],gWIZNETINFO_rb.sn[1],gWIZNETINFO_rb.sn[2],gWIZNETINFO_rb.sn[3]);
	xil_printf("GAR: %d.%d.%d.%d \r\n", gWIZNETINFO_rb.gw[0],gWIZNETINFO_rb.gw[1],gWIZNETINFO_rb.gw[2],gWIZNETINFO_rb.gw[3]);
	xil_printf("DNS: %d.%d.%d.%d \r\n", gWIZNETINFO_rb.dns[0],gWIZNETINFO_rb.dns[1],gWIZNETINFO_rb.dns[2],gWIZNETINFO_rb.dns[3]);
	xil_printf("====================== \r\n");
	xil_printf("RTR: %d (0x%04X) \r\n", getRTR(), getRTR());
	xil_printf("RCR: %d (0x%04X) \r\n", getRCR(), getRCR());
	xil_printf("====================== \r\n");
	
	ret = 0;
	if ( (gWIZNETINFO_rb.mac[0]==0xFF) &&
		 (gWIZNETINFO_rb.mac[1]==0xFF) &&
		 (gWIZNETINFO_rb.mac[2]==0xFF) &&
		 (gWIZNETINFO_rb.mac[3]==0xFF) &&
		 (gWIZNETINFO_rb.mac[4]==0xFF) &&
		 (gWIZNETINFO_rb.mac[5]==0xFF) )
		ret = 1;
	return ret;
	


}
/////////////////////////////////////////////////////////////


